﻿using Etopup_Ws.old_App_Code;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Services;
using RestSharp;

namespace Etopup_Ws
{
    /// <summary>
    /// Summary description for reports
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class reports : System.Web.Services.WebService
    {
        DataSet ds = new DataSet();
        string strQuery = string.Empty;
        string strLimit = string.Empty;
        ODBC objODBC = new ODBC();
        ODBC_LOG objODBCLOG = new ODBC_LOG();
        DataTable dt = new DataTable();
        DataTable dt2 = new DataTable();
        old_App_Code.DateFormat objDT = new old_App_Code.DateFormat();
        Encrypt_Decrypt ED = new Encrypt_Decrypt();
        CreateFile objCreateFile = new CreateFile();
        CommonFunction objcf = new CommonFunction();
        old_App_Code.DateFormat objDateFor = new old_App_Code.DateFormat();
        // intActivityType=75
        [WebMethod]
        public DataSet er_admin_member_activity_log_report(string strUserName, int intUserType, int intUserID, int usertype_id, int PageSize, int intPage, string strSearch, string strIPAddress, int intDesignationType, string strMacAddress, string strOSDetails, string strIMEINo, string strGcmID, string strAPPVersion, int intSource, int intCreatedByType)
        {
            try
            {
                if (intUserID > 0)
                {
                    try
                    {
                        objCreateFile.CreateTextFile(strIPAddress, strMacAddress, strOSDetails, strIMEINo, strGcmID, strAPPVersion, intSource.ToString(), intCreatedByType, 75, " Admin Member Activity Log Report", strUserName);
                    }
                    catch (Exception)
                    {


                    }
                    finally
                    {
                        strLimit = " LIMIT " + PageSize * (intPage - 1) + "," + PageSize + "";

                        if (intUserType > 0) // only for admin log mem member
                        {
                            int intCount = objODBCLOG.executeScalar_int("select count(1) from er_activity_logs a inner join er_login_log l on a.userid = l.userid where a.userid=" + intUserID + " and a.user_type=" + intUserType + " " + strSearch + "");

                            strQuery = "select l.username as User_ID,l.full_name as Name,a.ip_address as IP_Address,a.created_on as Login_Date_Time,a.os_details as Browser_Details,a.description as Description,a.old_value as Old_Value,a.modified_value as Modified_Value," + intCount + " as intCount from er_activity_logs a inner join er_login_log l on a.userid = l.userid where a.userid=" + intUserID + " and a.user_type =" + intUserType + " " + strSearch + " order by a.id desc " + strLimit + "";
                        }
                        else if (intUserType == 0) //  for all member log visible by admin only
                        {
                            int intCount = objODBCLOG.executeScalar_int("select count(1) from er_activity_logs a inner join er_login_log l on a.userid = l.userid where a.userid>1  and a.user_type >1 " + strSearch + "");

                            strQuery = "select l.username as User_ID,l.full_name as Name,a.ip_address as IP_Address,a.created_on as Login_Date_Time,a.os_details as Browser_Details,a.gcm_id as GCM_ID,a.imei_no as IMEI_No,a.app_version as App_Version,a.description as Description,a.old_value as Old_Value,a.modified_value as Modified_Value," + intCount + " as intCount from er_activity_logs a inner join er_login_log l on a.userid = l.userid where a.userid >1 and a.user_type >1 " + strSearch + " order by a.id desc " + strLimit + "";

                        }

                    }
                    ds = objODBCLOG.Report(strQuery);
                }
                return ds;
            }
            catch (Exception)
            {
                throw;

            }
            finally
            {
                ds.Dispose();
            }
        }


        // int ActivityType=76
        [WebMethod]
        public DataSet er_Wallet_Request_Report(string strUserName, int intUserID, int PageSize, int intPage, string strSearch, string strIPAddress, int intDesignationType, string strMacAddress, string strOSDetails, string strIMEINo, string strGcmID, string strAPPVersion, int intSource, int intCreatedByType)
        {
            try
            {
                try
                {
                    objCreateFile.CreateTextFile(strIPAddress, strMacAddress, strOSDetails, strIMEINo, strGcmID, strAPPVersion, intSource.ToString(), intCreatedByType, 76, " Wallet Request Report ", strUserName);
                }
                catch (Exception)
                {


                }
                finally
                {
                    if (intUserID > 0)
                    {
                        strLimit = " LIMIT " + PageSize * (intPage - 1) + "," + PageSize + "";

                        strQuery = "select l.username as User_ID,l.full_name as Full_Name,l.emailid as Email_Id,l.mobile as Mobile_No,case when w.payment_mode=1 then 'Cash' when w.payment_mode=2 then 'Bank Transfer' end as Payment_Mode,w.request_amt as Amount,w.rcpt_url,case when w.status=0 then 'Pending' when w.status=1 then 'Approved' when w.status=2 then 'Rejected' end as Status,w.comment as Comment,date_format(w.created_on,'%d %b %Y %H:%i:%s') as Requested_on,date_format(w.confirmed_on,'%d %b %Y %H:%i:%s') as Confirmed_on ,(select count(1) from er_wallet_purchase w inner join er_login l on w.userid = l.userid where w.parent_id =" + intUserID + " " + strSearch + ") as intCount from er_wallet_purchase w inner join er_login l on w.userid = l.userid where w.parent_id =" + intUserID + " " + strSearch + " order by w.id desc " + strLimit + "";



                        ds = objODBC.Report(strQuery);



                    }
                }
                return ds;
            }
            catch (Exception)
            {

                throw;
            }
            finally
            {
                ds.Dispose();
            }
        }



        // ActivityType=77 -- retailer
        [WebMethod]
        public DataSet er_Wallet_Transfer_Report(string strUserName, int intUserID, int PageSize, int intPage, string strSearch, string strIPAddress, int intDesignationType, string strMacAddress, string strOSDetails, string strIMEINo, string strGcmID, string strAPPVersion, int intSource, int intCreatedByType)
        {
            try
            {
                try
                {
                    objCreateFile.CreateTextFile(strIPAddress, strMacAddress, strOSDetails, strIMEINo, strGcmID, strAPPVersion, intSource.ToString(), intCreatedByType, 77, "  Wallet Transfer Report ", strUserName);
                }
                catch (Exception)
                {


                }
                finally
                {

                    if (intUserID > 0)
                    {
                        if (intSource == 2)
                        {
                            strLimit = " LIMIT " + PageSize * (intPage - 1) + "," + PageSize + "";
                            int intCount = objODBC.executeScalar_int("select count(1) from er_wallet_transfer_individual w inner join er_login l on w.parent_id = l.userid inner join er_login t on w.userid = t.userid where  w.parent_id=" + intUserID + "  " + strSearch + "");

                            strQuery = "select l.username as Sender_ID,l.full_name as Sender_Name,t.full_name as Receiver_Name,t.username as Receiver_ID,w.transfer_amt as Amount_Transferred,date_format(w.created_on,'%d %b %Y %H:%i:%s') as Date_of_Transfer," + intCount + " as intCount from er_wallet_transfer_individual w inner join er_login l on w.parent_id=l.userid inner join er_login t on w.userid = t.userid where w.parent_id=" + intUserID + " " + strSearch + " order by w.id desc " + strLimit + "";
                        }
                        else
                        {
                            strLimit = " LIMIT " + PageSize * (intPage - 1) + "," + PageSize + "";
                            int intCount = objODBC.executeScalar_int("select count(1) from er_wallet_transfer_individual w inner join er_login l on w.parent_id = l.userid inner join er_login t on w.userid = t.userid where  w.parent_id=" + intUserID + "  " + strSearch + "");

                            strQuery = "select l.username as Sender_ID,l.full_name as Sender_Name,t.full_name as Receiver_Name,t.username as Receiver_ID,w.transfer_amt as Amount_Transferred,date_format(w.created_on,'%d %b %Y %H:%i:%s') as Date_of_Transfer,w.trans_number," + intCount + " as intCount from er_wallet_transfer_individual w inner join er_login l on w.parent_id=l.userid inner join er_login t on w.userid = t.userid where w.parent_id=" + intUserID + " " + strSearch + " order by w.id desc " + strLimit + "";

                            //SELECT `id`, `trans_number`, `userid`, `parent_id`, `transfer_amt`, `total_amt`, `created_on`, `type` FROM `er_wallet_transfer_individual` WHERE 1
                            // 1 - by Admin; 2 - Reseller to Reseller; 3 - Reseller to member; 4 - member to member
                        }
                        ds = objODBC.Report(strQuery);
                    }
                   

                }
                return ds;
            }
            catch (Exception ex)
            {
                throw;
            }
            finally
            {
                ds.Dispose();
            }
        }


        // ActivityType=77 -- retailer
        [WebMethod]
        public DataSet er_Wallet_Transfer_AD_AllMember_Report(string strUserName, int intUserID, int PageSize, int intPage, string strSearch, string strIPAddress, int intDesignationType, string strMacAddress, string strOSDetails, string strIMEINo, string strGcmID, string strAPPVersion, int intSource, int intCreatedByType)
        {
            try
            {
                try
                {
                    objCreateFile.CreateTextFile(strIPAddress, strMacAddress, strOSDetails, strIMEINo, strGcmID, strAPPVersion, intSource.ToString(), intCreatedByType, 77, "  Wallet Transfer Report ", strUserName);
                }
                catch (Exception)
                {


                }
                finally
                {

                    
                    if (intUserID == 1)
                    {
                        if(intSource==2)
                        {
                            strLimit = " LIMIT " + PageSize * (intPage - 1) + "," + PageSize + "";
                            int intCount = objODBC.executeScalar_int("select count(1) from er_wallet_transfer_individual w inner join er_login l on w.parent_id = l.userid inner join er_login t on w.userid = t.userid where  w.parent_id!=" + intUserID + "  " + strSearch + "");

                            strQuery = "select l.full_name as Sender_Name,l.username as Sender_ID,case when l.usertype_id=1 then 'Reseller' when  l.usertype_id=3 then 'Distributor' when l.usertype_id=4 then 'Sub-Distributor' when l.usertype_id=5 then 'Retailer' end as SenderUser_Type,t.full_name as Receiver_Name,t.username as Receiver_ID,case when t.usertype_id=1 then 'Reseller' when  t.usertype_id=3 then 'Distributor' when t.usertype_id=4 then 'Sub-Distributor' when t.usertype_id=5 then 'Retailer' end as ReceiverUser_Type,w.transfer_amt as Amount_Transferred,date_format(w.created_on,'%d %b %Y %H:%i:%s') as Date_of_Transfer," + intCount + " as intCount from er_wallet_transfer_individual w inner join er_login l on w.parent_id=l.userid inner join er_login t on w.userid = t.userid where w.parent_id!=" + intUserID + " " + strSearch + " order by w.id desc " + strLimit + "";

                            //SELECT `id`, `trans_number`, `userid`, `parent_id`, `transfer_amt`, `total_amt`, `created_on`, `type` FROM `er_wallet_transfer_individual` WHERE 1
                            // 1 - by Admin; 2 - Reseller to Reseller; 3 - Reseller to member; 4 - member to member
                        }
                        else
                        {
                            strLimit = " LIMIT " + PageSize * (intPage - 1) + "," + PageSize + "";
                            int intCount = objODBC.executeScalar_int("select count(1) from er_wallet_transfer_individual w inner join er_login l on w.parent_id = l.userid inner join er_login t on w.userid = t.userid where  w.parent_id!=" + intUserID + "  " + strSearch + "");

                            strQuery = "select l.full_name as Sender_Name,l.username as Sender_ID,case when l.usertype_id=1 then 'Reseller' when  l.usertype_id=3 then 'Distributor' when l.usertype_id=4 then 'Sub-Distributor' when l.usertype_id=5 then 'Retailer' end as SenderUser_Type,t.full_name as Receiver_Name,t.username as Receiver_ID,case when t.usertype_id=1 then 'Reseller' when  t.usertype_id=3 then 'Distributor' when t.usertype_id=4 then 'Sub-Distributor' when t.usertype_id=5 then 'Retailer' end as ReceiverUser_Type,w.transfer_amt as Amount_Transferred,date_format(w.created_on,'%d %b %Y %H:%i:%s') as Date_of_Transfer,w.trans_number," + intCount + " as intCount from er_wallet_transfer_individual w inner join er_login l on w.parent_id=l.userid inner join er_login t on w.userid = t.userid where w.parent_id!=" + intUserID + " " + strSearch + " order by w.id desc " + strLimit + "";

                            //SELECT `id`, `trans_number`, `userid`, `parent_id`, `transfer_amt`, `total_amt`, `created_on`, `type` FROM `er_wallet_transfer_individual` WHERE 1
                            // 1 - by Admin; 2 - Reseller to Reseller; 3 - Reseller to member; 4 - member to member
                        }

                        ds = objODBC.Report(strQuery);
                    }

                }
                return ds;
            }
            catch (Exception ex)
            {
                throw;
            }
            finally
            {
                ds.Dispose();
            }
        }
        // ActivityType=78
        [WebMethod]
        public DataSet er_Reverse_Fund_Report(string strUserName, int intUserID, int PageSize, int intPage, string strSearch, string strIPAddress, int intDesignationType, string strMacAddress, string strOSDetails, string strIMEINo, string strGcmID, string strAPPVersion, int intSource, int intCreatedByType)
        {
            try
            {
                try
                {
                    objCreateFile.CreateTextFile(strIPAddress, strMacAddress, strOSDetails, strIMEINo, strGcmID, strAPPVersion, intSource.ToString(), intCreatedByType, 78, " er_Reverse_Fund_Report ", strUserName);
                }
                catch (Exception)
                {


                }
                finally
                {
                   
                    if (intUserID > 0)
                    {
                        if (intSource == 2)
                        {
                            strLimit = " LIMIT " + PageSize * (intPage - 1) + "," + PageSize + "";

                            int count = objODBC.executeScalar_int("select count(1) from ap_reverse_fund r inner join er_login l on r.userid = l.userid where r.userid >0 " + strSearch + "");

                            strQuery = "select l.username as User_ID,l.full_name as Name,case when l.usertype_id=1 then 'Reseller' when  l.usertype_id=3 then 'Distributor' when l.usertype_id=4 then 'Sub-Distributor' when l.usertype_id=5 then 'Retailer' end as User_Type,r.total_amt as Amount_Reverted,p.username as Revert_by_UserID,p.full_name as Revert_by_Name,date_format(r.created_on,'%d %b %Y %H:%i:%s') as Date," + count + " from ap_reverse_fund r inner join er_login l on r.userid = l.userid inner join er_login p on r.parent_id = p.userid where r.userid >0 " + strSearch + " order by r.id desc " + strLimit + "";
                        }
                        else
                        {


                            strLimit = " LIMIT " + PageSize * (intPage - 1) + "," + PageSize + "";

                            int count = objODBC.executeScalar_int("select count(1) from ap_reverse_fund r inner join er_login l on r.userid = l.userid where r.userid >0 " + strSearch + "");

                            strQuery = "select l.username as User_ID,l.full_name as Name,case when l.usertype_id=1 then 'Reseller' when  l.usertype_id=3 then 'Distributor' when l.usertype_id=4 then 'Sub-Distributor' when l.usertype_id=5 then 'Retailer' end as User_Type,r.total_amt as Amount_Reverted,p.username as Revert_by_UserID,p.full_name as Revert_by_Name,date_format(r.created_on,'%d %b %Y %H:%i:%s') as Date,r.trans_number," + count + " from ap_reverse_fund r inner join er_login l on r.userid = l.userid inner join er_login p on r.parent_id = p.userid where r.userid >0 " + strSearch + " order by r.id desc " + strLimit + "";
                        }
                    }


                    ds = objODBC.Report(strQuery);
                }
                return ds;
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                ds.Dispose();
            }
        }

        // ActivityType=79
        [WebMethod]
        public double er_CommissionForAdmin(string strUserName, int intUserID,string strIPAddress, int intDesignationType, string strMacAddress, string strOSDetails, string strIMEINo, string strGcmID, string strAPPVersion, int intSource, int intCreatedByType)
        {
            double dblcommission = 0;

            try
            {
                dblcommission = objODBC.executeScalar_dbl("select sum(c.commission_amount) from er_commission_amount c inner join er_login l on c.parent_id = l.userid inner join er_recharge r on c.recharge_id = r.id where c.parent_id= " + intUserID + " and c.status=2 ");
                objCreateFile.CreateTextFile(strIPAddress, strMacAddress, strOSDetails, strIMEINo, strGcmID, strAPPVersion, intSource.ToString(), intCreatedByType, 79, " er_CommissionForAdmin ", strUserName);                
            }
            catch (Exception)
            {
                dblcommission = 0;
            }           
            return dblcommission;
        }

        // ActivityType=79
        [WebMethod]
        public DataSet er_Commission_Report(string strUserName, int intUserID, int PageSize, int intPage, string strSearch, string strIPAddress, int intDesignationType, string strMacAddress, string strOSDetails, string strIMEINo, string strGcmID, string strAPPVersion, int intSource, int intCreatedByType)
        {
            try
            {
                try
                {
                    objCreateFile.CreateTextFile(strIPAddress, strMacAddress, strOSDetails, strIMEINo, strGcmID, strAPPVersion, intSource.ToString(), intCreatedByType, 79, " er_Commission_Report ", strUserName);
                }
                catch (Exception)
                {


                }
                finally
                {
                    if (intUserID > 0)
                    {
                        if (strUserName != "Admin" && intCreatedByType != 1)
                        {
                            strLimit = " LIMIT " + PageSize * (intPage - 1) + "," + PageSize + "";

                            int intCount = objODBC.executeScalar_int("select count(1)  from er_commission_amount c inner join er_login l on c.parent_id = l.userid inner join er_recharge r on c.recharge_id = r.id where c.parent_id= " + intUserID + " and c.status=2 " + strSearch + "");

                            strQuery = "select l.username as User_ID,l.full_name as Name,case when l.usertype_id=1 then 'Reseller' when  l.usertype_id=3 then 'Distributor' when l.usertype_id=4 then 'Sub-Distributor' when l.usertype_id=5 then 'Retailer' end as User_Type,case when c.operator_id=1 then 'Salaam' when c.operator_id=2 then 'Etisalat' when c.operator_id=3 then 'Roshan' when c.operator_id=5 then 'AWCC' when c.operator_id=4 then 'MTN' when c.operator_id=5 then 'AWCC' end as Operator,r.mobile_number as Mobile_No,c.recharge_amount as Amount,c.recharge_amount- c.commission_amount as Deduct_Amount,c.commission_amount as Commission_Earned,date_format(c.created_on,'%d %b %Y %H:%i:%s') as Date_Time," + intCount + " as intCount from er_commission_amount c inner join er_login l on c.parent_id = l.userid inner join er_recharge r on c.recharge_id = r.id where c.parent_id = " + intUserID + " and c.status=2 " + strSearch + " order by c.id desc " + strLimit + "";
                        }
                        else
                        {
                            strLimit = " LIMIT " + PageSize * (intPage - 1) + "," + PageSize + "";

                            int intCount = objODBC.executeScalar_int("select count(1)  from er_commission_amount c inner join er_login_admin l on c.parent_id = l.userid inner join er_recharge r on c.recharge_id = r.id where c.parent_id = 1 and c.status=2 " + strSearch + " order by c.id desc");

                            strQuery = "select l.username as User_ID,l.full_name as Name,case when c.operator_id=1 then 'Salaam' when c.operator_id=2 then 'Etisalat' when c.operator_id=3 then 'Roshan' when c.operator_id=5 then 'AWCC' when c.operator_id=4 then 'MTN' when c.operator_id=5 then 'AWCC' end as Operator,r.mobile_number as Mobile_No,c.recharge_amount as Amount,c.recharge_amount-c.commission_amount as Deduct_Amount,c.commission_amount as Commission_Earned,date_format(c.created_on,'%d %b %Y %H:%i:%s') as Date_Time," + intCount + " as intCount from er_commission_amount c inner join er_login_admin l on c.parent_id = l.userid inner join er_recharge r on c.recharge_id = r.id where c.parent_id = 1 and c.status=2 " + strSearch + " order by c.id desc " + strLimit + "";
                        }

                    }
                    else
                    {
                        strLimit = " LIMIT " + PageSize * (intPage - 1) + "," + PageSize + "";

                        int intCount = objODBC.executeScalar_int("select count(1) from er_commission_amount c inner join er_login l on c.parent_id = l.userid inner join er_recharge r on r.id = c.recharge_id where c.userid >0 and c.status=2 " + strSearch + "");

                        strQuery = "select l.username as User_ID,l.full_name as Name,case when l.usertype_id=1 then 'Reseller' when  l.usertype_id=3 then 'Distributor' when l.usertype_id=4 then 'Sub-Distributor' when l.usertype_id=5 then 'Retailer' end as User_Type,case when c.operator_id=1 then 'Salaam' when c.operator_id=2 then 'Etisalat' when c.operator_id=3 then 'Roshan' when c.operator_id=5 then 'AWCC' when c.operator_id=4 then 'MTN' when c.operator_id=5 then 'AWCC' end as Operator,r.mobile_number as Mobile_No,c.recharge_amount as Amount,c.recharge_amount-c.commission_amount as Deduct_Amount,c.commission_amount as Commission_Earned,date_format(c.created_on,'%d %b %Y %H:%i:%s') as Date_Time," + intCount + " as intCount from er_commission_amount c inner join er_login l on c.parent_id = l.userid inner join er_recharge r on r.id = c.recharge_id where c.userid >0 and c.status=2 " + strSearch + " order by c.id desc " + strLimit + "";
                    }

                }
                ds = objODBC.Report(strQuery);

                return ds;
            }
            catch (Exception)
            {

                throw;
            }
            finally
            {
                ds.Dispose();
            }
        }



        // ActivityType=79
        [WebMethod]
        public DataSet er_Commission_SubAdmin_Report(string strUserName, int intUserID, int PageSize, int intPage, string strSearch, string strIPAddress, int intDesignationType, string strMacAddress, string strOSDetails, string strIMEINo, string strGcmID, string strAPPVersion, int intSource, int intCreatedByType,int intType)
        {
            // intType 1= Admin &  2= Member else All
            try
            {
                try
                {
                    objCreateFile.CreateTextFile(strIPAddress, strMacAddress, strOSDetails, strIMEINo, strGcmID, strAPPVersion, intSource.ToString(), intCreatedByType, 79, " er_Commission_Report ", strUserName);
                }
                catch (Exception)
                {


                }
                finally
                {
                    if (intType == 1) // Admin
                    {

                        strLimit = " LIMIT " + PageSize * (intPage - 1) + "," + PageSize + "";

                        int intCount = objODBC.executeScalar_int("select count(1)  from er_commission_amount c inner join er_login_admin l on c.parent_id = l.userid inner join er_recharge r on c.recharge_id = r.id inner join er_login m on c.userid = m.userid where c.parent_id = 1 and c.status=2 " + strSearch + " order by c.id desc");

                        strQuery = "select l.username as User_ID,l.full_name as Name,m.username as MemberID,m.full_name as MemberName,c.recharge_id,case when c.operator_id=1 then 'Salaam' when c.operator_id=2 then 'Etisalat' when c.operator_id=3 then 'Roshan' when c.operator_id=5 then 'AWCC' when c.operator_id=4 then 'MTN' when c.operator_id=5 then 'AWCC' end as Operator,r.mobile_number as Mobile_No,c.recharge_amount as Amount,c.recharge_amount-c.commission_amount as Deduct_Amount,c.commission_amount as Commission_Earned,date_format(c.created_on,'%d %b %Y %H:%i:%s') as Date_Time," + intCount + " as intCount from er_commission_amount c inner join er_login_admin l on c.parent_id = l.userid inner join er_recharge r on c.recharge_id = r.id inner join er_login m on c.userid = m.userid where c.parent_id = 1 and c.status=2 " + strSearch + " order by c.id desc " + strLimit + "";


                    }
                    else if (intType == 2) // Member
                    {
                        strLimit = " LIMIT " + PageSize * (intPage - 1) + "," + PageSize + "";

                        int intCount = objODBC.executeScalar_int("select count(1) from er_commission_amount c inner join er_login l on c.parent_id = l.userid inner join er_recharge r on r.id = c.recharge_id inner join er_login m on c.userid = m.userid where c.userid >0 and c.status=2 " + strSearch + "");

                        strQuery = "select l.username as User_ID,l.full_name as Name,m.username as MemberID,m.full_name as MemberName,c.recharge_id,case when l.usertype_id=1 then 'Reseller' when  l.usertype_id=3 then 'Distributor' when l.usertype_id=4 then 'Sub-Distributor' when l.usertype_id=5 then 'Retailer' end as User_Type,case when c.operator_id=1 then 'Salaam' when c.operator_id=2 then 'Etisalat' when c.operator_id=3 then 'Roshan' when c.operator_id=5 then 'AWCC' when c.operator_id=4 then 'MTN' when c.operator_id=5 then 'AWCC' end as Operator,r.mobile_number as Mobile_No,c.recharge_amount as Amount,c.recharge_amount-c.commission_amount as Deduct_Amount,c.commission_amount as Commission_Earned,date_format(c.created_on,'%d %b %Y %H:%i:%s') as Date_Time," + intCount + " as intCount from er_commission_amount c inner join er_login l on c.parent_id = l.userid inner join er_recharge r on r.id = c.recharge_id inner join er_login m on c.userid = m.userid where c.parent_id > 1 and c.status=2 " + strSearch + " order by c.id desc " + strLimit + "";
                    }
                    else  // All 
                    {
                        strLimit = " LIMIT " + PageSize * (intPage - 1) + "," + PageSize + "";

                        int intCount = objODBC.executeScalar_int("select count(1) from er_commission_amount c inner join er_login l on c.parent_id = l.userid inner join er_recharge r on r.id = c.recharge_id inner join er_login m on c.userid = m.userid where c.userid >0 and c.status=2 " + strSearch + "");

                        strQuery = "select l.username as User_ID,l.full_name as Name,m.username as MemberID,m.full_name as MemberName,c.recharge_id,case when l.usertype_id=1 then 'Reseller' when  l.usertype_id=3 then 'Distributor' when l.usertype_id=4 then 'Sub-Distributor' when l.usertype_id=5 then 'Retailer' end as User_Type,case when c.operator_id=1 then 'Salaam' when c.operator_id=2 then 'Etisalat' when c.operator_id=3 then 'Roshan' when c.operator_id=5 then 'AWCC' when c.operator_id=4 then 'MTN' when c.operator_id=5 then 'AWCC' end as Operator,r.mobile_number as Mobile_No,c.recharge_amount as Amount,c.recharge_amount-c.commission_amount as Deduct_Amount,c.commission_amount as Commission_Earned,date_format(c.created_on,'%d %b %Y %H:%i:%s') as Date_Time," + intCount + " as intCount from er_commission_amount c inner join er_login l on c.parent_id = l.userid inner join er_recharge r on r.id = c.recharge_id inner join er_login m on c.userid = m.userid where c.userid>0 and c.status=2 " + strSearch + " order by c.id desc " + strLimit + "";
                    }

                }
                ds = objODBC.Report(strQuery);

                return ds;
            }
            catch (Exception)
            {

                throw;
            }
            finally
            {
                ds.Dispose();
            }
        }

        // ActivityType=80
        [WebMethod]
        public DataSet er_emoney_Generation_Report(string strUserName, int intUserID, int PageSize, int intPage, string strSearch, string strIPAddress, int intDesignationType, string strMacAddress, string strOSDetails, string strIMEINo, string strGcmID, string strAPPVersion, int intSource, int intCreatedByType)
        {
            try
            {
                try
                {
                    objCreateFile.CreateTextFile(strIPAddress, strMacAddress, strOSDetails, strIMEINo, strGcmID, strAPPVersion, intSource.ToString(), intCreatedByType, 80, "er_emoney_Generation_Report ", strUserName);
                }
                catch (Exception)
                {


                }
                finally
                {
                    strLimit = " LIMIT " + PageSize * (intPage - 1) + "," + PageSize + "";

                    if (intUserID > 0)
                    {
                        int intCount = objODBC.executeScalar_int("select count(1) from  er_emoney_transaction c inner join er_login_admin l on c.admin_id=l.userid where c.admin_id > 0  " + strSearch + "");

                        strQuery = "select l.username as User_ID,l.full_name as Name,l.emailid as Email_ID,case when c.wallet_id=1 then 'Salaam' when c.wallet_id=2 then 'Etisalat' when c.wallet_id=3 then 'Roshan' when c.wallet_id=5 then 'AWCC' when c.wallet_id=4 then 'MTN' end as Operator,date_format(c.created_on,'%d %b %Y %H:%i:%s') as Date_Time,c.closing_balance as Operator_Closing_Balance,c.amt as Amount," + intCount + " as intCount from er_emoney_transaction c inner join er_login_admin l on c.admin_id=l.userid where c.admin_id > 0  " + strSearch + " order by c.id desc " + strLimit + "";
                    }


                }
                ds = objODBC.Report(strQuery);

                return ds;
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                ds.Dispose();
            }
        }



        // ActivityType=80
        [WebMethod]
        public DataSet er_Rollback_emoney_Generation_Report(string strUserName, int intUserID, int PageSize, int intPage, string strSearch, string strIPAddress, int intDesignationType, string strMacAddress, string strOSDetails, string strIMEINo, string strGcmID, string strAPPVersion, int intSource, int intCreatedByType)
        {
            try
            {
                try
                {
                    objCreateFile.CreateTextFile(strIPAddress, strMacAddress, strOSDetails, strIMEINo, strGcmID, strAPPVersion, intSource.ToString(), intCreatedByType, 80, "er_Rollback_emoney_Generation_Report ", strUserName);
                }
                catch (Exception)
                {


                }
                finally
                {
                    strLimit = " LIMIT " + PageSize * (intPage - 1) + "," + PageSize + "";

                    if (intUserID > 0)
                    {
                       
                        int intCount = objODBC.executeScalar_int("select count(1) from  er_rollback_transaction c inner join er_login_admin l on c.admin_id=l.userid where c.admin_id > 0  " + strSearch + "");

                        strQuery = "select l.username as User_ID,l.full_name as Name,IF(c.recharge_id=0,'NA',c.recharge_id) as Recharge_ID,case when c.wallet_id=1 then 'Salaam' when c.wallet_id=2 then 'Etisalat' when c.wallet_id=3 then 'Roshan' when c.wallet_id=5 then 'AWCC' when c.wallet_id=4 then 'MTN' end as Operator,date_format(c.created_on,'%d %b %Y %H:%i:%s') as Date_Time,c.closing_balance as Operator_Closing_Balance,c.amt as Amount,case when c.trans_type=1 then 'Credit' when c.trans_type=2 then 'Debit' end as TransType," + intCount + " as intCount from er_rollback_transaction c inner join er_login_admin l on c.admin_id=l.userid where c.admin_id > 0  " + strSearch + " order by c.id desc " + strLimit + "";
                    }


                }
                ds = objODBC.Report(strQuery);

                return ds;
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                ds.Dispose();
            }
        }

        // ActivityType=81 -- this is for ratailer app and admin web
        [WebMethod]
        public DataSet er_topup_Report(string strUserName, int intUserID, int PageSize, int intPage, string strSearch, string strIPAddress, int intDesignationType, string strMacAddress, string strOSDetails, string strIMEINo, string strGcmID, string strAPPVersion, int intSource, int intCreatedByType)
        {
            try
            {
                try
                {
                    objCreateFile.CreateTextFile(strIPAddress, strMacAddress, strOSDetails, strIMEINo, strGcmID, strAPPVersion, intSource.ToString(), intCreatedByType, 81, " er_topup_Report", strUserName);

                   
                }
                catch (Exception)
                {


                }
                finally
                {
                    strLimit = " LIMIT " + PageSize * (intPage - 1) + "," + PageSize + "";
                    if (intCreatedByType == 1)
                    {
                        if (intUserID > 0)
                        {
                            if (strSearch == string.Empty)
                            {
                                string strDate = objDT.getCurDateString();
                                strSearch += " AND DATE(c.created_on)  BETWEEN '" + strDate + "' AND '" + strDate + "'";
                            }

                            int intCount = objODBC.executeScalar_int("Select count(1) from er_recharge c inner join er_login l on c.userid = l.userid where c.userid > 0 " + strSearch + " order by c.id desc");

                            strQuery = "select l.username as User_ID,l.full_name as Name,l.mobile as Customer_Contact,c.trans_number as Txn_No,c.operator_name as Operator,c.mobile_number as Top_Up_Mobile_No,c.amount as Amount,c.deduct_amt as Deducted_Amount,c.operator_balance as API_Balance,date_format(c.created_on,'%d %b %Y %H:%i:%s') as Date_of_Txn,c.operator_transid as Operator_Txn_Id,case when c.status=1 then 'Pending' when c.status=2 then 'Success' when c.status=3 then 'Failed' when c.dispute_status=1 then 'Disputed' end as Txn_Status,case when c.source = 1 then 'Web' when c.source = 2 then 'Android' when c.source = 3 then 'IOS' when c.source = 4 then 'USSD' when c.source=5 then 'SMS' end as Txn_Mode,c.operator_name as API_Type,l.province_Name as Province,case when c.rollback_status = 0 then 'Not Rollback' when c.rollback_status = 1 then 'Requested' when c.rollback_status = 2 then 'Processing' when c.rollback_status = 3 then 'Rejected' when c.rollback_status = 4 then 'Completed' end as Roll_Statuss,c.rollback_amount,c.comment," + intCount + " as intCount from er_recharge c inner join er_login l on c.userid = l.userid where c.userid > 0 " + strSearch + " order by c.id desc " + strLimit + "";
                        }
                    }
                    else
                    {
                        if (intUserID > 0)
                        {
                            //if (strSearch == string.Empty)
                            //{
                            //    string strDate = objDT.getCurDateString();
                            //    strSearch += " AND DATE(c.created_on)  BETWEEN '" + strDate + "' AND '" + strDate + "'";
                            //}

                            int intCount = objODBC.executeScalar_int("select count(1) from er_recharge c inner join er_login l on c.userid = l.userid where c.userid='" + intUserID + "' " + strSearch + " order by c.id");
                            strQuery = "select l.username as User_ID,l.full_name as Name,l.mobile as Customer_Contact,c.trans_number as Txn_No,c.operator_name as Operator,c.mobile_number as Top_Up_Mobile_No,c.amount as Amount,c.deduct_amt as Deducted_Amount,c.operator_balance as API_Balance, date_format(c.created_on,'%d %b %Y %H:%i:%s') as Date_of_Txn,c.operator_transid as Operator_Txn_Id,case when c.status=1 then 'Pending' when c.status=2 then 'Success' when c.status=3 then 'Failed' when c.dispute_status=1 then 'Disputed' end as Txn_Status,case when c.source = 1 then 'Web' when c.source = 2 then 'Android' when c.source = 3 then 'IOS' when c.source = 4 then 'USSD' when c.source=5 then 'SMS' end as Txn_Mode,c.operator_name as API_Type,l.province_Name as Province,c.comment," + intCount + " as intCount from er_recharge c inner join er_login l on c.userid = l.userid where c.userid='" + intUserID + "' " + strSearch + " order by c.id desc " + strLimit + "";
                        }
                    }
                    ds = objODBC.Report(strQuery);
                }
                return ds;
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                ds.Dispose();
            }
        }


        // ActivityType=81 -- this is for ratailer app and admin web
        [WebMethod]
        public DataSet er_topup_Pending_Report(string strUserName, int intUserID, int PageSize, int intPage, string strSearch, string strIPAddress, int intDesignationType, string strMacAddress, string strOSDetails, string strIMEINo, string strGcmID, string strAPPVersion, int intSource, int intCreatedByType)
        {
            try
            {
                try
                {
                    objCreateFile.CreateTextFile(strIPAddress, strMacAddress, strOSDetails, strIMEINo, strGcmID, strAPPVersion, intSource.ToString(), intCreatedByType, 81, " er_topup_Report", strUserName);
                }
                catch (Exception)
                {


                }
                finally
                {
                    strLimit = " LIMIT " + PageSize * (intPage - 1) + "," + PageSize + "";
                    if (intCreatedByType == 1)
                    {
                        if (intUserID > 0)
                        {


                            int intCount = objODBC.executeScalar_int("Select count(1) from er_recharge c inner join er_login l on c.userid = l.userid where c.userid > 0 and c.status=1 " + strSearch + " order by c.id desc");

                            strQuery = "select l.username as User_ID,l.full_name as Name,l.mobile as Customer_Contact,c.trans_number as Txn_No,c.operator_name as Operator,c.mobile_number as Top_Up_Mobile_No,c.amount as Amount,c.deduct_amt as Deducted_Amount,c.operator_balance as API_Balance,date_format(c.created_on,'%d %b %Y %H:%i:%s') as Date_of_Txn,c.operator_transid as Operator_Txn_Id,case when c.status=1 then 'Pending' when c.status=2 then 'Success' when c.status=3 then 'Failed' when c.dispute_status=1 then 'Disputed' end as Txn_Status,case when c.source = 1 then 'Web' when c.source = 2 then 'Android' when c.source = 3 then 'IOS' when c.source = 4 then 'USSD' when c.source=5 then 'SMS' end as Txn_Mode,c.operator_name as API_Type,l.province_Name as Province,c.comment," + intCount + " as intCount from er_recharge c inner join er_login l on c.userid = l.userid where c.userid > 0 and c.status=1 " + strSearch + " order by c.id desc " + strLimit + "";
                        }
                    }
                    else
                    {
                        if (intUserID > 0)
                        {
                            int intCount = objODBC.executeScalar_int("select count(1) from er_recharge c inner join er_login l on c.userid = l.userid where c.userid='" + intUserID + "' and c.status=1 " + strSearch + " order by c.id");
                            strQuery = "select l.username as User_ID,l.full_name as Name,l.mobile as Customer_Contact,c.trans_number as Txn_No,c.operator_name as Operator,c.mobile_number as Top_Up_Mobile_No,c.amount as Amount,c.deduct_amt as Deducted_Amount,c.operator_balance as API_Balance, date_format(c.created_on,'%d %b %Y %H:%i:%s') as Date_of_Txn,c.operator_transid as Operator_Txn_Id,case when c.status=1 then 'Pending' when c.status=2 then 'Success' when c.status=3 then 'Failed' when c.dispute_status=1 then 'Disputed' end as Txn_Status,case when c.source = 1 then 'Web' when c.source = 2 then 'Android' when c.source = 3 then 'IOS' when c.source = 4 then 'USSD' when c.source=5 then 'SMS' end as Txn_Mode,c.operator_name as API_Type,l.province_Name as Province,c.comment," + intCount + " as intCount from er_recharge c inner join er_login l on c.userid = l.userid where c.userid='" + intUserID + "' and c.status=1 " + strSearch + " order by c.id desc " + strLimit + "";
                        }
                    }
                    ds = objODBC.Report(strQuery);
                }
                return ds;
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                ds.Dispose();
            }
        }

        // ActivityType=82
        [WebMethod]
        public DataSet er_Admin_Transactions_Report(string strUserName, int intUserID, int PageSize, int intPage, string strSearch, string strIPAddress, int intDesignationType, string strMacAddress, string strOSDetails, string strIMEINo, string strGcmID, string strAPPVersion, int intSource, int intCreatedByType)
        {
            try
            {
                try
                {
                    objCreateFile.CreateTextFile(strIPAddress, strMacAddress, strOSDetails, strIMEINo, strGcmID, strAPPVersion, intSource.ToString(), intCreatedByType, 82, "er_Admin_Transactions_Report ", strUserName);
                }
                catch (Exception)
                {


                }
                finally
                {
                    strLimit = " LIMIT " + PageSize * (intPage - 1) + "," + PageSize + "";

                    if (intUserID >= 1)
                    {
                        strQuery = "select l.username as User_ID,l.full_name as Name,t.trans_number as Transaction_ID,t.amount as Amount,t.balance_amount as Closing_Balance,case when t.trans_type=1 then 'Credit' when t.trans_type=2 then 'Debit' end as Transaction_Type,date_format(t.trans_date_time,'%d %b %Y %H:%i:%s') as Date_Time,case when t.trans_for=1 then 'Top Up' when t.trans_for=2 then 'Top Up Failed' when t.trans_for=3 then 'Wallet Recieved' when t.trans_for=4 then 'Wallet Transfer' when t.trans_for=5 then 'Commission Credited' when t.trans_for=1 then 'Top Up' when t.trans_for=1 then 'Top Up' when t.trans_for=6 then 'Wallet Reverted' when t.trans_for=7 then 'Wallet Received after reversal'  when t.trans_for=20 then 'Topup Failed after Rollback ' end as Description,(select count(1) from er_wallet_transaction t inner join er_login l on t.userid = l.userid where t.userid = " + intUserID + "  " + strSearch + ") as intCount from er_wallet_transaction t inner join er_login l on t.userid = l.userid where t.userid = " + intUserID + "  " + strSearch + " order by t.id desc " + strLimit + "";
                    }
                    else if (intUserID == 0)
                    {
                        strQuery = "select l.username as User_ID,l.full_name as Name,t.trans_number as Transaction_ID,t.amount as Amount,t.balance_amount as Closing_Balance,case when t.trans_type=1 then 'Credit' when t.trans_type=2 then 'Debit' end as Transaction_Type,date_format(t.trans_date_time,'%d %b %Y %H:%i:%s') as Date_Time,case when t.trans_for=1 then 'Top Up' when t.trans_for=2 then 'Top Up Failed' when t.trans_for=3 then 'Wallet Recieved' when t.trans_for=4 then 'Wallet Transfer' when t.trans_for=5 then 'Commission Credited' when t.trans_for=1 then 'Top Up' when t.trans_for=1 then 'Top Up' when t.trans_for=6 then 'Wallet Reverted' when t.trans_for=7 then 'Wallet Received after reversal' when t.trans_for=20 then 'Topup Failed after Rollback ' end as Description,(select count(1) from er_wallet_transaction t inner join er_login l on t.userid = l.userid where t.userid >= 1  " + strSearch + ") as intCount from er_wallet_transaction t inner join er_login l on t.userid = l.userid where t.userid >= 1  " + strSearch + " order by t.id desc " + strLimit + "";
                    }
                }
                ds = objODBC.Report(strQuery);

                return ds;
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                ds.Dispose();
            }
        }

        // ActivityType=83
        [WebMethod]
        public DataSet er_Denomination_Wise_Report(string strUserName, int intUserID, int PageSize, int intPage, string strSearch, string strIPAddress, int intDesignationType, string strMacAddress, string strOSDetails, string strIMEINo, string strGcmID, string strAPPVersion, int intSource, int intCreatedByType)
        {
            try
            {
                try
                {
                    objCreateFile.CreateTextFile(strIPAddress, strMacAddress, strOSDetails, strIMEINo, strGcmID, strAPPVersion, intSource.ToString(), intCreatedByType, 83, " er_Denomination_Wise_Report ", strUserName);
                }
                catch (Exception)
                {


                }
                finally
                {
                    strLimit = " LIMIT " + PageSize * (intPage - 1) + "," + PageSize + "";

                    if (intUserID > 0)
                    {
                        strQuery = "select t.amount as Denomination,count(t.amount) as Count from er_recharge t inner join er_login l on t.userid = l.userid where t.userid > 0 " + strSearch + "  GROUP BY t.amount " + strLimit + "";
                    }
                    ds = objODBC.Report(strQuery);
                }
                return ds;
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                ds.Dispose();
            }
        }

        // ActivityType=84 
        [WebMethod]
        public DataSet er_direct_Report(string strUserName, int intUserID, int PageSize, int intPage, string strSearch, string strIPAddress, int intDesignationType, string strMacAddress, string strOSDetails, string strIMEINo, string strGcmID, string strAPPVersion, int intSource, int intCreatedByType)
        {
            try
            {
                try
                {
                    objCreateFile.CreateTextFile(strIPAddress, strMacAddress, strOSDetails, strIMEINo, strGcmID, strAPPVersion, intSource.ToString(), intCreatedByType, 84, " er_direct_Report ", strUserName);
                }
                catch (Exception)
                {


                }
                finally
                {
                    strLimit = " LIMIT " + PageSize * (intPage - 1) + "," + PageSize + "";
                    int intCount = objODBC.executeScalar_int("select count(1) from er_login l inner join er_slab_user s on l.userid = s.userid inner join er_commission_slab t on t.id = s.slab_id where l.parent_id =" + intUserID + " " + strSearch + "");
                    if (intUserID > 0)
                    {
                        strQuery = "select l.userid,l.username as UName,l.full_name as Name,l.mobile,l.emailid,case when l.usertype_id=1 then 'Reseller' when  l.usertype_id=3 then 'Distributor' when l.usertype_id=4 then 'Sub-Distributor' when l.usertype_id=5 then 'Retailer' end as usertype,s.slab_id,case when l.slab_status=0 then 'Not Assign' when  l.slab_status=1 then 'Mega Slab' when l.slab_status=2 then 'Normal Slab' end as Myslab,t.slab_name,l.usertype_id," + intCount + " as count from er_login l inner join er_slab_user s on l.userid = s.userid inner join er_commission_slab t on t.id = s.slab_id where l.parent_id =" + intUserID + " " + strSearch + " order by l.userid asc " + strLimit + "";

                        ds = objODBC.Report(strQuery);

                    }


                }
                return ds;
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                ds.Dispose();
            }
        }

        // ActivityType=85
        [WebMethod]
        public DataSet er_commission_structure_report(string strUserName, int intUserID, int intSlabId, int PageSize, int intPage, string strSearch, string strIPAddress, int intDesignationType, string strMacAddress, string strOSDetails, string strIMEINo, string strGcmID, string strAPPVersion, int intSource, int intCreatedByType)
        {
            try
            {
                try
                {
                    objCreateFile.CreateTextFile(strIPAddress, strMacAddress, strOSDetails, strIMEINo, strGcmID, strAPPVersion, intSource.ToString(), intCreatedByType, 85, " er_commission_structure_report ", strUserName);
                }
                catch (Exception)
                {


                }
                finally
                {
                    strLimit = " LIMIT " + PageSize * (intPage - 1) + "," + PageSize + "";

                    if (intUserID > 0)
                    {

                        strQuery = "select slab_id,case when operator_id=1 then 'Salaam' when operator_id=2 then 'Etisalat' when operator_id=3 then 'Roshan' when operator_id=5 then 'AWCC' when operator_id=4 then 'MTN' end as Operator,commission_per,surcharge_amt,target_amt,target_reward,case when api_type = 1 then 'Salaam' when api_type = 2 then 'Etisalat' when api_type = 3 then 'Roshan' when api_type = 4 then 'Easy Pay' when api_type = 5 then 'AWCC' when api_type = 6 then 'MTN' end as api_type from er_commission_structure where slab_id=" + intSlabId + " " + strSearch + " order by slab_id desc " + strLimit + "";

                    }
                    ds = objODBC.Report(strQuery);
                }
                return ds;
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                ds.Dispose();
            }
        }

        // ActivityType=107
        [WebMethod]
        public DataSet er_fund_receive_report(string strUserName, int intUserID, int PageSize, int intPage, string strSearch, string strIPAddress, int intDesignationType, string strMacAddress, string strOSDetails, string strIMEINo, string strGcmID, string strAPPVersion, int intSource, int intCreatedByType)
        {
            try
            {
                try
                {
                    objCreateFile.CreateTextFile(strIPAddress, strMacAddress, strOSDetails, strIMEINo, strGcmID, strAPPVersion, intSource.ToString(), intCreatedByType, 107, " er_fund_receive_report ", strUserName);
                }
                catch (Exception)
                {


                }
                finally
                {
                    strLimit = " LIMIT " + PageSize * (intPage - 1) + "," + PageSize + "";

                    if (intUserID > 0)
                    {
                        int intCount = objODBC.executeScalar_int("select count(1) from er_wallet_transfer_individual w inner join er_login l on w.parent_id=l.userid where w.userid=" + intUserID + " " + strSearch + "");

                        strQuery = "select l.username as Sender_ID,l.full_name as Sender_Name,w.transfer_amt as Amount,date_format(w.created_on,'%d %b %Y %H:%i:%s') as DateTime," + intCount + " as intCount from er_wallet_transfer_individual w inner join er_login l on w.parent_id=l.userid where w.userid=" + intUserID + " " + strSearch + " order by w.id desc " + strLimit + "";

                    }
                    ds = objODBC.Report(strQuery);
                }
                return ds;
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                ds.Dispose();
            }
        }

       /* [WebMethod]
        public DataSet er_my_commission_report(string strUserName, int intUserID, int PageSize, int intPage, string strSearch, string strIPAddress, int intDesignationType, string strMacAddress, string strOSDetails, string strIMEINo, string strGcmID, string strAPPVersion, int intSource, int intCreatedByType)
        {
            try
            {
                try
                {
                    objCreateFile.CreateTextFile(strIPAddress, strMacAddress, strOSDetails, strIMEINo, strGcmID, strAPPVersion, intSource.ToString(), intCreatedByType, 108, " er_my_commission_report ", strUserName);
                }
                catch (Exception)
                {


                }
                finally
                {
                    strLimit = " LIMIT " + PageSize * (intPage - 1) + "," + PageSize + "";

                 

                    if (intUserID > 0)
                    {
                        int intSlabID = objODBC.executeScalar_int("select slab_id from er_slab_user where userid=" + intUserID + "");
                        //
                        strQuery = "select commission_per,case when operator_id=1 then 'Salaam' when operator_id=2 then 'Etisalat' when operator_id=3 then 'Roshan' when operator_id=5 then 'AWCC' when operator_id=4 then 'MTN' end as Operator,when api_type=1 then 'Salaam' when api_type=2 then 'Etisalat' when api_type=3 then 'Roshan' when api_type=4 then 'Easy Pay' end as Operator where w.userid=" + intUserID + " " + strSearch + " order by id desc " + strLimit + "";

                    }
                    ds = objODBC.Report(strQuery);
                }
                return ds;
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                ds.Dispose();
            }
        }

    */

        [WebMethod]
        public DataSet er_bulktransfer_report(string strUserName, int intUserID, int intUsertype, int PageSize, int intPage, string strSearch, string strIPAddress, int intDesignationType, string strMacAddress, string strOSDetails, string strIMEINo, string strGcmID, string strAPPVersion, int intSource, int intCreatedByType)
        {
            try
            {
                try
                {
                    objCreateFile.CreateTextFile(strIPAddress, strMacAddress, strOSDetails, strIMEINo, strGcmID, strAPPVersion, intSource.ToString(), intCreatedByType, 85, "er_bulktransfer_report ", strUserName);
                }
                catch (Exception)
                {


                }
                finally
                {
                    strLimit = " LIMIT " + PageSize * (intPage - 1) + "," + PageSize + "";

                    if (intUserID > 0 && intUsertype == 1)
                    {
                        int intCount = objODBC.executeScalar_int("select count(1) from er_bulk_transfer_details b inner join er_wallet_transfer_group g on b.group_id = g.id where g.id>0" + strSearch + "");
                        // select l.username,l.mobile,l.full_name,t.transfer_amt,t.created_on from er_bulk_transfer_log t inner join er_login l on t.userid = l.userid where t.transfer_id = ; 0-Pending;1-Completed

                        strQuery = "select b.id,g.group_name as Group_Name,b.transfer_amt as Amount_Transferred,b.total_amt as Total_Amount,b.created_on as Date_of_Transfer,case when b.status=0 then 'Pending' when b.status=1 then 'Complete' end as Status,b.total_member_count as Total_Member," + intCount + " from er_bulk_transfer_details b inner join er_wallet_transfer_group g on b.group_id = g.id where g.id>0 " + strSearch + " order by b.id desc " + strLimit + "";

                    }
                    else if (intUserID > 0 && intUsertype != 1)
                    {
                        int intCount = objODBC.executeScalar_int("select count(1) from er_bulk_transfer_details b inner join er_wallet_transfer_group g on b.group_id = g.id where g.parent_id=" + intUserID + " " + strSearch + "");
                        // select l.username,l.mobile,l.full_name,t.transfer_amt,t.created_on from er_bulk_transfer_log t inner join er_login l on t.userid = l.userid where t.transfer_id = ; 0-Pending;1-Completed

                        strQuery = "select b.id,g.group_name as Group_Name,b.transfer_amt as Amount_Transferred,b.total_amt as Total_Amount,b.created_on as Date_of_Transfer,case when b.status=0 then 'Pending' when b.status=1 then 'Complete' end as Status,b.total_member_count as Total_Member," + intCount + " from er_bulk_transfer_details b inner join er_wallet_transfer_group g on b.group_id = g.id where g.parent_id=" + intUserID + "  " + strSearch + " order by b.id desc " + strLimit + "";

                    }
                    ds = objODBC.Report(strQuery);
                }
                return ds;
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                ds.Dispose();
            }
        }

        [WebMethod]
        public DataSet er_bulktransfer_details_report(string strUserName, int intUserID, int intTransid, int PageSize, int intPage, string strSearch, string strIPAddress, int intDesignationType, string strMacAddress, string strOSDetails, string strIMEINo, string strGcmID, string strAPPVersion, int intSource, int intCreatedByType)
        {
            try
            {
                try
                {
                    objCreateFile.CreateTextFile(strIPAddress, strMacAddress, strOSDetails, strIMEINo, strGcmID, strAPPVersion, intSource.ToString(), intCreatedByType, 85, "er_bulktransfer_details_report ", strUserName);
                }
                catch (Exception)
                {


                }
                finally
                {
                    strLimit = " LIMIT " + PageSize * (intPage - 1) + "," + PageSize + "";

                    if (intUserID > 0)
                    {
                        int intCount = objODBC.executeScalar_int("select count(1) from er_bulk_transfer_log t inner join er_login l on t.userid = l.userid where t.transfer_id=" + intTransid + " " + strSearch + "");

                        strQuery = "select l.username,l.mobile,l.full_name,t.transfer_amt,t.created_on from er_bulk_transfer_log t inner join er_login l on t.userid = l.userid where t.transfer_id=" + intTransid + " " + strSearch + " order by t.created_on desc " + strLimit + "";

                    }
                    ds = objODBC.Report(strQuery);
                }
                return ds;
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                ds.Dispose();
            }
        }

        [WebMethod]
        public DataSet er_Wallet_Requested_Report(string strUserName, int intUserID, int PageSize, int intPage, string strSearch, string strIPAddress, int intDesignationType, string strMacAddress, string strOSDetails, string strIMEINo, string strGcmID, string strAPPVersion, int intSource, int intCreatedByType)
        {
            try
            {
                try
                {
                    objCreateFile.CreateTextFile(strIPAddress, strMacAddress, strOSDetails, strIMEINo, strGcmID, strAPPVersion, intSource.ToString(), intCreatedByType, 76, " Wallet Request Report ", strUserName);
                }
                catch (Exception)
                {


                }
                finally
                {
                    if (intUserID > 0)
                    {
                        strLimit = " LIMIT " + PageSize * (intPage - 1) + "," + PageSize + "";

                        int intCOunt = objODBC.executeScalar_int("select count(1) from er_wallet_purchase w inner join er_login l on w.parent_id = l.userid where w.userid =" + intUserID + " " + strSearch + "");

                        strQuery = "select l.username as User_ID,l.full_name as Full_name,l.emailid as Email_Id,l.mobile as Mobile_No,case when w.payment_mode=1 then 'Cash' when w.payment_mode=2 then 'Bank Transfer' end as Payment_Mode,w.request_amt as Amount,w.rcpt_url,case when w.status=0 then 'Pending' when w.status=1 then 'Confirmed' when w.status=2 then 'Rejected' end as Status,w.comment," + intCOunt + " from er_wallet_purchase w inner join er_login l on w.userid = l.userid where w.userid =" + intUserID + " " + strSearch + " order by w.id desc " + strLimit + "";

                        //select l.username as User_ID,l.full_name as Full_Name,l.emailid as Email_Id,l.mobile as Mobile_No,case when w.payment_mode=1 then 'Cash' when w.payment_mode=2 then 'Bank Transfer' end as Payment_Mode,w.request_amt as Amount,w.rcpt_url,case when w.status=0 then 'Pending' when w.status=1 then 'Approved' when w.status=2 then 'Rejected' end as Status,w.comment as Comment,date_format(w.created_on,'%d %b %Y %H:%i:%s') as Requested_on,date_format(w.confirmed_on,'%d %b %Y %H:%i:%s') as Confirmed_on ,(select count(1) from er_wallet_purchase w inner join er_login l on w.userid = l.userid where w.parent_id =" + intUserID + " " + strSearch + ") as intCount from er_wallet_purchase w inner join er_login l on w.userid = l.userid where w.parent_id =" + intUserID + " " + strSearch + " order by w.id desc " + strLimit + "";
                        ds = objODBC.Report(strQuery);

                    }
                }
                return ds;
            }
            catch (Exception)
            {

                throw;
            }
            finally
            {
                ds.Dispose();
            }
        }

        [WebMethod]
        public DataSet er_topup_Report_for_distrubuter(string strUserName, int intUserID, int PageSize, int intPage, string strSearch, string strIPAddress, int intDesignationType, string strMacAddress, string strOSDetails, string strIMEINo, string strGcmID, string strAPPVersion, int intSource, int intCreatedByType)
        {
            try
            {
                try
                {
                    objCreateFile.CreateTextFile(strIPAddress, strMacAddress, strOSDetails, strIMEINo, strGcmID, strAPPVersion, intSource.ToString(), intCreatedByType, 81, "er_topup_Report_for_distrubuter", strUserName);
                }
                catch (Exception)
                {


                }
                finally
                {
                    if (intSource == 2) // For Android
                    {
                        int intCOunt = objODBC.executeScalar_int("select count(1) from er_recharge c inner join er_login l on c.userid = l.userid where c.userid > 0 " + strSearch + "");

                        int intType = objODBC.executeScalar_int("select usertype_id from er_login where userid= " + intUserID + "");

                        strLimit = " LIMIT " + PageSize * (intPage - 1) + "," + PageSize + "";

                        if (intType == 3)
                        {
                            if (intUserID > 0)
                            {
                                string strDownline = objODBC.executeScalar_str("SELECT child_id FROM er_login WHERE userid='" + intUserID + "'");
                                strQuery = "select l.username as User_ID,l.full_name as Name,l.mobile as Customer_Contact,c.trans_number as Txn_No,c.operator_name as Operator,c.mobile_number as Top_Up_Mobile_No,c.amount as Amount,c.deduct_amt as Deducted_Amount,c.operator_balance,date_format(c.created_on,'%d %b %Y %H:%i:%s') as Date_of_Txn,c.operator_transid as Operator_Txn_Id,case when c.status=1 then 'Pending' when c.status=2 then 'Success' when c.status=3 then 'Failed' when c.dispute_status=1 then 'Disputed' end as Txn_Status,case when c.source = 1 then 'Web' when c.source = 2 then 'Android' when c.source = 3 then 'IOS' end as Txn_Mode,c.operator_name as API_Type,l.province_Name as Province,c.comment," + intCOunt + " as intCount from er_recharge c inner join er_login l on c.userid = l.userid where (l.userid in (" + strDownline + ") or l.userid='" + intUserID + "') " + strSearch + " order by c.id desc " + strLimit + "";
                            }
                        }
                        else if (intType == 4)
                        {
                            if (intUserID > 0)
                            {
                                string strDownline = objODBC.executeScalar_str("SELECT child_id FROM er_login WHERE userid='" + intUserID + "'");
                                strQuery = "select l.username as User_ID,l.full_name as Name,l.mobile as Customer_Contact,c.trans_number as Txn_No,c.operator_name as Operator,c.mobile_number as Top_Up_Mobile_No,c.amount as Amount,c.deduct_amt as Deducted_Amount,c.operator_balance,date_format(c.created_on,'%d %b %Y %H:%i:%s')as Date_of_Txn,c.operator_transid as Operator_Txn_Id,case when c.status=1 then 'Pending' when c.status=2 then 'Success' when c.status=3 then 'Failed' when c.dispute_status=1 then 'Disputed' end as strstatus,case when c.source = 1 then 'Web' when c.source = 2 then 'Android' when c.source = 3 then 'IOS' end as Txn_Mode,c.operator_name as API_Type,l.province_Name as Province,c.comment," + intCOunt + " as intCount from er_recharge c inner join er_login l on c.userid = l.userid where (l.userid in (" + strDownline + ") or l.userid='" + intUserID + "') " + strSearch + " order by c.id desc " + strLimit + "";
                            }
                        }
                        ds = objODBC.Report(strQuery);
                    }
                    else
                    {
                        int intCOunt = objODBC.executeScalar_int("select count(1) from er_recharge c inner join er_login l on c.userid = l.userid where c.userid > 0 " + strSearch + "");

                        int intType = objODBC.executeScalar_int("select usertype_id from er_login where userid= " + intUserID + "");

                        strLimit = " LIMIT " + PageSize * (intPage - 1) + "," + PageSize + "";

                        if (intType == 3)
                        {
                            if (intUserID > 0)
                            {
                                string strDownline = objODBC.executeScalar_str("SELECT child_id FROM er_login WHERE userid='" + intUserID + "'");
                                strQuery = "select l.username as User_ID,l.full_name as Name,l.mobile as Customer_Contact,c.trans_number as Txn_No,c.operator_name as Operator,c.mobile_number as Top_Up_Mobile_No,c.amount as Amount,c.deduct_amt as Deducted_Amount,c.operator_balance,date_format(c.created_on,'%d %b %Y %H:%i:%s') as Date_of_Txn,c.operator_transid as Operator_Txn_Id,case when c.status=1 then 'Pending' when c.status=2 then 'Success' when c.status=3 then 'Failed' when c.dispute_status=1 then 'Disputed' end as Txn_Status,case when c.source = 1 then 'Web' when c.source = 2 then 'Android' when c.source = 3 then 'IOS' end as Txn_Mode,c.operator_name as API_Type,l.province_Name as Province,case when c.rollback_status = 0 then 'Not Rollback' when c.rollback_status = 1 then 'Requested' when c.rollback_status = 2 then 'Processing' when c.rollback_status = 3 then 'Rejected' when c.rollback_status = 4 then 'Completed' end as Roll_Status,c.rollback_amount,c.comment," + intCOunt + " as intCount from er_recharge c inner join er_login l on c.userid = l.userid where (l.userid in (" + strDownline + ") or l.userid='" + intUserID + "' ) " + strSearch + " order by c.id desc " + strLimit + "";


                                

                            }
                        }
                        else if (intType == 4)
                        {
                            if (intUserID > 0)
                            {
                                string strDownline = objODBC.executeScalar_str("SELECT child_id FROM er_login WHERE userid='" + intUserID + "'");
                                strQuery = "select l.username as User_ID,l.full_name as Name,l.mobile as Customer_Contact,c.trans_number as Txn_No,c.operator_name as Operator,c.mobile_number as Top_Up_Mobile_No,c.amount as Amount,c.deduct_amt as Deducted_Amount,c.operator_balance,date_format(c.created_on,'%d %b %Y %H:%i:%s')as Date_of_Txn,c.operator_transid as Operator_Txn_Id,case when c.status=1 then 'Pending' when c.status=2 then 'Success' when c.status=3 then 'Failed' when c.dispute_status=1 then 'Disputed' end as strstatus,case when c.source = 1 then 'Web' when c.source = 2 then 'Android' when c.source = 3 then 'IOS' end as Txn_Mode,c.operator_name as API_Type,l.province_Name as Province,case when c.rollback_status = 0 then 'Not Rollback' when c.rollback_status = 1 then 'Requested' when c.rollback_status = 2 then 'Processing' when c.rollback_status = 3 then 'Rejected' when c.rollback_status = 4 then 'Completed' end as Roll_Statuss,c.rollback_amount,c.comment," + intCOunt + " as intCount from er_recharge c inner join er_login l on c.userid = l.userid where (l.userid in (" + strDownline + ") or l.userid='" + intUserID + "' ) " + strSearch + " order by c.id desc " + strLimit + "";
                            }
                        }
                        ds = objODBC.Report(strQuery);
                    }
                }
                return ds;
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                ds.Dispose();
            }
        }

        [WebMethod]
        public DataSet er_Dispute_Report(string strUserName, int intTranNO, int PageSize, int intPage, string strSearch, string strIPAddress, int intDesignationType, string strMacAddress, string strOSDetails, string strIMEINo, string strGcmID, string strAPPVersion, int intSource, int intCreatedByType)
        {
            try
            {
                try
                {
                    objCreateFile.CreateTextFile(strIPAddress, strMacAddress, strOSDetails, strIMEINo, strGcmID, strAPPVersion, intSource.ToString(), intCreatedByType, 81, "er_Dispute_Report", strUserName);
                }
                catch (Exception)
                {


                }
                finally
                {
                    int intCOunt = objODBC.executeScalar_int("SELECT count(1) From er_recharge Where trans_number= " + intTranNO + "" + strSearch + "");

                    strLimit = " LIMIT " + PageSize * (intPage - 1) + "," + PageSize + "";


                    strQuery = "SELECT case when a.status=1 then 'Pending' when a.status=2 then 'Success' when a.status=3 then 'Failed' end as strstatus,case when a.source = 1 then 'Web' when a.source = 2 then 'Android' when a.source = 3 then 'IOS' end as Mode_of_Transaction,l.province_Name,a.operator_transid From er_recharge a inner join er_login l on a.userid = l.userid Where a.trans_number=" + intTranNO + " " + strSearch + " order by a.id desc " + strLimit + "";


                    ds = objODBC.Report(strQuery);
                }
                return ds;
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                ds.Dispose();
            }
        }

        [WebMethod]
        public DataSet er_raised_ticket_Report(string strUserName, int intTranNO, int PageSize, int intPage, string strSearch, string strIPAddress, int intDesignationType, string strMacAddress, string strOSDetails, string strIMEINo, string strGcmID, string strAPPVersion, int intSource, int intCreatedByType)
        {
            try
            {
                try
                {
                    objCreateFile.CreateTextFile(strIPAddress, strMacAddress, strOSDetails, strIMEINo, strGcmID, strAPPVersion, intSource.ToString(), intCreatedByType, 81, "er_raised_ticket_Report", strUserName);
                }
                catch (Exception)
                {


                }
                finally
                {
                    //int intCOunt = objODBC.executeScalar_int("SELECT count(1) From er_recharge Where trans_number= " + intTranNO + "" + strSearch + "");

                    strLimit = " LIMIT " + PageSize * (intPage - 1) + "," + PageSize + "";


                    strQuery = "SELECT a.trans_number,DATE_FORMAT(a.created_on,'%d %b %Y %H:%i:%s') as created_on,a.amount From  er_recharge a  Where a.trans_number=" + intTranNO + " " + strSearch + " order by a.id desc " + strLimit + "";


                    ds = objODBC.Report(strQuery);
                }
                return ds;
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                ds.Dispose();
            }
        }



        [WebMethod]
        public DataSet er_print_ticket_Report(string strUserName, int intTranNO, int PageSize, int intPage, string strSearch, string strIPAddress, int intDesignationType, string strMacAddress, string strOSDetails, string strIMEINo, string strGcmID, string strAPPVersion, int intSource, int intCreatedByType)
        {
            try
            {
                try
                {
                    objCreateFile.CreateTextFile(strIPAddress, strMacAddress, strOSDetails, strIMEINo, strGcmID, strAPPVersion, intSource.ToString(), intCreatedByType, 81, "er_raised_ticket_Report", strUserName);
                }
                catch (Exception)
                {


                }
                finally
                {
                    //int intCOunt = objODBC.executeScalar_int("SELECT count(1) From er_recharge Where trans_number= " + intTranNO + "" + strSearch + "");

                    strLimit = " LIMIT " + PageSize * (intPage - 1) + "," + PageSize + "";

                    strQuery = "SELECT a.trans_number,a.operator_transid,DATE_FORMAT(a.created_on,'%d %b %Y %H:%i:%s') as created_on,a.amount,a.id,a.mobile_number,a.operator_name,case when a.source = 1 then 'Web' when a.source = 2 then 'Android' when a.source = 3 then 'IOS' end as Mode_of_Transaction From  er_recharge a  Where a.trans_number=" + intTranNO + " " + strSearch + " order by a.id desc " + strLimit + "";

                    ds = objODBC.Report(strQuery);
                }
                return ds;
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                ds.Dispose();
            }
        }

        [WebMethod]
        public string er_sub_dispute(string strUserName, int inRechrid, string strmsg, int intUserID, int PageSize, int intPage, string strSearch, string strIPAddress, int intDesignationType, string strMacAddress, string strOSDetails, string strIMEINo, string strGcmID, string strAPPVersion, int intSource, int intCreatedByType)
        {
            string strResult = string.Empty;
            try
            {
                try
                {
                    objCreateFile.CreateTextFile(strIPAddress, strMacAddress, strOSDetails, strIMEINo, strGcmID, strAPPVersion, intSource.ToString(), intCreatedByType, 81, "er_sub_dispute", strUserName);
                }
                catch (Exception)
                {

                }
                finally
                {
                    int count = objODBC.executeScalar_int("Select count(1) FROM ticket_details WHERE recharge_id='" + inRechrid + "'");

                    if (count == 0)
                    {
                        ds = objODBC.getDataSet("SELECT a.id,e.userid,a.mobile_number,a.amount,a.operator_name,a.source,e.username From er_login e LEFT JOIN er_recharge a On a.userid = e.userid where e.Active=1 and a.trans_number=" + inRechrid + " ");

                        objODBC.executeNonQuery("INSERT INTO `ticket_details`(reseller_id,ticket_id, userid, recharge_id, created_on, message, username, trans_no, `mobile`, `rechrg_amount`, `operator_name`, `source`) VALUES ('" + intUserID + "','" + ds.Tables[0].Rows[0][0].ToString() + "','" + ds.Tables[0].Rows[0][1].ToString() + "','" + inRechrid + "','" + objDateFor.getCurDateTimeString() + "','" + strmsg + "','" + ds.Tables[0].Rows[0][6].ToString() + "','" + inRechrid + "','" + ds.Tables[0].Rows[0][2].ToString() + "','" + ds.Tables[0].Rows[0][3].ToString() + "','" + ds.Tables[0].Rows[0][4].ToString() + "','" + ds.Tables[0].Rows[0][5].ToString() + "')");

                        support objsuport = new support();
                        objsuport.ico_Raised_Ticket(intUserID, 3, "high", "Recharge dispute", strmsg, "0");

                        string tcktID = objODBC.executeScalar_str("select ticket_no from ico_tckt_support order by id desc limit 1");
                        objODBC.executeNonQuery("update ico_tckt_support set ticket_no='" + inRechrid + "' where ticket_no='" + tcktID + "'");

                        objODBC.executeNonQuery("update ico_support_chat set ticket_no='" + inRechrid + "' where ticket_no='" + tcktID + "'");
                        strResult = "Ticket submitted successfully.";
                    }
                    else
                    {
                        strResult = "You have already submitted the ticket for this recharge.";
                    }
                }
                return strResult;
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                ds.Dispose();
            }
        }

        [WebMethod]
        public DataSet er_group_topup_report(string strUserName, int intUserID, int intUserType, int PageSize, int intPage, string strSearch, string strIPAddress, int intDesignationType, string strMacAddress, string strOSDetails, string strIMEINo, string strGcmID, string strAPPVersion, int intSource, int intCreatedByType)
        {
            try
            {
                try
                {
                    objCreateFile.CreateTextFile(strIPAddress, strMacAddress, strOSDetails, strIMEINo, strGcmID, strAPPVersion, intSource.ToString(), intCreatedByType, 81, "er_group_topup_report", strUserName);
                }
                catch (Exception)
                {


                }
                finally
                {
                    if (intUserID > 0 && intUserType == 1)
                    {
                        int intCOunt = objODBC.executeScalar_int("SELECT count(1) from er_group_topup_inprocess g inner join er_top_up_group e on g.groupid = e.id inner join er_login l on g.parentid = l.userid Where g.id > 0 " + strSearch + "");

                        strLimit = " LIMIT " + PageSize * (intPage - 1) + "," + PageSize + "";

                        strQuery = "select g.tran_id,l.username as UName,l.full_name,e.group_name,case when e.operator_id=1 then 'Salaam' when e.operator_id=2 then 'Etisalat' when e.operator_id=3 then 'Roshan' when e.operator_id=5 then 'AWCC' when e.operator_id=4 then 'MTN' end as Operator,g.amount,DATE_FORMAT(g.created_on,'%d %b %Y %H:%i:%s') as c_date,case when g.status=0 then 'Pending' when g.status=1 then 'Inprocess' when g.status=2 then 'Completed' end as status," + intCOunt + " as count from er_group_topup_inprocess g inner join er_top_up_group e on g.groupid = e.id inner join er_login l on g.parentid = l.userid where g.id > 0 " + strSearch + " order by g.id desc " + strLimit + "";
                    }
                    else if (intUserID > 0 && intUserType != 1)
                    {
                        int intCOunt = objODBC.executeScalar_int("SELECT count(1) from er_group_topup_inprocess g inner join er_top_up_group e on g.groupid = e.id inner join er_login l on g.parentid = l.userid Where g.parentid = " + intUserID + " " + strSearch + "");

                        strLimit = " LIMIT " + PageSize * (intPage - 1) + "," + PageSize + "";

                        strQuery = "select g.tran_id,e.group_name,case when e.operator_id=1 then 'Salaam' when e.operator_id=2 then 'Etisalat' when e.operator_id=3 then 'Roshan' when e.operator_id=5 then 'AWCC' when e.operator_id=4 then 'MTN' end as Operator,g.amount,DATE_FORMAT(g.created_on,'%d %b %Y %H:%i:%s') as c_date,case when g.status=0 then 'Pending' when g.status=1 then 'Inprocess' when g.status=2 then 'Completed' end as status," + intCOunt + " as count from er_group_topup_inprocess g inner join er_top_up_group e on g.groupid = e.id where g.parentid = " + intUserID + "  " + strSearch + " order by g.id desc " + strLimit + "";
                    }
                    ds = objODBC.Report(strQuery);
                }
                return ds;
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                ds.Dispose();
            }
        }



        [WebMethod]
        public DataSet er_group_topup_report_details(string strUserName, int intUserID, int intUserType, string intID, int PageSize, int intPage, string strSearch, string strIPAddress, int intDesignationType, string strMacAddress, string strOSDetails, string strIMEINo, string strGcmID, string strAPPVersion, int intSource, int intCreatedByType)
        {
            try
            {
                try
                {
                    objCreateFile.CreateTextFile(strIPAddress, strMacAddress, strOSDetails, strIMEINo, strGcmID, strAPPVersion, intSource.ToString(), intCreatedByType, 81, "er_group_topup_report_details", strUserName);
                }
                catch (Exception)
                {


                }
                finally
                {
                    if (intUserID > 0 && intUserType == 1)
                    {
                        int intCOunt = objODBC.executeScalar_int("SELECT count(1) from er_recharge c Where c.group_topup_id = '" + intID + "' " + strSearch + "");

                        strLimit = " LIMIT " + PageSize * (intPage - 1) + "," + PageSize + "";

                        strQuery = "select l.username as User_ID,l.full_name as Name,l.mobile as Customer_Contact,c.trans_number as Txn_No,c.operator_name as Operator,c.mobile_number as Top_Up_Mobile_No,c.amount as Amount,c.deduct_amt as Deducted_Amount,c.operator_balance as API_Balance, date_format(c.created_on,'%d %b %Y %H:%i:%s') as Date_of_Txn,c.operator_transid as Operator_Txn_Id,case when c.status=1 then 'Pending' when c.status=2 then 'Success' when c.status=3 then 'Failed' when c.dispute_status=1 then 'Disputed' end as Txn_Status,case when c.source = 1 then 'Web' when c.source = 2 then 'Android' when c.source = 3 then 'IOS' when c.source = 4 then 'USSD' when c.source=5 then 'SMS' end as Txn_Mode,c.operator_name as API_Type,l.province_Name as Province,c.comment,(select count(1) from er_recharge c inner join er_login l on c.userid = l.userid where c.group_topup_id ='" + intID + "'" + strSearch + ") as intCount from er_recharge c inner join er_login l on c.userid = l.userid where c.group_topup_id='" + intID + "' " + strSearch + " order by c.id desc " + strLimit + "";
                    }
                    ds = objODBC.Report(strQuery);
                }
                return ds;
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                ds.Dispose();
            }
        }

        [WebMethod]
        public DataSet er_rewards_report(string strUserName, int intUserID, int intUserType, int intID, int PageSize, int intPage, string strSearch, string strIPAddress, int intDesignationType, string strMacAddress, string strOSDetails, string strIMEINo, string strGcmID, string strAPPVersion, int intSource, int intCreatedByType)
        {
            try
            {
                try
                {
                    objCreateFile.CreateTextFile(strIPAddress, strMacAddress, strOSDetails, strIMEINo, strGcmID, strAPPVersion, intSource.ToString(), intCreatedByType, 81, "er_rewards_report", strUserName);
                }
                catch (Exception)
                {


                }
                finally
                {
                    if (intUserID > 0)
                    {
                        strLimit = " LIMIT " + PageSize * (intPage - 1) + "," + PageSize + "";

                        strQuery = "select r.id,r.userid,l.username as User_ID,l.full_name as Full_Name,l.mobile as Contact_No,l.emailid as Email_Id, r.target_amt as Target,r.reward_amt as Reward,r.reward_amt as Reward,case when date_format(r.updated_on,'%d %b %Y %H:%i:%s') ='01 Jan 1900 01:00:00' then '--' else date_format(r.updated_on,'%d %b %Y %H:%i:%s') end as Datetim,case when r.status=0 then 'Pending' when r.status=1 then 'Approved' when r.status=2 then 'Reject' end as Status,r.month,r.year,case when r.operator_id=1 then 'Salaam' when r.operator_id=2 then 'Etisalat' when r.operator_id=3 then 'Roshan' when r.operator_id=5 then 'AWCC' when r.operator_id=4 then 'MTN' end as Operator,(select count(1) from er_commission_reward_details r inner join er_login l on r.userid = l.userid where  r.id > 0  " + strSearch + ") as intCount from er_commission_reward_details r inner join er_login l on r.userid = l.userid  where r.id > 0 " + strSearch + " order by r.id desc " + strLimit + "";

                    }
                    ds = objODBC.Report(strQuery);
                }
                return ds;
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                ds.Dispose();
            }
        }

        [WebMethod]
        public ArrayList er_update_reward_status(int intUserid, string strUserName, string strOperatorName, string strMonthYear, int intID, int intStatus, string strIPAddress, int intDesignationType, string strMacAddress, string strOSDetails, string strIMEINo, string strGcmID, string strAPPVersion, int intSource, int intCreatedByType)
        {
            string strResult = string.Empty;
            ArrayList arrlst = new ArrayList();
            try
            {
                objCreateFile.CreateTextFile(strIPAddress, strMacAddress, strOSDetails, strIMEINo, strGcmID, strAPPVersion, intSource.ToString(), intCreatedByType, 25, "Reward for achieving the target for '" + strOperatorName + "' in '" + strMonthYear + "'", strUserName);

                objODBCLOG.executeNonQuery("call activity_logs('1','" + intDesignationType + "','" + intCreatedByType + "','" + strIPAddress + "','" + strMacAddress + "','" + strOSDetails + "','" + strIMEINo + "','" + strGcmID + "','" + strAPPVersion + "','" + intSource + "',' Reward for achieving the target for '" + strOperatorName + "' in '" + strMonthYear + "'',25,'','')");
            }
            catch (Exception)
            {
            }
            finally
            {
                try
                {

                    string strDate = objDT.getCurDateTimeString();

                    strQuery = "select count(1) from er_commission_reward_details where id=" + intID + " and status=0";

                    int intCount = objODBC.executeScalar_int(strQuery);

                    if (intCount > 0)
                    {
                        dt = objODBC.getDataTable("select userid,reward_amt from er_commission_reward_details where id=" + intID + " and status = 0");

                        if (dt.Rows.Count > 0)
                        {
                            if (intStatus == 1) // approved
                            {
                                strQuery = "update er_commission_reward_details set status=" + intStatus + ",updated_on='" + strDate + "' where id =" + intID + "";
                                objODBC.executeNonQuery(strQuery);

                                int intUserID = Convert.ToInt32(dt.Rows[0][0]);
                                double dblAMt = Convert.ToDouble(dt.Rows[0][1]);
                                purchase_wallet1 obj_pw = new purchase_wallet1();

                                double dblUserBalance = obj_pw.GetWalletAmount(1);

                                if (dblUserBalance >= dblAMt)
                                {
                                    objODBC.executeNonQuery("Call ap_walletTransfer(1," + intUserID + "," + dblAMt + ")");

                                    arrlst.Add("TRUE");
                                    arrlst.Add("Reward amount has been transfered successfully");
                                }
                                else
                                {
                                    arrlst.Add("FALSE");
                                    arrlst.Add("you dont have a sufficient balance");

                                }
                            }
                            else if (intStatus == 2) // rejected
                            {
                                strQuery = "update er_commission_reward_details set status=2,updated_on='" + strDate + "' where id =" + intID + "";
                                objODBC.executeNonQuery(strQuery);

                                arrlst.Add("TRUE");
                                arrlst.Add("Reward is rejected successfully");
                            }

                        }
                        else
                        {
                            arrlst.Add("FALSE");
                            arrlst.Add("somthing went wrong");
                        }

                    }
                    else
                    {

                        arrlst.Add("FALSE");
                        arrlst.Add("status already changed");
                    }


                }
                catch (Exception)
                {
                    arrlst.Add("FALSE");
                    arrlst.Add("somthing went wrong");
                }
            }

            return arrlst;
        }

        [WebMethod]
        public DataSet er_admin_submit_and_refund_report(string strUserName, int intUserID, int intUserType, int PageSize, int intPage, string strSearch, string strIPAddress, int intDesignationType, string strMacAddress, string strOSDetails, string strIMEINo, string strGcmID, string strAPPVersion, int intSource, int intCreatedByType)
        {
            try
            {
                try
                {
                    objCreateFile.CreateTextFile(strIPAddress, strMacAddress, strOSDetails, strIMEINo, strGcmID, strAPPVersion, intSource.ToString(), intCreatedByType, 81, "er_admin_submit_and_refund_report", strUserName);
                }
                catch (Exception)
                {


                }
                finally
                {
                    if (intUserID > 0)
                    {
                        strLimit = " LIMIT " + PageSize * (intPage - 1) + "," + PageSize + "";

                        strQuery = "select r.id,l.username,l.full_name,r.mobile_number,r.amount,r.deduct_amt,r.operator_transid,case when r.operator_id=1 then 'Salaam' when r.operator_id=2 then 'Etisalat' when r.operator_id=3 then 'Roshan' when r.operator_id=5 then 'AWCC' when r.operator_id=4 then 'MTN' end as Operator,date_format(r.created_on,'%d %b %Y %H:%i:%s') as dt,(select count(1) from er_recharge r inner join er_login l on r.userid = l.userid where r.id >0 and r.status=1 " + strSearch + ") as intCount from er_recharge r inner join er_login l on r.userid = l.userid  where r.id> 0 and r.status=1" + strSearch + " order by r.id asc " + strLimit + "";

                    }
                    ds = objODBC.Report(strQuery);
                }
                return ds;
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                ds.Dispose();
            }
        }


        [WebMethod]
        public DataSet er_admin_NoActivity_Log_report(string strUserName, int intUserID, int intUserType, int PageSize, int intPage, string strSearch, string strIPAddress, int intDesignationType, string strMacAddress, string strOSDetails, string strIMEINo, string strGcmID, string strAPPVersion, int intSource, int intCreatedByType)
        {
            try
            {
                try
                {
                    objCreateFile.CreateTextFile(strIPAddress, strMacAddress, strOSDetails, strIMEINo, strGcmID, strAPPVersion, intSource.ToString(), intCreatedByType, 81, "er_admin_submit_and_refund_report", strUserName);
                }
                catch (Exception)
                {


                }
                finally
                {
                    if (intUserID > 0)
                    {
                        strLimit = " LIMIT " + PageSize * (intPage - 1) + "," + PageSize + "";

                        int intCount = objODBCLOG.executeScalar_int("select count(1) from er_no_activity_perform r inner join er_login_log l on r.userid = l.userid  where r.usertype=2 " + strSearch + " order by r.id Desc ");

                        strQuery = "select r.id,l.username as User_ID,l.full_name as Name,r.total_Days as Total_Days,date_format(r.created_on,'%d %b %Y %H:%i:%s') as CurrentDate,date_format(r.last_activity_date,'%d %b %Y %H:%i:%s') as Last_Log_Date," + intCount + " as intCount from er_no_activity_perform r inner join er_login_log l on r.userid = l.userid  where r.usertype=2 " + strSearch + " order by r.id Desc " + strLimit + "";

                    }
                    ds = objODBCLOG.Report(strQuery);
                }
                return ds;
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                ds.Dispose();
            }
        }



        [WebMethod]
        public DataSet er_Reverse_TopUp_Report(string strUserName, int intUserID, int PageSize, int intPage, string strSearch, string strIPAddress, int intDesignationType, string strMacAddress, string strOSDetails, string strIMEINo, string strGcmID, string strAPPVersion, int intSource, int intCreatedByType)
        {
            try
            {
                try
                {
                    objCreateFile.CreateTextFile(strIPAddress, strMacAddress, strOSDetails, strIMEINo, strGcmID, strAPPVersion, intSource.ToString(), intCreatedByType, 79, " er_Reverse_TopUp_Report ", strUserName);
                }
                catch (Exception)
                {


                }
                finally
                {
                    if (intUserID > 1)
                    {
                        strLimit = " LIMIT " + PageSize * (intPage - 1) + "," + PageSize + "";

                        int intCount = objODBC.executeScalar_int("select count(1) from etisalat_reverse_topup c inner join er_login l on c.userid = l.userid inner join er_recharge r on c.recharge_id = r.id where c.userid = " + intUserID + "  " + strSearch + " order by c.id desc " + strLimit + "");

                        strQuery = "select l.username as User_ID,l.full_name as Name,case when c.status=0 then 'Pending' when  c.status=1 then 'Failed' when  c.status=2 then 'Success' end as status,c.mobile as Mobile_No,c.amount as Amount,date_format(c.created_on,'%d %b %Y %H:%i:%s') as Date_Time," + intCount + " as intCount from etisalat_reverse_topup c inner join er_login l on c.userid = l.userid inner join er_recharge r on c.recharge_id = r.id where c.userid = " + intUserID + "  " + strSearch + " order by c.id desc " + strLimit + "";
                    }
                    else
                    {
                        strLimit = " LIMIT " + PageSize * (intPage - 1) + "," + PageSize + "";

                        int intCount = objODBC.executeScalar_int("select count(1) from etisalat_reverse_topup c inner join er_login l on c.userid = l.userid where c.userid >0  " + strSearch + " order by c.id desc " + strLimit + "");

                        strQuery = "select l.username as User_ID,l.full_name as Name,case when c.status=0 then 'Pending' when  c.status=1 then 'Failed' when  c.status=2 then 'Success' end as status,c.mobile as Mobile_No,c.amount as Amount,date_format(c.created_on,'%d %b %Y %H:%i:%s') as Date_Time," + intCount + " as intCount from etisalat_reverse_topup c inner join er_login l on c.userid = l.userid where c.userid >0  " + strSearch + " order by c.id desc " + strLimit + "";
                    }

                }
                ds = objODBC.Report(strQuery);

                return ds;
            }
            catch (Exception)
            {

                throw;
            }
            finally
            {
                ds.Dispose();
            }
        }


        // ActivityType=81 -- this is for ratailer app and admin web
        [WebMethod]
        public DataSet er_RollBack_Etisalat_Success_topup_Report(string strUserName, int intUserID, int PageSize, int intPage, string strSearch, string strIPAddress, int intDesignationType, string strMacAddress, string strOSDetails, string strIMEINo, string strGcmID, string strAPPVersion, int intSource, int intCreatedByType)
        {
            try
            {
                try
                {
                    objCreateFile.CreateTextFile(strIPAddress, strMacAddress, strOSDetails, strIMEINo, strGcmID, strAPPVersion, intSource.ToString(), intCreatedByType, 81, " er_topup_Report", strUserName);
                }
                catch (Exception)
                {


                }
                finally
                {
                    strLimit = " LIMIT " + PageSize * (intPage - 1) + "," + PageSize + "";

                    if (intUserID > 0)
                    {
                        int intCount = objODBC.executeScalar_int("select count(1) from er_recharge c inner join er_login l on c.userid = l.userid where c.userid > 0 and c.status=2 and c.rollback_status=0  " + strSearch + "");
                        strQuery = "select l.userid as UserID,l.username as UserName,l.full_name as Name,l.mobile as Customer_Contact,c.trans_number as Txn_No,c.mobile_number as Top_Up_Mobile_No,c.amount as Amount,c.deduct_amt as Deducted_Amount,c.operator_balance as API_Balance,date_format(c.created_on,'%d %b %Y %H:%i:%s') as Date_of_Txn,c.operator_transid as Operator_Txn_Id,case when c.source = 1 then 'Web' when c.source = 2 then 'Android' when c.source = 3 then 'IOS' when c.source = 4 then 'USSD' when c.source=5 then 'SMS' end as Txn_Mode,l.province_Name as Province,c.comment,c.operator_name as Operator,c.operator_id as OperatorID,'" + intCount + "'as intCount from er_recharge c inner join er_login l on c.userid = l.userid where c.userid > 0 and c.status=2 and c.rollback_status=0 " + strSearch + " order by c.id desc " + strLimit + "";
                    }

                    ds = objODBC.Report(strQuery);
                }
                return ds;
            }
            catch (Exception)
            {
                return ds;
            }
            finally
            {
                ds.Dispose();
            }
        }
        // ActivityType=81 -- this is for ratailer app and admin web
        [WebMethod]
        public DataSet er_RollBack_Etisalat_Inproess_topup_Report(string strUserName, int intUserID, int PageSize, int intPage, string strSearch, string strIPAddress, int intDesignationType, string strMacAddress, string strOSDetails, string strIMEINo, string strGcmID, string strAPPVersion, int intSource, int intCreatedByType)
        {
            try
            {
                try
                {
                    objCreateFile.CreateTextFile(strIPAddress, strMacAddress, strOSDetails, strIMEINo, strGcmID, strAPPVersion, intSource.ToString(), intCreatedByType, 81, " er_topup_Report", strUserName);
                }
                catch (Exception)
                {


                }
                finally
                {
                    strLimit = " LIMIT " + PageSize * (intPage - 1) + "," + PageSize + "";

                    if (intUserID > 0)
                    {
                        int intCount = objODBC.executeScalar_int("select count(1) from er_recharge c inner join er_login l on c.userid = l.userid where c.userid > 0 and c.status=2  and c.rollback_status=1 and c.operator_id!=2 " + strSearch + "");
                        strQuery = "select l.userid as UserID,l.username as UserName,l.full_name as Name,l.mobile as Customer_Contact,c.trans_number as Txn_No,c.mobile_number as Top_Up_Mobile_No,c.amount as Amount,c.deduct_amt as Deducted_Amount,c.operator_balance as API_Balance,date_format(c.created_on,'%d %b %Y %H:%i:%s') as Date_of_Txn,c.operator_transid as Operator_Txn_Id,case when c.rollback_status = 1 then 'Pending' when c.rollback_status = 2 then 'In Process' when c.rollback_status = 3 then 'Reject' when c.rollback_status = 4 then 'Complete' end as Txn_Mode,l.province_Name as Province,c.comment,c.operator_name as Operator,c.operator_id as OperatorID,'" + intCount + "'as intCount from er_recharge c inner join er_login l on c.userid = l.userid where c.userid > 0 and c.status=2 and c.rollback_status=1 and c.operator_id!=2 " + strSearch + " order by c.id desc " + strLimit + "";
                    }

                    ds = objODBC.Report(strQuery);
                }
                return ds;
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                ds.Dispose();
            }
        }

        // ActivityType=81 -- this is for ratailer app and admin web
        [WebMethod]
        public DataSet er_RollBack_Etisalat_Complete_topup_Report(string strUserName, int intUserID, int PageSize, int intPage, string strSearch, string strIPAddress, int intDesignationType, string strMacAddress, string strOSDetails, string strIMEINo, string strGcmID, string strAPPVersion, int intSource, int intCreatedByType)
        {
            try
            {
                try
                {
                    objCreateFile.CreateTextFile(strIPAddress, strMacAddress, strOSDetails, strIMEINo, strGcmID, strAPPVersion, intSource.ToString(), intCreatedByType, 81, " er_topup_Report", strUserName);
                }
                catch (Exception)
                {


                }
                finally
                {
                    strLimit = " LIMIT " + PageSize * (intPage - 1) + "," + PageSize + "";

                    if (intUserID > 0)
                    {
                        int intCount = objODBC.executeScalar_int("select count(1) from er_recharge c inner join er_login l on c.userid = l.userid where c.userid > 0 and c.status=2  and c.rollback_status>2 " + strSearch + "");
                        strQuery = "select l.userid as UserID,l.username as UserName,l.full_name as Name,l.mobile as Customer_Contact,c.trans_number as Txn_No,c.mobile_number as Top_Up_Mobile_No,c.amount as Amount,c.deduct_amt as Deducted_Amount,c.operator_balance as API_Balance,date_format(c.rollback_confirm_on,'%d %b %Y %H:%i:%s') as Date_of_Txn,c.operator_transid as Operator_Txn_Id,case when c.rollback_status = 1 then 'Pending' when c.rollback_status = 2 then 'In Process' when c.rollback_status = 3 then 'Reject' when c.rollback_status = 4 then 'Complete' end as Txn_Mode,l.province_Name as Province,c.comment,c.operator_name as Operator,c.operator_id as OperatorID,c.rollback_amount,'" + intCount + "'as intCount from er_recharge c inner join er_login l on c.userid = l.userid where c.userid > 0 and c.status=2 and c.rollback_status=2 " + strSearch + " order by c.id desc " + strLimit + "";
                    }

                    ds = objODBC.Report(strQuery);
                }
                return ds;
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                ds.Dispose();
            }
        }


        // ActivityType=81 -- this is for ratailer app and admin web
        [WebMethod]
        public DataSet er_RollBack_topup_Report(string strUserName, int intUserID, int PageSize, int intPage, string strSearch, string strIPAddress, int intDesignationType, string strMacAddress, string strOSDetails, string strIMEINo, string strGcmID, string strAPPVersion, int intSource, int intCreatedByType)
        {
            try
            {
                try
                {
                    objCreateFile.CreateTextFile(strIPAddress, strMacAddress, strOSDetails, strIMEINo, strGcmID, strAPPVersion, intSource.ToString(), intCreatedByType, 81, " er_topup_Report", strUserName);
                }
                catch (Exception)
                {


                }
                finally
                {
                    strLimit = " LIMIT " + PageSize * (intPage - 1) + "," + PageSize + "";

                    if (intUserID > 0)
                    {
                        if (intCreatedByType == 1)
                        {
                            int intCount = objODBC.executeScalar_int("select count(1) from er_recharge c inner join er_login l on c.userid = l.userid where c.userid > 0 and c.rollback_status>2 " + strSearch + "");
                            strQuery = "select  l.username as UserName,l.full_name as Name,l.mobile as Customer_Contact,c.trans_number as Txn_No,c.operator_name as Operator,c.mobile_number as Top_Up_Mobile_No,c.amount as Amount,c.deduct_amt as Deducted_Amount,date_format(c.rollback_confirm_on,'%d %b %Y %H:%i:%s') as Date_of_Txn,case when c.rollback_status = 1 then 'Pending' when c.rollback_status = 2 then 'In Process' when c.rollback_status = 3 then 'Reject' when c.rollback_status = 4 then 'Complete' end as Status,c.rollback_amount,'" + intCount + "'as intCount from er_recharge c inner join er_login l on c.userid = l.userid where c.userid > 0 and c.rollback_status>2 " + strSearch + " order by c.id desc " + strLimit + "";
                        }
                        else
                        {
                            int intCount = objODBC.executeScalar_int("select count(1) from er_recharge c inner join er_login l on c.userid = l.userid where c.userid='"+ intUserID + "'  and c.rollback_status>2 " + strSearch + "");
                            strQuery = "select  l.username as UserName,l.full_name as Name,l.mobile as Customer_Contact,c.trans_number as Txn_No,c.operator_name as Operator,c.mobile_number as Top_Up_Mobile_No,c.amount as Amount,c.deduct_amt as Deducted_Amount,date_format(c.rollback_confirm_on,'%d %b %Y %H:%i:%s') as Date_of_Txn,case when c.rollback_status = 1 then 'Pending' when c.rollback_status = 2 then 'In Process' when c.rollback_status = 3 then 'Reject' when c.rollback_status = 4 then 'Complete' end as Status,c.rollback_amount,'" + intCount + "'as intCount from er_recharge c inner join er_login l on c.userid = l.userid where c.userid='" + intUserID + "' and c.rollback_status>2 " + strSearch + " order by c.id desc " + strLimit + "";
                        }
                    }

                    ds = objODBC.Report(strQuery);
                }
                return ds;
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                ds.Dispose();
            }
        }

        [WebMethod] // intGroupID = er_top_up_group_details_id
        public DataSet er_group_done_topup_report(string strUserName, int intUserID, string intGroupID, string strIPAddress, int intDesignationType, string strMacAddress, string strOSDetails, string strIMEINo, string strGcmID, string strAPPVersion, int intSource, int intCreatedByType)
        {
            try
            {
                try
                {
                    objCreateFile.CreateTextFile(strIPAddress, strMacAddress, strOSDetails, strIMEINo, strGcmID, strAPPVersion, intSource.ToString(), intCreatedByType, 81, "er_group_done_topup_report", strUserName);
                }
                catch (Exception)
                {


                }
                finally
                {
                    strQuery = "SELECT g.recharge_id as TransID,r.mobile_number,r.amount,case when r.status=1 then 'Pending' when r.status=2 then 'Success' when r.status=3 then 'Failed' when r.dispute_status=1 then 'Disputed' end as Status FROM er_group_topup_done_details g inner join er_recharge r on g.recharge_id=r.id  WHERE g.er_top_up_group_details_id='" + intGroupID + "'";

                    ds = objODBC.Report(strQuery);
                }
                return ds;
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                ds.Dispose();
            }
        }


        [WebMethod]
        public DataSet er_mega_slab_details_report(string strUserName, int intUserID, int intUsertype, string strIPAddress, int intDesignationType, string strMacAddress, string strOSDetails, string strIMEINo, string strGcmID, string strAPPVersion, int intSource, int intCreatedByType)
        {
            try
            {
                try
                {
                    objCreateFile.CreateTextFile(strIPAddress, strMacAddress, strOSDetails, strIMEINo, strGcmID, strAPPVersion, intSource.ToString(), intCreatedByType, 81, "er_mega_slab_details_report", strUserName);
                }
                catch (Exception)
                {


                }
                finally
                {
                    int intslabID = objODBC.executeScalar_int("select ifnull(max(slab_id),0) from er_slab_user where userid=" + intUserID + "");

                    int intGroupID = objODBC.executeScalar_int("select ifnull(max(group_id),0) from er_commission_structure where userid=" + intUserID + "");

                    if (intslabID > 0 && intGroupID > 0)
                    {
                        if (intUsertype == 3 && intGroupID > 0) // distributer
                        {
                            int intCount = objODBC.executeScalar_int("select count(1) from er_commission_structure a, er_commission_structure res , er_recharge_operator_mobile b, er_commission_slab c where a.operator_id=b.id and c.id=a.slab_id and res.slab_id=c.parent_slab_id and res.operator_id=b.id and a.group_id=" + intGroupID + "");

                            ds = objODBC.getDataSet("Select a.id, a.operator_id as Operator_ID, a.commission_per as Set_Commission, res.commission_per as My_Commission, b.operator_name as Operator_Name,a.target_amt as Target ,a.target_reward as Reward,a.target_status as Target_Status," + intCount + " FROM er_commission_structure a, er_commission_structure res , er_recharge_operator_mobile b, er_commission_slab c where a.operator_id=b.id and c.id=a.slab_id and res.slab_id=c.parent_slab_id and res.operator_id=b.id and a.group_id=" + intGroupID + " ");
                        }
                        else if (intUsertype == 4 && intGroupID > 0) // sub distributer
                        {

                            int intCount = objODBC.executeScalar_int("select count(1) from er_commission_structure a, er_commission_structure res , er_recharge_operator_mobile b, er_commission_slab c where a.operator_id=b.id and c.id=a.slab_id and res.slab_id=c.parent_slab_id and res.operator_id=b.id and a.group_id=" + intGroupID + " order by a.id asc limit 5,10");


                            ds = objODBC.getDataSet("Select a.id, a.operator_id as Operator_ID, a.commission_per as Set_Commission, res.commission_per as My_Commission, b.operator_name as Operator_Name,a.target_amt as Target ,a.target_reward as Reward,a.target_status as Target_Status," + intCount + " FROM er_commission_structure a, er_commission_structure res , er_recharge_operator_mobile b, er_commission_slab c where a.operator_id=b.id and c.id=a.slab_id and res.slab_id=c.parent_slab_id and res.operator_id=b.id and a.group_id=" + intGroupID + " order by a.id asc limit 5,10");
                        }
                        else if (intUsertype == 4 && intGroupID > 0) // retailer 
                        {
                            int intCount = objODBC.executeScalar_int("select count(1) from er_commission_structure a, er_commission_structure res , er_recharge_operator_mobile b, er_commission_slab c where a.operator_id=b.id and c.id=a.slab_id and res.slab_id=c.parent_slab_id and res.operator_id=b.id and a.group_id=" + intGroupID + " order by a.id asc limit 10,15 ");

                            ds = objODBC.getDataSet("Select a.id, a.operator_id as Operator_ID, a.commission_per as Set_Commission, res.commission_per as My_Commission, b.operator_name as Operator_Name,a.target_amt as Target ,a.target_reward as Reward,a.target_status as Target_Status,"+ intCount + " FROM er_commission_structure a, er_commission_structure res , er_recharge_operator_mobile b, er_commission_slab c where a.operator_id=b.id and c.id=a.slab_id and res.slab_id=c.parent_slab_id and res.operator_id=b.id and a.group_id=" + intGroupID + " order by a.id asc limit 10,15 ");
                        }
                    }
                    else
                    {

                        int intCount = objODBC.executeScalar_int("select count(1) from er_commission_structure a, er_commission_structure res , er_recharge_operator_mobile b, er_commission_slab c where a.operator_id=b.id and c.id=a.slab_id and res.slab_id=c.parent_slab_id and res.operator_id=b.id and a.group_id=-1 order by a.id asc limit 10,15 ");

                        ds = objODBC.getDataSet("Select a.id, a.operator_id as Operator_ID, a.commission_per as Set_Commission, res.commission_per as My_Commission, b.operator_name as Operator_Name,a.target_amt as Target ,a.target_reward as Reward,a.target_status as Target_Status," + intCount + " FROM er_commission_structure a, er_commission_structure res , er_recharge_operator_mobile b, er_commission_slab c where a.operator_id=b.id and c.id=a.slab_id and res.slab_id=c.parent_slab_id and res.operator_id=b.id and a.group_id=-1");

                    }


                    ds = objODBC.Report(strQuery);
                }
                return ds;
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                ds.Dispose();
            }
        }


        // Admin Finincial Report


        [WebMethod]
        public DataSet er_GetAD_SubDis_OP_details(string strUserName, string strUserNameID, string strSearch, string strSearch2, string strIPAddress, int intDesignationType, string strMacAddress, string strOSDetails, string strIMEINo, string strGcmID, string strAPPVersion, int intSource, int intCreatedByType)
        {
            try
            {
                try
                {
                    objCreateFile.CreateTextFile(strIPAddress, strMacAddress, strOSDetails, strIMEINo, strGcmID, strAPPVersion, intSource.ToString(), intCreatedByType, 81, "er_GetAD_SubDis_OP_details", strUserName);
                }
                catch (Exception)
                {


                }
                finally
                {
                    int intCount = objODBC.executeScalar_int("SELECT count(1) FROM `er_login` where username='" + strUserNameID + "'");
                    if(intCount>0)
                    {
                        int intUserID = objODBC.executeScalar_int("SELECT userid FROM `er_login` where username='" + strUserNameID + "'");
                        strQuery = "call `er_get_SubDis_OP_details`('" + intUserID + "','" + strSearch + "','"+ strSearch2 + "')";
                        ds = objODBC.Report(strQuery);                       
                    }
                    else
                    {
                        ds = objODBC.Report(strQuery);                        
                    }
                }
                return ds;
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                ds.Dispose();
            }
        }


        // and DATE(c.created_on)  BETWEEN '2019-11-11' AND '2019-11-14'
        [WebMethod]
        public DataSet er_GetAD_Operator_Userwise_details(string strUserName, int intUserID, int intOperatorID, string strSearch,string strINSearch, string strIPAddress, int intDesignationType, string strMacAddress, string strOSDetails, string strIMEINo, string strGcmID, string strAPPVersion, int intSource, int intCreatedByType)
        {
            try
            {
                try
                {
                    objCreateFile.CreateTextFile(strIPAddress, strMacAddress, strOSDetails, strIMEINo, strGcmID, strAPPVersion, intSource.ToString(), intCreatedByType, 81, "er_GetAD_Operator_Userwise_details", strUserName);
                }
                catch (Exception)
                {


                }
                finally
                {
                    strQuery = "call `er_GetAD_Operator_Userwise_details`('" + intOperatorID + "','" + strSearch + "','"+ strINSearch + "');";

                    ds = objODBC.Report(strQuery);
                }
                return ds;
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                ds.Dispose();
            }
        }


        [WebMethod]
        public DataSet er_GetAD_Operators_Summary_Report(string strUserName, int intUserID, string strStartDate, string strEndDate, string strSearch, string strIPAddress, int intDesignationType, string strMacAddress, string strOSDetails, string strIMEINo, string strGcmID, string strAPPVersion, int intSource, int intCreatedByType)
        {
            try
            {
                try
                {
                    objCreateFile.CreateTextFile(strIPAddress, strMacAddress, strOSDetails, strIMEINo, strGcmID, strAPPVersion, intSource.ToString(), intCreatedByType, 81, "er_GetAD_Operator_Userwise_details", strUserName);
                }
                catch (Exception)
                {


                }
                finally
                {
                    if (strStartDate == objDT.getCurDateString())
                    {
                        strQuery = "call `er_GetAD_Operator_Userwise_Summ_details`('" + strSearch + "','" + strStartDate + "','" + strEndDate + "');";
                    }
                    else
                    {
                        int intFinCount = objODBC.executeScalar_int("Select Count(1) from er_admin_finance_op_details where DATE(created_on)='" + strStartDate + "'");
                        if (intFinCount == 1)
                        {
                            strQuery = "Select COALESCE(OpBalanceSalaam,0) as OpBalanceSalaam,COALESCE(OpBalanceEtisalat,0) as OpBalanceEtisalat,COALESCE(OpBalanceRoshan,0) as OpBalanceRoshan,COALESCE(OpBalanceMTN,0) as OpBalanceMTN,COALESCE(OpBalanceAWCC,0) as OpBalanceAWCC,COALESCE(CLOBalanceSalaam,0) as CLOBalanceSalaam,COALESCE(CLOBalanceEtisalat,0) as CLOBalanceEtisalat,COALESCE(CLOBalanceRoshan,0) as CLOBalanceRoshan,COALESCE(CLOBalanceMTN,0) as CLOBalanceMTN,COALESCE(CLOBalanceAWCC,0) as CLOBalanceAWCC,COALESCE(sum(Purchased_Balance_Salaam),0) as Purchased_Balance_Salaam,COALESCE(sum(Purchased_Balance_Etisalat),0) as Purchased_Balance_Etisalat,COALESCE(sum(Purchased_Balance_Roshan),0) as Purchased_Balance_Roshan,COALESCE(sum(Purchased_Balance_MTN),0) as Purchased_Balance_MTN,COALESCE(sum(Purchased_Balance_AWCC ),0) as Purchased_Balance_AWCC,COALESCE(sum(Ad_Earn_Commission_Salaam),0) as Ad_Earn_Commission_Salaam,COALESCE(sum(Ad_Earn_Commission_Etisalat),0) as Ad_Earn_Commission_Etisalat,COALESCE(sum(Ad_Earn_Commission_Roshan),0) as Ad_Earn_Commission_Roshan,COALESCE(sum(Ad_Earn_Commission_MTN),0) as Ad_Earn_Commission_MTN,COALESCE(sum(Ad_Earn_Commission_AWCC),0) as Ad_Earn_Commission_AWCC,COALESCE(sum(User_Earn_Commission_Salaam),0) as User_Earn_Commission_Salaam,COALESCE(sum(User_Earn_Commission_Etisalat),0) as User_Earn_Commission_Etisalat,COALESCE(sum(User_Earn_Commission_Roshan),0) as User_Earn_Commission_Roshan,COALESCE(sum(User_Earn_Commission_MTN),0) as User_Earn_Commission_MTN,COALESCE(sum(User_Earn_Commission_AWCC),0) as User_Earn_Commission_AWCC,COALESCE(sum(Total_Sale_Salaam),0) as Total_Sale_Salaam,COALESCE(sum( Total_Sale_Etisalat),0) as Total_Sale_Etisalat,COALESCE(sum( Total_Sale_Roshan),0) as Total_Sale_Roshan,COALESCE(sum( Total_Sale_MTN),0) as Total_Sale_MTN,COALESCE(sum( Total_Sale_AWCC),0) as Total_Sale_AWCC,COALESCE(sum(Generate_Emoney_Salaam),0) as Generate_Emoney_Salaam,COALESCE(sum( Generate_Emoney_Etisalat),0) as Generate_Emoney_Etisalat,COALESCE(sum( Generate_Emoney_Roshan),0) as Generate_Emoney_Roshan,COALESCE(sum( Generate_Emoney_MTN),0) as Generate_Emoney_MTN,COALESCE(sum( Generate_Emoney_AWCC),0) as Generate_Emoney_AWCC,COALESCE(sum(Net_Profit_Salaam),0) as Net_Profit_Salaam,COALESCE(sum(Net_Profit_Etisalat),0) as Net_Profit_Etisalat,COALESCE(sum(Net_Profit_Roshan),0) as Net_Profit_Roshan,COALESCE(sum(Net_Profit_MTN),0) as Net_Profit_MTN,COALESCE(sum(Net_Profit_AWCC),0) as Net_Profit_AWCC from er_admin_finance_op_details where DATE(created_on) BETWEEN '" + strStartDate + "' AND '" + strEndDate + "';";
                        }
                        else
                        {
                            strQuery = "Select COALESCE(OpBalanceSalaam,0) as OpBalanceSalaam,COALESCE(OpBalanceEtisalat,0) as OpBalanceEtisalat,COALESCE(OpBalanceRoshan,0) as OpBalanceRoshan,COALESCE(OpBalanceMTN,0) as OpBalanceMTN,COALESCE(OpBalanceAWCC,0) as OpBalanceAWCC,COALESCE(CLOBalanceSalaam,0) as CLOBalanceSalaam,COALESCE(CLOBalanceEtisalat,0) as CLOBalanceEtisalat,COALESCE(CLOBalanceRoshan,0) as CLOBalanceRoshan,COALESCE(CLOBalanceMTN,0) as CLOBalanceMTN,COALESCE(CLOBalanceAWCC,0) as CLOBalanceAWCC,COALESCE(sum(Purchased_Balance_Salaam),0) as Purchased_Balance_Salaam,COALESCE(sum(Purchased_Balance_Etisalat),0) as Purchased_Balance_Etisalat,COALESCE(sum(Purchased_Balance_Roshan),0) as Purchased_Balance_Roshan,COALESCE(sum(Purchased_Balance_MTN),0) as Purchased_Balance_MTN,COALESCE(sum(Purchased_Balance_AWCC ),0) as Purchased_Balance_AWCC,COALESCE(sum(Ad_Earn_Commission_Salaam),0) as Ad_Earn_Commission_Salaam,COALESCE(sum(Ad_Earn_Commission_Etisalat),0) as Ad_Earn_Commission_Etisalat,COALESCE(sum(Ad_Earn_Commission_Roshan),0) as Ad_Earn_Commission_Roshan,COALESCE(sum(Ad_Earn_Commission_MTN),0) as Ad_Earn_Commission_MTN,COALESCE(sum(Ad_Earn_Commission_AWCC),0) as Ad_Earn_Commission_AWCC,COALESCE(sum(User_Earn_Commission_Salaam),0) as User_Earn_Commission_Salaam,COALESCE(sum(User_Earn_Commission_Etisalat),0) as User_Earn_Commission_Etisalat,COALESCE(sum(User_Earn_Commission_Roshan),0) as User_Earn_Commission_Roshan,COALESCE(sum(User_Earn_Commission_MTN),0) as User_Earn_Commission_MTN,COALESCE(sum(User_Earn_Commission_AWCC),0) as User_Earn_Commission_AWCC,COALESCE(sum(Total_Sale_Salaam),0) as Total_Sale_Salaam,COALESCE(sum( Total_Sale_Etisalat),0) as Total_Sale_Etisalat,COALESCE(sum( Total_Sale_Roshan),0) as Total_Sale_Roshan,COALESCE(sum( Total_Sale_MTN),0) as Total_Sale_MTN,COALESCE(sum( Total_Sale_AWCC),0) as Total_Sale_AWCC,COALESCE(sum(Generate_Emoney_Salaam),0) as Generate_Emoney_Salaam,COALESCE(sum( Generate_Emoney_Etisalat),0) as Generate_Emoney_Etisalat,COALESCE(sum( Generate_Emoney_Roshan),0) as Generate_Emoney_Roshan,COALESCE(sum( Generate_Emoney_MTN),0) as Generate_Emoney_MTN,COALESCE(sum( Generate_Emoney_AWCC),0) as Generate_Emoney_AWCC,COALESCE(sum(Net_Profit_Salaam),0) as Net_Profit_Salaam,COALESCE(sum(Net_Profit_Etisalat),0) as Net_Profit_Etisalat,COALESCE(sum(Net_Profit_Roshan),0) as Net_Profit_Roshan,COALESCE(sum(Net_Profit_MTN),0) as Net_Profit_MTN,COALESCE(sum(Net_Profit_AWCC),0) as Net_Profit_AWCC from er_admin_finance_op_details where DATE(created_on) BETWEEN '" + strStartDate + "' AND '" + strEndDate + "';";
                            try
                            {
                                objODBC.executeNonQuery("CALL sp_er_Admin_Operator_date_Call();");
                            }
                            catch (Exception)
                            {

                            }
                        }
                    }

                    ds = objODBC.Report(strQuery);
                }
                return ds;
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                ds.Dispose();
            }
        }



        [WebMethod]
        public DataSet er_GetAD_SubDis_wise_Summ_details(string strUserName, string strStartDate, string strEndDate, string strSearch,string strINSearch, string strIPAddress, int intDesignationType, string strMacAddress, string strOSDetails, string strIMEINo, string strGcmID, string strAPPVersion, int intSource, int intCreatedByType)
        {
            try
            {
                try
                {
                    objCreateFile.CreateTextFile(strIPAddress, strMacAddress, strOSDetails, strIMEINo, strGcmID, strAPPVersion, intSource.ToString(), intCreatedByType, 81, "er_SubDis_wise_Summ_details", strUserName);
                }
                catch (Exception)
                {


                }
                finally
                {
                    if (strSearch != "0")
                    {
                        // strQuery = "SELECT distinct l.userid,l.username as username,sum(distinct c.recharge_amount) as Total_Recharge_Amount, sum(c.total_commission) as TotalCommission,OPB.closing_balance as OpeningBalance,OPB.created_on as OpeningBalanceDate,CPB.closing_balance as UClosingBalance,CPB.created_on as UClosingBalanceDate  FROM er_commission_amount c inner join er_login l on c.parent_id=l.userid inner join er_users_trackbalance OPB on c.parent_id=OPB.userid  inner join er_users_trackbalance CPB on c.parent_id=CPB.userid WHERE c.status=2 and OPB.created_on=DATE_SUB(DATE( '" + strStartDate + "'), INTERVAL 1 DAY) and CPB.created_on=Date( '" + strEndDate + "') and DATE(c.created_on)  BETWEEN '" + strStartDate + "' AND  '" + strEndDate + "' and l.parent_id=1 " + strSearch + " GROUP BY c.parent_id order by l.parent_id Asc";

                        // strQuery = "SELECT distinct l.userid,l.username as username,(Select sum(recharge_amount) from er_commission_amount where parent_id=c.parent_id and status=2 and DATE(created_on)  BETWEEN '" + strStartDate + "' AND  '" + strEndDate + "' " + strSearch + ") as Total_Recharge_Amount,(Select sum(commission_amount) from er_commission_amount where parent_id=c.parent_id and status=2 and DATE(created_on)  BETWEEN '" + strStartDate + "' AND  '" + strEndDate + "' " + strSearch + ") as TotalCommission,OPB.closing_balance as OpeningBalance,OPB.created_on as OpeningBalanceDate,CPB.closing_balance as UClosingBalance,CPB.created_on as UClosingBalanceDate  FROM er_commission_amount c inner join er_login l on c.parent_id=l.userid inner join er_users_trackbalance OPB on c.parent_id=OPB.userid  inner join er_users_trackbalance CPB on c.parent_id=CPB.userid WHERE c.operator_id=2 and c.status=2 and OPB.created_on=DATE_SUB(DATE( '" + strStartDate + "'), INTERVAL 1 DAY) and CPB.created_on=Date( '" + strEndDate + "') and DATE(c.created_on)  BETWEEN '" + strStartDate + "' AND  '" + strEndDate + "' and l.parent_id=1 " + strSearch + " order by l.userid desc";

                        //   strQuery = "SELECT distinct l.userid,l.username as username,(Select COALESCE(SUM(recharge_amount), 0) from er_commission_amount where parent_id=c.parent_id and status=2 and DATE(created_on)  BETWEEN '" + strStartDate + "' AND  '" + strEndDate + "' " + strSearch + ") as Total_Recharge_Amount,(Select COALESCE(sum(commission_amount), 0)  from er_commission_amount where parent_id=c.parent_id and status=2 and DATE(created_on)  BETWEEN '" + strStartDate + "' AND  '" + strEndDate + "' " + strSearch + ") as TotalCommission,(select COALESCE(closing_balance, 0) from er_users_trackbalance where userid=c.parent_id and created_on=DATE_SUB(DATE( '" + strStartDate + "'), INTERVAL 1 DAY)) as OpeningBalance,DATE_SUB(DATE( '" + strStartDate + "'), INTERVAL 1 DAY) as OpeningBalanceDate,(select COALESCE(closing_balance, 0) from er_users_trackbalance where userid=c.parent_id and created_on=Date( '" + strEndDate + "')) as UClosingBalance,'" + strEndDate + "' as UClosingBalanceDate  FROM er_commission_amount c inner join er_login l on c.parent_id=l.userid WHERE c.status=2 and DATE(c.created_on)  BETWEEN '" + strStartDate + "' AND  '" + strEndDate + "' and l.parent_id=1 " + strSearch + " order by l.userid desc";

                        strQuery = "SELECT l.userid,l.username as username,CASE WHEN l.usertype_id = 5 THEN(Select COALESCE(SUM(recharge_amount), 0) from er_commission_amount where userid = parent_id and l.userid = userid and status = 2 and DATE(created_on)  BETWEEN '" + strStartDate + "' AND  '" + strEndDate + "') WHEN l.usertype_id = 4 THEN(Select COALESCE(SUM(recharge_amount), 0) from er_commission_amount where parent_id = l.userid and status = 2 and DATE(created_on)  BETWEEN '" + strStartDate + "' AND  '" + strEndDate + "') WHEN l.usertype_id = 3 THEN(Select COALESCE(SUM(recharge_amount), 0) from er_commission_amount where parent_id = l.userid and status = 2 and DATE(created_on)  BETWEEN '" + strStartDate + "' AND  '" + strEndDate + "') WHEN l.usertype_id = 1 THEN(Select COALESCE(SUM(recharge_amount), 0) from er_commission_amount where parent_id = l.userid and status = 2 and DATE(created_on)  BETWEEN '" + strStartDate + "' AND  '" + strEndDate + "')  ELSE 0 END AS Total_Recharge_Amount,(Select COALESCE(sum(commission_amount), 0)  from er_commission_amount where parent_id=l.userid and status=2 and DATE(created_on)  BETWEEN '" + strStartDate + "' AND  '" + strEndDate + "') as TotalCommission,(select COALESCE(closing_balance, 0) from er_users_trackbalance where l.userid=userid and created_on=DATE_SUB(DATE( '" + strStartDate + "'), INTERVAL 1 DAY)) as OpeningBalance,DATE_SUB(DATE( '" + strStartDate + "'), INTERVAL 1 DAY) as OpeningBalanceDate,(select COALESCE(closing_balance, 0) from er_users_trackbalance where userid=l.userid and created_on=Date('" + strEndDate + "')) as UClosingBalance,'" + strEndDate + "' as UClosingBalanceDate  FROM er_login l WHERE 1=1 group by l.userid order by l.userid Asc;";

                        ds = objODBC.Report(strQuery);
                    }
                    else
                    {

                        //  strQuery = "SELECT distinct l.userid,l.username as username,sum(distinct c.recharge_amount) as Total_Recharge_Amount, sum(c.total_commission) as TotalCommission,OPB.closing_balance as OpeningBalance,OPB.created_on as OpeningBalanceDate,CPB.closing_balance as UClosingBalance,CPB.created_on as UClosingBalanceDate  FROM er_commission_amount c inner join er_login l on c.parent_id=l.userid inner join er_users_trackbalance OPB on c.parent_id=OPB.userid  inner join er_users_trackbalance CPB on c.parent_id=CPB.userid WHERE c.status=2 and OPB.created_on=DATE_SUB(DATE( '" + strStartDate + "'), INTERVAL 1 DAY) and CPB.created_on=Date( '" + strStartDate + "')  and l.parent_id=1  GROUP BY c.parent_id order by c.parent_id Asc";

                        // strQuery = "SELECT distinct l.userid,l.username as username,(Select sum(recharge_amount) from er_commission_amount where parent_id=c.parent_id and status=2 and DATE(created_on)  BETWEEN '" + strStartDate + "' AND  '" + strEndDate + "') as Total_Recharge_Amount,(Select sum(commission_amount) from er_commission_amount where parent_id=c.parent_id and status=2 and DATE(created_on)  BETWEEN '" + strStartDate + "' AND  '" + strEndDate + "') as TotalCommission,OPB.closing_balance as OpeningBalance,OPB.created_on as OpeningBalanceDate,CPB.closing_balance as UClosingBalance,CPB.created_on as UClosingBalanceDate  FROM er_commission_amount c inner join er_login l on c.parent_id=l.userid inner join er_users_trackbalance OPB on c.parent_id=OPB.userid  inner join er_users_trackbalance CPB on c.parent_id=CPB.userid WHERE c.operator_id=2 and c.status=2 and OPB.created_on=DATE_SUB(DATE( '" + strStartDate + "'), INTERVAL 1 DAY) and CPB.created_on=Date( '" + strEndDate + "') and DATE(c.created_on)  BETWEEN '" + strStartDate + "' AND  '" + strEndDate + "' and l.parent_id=1 order by l.userid desc";


                        //  strQuery = "SELECT distinct l.userid,l.username as username,(Select COALESCE(SUM(recharge_amount), 0) from er_commission_amount where parent_id=c.parent_id and status=2 and DATE(created_on)  BETWEEN '" + strStartDate + "' AND  '" + strEndDate + "' ) as Total_Recharge_Amount,(Select COALESCE(sum(commission_amount), 0)  from er_commission_amount where parent_id=c.parent_id and status=2 and DATE(created_on)  BETWEEN '" + strStartDate + "' AND  '" + strEndDate + "') as TotalCommission,(select COALESCE(closing_balance, 0) from er_users_trackbalance where userid=c.parent_id and created_on=DATE_SUB(DATE( '" + strStartDate + "'), INTERVAL 1 DAY)) as OpeningBalance,DATE_SUB(DATE( '" + strStartDate + "'), INTERVAL 1 DAY) as OpeningBalanceDate,(select COALESCE(closing_balance, 0) from er_users_trackbalance where userid=c.parent_id and created_on=Date( '" + strEndDate + "')) as UClosingBalance,'" + strEndDate + "' as UClosingBalanceDate  FROM er_commission_amount c inner join er_login l on c.parent_id=l.userid WHERE c.status=2 and DATE(c.created_on)  BETWEEN '" + strStartDate + "' AND  '" + strEndDate + "' and l.parent_id=1  order by l.userid desc";

                        strQuery = "SELECT l.userid,l.username as username,CASE WHEN l.usertype_id = 5 THEN(Select COALESCE(SUM(recharge_amount), 0) from er_commission_amount where userid = parent_id and l.userid = userid and status = 2 and DATE(created_on)  BETWEEN '" + strStartDate + "' AND  '" + strEndDate + "') WHEN l.usertype_id = 4 THEN(Select COALESCE(SUM(recharge_amount), 0) from er_commission_amount where parent_id = l.userid and status = 2 and DATE(created_on)  BETWEEN '" + strStartDate + "' AND  '" + strEndDate + "') WHEN l.usertype_id = 3 THEN(Select COALESCE(SUM(recharge_amount), 0) from er_commission_amount where parent_id = l.userid and status = 2 and DATE(created_on)  BETWEEN '" + strStartDate + "' AND  '" + strEndDate + "') WHEN l.usertype_id = 1 THEN(Select COALESCE(SUM(recharge_amount), 0) from er_commission_amount where parent_id = l.userid and status = 2 and DATE(created_on)  BETWEEN '" + strStartDate + "' AND  '" + strEndDate + "')  ELSE 0 END AS Total_Recharge_Amount,(Select COALESCE(sum(commission_amount), 0)  from er_commission_amount where parent_id=l.userid and status=2 and DATE(created_on)  BETWEEN '" + strStartDate + "' AND  '" + strEndDate + "') as TotalCommission,(select COALESCE(closing_balance, 0) from er_users_trackbalance where l.userid=userid and created_on=DATE_SUB(DATE( '" + strStartDate + "'), INTERVAL 1 DAY)) as OpeningBalance,DATE_SUB(DATE( '" + strStartDate + "'), INTERVAL 1 DAY) as OpeningBalanceDate,(select COALESCE(closing_balance, 0) from er_users_trackbalance where userid=l.userid and created_on=Date('" + strEndDate + "')) as UClosingBalance,'" + strEndDate + "' as UClosingBalanceDate  FROM er_login l WHERE 1=1 group by l.userid order by l.userid Asc;";





                        ds = objODBC.Report(strQuery);
                    }
                }
                return ds;
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                ds.Dispose();
            }
        }

        // ------------------------------ Support Operator All Response Report -------------------------------------------------------------- //

        [WebMethod]
        public DataSet er_getEtisalatResponse(string strUserName, int intUserID, string strSearch, string strIPAddress, int intDesignationType, string strMacAddress, string strOSDetails, string strIMEINo, string strGcmID, string strAPPVersion, int intSource, int intCreatedByType)
        {
            try
            {
                try
                {
                    objCreateFile.CreateTextFile(strIPAddress, strMacAddress, strOSDetails, strIMEINo, strGcmID, strAPPVersion, intSource.ToString(), intCreatedByType, 81, "er_getEtisalatResponse", strUserName);
                }
                catch (Exception)
                {


                }
                finally
                {



                    strQuery = "SELECT e.`id`,l.username,e.`userid`,e.`recharge_id`,e.`topup_mobile`,e.`amount`,e.`status`,e.`login_request`,e. `login_response`,e.`balance`,e.`login_status`,e.`login_operation_code`,e.`transaction_ID_recharge`,e.`transaction_OperaterID`, e.`transaction_RETN`,e.`transaction_DESC`,e.`BEFOROPER`,e.`FEE`,e.`AFTEROPER`,e.`CSVSTOP`, e.`Transaction_Request`,e.`Transaction_Response`, e.`Transactio_Status`,e.`ReverseID`,e.`ReverseID_operaterID`,case when e.transaction_type = 1 then 'Topup' when e.transaction_type = 2 then 'Reversed'  end as TransactionType,case when e.Topup_type = 1 then 'Topup' when e.Topup_type = 2 then 'USSD' when e.Topup_type = 3 then 'SMS' end as topupType,e.`created_on`,e.`all_response` FROM `etisalat_topup_request` e inner join er_login l on e.userid=l.userid  where 1=1 '" + strSearch + "');";

                    ds = objODBC.Report(strQuery);
                }
                return ds;
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                ds.Dispose();
            }
        }


        [WebMethod]
        public DataSet er_getEtisalatReverseToupResponse(string strUserName, int intUserID, string strSearch, string strIPAddress, int intDesignationType, string strMacAddress, string strOSDetails, string strIMEINo, string strGcmID, string strAPPVersion, int intSource, int intCreatedByType)
        {
            try
            {
                try
                {
                    objCreateFile.CreateTextFile(strIPAddress, strMacAddress, strOSDetails, strIMEINo, strGcmID, strAPPVersion, intSource.ToString(), intCreatedByType, 81, "er_getEtisalatReverseToupResponse", strUserName);
                }
                catch (Exception)
                {


                }
                finally
                {

                    strQuery = "SELECT e.`id`,l.username,e.`userid`,e.`etisalat_table_topup_ID`,e.`mobile`,e.`amount`,e.`recharge_id`,e.`OperatorID`, e.`api_Operater_ID`,e.`created_on`,case when e.status = 0 then 'Pending' when e.status = 2 then 'Success'  when e.status = 1 then 'Failed' end as Status,e.`request`,e.`resposne`,e.`all_response` FROM `etisalat_topup_request` e inner join er_login l on e.userid=l.userid  where 1=1 '" + strSearch + "');";

                    ds = objODBC.Report(strQuery);
                }
                return ds;
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                ds.Dispose();
            }
        }







        // ------------------------------ for android -------------------------------------------------------------- //
        [WebMethod]
        public ArrayList er_android_mega_slab_details_report(string strUserName, int intUserID, int intUsertype, string strIPAddress, int intDesignationType, string strMacAddress, string strOSDetails, string strIMEINo, string strGcmID, string strAPPVersion, int intSource, int intCreatedByType)
        {
            ArrayList arrlst = new ArrayList();
            try
            {
                objCreateFile.CreateTextFile(strIPAddress, strMacAddress, strOSDetails, strIMEINo, strGcmID, strAPPVersion, intSource.ToString(), intCreatedByType, 111, " er_mega_slab_details_report ", strUserName);
            }
            catch (Exception)
            {


            }
            finally
            {


                try
                {
                    ds = er_mega_slab_details_report(strUserName, intUserID, intUsertype, strIPAddress, intDesignationType, strMacAddress, strOSDetails, strIMEINo, strGcmID, strAPPVersion, intSource, intCreatedByType);

                    arrlst = objcf.Get_ArrayLlist_From_Dataset(ds);

                }
                catch (Exception)
                {

                    throw;
                }


            }
            return arrlst;
        }


        [WebMethod]
        public ArrayList er_android_group_done_topup_report(string strUserName, int intUserID, string intGroupID, string strIPAddress, int intDesignationType, string strMacAddress, string strOSDetails, string strIMEINo, string strGcmID, string strAPPVersion, int intSource, int intCreatedByType)
        {
            ArrayList arrlst = new ArrayList();
            try
            {
                objCreateFile.CreateTextFile(strIPAddress, strMacAddress, strOSDetails, strIMEINo, strGcmID, strAPPVersion, intSource.ToString(), intCreatedByType, 111, " er_android_group_topup_report ", strUserName);
            }
            catch (Exception)
            {


            }
            finally
            {


                try
                {
                    ds = er_group_done_topup_report(strUserName, intUserID, intGroupID, strIPAddress, intDesignationType, strMacAddress, strOSDetails, strIMEINo, strGcmID, strAPPVersion, intSource, intCreatedByType);

                    arrlst = objcf.Get_ArrayLlist_From_Dataset(ds);

                }
                catch (Exception)
                {

                    throw;
                }


            }
            return arrlst;
        }



        [WebMethod]
        public ArrayList er_android_Reverse_TopUp_Report(string strUserName, int intUserID, int PageSize, int intPage, string strSearch, string strIPAddress, int intDesignationType, string strMacAddress, string strOSDetails, string strIMEINo, string strGcmID, string strAPPVersion, int intSource, int intCreatedByType)
        {
            ArrayList arrlst = new ArrayList();
            try
            {
                objCreateFile.CreateTextFile(strIPAddress, strMacAddress, strOSDetails, strIMEINo, strGcmID, strAPPVersion, intSource.ToString(), intCreatedByType, 111, " er_android_group_topup_report ", strUserName);
            }
            catch (Exception)
            {


            }
            finally
            {


                try
                {
                    ds = er_Reverse_TopUp_Report(strUserName, intUserID, PageSize, intPage, strSearch, strIPAddress, intDesignationType, strMacAddress, strOSDetails, strIMEINo, strGcmID, strAPPVersion, intSource, intCreatedByType);

                    arrlst = objcf.Get_ArrayLlist_From_Dataset(ds);

                }
                catch (Exception)
                {

                    throw;
                }


            }
            return arrlst;
        }



        [WebMethod]
        public ArrayList er_android_group_topup_report(string strUserName, int intUserID, int intUserType, int usertype_id, int PageSize, int intPage, string strSearch, string strIPAddress, int intDesignationType, string strMacAddress, string strOSDetails, string strIMEINo, string strGcmID, string strAPPVersion, int intSource, int intCreatedByType)
        {
            ArrayList arrlst = new ArrayList();
            try
            {
                objCreateFile.CreateTextFile(strIPAddress, strMacAddress, strOSDetails, strIMEINo, strGcmID, strAPPVersion, intSource.ToString(), intCreatedByType, 111, " er_android_group_topup_report ", strUserName);
            }
            catch (Exception)
            {


            }
            finally
            {


                try
                {
                    ds = er_group_topup_report(strUserName, intUserID, intUserType, PageSize, intPage, strSearch, strIPAddress, intDesignationType, strMacAddress, strOSDetails, strIMEINo, strGcmID, strAPPVersion, intSource, intCreatedByType);

                    arrlst = objcf.Get_ArrayLlist_From_Dataset(ds);

                }
                catch (Exception)
                {

                    throw;
                }


            }
            return arrlst;
        }

        [WebMethod]
        public ArrayList er_android_group_topup_report_details(string strUserName, int intUserID, int intUserType, string intID, int usertype_id, int PageSize, int intPage, string strSearch, string strIPAddress, int intDesignationType, string strMacAddress, string strOSDetails, string strIMEINo, string strGcmID, string strAPPVersion, int intSource, int intCreatedByType)
        {
            ArrayList arrlst = new ArrayList();
            try
            {
                objCreateFile.CreateTextFile(strIPAddress, strMacAddress, strOSDetails, strIMEINo, strGcmID, strAPPVersion, intSource.ToString(), intCreatedByType, 111, " er_android_group_topup_report_details ", strUserName);
            }
            catch (Exception)
            {


            }
            finally
            {


                try
                {
                    ds = er_group_topup_report_details(strUserName, intUserID, intUserType, intID.ToString(), PageSize, intPage, strSearch, strIPAddress, intDesignationType, strMacAddress, strOSDetails, strIMEINo, strGcmID, strAPPVersion, intSource, intCreatedByType);

                    arrlst = objcf.Get_ArrayLlist_From_Dataset(ds);

                }
                catch (Exception)
                {

                    throw;
                }


            }
            return arrlst;
        }



        [WebMethod]
        public ArrayList er_android_admin_member_activity_log_report(string strUserName, int intUserID, int intUserType, int usertype_id, int PageSize, int intPage, string strSearch, string strIPAddress, int intDesignationType, string strMacAddress, string strOSDetails, string strIMEINo, string strGcmID, string strAPPVersion, int intSource, int intCreatedByType)
        {
            ArrayList arrlst = new ArrayList();
            try
            {
                objCreateFile.CreateTextFile(strIPAddress, strMacAddress, strOSDetails, strIMEINo, strGcmID, strAPPVersion, intSource.ToString(), intCreatedByType, 111, " er_android_admin_member_activity_log_report ", strUserName);
            }
            catch (Exception)
            {


            }
            finally
            {


                try
                {
                    ds = er_admin_member_activity_log_report(strUserName, intUserType, intUserID, usertype_id, PageSize, intPage, strSearch, strIPAddress, intDesignationType, strMacAddress, strOSDetails, strIMEINo, strGcmID, strAPPVersion, intSource, intCreatedByType);

                    arrlst = objcf.Get_ArrayLlist_From_Dataset(ds);

                }
                catch (Exception)
                {

                    throw;
                }


            }
            return arrlst;
        }

        [WebMethod]
        public ArrayList er_android_wallet_request_report(string strUserName, int intUserID, int intUserType, int PageSize, int intPage, string strSearch, string strIPAddress, int intDesignationType, string strMacAddress, string strOSDetails, string strIMEINo, string strGcmID, string strAPPVersion, int intSource, int intCreatedByType)
        {
            ArrayList arrlst = new ArrayList();
            try
            {
                objCreateFile.CreateTextFile(strIPAddress, strMacAddress, strOSDetails, strIMEINo, strGcmID, strAPPVersion, intSource.ToString(), intCreatedByType, 112, " er_android_wallet_request_report ", strUserName);
            }
            catch (Exception)
            {


            }
            finally
            {
                try
                {
                    ds = er_Wallet_Requested_Report(strUserName, intUserID, PageSize, intPage, strSearch, strIPAddress, intDesignationType, strMacAddress, strOSDetails, strIMEINo, strGcmID, strAPPVersion, intSource, intCreatedByType);

                    arrlst = objcf.Get_ArrayLlist_From_Dataset(ds);

                }
                catch (Exception)
                {
                    throw;

                }
            }

            return arrlst;

        }

        [WebMethod]
        public ArrayList er_android_commission_report(string strUserName, int intUserID, int PageSize, int intPage, string strSearch, string strIPAddress, int intDesignationType, string strMacAddress, string strOSDetails, string strIMEINo, string strGcmID, string strAPPVersion, int intSource, int intCreatedByType)
        {
            ArrayList arrlst = new ArrayList();
            try
            {
                objCreateFile.CreateTextFile(strIPAddress, strMacAddress, strOSDetails, strIMEINo, strGcmID, strAPPVersion, intSource.ToString(), intCreatedByType, 113, " er_android_commission_report ", strUserName);
            }
            catch (Exception)
            {


            }
            finally
            {
                try
                {
                    ds = er_Commission_Report(strUserName, intUserID, PageSize, intPage, strSearch, strIPAddress, intDesignationType, strMacAddress, strOSDetails, strIMEINo, strGcmID, strAPPVersion, intSource, intCreatedByType);

                    arrlst = objcf.Get_ArrayLlist_From_Dataset(ds);

                }
                catch (Exception)
                {


                }
            }

            return arrlst;

        }

        [WebMethod]
        public ArrayList er_android_topup_report(string strUserName, int intUserID, int PageSize, int intPage, string strSearch, string strIPAddress, int intDesignationType, string strMacAddress, string strOSDetails, string strIMEINo, string strGcmID, string strAPPVersion, int intSource, int intCreatedByType)
        {
            ArrayList arrlst = new ArrayList();
            try
            {
                objCreateFile.CreateTextFile(strIPAddress, strMacAddress, strOSDetails, strIMEINo, strGcmID, strAPPVersion, intSource.ToString(), intCreatedByType, 114, " er_android_topup_report", strUserName);
            }
            catch (Exception)
            {


            }
            finally
            {
                try
                {
                    ds = er_topup_Report(strUserName, intUserID, PageSize, intPage, strSearch, strIPAddress, intDesignationType, strMacAddress, strOSDetails, strIMEINo, strGcmID, strAPPVersion, intSource, intCreatedByType);

                    arrlst = objcf.Get_ArrayLlist_From_Dataset(ds);

                }
                catch (Exception)
                {


                }
            }
            return arrlst;
        }

        [WebMethod]
        public ArrayList er_android_transactions_report(string strUserName, int intUserID, int PageSize, int intPage, string strSearch, string strIPAddress, int intDesignationType, string strMacAddress, string strOSDetails, string strIMEINo, string strGcmID, string strAPPVersion, int intSource, int intCreatedByType)
        {
            ArrayList arrlst = new ArrayList();
            try
            {
                objCreateFile.CreateTextFile(strIPAddress, strMacAddress, strOSDetails, strIMEINo, strGcmID, strAPPVersion, intSource.ToString(), intCreatedByType, 115, " er_android_transactions_report", strUserName);
            }
            catch (Exception)
            {


            }
            finally
            {
                try
                {
                    ds = er_Admin_Transactions_Report(strUserName, intUserID, PageSize, intPage, strSearch, strIPAddress, intDesignationType, strMacAddress, strOSDetails, strIMEINo, strGcmID, strAPPVersion, intSource, intCreatedByType);

                    arrlst = objcf.Get_ArrayLlist_From_Dataset(ds);

                }
                catch (Exception)
                {


                }

            }
            return arrlst;

        }

        [WebMethod]
        public ArrayList er_android_fund_receive_report(string strUserName, int intUserID, int PageSize, int intPage, string strSearch, string strIPAddress, int intDesignationType, string strMacAddress, string strOSDetails, string strIMEINo, string strGcmID, string strAPPVersion, int intSource, int intCreatedByType)
        {
            ArrayList arrlst = new ArrayList();
            try
            {
                objCreateFile.CreateTextFile(strIPAddress, strMacAddress, strOSDetails, strIMEINo, strGcmID, strAPPVersion, intSource.ToString(), intCreatedByType, 109, " er_android_fund_receive_report ", strUserName);
            }
            catch (Exception)
            {


            }
            finally
            {
                try
                {
                    ds = er_fund_receive_report(strUserName, intUserID, PageSize, intPage, strSearch, strIPAddress, intDesignationType, strMacAddress, strOSDetails, strIMEINo, strGcmID, strAPPVersion, intSource, intCreatedByType);

                    arrlst = objcf.Get_ArrayLlist_From_Dataset(ds);

                }
                catch (Exception)
                {

                    throw;
                }
            }
            return arrlst;

        }
        /*
        [WebMethod]
        public ArrayList er_android_my_commission_report(string strUserName, int intUserID, int PageSize, int intPage, string strSearch, string strIPAddress, int intDesignationType, string strMacAddress, string strOSDetails, string strIMEINo, string strGcmID, string strAPPVersion, int intSource, int intCreatedByType)
        {
            ArrayList arrlst = new ArrayList();
           
           
                try
                {
                    ds = er_my_commission_report(strUserName, intUserID, PageSize, intPage, strSearch, strIPAddress, intDesignationType, strMacAddress, strOSDetails, strIMEINo, strGcmID, strAPPVersion, intSource, intCreatedByType);

                    arrlst = objcf.Get_ArrayLlist_From_Dataset(ds);

                }
                catch (Exception)
                {


                }
            
            return arrlst;


        }
        */
        [WebMethod]
        public ArrayList er_android_bulktransfer_report(string strUserName, int intUserID, int PageSize, int intPage, string strSearch, string strIPAddress, int intDesignationType, string strMacAddress, string strOSDetails, string strIMEINo, string strGcmID, string strAPPVersion, int intSource, int intCreatedByType, int intUsertype)
        {
            ArrayList arrlst = new ArrayList();
            try
            {
                try
                {
                    //string strUserName, int intUserID, int intTransid, int PageSize, int intPage, string strSearch, string strIPAddress, int intDesignationType, string strMacAddress, string strOSDetails, string strIMEINo, string strGcmID, string strAPPVersion, int intSource, int intCreatedByType

                    ds = er_bulktransfer_report(strUserName, intUserID, intUsertype, PageSize, intPage, strSearch, strIPAddress, intDesignationType, strMacAddress, strOSDetails, strIMEINo, strGcmID, strAPPVersion, intSource, intCreatedByType);
                  
                    arrlst = objcf.Get_ArrayLlist_From_Dataset(ds);

                }
                catch (Exception)
                {
                    throw;

                }
                finally
                {

                }

            }
            catch (Exception)
            {

            }
            finally
            {
                ds.Dispose();
            }
            return arrlst;
        }

        [WebMethod]
        public ArrayList er_android_bulktransfer_details_report(string strUserName, int intUserID, int intTransid, int PageSize, int intPage, string strSearch, string strIPAddress, int intDesignationType, string strMacAddress, string strOSDetails, string strIMEINo, string strGcmID, string strAPPVersion, int intSource, int intCreatedByType)
        {
            ArrayList arrlst = new ArrayList();
            try
            {
                try
                {
                    ds = er_bulktransfer_details_report(strUserName, intUserID, intTransid, PageSize, intPage, strSearch, strIPAddress, intDesignationType, strMacAddress, strOSDetails, strIMEINo, strGcmID, strAPPVersion, intSource, intCreatedByType);

                    arrlst = objcf.Get_ArrayLlist_From_Dataset(ds);

                }
                catch (Exception)
                {


                }
                finally
                {

                }
                return arrlst;
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                ds.Dispose();
            }
        }

        // ActivityType=122
        [WebMethod]
        public DataSet er_emoney_Transaction_Report(string strUserName, int intUserID, int PageSize, int intPage, string strSearch, string strIPAddress, int intDesignationType, string strMacAddress, string strOSDetails, string strIMEINo, string strGcmID, string strAPPVersion, int intSource, int intCreatedByType)
        {
            try
            {
                try
                {
                    objCreateFile.CreateTextFile(strIPAddress, strMacAddress, strOSDetails, strIMEINo, strGcmID, strAPPVersion, intSource.ToString(), intCreatedByType, 122, "er_emoney_Transaction_Report ", strUserName);
                }
                catch (Exception)
                {


                }
                finally
                {
                    strLimit = " LIMIT " + PageSize * (intPage - 1) + "," + PageSize + "";

                    if (intUserID > 0)
                    {
                        int intCount = objODBC.executeScalar_int("select count(1) from er_emoney_transaction c where c.id > 0  " + strSearch + "");

                        strQuery = "select case when c.wallet_id=1 then 'Salaam' when c.wallet_id=2 then 'Etisalat' when c.wallet_id=3 then 'Roshan' when c.wallet_id=5 then 'AWCC' when c.wallet_id=4 then 'Easy Pay' end as Operator,date_format(c.created_on,'%d %b %Y %H:%i:%s') as Date_Time,c.closing_balance as Closing_Balance,c.amt as Amount,case when c.recharge_id=0 then 'NA' when c.recharge_id!=0 then c.recharge_id end as Recharge_ID,case when c.trans_type=1 then 'Add EMoney' when c.trans_type=2 then 'Top Up' when c.trans_type=3 then 'Top Up Reversal' end as Description ,case when c.trans_type=1 then 'Credit' when c.trans_type=2 then 'Debit' when c.trans_type=3 then 'Debit' end as Txn_Type ," + intCount + " as intCount from er_emoney_transaction c where c.id > 0  " + strSearch + " order by c.id desc " + strLimit + "";
                    }


                }
                ds = objODBC.Report(strQuery);

                return ds;
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                ds.Dispose();
            }
        }

        [WebMethod]
        public ArrayList er_android_Wallet_Request_Report(string strUserName, int intUserID, int PageSize, int intPage, string strSearch, string strIPAddress, int intDesignationType, string strMacAddress, string strOSDetails, string strIMEINo, string strGcmID, string strAPPVersion, int intSource, int intCreatedByType)
        {
            ArrayList arrlst = new ArrayList();
            try
            {
                try
                {
                    objCreateFile.CreateTextFile(strIPAddress, strMacAddress, strOSDetails, strIMEINo, strGcmID, strAPPVersion, intSource.ToString(), intCreatedByType, 76, " Wallet Request Report ", strUserName);
                }
                catch (Exception)
                {


                }
                finally
                {
                    if (intUserID > 0)
                    {
                        strLimit = " LIMIT " + PageSize * (intPage - 1) + "," + PageSize + "";

                        int intCOunt = objODBC.executeScalar_int("select count(1) from er_wallet_purchase w inner join er_login l on w.parent_id = l.userid where w.userid =" + intUserID + " " + strSearch + "");

                        strQuery = "select l.username,l.full_name,l.emailid,l.mobile,case when w.payment_mode=1 then 'Cash' when w.payment_mode=2 then 'Bank Transfer' end as payment_mode,w.request_amt,w.rcpt_url,case when w.status=0 then 'Pending' when w.status=1 then 'Confirmed' when w.status=2 then 'Rejected' end as status,w.comment," + intCOunt + " from er_wallet_purchase w inner join er_login l on w.userid = l.userid where w.userid =" + intUserID + " " + strSearch + " order by w.id desc " + strLimit + "";

                        ds = objODBC.Report(strQuery);

                        arrlst = objcf.Get_ArrayLlist_From_Dataset(ds);
                    }
                }
                return arrlst;
            }
            catch (Exception)
            {

                throw;
            }
            finally
            {
                ds.Dispose();
            }
        }

        [WebMethod]
        public ArrayList er_android_topup_Report_for_distrubuter(string strUserName, int intUserID, int PageSize, int intPage, string strSearch, string strIPAddress, int intDesignationType, string strMacAddress, string strOSDetails, string strIMEINo, string strGcmID, string strAPPVersion, int intSource, int intCreatedByType)
        {
            ArrayList arrlst = new ArrayList();
            try
            {
                ds = er_topup_Report_for_distrubuter(strUserName, intUserID, PageSize, intPage, strSearch, strIPAddress, intDesignationType, strMacAddress, strOSDetails, strIMEINo, strGcmID, strAPPVersion, intSource, intCreatedByType);

                arrlst = objcf.Get_ArrayLlist_From_Dataset(ds);

            }
            catch (Exception)
            {


            }
            finally
            {

            }
            return arrlst;

        }


        [WebMethod]
        public ArrayList er_android_Wallet_Transfer_Report(string strUserName, int intUserID, int PageSize, int intPage, string strSearch, string strIPAddress, int intDesignationType, string strMacAddress, string strOSDetails, string strIMEINo, string strGcmID, string strAPPVersion, int intSource, int intCreatedByType)
        {
            ArrayList arrlst = new ArrayList();

            try
            {
                ds = er_Wallet_Transfer_Report(strUserName, intUserID, PageSize, intPage, strSearch, strIPAddress, intDesignationType, strMacAddress, strOSDetails, strIMEINo, strGcmID, strAPPVersion, intSource, intCreatedByType);

                arrlst = objcf.Get_ArrayLlist_From_Dataset(ds);

            }
            catch (Exception)
            {


            }
            finally
            {

            }
            return arrlst;


        }



        // public DataSet er_direct_Report(string strUserName, int intUserID, int PageSize, int intPage, string strSearch, string strIPAddress, int intDesignationType, string strMacAddress, string strOSDetails, string strIMEINo, string strGcmID, string strAPPVersion, int intSource, int intCreatedByType)

        [WebMethod]
        public ArrayList er_android_direct_Report(string strUserName, int intUserID, int PageSize, int intPage, string strSearch, string strIPAddress, int intDesignationType, string strMacAddress, string strOSDetails, string strIMEINo, string strGcmID, string strAPPVersion, int intSource, int intCreatedByType)
        {
            ArrayList arrlst = new ArrayList();
            try
            {
                ds = er_direct_Report(strUserName, intUserID, PageSize, intPage, strSearch, strIPAddress, intDesignationType, strMacAddress, strOSDetails, strIMEINo, strGcmID, strAPPVersion, intSource, intCreatedByType);

                arrlst = objcf.Get_ArrayLlist_From_Dataset(ds);

            }
            catch (Exception)
            {


            }
            finally
            {

            }
            return arrlst;

        }

        [WebMethod]
        public ArrayList er_android_Dispute_Report(string strUserName, int intTranNO, int PageSize, int intPage, string strSearch, string strIPAddress, int intDesignationType, string strMacAddress, string strOSDetails, string strIMEINo, string strGcmID, string strAPPVersion, int intSource, int intCreatedByType)
        {
            ArrayList arls = new ArrayList();

            ds = er_Dispute_Report(strUserName, intTranNO, PageSize, intPage, strSearch, strIPAddress, intDesignationType, strMacAddress, strOSDetails, strIMEINo, strGcmID, strAPPVersion, intSource, intCreatedByType);

            arls = objcf.Get_ArrayLlist_From_Dataset(ds);

            return arls;
        }

        [WebMethod]
        public ArrayList er_android_raised_ticket_Report(string strUserName, int intTranNO, int PageSize, int intPage, string strSearch, string strIPAddress, int intDesignationType, string strMacAddress, string strOSDetails, string strIMEINo, string strGcmID, string strAPPVersion, int intSource, int intCreatedByType)
        {
            ArrayList arls = new ArrayList();

            ds = er_raised_ticket_Report(strUserName, intTranNO, PageSize, intPage, strSearch, strIPAddress, intDesignationType, strMacAddress, strOSDetails, strIMEINo, strGcmID, strAPPVersion, intSource, intCreatedByType);

            arls = objcf.Get_ArrayLlist_From_Dataset(ds);

            return arls;
        }

        [WebMethod]
        public ArrayList er_android_print_ticket_Report(string strUserName, int intTranNO, int PageSize, int intPage, string strSearch, string strIPAddress, int intDesignationType, string strMacAddress, string strOSDetails, string strIMEINo, string strGcmID, string strAPPVersion, int intSource, int intCreatedByType)
        {
            ArrayList arls = new ArrayList();

            ds = er_print_ticket_Report(strUserName, intTranNO, PageSize, intPage, strSearch, strIPAddress, intDesignationType, strMacAddress, strOSDetails, strIMEINo, strGcmID, strAPPVersion, intSource, intCreatedByType);

            arls = objcf.Get_ArrayLlist_From_Dataset(ds);

            return arls;
        }

    }
}
