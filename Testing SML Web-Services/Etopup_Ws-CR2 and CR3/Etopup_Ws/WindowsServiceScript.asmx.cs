﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Services;
using Etopup_Ws.old_App_Code;

namespace Etopup_Ws
{
    /// <summary>
    /// Summary description for WindowsServiceScript
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class WindowsServiceScript : System.Web.Services.WebService
    {

        [WebMethod]
        public string PendingTopupRequest()
        {
            try
            {
                Recharge_pending_request onjRecharge = new Recharge_pending_request();
                onjRecharge.GetPendingRechargeDetailEtisalat();
                return "TRUE";
            }
            catch (Exception)
            {
                return "FALSE";
            }
        }

        [WebMethod]
        public string ReverseTopupRequest()
        {
            try
            {
                reverse_topup_script onjRecharge = new reverse_topup_script();
                onjRecharge.GetPendingRechargeDetails();
                return "TRUE";
            }
            catch (Exception)
            {
                return "FALSE";
            }
        }


        [WebMethod]
        public string Group_topup_10mScript()
        {
            try
            {
                er_group_topup_10mscript onjRecharge = new er_group_topup_10mscript();
                onjRecharge.getGroupTopup();
                return "TRUE";
            }
            catch (Exception)
            {
                return "FALSE";
            }
        }

        [WebMethod]
        public string ActiveUserScript()
        {
            try
            {
                ODBC_LOG objODBC_LOG = new ODBC_LOG();
                DateFormat objDT = new DateFormat();
                ODBC objODBC = new ODBC();
                try
                {
                    objODBC_LOG.executeNonQuery("TRUNCATE TABLE `er_no_activity_perform`;");
                    objODBC_LOG.executeNonQuery("call sp_getActivitybyUser('" + objDT.getCurDateTimeString().ToString() + "')");
                }
                catch (Exception)
                { }

               
                try
                {
                    try
                    {
                        DataTable dt = new DataTable();
                        int intDays = Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["Retailer_inactive_script_days_condtion"]);
                        string strDate = objDT.getCurDateTimeString();
                        dt = objODBC.getDataTable("select l.userid from er_login l inner join er_wallet_purchase p on l.userid != p.userid where l.usertype_id=5 and l.Active=1 and DATEDIFF('" + strDate + "',l.created_on ) >" + intDays + " order by l.userid asc");
                        for (int i = 0; i < dt.Rows.Count; i++)
                        {
                            objODBC.executeNonQuery("update er_login set Active = 2 where userid =" + Convert.ToInt32(dt.Rows[i][0]) + "");
                        }
                    }
                    catch (Exception)
                    {

                      
                    }
                }
                catch (Exception)
                { }

                return "TRUE";
            }
            catch (Exception)
            {
                return "FALSE";
            }
        }
    }
}
