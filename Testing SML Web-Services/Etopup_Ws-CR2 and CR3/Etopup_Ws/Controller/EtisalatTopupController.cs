﻿using System;
using System.Net.Http;
using System.Web.Http;
using System.Xml.Linq;
using Etopup_Ws.old_App_Code;

using System.Threading.Tasks;


namespace Etopup_Ws.Controller
{
    public class EtisalatTopupController : ApiController
    {
       
        ODBC objODBC = new ODBC();
       
     
        // POST: api/EtisalatBody
        [HttpPost]
        public async Task<HttpResponseMessage> EtisalatTopup()
        {
            string strQuerry = await Request.Content.ReadAsStringAsync();
            HttpResponseMessage response = new HttpResponseMessage();
            int intCount = 0;
            try
            {
                await Task.Run(() =>
                {
                    // String STR = objODBC.executeScalar_str(strQuerry);
                    intCount = objODBC.executeNonQuery(strQuerry);
                    response.Content = new StringContent(intCount.ToString());
                    return response;
                });
            }
            catch (Exception)
            {

            }
            response.Content = new StringContent(intCount.ToString());
            return response;
        }

    }
}