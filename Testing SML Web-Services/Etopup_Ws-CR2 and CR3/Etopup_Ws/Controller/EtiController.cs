﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Xml.Linq;
using Etopup_Ws.old_App_Code;
using Etopup_Ws.Models;
using System.Xml;
using System.Xml.Serialization;
using System.Web;
using System.Threading.Tasks;


namespace Etopup_Ws.Controller
{
    public class EtiController : ApiController
    {
        ODBC objODBC = new ODBC();


        // POST: api/EtisalatBody
        [HttpPost]
        public async Task<HttpResponseMessage> PostRawBufferManual()
        {
            string result = await Request.Content.ReadAsStringAsync();
            HttpResponseMessage response = new HttpResponseMessage();
            try
            {
                objODBC.executeNonQuery("INSERT INTO temp(data,type,request) values('Etisalat request Api Hit',1,'" + result + "')");
            }
            catch (Exception)
            {

            }

            result = "<Request>" + result + "</Request>";
            string strPhone = "", strUSSD = "", strCDate = "", strSign = "";

            XmlDocument xmlResponse = new XmlDocument();
            XmlDocument xmlRequest = new XmlDocument();
            try
            {
                xmlRequest.LoadXml(result.ToString());



                try
                {
                    strPhone = xmlRequest.DocumentElement.ChildNodes[0].ChildNodes[0].Value.ToString();
                }
                catch (Exception)
                {

                }
                try
                {
                    strUSSD = xmlRequest.DocumentElement.ChildNodes[1].ChildNodes[0].Value.ToString();
                }
                catch (Exception)
                {

                }
                try
                {
                    strCDate = xmlRequest.DocumentElement.ChildNodes[2].ChildNodes[0].Value.ToString();
                }
                catch (Exception)
                {

                }
                try
                {
                    strSign = xmlRequest.DocumentElement.ChildNodes[3].ChildNodes[0].Value.ToString();
                }
                catch (Exception)
                {

                }

                string strData = "Phone:" + strPhone + ",ussd:" + strUSSD + ",cdate:" + strCDate + ",sign:" + strSign;
                try
                {
                    objODBC.executeNonQuery("INSERT INTO temp(data,type,request) values('Etisalat request Api Data',3,'" + strData + "')");
                }
                catch (Exception)
                {

                }


                XmlNode docNode = xmlResponse.CreateXmlDeclaration("1.0", "UTF-8", null);
                xmlResponse.AppendChild(docNode);

                XmlNode Request = xmlResponse.CreateElement("Response");
                xmlResponse.AppendChild(Request);

                XmlNode Status = xmlResponse.CreateElement("Status");
                Status.AppendChild(xmlResponse.CreateTextNode("0"));
                Request.AppendChild(Status);

                XmlNode phone = xmlResponse.CreateElement("phone");
                phone.AppendChild(xmlResponse.CreateTextNode(strPhone));
                Request.AppendChild(phone);


                XmlNode message = xmlResponse.CreateElement("message");
                message.AppendChild(xmlResponse.CreateTextNode("Balance is ___"));
                Request.AppendChild(message);

                XmlNode act = xmlResponse.CreateElement("act");
                act.AppendChild(xmlResponse.CreateTextNode("0"));
                Request.AppendChild(act);


                objODBC.executeNonQuery("INSERT INTO `er_etisalat_request`(`phone`, `ussd`, `cdate`, `sign`,`response`) VALUES ('" + strPhone + "','" + strUSSD + "','" + strCDate + "','" + strSign + "','" + xmlResponse.InnerXml + "')");


                response.Content = new StringContent(xmlResponse.InnerXml);

                try
                {
                    objODBC.executeNonQuery("INSERT INTO temp(data,type,request) values('Etisalat request Api Output',4,'" + xmlResponse.InnerXml + "')");
                }
                catch (Exception)
                {

                }


            }
            catch (Exception e2)
            {

            }

            return response;
        }

    }

}
