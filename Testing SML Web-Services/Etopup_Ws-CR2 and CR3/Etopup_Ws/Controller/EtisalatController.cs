﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Xml.Linq;
using Etopup_Ws.old_App_Code;
using Etopup_Ws.Models;
using System.Xml;
using System.Xml.Serialization;
using System.Web;
using System.Threading.Tasks;
using System.Collections;
using System.Data;
using System.Text.RegularExpressions;

namespace Etopup_Ws.Controller
{
    public class EtisalatController : ApiController
    {
        DateFormat objDF = new DateFormat();
        ODBC objODBC = new ODBC();
        ODBC_LOG objODBCLOG = new ODBC_LOG();
        DataTable dt = new DataTable();
        DataTable dt2 = new DataTable();
        DateFormat objDT = new DateFormat();
        Encrypt_Decrypt ED = new Encrypt_Decrypt();
        CreateFile objCreateFile = new CreateFile();
        SMPP_SMS_Format objSMPPSMS = new SMPP_SMS_Format();
        public string FundTransferMessage = "NA";



        // POST: api/EtisalatBody
        [HttpPost]
        public async Task<HttpResponseMessage> PostRawBufferManual()
        {
            string result = await Request.Content.ReadAsStringAsync();
            HttpResponseMessage response = new HttpResponseMessage();
            //try
            //{
            //    objODBC.executeNonQuery("INSERT INTO temp(data,type,request) values('Etisalat request Api Hit',1,'" + result + "')");
            //}
            //catch (Exception)
            //{

            //}

            result = "<Request>" + result + "</Request>";
            string strPhone = "", strUSSD = "", strCDate = "", strSign = "";

            XmlDocument xmlResponse = new XmlDocument();
            XmlDocument xmlRequest = new XmlDocument();
            try
            {
                xmlRequest.LoadXml(result.ToString());



                try
                {
                    strPhone = xmlRequest.DocumentElement.ChildNodes[0].ChildNodes[0].Value.ToString();

                    if (strPhone.Length == 11)
                    {
                        strPhone = strPhone.Substring(2, strPhone.Length - 2); // Truncate From Position of Words
                    }
                }
                catch (Exception)
                {

                }
                try
                {
                    strUSSD = xmlRequest.DocumentElement.ChildNodes[1].ChildNodes[0].Value.ToString();
                }
                catch (Exception)
                {

                }
                try
                {
                    strCDate = xmlRequest.DocumentElement.ChildNodes[2].ChildNodes[0].Value.ToString();
                }
                catch (Exception)
                {

                }
                try
                {
                    strSign = xmlRequest.DocumentElement.ChildNodes[3].ChildNodes[0].Value.ToString();
                }
                catch (Exception)
                {

                }

                string strData = "Phone:" + strPhone + ",ussd:" + strUSSD + ",cdate:" + strCDate + ",sign:" + strSign;

                try
                {

                    string strURL = strData;
                    string str_short_code;

                    string strUSSDSHCode = "";
                    string strOperatorCode = "";
                    string strMPin = "";
                    int intApiType = 0;
                    string strCoustomerMobileNumber = "NA";
                    string strRechargeAmount = "0";
                    string[] strParse = strUSSD.Split(new char[] { '*' });
                    string strOperatorTableID = "0";
                    string strUSSDResponseMessage = "";

                    if (strParse.Length.ToString() == "4") // balance for Operater Code
                    {
                        str_short_code = strParse[0].ToString();
                        strUSSDSHCode = strParse[1].ToString();
                        strOperatorCode = strParse[2].ToString();
                        strMPin = strParse[3].Replace("#", "").ToString();
                        try
                        {
                            if (strOperatorCode == "1") // Fetch Balance // *543*1*<MPin>#
                            {
                                string strUSSDOperationFunction = USSDGetbalance(strPhone, strMPin, int.Parse(strOperatorCode), strSign, str_short_code, strUSSD, strCDate, strURL, strCoustomerMobileNumber, strRechargeAmount);
                                string[] strResposneDataUSSDOperation = strUSSDOperationFunction.Split(new char[] { ',' }); // 8 Length
                                                                                                                            //  strResposne = "1,TRUE,NA," + strUserID + "," + strUserName + "," + strBalance.ToString() + ",Successfully Fetch";
                                string strStatus = strResposneDataUSSDOperation[0]; //Status Message 1=Success,2:Failed                           
                                string strStatusMessage = strResposneDataUSSDOperation[1]; //Status Message TRUE/FALSE
                                string strTransactionID = strResposneDataUSSDOperation[2];
                                string strUserID = strResposneDataUSSDOperation[3];
                                string strUserName = strResposneDataUSSDOperation[4];
                                string strBalance = strResposneDataUSSDOperation[5];
                                string strOtherMessage = strResposneDataUSSDOperation[6];
                                strOperatorTableID = strResposneDataUSSDOperation[7];
                                if (strStatus == "1" && strStatusMessage == "TRUE")
                                {
                                    strUSSDResponseMessage = objSMPPSMS.USSDTopupBalance(objODBC.executeScalar_str("SELECT username From er_login Where userid='" + strUserID + "'").ToString(), strUserName, double.Parse(strBalance));
                                }
                                else
                                {
                                    strUSSDResponseMessage = strOtherMessage + "Thanks for using Setaragan Top-up !";
                                }
                            }
                            if (strOperatorCode == "3") // LastFiveTransaction // *543*3*<MPin>#
                            {
                                ArrayList arrayList = new ArrayList();
                                arrayList = getLast5Transaction(strPhone, strMPin, int.Parse(strOperatorCode), strURL);
                                if (arrayList[0].ToString() == "TRUE")
                                {
                                    strUSSDResponseMessage = "Tnx Details :\n";
                                    for (int i = 1; i < arrayList.Count; i++)
                                    {
                                        strUSSDResponseMessage = strUSSDResponseMessage + arrayList[i].ToString();
                                    }
                                    //  strUSSDResponseMessage = strUSSDResponseMessage + "Thanks for using Setaragan Top-up !";
                                }
                                else
                                {
                                    strUSSDResponseMessage = arrayList[1].ToString() + ". Thanks for using Setaragan Top-up !";
                                }
                            }
                            if (strOperatorCode == "9") // CheckTotalSale // *543*9*<MPin>#
                            {
                                string strUSSDOperationFunction = USSDTopupSale(strPhone, strMPin, int.Parse(strOperatorCode), strSign, str_short_code, strUSSD, strCDate, strURL, strCoustomerMobileNumber, strRechargeAmount);
                                string[] strResposneDataUSSDOperation = strUSSDOperationFunction.Split(new char[] { ',' }); // 8 Length
                                                                                                                            //  strResposne = "1,TRUE,NA," + strUserID + "," + strUserName + "," + strBalance.ToString() + ",Successfully Fetch";
                                string strStatus = strResposneDataUSSDOperation[0]; //Status Message 1=Success,2:Failed                           
                                string strStatusMessage = strResposneDataUSSDOperation[1]; //Status Message TRUE/FALSE
                                string strTransactionID = strResposneDataUSSDOperation[2];
                                string strUserID = strResposneDataUSSDOperation[3];
                                string strUserName = strResposneDataUSSDOperation[4];
                                string strBalance = strResposneDataUSSDOperation[5];
                                string strOtherMessage = strResposneDataUSSDOperation[6];
                                string strUserTypeID = strResposneDataUSSDOperation[7];
                                if (strStatus == "1" && strStatusMessage == "TRUE")
                                {
                                    strUSSDResponseMessage = objSMPPSMS.USSDTopupSaleSMS(objODBC.executeScalar_str("SELECT username From er_login Where userid='" + strUserID + "'").ToString(), strUserName, double.Parse(strBalance), strUserTypeID);
                                }
                                else
                                {
                                    strUSSDResponseMessage = strOtherMessage + "Thanks for using Setaragan Top-up !";
                                }
                            }

                        }
                        catch (Exception)
                        {

                        }
                    }
                    else if (strParse.Length.ToString() == "6")
                    {
                        str_short_code = strParse[0].ToString();
                        strUSSDSHCode = strParse[1].ToString();
                        strOperatorCode = strParse[2].ToString();
                        strCoustomerMobileNumber = strParse[3].ToString();
                        strRechargeAmount = strParse[4].ToString();
                        strMPin = strParse[5].Replace("#", "").ToString();


                        if (strOperatorCode == "2") // *543*2*<strCoustomerMobileNumber>*<Amt>*<MPin># // Topup Here OpID=2
                        {
                            string strUSSDOperationFunction = USSDTopupFunction(strPhone, strMPin, int.Parse(strOperatorCode), strSign, str_short_code, strUSSD, strCDate, strURL, strCoustomerMobileNumber, strRechargeAmount);
                            string[] strResposneDataUSSDOperation = strUSSDOperationFunction.Split(new char[] { ',' }); // 8 Length
                                                                                                                        //  strResposne = "1,TRUE,NA," + strUserID + "," + strUserName + "," + strBalance.ToString() + ",Successfully Fetch";
                            string strStatus = strResposneDataUSSDOperation[0]; //Status Message 1=Success,2:Failed                           
                            string strStatusMessage = strResposneDataUSSDOperation[1]; //Status Message TRUE/FALSE
                            string strTransactionID = strResposneDataUSSDOperation[2];
                            string strUserID = strResposneDataUSSDOperation[3];
                            string strUserName = strResposneDataUSSDOperation[4];
                            string strBalance = strResposneDataUSSDOperation[5];
                            string strOtherMessage = strResposneDataUSSDOperation[6];
                            strOperatorTableID = strResposneDataUSSDOperation[7];
                            if (strStatus == "1" && strStatusMessage == "TRUE")
                            {
                                //  strResposne = "1,TRUE," + strTransactionID + "," + strUserID + "," + strUserName + "," + strBalance + ",Top-Up successfully done," + intUSSD_ID.ToString();

                                strUSSDResponseMessage = "Status:SUCCESS,TnxID:" + strTransactionID + ",AMT:" + strRechargeAmount + "AFN,Mob:" + strCoustomerMobileNumber + "& Current Bal:" + strBalance + "AFN.";
                            }
                            else
                            {
                                if (strOtherMessage == "PENDING")
                                {
                                    strUSSDResponseMessage = "Status:INPROCESS,TnxID:" + strTransactionID + ",AMT:" + strRechargeAmount + "AFN,Mob:" + strCoustomerMobileNumber + "& Current Bal:" + strBalance + "AFN.";
                                }
                                else
                                {
                                    strUSSDResponseMessage = strOtherMessage.Replace("!", "").ToString() + ". Thanks for using Setaragan Top-up !";
                                }
                            }
                        }
                        if (strOperatorCode == "6")  // *543*6*<strCoustomerMobileNumber>*<Amt>*<MPin># // Fund Transfer Here OpID=6
                        {
                            string strUSSDOperationFunction = USSDFundTransferFunction(strPhone, strMPin, int.Parse(strOperatorCode), strURL, strCoustomerMobileNumber, strRechargeAmount);


                            string[] strResposneDataUSSDOperation = strUSSDOperationFunction.Split(new char[] { ',' }); // 8 Length
                          //   strMessage = "1,TRUE," + dblSenderBalance + "," + intSenderID + "," + intUserID + "," + dblTransferAmount + ",Wallet fund successfully transferred ";
                            string strStatus = strResposneDataUSSDOperation[0]; //Status Message 1=Success,2:Failed                           
                            string strStatusMessage = strResposneDataUSSDOperation[1]; //Status Message TRUE/FALSE
                            string strUserBalance = strResposneDataUSSDOperation[2];
                            string intSenderUSerID = strResposneDataUSSDOperation[3];
                            string strUserID = strResposneDataUSSDOperation[4];
                            string strTransferedAmount = strResposneDataUSSDOperation[5];
                            string strOtherMessage = strResposneDataUSSDOperation[6];
                            if (strStatus == "1" && strStatusMessage == "TRUE")
                            {
                                //  strResposne = "1,TRUE," + strTransactionID + "," + strUserID + "," + strUserName + "," + strBalance + ",Top-Up successfully done," + intUSSD_ID.ToString();
                              
                                    strUSSDResponseMessage = strOtherMessage + ",Current Bal:" + strUserBalance + " AFN";

                            }
                            else
                            {
                                strUSSDResponseMessage = strOtherMessage.Replace("!", "").ToString();
                            }
                        }

                        if (strOperatorCode == "8") // *543*8*<New-Mpin>*<New-Mpin_Confirm>*<Old-MPin># // Topup Here OpID=2
                        {
                            
                            string strUSSDOperationFunction = USSDChangeMPin(strPhone, strMPin, int.Parse(strOperatorCode), strSign, str_short_code, strUSSD, strCDate, strURL, strCoustomerMobileNumber, strRechargeAmount);
                            string[] strResposneDataUSSDOperation = strUSSDOperationFunction.Split(new char[] { ',' }); // 8 Length
                                                                                                                        //  strResposne = "1,TRUE,NA," + strUserID + "," + strUserName + "," + strBalance.ToString() + ",Successfully Fetch";
                            string strStatus = strResposneDataUSSDOperation[0]; //Status Message 1=Success,2:Failed                           
                            string strStatusMessage = strResposneDataUSSDOperation[1]; //Status Message TRUE/FALSE
                            string strTransactionID = strResposneDataUSSDOperation[2];
                            string strUserID = strResposneDataUSSDOperation[3];
                            string strUserName = strResposneDataUSSDOperation[4];
                            string strBalance = strResposneDataUSSDOperation[5];
                            string strOtherMessage = strResposneDataUSSDOperation[6];
                            strOperatorTableID = strResposneDataUSSDOperation[7];
                            if (strStatus == "1" && strStatusMessage == "TRUE")
                            {
                                //  strResposne = "1,TRUE," + strTransactionID + "," + strUserID + "," + strUserName + "," + strBalance + ",Top-Up successfully done," + intUSSD_ID.ToString();

                                strUSSDResponseMessage = strOtherMessage;
                            }
                            else
                            {

                                strUSSDResponseMessage = strOtherMessage.Replace("!", "").ToString() + ". Thanks for using Setaragan Top-up !";

                            }
                        }
                        if (strOperatorCode == "7")  // *543*7*<strTopupTransactionID>*<Amt>*<MPin># // Reverse Topup Here OpID=7
                        {
                            string strUSSDOperationFunction = USSDReverseTopupFunction(strPhone, strMPin, int.Parse(strOperatorCode), strSign, str_short_code, strUSSD, strCDate, strURL, strCoustomerMobileNumber, strRechargeAmount);


                            string[] strResposneDataUSSDOperation = strUSSDOperationFunction.Split(new char[] { ',' }); // 8 Length
                                                                                                                        //  strResposne = "1,TRUE,NA," + strUserID + "," + strUserName + "," + strBalance.ToString() + ",Successfully Fetch";
                            string strStatus = strResposneDataUSSDOperation[0]; //Status Message 1=Success,2:Failed                           
                            string strStatusMessage = strResposneDataUSSDOperation[1]; //Status Message TRUE/FALSE
                            string strTransactionID = strResposneDataUSSDOperation[2];
                            string strUserBalance = strResposneDataUSSDOperation[3];
                            string strSenderUserID = strResposneDataUSSDOperation[4];
                            string strReceiverUserID = strResposneDataUSSDOperation[5];
                            string strOtherMessage = strResposneDataUSSDOperation[6];
                            if (strStatus == "1" && strStatusMessage == "TRUE")
                            {
                                //  strResposne = "1,TRUE," + strTransactionID + "," + strUserID + "," + strUserName + "," + strBalance + ",Top-Up successfully done," + intUSSD_ID.ToString();
                                if (FundTransferMessage != "NA")
                                {
                                    strUSSDResponseMessage = FundTransferMessage;
                                }
                                else
                                {
                                    strUSSDResponseMessage = strOtherMessage + ". Your Current Bal is " + strUserBalance + " AFN,Thanks for using Setaragan Top-up !";
                                }

                            }
                            else
                            {
                                strUSSDResponseMessage = strOtherMessage.Replace("!", "").ToString() + ". Thanks for using Setaragan Top-up !";
                            }
                        }
                    }
                    else if (strParse.Length.ToString() == "5") // Check Transaction By Mobile number and TransactionID
                    {
                        strUSSDSHCode = strParse[1].ToString();
                        strOperatorCode = strParse[2].ToString();
                        strCoustomerMobileNumber = strParse[3].ToString();
                        strMPin = strParse[4].Replace("#", "").ToString();


                        if (strOperatorCode == "4") //Check Transaction By Mobile Numebr  *543*4*795212454*12345#
                        {
                            ArrayList arrayList = new ArrayList();
                            arrayList = getMobileTransactionDetails(strPhone, strMPin, strCoustomerMobileNumber, int.Parse(strOperatorCode), strURL);
                            if (arrayList[0].ToString() == "TRUE")
                            {
                                strUSSDResponseMessage = "Tnx Details :\n";
                                for (int i = 1; i < arrayList.Count; i++)
                                {
                                    strUSSDResponseMessage = strUSSDResponseMessage + arrayList[i].ToString();
                                }
                                //    strUSSDResponseMessage = strUSSDResponseMessage + "Thanks for using Setaragan Top-up !";
                            }
                            else
                            {
                                strUSSDResponseMessage = arrayList[1].ToString() + ". Thanks for using Setaragan Top-up !";
                            }
                        }
                        else if (strOperatorCode == "5") //Check Transaction By Transaction ID *543*5*9554*12345#
                        {
                            ArrayList arrayList = new ArrayList();
                            arrayList = getTransactionIDTransactionDetails(strPhone, strMPin, strCoustomerMobileNumber, int.Parse(strOperatorCode), strURL);
                            if (arrayList[0].ToString() == "TRUE")
                            {
                                strUSSDResponseMessage = "Tnx Details :\n";
                                for (int i = 1; i < arrayList.Count; i++)
                                {
                                    strUSSDResponseMessage = strUSSDResponseMessage + arrayList[i].ToString();
                                }
                                //    strUSSDResponseMessage = strUSSDResponseMessage + "Thanks for using Setaragan Top-up !";
                            }
                            else
                            {
                                strUSSDResponseMessage = arrayList[1].ToString() + ". Thanks for using Setaragan Top-up !";
                            }
                        }
                    }
                    else if (strParse.Length.ToString() == "7") // Check Transaction By Mobile number and TransactionID
                    {
                        str_short_code = strParse[1].ToString();
                        strOperatorCode = strParse[2].ToString();
                        strCoustomerMobileNumber = strParse[3].ToString();
                        intApiType = int.Parse(strParse[4].ToString());
                        strRechargeAmount = strParse[5].ToString();
                        strMPin = strParse[6].Replace("#", "").ToString();

                        if (strOperatorCode == "2") // *543*2*<strCoustomerMobileNumber>*<ApiType>*<Amt>*<MPin># // Topup Here OpID=2
                        {
                            //string strUSSDOperationFunction = USSDTopupFunctionOtherOperator(strPhone, strMPin, int.Parse(strOperatorCode), strSign, str_short_code, strUSSD, strCDate, strURL, strCoustomerMobileNumber, strRechargeAmount, intApiType);
                            //string[] strResposneDataUSSDOperation = strUSSDOperationFunction.Split(new char[] { ',' }); // 8 Length
                            //                                                                                            //  strResposne = "1,TRUE,NA," + strUserID + "," + strUserName + "," + strBalance.ToString() + ",Successfully Fetch";
                            //string strStatus = strResposneDataUSSDOperation[0]; //Status Message 1=Success,2:Failed                           
                            //string strStatusMessage = strResposneDataUSSDOperation[1]; //Status Message TRUE/FALSE
                            //string strTransactionID = strResposneDataUSSDOperation[2];
                            //string strUserID = strResposneDataUSSDOperation[3];
                            //string strUserName = strResposneDataUSSDOperation[4];
                            //string strBalance = strResposneDataUSSDOperation[5];
                            //string strOtherMessage = strResposneDataUSSDOperation[6];
                            //strOperatorTableID = strResposneDataUSSDOperation[7];
                            //if (strStatus == "1" && strStatusMessage == "TRUE")
                            //{
                            //    //  strResposne = "1,TRUE," + strTransactionID + "," + strUserID + "," + strUserName + "," + strBalance + ",Top-Up successfully done," + intUSSD_ID.ToString();

                            //    strUSSDResponseMessage = "Dear " + strUserName + ",Status:'" + strOtherMessage + "',TnxID:" + strTransactionID + ",amount:" + strRechargeAmount + "AFN,Mobile:" + strCoustomerMobileNumber + ",Current Bal:" + strBalance + "AFN.";
                            //}
                            //else
                            //{
                            //    //  strUSSDResponseMessage = strOtherMessage.Replace("!", "").ToString() + ". Thanks for using Setaragan Top-up !";
                            //    strUSSDResponseMessage = "Dear " + strUserName + ",Status:'" + strOtherMessage + "',TnxID:" + strTransactionID + ",amount:" + strRechargeAmount + "AFN,Mobile:" + strCoustomerMobileNumber + ",Current Bal:" + strBalance + "AFN.";
                            //}
                            strUSSDResponseMessage = "Invalid USSD code ,Thanks for using Setaragan Top-up !";
                        }
                    }
                    else
                    {
                        strUSSDResponseMessage = "Invalid USSD code ,Thanks for using Setaragan Top-up !";
                    }

                    if (strUSSDResponseMessage == "")
                    {
                        strUSSDResponseMessage = "Invalid USSD Code. Thanks for using Setaragan Top-up !";
                    }

                    XmlNode docNode = xmlResponse.CreateXmlDeclaration("1.0", "UTF-8", null);
                    xmlResponse.AppendChild(docNode);

                    XmlNode Request = xmlResponse.CreateElement("Response");
                    xmlResponse.AppendChild(Request);

                    XmlNode Status = xmlResponse.CreateElement("Status");
                    Status.AppendChild(xmlResponse.CreateTextNode("0"));
                    Request.AppendChild(Status);

                    XmlNode phone = xmlResponse.CreateElement("phone");
                    phone.AppendChild(xmlResponse.CreateTextNode(strPhone));
                    Request.AppendChild(phone);


                    XmlNode message = xmlResponse.CreateElement("message");
                    message.AppendChild(xmlResponse.CreateTextNode(strUSSDResponseMessage));
                    Request.AppendChild(message);

                    XmlNode act = xmlResponse.CreateElement("act");
                    act.AppendChild(xmlResponse.CreateTextNode("0"));
                    Request.AppendChild(act);


                    response.Content = new StringContent(xmlResponse.InnerXml);


                    if (strOperatorTableID != "0")
                    {
                        objODBC.executeNonQuery("update `er_etisalat_request_ussd` set `response`='" + xmlResponse.InnerXml + "' where id='" + strOperatorTableID + "'");
                    }

                    try
                    {
                        string strDate = objDT.getCurDateTimeString();
                        objODBC.executeNonQuery("INSERT INTO `er_ussd_request_all`(`request`, `response`, `created_on`, `operator_id`) VALUES ('" + result.ToString() + "','" + xmlResponse.InnerXml.ToString() + "','" + strDate + "',2)");
                    }
                    catch (Exception)
                    {

                    }

                }
                catch (Exception)
                {

                }

            }
            catch (Exception e2)
            {

            }

            return response;
        }

        public string USSDChangeMPin(string strPhone, string strMPin, int strOperatorCode, string strSign, string str_short_code, string strUSSD, string strCDate, string strURL,  string strNew_M_Pin, string strNew_Confirm_M_Pin)
        {
            //   strResponse = "1,TRUE,LoginSuccessfull,"+ intUserID +","+ intDesignationType +"";
            // strResponse = "2,FALSE,Invalid Account Number,NA,NA";

        string strResposne = string.Empty;

            if (strNew_M_Pin == strNew_Confirm_M_Pin)
            {
                if (strNew_Confirm_M_Pin.Length == 4)
                {
                    string strRequestData = Login(strPhone, strMPin, strURL);
                    int intInvestmentID = 0;
                    string[] strData = strRequestData.Split(new char[] { ',' });
                    string strStatus = strData[0]; // 1:True & 2: False
                    string strStatusData = strData[1];
                    string strMessage = strData[2];
                    string strUserID = strData[3];
                    string strUserName = strData[4];
                    if (strStatus == "1" && strStatusData == "TRUE")
                    {
                        if (strOperatorCode == 8) //  Change M-Pin
                        {

                            DataSet dsData = new DataSet();
                            try
                            {

                                dsData = objODBC.getDataSet("SELECT m.`mobile`,m.`operator_id`,l.pass_key FROM `er_mobile` m inner join er_login l on m.userid=l.userid WHERE m.userid='" + strUserID + "' and m.usertype='" + 2 + "' and m.mob_type=1");
                                if (dsData.Tables[0].Rows.Count > 0)
                                {
                                    string strMobile = dsData.Tables[0].Rows[0][0].ToString();
                                    int intOperaterID = int.Parse(dsData.Tables[0].Rows[0][1].ToString());
                                    string strPassKey = dsData.Tables[0].Rows[0][2].ToString();

                                    // UpdateMpin
                                    Encryption objEncrypt = new Encryption();
                                    string strSMSMPin = strNew_Confirm_M_Pin;
                                    string MPIN = strSMSMPin;

                                    strSMSMPin = objEncrypt.EncryptQueryString(MPIN, strPassKey);
                                    objODBC.executeNonQuery("update er_login set M_Pin='" + strSMSMPin + "' where userid='" + strUserID + "'");
                                    strResposne = "1,TRUE,NA," + strUserID + ",NA,NA,M-Pin has been changed Successfully,Thanks for using Setaragan Top-up !,0";
                                    // SMPP SMS Code Below
                                    SMPP_SMS_Format objSMPP_SMS_Format = new SMPP_SMS_Format();
                                    string strGetSMS = objSMPP_SMS_Format.GetForgotMPIN(strUserName, objODBC.executeScalar_str("SELECT full_name From er_login Where userid='" + strUserID + "'").ToString(), strMobile, MPIN).ToString();

                                    SMPP1 objSMPP1 = new SMPP1();
                                    string strSMPPResponse = "NA";
                                    DataTable dtSMPP = new DataTable();
                                    try
                                    {
                                        CommonFunction objComFUN = new CommonFunction();
                                        objComFUN.er_SMS_Alert(int.Parse(strUserID), 2, strMobile, intOperaterID, strGetSMS, 1, strSMPPResponse);

                                    }
                                    catch (Exception)
                                    {

                                    }
                                    finally
                                    {
                                        dtSMPP.Dispose();

                                    }
                                }
                                else
                                {

                                    strResposne = "2,FALSE,NA," + strUserID + ",NA,NA,Kindly Enter Valid Details,0";
                                }

                            }
                            catch (Exception)
                            {
                                strResposne = "2,FALSE,NA," + strUserID + ",NA,NA,Invalid USSD code,0";
                            }
                        }
                        else
                        {

                            strResposne = "2,FALSE,NA," + strUserID + ",NA,NA,Invalid USSD code,0";
                        }
                    }
                    else
                    {
                        strResposne = "2,FALSE,NA," + strUserID + ",NA,NA," + strMessage + ",0";
                    }
                }
                else
                {
                    strResposne = "2,FALSE,NA,NA,NA,NA,Kindly Enter M-Pin With 4 character,0";

                }
            }
            else
            {
                strResposne = "2,FALSE,NA,NA,NA,NA,New M-Pin and Confirm M-Pin does not match,0";                
            }
            return strResposne;
        }
        public string USSDGetbalance(string str_msisdn, string strMPINPassword, int intOperationID, string strCoustomerMobileNUmber, string str_dlg_id, string str_short_code, string str_resp_text, string str_req_text, string str_session_id, string str_concat_req_string, string str_date_time, string str_circle_id, string str_dcs, string str_curr_menu, string strURL, string strCoustomerMobile, string strRechargeAmount)
        {
            //   strResponse = "1,TRUE,LoginSuccessfull,"+ intUserID +","+ intDesignationType +"";
            // strResponse = "2,FALSE,Invalid Account Number,NA,NA";

            string strResposne = string.Empty;
            string strRequestData = Login(str_msisdn, strMPINPassword, strURL);
            int intInvestmentID = 0;
            string[] strData = strRequestData.Split(new char[] { ',' });
            string strStatus = strData[0]; // 1:True & 2: False
            string strStatusData = strData[1];
            string strMessage = strData[2];
            string strUserID = strData[3];
            string strUserName = strData[4];
            if (strStatus == "1" && strStatusData == "TRUE")
            {
                purchase_wallet1 objWallet = new purchase_wallet1();
                // strResposne = "1,TRUE,Message,"+ strUserID+","+ intInvestmentID+",OperaterID";
                if (intOperationID == 1) // Balance
                {

                    double strBalance = objWallet.GetWalletAmount(Convert.ToInt32(strUserID));

                    if (strBalance <= 0)
                    {
                        strBalance = 0.0;
                    }
                    strResposne = "1,TRUE,NA," + strUserID + "," + strUserName + "," + strBalance.ToString() + ",Successfully Fetch,0";
                }
                else
                {

                    strResposne = "2,FALSE,NA," + strUserID + ",NA,NA,Invalid USSD code,0";
                }
            }
            else
            {
                strResposne = "2,FALSE,NA," + strUserID + ",NA,NA," + strMessage + ",0";
            }
            return strResposne;
        }

        public ArrayList getLast5Transaction(string strPhone, string strMPINPassword, int intOperationID, string strURL)
        {
            ArrayList arrayList = new ArrayList();
            string strResposne = string.Empty;
            string strRequestData = Login(strPhone, strMPINPassword, strURL);
            string[] strData = strRequestData.Split(new char[] { ',' });
            string strStatus = strData[0]; // 1:True & 2: False
            string strStatusData = strData[1];
            string strMessage = strData[2];
            string strUserID = strData[3];
            string strUserName = strData[4];

            if (strStatus == "1" && strStatusData == "TRUE")
            {
                if (intOperationID == 3) //  Last Five Transaction 
                {

                    DataSet ds = new DataSet();

                    try
                    {
                        ds = objODBC.getDataSet("select trans_number,mobile_number,amount,CASE WHEN status=1 THEN 'pending' WHEN status=2 THEN 'success' WHEN status=3 THEN 'Fail' ELSE 'Pending' END as Status,created_on from er_recharge where userid='" + strUserID + "' order by id desc limit 2 ");
                        if (ds.Tables[0].Rows.Count > 0)
                        {
                            arrayList.Add("TRUE");
                            for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                            {
                                //arrayList.Add("TNX ID:" + ds.Tables[0].Rows[i][0].ToString() + ",");
                                arrayList.Add("Mob.No.:" + ds.Tables[0].Rows[i][1].ToString() + ",");
                                arrayList.Add("AMT:" + ds.Tables[0].Rows[i][2].ToString() + ",");
                                arrayList.Add("Status:" + ds.Tables[0].Rows[i][3].ToString() + ",");
                                //arrayList.Add("Date:" + ds.Tables[0].Rows[i][4].ToString() + ",");
                            }
                        }
                        else
                        {
                            arrayList.Add("FALSE");
                            arrayList.Add("Sorry no transaction is found");

                        }
                    }
                    catch (Exception)
                    {
                        arrayList.Add("FALSE");
                        arrayList.Add("Something Went Wrong");
                    }
                    finally
                    {
                        ds.Dispose();
                    }
                }
                else
                {

                    arrayList.Add("FALSE");
                    arrayList.Add("Invalid Operater ID");
                }
            }
            else
            {
                arrayList.Add("FALSE");
                arrayList.Add(strMessage);

            }
            return arrayList;
        }


        public ArrayList getMobileTransactionDetails(string strPhone, string strMPINPassword, string strCoustomerMobileNumber, int intOperationID, string strURL)
        {
            ArrayList arrayList = new ArrayList();
            string strResposne = string.Empty;
            string strRequestData = Login(strPhone, strMPINPassword, strURL);
            string[] strData = strRequestData.Split(new char[] { ',' });
            string strStatus = strData[0]; // 1:True & 2: False
            string strStatusData = strData[1];
            string strMessage = strData[2];
            string strUserID = strData[3];
            string strUserName = strData[4];

            if (strStatus == "1" && strStatusData == "TRUE")
            {
                if (intOperationID == 4) //  Get Details For Coustomer Mobile NUmebr
                {

                    DataSet ds = new DataSet();

                    try
                    {
                        ds = objODBC.getDataSet("select trans_number,mobile_number,amount,CASE WHEN status=1 THEN 'pending' WHEN status=2 THEN 'success' WHEN status=3 THEN 'Fail' ELSE 'Pending' END as Status,created_on from er_recharge where userid='" + strUserID + "' and mobile_number like'%" + strCoustomerMobileNumber + "%' order by id desc limit 1 ");
                        if (ds.Tables[0].Rows.Count > 0)
                        {
                            arrayList.Add("TRUE");
                            for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                            {
                                arrayList.Add("TNX ID:" + ds.Tables[0].Rows[i][0].ToString() + ",");
                                arrayList.Add("AMT:" + ds.Tables[0].Rows[i][2].ToString() + ",");
                                arrayList.Add("Status:" + ds.Tables[0].Rows[i][3].ToString() + ",");
                                arrayList.Add("Date:" + ds.Tables[0].Rows[i][4].ToString() + ".");
                            }
                        }
                        else
                        {
                            arrayList.Add("FALSE");
                            arrayList.Add("Sorry no such transaction is found");

                        }
                    }
                    catch (Exception)
                    {
                        arrayList.Add("FALSE");
                        arrayList.Add("Something Went Wrong");
                    }
                    finally
                    {
                        ds.Dispose();
                    }
                }
                else
                {

                    arrayList.Add("FALSE");
                    arrayList.Add("Invalid Operater ID");
                }
            }
            else
            {
                arrayList.Add("FALSE");
                arrayList.Add(strMessage);

            }
            return arrayList;
        }



        public ArrayList getTransactionIDTransactionDetails(string strPhone, string strMPINPassword, string strTransactionID, int intOperationID, string strURL)
        {
            ArrayList arrayList = new ArrayList();
            string strResposne = string.Empty;
            string strRequestData = Login(strPhone, strMPINPassword, strURL);
            string[] strData = strRequestData.Split(new char[] { ',' });
            string strStatus = strData[0]; // 1:True & 2: False
            string strStatusData = strData[1];
            string strMessage = strData[2];
            string strUserID = strData[3];
            string strUserName = strData[4];

            if (strStatus == "1" && strStatusData == "TRUE")
            {
                if (intOperationID == 5) //  Get Details For Coustomer Mobile NUmebr
                {

                    DataSet ds = new DataSet();

                    try
                    {
                        ds = objODBC.getDataSet("select trans_number,mobile_number,amount,CASE WHEN status=1 THEN 'pending' WHEN status=2 THEN 'success' WHEN status=3 THEN 'Fail' ELSE 'Pending' END as Status,created_on from er_recharge where userid='" + strUserID + "' and trans_number like'%" + strTransactionID + "%' order by id desc limit 1 ");
                        if (ds.Tables[0].Rows.Count > 0)
                        {
                            arrayList.Add("TRUE");
                            for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                            {
                                arrayList.Add("Mob.No.:" + ds.Tables[0].Rows[i][1].ToString() + ",");
                                arrayList.Add("AMT:" + ds.Tables[0].Rows[i][2].ToString() + ",");
                                arrayList.Add("Status:" + ds.Tables[0].Rows[i][3].ToString() + ",");
                                arrayList.Add("Date:" + ds.Tables[0].Rows[i][4].ToString() + ".");
                            }
                        }
                        else
                        {
                            arrayList.Add("FALSE");
                            arrayList.Add("Sorry no such transaction is found");

                        }
                    }
                    catch (Exception)
                    {
                        arrayList.Add("FALSE");
                        arrayList.Add("Something Went Wrong");
                    }
                    finally
                    {
                        ds.Dispose();
                    }
                }
                else
                {

                    arrayList.Add("FALSE");
                    arrayList.Add("Invalid Operater ID");
                }
            }
            else
            {
                arrayList.Add("FALSE");
                arrayList.Add(strMessage);

            }
            return arrayList;
        }



        public string LoginTopup_Retailer(string strMobileNumber, string strPassword, string strURL)
        {
            string strDescription = "Retailer Login for Etisalat USSD Topup";
            string strResponse = string.Empty;
            if (strMobileNumber.Length == 11)
            {
                strMobileNumber = strMobileNumber.Substring(2, strMobileNumber.Length - 2);
            }
            int intActivityType = 500;
            try
            {
                try
                {
                    objCreateFile.CreateTextFile("", "", "", "", "", "", "", 5, intActivityType, strDescription + ",UserID" + strMobileNumber + ",Password:" + strPassword + ",Data:" + strURL + "", strMobileNumber);


                }
                catch (Exception)
                {

                }

                Encryption objEncrypt = new Encryption();

                string strQueryValidation = "SELECT Count(1) From er_login l inner join er_mobile m on l.userid=m.userid Where m.mobile='" + strMobileNumber + "'  and l.Active=1";

                int intCount = objODBC.executeScalar_int(strQueryValidation);

                if (intCount > 0)
                {
                    string strQueryValidation1 = "SELECT Count(1) From er_login l inner join er_mobile m on l.userid=m.userid Where m.mobile='" + strMobileNumber + "' and l.Active=1 and l.usertype_id=5";

                    int intCount1 = objODBC.executeScalar_int(strQueryValidation);

                    if (intCount1 > 0)
                    {



                        dt2 = objODBC.getDataTable("SELECT l.pass_key,l.usertype_id,l.userid,l.full_name From er_login l inner join er_mobile m on l.userid=m.userid Where  m.mobile='" + strMobileNumber + "' and l.user_status=1 and l.Active=1 and l.usertype_id=5 limit 1");
                        if (dt2.Rows.Count > 0)
                        {
                            string passkey = dt2.Rows[0][0].ToString();
                            string intDesignationType = dt2.Rows[0][1].ToString();
                            string strMpinCheck = objEncrypt.EncryptQueryString(strPassword, passkey);
                            string intUserID = dt2.Rows[0][2].ToString();
                            string intUserName = dt2.Rows[0][3].ToString();

                            int logAttemptCount = objODBC.executeScalar_int("SELECT `count_attempt` from er_login_counter Where userid='" + intUserID + "'");
                            if (logAttemptCount <= 3)
                            {
                                int intLoginCount = objODBC.executeScalar_int("SELECT count(1) From er_login l inner join er_mobile m on l.userid=m.userid Where l.userid='" + intUserID + "' and m.userid='" + intUserID + "'  and m.mobile='" + strMobileNumber + "' and l.M_Pin='" + strMpinCheck + "' and l.user_status=1 and l.Active=1 and l.usertype_id=5 limit 1");
                                if (intLoginCount > 0)
                                {
                                    objODBC.executeNonQuery("UPDATE `er_login_counter` SET `count_attempt`=0 Where userid='" + intUserID + "'");
                                    strResponse = "1,TRUE,LoginSuccessfull," + intUserID + "," + intUserName + "";
                                    try
                                    {

                                        objODBCLOG.executeNonQuery("call activity_logs('" + intUserID + "','" + intDesignationType + "','2','NA','NA','NA','NA','NA','1','4','" + strDescription + "','" + intActivityType + "','NA','NA')");
                                    }
                                    catch (Exception)
                                    {
                                        strResponse = "1,TRUE,LoginSuccessfull," + intUserID + "," + intUserName + "";
                                    }
                                }
                                else
                                {
                                    if (logAttemptCount > 3)
                                    {
                                        strResponse = "2,FALSE,User Deactive ! Entered an Incorrect M-Pin 0 attempt left,NA,NA";
                                    }
                                    else
                                    {
                                        objODBC.executeNonQuery("UPDATE `er_login_counter` SET `count_attempt`=count_attempt+1 Where userid='" + intUserID + "'");
                                        if (logAttemptCount == 2)
                                        {
                                            try
                                            {
                                                objODBCLOG.executeNonQuery("call activity_logs('" + intUserID + "','" + intDesignationType + "','2','NA','NA','Login Attempt Failed User Deactive from USSD Mobile Topup','NA','NA','0',2,'Login Attempt Failed User Deactive from USSD Mobile Topup',500,'','')");
                                                objCreateFile.CreateTextFile("", "", "", "", "", "", "", 5, intActivityType, "Login Attempt Failed User Deactive from USSD Mobile Topup" + ",UserID" + strMobileNumber + ",Password:" + strPassword + ",Data:" + strURL + "", strMobileNumber);
                                            }
                                            catch(Exception)
                                            {

                                            }
                                            objODBC.executeNonQuery(" UPDATE er_login SET user_status=2 Where userid='" + intUserID + "'");
                                            objODBC.executeNonQuery("UPDATE `er_login_counter` SET `count_attempt`=0 Where userid='" + intUserID + "'");
                                            strResponse = "2,FALSE,Your account has been Locked due to 3 wrong attempt,NA,NA";

                                            try
                                            {
                                                int intLogStatusCount = objODBC.executeScalar_int("SELECT count(1) FROM `er_login_inactive` WHERE status=0 and userid=" + intUserID);
                                                if (intLogStatusCount == 0)
                                                {
                                                    objODBC.executeNonQuery("INSERT INTO `er_login_inactive`(`userid`, `username`,`created_on`,`source`) VALUES ('" + intUserID + "','" + strMobileNumber + "','" + objDT.getCurDateTimeString() + "','4')");

                                                }
                                            }
                                            catch (Exception)
                                            {


                                            }
                                        }
                                        else
                                        {
                                            strResponse = "2,FALSE,Invalid M-PIN you have " + (3 - (logAttemptCount + 1)).ToString() + " attempt left,NA,NA";
                                        }
                                    }

                                }
                            }
                            else
                            {

                                if (logAttemptCount > 3)
                                {
                                    strResponse = "2,FALSE,User Deactive ! Entered an Incorrect M-Pin 0 attempt left,NA,NA";
                                }
                                else
                                {
                                    objODBC.executeNonQuery("UPDATE `er_login_counter` SET `count_attempt`=count_attempt+1 Where userid='" + intUserID + "'");
                                    if (logAttemptCount == 2)
                                    {
                                        try
                                        {
                                            objODBCLOG.executeNonQuery("call activity_logs('" + intUserID + "','" + intDesignationType + "','2','NA','NA','Login Attempt Failed User Deactive from USSD Mobile Topup','NA','NA','0',2,'Login Attempt Failed User Deactive from USSD Mobile Topup',500,'','')");
                                            objCreateFile.CreateTextFile("", "", "", "", "", "", "", 5, intActivityType, "Login Attempt Failed User Deactive from USSD Mobile Topup" + ",UserID" + strMobileNumber + ",Password:" + strPassword + ",Data:" + strURL + "", strMobileNumber);
                                        }
                                        catch (Exception)
                                        {

                                        }
                                        objODBC.executeNonQuery(" UPDATE er_login SET user_status=2 Where userid='" + intUserID + "'");
                                        objODBC.executeNonQuery("UPDATE `er_login_counter` SET `count_attempt`=0 Where userid='" + intUserID + "'");
                                        strResponse = "2,FALSE,Your account has been Locked due to 3 wrong attempt,NA,NA";
                                        try
                                        {
                                            int intLogStatusCount = objODBC.executeScalar_int("SELECT count(1) FROM `er_login_inactive` WHERE status=0 and userid=" + intUserID);
                                            if (intLogStatusCount == 0)
                                            {
                                                objODBC.executeNonQuery("INSERT INTO `er_login_inactive`(`userid`, `username`,`created_on`,`source`) VALUES ('" + intUserID + "','" + strMobileNumber + "','" + objDT.getCurDateTimeString() + "','4')");

                                            }
                                        }
                                        catch (Exception)
                                        {

                                        }
                                    }
                                    else
                                    {
                                        strResponse = "2,FALSE,Invalid M-PIN you have " + (3 - (logAttemptCount + 1)).ToString() + " attempt left,NA,NA";
                                    }
                                }
                            }
                        }
                        else
                        {
                            strResponse = "2,FALSE, User Deactive ! Kindly Contact Admin ,NA,NA";
                        }

                    }
                    else
                    {
                        strResponse = "2,FALSE,You are not allowed to subscribe for this feature ,NA,NA";
                    }
                }
                else
                {
                    strResponse = "2,FALSE,Invalid Account Number,NA,NA";
                }
            }
            catch (Exception ex)
            {
                strResponse = "2,FALSE,Something Went Wrong,NA,NA";
            }
            return strResponse;
        }


        public string USSDTopupFunction(string strPhone, string strMPINPassword, int intOperationID, string strSign, string str_short_code, string strUSSD, string strCDate, string strURL, string strCoustomerMobile, string strRechargeAmount)
        {
            //   strResponse = "1,TRUE,LoginSuccessfull,"+ intUserID +","+ intDesignationType +"";
            // strResponse = "2,FALSE,Invalid Account Number,NA,NA";

            string strResposne = string.Empty;
            string strRequestData = Login(strPhone, strMPINPassword, strURL);
            int intInvestmentID = 0;
            string[] strData = strRequestData.Split(new char[] { ',' });
            string strStatus = strData[0]; // 1:True & 2: False
            string strStatusData = strData[1];
            string strMessage = strData[2];
            string strUserID = strData[3];
            string strUserName = strData[4];
            if (strStatus == "1" && strStatusData == "TRUE")
            {
                purchase_wallet1 objWallet = new purchase_wallet1();
                // strResposne = "1,TRUE,Message,"+ strUserID+","+ intInvestmentID+",OperaterID";
                if (intOperationID == 1) // Balance
                {

                    double strBalance = objWallet.GetWalletAmount(Convert.ToInt32(strUserID));

                    if (strBalance <= 0)
                    {
                        strBalance = 0.0;
                    }
                    strResposne = "1,TRUE,NA," + strUserID + "," + strUserName + "," + strBalance.ToString() + ",Successfully Fetch,0";
                }
                else if (intOperationID == 2) //  TopUp
                {
                    int USSDCount = objODBC.executeScalar_int("SELECT `ussd_top_status` from er_login  Where userid='" + strUserID + "'");
                    if (USSDCount == 1)
                    {
                        DataSet ds = new DataSet();
                        Recharge objRecharge = new Recharge();
                        ds = objODBC.getDataSet("call er_USSDRequest('" + strUserID + "',2,1)"); // 2 Etisalat and 1 For = 1: USSD Request & 2: SMPP USSD Request
                        if (ds.Tables[0].Rows.Count > 0)
                        {
                            int intUSSD_ID = int.Parse(ds.Tables[0].Rows[0][0].ToString());

                            objODBC.executeNonQuery("Update `er_etisalat_request_ussd` set request='" + strURL + "' where id='" + intUSSD_ID + "'");
                            int intAPIType = 0;

                            Regex objMobilePatternSalaam = new Regex(@"^74[\d]{7}$");
                            Regex objMobilePatternEtisalat = new Regex(@"^(78|73)[\d]{7}$");
                            Regex objMobilePatternRoshan = new Regex(@"^(79|72)[\d]{7}$");
                            Regex objMobilePatternMTN = new Regex(@"^(77|76)[\d]{7}$");
                            Regex objMobilePatternAWCC = new Regex(@"^(70|71)[\d]{7}$");

                            if (objMobilePatternSalaam.IsMatch(strCoustomerMobile))
                            {
                                intAPIType = 1; // Salaam
                            }
                            else if (objMobilePatternEtisalat.IsMatch(strCoustomerMobile))
                            {
                                intAPIType = 2; // Etisalat
                            }
                            else if (objMobilePatternRoshan.IsMatch(strCoustomerMobile))
                            {
                                intAPIType = 3; // Roshan
                            }
                            else if (objMobilePatternMTN.IsMatch(strCoustomerMobile))
                            {
                                intAPIType = 4; // MTN
                            }
                            else if (objMobilePatternAWCC.IsMatch(strCoustomerMobile))
                            {
                                intAPIType = 5; // AWCC
                            }
                            else
                            {
                                strResposne = "2,FALSE,NA," + strUserID + ",NA,NA,Invalid Mobile Number,0";
                            }

                            if (intAPIType > 0)
                            {
                                ArrayList arrUserParams = new ArrayList();
                                arrUserParams = objRecharge.MakeMobileRechargeforUSSDTopup(int.Parse(strUserID), intAPIType, strCoustomerMobile, double.Parse(strRechargeAmount), intUSSD_ID, 4, strPhone);
                                string[] Rechargearray = arrUserParams.ToArray(typeof(string)) as string[];
                                string strStatusRecharge = Rechargearray[0];
                                string strStatusMessage = Rechargearray[1];
                                string strTransactionID = Rechargearray[2];
                                string strOperaterTransactionID = Rechargearray[3];

                                if (strStatusRecharge == "TRUE")
                                {
                                    double strBalance = objWallet.GetWalletAmount(Convert.ToInt32(strUserID));

                                    objODBC.executeNonQuery("UPDATE `er_etisalat_request_ussd` SET coustomer_mobile='" + strCoustomerMobile + "',`phone`='" + strPhone + "',`ussd`='" + strUSSD + "',`cdate`='" + strCDate + "',`sign`='" + strSign + "',`request`='" + strURL + "',`recharge_ID`='" + strTransactionID + "',Operater_TransactionID='" + strOperaterTransactionID + "',Balance='" + strBalance + "' where id='" + intUSSD_ID + "'");

                                    strResposne = "1,TRUE," + strTransactionID + "," + strUserID + "," + strUserName + "," + strBalance + ",Top-Up successfully done," + intUSSD_ID.ToString();
                                }
                                else
                                {
                                    double strBalance = objWallet.GetWalletAmount(Convert.ToInt32(strUserID));
                                    if (strStatusMessage == "PENDING")
                                    {

                                        strResposne = "2,FALSE," + strTransactionID + "," + strUserID + "," + strUserName + "," + strBalance + "," + strStatusMessage + ",Transaction ID:" + strTransactionID + "!," + intUSSD_ID.ToString();
                                    }
                                    else
                                    {
                                        strResposne = "2,FALSE," + strTransactionID + "," + strUserID + "," + strUserName + "," + strBalance + "," + strStatusMessage + "," + intUSSD_ID.ToString();
                                    }
                                }
                            }
                        }
                        else if (intOperationID == 3) //  Last 5 Transaction 
                        {
                        }
                        else
                        {
                            strResposne = "2,FALSE,NA," + strUserID + ",NA,NA,Invalid USSD code,0";
                        }
                    }
                    else
                    {
                        strResposne = "2,FALSE,NA," + strUserID + ",NA,NA,You do not have permission to recharge from USSD,0";
                    }
                }
                else
                {

                    strResposne = "2,FALSE,NA," + strUserID + ",NA,NA,Invalid USSD code,0";
                }
            }
            else
            {
                strResposne = "2,FALSE,NA," + strUserID + ",NA,NA," + strMessage + ",0";
            }
            return strResposne;
        }
        public string USSDTopupFunctionOtherOperator(string strPhone, string strMPINPassword, int intOperationID, string strSign, string str_short_code, string strUSSD, string strCDate, string strURL, string strCoustomerMobile, string strRechargeAmount,int intApiType)
        {
            //   strResponse = "1,TRUE,LoginSuccessfull,"+ intUserID +","+ intDesignationType +"";
            // strResponse = "2,FALSE,Invalid Account Number,NA,NA";

            string strResposne = string.Empty;
            string strRequestData = Login(strPhone, strMPINPassword, strURL);
            int intInvestmentID = 0;
            string[] strData = strRequestData.Split(new char[] { ',' });
            string strStatus = strData[0]; // 1:True & 2: False
            string strStatusData = strData[1];
            string strMessage = strData[2];
            string strUserID = strData[3];
            string strUserName = strData[4];
            if (strStatus == "1" && strStatusData == "TRUE")
            {
                purchase_wallet1 objWallet = new purchase_wallet1();
                // strResposne = "1,TRUE,Message,"+ strUserID+","+ intInvestmentID+",OperaterID";
                if (intOperationID == 1) // Balance
                {

                    double strBalance = objWallet.GetWalletAmount(Convert.ToInt32(strUserID));

                    if (strBalance <= 0)
                    {
                        strBalance = 0.0;
                    }
                    strResposne = "1,TRUE,NA," + strUserID + "," + strUserName + "," + strBalance.ToString() + ",Successfully Fetch,0";
                }
                else if (intOperationID == 2) //  TopUp
                {

                    DataSet ds = new DataSet();
                    Recharge objRecharge = new Recharge();
                    ds = objODBC.getDataSet("call er_USSDRequest('" + strUserID + "',2,1)"); // 2 Etisalat and 1 For = 1: USSD Request & 2: SMPP USSD Request
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        int intUSSD_ID = int.Parse(ds.Tables[0].Rows[0][0].ToString());

                        objODBC.executeNonQuery("Update `er_etisalat_request_ussd` set request='" + strURL + "' where id='" + intUSSD_ID + "'");

                        ArrayList arrUserParams = new ArrayList();
                        arrUserParams = objRecharge.MakeMobileRechargeforUSSDTopup(int.Parse(strUserID),intApiType, strCoustomerMobile, double.Parse(strRechargeAmount), intUSSD_ID,4, strPhone);
                        string[] Rechargearray = arrUserParams.ToArray(typeof(string)) as string[];
                        string strStatusRecharge = Rechargearray[0];
                        string strStatusMessage = Rechargearray[1];
                        string strTransactionID = Rechargearray[2];
                        string strOperaterTransactionID = Rechargearray[3];

                        if (strStatusRecharge == "TRUE")
                        {
                            double strBalance = objWallet.GetWalletAmount(Convert.ToInt32(strUserID));

                            objODBC.executeNonQuery("UPDATE `er_etisalat_request_ussd` SET coustomer_mobile='" + strCoustomerMobile + "',`phone`='" + strPhone + "',`ussd`='" + strUSSD + "',`cdate`='" + strCDate + "',`sign`='" + strSign + "',`request`='" + strURL + "',`recharge_ID`='" + strTransactionID + "',Operater_TransactionID='" + strOperaterTransactionID + "',Balance='" + strBalance + "' where id='" + intUSSD_ID + "'");

                            strResposne = "1,TRUE," + strTransactionID + "," + strUserID + "," + strUserName + "," + strBalance + ",Top-Up successfully done," + intUSSD_ID.ToString();
                        }
                        else
                        {
                            double strBalance = objWallet.GetWalletAmount(Convert.ToInt32(strUserID));
                            if (strStatusMessage == "PENDING")
                            {

                                strResposne = "2,FALSE," + strTransactionID + "," + strUserID + "," + strUserName + "," + strBalance + "," + strStatusMessage + "," + intUSSD_ID.ToString();
                            }
                            else
                            {
                                strResposne = "2,FALSE," + strTransactionID + "," + strUserID + "," + strUserName + "," + strBalance + "," + strStatusMessage + "," + intUSSD_ID.ToString();
                            }


                        }
                    }
                    else if (intOperationID == 3) //  Last 5 Transaction 
                    {
                    }
                    else
                    {
                        strResposne = "2,FALSE,NA," + strUserID + ",NA,NA,Invalid USSD code,0";
                    }
                }
                else
                {

                    strResposne = "2,FALSE,NA," + strUserID + ",NA,NA,Invalid USSD code,0";
                }
            }
            else
            {
                strResposne = "2,FALSE,NA," + strUserID + ",NA,NA," + strMessage + ",0";
            }
            return strResposne;
        }

        public string USSDGetbalance(string strPhone, string strMPINPassword, int intOperationID, string strSign, string str_short_code, string strUSSD, string strCDate, string strURL, string strCoustomerMobile, string strRechargeAmount)
        {
            //   strResponse = "1,TRUE,LoginSuccessfull,"+ intUserID +","+ intDesignationType +"";
            // strResponse = "2,FALSE,Invalid Account Number,NA,NA";

            string strResposne = string.Empty;
            string strRequestData = Login(strPhone, strMPINPassword, strURL);
            int intInvestmentID = 0;
            string[] strData = strRequestData.Split(new char[] { ',' });
            string strStatus = strData[0]; // 1:True & 2: False
            string strStatusData = strData[1];
            string strMessage = strData[2];
            string strUserID = strData[3];
            string strUserName = strData[4];
            if (strStatus == "1" && strStatusData == "TRUE")
            {
                purchase_wallet1 objWallet = new purchase_wallet1();
                // strResposne = "1,TRUE,Message,"+ strUserID+","+ intInvestmentID+",OperaterID";
                if (intOperationID == 1) // Balance
                {

                    double strBalance = objWallet.GetWalletAmount(Convert.ToInt32(strUserID));

                    if (strBalance <= 0)
                    {
                        strBalance = 0.0;
                    }
                    strResposne = "1,TRUE,NA," + strUserID + "," + strUserName + "," + strBalance.ToString() + ",Successfully Fetch,0";
                }
                else if (intOperationID == 2) //  TopUp
                {
                    strResposne = "2,FALSE,NA," + strUserID + ",NA,NA,Invalid USSD code,0";
                }
                else
                {

                    strResposne = "2,FALSE,NA," + strUserID + ",NA,NA,Invalid USSD code,0";
                }
            }
            else
            {
                strResposne = "2,FALSE,NA," + strUserID + ",NA,NA," + strMessage + ",0";
            }
            return strResposne;
        }


        public string USSDTopupSale(string strPhone, string strMPINPassword, int intOperationID, string strSign, string str_short_code, string strUSSD, string strCDate, string strURL, string strCoustomerMobile, string strRechargeAmount)
        {
            //   strResponse = "1,TRUE,LoginSuccessfull,"+ intUserID +","+ intDesignationType +"";
            // strResponse = "2,FALSE,Invalid Account Number,NA,NA";

            string strResposne = string.Empty;
            string strRequestData = Login(strPhone, strMPINPassword, strURL);            
            string[] strData = strRequestData.Split(new char[] { ',' });
            string strStatus = strData[0]; // 1:True & 2: False
            string strStatusData = strData[1];
            string strMessage = strData[2];
            string strUserID = strData[3];
            string strUserName = strData[4];
            if (strStatus == "1" && strStatusData == "TRUE")
            {
               
                // strResposne = "1,TRUE,Message,"+ strUserID+","+ intInvestmentID+",OperaterID";
                if (intOperationID == 9) // Check Topup Sale
                {

                    int intUserType = objODBC.executeScalar_int("select usertype_id from er_login where userid=" + strUserID);

                    if (intUserType > 1)
                    {                        
                        double dblGetTotalSale = objODBC.executeScalar_dbl("call getTopupSale('" + strUserID + "','" + intUserType + "');");
                        strResposne = "1,TRUE,NA," + strUserID + "," + strUserName + "," + dblGetTotalSale.ToString() + ",Successfully Fetch,"+ intUserType;
                    }
                    else
                    {
                        strResposne = "2,FALSE,NA," + strUserID + ",NA,NA,Invalid User details,0";
                    }



                }              
                else
                {

                    strResposne = "2,FALSE,NA," + strUserID + ",NA,NA,Invalid USSD code,0";
                }
            }
            else
            {
                strResposne = "2,FALSE,NA," + strUserID + ",NA,NA," + strMessage + ",0";
            }
            return strResposne;
        }

        public string Login(string strMobileNumber, string strPassword, string strURL)
        {
            string strDescription = "Retailer Login for USSD";
            string strResponse = string.Empty;

            int intActivityType = 500;
            try
            {
                try
                {
                    objCreateFile.CreateTextFile("", "", "", "", "", "", "", 5, intActivityType, strDescription + ",UserID" + strMobileNumber + ",Password:" + strPassword + ",Data:" + strURL + "", strMobileNumber);


                }
                catch (Exception)
                {

                }

                Encryption objEncrypt = new Encryption();
                if (strMobileNumber.Length == 11)
                {
                    strMobileNumber = strMobileNumber.Substring(2, strMobileNumber.Length - 2);
                }


                string strQueryValidation = "SELECT Count(1) From er_login l inner join er_mobile m on l.userid=m.userid Where m.mobile='" + strMobileNumber + "' and l.Active=1";

                int intCount = objODBC.executeScalar_int(strQueryValidation);

                if (intCount > 0)
                {
                    dt2 = objODBC.getDataTable("SELECT l.pass_key,l.usertype_id,l.userid,l.full_name From er_login l inner join er_mobile m on l.userid=m.userid Where  m.mobile='" + strMobileNumber + "' and l.user_status=1 and l.Active=1 limit 1");
                    if (dt2.Rows.Count > 0)
                    {
                        string passkey = dt2.Rows[0][0].ToString();
                        string intDesignationType = dt2.Rows[0][1].ToString();
                        string strMpinCheck = objEncrypt.EncryptQueryString(strPassword, passkey);
                        string intUserID = dt2.Rows[0][2].ToString();
                        string intUserName = dt2.Rows[0][3].ToString();

                        int logAttemptCount = objODBC.executeScalar_int("SELECT `count_attempt` from er_login_counter Where userid='" + intUserID + "'");
                        if (logAttemptCount <= 3)
                        {
                            int intLoginCount = objODBC.executeScalar_int("SELECT count(1) From er_login l inner join er_mobile m on l.userid=m.userid Where l.userid='" + intUserID + "' and m.userid='" + intUserID + "'  and m.mobile='" + strMobileNumber + "' and l.M_Pin='" + strMpinCheck + "' and l.user_status=1 and l.Active=1 and l.usertype_id='" + intDesignationType + "' limit 1");
                            if (intLoginCount > 0)
                            {
                                objODBC.executeNonQuery("UPDATE `er_login_counter` SET `count_attempt`=0 Where userid='" + intUserID + "'");
                                strResponse = "1,TRUE,LoginSuccessfull," + intUserID + "," + intUserName + "";
                                try
                                {
                                    objODBCLOG.executeNonQuery("call activity_logs('" + intUserID + "','" + intDesignationType + "','2','NA','NA','Login for USSD Mobile Topup','NA','NA','0',2,'" + strDescription + "',500,'','')");


                                }
                                catch (Exception)
                                {

                                }
                            }
                            else
                            {

                                if (logAttemptCount > 3)
                                {
                                    strResponse = "2,FALSE,User Deactive ! Entered an Incorrect M-Pin 0 attempt left,NA,NA";
                                }
                                else
                                {
                                    objODBC.executeNonQuery("UPDATE `er_login_counter` SET `count_attempt`=count_attempt+1 Where userid='" + intUserID + "'");
                                    if (logAttemptCount == 2)
                                    {
                                        objODBCLOG.executeNonQuery("call activity_logs('" + intUserID + "','" + intDesignationType + "','2','NA','NA','Login Attempt Failed User Deactive from USSD Mobile Topup','NA','NA','0',2,'Login Attempt Failed User Deactive from USSD Mobile Topup',500,'','')");
                                        objCreateFile.CreateTextFile("", "", "", "", "", "", "", 5, intActivityType, "Login Attempt Failed User Deactive from USSD Mobile Topup" + ",UserID" + strMobileNumber + ",Password:" + strPassword + ",Data:" + strURL + "", strMobileNumber);

                                        objODBC.executeNonQuery(" UPDATE er_login SET user_status=2 Where userid='" + intUserID + "'");
                                        objODBC.executeNonQuery("UPDATE `er_login_counter` SET `count_attempt`=0 Where userid='" + intUserID + "'");

                                       

                                        strResponse = "2,FALSE,Your account has been Locked due to 3 wrong attempt,NA,NA";

                                        try
                                        {
                                            int intLogStatusCount = objODBC.executeScalar_int("SELECT count(1) FROM `er_login_inactive` WHERE status=0 and userid=" + intUserID);
                                            if (intLogStatusCount == 0)
                                            {
                                                objODBC.executeNonQuery("INSERT INTO `er_login_inactive`(`userid`, `username`,`created_on`,`source`) VALUES ('" + intUserID + "','" + strMobileNumber + "','" + objDT.getCurDateTimeString() + "','4')");

                                            }
                                        }
                                        catch (Exception)
                                        {


                                        }
                                    }
                                    else
                                    {
                                        strResponse = "2,FALSE,Invalid M-PIN you have " + (3 - (logAttemptCount + 1)).ToString() + " attempt left,NA,NA";
                                    }
                                }                              

                            }
                        }
                        else
                        {
                            
                            if (logAttemptCount > 3)
                            {                               
                                strResponse = "2,FALSE,User Deactive ! Entered an Incorrect M-Pin 0 attempt left,NA,NA";
                            }
                            else
                            {
                                objODBC.executeNonQuery("UPDATE `er_login_counter` SET `count_attempt`=count_attempt+1 Where userid='" + intUserID + "'");
                                if (logAttemptCount == 2)
                                {
                                    objODBCLOG.executeNonQuery("call activity_logs('" + intUserID + "','" + intDesignationType + "','2','NA','NA','Login Attempt Failed User Deactive from USSD Mobile Topup','NA','NA','0',2,'Login Attempt Failed User Deactive from USSD Mobile Topup',500,'','')");
                                    objCreateFile.CreateTextFile("", "", "", "", "", "", "", 5, intActivityType, "Login Attempt Failed User Deactive from USSD Mobile Topup" + ",UserID" + strMobileNumber + ",Password:" + strPassword + ",Data:" + strURL + "", strMobileNumber);
                                    objODBC.executeNonQuery(" UPDATE er_login SET user_status=2 Where userid='" + intUserID + "'");
                                    objODBC.executeNonQuery("UPDATE `er_login_counter` SET `count_attempt`=0 Where userid='" + intUserID + "'");


                                    strResponse = "2,FALSE,Your account has been Locked due to 3 wrong attempt,NA,NA";

                                    try
                                    {
                                        int intLogStatusCount = objODBC.executeScalar_int("SELECT count(1) FROM `er_login_inactive` WHERE status=0 and userid=" + intUserID);
                                        if (intLogStatusCount == 0)
                                        {
                                            objODBC.executeNonQuery("INSERT INTO `er_login_inactive`(`userid`, `username`,`created_on`,`source`) VALUES ('" + intUserID + "','" + strMobileNumber + "','" + objDT.getCurDateTimeString() + "','4')");

                                        }
                                    }
                                    catch (Exception)
                                    {


                                    }
                                }
                                else
                                {
                                    strResponse = "2,FALSE,Invalid M-PIN you have " + (3 - (logAttemptCount + 1)).ToString() + " attempt left,NA,NA";
                                }
                            }
                        }
                    }
                    else
                    {
                        
                        strResponse = "2,FALSE, User Deactive ! Kindly Contact Admin ,NA,NA";
                    }
                }
                else
                {
                    strResponse = "2,FALSE,Invalid Account Number ,NA,NA";
                }
            }
            catch (Exception ex)
            {
                strResponse = "2,FALSE,Something Went Wrong ,NA,NA";
            }
            return strResponse;
        }





      


        public string USSDFundTransferFunction(string strPhone, string strMPINPassword, int intOperationID, string strURL, string strCoustomerMobile, string dblTransferAmount)
        {
            //   strResponse = "1,TRUE,LoginSuccessfull,"+ intUserID +","+ intDesignationType +"";
            // strResponse = "2,FALSE,Invalid Account Number,NA,NA";

            string strResposne = string.Empty;
            string strRequestData = Login(strPhone, strMPINPassword, strURL);
            int intInvestmentID = 0;
            string[] strData = strRequestData.Split(new char[] { ',' });
            string strStatus = strData[0]; // 1:True & 2: False
            string strStatusData = strData[1];
            string strMessage = strData[2];
            string strUserID = strData[3];
            string strUserName = strData[4];
            if (strStatus == "1" && strStatusData == "TRUE")
            {
                purchase_wallet1 objWallet = new purchase_wallet1();
                if (intOperationID == 6) //  Fund Transfer
                {
                    //int intCountStatusCheck = objODBC.executeScalar_int("select usertype_id from er_login where userid=" + int.Parse(strUserID));
                    //if (intCountStatusCheck != 5)
                    //{
                        // strMessage = "1,TRUE," + dblUserBalance + "," + intSenderID + "," + intUserID + "," + dblTransferAmount + ",Wallet fund successfully transferred ";
                        int intStatus = objODBC.executeScalar_int("SELECT `status` FROM `er_status_manager` WHERE id=1");
                        if (intStatus == 1)
                        {
                            FundTransferUSSD objFundTransferUSSD = new FundTransferUSSD();
                            strResposne = objFundTransferUSSD.FundTransfer(int.Parse(strUserID), strCoustomerMobile, dblTransferAmount, strPhone);
                            // strResposne = FundTransfer(int.Parse(strUserID), strCoustomerMobile, dblTransferAmount, strPhone);
                        }
                        else
                        {
                            strResposne = "2,FALSE,NA,NA,NA," + dblTransferAmount + ",Permission denied by system ";
                        }
                    //}
                    //else
                    //{
                    //    strResposne = "2,FALSE,NA,NA,NA," + dblTransferAmount + ",Invalid USSD code ";
                    //}
                }
                else
                {
                    strResposne = "2,FALSE,NA,NA,NA," + dblTransferAmount + ",Invalid USSD code ";
                }
            }
            else
            {
                strResposne = "2,FALSE,NA,NA,NA," + dblTransferAmount + "," + strMessage;
            }
            return strResposne;
        }






        public string USSDReverseTopupFunction(string strPhone, string strMPINPassword, int intOperationID, string strSign, string str_short_code, string strUSSD, string strCDate, string strURL, string strTopupTransaction, string strRechargeAmount)
        {
            //   strResponse = "1,TRUE,LoginSuccessfull,"+ intUserID +","+ intDesignationType +"";
            // strResponse = "2,FALSE,Invalid Account Number,NA,NA";

            string strResposne = string.Empty;
            string strRequestData = Login(strPhone, strMPINPassword, strURL);
            int intInvestmentID = 0;
            string[] strData = strRequestData.Split(new char[] { ',' });
            string strStatus = strData[0]; // 1:True & 2: False
            string strStatusData = strData[1];
            string strMessage = strData[2];
            string strUserID = strData[3];
            string strUserName = strData[4];
            if (strStatus == "1" && strStatusData == "TRUE")
            {
                purchase_wallet1 objWallet = new purchase_wallet1();
                // strResposne = "1,TRUE,Message,"+ strUserID+","+ intInvestmentID+",OperaterID";
                if (intOperationID == 1) // Balance
                {

                    double strBalance = objWallet.GetWalletAmount(Convert.ToInt32(strUserID));

                    if (strBalance <= 0)
                    {
                        strBalance = 0.0;
                    }
                    strResposne = "1,TRUE,NA," + strUserID + "," + strUserName + "," + strBalance.ToString() + ",Successfully Fetch,0";
                }
                else if (intOperationID == 7) //  Reverse TopUp
                {

                    DataSet ds = new DataSet();
                    Recharge objRecharge = new Recharge();
                    ds = objODBC.getDataSet("call er_USSDRequest('" + strUserID + "',2,1)"); // 2 Etisalat and 1 For = 1: USSD Request & 2: SMPP USSD Request
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        int intUSSD_ID = int.Parse(ds.Tables[0].Rows[0][0].ToString());

                        objODBC.executeNonQuery("Update `er_etisalat_request_ussd` set request='" + strURL + "' where id='" + intUSSD_ID + "'");

                        ArrayList arrUserParams = new ArrayList();
                        arrUserParams = objRecharge.ReverseRechargeforUSSDTopup(int.Parse(strUserID), 2, objODBC.executeScalar_str("select mobile_number FROM `er_recharge` where id='" + strTopupTransaction + "'").ToString(), double.Parse(strRechargeAmount), intUSSD_ID, strTopupTransaction);
                        string[] Rechargearray = arrUserParams.ToArray(typeof(string)) as string[];
                        string strStatusRecharge = Rechargearray[0];
                        string strStatusMessage = Rechargearray[1];
                        string strTransactionID = Rechargearray[2];
                        string strOperaterTransactionID = Rechargearray[3];

                        if (strStatusRecharge == "TRUE")
                        {
                            double strBalance = objWallet.GetWalletAmount(Convert.ToInt32(strUserID));

                            objODBC.executeNonQuery("UPDATE `er_etisalat_request_ussd` SET coustomer_mobile='" + strTopupTransaction + "',`phone`='" + strPhone + "',`ussd`='" + strUSSD + "',`cdate`='" + strCDate + "',`sign`='" + strSign + "',`request`='" + strURL + "',`recharge_ID`='" + strTransactionID + "',Operater_TransactionID='" + strOperaterTransactionID + "',Balance='" + strBalance + "' where id='" + intUSSD_ID + "'");

                            strResposne = "1,TRUE," + strTransactionID + "," + strUserID + "," + strUserName + "," + strBalance + ",Top-Up successfully done," + intUSSD_ID.ToString();
                        }
                        else
                        {
                            double strBalance = objWallet.GetWalletAmount(Convert.ToInt32(strUserID));
                            if (strStatusMessage == "PENDING")
                            {

                                strResposne = "2,FALSE," + strTransactionID + "," + strUserID + "," + strUserName + "," + strBalance + "," + "Status:Pending,Transaction ID: :" + strTransactionID + "!," + intUSSD_ID.ToString();
                            }
                            else
                            {
                                strResposne = "2,FALSE," + strTransactionID + "," + strUserID + "," + strUserName + "," + strBalance + "," + strStatusMessage + "," + intUSSD_ID.ToString();
                            }


                        }
                    }
                    else if (intOperationID == 3) //  Last 5 Transaction 
                    {
                    }
                    else
                    {
                        strResposne = "2,FALSE,NA," + strUserID + ",NA,NA,Invalid USSD code,0";
                    }
                }
                else
                {

                    strResposne = "2,FALSE,NA," + strUserID + ",NA,NA,Invalid USSD code,0";
                }
            }
            else
            {
                strResposne = "2,FALSE,NA," + strUserID + ",NA,NA," + strMessage + ",0";
            }
            return strResposne;
        }
    }

}
