﻿using System;
using System.Net.Http;
using System.Web.Http;
using System.Xml.Linq;
using Etopup_Ws.old_App_Code;
using System.Threading.Tasks;
using Etopup_Ws.Models;
using System.Net;

namespace Etopup_Ws.Controller
{
    public class BoloroSMSController : ApiController
    {
        ODBC objODBC = new ODBC();
      
        [HttpPost]
        public async Task<HttpResponseMessage> BoloroSMS([FromBody]SMS ModSMS)
        {
            SMS _ModSMS = new SMS();
            _ModSMS.intUserID = ModSMS.intUserID;
            _ModSMS.strMobile = ModSMS.strMobile;
            _ModSMS.strOperaterName = ModSMS.strOperaterName;          
            _ModSMS.strOperaterID = ModSMS.strOperaterID;
            _ModSMS.dblActualAmt = ModSMS.dblActualAmt;
            _ModSMS.intRechargeLogType = ModSMS.intRechargeLogType;
            _ModSMS.TransactionID = ModSMS.TransactionID;
            _ModSMS.strPhone = ModSMS.strPhone;

            HttpResponseMessage returnMessage = new HttpResponseMessage();

            string message = "Your Request is In Process  ";
            try
            {
                await Task.Run(() =>
                {
                    Recharge objRecharge = new Recharge();
                    objRecharge.SendSMPPSMS(_ModSMS.intUserID, objODBC.executeScalar_str("select username FROM `er_login` where userid='" + _ModSMS.intUserID + "'"), _ModSMS.strMobile, _ModSMS.strOperaterName, _ModSMS.strOperaterID, _ModSMS.dblActualAmt, _ModSMS.intRechargeLogType, _ModSMS.TransactionID, _ModSMS.strPhone);
                });

                returnMessage = new HttpResponseMessage(HttpStatusCode.Created);
                returnMessage.RequestMessage = new HttpRequestMessage(HttpMethod.Post, message);
            }
            catch (Exception ex)
            {
                returnMessage = new HttpResponseMessage(HttpStatusCode.ExpectationFailed);
                returnMessage.RequestMessage = new HttpRequestMessage(HttpMethod.Post, ex.ToString());
            }
            finally
            {
              
            }

            return await Task.FromResult(returnMessage);
        }



    }
}
