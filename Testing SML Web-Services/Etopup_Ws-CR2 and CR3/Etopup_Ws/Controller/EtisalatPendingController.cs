﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Web.Http;
using Etopup_Ws.Models;
using Etopup_Ws.old_App_Code;
using System.Data;

namespace Etopup_Ws
{
    public class EtisalatPendingController : ApiController
    {
        ODBC objODBC = new ODBC();
        DataSet ds = new DataSet();
        //[HttpPost]
        //public string EtisalatPending([FromBody] EtisalatPendingModel EtM)
        //{

        //    return string.Format("Name: {0}, Value: {1}, Date: {2}, Action: {3}", etisalatPendingModel.intEtisalatID, etisalatPendingModel.intRechargeID, etisalatPendingModel.strCoustomerMobile, etisalatPendingModel.strEtisalatResponse);
        //}
        public string TruncateAtWord(string input, int length)
        {
            if (input == null || input.Length < length)
                return input;
            int iNextSpace = input.LastIndexOf("", length, StringComparison.Ordinal);
            return string.Format("{0}", input.Substring(0, (iNextSpace > 0) ? iNextSpace : length).Trim());
        }

        [HttpPost]
        public async Task<HttpResponseMessage> EtisalatPending([FromBody]EtisalatPendingModel EtM)
        {
            EtisalatPendingModel _EtM = new EtisalatPendingModel();
            _EtM.intRechargeID = EtM.intRechargeID;
            _EtM.intEtisalatID = EtM.intEtisalatID;
            _EtM.strEtisalatResponse = EtM.strEtisalatResponse;
            _EtM.strEtisalatMNOTransID = EtM.strEtisalatMNOTransID;
            _EtM.strCoustomerMobile = EtM.strCoustomerMobile;
            _EtM.dblAmount = EtM.dblAmount;
            _EtM.transaction_bytereceived = EtM.transaction_bytereceived;
            _EtM.TransType = EtM.TransType;



            HttpResponseMessage returnMessage = new HttpResponseMessage();


            try
            {
                // add details to database;
                //  int intCount = objODBC.executeNonQuery("asasas");
                //  string message = ($"EtisalatResponse - {_EtM.strEtisalatResponse}");
                try
                {
                    string strQuerry = "update etisalat_topup_request set all_response=' @@  @@ " + _EtM.strEtisalatResponse + "',Transaction_Response='" + _EtM.strEtisalatResponse + "',transaction_bytereceived='" + EtM.transaction_bytereceived + "' where id='" + _EtM.intEtisalatID + "'";
                    objODBC.executeNonQuery(strQuerry);
                }
                catch (Exception)
                {


                }

                // Login
                //`SC`00541.00CCB00000PPSPHS0000000000DLGLGNFFFF00000001TXBEG FFFFLOGIN: USER=stmt,PSWD=raga  C9AFD3D8
                //  `SC`005c1.00CCB00000PPSPHS0000000002DLGLGNFFFF00000001TXEND FFFFACK:LOGIN:RETN=0000,DESC="Success"  DFEFEDD6


                // Trans
                //  @@  @@ `SC`00a01.00CCB00000PPSPHS0000000002DLGCONFFFF00000004TXEND FFFFACK:CHARGE CASH ACNT:RETN=0000,DESC=SUCCESS,BEFOROPER=238,FEE=11000,AFTEROPER=10138,CSVSTOP=3000-01-01  8DEC9495
                // `SC`00a01.00CCB00000PPSPHS0000000002DLGCONFFFF00000004TXEND FFFFACK:CHARGE CASH ACNT:RETN=0000,DESC=SUCCESS,BEFOROPER=238,FEE=11000,AFTEROPER=10138,CSVSTOP=3000-01-01  8DEC9495


                string message = "Your Request is In Process , Thanks for  ";
                await Task.Run(() =>
                {
                    if (_EtM.TransType == 1) // Topup
                    {

                        if (Regex.IsMatch(_EtM.strEtisalatResponse, string.Format(@"\b{0}\b", Regex.Escape("ACNT:RETN=0000,DESC=SUCCESS,BEFOROPER"))))
                        {
                            string strResposne = TruncateAtWord(_EtM.strEtisalatResponse, 64); // Truncate From Position of Words
                            string output = _EtM.strEtisalatResponse.Replace(strResposne, ""); // Replace Words From Data
                            output = output.Remove(output.Length - 8); // Deduct Words from Last                   
                            string StrCheckSum = _EtM.strEtisalatResponse.Substring(_EtM.strEtisalatResponse.Length - 8); // Getting Checksum

                            string[] strDescriptionResponse = output.Split(new char[] { ',' });
                            // strMessage = "2,OOPs! You do not have sufficient balance.,Failed,NA,NA";

                            string TRDStatus = strDescriptionResponse[0];
                            string TRDDescription = strDescriptionResponse[1]; ;
                            string BEFOROPER = strDescriptionResponse[2];
                            string FEE = strDescriptionResponse[3];
                            string AFTEROPER = strDescriptionResponse[4];
                            string CSVSTOP = strDescriptionResponse[5];

                            string TXNID = _EtM.strEtisalatMNOTransID;

                            try
                            {
                                string strBalance = objODBC.executeScalar_str("SELECT amt FROM `er_sys_wallet` where id=2;");
                                objODBC.executeNonQuery("update er_recharge set comment='" + _EtM.strEtisalatResponse + "',operator_balance='" + strBalance + "' where id='" + _EtM.intRechargeID + "'");

                            }
                            catch (Exception)
                            {

                            }
                            try
                            {
                                objODBC.executeNonQuery("update etisalat_topup_request set ,transaction_DESC='" + TRDDescription + "',Transactio_Status='" + TRDStatus + "',BEFOROPER='" + BEFOROPER + "',FEE='" + FEE + "',AFTEROPER='" + AFTEROPER + "','" + CSVSTOP + "',Trans_Receive_Checksum='" + StrCheckSum + "',status=1,all_response=' @@  @@ " + _EtM.strEtisalatResponse + "',Transaction_Response='" + _EtM.strEtisalatResponse + "' WHERE recharge_id =" + _EtM.intRechargeID + "");
                            }
                            catch (Exception)
                            {

                            }

                            try
                            {
                                objODBC.executeNonQuery("call `updateResponseV3`('" + _EtM.strEtisalatMNOTransID + "','" + _EtM.intRechargeID + "',1)");
                            }
                            catch (Exception)
                            {

                            }
                            try
                            {

                                ds = objODBC.getDataSet("Select r.id,r.userid,r.operator_id,r.operator_name,r.Operater_table_id,r.amount,r.mobile_number,r.request_mobile_no,l.username,l.full_name from er_recharge r inner join er_login l on r.userid=l.userid where r.id=" + _EtM.intRechargeID + " order by r.id ASC limit 1;");
                                int intCount = Convert.ToInt32((ds.Tables[0].Rows.Count));
                                if (intCount > 0)
                                {

                                    int intInvestmentID = int.Parse(ds.Tables[0].Rows[0]["id"].ToString());
                                    int intUserID = int.Parse(ds.Tables[0].Rows[0]["userid"].ToString());
                                    string strTransactionNumber = ds.Tables[0].Rows[0]["id"].ToString();
                                    string strOperaterID = ds.Tables[0].Rows[0]["operator_id"].ToString();
                                    string strOperaterName = ds.Tables[0].Rows[0]["operator_name"].ToString();
                                    string strOperaterTableID = ds.Tables[0].Rows[0]["Operater_table_id"].ToString();
                                    double dblActualAmt = double.Parse(ds.Tables[0].Rows[0]["amount"].ToString());
                                    string strMobile = ds.Tables[0].Rows[0]["mobile_number"].ToString();
                                    string strPhone = ds.Tables[0].Rows[0]["request_mobile_no"].ToString();
                                    string strUserName = ds.Tables[0].Rows[0]["username"].ToString();
                                    string strFullname = ds.Tables[0].Rows[0]["full_name"].ToString();

                                    try
                                    {
                                        Recharge objRecharge = new Recharge();
                                        objRecharge.SendSMPPSMS(intUserID, strUserName, strMobile, strOperaterName, int.Parse(strOperaterID), dblActualAmt, 1, strTransactionNumber.ToString(), strPhone);
                                    }
                                    catch (Exception)
                                    {

                                    }
                                    try
                                    {
                                        SMPP_SMS_Format objSMPP_SMS_Format = new SMPP_SMS_Format();
                                        CommonFunction objCOMFUN = new CommonFunction();
                                        string strGetSMS = objSMPP_SMS_Format.TopupSuccessEndUser(strFullname, strMobile, dblActualAmt, intInvestmentID.ToString());
                                        objCOMFUN.er_SMS_Alert(intUserID, 2, strMobile, 2, strGetSMS, 9, "");
                                    }
                                    catch (Exception)
                                    {

                                    }
                                }
                            }
                            catch (Exception)
                            {

                            }
                            finally
                            {
                                ds.Dispose();
                            }
                        }
                        else if (Regex.IsMatch(_EtM.strEtisalatResponse, string.Format(@"\b{0}\b", Regex.Escape("ACNT:RETN=1099,DESC=S-ACT-00517,Call event rating time out"))))
                        {

                            string strResposne = TruncateAtWord(_EtM.strEtisalatResponse, 64); // Truncate From Position of Words
                            string output = _EtM.strEtisalatResponse.Replace(strResposne, ""); // Replace Words From Data
                            output = output.Remove(output.Length - 8); // Deduct Words from Last                   
                            string StrCheckSum = _EtM.strEtisalatResponse.Substring(_EtM.strEtisalatResponse.Length - 8); // Getting Checksum

                            string[] strDescriptionResponse = output.Split(new char[] { ',' });
                            // strMessage = "2,OOPs! You do not have sufficient balance.,Failed,NA,NA";


                            string TRDStatus = strDescriptionResponse[0];
                            string TRDDescription = strDescriptionResponse[1];


                            string TXNID = _EtM.strEtisalatMNOTransID;
                            try
                            {
                                objODBC.executeNonQuery("call `updateResponseV3`('" + TXNID + "','" + _EtM.intRechargeID + "',2)");
                            }
                            catch (Exception)
                            {

                            }
                            try
                            {
                                string strBalance = objODBC.executeScalar_str("SELECT amt FROM `er_sys_wallet` where id=2;");
                                objODBC.executeNonQuery("update er_recharge set comment='" + _EtM.strEtisalatResponse + "',operator_balance='" + strBalance + "' where id='" + _EtM.intRechargeID + "'");

                            }
                            catch (Exception)
                            {

                            }

                            try
                            {
                                objODBC.executeNonQuery("update etisalat_topup_request set transaction_DESC='" + TRDDescription + "',Transactio_Status='" + TRDStatus + "',Trans_Receive_Checksum='" + StrCheckSum + "',status=2 WHERE recharge_id =" + _EtM.intRechargeID + "");
                            }
                            catch (Exception)
                            {

                            }

                            try
                            {

                                ds = objODBC.getDataSet("Select r.id,r.userid,r.operator_id,r.operator_name,r.Operater_table_id,r.amount,r.mobile_number,r.request_mobile_no,l.username,l.full_name from er_recharge r inner join er_login l on r.userid=l.userid where r.id=" + _EtM.intRechargeID + " order by r.id ASC limit 1;");
                                int intCount = Convert.ToInt32((ds.Tables[0].Rows.Count));
                                if (intCount > 0)
                                {

                                    int intInvestmentID = int.Parse(ds.Tables[0].Rows[0]["id"].ToString());
                                    int intUserID = int.Parse(ds.Tables[0].Rows[0]["userid"].ToString());
                                    string strTransactionNumber = ds.Tables[0].Rows[0]["id"].ToString();
                                    string strOperaterID = ds.Tables[0].Rows[0]["operator_id"].ToString();
                                    string strOperaterName = ds.Tables[0].Rows[0]["operator_name"].ToString();
                                    string strOperaterTableID = ds.Tables[0].Rows[0]["Operater_table_id"].ToString();
                                    double dblActualAmt = double.Parse(ds.Tables[0].Rows[0]["amount"].ToString());
                                    string strMobile = ds.Tables[0].Rows[0]["mobile_number"].ToString();
                                    string strPhone = ds.Tables[0].Rows[0]["request_mobile_no"].ToString();
                                    string strUserName = ds.Tables[0].Rows[0]["username"].ToString();
                                    string strFullname = ds.Tables[0].Rows[0]["full_name"].ToString();

                                    try
                                    {
                                        Recharge objRecharge = new Recharge();
                                        objRecharge.SendSMPPSMS(intUserID, strUserName, strMobile, strOperaterName, int.Parse(strOperaterID), dblActualAmt, 2, strTransactionNumber.ToString(), strPhone);
                                    }
                                    catch (Exception)
                                    { }
                                    //try
                                    //{
                                    //    SMPP_SMS_Format objSMPP_SMS_Format = new SMPP_SMS_Format();
                                    //    CommonFunction objCOMFUN = new CommonFunction();
                                    //    string strGetSMS = objSMPP_SMS_Format.TopupFailedEndUser(objODBC.executeScalar_str("SELECT full_name From er_login Where userid='" + intUserID + "'").ToString(), strMobile, dblActualAmt, intInvestmentID.ToString());
                                    //    objCOMFUN.er_SMS_Alert(intUserID, 2, strMobile, 2, strGetSMS, 9, "");
                                    //}
                                    //catch (Exception)
                                    //{

                                    //}
                                }
                            }
                            catch (Exception)
                            {

                            }
                            finally
                            {
                                ds.Dispose();
                            }
                        }
                        else if (Regex.IsMatch(_EtM.strEtisalatResponse, string.Format(@"\b{0}\b", Regex.Escape("ACNT:RETN=1001,DESC=The user does not exist"))))
                        {

                            string strResposne = TruncateAtWord(_EtM.strEtisalatResponse, 64); // Truncate From Position of Words
                            string output = _EtM.strEtisalatResponse.Replace(strResposne, ""); // Replace Words From Data
                            output = output.Remove(output.Length - 8); // Deduct Words from Last                   
                            string StrCheckSum = _EtM.strEtisalatResponse.Substring(_EtM.strEtisalatResponse.Length - 8); // Getting Checksum

                            string[] strDescriptionResponse = output.Split(new char[] { ',' });
                            // strMessage = "2,OOPs! You do not have sufficient balance.,Failed,NA,NA";


                            string TRDStatus = strDescriptionResponse[0];
                            string TRDDescription = strDescriptionResponse[1];


                            string TXNID = _EtM.strEtisalatMNOTransID;
                            try
                            {
                                objODBC.executeNonQuery("call `updateResponseV3`('" + TXNID + "','" + _EtM.intRechargeID + "',2)");
                            }
                            catch (Exception)
                            {

                            }
                            try
                            {
                                string strBalance = objODBC.executeScalar_str("SELECT amt FROM `er_sys_wallet` where id=2;");
                                objODBC.executeNonQuery("update er_recharge set comment='" + _EtM.strEtisalatResponse + "',operator_balance='" + strBalance + "' where id='" + _EtM.intRechargeID + "'");

                            }
                            catch (Exception)
                            {

                            }

                            try
                            {
                                objODBC.executeNonQuery("update etisalat_topup_request set transaction_DESC='" + TRDDescription + "',Transactio_Status='" + TRDStatus + "',Trans_Receive_Checksum='" + StrCheckSum + "',status=2 WHERE recharge_id =" + _EtM.intRechargeID + "");
                            }
                            catch (Exception)
                            {

                            }

                            try
                            {

                                ds = objODBC.getDataSet("Select r.id,r.userid,r.operator_id,r.operator_name,r.Operater_table_id,r.amount,r.mobile_number,r.request_mobile_no,l.username,l.full_name from er_recharge r inner join er_login l on r.userid=l.userid where r.id=" + _EtM.intRechargeID + " order by r.id ASC limit 1;");
                                int intCount = Convert.ToInt32((ds.Tables[0].Rows.Count));
                                if (intCount > 0)
                                {

                                    int intInvestmentID = int.Parse(ds.Tables[0].Rows[0]["id"].ToString());
                                    int intUserID = int.Parse(ds.Tables[0].Rows[0]["userid"].ToString());
                                    string strTransactionNumber = ds.Tables[0].Rows[0]["id"].ToString();
                                    string strOperaterID = ds.Tables[0].Rows[0]["operator_id"].ToString();
                                    string strOperaterName = ds.Tables[0].Rows[0]["operator_name"].ToString();
                                    string strOperaterTableID = ds.Tables[0].Rows[0]["Operater_table_id"].ToString();
                                    double dblActualAmt = double.Parse(ds.Tables[0].Rows[0]["amount"].ToString());
                                    string strMobile = ds.Tables[0].Rows[0]["mobile_number"].ToString();
                                    string strPhone = ds.Tables[0].Rows[0]["request_mobile_no"].ToString();
                                    string strUserName = ds.Tables[0].Rows[0]["username"].ToString();
                                    string strFullname = ds.Tables[0].Rows[0]["full_name"].ToString();

                                    try
                                    {
                                        Recharge objRecharge = new Recharge();
                                        objRecharge.SendSMPPSMS(intUserID, strUserName, strMobile, strOperaterName, int.Parse(strOperaterID), dblActualAmt, 2, strTransactionNumber.ToString(), strPhone);
                                    }
                                    catch (Exception)
                                    { }
                                    //try
                                    //{
                                    //    SMPP_SMS_Format objSMPP_SMS_Format = new SMPP_SMS_Format();
                                    //    CommonFunction objCOMFUN = new CommonFunction();
                                    //    string strGetSMS = objSMPP_SMS_Format.TopupFailedEndUser(objODBC.executeScalar_str("SELECT full_name From er_login Where userid='" + intUserID + "'").ToString(), strMobile, dblActualAmt, intInvestmentID.ToString());
                                    //    objCOMFUN.er_SMS_Alert(intUserID, 2, strMobile, 2, strGetSMS, 9, "");
                                    //}
                                    //catch (Exception)
                                    //{

                                    //}
                                }
                            }
                            catch (Exception)
                            {

                            }
                            finally
                            {
                                ds.Dispose();
                            }
                        }
                        else if (Regex.IsMatch(_EtM.strEtisalatResponse, string.Format(@"\b{0}\b", Regex.Escape("ACNT:RETN=1006,DESC=The subscriber has been reported loss"))))
                        {

                            string strResposne = TruncateAtWord(_EtM.strEtisalatResponse, 64); // Truncate From Position of Words
                            string output = _EtM.strEtisalatResponse.Replace(strResposne, ""); // Replace Words From Data
                            output = output.Remove(output.Length - 8); // Deduct Words from Last                   
                            string StrCheckSum = _EtM.strEtisalatResponse.Substring(_EtM.strEtisalatResponse.Length - 8); // Getting Checksum

                            string[] strDescriptionResponse = output.Split(new char[] { ',' });
                            // strMessage = "2,OOPs! You do not have sufficient balance.,Failed,NA,NA";


                            string TRDStatus = strDescriptionResponse[0];
                            string TRDDescription = strDescriptionResponse[1];


                            string TXNID = _EtM.strEtisalatMNOTransID;
                            try
                            {
                                objODBC.executeNonQuery("call `updateResponseV3`('" + TXNID + "','" + _EtM.intRechargeID + "',2)");
                            }
                            catch (Exception)
                            {

                            }
                            try
                            {
                                string strBalance = objODBC.executeScalar_str("SELECT amt FROM `er_sys_wallet` where id=2;");
                                objODBC.executeNonQuery("update er_recharge set comment='" + _EtM.strEtisalatResponse + "',operator_balance='" + strBalance + "' where id='" + _EtM.intRechargeID + "'");

                            }
                            catch (Exception)
                            {

                            }

                            try
                            {
                                objODBC.executeNonQuery("update etisalat_topup_request set transaction_DESC='" + TRDDescription + "',Transactio_Status='" + TRDStatus + "',Trans_Receive_Checksum='" + StrCheckSum + "',status=2 WHERE recharge_id =" + _EtM.intRechargeID + "");
                            }
                            catch (Exception)
                            {

                            }

                            try
                            {

                                ds = objODBC.getDataSet("Select r.id,r.userid,r.operator_id,r.operator_name,r.Operater_table_id,r.amount,r.mobile_number,r.request_mobile_no,l.username,l.full_name from er_recharge r inner join er_login l on r.userid=l.userid where r.id=" + _EtM.intRechargeID + " order by r.id ASC limit 1;");
                                int intCount = Convert.ToInt32((ds.Tables[0].Rows.Count));
                                if (intCount > 0)
                                {

                                    int intInvestmentID = int.Parse(ds.Tables[0].Rows[0]["id"].ToString());
                                    int intUserID = int.Parse(ds.Tables[0].Rows[0]["userid"].ToString());
                                    string strTransactionNumber = ds.Tables[0].Rows[0]["id"].ToString();
                                    string strOperaterID = ds.Tables[0].Rows[0]["operator_id"].ToString();
                                    string strOperaterName = ds.Tables[0].Rows[0]["operator_name"].ToString();
                                    string strOperaterTableID = ds.Tables[0].Rows[0]["Operater_table_id"].ToString();
                                    double dblActualAmt = double.Parse(ds.Tables[0].Rows[0]["amount"].ToString());
                                    string strMobile = ds.Tables[0].Rows[0]["mobile_number"].ToString();
                                    string strPhone = ds.Tables[0].Rows[0]["request_mobile_no"].ToString();
                                    string strUserName = ds.Tables[0].Rows[0]["username"].ToString();
                                    string strFullname = ds.Tables[0].Rows[0]["full_name"].ToString();

                                    try
                                    {
                                        Recharge objRecharge = new Recharge();
                                        objRecharge.SendSMPPSMS(intUserID, strUserName, strMobile, strOperaterName, int.Parse(strOperaterID), dblActualAmt, 2, strTransactionNumber.ToString(), strPhone);
                                    }
                                    catch (Exception)
                                    { }
                                    //try
                                    //{
                                    //    SMPP_SMS_Format objSMPP_SMS_Format = new SMPP_SMS_Format();
                                    //    CommonFunction objCOMFUN = new CommonFunction();
                                    //    string strGetSMS = objSMPP_SMS_Format.TopupFailedEndUser(objODBC.executeScalar_str("SELECT full_name From er_login Where userid='" + intUserID + "'").ToString(), strMobile, dblActualAmt, intInvestmentID.ToString());
                                    //    objCOMFUN.er_SMS_Alert(intUserID, 2, strMobile, 2, strGetSMS, 9, "");
                                    //}
                                    //catch (Exception)
                                    //{

                                    //}
                                }
                            }
                            catch (Exception)
                            {

                            }
                            finally
                            {
                                ds.Dispose();
                            }
                        }
                        else if (Regex.IsMatch(_EtM.strEtisalatResponse, string.Format(@"\b{0}\b", Regex.Escape("ACNT:RETN=1099,DESC=S-ACT-00037,The payment is already exist"))))
                        {

                            string strResposne = TruncateAtWord(_EtM.strEtisalatResponse, 64); // Truncate From Position of Words
                            string output = _EtM.strEtisalatResponse.Replace(strResposne, ""); // Replace Words From Data
                            output = output.Remove(output.Length - 8); // Deduct Words from Last                   
                            string StrCheckSum = _EtM.strEtisalatResponse.Substring(_EtM.strEtisalatResponse.Length - 8); // Getting Checksum

                            string[] strDescriptionResponse = output.Split(new char[] { ',' });
                            // strMessage = "2,OOPs! You do not have sufficient balance.,Failed,NA,NA";


                            string TRDStatus = strDescriptionResponse[0];
                            string TRDDescription = strDescriptionResponse[1];


                            string TXNID = _EtM.strEtisalatMNOTransID;
                            try
                            {
                                objODBC.executeNonQuery("call `updateResponseV3`('" + TXNID + "','" + _EtM.intRechargeID + "',2)");
                            }
                            catch (Exception)
                            {

                            }
                            try
                            {
                                string strBalance = objODBC.executeScalar_str("SELECT amt FROM `er_sys_wallet` where id=2;");
                                objODBC.executeNonQuery("update er_recharge set comment='" + _EtM.strEtisalatResponse + "',operator_balance='" + strBalance + "' where id='" + _EtM.intRechargeID + "'");

                            }
                            catch (Exception)
                            {

                            }

                            try
                            {
                                objODBC.executeNonQuery("update etisalat_topup_request set transaction_DESC='" + TRDDescription + "',Transactio_Status='" + TRDStatus + "',Trans_Receive_Checksum='" + StrCheckSum + "',status=2 WHERE recharge_id =" + _EtM.intRechargeID + "");
                            }
                            catch (Exception)
                            {

                            }

                            try
                            {

                                ds = objODBC.getDataSet("Select r.id,r.userid,r.operator_id,r.operator_name,r.Operater_table_id,r.amount,r.mobile_number,r.request_mobile_no,l.username,l.full_name from er_recharge r inner join er_login l on r.userid=l.userid where r.id=" + _EtM.intRechargeID + " order by r.id ASC limit 1;");
                                int intCount = Convert.ToInt32((ds.Tables[0].Rows.Count));
                                if (intCount > 0)
                                {

                                    int intInvestmentID = int.Parse(ds.Tables[0].Rows[0]["id"].ToString());
                                    int intUserID = int.Parse(ds.Tables[0].Rows[0]["userid"].ToString());
                                    string strTransactionNumber = ds.Tables[0].Rows[0]["id"].ToString();
                                    string strOperaterID = ds.Tables[0].Rows[0]["operator_id"].ToString();
                                    string strOperaterName = ds.Tables[0].Rows[0]["operator_name"].ToString();
                                    string strOperaterTableID = ds.Tables[0].Rows[0]["Operater_table_id"].ToString();
                                    double dblActualAmt = double.Parse(ds.Tables[0].Rows[0]["amount"].ToString());
                                    string strMobile = ds.Tables[0].Rows[0]["mobile_number"].ToString();
                                    string strPhone = ds.Tables[0].Rows[0]["request_mobile_no"].ToString();
                                    string strUserName = ds.Tables[0].Rows[0]["username"].ToString();
                                    string strFullname = ds.Tables[0].Rows[0]["full_name"].ToString();

                                    try
                                    {
                                        Recharge objRecharge = new Recharge();
                                        objRecharge.SendSMPPSMS(intUserID, strUserName, strMobile, strOperaterName, int.Parse(strOperaterID), dblActualAmt, 2, strTransactionNumber.ToString(), strPhone);
                                    }
                                    catch (Exception)
                                    { }
                                    //try
                                    //{
                                    //    SMPP_SMS_Format objSMPP_SMS_Format = new SMPP_SMS_Format();
                                    //    CommonFunction objCOMFUN = new CommonFunction();
                                    //    string strGetSMS = objSMPP_SMS_Format.TopupFailedEndUser(objODBC.executeScalar_str("SELECT full_name From er_login Where userid='" + intUserID + "'").ToString(), strMobile, dblActualAmt, intInvestmentID.ToString());
                                    //    objCOMFUN.er_SMS_Alert(intUserID, 2, strMobile, 2, strGetSMS, 9, "");
                                    //}
                                    //catch (Exception)
                                    //{

                                    //}
                                }
                            }
                            catch (Exception)
                            {

                            }
                            finally
                            {
                                ds.Dispose();
                            }
                        }
                        else if (Regex.IsMatch(_EtM.strEtisalatResponse, string.Format(@"\b{0}\b", Regex.Escape("ACNT:RETN=1099,DESC=S-DAT-00010,error accurs getting connection!"))))
                        {

                            string strResposne = TruncateAtWord(_EtM.strEtisalatResponse, 64); // Truncate From Position of Words
                            string output = _EtM.strEtisalatResponse.Replace(strResposne, ""); // Replace Words From Data
                            output = output.Remove(output.Length - 8); // Deduct Words from Last                   
                            string StrCheckSum = _EtM.strEtisalatResponse.Substring(_EtM.strEtisalatResponse.Length - 8); // Getting Checksum

                            string[] strDescriptionResponse = output.Split(new char[] { ',' });
                            // strMessage = "2,OOPs! You do not have sufficient balance.,Failed,NA,NA";


                            string TRDStatus = strDescriptionResponse[0];
                            string TRDDescription = strDescriptionResponse[1];


                            string TXNID = _EtM.strEtisalatMNOTransID;
                            try
                            {
                                objODBC.executeNonQuery("call `updateResponseV3`('" + TXNID + "','" + _EtM.intRechargeID + "',2)");
                            }
                            catch (Exception)
                            {

                            }
                            try
                            {
                                string strBalance = objODBC.executeScalar_str("SELECT amt FROM `er_sys_wallet` where id=2;");
                                objODBC.executeNonQuery("update er_recharge set comment='" + _EtM.strEtisalatResponse + "',operator_balance='" + strBalance + "' where id='" + _EtM.intRechargeID + "'");

                            }
                            catch (Exception)
                            {

                            }

                            try
                            {
                                objODBC.executeNonQuery("update etisalat_topup_request set transaction_DESC='" + TRDDescription + "',Transactio_Status='" + TRDStatus + "',Trans_Receive_Checksum='" + StrCheckSum + "',status=2 WHERE recharge_id =" + _EtM.intRechargeID + "");
                            }
                            catch (Exception)
                            {

                            }

                            try
                            {

                                ds = objODBC.getDataSet("Select r.id,r.userid,r.operator_id,r.operator_name,r.Operater_table_id,r.amount,r.mobile_number,r.request_mobile_no,l.username,l.full_name from er_recharge r inner join er_login l on r.userid=l.userid where r.id=" + _EtM.intRechargeID + " order by r.id ASC limit 1;");
                                int intCount = Convert.ToInt32((ds.Tables[0].Rows.Count));
                                if (intCount > 0)
                                {

                                    int intInvestmentID = int.Parse(ds.Tables[0].Rows[0]["id"].ToString());
                                    int intUserID = int.Parse(ds.Tables[0].Rows[0]["userid"].ToString());
                                    string strTransactionNumber = ds.Tables[0].Rows[0]["id"].ToString();
                                    string strOperaterID = ds.Tables[0].Rows[0]["operator_id"].ToString();
                                    string strOperaterName = ds.Tables[0].Rows[0]["operator_name"].ToString();
                                    string strOperaterTableID = ds.Tables[0].Rows[0]["Operater_table_id"].ToString();
                                    double dblActualAmt = double.Parse(ds.Tables[0].Rows[0]["amount"].ToString());
                                    string strMobile = ds.Tables[0].Rows[0]["mobile_number"].ToString();
                                    string strPhone = ds.Tables[0].Rows[0]["request_mobile_no"].ToString();
                                    string strUserName = ds.Tables[0].Rows[0]["username"].ToString();
                                    string strFullname = ds.Tables[0].Rows[0]["full_name"].ToString();

                                    try
                                    {
                                        Recharge objRecharge = new Recharge();
                                        objRecharge.SendSMPPSMS(intUserID, strUserName, strMobile, strOperaterName, int.Parse(strOperaterID), dblActualAmt, 2, strTransactionNumber.ToString(), strPhone);
                                    }
                                    catch (Exception)
                                    { }
                                    //try
                                    //{
                                    //    SMPP_SMS_Format objSMPP_SMS_Format = new SMPP_SMS_Format();
                                    //    CommonFunction objCOMFUN = new CommonFunction();
                                    //    string strGetSMS = objSMPP_SMS_Format.TopupFailedEndUser(objODBC.executeScalar_str("SELECT full_name From er_login Where userid='" + intUserID + "'").ToString(), strMobile, dblActualAmt, intInvestmentID.ToString());
                                    //    objCOMFUN.er_SMS_Alert(intUserID, 2, strMobile, 2, strGetSMS, 9, "");
                                    //}
                                    //catch (Exception)
                                    //{

                                    //}
                                }
                            }
                            catch (Exception)
                            {

                            }
                            finally
                            {
                                ds.Dispose();
                            }
                        }
                        else if (Regex.IsMatch(_EtM.strEtisalatResponse, string.Format(@"\b{0}\b", Regex.Escape("ACNT:RETN=1099,DESC=S-ACT-00144,Fail to deal with the accumulation"))))
                        {

                            string strResposne = TruncateAtWord(_EtM.strEtisalatResponse, 64); // Truncate From Position of Words
                            string output = _EtM.strEtisalatResponse.Replace(strResposne, ""); // Replace Words From Data
                            output = output.Remove(output.Length - 8); // Deduct Words from Last                   
                            string StrCheckSum = _EtM.strEtisalatResponse.Substring(_EtM.strEtisalatResponse.Length - 8); // Getting Checksum

                            string[] strDescriptionResponse = output.Split(new char[] { ',' });
                            // strMessage = "2,OOPs! You do not have sufficient balance.,Failed,NA,NA";


                            string TRDStatus = strDescriptionResponse[0];
                            string TRDDescription = strDescriptionResponse[1];


                            string TXNID = _EtM.strEtisalatMNOTransID;
                            try
                            {
                                objODBC.executeNonQuery("call `updateResponseV3`('" + TXNID + "','" + _EtM.intRechargeID + "',2)");
                            }
                            catch (Exception)
                            {

                            }
                            try
                            {
                                string strBalance = objODBC.executeScalar_str("SELECT amt FROM `er_sys_wallet` where id=2;");
                                objODBC.executeNonQuery("update er_recharge set comment='" + _EtM.strEtisalatResponse + "',operator_balance='" + strBalance + "' where id='" + _EtM.intRechargeID + "'");

                            }
                            catch (Exception)
                            {

                            }

                            try
                            {
                                objODBC.executeNonQuery("update etisalat_topup_request set transaction_DESC='" + TRDDescription + "',Transactio_Status='" + TRDStatus + "',Trans_Receive_Checksum='" + StrCheckSum + "',status=2 WHERE recharge_id =" + _EtM.intRechargeID + "");
                            }
                            catch (Exception)
                            {

                            }

                            try
                            {

                                ds = objODBC.getDataSet("Select r.id,r.userid,r.operator_id,r.operator_name,r.Operater_table_id,r.amount,r.mobile_number,r.request_mobile_no,l.username,l.full_name from er_recharge r inner join er_login l on r.userid=l.userid where r.id=" + _EtM.intRechargeID + " order by r.id ASC limit 1;");
                                int intCount = Convert.ToInt32((ds.Tables[0].Rows.Count));
                                if (intCount > 0)
                                {

                                    int intInvestmentID = int.Parse(ds.Tables[0].Rows[0]["id"].ToString());
                                    int intUserID = int.Parse(ds.Tables[0].Rows[0]["userid"].ToString());
                                    string strTransactionNumber = ds.Tables[0].Rows[0]["id"].ToString();
                                    string strOperaterID = ds.Tables[0].Rows[0]["operator_id"].ToString();
                                    string strOperaterName = ds.Tables[0].Rows[0]["operator_name"].ToString();
                                    string strOperaterTableID = ds.Tables[0].Rows[0]["Operater_table_id"].ToString();
                                    double dblActualAmt = double.Parse(ds.Tables[0].Rows[0]["amount"].ToString());
                                    string strMobile = ds.Tables[0].Rows[0]["mobile_number"].ToString();
                                    string strPhone = ds.Tables[0].Rows[0]["request_mobile_no"].ToString();
                                    string strUserName = ds.Tables[0].Rows[0]["username"].ToString();
                                    string strFullname = ds.Tables[0].Rows[0]["full_name"].ToString();

                                    try
                                    {
                                        Recharge objRecharge = new Recharge();
                                        objRecharge.SendSMPPSMS(intUserID, strUserName, strMobile, strOperaterName, int.Parse(strOperaterID), dblActualAmt, 2, strTransactionNumber.ToString(), strPhone);
                                    }
                                    catch (Exception)
                                    { }
                                    //try
                                    //{
                                    //    SMPP_SMS_Format objSMPP_SMS_Format = new SMPP_SMS_Format();
                                    //    CommonFunction objCOMFUN = new CommonFunction();
                                    //    string strGetSMS = objSMPP_SMS_Format.TopupFailedEndUser(objODBC.executeScalar_str("SELECT full_name From er_login Where userid='" + intUserID + "'").ToString(), strMobile, dblActualAmt, intInvestmentID.ToString());
                                    //    objCOMFUN.er_SMS_Alert(intUserID, 2, strMobile, 2, strGetSMS, 9, "");
                                    //}
                                    //catch (Exception)
                                    //{

                                    //}
                                }
                            }
                            catch (Exception)
                            {

                            }
                            finally
                            {
                                ds.Dispose();
                            }
                        }
                        else if (Regex.IsMatch(_EtM.strEtisalatResponse, string.Format(@"\b{0}\b", Regex.Escape("ACNT:RETN=1099,DESC=S-DAT-00021,update error"))))
                        {

                            string strResposne = TruncateAtWord(_EtM.strEtisalatResponse, 64); // Truncate From Position of Words
                            string output = _EtM.strEtisalatResponse.Replace(strResposne, ""); // Replace Words From Data
                            output = output.Remove(output.Length - 8); // Deduct Words from Last                   
                            string StrCheckSum = _EtM.strEtisalatResponse.Substring(_EtM.strEtisalatResponse.Length - 8); // Getting Checksum

                            string[] strDescriptionResponse = output.Split(new char[] { ',' });
                            // strMessage = "2,OOPs! You do not have sufficient balance.,Failed,NA,NA";


                            string TRDStatus = strDescriptionResponse[0];
                            string TRDDescription = strDescriptionResponse[1];


                            string TXNID = _EtM.strEtisalatMNOTransID;
                            try
                            {
                                objODBC.executeNonQuery("call `updateResponseV3`('" + TXNID + "','" + _EtM.intRechargeID + "',2)");
                            }
                            catch (Exception)
                            {

                            }
                            try
                            {
                                string strBalance = objODBC.executeScalar_str("SELECT amt FROM `er_sys_wallet` where id=2;");
                                objODBC.executeNonQuery("update er_recharge set comment='" + _EtM.strEtisalatResponse + "',operator_balance='" + strBalance + "' where id='" + _EtM.intRechargeID + "'");

                            }
                            catch (Exception)
                            {

                            }

                            try
                            {
                                objODBC.executeNonQuery("update etisalat_topup_request set transaction_DESC='" + TRDDescription + "',Transactio_Status='" + TRDStatus + "',Trans_Receive_Checksum='" + StrCheckSum + "',status=2 WHERE recharge_id =" + _EtM.intRechargeID + "");
                            }
                            catch (Exception)
                            {

                            }

                            try
                            {

                                ds = objODBC.getDataSet("Select r.id,r.userid,r.operator_id,r.operator_name,r.Operater_table_id,r.amount,r.mobile_number,r.request_mobile_no,l.username,l.full_name from er_recharge r inner join er_login l on r.userid=l.userid where r.id=" + _EtM.intRechargeID + " order by r.id ASC limit 1;");
                                int intCount = Convert.ToInt32((ds.Tables[0].Rows.Count));
                                if (intCount > 0)
                                {

                                    int intInvestmentID = int.Parse(ds.Tables[0].Rows[0]["id"].ToString());
                                    int intUserID = int.Parse(ds.Tables[0].Rows[0]["userid"].ToString());
                                    string strTransactionNumber = ds.Tables[0].Rows[0]["id"].ToString();
                                    string strOperaterID = ds.Tables[0].Rows[0]["operator_id"].ToString();
                                    string strOperaterName = ds.Tables[0].Rows[0]["operator_name"].ToString();
                                    string strOperaterTableID = ds.Tables[0].Rows[0]["Operater_table_id"].ToString();
                                    double dblActualAmt = double.Parse(ds.Tables[0].Rows[0]["amount"].ToString());
                                    string strMobile = ds.Tables[0].Rows[0]["mobile_number"].ToString();
                                    string strPhone = ds.Tables[0].Rows[0]["request_mobile_no"].ToString();
                                    string strUserName = ds.Tables[0].Rows[0]["username"].ToString();
                                    string strFullname = ds.Tables[0].Rows[0]["full_name"].ToString();

                                    try
                                    {
                                        Recharge objRecharge = new Recharge();
                                        objRecharge.SendSMPPSMS(intUserID, strUserName, strMobile, strOperaterName, int.Parse(strOperaterID), dblActualAmt, 2, strTransactionNumber.ToString(), strPhone);
                                    }
                                    catch (Exception)
                                    { }
                                    //try
                                    //{
                                    //    SMPP_SMS_Format objSMPP_SMS_Format = new SMPP_SMS_Format();
                                    //    CommonFunction objCOMFUN = new CommonFunction();
                                    //    string strGetSMS = objSMPP_SMS_Format.TopupFailedEndUser(objODBC.executeScalar_str("SELECT full_name From er_login Where userid='" + intUserID + "'").ToString(), strMobile, dblActualAmt, intInvestmentID.ToString());
                                    //    objCOMFUN.er_SMS_Alert(intUserID, 2, strMobile, 2, strGetSMS, 9, "");
                                    //}
                                    //catch (Exception)
                                    //{

                                    //}
                                }
                            }
                            catch (Exception)
                            {

                            }
                            finally
                            {
                                ds.Dispose();
                            }
                        }
                        else if (Regex.IsMatch(_EtM.strEtisalatResponse, string.Format(@"\b{0}\b", Regex.Escape("ACNT:RETN=1099,DESC=S-ACT-00113,Fail to deal with the presen"))))
                        {

                            string strResposne = TruncateAtWord(_EtM.strEtisalatResponse, 64); // Truncate From Position of Words
                            string output = _EtM.strEtisalatResponse.Replace(strResposne, ""); // Replace Words From Data
                            output = output.Remove(output.Length - 8); // Deduct Words from Last                   
                            string StrCheckSum = _EtM.strEtisalatResponse.Substring(_EtM.strEtisalatResponse.Length - 8); // Getting Checksum

                            string[] strDescriptionResponse = output.Split(new char[] { ',' });
                            // strMessage = "2,OOPs! You do not have sufficient balance.,Failed,NA,NA";


                            string TRDStatus = strDescriptionResponse[0];
                            string TRDDescription = strDescriptionResponse[1];


                            string TXNID = _EtM.strEtisalatMNOTransID;
                            try
                            {
                                objODBC.executeNonQuery("call `updateResponseV3`('" + TXNID + "','" + _EtM.intRechargeID + "',2)");
                            }
                            catch (Exception)
                            {

                            }
                            try
                            {
                                string strBalance = objODBC.executeScalar_str("SELECT amt FROM `er_sys_wallet` where id=2;");
                                objODBC.executeNonQuery("update er_recharge set comment='" + _EtM.strEtisalatResponse + "',operator_balance='" + strBalance + "' where id='" + _EtM.intRechargeID + "'");

                            }
                            catch (Exception)
                            {

                            }

                            try
                            {
                                objODBC.executeNonQuery("update etisalat_topup_request set transaction_DESC='" + TRDDescription + "',Transactio_Status='" + TRDStatus + "',Trans_Receive_Checksum='" + StrCheckSum + "',status=2 WHERE recharge_id =" + _EtM.intRechargeID + "");
                            }
                            catch (Exception)
                            {

                            }

                            try
                            {

                                ds = objODBC.getDataSet("Select r.id,r.userid,r.operator_id,r.operator_name,r.Operater_table_id,r.amount,r.mobile_number,r.request_mobile_no,l.username,l.full_name from er_recharge r inner join er_login l on r.userid=l.userid where r.id=" + _EtM.intRechargeID + " order by r.id ASC limit 1;");
                                int intCount = Convert.ToInt32((ds.Tables[0].Rows.Count));
                                if (intCount > 0)
                                {

                                    int intInvestmentID = int.Parse(ds.Tables[0].Rows[0]["id"].ToString());
                                    int intUserID = int.Parse(ds.Tables[0].Rows[0]["userid"].ToString());
                                    string strTransactionNumber = ds.Tables[0].Rows[0]["id"].ToString();
                                    string strOperaterID = ds.Tables[0].Rows[0]["operator_id"].ToString();
                                    string strOperaterName = ds.Tables[0].Rows[0]["operator_name"].ToString();
                                    string strOperaterTableID = ds.Tables[0].Rows[0]["Operater_table_id"].ToString();
                                    double dblActualAmt = double.Parse(ds.Tables[0].Rows[0]["amount"].ToString());
                                    string strMobile = ds.Tables[0].Rows[0]["mobile_number"].ToString();
                                    string strPhone = ds.Tables[0].Rows[0]["request_mobile_no"].ToString();
                                    string strUserName = ds.Tables[0].Rows[0]["username"].ToString();
                                    string strFullname = ds.Tables[0].Rows[0]["full_name"].ToString();

                                    try
                                    {
                                        Recharge objRecharge = new Recharge();
                                        objRecharge.SendSMPPSMS(intUserID, strUserName, strMobile, strOperaterName, int.Parse(strOperaterID), dblActualAmt, 2, strTransactionNumber.ToString(), strPhone);
                                    }
                                    catch (Exception)
                                    { }
                                    //try
                                    //{
                                    //    SMPP_SMS_Format objSMPP_SMS_Format = new SMPP_SMS_Format();
                                    //    CommonFunction objCOMFUN = new CommonFunction();
                                    //    string strGetSMS = objSMPP_SMS_Format.TopupFailedEndUser(objODBC.executeScalar_str("SELECT full_name From er_login Where userid='" + intUserID + "'").ToString(), strMobile, dblActualAmt, intInvestmentID.ToString());
                                    //    objCOMFUN.er_SMS_Alert(intUserID, 2, strMobile, 2, strGetSMS, 9, "");
                                    //}
                                    //catch (Exception)
                                    //{

                                    //}
                                }
                            }
                            catch (Exception)
                            {

                            }
                            finally
                            {
                                ds.Dispose();
                            }
                        }
                        else if (Regex.IsMatch(_EtM.strEtisalatResponse, string.Format(@"\b{0}\b", Regex.Escape("ACNT:RETN=1099,DESC=S-ACT-00004,Fail to parse the calculated fee result"))))
                        {
                            // @@  @@ `SC`00981.00CCB00000PPSPHS0000000002DLGCONFFFF00000004TXEND FFFFACK:CHARGE CASH ACNT:RETN=1099,DESC=S-ACT-00004,Fail to parse the calculated fee result,BKID=0  F28FE7EB

                            string strResposne = TruncateAtWord(_EtM.strEtisalatResponse, 64); // Truncate From Position of Words
                            string output = _EtM.strEtisalatResponse.Replace(strResposne, ""); // Replace Words From Data
                            output = output.Remove(output.Length - 8); // Deduct Words from Last                   
                            string StrCheckSum = _EtM.strEtisalatResponse.Substring(_EtM.strEtisalatResponse.Length - 8); // Getting Checksum

                            string[] strDescriptionResponse = output.Split(new char[] { ',' });
                            // strMessage = "2,OOPs! You do not have sufficient balance.,Failed,NA,NA";


                            string TRDStatus = strDescriptionResponse[0];
                            string TRDDescription = strDescriptionResponse[1];


                            string TXNID = _EtM.strEtisalatMNOTransID;
                            try
                            {
                                objODBC.executeNonQuery("call `updateResponseV3`('" + TXNID + "','" + _EtM.intRechargeID + "',2)");
                            }
                            catch (Exception)
                            {

                            }
                            try
                            {
                                string strBalance = objODBC.executeScalar_str("SELECT amt FROM `er_sys_wallet` where id=2;");
                                objODBC.executeNonQuery("update er_recharge set comment='" + _EtM.strEtisalatResponse + "',operator_balance='" + strBalance + "' where id='" + _EtM.intRechargeID + "'");

                            }
                            catch (Exception)
                            {

                            }

                            try
                            {
                                objODBC.executeNonQuery("update etisalat_topup_request set transaction_DESC='" + TRDDescription + "',Transactio_Status='" + TRDStatus + "',Trans_Receive_Checksum='" + StrCheckSum + "',status=2 WHERE recharge_id =" + _EtM.intRechargeID + "");
                            }
                            catch (Exception)
                            {

                            }

                            try
                            {

                                ds = objODBC.getDataSet("Select r.id,r.userid,r.operator_id,r.operator_name,r.Operater_table_id,r.amount,r.mobile_number,r.request_mobile_no,l.username,l.full_name from er_recharge r inner join er_login l on r.userid=l.userid where r.id=" + _EtM.intRechargeID + " order by r.id ASC limit 1;");
                                int intCount = Convert.ToInt32((ds.Tables[0].Rows.Count));
                                if (intCount > 0)
                                {

                                    int intInvestmentID = int.Parse(ds.Tables[0].Rows[0]["id"].ToString());
                                    int intUserID = int.Parse(ds.Tables[0].Rows[0]["userid"].ToString());
                                    string strTransactionNumber = ds.Tables[0].Rows[0]["id"].ToString();
                                    string strOperaterID = ds.Tables[0].Rows[0]["operator_id"].ToString();
                                    string strOperaterName = ds.Tables[0].Rows[0]["operator_name"].ToString();
                                    string strOperaterTableID = ds.Tables[0].Rows[0]["Operater_table_id"].ToString();
                                    double dblActualAmt = double.Parse(ds.Tables[0].Rows[0]["amount"].ToString());
                                    string strMobile = ds.Tables[0].Rows[0]["mobile_number"].ToString();
                                    string strPhone = ds.Tables[0].Rows[0]["request_mobile_no"].ToString();
                                    string strUserName = ds.Tables[0].Rows[0]["username"].ToString();
                                    string strFullname = ds.Tables[0].Rows[0]["full_name"].ToString();

                                    try
                                    {
                                        Recharge objRecharge = new Recharge();
                                        objRecharge.SendSMPPSMS(intUserID, strUserName, strMobile, strOperaterName, int.Parse(strOperaterID), dblActualAmt, 2, strTransactionNumber.ToString(), strPhone);
                                    }
                                    catch (Exception)
                                    { }
                                    //try
                                    //{
                                    //    SMPP_SMS_Format objSMPP_SMS_Format = new SMPP_SMS_Format();
                                    //    CommonFunction objCOMFUN = new CommonFunction();
                                    //    string strGetSMS = objSMPP_SMS_Format.TopupFailedEndUser(objODBC.executeScalar_str("SELECT full_name From er_login Where userid='" + intUserID + "'").ToString(), strMobile, dblActualAmt, intInvestmentID.ToString());
                                    //    objCOMFUN.er_SMS_Alert(intUserID, 2, strMobile, 2, strGetSMS, 9, "");
                                    //}
                                    //catch (Exception)
                                    //{

                                    //}
                                }
                            }
                            catch (Exception)
                            {

                            }
                            finally
                            {
                                ds.Dispose();
                            }
                        }

                    }
                    else if (_EtM.TransType == 2) // Rollback 
                    {
                       // script will make this 
                    }
                });


                returnMessage = new HttpResponseMessage(HttpStatusCode.Created);
                returnMessage.RequestMessage = new HttpRequestMessage(HttpMethod.Post, message);
            }
            catch (Exception ex)
            {
                returnMessage = new HttpResponseMessage(HttpStatusCode.ExpectationFailed);
                returnMessage.RequestMessage = new HttpRequestMessage(HttpMethod.Post, ex.ToString());
            }
            finally
            {
                ds.Dispose();
            }

            return await Task.FromResult(returnMessage);
        }



        //[HttpPost]
        //public async Task<HttpResponseMessage> Post([FromBody]StudentModel model)
        //{
        //    Student _student = new Student();
        //    _student.StuId = model.StuId;
        //    _student.FirstName = model.FirstName;
        //    _student.Email = model.Email;
        //    _student.Cotact = model.Cotact;
        //    _student.City = model.City;

        //    HttpResponseMessage returnMessage = new HttpResponseMessage();


        //    try
        //    {
        //        // add details to database;
        //        string message = ($"Student Created - {_student.StuId}");
        //        returnMessage = new HttpResponseMessage(HttpStatusCode.Created);
        //        returnMessage.RequestMessage = new HttpRequestMessage(HttpMethod.Post, message);
        //    }
        //    catch (Exception ex)
        //    {
        //        returnMessage = new HttpResponseMessage(HttpStatusCode.ExpectationFailed);
        //        returnMessage.RequestMessage = new HttpRequestMessage(HttpMethod.Post, ex.ToString());
        //    }


        //    return await Task.FromResult(returnMessage);
        //}

    }
}