﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Etopup_Ws.Models
{
    public class EtisalatPendingModel
    {

        public string intRechargeID { get; set; }
        public string intEtisalatID { get; set; }
        public string strEtisalatResponse { get; set; }

        public string strEtisalatMNOTransID { get; set; }

        public string strCoustomerMobile { get; set; }

        public double dblAmount { get; set; }

        public string transaction_bytereceived { get; set; }

        public int TransType { get; set; }
    }
}