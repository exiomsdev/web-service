﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Etopup_Ws.Models
{
    public class SMS
    {
        public int intUserID { get; set; }
        public string strMobile { get; set; }
        public string strOperaterName { get; set; }       
        public int strOperaterID { get; set; }

        public double dblActualAmt { get; set; }

        public int intRechargeLogType { get; set; }

        public string TransactionID { get; set; }
        public string strPhone  { get; set; }
       
    }
}