﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Etopup_Ws.old_App_Code;
using System.Data;
using System.Collections;

namespace Etopup_Ws
{
    public partial class er_group_topup_10mscript : System.Web.UI.Page
    {
        ODBC objODBC = new ODBC(); Encrypt_Decrypt ED = new Encrypt_Decrypt();
        ODBC_LOG objODBCLOG = new ODBC_LOG();
        CreateFile objCreateFile = new CreateFile();
        CommonFunction objCOMFUN = new CommonFunction();
        Encryption objEncrypMD5SHA1 = new Encryption();
        old_App_Code.DateFormat objDateFor = new old_App_Code.DateFormat();
        DataTable dt_group = new DataTable();
        DataTable dt_member = new DataTable();
        Recharge objRchg = new Recharge();
        DateFormat objDF = new DateFormat();
        Encryption objEncrypt = new Encryption();
        DataSet dsData = new DataSet();
        CommonFunction objComFUN = new CommonFunction();
       
        protected void Page_Load(object sender, EventArgs e)
        {
         /*   try
            {
                DateFormat objDF = new DateFormat();
                objODBC.executeNonQuery("INSERT INTO `temp`(`data`, `type`, `created_on`, `request`) VALUES ('Windows Services ',3,'" + objDF.getCurDateTimeString() + "','er_group_topup_10mscript')");
            }
            catch (Exception)
            {

            }
            */
          try
            {
                getGroupTopup();
                sendRegistrationSMS();
                
            }
            catch (Exception)
            {

            }
            Response.ContentType = "text/plain";
            Response.Write("Exicute Successfully !");
            Response.End();
        }
        public void sendRegistrationSMS()
        {
            string strDate2 = objDF.getCurDateTimeString();
            try
            {

                try
                {
                    dsData = objODBC.getDataSet("SELECT l.username,l.full_name,l.password,l.M_Pin,l.pass_key,m.mobile,m.operator_id,l.userid FROM db_topup_quick_pay.er_login l inner join er_mobile m on l.userid=m.userid Where l.reg_sms_status=0 and m.mob_type=1 and m.usertype=2 And TIMESTAMPDIFF(MINUTE,l.created_on,'" + strDate2 + "')<=6  order by userid asc limit 10;");
                    if (dsData.Tables[0].Rows.Count > 0)
                    {
                        for (int i = 0; i < dsData.Tables[0].Rows.Count; i++)
                        {
                            string strPasswordDetail = objEncrypt.DecryptQueryString(dsData.Tables[0].Rows[i]["password"].ToString(), dsData.Tables[0].Rows[i]["pass_key"].ToString());
                            string strMPin = objEncrypt.DecryptQueryString(dsData.Tables[0].Rows[i]["M_Pin"].ToString(), dsData.Tables[0].Rows[i]["pass_key"].ToString());
                            // SMPP SMS Code Below
                            SMPP_SMS_Format objSMPP_SMS_Format = new SMPP_SMS_Format();
                            string strGetSMS = objSMPP_SMS_Format.MemberRegistration(dsData.Tables[0].Rows[i]["username"].ToString(), dsData.Tables[0].Rows[i]["full_name"].ToString(), strPasswordDetail, strMPin);
                            string strSMPPResponse = "NA";
                            try
                            {
                                //  objComFUN.WriteToSMSFile(intUserID, 2, dsData.Tables[0].Rows[i]["mobile"].ToString(), int.Parse(dsData.Tables[0].Rows[i]["operator_id"].ToString()), strGetSMS, 0, strSMPPResponse, strSMPPResponse, strSMPPResponse, strSMPPResponse, strSMPPResponse);
                                objComFUN.er_SMS_Alert(int.Parse(dsData.Tables[0].Rows[i]["userid"].ToString()), 2, dsData.Tables[0].Rows[i]["mobile"].ToString(), int.Parse(dsData.Tables[0].Rows[i]["operator_id"].ToString()), strGetSMS, 1, strSMPPResponse);
                                objODBC.executeNonQuery("Update er_login set reg_sms_status=1 where userid=" + int.Parse(dsData.Tables[0].Rows[i]["userid"].ToString()));
                            }
                            catch (Exception)
                            {

                            }
                        }
                    }

                }
                catch (Exception)
                {

                }
                finally
                {
                    dsData.Dispose();
                }
            }
            catch (Exception)
            {


            }
        }


       
        public void getGroupTopup()
        {
            try
            {

                dt_group = objODBC.getDataTable("select id,tran_id,groupid,parentid,mobile,operatorid,amount from er_group_topup_inprocess where status=0 order by id asc limit 0,10");


                if (dt_group.Rows.Count > 0)
                {
                    for (int i = 0; i < dt_group.Rows.Count; i++)
                    {
                        string strID = dt_group.Rows[i][0].ToString();
                        objODBC.executeNonQuery("update er_group_topup_inprocess set status=1 where id=" + strID + "");
                        string strTransID = dt_group.Rows[i][1].ToString();
                        string strGroupID = dt_group.Rows[i][2].ToString();
                        string strPID = dt_group.Rows[i][3].ToString();
                        string strMob = dt_group.Rows[i][4].ToString();
                        string stroperatorid = dt_group.Rows[i][5].ToString();
                        double strAmt = Convert.ToDouble(dt_group.Rows[i][6]);
                        string[] values = strMob.Split(',');
                        string strRechargeID = string.Empty;

                        for (int j = 0; j < values.Length; j++)
                        {
                            ArrayList arrUserParams = new ArrayList();
                            arrUserParams = objRchg.MakeMobileRechargeforGroupTopup(Convert.ToInt32(strPID), Convert.ToInt32(stroperatorid), values[j].Trim(), strAmt, int.Parse(strGroupID));

                            strRechargeID = arrUserParams[2].ToString();
                            string strDate = objDF.getCurDateTimeString();

                            objODBC.executeNonQuery("INSERT INTO `er_group_topup_done_details`(`parent_id`,`er_top_up_group_details_id`, `er_top_up_group_id`,`recharge_id`,`amount`,`created_on`) VALUES ('" + Convert.ToInt32(strPID) + "','" + strTransID + "','" + strGroupID + "','" + strRechargeID + "','" + strAmt + "','" + strDate + "')");

                        }

                        objODBC.executeNonQuery("update er_group_topup_inprocess set status=2,recharge_id=CONCAT(recharge_id, ',', '" + strRechargeID + "') where tran_id='" + strTransID + "'");


                    }

                }
            }
            catch (Exception)
            {
                               
            }
            finally
            {
                dt_group.Dispose();
            }
        }
    }
}