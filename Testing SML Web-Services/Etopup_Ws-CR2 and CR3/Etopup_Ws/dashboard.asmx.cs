﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Etopup_Ws.old_App_Code;
using System.Web.Services;
using System.Collections;
using System.Data;

namespace Etopup_Ws
{
    /// <summary>
    /// Summary description for dashboard
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class dashboard : System.Web.Services.WebService
    {
        ODBC objODBC = new ODBC();
        ODBC_LOG objODBCLOG = new ODBC_LOG();
        DataTable dt = new DataTable();
        DataTable dt2 = new DataTable();
        DateFormat objDT = new DateFormat();
        Encrypt_Decrypt ED = new Encrypt_Decrypt();
        CreateFile objCreateFile = new CreateFile();
        CommonFunction objCommonFunction = new CommonFunction();
        // ActivityType=63;
        [WebMethod]
        public ArrayList and_Retailer_Dashboard(string strUserName, int intUserID, string strIPAddress, int intDesignationType, string strMacAddress, string strOSDetails, string strIMEINo, string strGcmID, string strAPPVersion, int intSource, int intCreatedByType)
        {
            int intActivityType = 63;


            string strQuery = "";
            string strLimit = "";
            ArrayList arrUserParams = new ArrayList();

            try
            {

                arrUserParams = objCommonFunction.Get_ArrayLlist("Call getDashboardDetailsRetMob('" + intUserID + "')");


                objCreateFile.CreateTextFile(strIPAddress, strMacAddress, strOSDetails, strIMEINo, strGcmID, strAPPVersion, intSource.ToString(), intCreatedByType, intActivityType, "Android Retailer Dashboard Fetch Sucessfully", strUserName);

                return arrUserParams;
            }
            catch (Exception ex)
            {

                objCreateFile.CreateTextFile(strIPAddress, strMacAddress, strOSDetails, strIMEINo, strGcmID, strAPPVersion, intSource.ToString(), intCreatedByType, intActivityType, "Android Retailer Dashboard Fetch Failed", strUserName);

                return arrUserParams;
            }
        }

        //  int intActivityType = 66;
        [WebMethod]
        public DataSet GetSuperAdminDashboard(string strUserName, int intUserID, string strIPAddress, int intDesignationType, string strMacAddress, string strOSDetails, string strIMEINo, string strGcmID, string strAPPVersion, int intSource, int intCreatedByType)
        {
            int intActivityType = 66;
            string strMessage = "";

            string strQuery = "call getDashboardDetailsAdmin('" + intUserID + "');";

            System.Data.DataSet ds = new System.Data.DataSet();

            try
            {
                ds = objODBC.getDataSet(strQuery);

                if (ds.Tables[0].Rows.Count > 0)
                {
                    System.Data.DataTable firstTable = ds.Tables[0];
                    strMessage = "Get Admin Dashboard Fetch SuccessFully";



                    objCreateFile.CreateTextFile(strIPAddress, strMacAddress, strOSDetails, strIMEINo, strGcmID, strAPPVersion, intSource.ToString(), intCreatedByType, intActivityType, strMessage, strUserName);
                    return ds;
                }
                else
                {
                    strMessage = "Get Admin Dashboard Fetch Failed";


                    objCreateFile.CreateTextFile(strIPAddress, strMacAddress, strOSDetails, strIMEINo, strGcmID, strAPPVersion, intSource.ToString(), intCreatedByType, intActivityType, strMessage, strUserName);
                    return ds;

                }


            }
            catch (Exception ex)
            {
                strMessage = "Get Admin Dashboard Fetch Failed" + ex;

                objCreateFile.CreateTextFile(strIPAddress, strMacAddress, strOSDetails, strIMEINo, strGcmID, strAPPVersion, intSource.ToString(), intCreatedByType, intActivityType, strMessage, strUserName);
                return ds;
            }

            finally
            {


                ds.Dispose();
            }
        }

        [WebMethod]
        public ArrayList er_get_oprator_balance(string strUserName, int intUserID, string strIPAddress, int intDesignationType, string strMacAddress, string strOSDetails, string strIMEINo, string strGcmID, string strAPPVersion, int intSource, int intCreatedByType)
        {
            ArrayList alr = new ArrayList();
            BoloroApi objBoloroApi = new BoloroApi();
            EasyPay objEasyPay = new EasyPay();
            Roshan objRoshan = new Roshan();
            SalaamApi objSalaamApi = new SalaamApi();
            MTNApi objMTNAPI = new MTNApi();
            int intActivityType = 118;
            string strMessage = "operator balance";
            try
            {
                objCreateFile.CreateTextFile(strIPAddress, strMacAddress, strOSDetails, strIMEINo, strGcmID, strAPPVersion, intSource.ToString(), intCreatedByType, intActivityType, strMessage, strUserName);
            }
            catch (Exception)
            {


            }
            finally
            {
                try
                {
                    try
                    {
                        alr.Add("Salaam");
                        alr.Add(objSalaamApi.getSalaamBalance());
                       // alr.Add("0");
                    }
                    catch (Exception)
                    {

                    }
                    alr.Add("Etisalat");
                    alr.Add(objODBC.executeScalar_str("SELECT amt FROM er_sys_wallet where id=2;"));
                    try
                    {
                        alr.Add("RoshanBalance");
                        alr.Add(objRoshan.getRoshanBalanceBalanceAPI());
                        // alr.Add("0");
                    }
                    catch (Exception)
                    {

                    }
                    try
                    {
                        alr.Add("EasyPayBalance");
                        // alr.Add(objEasyPay.getEasyPayBalanceAPI());
                        alr.Add(objBoloroApi.getBoloroBalance());
                        // alr.Add("0");
                    }
                    catch (Exception)
                    {

                    }
                    try
                    {
                        alr.Add("BoloroBalance");
                        alr.Add(objBoloroApi.getBoloroBalance());
                        // alr.Add("0");
                    }
                    catch (Exception)
                    {

                    }
                    try
                    {
                        alr.Add("MTNAPIBalance");
                        alr.Add(objMTNAPI.GetMTNAPIBalance());
                       // alr.Add("0");
                    }
                    catch (Exception)
                    {

                    }
                }
                catch (Exception)
                {
                    alr.Add("FALSE");
                    alr.Add("somthing went wrong");

                }

            }

            return alr;
        }


        // ActivityType=120;
        [WebMethod]
        public ArrayList and_Distributer_Dashboard(string strUserName, int intUserID, string strIPAddress, int intDesignationType, string strMacAddress, string strOSDetails, string strIMEINo, string strGcmID, string strAPPVersion, int intSource, int intCreatedByType)
        {
            int intActivityType = 63;


            string strQuery = "";
            string strLimit = "";
            ArrayList arrUserParams = new ArrayList();

            try
            {
                arrUserParams = objCommonFunction.Get_ArrayLlist("Call getDashboardDetailsDistMob('" + intUserID + "')");

                objCreateFile.CreateTextFile(strIPAddress, strMacAddress, strOSDetails, strIMEINo, strGcmID, strAPPVersion, intSource.ToString(), intCreatedByType, intActivityType, "Android Distributer Dashboard Fetch Sucessfully", strUserName);

                return arrUserParams;
            }
            catch (Exception ex)
            {

                objCreateFile.CreateTextFile(strIPAddress, strMacAddress, strOSDetails, strIMEINo, strGcmID, strAPPVersion, intSource.ToString(), intCreatedByType, intActivityType, "Android Distributer Dashboard Fetch Failed", strUserName);

                return arrUserParams;
            }
        }

        // ActivityType=60
        [WebMethod]
        public ArrayList android_AllTopupStatusReport(string strUserName, int intUserID, string strSearch, int intPage, int PageSize, string strIPAddress, int intDesignationType, string strMacAddress, string strOSDetails, string strIMEINo, string strGcmID, string strAPPVersion, int intSource, int intCreatedByType)
        {
            ArrayList arrUserParams = new ArrayList();
            string strQuery = "";
            string strLimit = "";
            DataSet ds = new DataSet();

            try
            {
                // string strCount = objODBC.executeScalar_str("select count(1) From er_login Where Active=1 and and userid='" + intUserID + "' " + strSearch + "");
                //" + strCount + "

                strQuery = "SELECT  salaam_status,etisalat_status,roshan_status,mtn_status,awcc_status From er_login Where Active=1 and userid='" + intUserID + "' ORDER BY userid DESC ";

                arrUserParams = objCommonFunction.Get_ArrayLlist(strQuery);

                objCreateFile.CreateTextFile(strIPAddress, strMacAddress, strOSDetails, strIMEINo, strGcmID, strAPPVersion, intSource.ToString(), intCreatedByType, intDesignationType, "android All Topup Status Report", strUserName);


                return arrUserParams;
            }
            catch (Exception ex)
            {

                objCreateFile.CreateTextFile(strIPAddress, strMacAddress, strOSDetails, strIMEINo, strGcmID, strAPPVersion, intSource.ToString(), intCreatedByType, intDesignationType, "android All Topup Status Report", strUserName);


                return arrUserParams;
            }
            finally
            {
            }
        }


        // ActivityType=120;
        [WebMethod]
        public ArrayList Web_Distributer_Dashboard(string strUserName, int intUserID, string strIPAddress, int intDesignationType, string strMacAddress, string strOSDetails, string strIMEINo, string strGcmID, string strAPPVersion, int intSource, int intCreatedByType)
        {
            int intActivityType = 63;

            string strQuery = "";
            string strLimit = "";
            ArrayList arrUserParams = new ArrayList();

            try
            {
                arrUserParams = objCommonFunction.Get_ArrayLlist("Call getDashboardDetailsDistWeb('" + intUserID + "')");

                objCreateFile.CreateTextFile(strIPAddress, strMacAddress, strOSDetails, strIMEINo, strGcmID, strAPPVersion, intSource.ToString(), intCreatedByType, intActivityType, "Android Distributer Dashboard Fetch Sucessfully", strUserName);

                return arrUserParams;
            }
            catch (Exception ex)
            {

                objCreateFile.CreateTextFile(strIPAddress, strMacAddress, strOSDetails, strIMEINo, strGcmID, strAPPVersion, intSource.ToString(), intCreatedByType, intActivityType, "Android Distributer Dashboard Fetch Failed", strUserName);

                return arrUserParams;
            }
        }
    }
}
