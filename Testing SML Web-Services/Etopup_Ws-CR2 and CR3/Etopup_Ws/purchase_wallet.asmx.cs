﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Etopup_Ws.old_App_Code;
using System.Web.Services;
using System.Collections;
using System.Data;
using System.IO;

namespace Etopup_Ws
{
    /// <summary>
    /// Summary description for purchase_wallet1
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class purchase_wallet1 : System.Web.Services.WebService
    {
        ODBC objODBC = new ODBC();
        ODBC_LOG objODBCLOG = new ODBC_LOG();
        CreateFile objCreateFile = new CreateFile();
        DataTable dt = new DataTable();
        Encrypt_Decrypt ED = new Encrypt_Decrypt();
        DateFormat objDT = new DateFormat();
        CommonFunction objComFun = new CommonFunction();
        // WalletFunction objWalletFN = new WalletFunction();\

        // Activity Type=25
        // int intUserID=0, int dblRequestAmount=1, int intMOP=2, string strReferenceNumber=3, string strRecptURL=4, string strComment=5, string strIPAddress=6, int intDesignationType=7, string strMacAddress=8, string strOSDetails=9, string strIMEINo=10, string strGcmID=11, string strAPPVersion=12, int intSource=13, int intCreatedByType=14)
        [WebMethod]
        public string ap_PurchaseWallet(string strUserName, string strData, int intType, int intSource, string strBase64Image, int intUtype)
        {
            string strMessage = "";
            byte[] imageBytes;
            string imgURl = "";
            string strGenerateRandumName = "NoImageAvailable.png";

            var data = ED.Decrypt(strUserName, strData, intUtype, 1, intSource).Split(',');
            try
            {
                int intUserID = int.Parse(data[0]);
                int dblRequestAmount = int.Parse(data[1]);
                int intMOP = int.Parse(data[2]);
                string strReferenceNumber = data[3];
                string strRecptURL = data[4];
                string strComment = data[5];
                string strIPAddress = data[6];
                int intDesignationType = int.Parse(data[7]);
                string strMacAddress = data[8];
                string strOSDetails = data[9];
                string strIMEINo = data[10];
                string strGcmID = data[11];
                string strAPPVersion = data[12];
                int intSourceData = int.Parse(data[13]);
                int intCreatedByType = int.Parse(data[14]);

                try
                {
                    objCreateFile.CreateTextFile(strIPAddress, strMacAddress, strOSDetails, strIMEINo, strGcmID, strAPPVersion, intSourceData.ToString(), intCreatedByType, 25, "Purchase Wallet, Data: " + strData, strUserName);
                }
                catch (Exception)
                {

                }


                if (((intUserID > 0)) && (dblRequestAmount > 0) && (intMOP != 0) && (strComment != "" || strComment != null))
                {
                    int intStatus = objODBC.executeScalar_int("SELECT `status` FROM `er_status_manager` WHERE id=1");
                    if (intStatus == 1)
                    {
                        string strTransacctionNumber = objDT.getCurDateTime_Dayanamic_format("yyyymmdd").ToString() + objComFun.GenrateNumberString();

                        string strDate = objDT.getCurDateTimeString();

                        int intchkDuplicateRecgarge = objODBC.executeScalar_int("Select Count(1) From er_wallet_purchase where userid=" + intUserID + " And TIMESTAMPDIFF(MINUTE,created_on,'" + strDate + "') <=5");

                        if (intchkDuplicateRecgarge == 0)
                        {
                            int intParentID = objODBC.executeScalar_int("SELECT parent_id From er_login Where userid=" + intUserID);
                            try
                            {
                                strGenerateRandumName = objComFun.GenrateRandomString() + strTransacctionNumber.ToString() + intUserID.ToString();
                            }
                            catch (Exception)
                            {

                            }

                            try
                            {
                                if (strBase64Image != null && strBase64Image != "")
                                {

                                    strGenerateRandumName = strGenerateRandumName + ".jpg";
                                    imageBytes = Convert.FromBase64String(strBase64Image);
                                    MemoryStream ms = new MemoryStream(imageBytes, 0, imageBytes.Length);
                                    ms.Write(imageBytes, 0, imageBytes.Length);
                                    System.Drawing.Image image = System.Drawing.Image.FromStream(ms, true);
                                    image.Save(Server.MapPath("~/PurchaseWalletRequestImage/" + strGenerateRandumName));
                                    imgURl = "http://topup.setaraganmutahed.com/PurchaseWalletRequestImage/" + strGenerateRandumName;
                                }
                                else
                                {
                                    strGenerateRandumName = "NoImageAvailable.png";
                                }

                            }
                            catch (Exception)
                            {

                            }


                            string strQuery = "INSERT INTO er_wallet_purchase (trans_number,userid,parent_id,request_amt,payment_mode,reference_no,rcpt_url,created_on,comment) VALUES ('" + strTransacctionNumber + "'," + intUserID + "," + intParentID + "," + dblRequestAmount + "," + intMOP + ",'" + strReferenceNumber + "','" + strGenerateRandumName + "','" + strDate + "','" + strComment + "')";

                            objODBC.executeNonQuery(strQuery);

                            strMessage = "Wallet Purchase Request successfully added.";

                            // SMPP SMS Code Below
                            SMPP_SMS_Format objSMPP_SMS_Format = new SMPP_SMS_Format();
                            string strGetSMS = objSMPP_SMS_Format.FundRequestWallet(strUserName, objODBC.executeScalar_str("SELECT full_name From er_login Where userid='" + intUserID + "'").ToString(), intSource, dblRequestAmount, strTransacctionNumber, intMOP).ToString();

                            //   objComFun.er_insert_notification(intUserID, "Fund Request", strGetSMS,2);



                            int intPIDI = objODBC.executeScalar_int("SELECT parent_id From er_login Where userid='" + intUserID + "'");
                            string strGetSMSParent = objSMPP_SMS_Format.FundRequestWalletParent(objODBC.executeScalar_str("SELECT full_name From er_login Where userid='" + intPIDI + "'").ToString(), intSource, dblRequestAmount, strTransacctionNumber, intMOP, strUserName, objODBC.executeScalar_str("SELECT full_name From er_login Where userid='" + intUserID + "'").ToString()).ToString();

                            objComFun.er_insert_notification(intPIDI, "Fund Request", strGetSMSParent, intCreatedByType);
                            SMPP1 objSMPP1 = new SMPP1();
                            string strSMPPResponse = "NA";
                            DataTable dtSMPP = new DataTable();
                            try
                            {


                                dtSMPP = objODBC.getDataTable("Select mobile,operator_id from er_mobile where userid='" + intUserID + "' and usertype=2 and mob_type=1");

                                if (dtSMPP.Rows.Count > 0)
                                {
                                    string strMobile = dtSMPP.Rows[0][0].ToString();
                                    int intOperaterID = int.Parse(dtSMPP.Rows[0][1].ToString());

                                    objComFun.er_SMS_Alert(intUserID, 2, strMobile, intOperaterID, strGetSMS, 1, strSMPPResponse);

                                }
                            }
                            catch (Exception)
                            {

                            }
                            finally
                            {
                                dtSMPP.Dispose();
                            }


                        }
                        else
                        {
                            strMessage = "You have recently requested for Wallet Purchase ! Kindly try again after sometime !";
                        }
                        try
                        {
                            objODBCLOG.executeNonQuery("call activity_logs('" + intUserID + "','" + intDesignationType + "','" + intCreatedByType + "','" + strIPAddress + "','" + strMacAddress + "','" + strOSDetails + "','" + strIMEINo + "','" + strGcmID + "','" + strAPPVersion + "','" + intSourceData + "',' Purchase Wallet Request',25,'','"+ strUserName + "')");


                        }
                        catch (Exception)
                        {

                        }
                    }
                    else
                    {
                        strMessage = "Permission denied by system !";
                    }
                }
                else
                {
                    strMessage = "Kindly Enter Valid Details !";
                }

                return strMessage;
            }
            catch (Exception ex)
            {
                strMessage = ex.Message.ToString();

                return strMessage;
            }
        }

        //----------------------

        // Activity Type=26
        [WebMethod]
        public double ReturnWalletAmount(string strUserName, int intUserID, string strIPAddress, int intDesignationType, string strMacAddress, string strOSDetails, string strIMEINo, string strGcmID, string strAPPVersion, int intSource, int intCreatedByType)
        {
            double amount = 0;
            try
            {
                if ((intUserID > 0))
                {
                    amount = objODBC.executeScalar_dbl("SELECT ex_wallet FROM er_wallet WHERE userid=" + intUserID);

                }
                else
                {
                    amount = 0.0;
                }
                return amount;
            }
            catch (Exception ex)
            {
                return 0.0;
            }
        }

        // Activity Type=27
        [WebMethod]
        public string ReturnWalletAmountByUserName(string strUsername, string strIPAddress, int intDesignationType, string strMacAddress, string strOSDetails, string strIMEINo, string strGcmID, string strAPPVersion, int intSource, int intCreatedByType)
        {
            string strMessage = "";
            try
            {
                if ((intDesignationType != 0) && (strIPAddress != "" || strIPAddress != null) && (strMacAddress != "" || strMacAddress != null) && (strOSDetails != "" || strOSDetails != null) && (strIMEINo != "" || strIMEINo != null) && (strGcmID != "" || strGcmID != null) && (strAPPVersion != "" || strAPPVersion != null) && (strUsername != "" || strUsername != null) && (intSource != 0) && (intCreatedByType != 0))
                {
                    int intUserID = objODBC.executeScalar_int("SELECT userid From er_login Where username='" + strUsername + "'");
                    strMessage = GetWalletAmount(intUserID).ToString();



                    objCreateFile.CreateTextFile(strIPAddress, strMacAddress, strOSDetails, strIMEINo, strGcmID, strAPPVersion, intSource.ToString(), intCreatedByType, 27, " Return Wallet Amount By UserName Successfully ", strUsername);
                }
                else
                {
                    strMessage = "Kindly Enter Valid Details !";
                }
            }
            catch (Exception ex)
            {
                strMessage = ex.Message.ToString();
            }
            return strMessage;
        }


        public double GetWalletAmount(int intUserID)
        {
            double amount = 0;
            try
            {
                amount = objODBC.executeScalar_dbl("SELECT ex_wallet FROM er_wallet WHERE userid=" + intUserID);
                return amount;
            }
            catch (Exception ex)
            {
                return 0.0;
            }
        }

        // Activity Type=28
        [WebMethod]
        public double ReturnThresholdAmount(string strUserName, int intUserID, string strIPAddress, int intDesignationType, string strMacAddress, string strOSDetails, string strIMEINo, string strGcmID, string strAPPVersion, int intSource, int intCreatedByType)
        /* TODO ERROR: Skipped SkippedTokensTrivia */
        {
            double amount = 0;
            string strMessage = string.Empty;
            try
            {
                if ((intDesignationType != 0) && (strIPAddress != "" || strIPAddress != null) && (strMacAddress != "" || strMacAddress != null) && (strOSDetails != "" || strOSDetails != null) && (strIMEINo != "" || strIMEINo != null) && (strGcmID != "" || strGcmID != null) && (strAPPVersion != "" || strAPPVersion != null) && (intSource != 0) && (intCreatedByType != 0))
                {

                    objCreateFile.CreateTextFile(strIPAddress, strMacAddress, strOSDetails, strIMEINo, strGcmID, strAPPVersion, intSource.ToString(), intCreatedByType, 28, " Return Threshold Amount Successfully", strUserName);


                    amount = objODBC.executeScalar_dbl("SELECT min_wallet FROM er_wallet WHERE userid=" + intUserID);
                }
                else
                {
                    strMessage = "Kindly Enter Valid Details !";
                }
                return amount;
            }
            catch (Exception ex)
            {
                return 0.0;
            }
        }

        // Activity Type=29
        [WebMethod]
        public double ReturnUsableWalletAmount(string strUserName, int intUserID, string strIPAddress, int intDesignationType, string strMacAddress, string strOSDetails, string strIMEINo, string strGcmID, string strAPPVersion, int intSource, int intCreatedByType)
        /* TODO ERROR: Skipped SkippedTokensTrivia */
        {
            double amount = 0;
            string strMessage = string.Empty;
            try
            {
                if ((intUserID > 0) && (intDesignationType != 0) && (strIPAddress != "" || strIPAddress != null) && (strMacAddress != "" || strMacAddress != null) && (strOSDetails != "" || strOSDetails != null) && (strIMEINo != "" || strIMEINo != null) && (strGcmID != "" || strGcmID != null) && (strAPPVersion != "" || strAPPVersion != null) && (intSource != 0) && (intCreatedByType != 0))
                {


                    objCreateFile.CreateTextFile(strIPAddress, strMacAddress, strOSDetails, strIMEINo, strGcmID, strAPPVersion, intSource.ToString(), intCreatedByType, 29, "Return Usable Wallet Amount Successfully", strUserName);

                    amount = objODBC.executeScalar_dbl("SELECT (ex_wallet-min_wallet) as amount FROM er_wallet WHERE userid=" + intUserID);

                }
                else
                {
                    strMessage = "Kindly Enter Valid Details !";
                }
                return amount;
            }
            catch (Exception ex)
            {
                return 0.0;
            }
        }

        // Activity Type=30
        // int intSenderID=0, int intUserID=1, double dblTransferAmount=2, string strIPAddress=3, int intDesignationType=4, string strMacAddress=5, string strOSDetails=6, string strIMEINo=7, string strGcmID=8, string strAPPVersion=9, int intSource=10, int intCreatedByType=11,int intActualUserID=12,double dblSalaamWallet=13,double dblEtisalatWallet=14,double dblRoshanWallet=15,double dblMTNWallet=16,double dblAWCCWallet =17)

        [WebMethod]
        public string ExAdminWalletFundTransfer(string strUserName, string strData, int intType, int intSource, int intUtype)
        {
            string strMessage = "";
            var data = ED.Decrypt(strUserName, strData, intUtype, 1, intSource).Split(',');
            try
            {

                int intSenderID = int.Parse(data[0]);
                int intUserID = int.Parse(data[1]);
                double dblTransferAmount = double.Parse(data[2]);
                string strIPAddress = data[3];
                int intDesignationType = int.Parse(data[4]);
                string strMacAddress = data[5];
                string strOSDetails = data[6];
                string strIMEINo = data[7];
                string strGcmID = data[8];
                string strAPPVersion = data[9];
                int intSourceData = int.Parse(data[10]);
                int intCreatedByType = int.Parse(data[11]);
                int intActualUserID = int.Parse(data[12]);


                double dblSalaamWallet = double.Parse(data[13]);
                double dblEtisalatWallet = double.Parse(data[14]);
                double dblRoshanWallet = double.Parse(data[15]);
                double dblMTNWallet = double.Parse(data[16]);
                double dblAWCCWallet = double.Parse(data[17]);


                try
                {

                    objCreateFile.CreateTextFile(strIPAddress, strMacAddress, strOSDetails, strIMEINo, strGcmID, strAPPVersion, intSourceData.ToString(), intCreatedByType, 30, " Wallet Fund Transfer" + ",Data: " + strData, strUserName);
                }
                catch (Exception)
                {

                }
                try
                {
                    if ((intSenderID != intUserID) && (intUserID > 0) && (dblTransferAmount > 0) && (intSenderID > 0) && (intDesignationType != 0) && (strIPAddress != "" || strIPAddress != null) && (strMacAddress != "" || strMacAddress != null) && (strOSDetails != "" || strOSDetails != null) && (strIMEINo != "" || strIMEINo != null) && (strGcmID != "" || strGcmID != null) && (strAPPVersion != "" || strAPPVersion != null) && (intSourceData != 0) && (intCreatedByType != 0))
                    {
                        try
                        {
                            objODBCLOG.executeNonQuery("call activity_logs('" + intActualUserID + "','" + intDesignationType + "','" + intCreatedByType + "','" + strIPAddress + "','" + strMacAddress + "','" + strOSDetails + "','" + strIMEINo + "','" + strGcmID + "','" + strAPPVersion + "','" + intSourceData + "',' Wallet Fund Transfer ',30,'','" + strUserName + "')");

                            objCreateFile.CreateTextFile(strIPAddress, strMacAddress, strOSDetails, strIMEINo, strGcmID, strAPPVersion, intSourceData.ToString(), intCreatedByType, 30, " Wallet Fund Transfer" + ",Data: " + strData, strUserName);
                        }
                        catch (Exception)
                        {

                        }
                        int intStatus = objODBC.executeScalar_int("SELECT `status` FROM `er_status_manager` WHERE id=1");
                        if (intStatus == 1)
                        {
                            int intCount = objODBC.executeScalar_int("Select count(1) from er_login where userid='" + intUserID + "' and user_status=1 and Active=1 and parent_id='" + intSenderID + "'");
                            if (intCount > 0)
                            {
                                string strDate2 = objDT.getCurDateTimeString();

                                int intCountCheckTransaction = objODBC.executeScalar_int("SELECT count(1) FROM `er_wallet_transfer_individual` WHERE parent_id='" + intSenderID + "' and userid='" + intUserID + "' and transfer_amt='" + dblTransferAmount + "' and TIMESTAMPDIFF(MINUTE,created_on,'" + strDate2 + "')<=5");
                                if (intCountCheckTransaction == 0)
                                {
                                    double dblUserBalance = ReturnUsableWalletAmount(intSenderID);

                                    if (dblUserBalance >= dblTransferAmount)
                                    {

                                        try
                                        {
                                            dt = objODBC.getDataTable("Call ap_Admin_walletTransfer(" + intSenderID + "," + intUserID + "," + dblTransferAmount + ",'" + dblSalaamWallet + "','" + dblEtisalatWallet + "','" + dblRoshanWallet + "','" + dblMTNWallet + "','" + dblAWCCWallet + "')");

                                            if (dt.Rows[0]["Response"].ToString() == "Success")
                                            {
                                                string intTransID = dt.Rows[0]["intTransID"].ToString();
                                                strMessage = dt.Rows[0]["FeildMessage"].ToString();

                                                double dblSenderBalance = GetWalletAmount(intSenderID);
                                                double dblReceiverBalance = GetWalletAmount(intUserID);
                                                // SMPP1 SMS Send
                                                DataSet ds = new DataSet();
                                                try
                                                {

                                                    ds = objODBC.getDataSet("SELECT l.full_name,l.username,m.mobile,m.operator_id From er_login l inner join er_mobile m on l.userid=m.userid Where l.userid='" + intSenderID + "' and m.usertype='" + intCreatedByType + "' and m.mob_type=1");
                                                    if (ds.Tables[0].Rows.Count > 0)
                                                    {
                                                        string strSenderFullName = ds.Tables[0].Rows[0][0].ToString();
                                                        string strSenderUserName = ds.Tables[0].Rows[0][1].ToString();
                                                        string strSenderMobileNumber = ds.Tables[0].Rows[0][2].ToString();
                                                        int strSenderOperaterID = int.Parse(ds.Tables[0].Rows[0][3].ToString());


                                                        // SMPP SMS Code Below Sender
                                                        SMPP_SMS_Format objSMPP_SMS_Format = new SMPP_SMS_Format();
                                                        string strGetSMS = objSMPP_SMS_Format.FundTransferWalletSender(strUserName, objODBC.executeScalar_str("SELECT full_name From er_login Where userid='" + intUserID + "'").ToString(), intSource, dblTransferAmount, strSenderFullName, strSenderUserName.Trim(), dblSenderBalance, intTransID).ToString();
                                                        objComFun.er_insert_notification(intSenderID, "Fund Request", strGetSMS, intCreatedByType);
                                                        
                                                        string strSMPPResponse = "NA";
                                                        try
                                                        {
                                                           // objODBCLOG.executeNonQuery("INSERT INTO `er_wallet_transfer_sms_log`(`userid`, `username`, `mobile`, `operator_id`, `SMS`, `created_on`) VALUES ('"+ intSenderID + "','"+ strUserName + "','"+ strSenderMobileNumber + "','"+ strSenderOperaterID + "','"+ strGetSMS + "','"+ strDate2 + "')");
                                                            objComFun.er_SMS_Alert(intSenderID, 2, strSenderMobileNumber, strSenderOperaterID, strGetSMS, 1, strSMPPResponse);

                                                        }
                                                        catch (Exception)
                                                        {
                                                            // objComFun.er_SMS_Alert(intSenderID, 2, strSenderMobileNumber, strSenderOperaterID, strGetSMS, 1, strSMPPResponse);
                                                        }
                                                        finally
                                                        {

                                                        }

                                                        // SMPP SMS Code Below Receiver                                      
                                                        string strGetReceiverSMS = objSMPP_SMS_Format.FundTransferWalletReceiver(objODBC.executeScalar_str("SELECT username From er_login Where userid='" + intUserID + "'").ToString(), objODBC.executeScalar_str("SELECT full_name From er_login Where userid='" + intUserID + "'").ToString(), intSource, dblTransferAmount, strSenderFullName, strUserName, dblReceiverBalance, intTransID).ToString();
                                                        objComFun.er_insert_notification(intUserID, "Fund Request", strGetReceiverSMS, intCreatedByType);

                                                        strSMPPResponse = "NA";
                                                        DataTable dtSMPP = new DataTable();
                                                        try
                                                        {


                                                            dtSMPP = objODBC.getDataTable("Select mobile,operator_id from er_mobile where userid='" + intUserID + "' and usertype=2 and mob_type=1");

                                                            if (dtSMPP.Rows.Count > 0)
                                                            {
                                                                string strMobile = dtSMPP.Rows[0][0].ToString();
                                                                int intOperaterID = int.Parse(dtSMPP.Rows[0][1].ToString());
                                                                try
                                                                {
                                                                   // objODBCLOG.executeNonQuery("INSERT INTO `er_wallet_transfer_sms_log`(`userid`, `username`, `mobile`, `operator_id`, `SMS`, `created_on`) VALUES ('" + intUserID + "','" + objODBC.executeScalar_str("SELECT username From er_login Where userid='" + intUserID + "'").ToString() + "','" + strMobile + "','" + intOperaterID + "','" + strGetReceiverSMS + "','" + strDate2 + "')");

                                                                    objComFun.er_SMS_Alert(intUserID, 2, strMobile, intOperaterID, strGetReceiverSMS, 1, strSMPPResponse);
                                                                }
                                                                catch (Exception)
                                                                {
                                                                   //  objComFun.er_SMS_Alert(intUserID, 2, strMobile, intOperaterID, strGetReceiverSMS, 1, strSMPPResponse);
                                                                }

                                                            }
                                                        }
                                                        catch (Exception)
                                                        {
                                                           
                                                        }
                                                        finally
                                                        {
                                                            dtSMPP.Dispose();
                                                        }
                                                    }
                                                }
                                                catch (Exception ex)
                                                {
                                                    ds.Dispose();
                                                }
                                            }
                                            else
                                            {
                                                strMessage = dt.Rows[0]["FeildMessage"].ToString();
                                            }
                                        }
                                        catch (Exception ex)
                                        {
                                            dt.Dispose();
                                        }
                                        finally
                                        {
                                            dt.Dispose();
                                        }


                                    }
                                    else
                                    {
                                        SMPP_SMS_Format objSMPP_SMS_Format = new SMPP_SMS_Format();
                                        string strGetReceiverSMS = objSMPP_SMS_Format.LowBalance(strUserName, objODBC.executeScalar_str("SELECT full_name From er_login Where userid='" + intUserID + "' and user_status=1 and Active=1 and parent_id='" + intSenderID + "'").ToString(), dblTransferAmount).ToString();
                                        objComFun.er_insert_notification(intUserID, "Fund Request", strGetReceiverSMS, intCreatedByType);
                                        strMessage = "You do not have sufficient Balance in Wallet !";
                                    }
                                }
                                else
                                {
                                    strMessage = "You can not Transfer Balance with same amount more than once within 5 minutes!";
                                }

                            }
                            else
                            {
                                strMessage = "You can not Transfer Balance to this User !";
                            }
                        }
                        else
                        {
                            strMessage = "Permission denied by system !";
                        }
                    }
                    else
                    {
                        strMessage = "Kindly Enter Valid Details !";
                    }
                }
                catch (Exception ex)
                {
                    strMessage = ex.Message.ToString();

                    return strMessage;
                }
            }
            catch (Exception ex)
            {
                strMessage = ex.Message.ToString();

                return strMessage;
            }

            return strMessage;
        }

        // Activity Type=30
        // int intSenderID=0, int intUserID=1, double dblTransferAmount=2, string strIPAddress=3, int intDesignationType=4, string strMacAddress=5, string strOSDetails=6, string strIMEINo=7, string strGcmID=8, string strAPPVersion=9, int intSource=10, int intCreatedByType=11,int intActualUserID=12)

        [WebMethod]
        public string ExWalletFundTransfer(string strUserName, string strData, int intType, int intSource, int intUtype)
        {
            string strMessage = "";
            var data = ED.Decrypt(strUserName, strData, intUtype, 1, intSource).Split(',');
            try
            {

                int intSenderID = int.Parse(data[0]);
                int intUserID = int.Parse(data[1]);
                double dblTransferAmount = double.Parse(data[2]);
                string strIPAddress = data[3];
                int intDesignationType = int.Parse(data[4]);
                string strMacAddress = data[5];
                string strOSDetails = data[6];
                string strIMEINo = data[7];
                string strGcmID = data[8];
                string strAPPVersion = data[9];
                int intSourceData = int.Parse(data[10]);
                int intCreatedByType = int.Parse(data[11]);
                int intActualUserID = int.Parse(data[12]);
                try
                {

                    objCreateFile.CreateTextFile(strIPAddress, strMacAddress, strOSDetails, strIMEINo, strGcmID, strAPPVersion, intSourceData.ToString(), intCreatedByType, 30, " Wallet Fund Transfer" + ",Data: " + strData, strUserName);
                }
                catch (Exception)
                {

                }
                try
                {
                    if ((intSenderID != intUserID) && (intUserID > 0) && (dblTransferAmount > 0) && (intSenderID > 0) && (intDesignationType != 0) && (strIPAddress != "" || strIPAddress != null) && (strMacAddress != "" || strMacAddress != null) && (strOSDetails != "" || strOSDetails != null) && (strIMEINo != "" || strIMEINo != null) && (strGcmID != "" || strGcmID != null) && (strAPPVersion != "" || strAPPVersion != null) && (intSourceData != 0) && (intCreatedByType != 0))
                    {
                        try
                        {
                            objODBCLOG.executeNonQuery("call activity_logs('" + intActualUserID + "','" + intDesignationType + "','" + intCreatedByType + "','" + strIPAddress + "','" + strMacAddress + "','" + strOSDetails + "','" + strIMEINo + "','" + strGcmID + "','" + strAPPVersion + "','" + intSourceData + "',' Wallet Fund Transfer ',30,'','" + strUserName + "')");

                            objCreateFile.CreateTextFile(strIPAddress, strMacAddress, strOSDetails, strIMEINo, strGcmID, strAPPVersion, intSourceData.ToString(), intCreatedByType, 30, " Wallet Fund Transfer" + ",Data: " + strData, strUserName);
                        }
                        catch (Exception)
                        {

                        }
                        int intStatus = objODBC.executeScalar_int("SELECT `status` FROM `er_status_manager` WHERE id=1");
                        if (intStatus == 1)
                        {
                            int intCount = objODBC.executeScalar_int("Select count(1) from er_login where userid='" + intUserID + "' and user_status=1 and Active=1 and parent_id='" + intSenderID + "'");
                            if (intCount > 0)
                            {
                                string strDate2 = objDT.getCurDateTimeString();

                                int intCountCheckTransaction = objODBC.executeScalar_int("SELECT count(1) FROM `er_wallet_transfer_individual` WHERE parent_id='" + intSenderID + "' and userid='" + intUserID + "' and transfer_amt='" + dblTransferAmount + "' and TIMESTAMPDIFF(MINUTE,created_on,'" + strDate2 + "')<=5");
                                if (intCountCheckTransaction == 0)
                                {
                                    double dblUserBalance = ReturnUsableWalletAmount(intSenderID);

                                    if (dblUserBalance >= dblTransferAmount)
                                    {
                                        dt = objODBC.getDataTable("Call ap_walletTransfer(" + intSenderID + "," + intUserID + "," + dblTransferAmount + ")");
                                        string strTransID = dt.Rows[0][0].ToString();                                      
                                        strMessage = "Wallet Fund Successfully transferred.";
                                        double dblSenderBalance = GetWalletAmount(intSenderID);
                                        double dblReceiverBalance = GetWalletAmount(intUserID);
                                        // SMPP1 SMS Send
                                        DataSet ds = new DataSet();
                                        try
                                        {

                                            ds = objODBC.getDataSet("SELECT l.full_name,l.username,m.mobile,m.operator_id From er_login l inner join er_mobile m on l.userid=m.userid Where l.userid='" + intSenderID + "' and m.usertype='" + intCreatedByType + "' and m.mob_type=1");
                                            if (ds.Tables[0].Rows.Count > 0)
                                            {
                                                string strSenderFullName = ds.Tables[0].Rows[0][0].ToString();
                                                string strSenderUserName = ds.Tables[0].Rows[0][1].ToString();
                                                string strSenderMobileNumber = ds.Tables[0].Rows[0][2].ToString();
                                                int strSenderOperaterID = int.Parse(ds.Tables[0].Rows[0][3].ToString());


                                                // SMPP SMS Code Below Sender
                                                SMPP_SMS_Format objSMPP_SMS_Format = new SMPP_SMS_Format();
                                                string strGetSMS = objSMPP_SMS_Format.FundTransferWalletSender(strUserName, objODBC.executeScalar_str("SELECT full_name From er_login Where userid='" + intUserID + "'").ToString(), intSource, dblTransferAmount, strSenderFullName, strSenderUserName.Trim(), dblSenderBalance, strTransID).ToString();
                                                objComFun.er_insert_notification(intSenderID, "Fund Request", strGetSMS, intCreatedByType);
                                                SMPP1 objSMPP1 = new SMPP1();
                                                string strSMPPResponse = "NA";

                                                try
                                                {
                                                   // objODBCLOG.executeNonQuery("INSERT INTO `er_wallet_transfer_sms_log`(`userid`, `username`, `mobile`, `operator_id`, `SMS`, `created_on`) VALUES ('" + intSenderID + "','" + strUserName + "','" + strSenderMobileNumber + "','" + strSenderOperaterID + "','" + strGetSMS + "','" + strDate2 + "')");

                                                    objComFun.er_SMS_Alert(intSenderID, 2, strSenderMobileNumber, strSenderOperaterID, strGetSMS, 9, strSMPPResponse);

                                                }
                                                catch (Exception)
                                                {
                                                   // objComFun.er_SMS_Alert(intSenderID, 2, strSenderMobileNumber, strSenderOperaterID, strGetSMS, 9, strSMPPResponse);
                                                }
                                                finally
                                                {

                                                }

                                                // SMPP SMS Code Below Receiver                                      
                                                string strGetReceiverSMS = objSMPP_SMS_Format.FundTransferWalletReceiver(objODBC.executeScalar_str("SELECT username From er_login Where userid='" + intUserID + "'").ToString(), objODBC.executeScalar_str("SELECT full_name From er_login Where userid='" + intUserID + "'").ToString(), intSource, dblTransferAmount, strSenderFullName, strUserName, dblReceiverBalance, strTransID).ToString();
                                                objComFun.er_insert_notification(intUserID, "Fund Request", strGetReceiverSMS, intCreatedByType);

                                                strSMPPResponse = "NA";
                                                DataTable dtSMPP = new DataTable();
                                                try
                                                {


                                                    dtSMPP = objODBC.getDataTable("Select mobile,operator_id from er_mobile where userid='" + intUserID + "' and usertype=2 and mob_type=1");

                                                    if (dtSMPP.Rows.Count > 0)
                                                    {
                                                        string strMobile = dtSMPP.Rows[0][0].ToString();
                                                        int intOperaterID = int.Parse(dtSMPP.Rows[0][1].ToString());
                                                        try
                                                        {
                                                         //   objODBCLOG.executeNonQuery("INSERT INTO `er_wallet_transfer_sms_log`(`userid`, `username`, `mobile`, `operator_id`, `SMS`, `created_on`) VALUES ('" + intUserID + "','" + objODBC.executeScalar_str("SELECT username From er_login Where userid='" + intUserID + "'").ToString() + "','" + strMobile + "','" + intOperaterID + "','" + strGetReceiverSMS + "','" + strDate2 + "')");

                                                            objComFun.er_SMS_Alert(intUserID, 2, strMobile, intOperaterID, strGetReceiverSMS, 1, strSMPPResponse);
                                                        }
                                                        catch(Exception)
                                                        {
                                                          //  objComFun.er_SMS_Alert(intUserID, 2, strMobile, intOperaterID, strGetReceiverSMS, 1, strSMPPResponse);
                                                        }

                                                    }
                                                }
                                                catch (Exception)
                                                {
                                                    
                                                }
                                                finally
                                                {
                                                    dt.Dispose();
                                                    dtSMPP.Dispose();
                                                }
                                            }
                                        }
                                        catch (Exception ex)
                                        {
                                            ds.Dispose();
                                            dt.Dispose();
                                        }


                                    }
                                    else
                                    {
                                        SMPP_SMS_Format objSMPP_SMS_Format = new SMPP_SMS_Format();
                                        string strGetReceiverSMS = objSMPP_SMS_Format.LowBalance(strUserName, objODBC.executeScalar_str("SELECT full_name From er_login Where userid='" + intUserID + "' and user_status=1 and Active=1 and parent_id='" + intSenderID + "'").ToString(), dblTransferAmount).ToString();
                                        objComFun.er_insert_notification(intUserID, "Fund Request", strGetReceiverSMS, intCreatedByType);
                                        strMessage = "You do not have sufficient Balance in Wallet !";
                                    }
                                }
                                else
                                {
                                    strMessage = "You can not Transfer Balance with same amount more than once within 5 minutes!";
                                }

                            }
                            else
                            {
                                strMessage = "You can not Transfer Balance to this User !";
                            }
                        }
                        else
                        {
                            strMessage = "Permission denied by system !";
                        }
                    }
                    else
                    {
                        strMessage = "Kindly Enter Valid Details !";
                    }
                }
                catch (Exception ex)
                {
                    strMessage = ex.Message.ToString();

                    return strMessage;
                }
            }
            catch (Exception ex)
            {
                strMessage = ex.Message.ToString();

                return strMessage;
            }

            return strMessage;
        }

        // Activity Type=31
        [WebMethod]
        public string CreateGroupWalletTransfer(string strUserName, int intParentID, string strName, string strIPAddress, int intDesignationType, string strMacAddress, string strOSDetails, string strIMEINo, string strGcmID, string strAPPVersion, int intSource, int intCreatedByType)
        {
            string strMessage = "";

            string strDateTime = objDT.getCurDateTimeString();
            if ((intParentID > 0) && (strName != "" || strName != null) && (intDesignationType != 0) && (strIPAddress != "" || strIPAddress != null) && (strMacAddress != "" || strMacAddress != null) && (strOSDetails != "" || strOSDetails != null) && (strIMEINo != "" || strIMEINo != null) && (strGcmID != "" || strGcmID != null) && (strAPPVersion != "" || strAPPVersion != null) && (intSource != 0) && (intCreatedByType != 0))
            {
                try
                {
                    objODBCLOG.executeNonQuery("call activity_logs('" + intParentID + "','" + intDesignationType + "','" + intCreatedByType + "','" + strIPAddress + "','" + strMacAddress + "','" + strOSDetails + "','" + strIMEINo + "','" + strGcmID + "','" + strAPPVersion + "','" + intSource + "',' Create Group ( Wallet Transfer ) ',31,'','" + strUserName + "')");
                    objCreateFile.CreateTextFile(strIPAddress, strMacAddress, strOSDetails, strIMEINo, strGcmID, strAPPVersion, intSource.ToString(), intCreatedByType, 31, "  Create Group ( Wallet Transfer ) ", strUserName);

                    int intStatus = objODBC.executeScalar_int("SELECT `status` FROM `er_status_manager` WHERE id=1");
                    if (intStatus == 1)
                    {
                        int intCHkExi = objODBC.executeScalar_int("select count(1) from er_wallet_transfer_group where group_name ='" + strName + "'");

                        if (intCHkExi == 0)
                        {
                            //int intPID = objODBC.executeScalar_int("select parent_id from er_login where userid =" + intParentID + "");

                            objODBC.executeNonQuery("INSERT INTO er_wallet_transfer_group(parent_id,group_name,created_on) VALUES(" + intParentID + ",'" + strName + "','" + strDateTime + "')");
                            strMessage = "Group created successfully !";
                        }
                        else
                        {
                            strMessage = "Group already exists !";
                        }
                    }
                    else
                    {
                        strMessage = "Permission denied by system !";
                    }
                }
                catch (Exception ex)
                {
                    strMessage = ex.Message.ToString();

                    return strMessage;
                }
            }
            else
            {
                strMessage = "Kindly Enter Valid Details !";
            }

            return strMessage;
        }



        // Activity Type=32
        [WebMethod] //intmanage - 1-add member 2- remove member
        public string AddMemberGroupWalletTransfer(string strUserName, string strParentSessionID, int intGroupID, int intUserID, int intAction, string strIPAddress, int intDesignationType, string strMacAddress, string strOSDetails, string strIMEINo, string strGcmID, string strAPPVersion, int intSource, int intCreatedByType)
        {
            string strMessage = "";
            try
            {

                objCreateFile.CreateTextFile(strIPAddress, strMacAddress, strOSDetails, strIMEINo, strGcmID, strAPPVersion, intSource.ToString(), intCreatedByType, 32, "Add Member Group Wallet Transfer", strUserName);
            }
            catch (Exception)
            {

            }
            string strDateTime = objDT.getCurDateTimeString();
            if ((intGroupID > 0) && (intUserID > 0) && (intDesignationType != 0) && (strIPAddress != "" || strIPAddress != null) && (strMacAddress != "" || strMacAddress != null) && (strOSDetails != "" || strOSDetails != null) && (strIMEINo != "" || strIMEINo != null) && (strGcmID != "" || strGcmID != null) && (strAPPVersion != "" || strAPPVersion != null) && (intSource != 0) && (intCreatedByType != 0))
            {
                try
                {
                    objODBCLOG.executeNonQuery("call activity_logs('" + strParentSessionID + "','" + intDesignationType + "','" + intCreatedByType + "','" + strIPAddress + "','" + strMacAddress + "','" + strOSDetails + "','" + strIMEINo + "','" + strGcmID + "','" + strAPPVersion + "','" + intSource + "',' Add Member to Group (Wallet Transfer)',32,'','" + strUserName + "')");
                }
                catch (Exception)
                {

                }
                int intStatus = objODBC.executeScalar_int("SELECT `status` FROM `er_status_manager` WHERE id=1");
                if (intStatus == 1)
                {
                    int intCountLogin = objODBC.executeScalar_int("SELECT COUNT(1) FROM er_login WHERE parent_id='" + strParentSessionID + "' and userid=" + intUserID);
                    if (intCountLogin > 0)
                    {
                        int intCount = objODBC.executeScalar_int("SELECT COUNT(1) FROM er_wallet_transfer_group_details WHERE group_id=" + intGroupID + " and userid=" + intUserID);
                        if (intAction == 1)
                        {
                            if (intCount == 0)
                            {
                                try
                                {
                                    objODBC.executeNonQuery("INSERT INTO er_wallet_transfer_group_details (group_id,userid) VALUES(" + intGroupID + "," + intUserID + ")");
                                    strMessage = "Added Member In Wallet Transfer Group";
                                }
                                catch (Exception ex)
                                {
                                    strMessage = ex.Message.ToString();

                                    return strMessage;
                                }
                            }
                        }
                        else if (intAction == 2)
                        {
                            try
                            {
                                objODBC.executeNonQuery("DELETE from er_wallet_transfer_group_details WHERE group_id=" + intGroupID + " and userid =" + intUserID);
                                strMessage = "Removed Member from Wallet Transfer Group";
                            }
                            catch (Exception ex)
                            {
                                strMessage = ex.Message.ToString();

                                return strMessage;
                            }
                        }
                    }
                    else
                    {
                        strMessage = "Member is not Belong to Login Member !";
                    }
                }
                else
                {
                    strMessage = "Permission denied by system !";
                }

            }
            else
            {
                strMessage = "Kindly Enter Valid Details !";
            }

            return strMessage;
        }



        // Activity Type=33
        // int intUserID=0, int intGroupID=1, double dblAmt=2, string strIPAddress=3, int intDesignationType=4, string strMacAddress=5, string strOSDetails=6, string strIMEINo=7, string strGcmID=8, string strAPPVersion=9, int intSource=10, int intCreatedByType=11,int intActualUserID=12
        [WebMethod]
        public string ap_walletTransferBulk(string strUserName, string strData, int intType, int intSource, int intUtype)
        {
            string strMessage = "";

            var data = ED.Decrypt(strUserName, strData, intUtype, 1, intSource).Split(',');
            try
            {

                int intUserID = int.Parse(data[0]);
                int intGroupID = int.Parse(data[1]);
                double dblAmt = double.Parse(data[2]);
                string strIPAddress = data[3];
                int intDesignationType = int.Parse(data[4]);
                string strMacAddress = data[5];
                string strOSDetails = data[6];
                string strIMEINo = data[7];
                string strGcmID = data[8];
                string strAPPVersion = data[9];
                int intSourceData = int.Parse(data[10]);
                int intCreatedByType = int.Parse(data[11]);
                int intActualUserID = int.Parse(data[12]);

                try
                {
                    objCreateFile.CreateTextFile(strIPAddress, strMacAddress, strOSDetails, strIMEINo, strGcmID, strAPPVersion, intSourceData.ToString(), intCreatedByType, 33, " Wallet Transfer Bulk,Data:" + strData, strUserName);
                }
                catch (Exception)
                {

                }

                if ((intGroupID > 0) && (intUserID > 0) && (dblAmt > 0))
                {
                    try
                    {
                        int intStatus = objODBC.executeScalar_int("SELECT `status` FROM `er_status_manager` WHERE id=1");
                        if (intStatus == 1)
                        {
                            double dblUserBalance = ReturnUsableWalletAmount(intUserID);

                            if (dblUserBalance >= dblAmt)
                            {
                                objODBC.executeNonQuery("Call ap_walletTransferBulk(" + intUserID + "," + intGroupID + "," + dblAmt + ")");
                                double dblSenderBalance = GetWalletAmount(intUserID);
                                try
                                {
                                    objODBCLOG.executeNonQuery("call activity_logs('" + intActualUserID + "','" + intDesignationType + "','" + intCreatedByType + "','" + strIPAddress + "','" + strMacAddress + "','" + strOSDetails + "','" + strIMEINo + "','" + strGcmID + "','" + strAPPVersion + "','" + intSourceData + "',' Wallet Bulk Transfer ',33,'','')");
                                }
                                catch (Exception)
                                {

                                }
                                DataSet ds = new DataSet();
                                try
                                {
                                    ds = objODBC.getDataSet("select w.userid,l.full_name,l.username,m.mobile,m.operator_id,w.trans_number from er_wallet_transfer_group_details w inner join er_login l on w.userid=l.userid inner join er_mobile m on w.userid=m.userid where w.group_id='" + intGroupID + "'");

                                    for (int i = 0; i > ds.Tables[0].Rows.Count; i++)
                                    {
                                        int intSMSUserID = int.Parse(ds.Tables[0].Rows[i][0].ToString());
                                        string strSMSFullName = ds.Tables[0].Rows[i][1].ToString();
                                        string strSMSUserName = ds.Tables[0].Rows[i][2].ToString();
                                        string strSMSMobile = ds.Tables[0].Rows[i][3].ToString();
                                        int intSMSOperaterID = int.Parse(ds.Tables[0].Rows[i][4].ToString());
                                        string strTransID = ds.Tables[0].Rows[i][5].ToString();

                                        double dblReceiverBalance = GetWalletAmount(intSMSUserID);

                                        // Receiver
                                        SMPP_SMS_Format objSMPP_SMS_Format = new SMPP_SMS_Format();
                                        string strGetReceiverSMS = objSMPP_SMS_Format.FundTransferWalletReceiver(strUserName, objODBC.executeScalar_str("SELECT full_name From er_login Where userid='" + intUserID + "'").ToString(), intSource, dblAmt, strSMSFullName, strSMSUserName, dblReceiverBalance, strTransID).ToString();
                                        objComFun.er_insert_notification(intSMSUserID, "Fund Request", strGetReceiverSMS, 2);
                                        objComFun.er_SMS_Alert(intUserID, 2, strSMSMobile, intSMSOperaterID, strGetReceiverSMS, 1, "Bounded");

                                        // Sender
                                        string strGetSenderSMS = objSMPP_SMS_Format.FundTransferWalletSender(strSMSUserName, strSMSFullName, intSource, dblAmt, strUserName, objODBC.executeScalar_str("SELECT full_name From er_login Where userid='" + intUserID + "'").ToString(), dblSenderBalance, strTransID).ToString();
                                        objComFun.er_insert_notification(intUserID, "Fund Request", strGetSenderSMS, 2);
                                    }
                                }
                                catch (Exception)
                                {

                                }
                                finally
                                {
                                    ds.Dispose();
                                }
                                strMessage = "Request is in process kindly check the report for the status !";

                            }
                            else
                            {
                                SMPP_SMS_Format objSMPP_SMS_Format = new SMPP_SMS_Format();
                                string strGetReceiverSMS = objSMPP_SMS_Format.LowBalance(strUserName, objODBC.executeScalar_str("SELECT full_name From er_login Where userid='" + intUserID + "'").ToString(), dblAmt).ToString();
                                objComFun.er_insert_notification(intUserID, "Bulk Wallet Transfer", strGetReceiverSMS, 2);
                                strMessage = "Amount should be Greater then threshold amount !";
                            }
                        }
                        else
                        {
                            strMessage = "Permission denied by system !";
                        }
                    }
                    catch (Exception ex)
                    {
                        strMessage = "Error ! Something went Wrong.";
                        return strMessage;
                    }
                }
                else
                {
                    strMessage = "Kindly Enter Valid Details !";
                }
            }
            catch (Exception ex)
            {
                strMessage = "Error ! Something went Wrong.";
                return strMessage;
            }

            return strMessage;
        }

        public double ReturnUsableWalletAmount(int userid)
        {
            double amount;
            try
            {
                amount = objODBC.executeScalar_dbl("SELECT ex_wallet-min_wallet FROM er_wallet WHERE userid=" + userid);
                return amount;
            }
            catch (Exception ex)
            {
                return 0.0;
            }
        }


        // Activity Type=34
        // intUserID= 0,intOpID=1,dblAmt=2,strIPAddress=3,DesignationType=4,strMacAddress=5,strOSDetails=6,strIMEINo=7,strGcmID=8,strAPPVersion=9,intSource=10, intCreatedByType=11,int intActualUserID=12,double dblCommissionAmt=13
        [WebMethod]
        public string generateEMoneyEtisalat(string strUserName, string strData, int intType, int intSource, int intUtype)
        {
            string strMessage = "";
            string strDT = objDT.getCurDateTimeString();

            var data = ED.Decrypt(strUserName, strData, intUtype, 1, intSource).Split(',');
            try
            {
                string strIPAddress = data[3], strMacAddress = data[5], strOSDetails = data[6], strIMEINo = data[7], strGcmID = data[8], strAPPVersion = data[9];
                int intOpID = int.Parse(data[1]);
                int intUserID = int.Parse(data[0]);
                int intSourceData = int.Parse(data[10]);
                int intDesignationType = int.Parse(data[4]);
                int intCreatedByType = int.Parse(data[11]);
                double dblAmt = double.Parse(data[2]);
                double intActualUserID = double.Parse(data[12]);
                double dblCommissionAmt = double.Parse(data[13]);
                double dblTotalAmount = dblCommissionAmt + dblAmt;

                if ((intOpID > 0) && (intUserID == 1) && (dblAmt > 0) && (dblCommissionAmt > 0) && (intDesignationType != 0) && (strIPAddress != "" || strIPAddress != null) && (strMacAddress != "" || strMacAddress != null) && (strOSDetails != "" || strOSDetails != null) && (strIMEINo != "" || strIMEINo != null) && (strGcmID != "" || strGcmID != null) && (strAPPVersion != "" || strAPPVersion != null) && (intSourceData != 0) && (intCreatedByType != 0))
                {
                    if (intUserID == 1)
                    {

                        try
                        {
                            try
                            {
                                objODBCLOG.executeNonQuery("call activity_logs('" + intActualUserID + "','" + intDesignationType + "','" + intCreatedByType + "','" + strIPAddress + "','" + strMacAddress + "','" + strOSDetails + "','" + strIMEINo + "','" + strGcmID + "','" + strAPPVersion + "','" + intSourceData + "',' generate E Money Transaction ',34,'','" + strUserName + "')");

                                objCreateFile.CreateTextFile(strIPAddress, strMacAddress, strOSDetails, strIMEINo, strGcmID, strAPPVersion, intSourceData.ToString(), intCreatedByType, 34, " generate E Money Transaction  ", strUserName);
                            }
                            catch (Exception)
                            {

                            }

                            double dblCur = objODBC.executeScalar_dbl("SELECT amt FROM er_sys_wallet WHERE id=" + intOpID);
                            double dblAdminCur = objODBC.executeScalar_dbl("SELECT ex_wallet FROM er_wallet WHERE userid=1");
                            if ((dblCur - dblTotalAmount >= 0) && (dblAdminCur - dblTotalAmount) >= 0)
                            {
                                objODBC.executeNonQuery("UPDATE er_sys_wallet SET amt=amt-" + dblTotalAmount + " WHERE id=" + intOpID);
                                objODBC.executeNonQuery("UPDATE er_wallet SET ex_wallet=ex_wallet-" + dblAmt + " WHERE userid=1");

                                objODBC.executeNonQuery("INSERT INTO er_emoney_transaction(wallet_id,amt,created_on,admin_id,closing_balance,trans_type) VALUES('" + intOpID + "','" + dblTotalAmount + "','" + strDT + "','" + intUserID + "','" + (dblCur - dblTotalAmount) + "',4)");

                                objODBC.executeNonQuery("INSERT INTO `graph_sys_wallet_track`( `balance`, `operaterId`, `created_on`,`admin_amt`,`emoney_amt`,`comm_amt`,`prev_Em_balance`) VALUES ('" + -dblTotalAmount + "','" + intOpID + "','" + strDT + "','" + -dblAmt + "','" + -dblTotalAmount + "','" + dblCommissionAmt + "','" + dblCur + "')");

                                objODBC.executeNonQuery("call update_OperatorWise_details('" + intOpID + "','" + -dblTotalAmount + "')");

                                strMessage = "Emoney successfully Deduct to the wallet !";
                            }
                            else
                            {
                                strMessage = "Deduct Emoney Wallet Amount is less the 0 !";
                            }

                        }
                        catch (Exception ex)
                        {
                            strMessage = ex.Message.ToString();
                            return strMessage;
                        }
                    }
                    else
                        strMessage = "Invalid Details !";
                }
                else
                {
                    strMessage = "Kindly Enter Valid Details !";
                }
            }
            catch (Exception ex)
            {
                strMessage = ex.Message.ToString();
                return strMessage;
            }
            return strMessage;
        }


        // Activity Type=34
        // intUserID= 0,intOpID=1,dblAmt=2,strIPAddress=3,DesignationType=4,strMacAddress=5,strOSDetails=6,strIMEINo=7,strGcmID=8,strAPPVersion=9,intSource=10, intCreatedByType=11,int intActualUserID=12,int intType=13
        [WebMethod]
        public string RollbackEMoney(string strUserName, string strData, int intType, int intSource, int intUtype)
        {
            string strMessage = "";
            string strDateTime = objDT.getCurDateTimeString();

            var data = ED.Decrypt(strUserName, strData, intUtype, 1, intSource).Split(',');
            try
            {
                string strIPAddress = data[3], strMacAddress = data[5], strOSDetails = data[6], strIMEINo = data[7], strGcmID = data[8], strAPPVersion = data[9];
                int intOpID = int.Parse(data[1]);
                int intUserID = int.Parse(data[0]);
                int intSourceData = int.Parse(data[10]);
                int intDesignationType = int.Parse(data[4]);
                int intCreatedByType = int.Parse(data[11]);
                double dblAmt = double.Parse(data[2]);
                double intActualUserID = double.Parse(data[12]);
                int intTypeValue = int.Parse(data[13]);
                //double dblTotalAmount = dblCommissionAmt + dblAmt;

                if ((intOpID > 0) && (intUserID == 1) && (intActualUserID>0) && (dblAmt > 0) && (intDesignationType != 0) && (strIPAddress != "" || strIPAddress != null) && (strMacAddress != "" || strMacAddress != null) && (strOSDetails != "" || strOSDetails != null) && (strIMEINo != "" || strIMEINo != null) && (strGcmID != "" || strGcmID != null) && (strAPPVersion != "" || strAPPVersion != null) && (intSourceData != 0) && (intCreatedByType != 0))
                {
                    if (intUserID == 1)
                    {

                        try
                        {
                            try
                            {
                                objODBCLOG.executeNonQuery("call activity_logs('" + intActualUserID + "','" + intDesignationType + "','" + intCreatedByType + "','" + strIPAddress + "','" + strMacAddress + "','" + strOSDetails + "','" + strIMEINo + "','" + strGcmID + "','" + strAPPVersion + "','" + intSourceData + "',' Rollback E Money Transaction ',34,'','" + strUserName + "')");

                                objCreateFile.CreateTextFile(strIPAddress, strMacAddress, strOSDetails, strIMEINo, strGcmID, strAPPVersion, intSourceData.ToString(), intCreatedByType, 34, " Rollback E Money Transaction  ", strUserName);
                            }
                            catch (Exception)
                            {

                            }


                            int intCountCheckTransaction = objODBC.executeScalar_int("SELECT count(1) FROM `er_rollback_transaction` WHERE  userid='" + intActualUserID + "' and amt='" + dblAmt + "' and trans_type='"+ intTypeValue + "' and wallet_id='" + intOpID + "' and TIMESTAMPDIFF(MINUTE,created_on,'" + strDateTime + "')<=5");
                            if (intCountCheckTransaction == 0)
                            {
                                objODBC.executeNonQuery("call ap_updateRollBackTransaction('" + dblAmt + "','" + intTypeValue + "','0','" + intOpID + "','" + intActualUserID + "');");
                                if (intTypeValue == 1)
                                {
                                    strMessage = "RollBack Emoney Added Sucessfully !";
                                }
                                else
                                {
                                    strMessage = "RollBack Emoney Deduct Sucessfully !";
                                }

                            }
                            else
                            {
                                strMessage = "You can not RollBack Emoney with same amount more than once within 5 minutes!";
                            }

                        }
                        catch (Exception ex)
                        {
                            strMessage = ex.Message.ToString();
                            return strMessage;
                        }
                    }
                    else
                        strMessage = "Invalid Details !";
                }
                else
                {
                    strMessage = "Kindly Enter Valid Details !";
                }
            }
            catch (Exception ex)
            {
                strMessage = ex.Message.ToString();
                return strMessage;
            }
            return strMessage;
        }

        [WebMethod]
        public DataSet er_GetWalletLimit(string strUserName, int intUserID, string strIPAddress, int intDesignationType, string strMacAddress, string strOSDetails, string strIMEINo, string strGcmID, string strAPPVersion, int intSource, int intCreatedByType)
        {
            DataSet ds = new DataSet();
            try
            {

                try
                {
                    objCreateFile.CreateTextFile(strIPAddress, strMacAddress, strOSDetails, strIMEINo, strGcmID, strAPPVersion, intSource.ToString(), intCreatedByType, 81, "er_GetAD_Operator_Userwise_details", strUserName);
                }
                catch (Exception)
                {


                }
                finally
                {
                    string strQuery = "SELECT `wallet1`, `wallet2`, `wallet3`, `wallet4`, `wallet5` FROM `er_wallet` WHERE userid='" + intUserID + "'";

                    ds = objODBC.Report(strQuery);
                }
                return ds;
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                ds.Dispose();
            }
        }

        // Activity Type=34
        // intUserID= 0,intOpID=1,dblAmt=2,strIPAddress=3,DesignationType=4,strMacAddress=5,strOSDetails=6,strIMEINo=7,strGcmID=8,strAPPVersion=9,intSource=10, intCreatedByType=11,int intActualUserID=12,double dblCommissionAmt=13
        [WebMethod]
        public string generateEMoney(string strUserName, string strData, int intType, int intSource, int intUtype)
        {
            string strMessage = "";
            string strDT = objDT.getCurDateTimeString();

            var data = ED.Decrypt(strUserName, strData, intUtype, 1, intSource).Split(',');
            try
            {
                string strIPAddress = data[3], strMacAddress = data[5], strOSDetails = data[6], strIMEINo = data[7], strGcmID = data[8], strAPPVersion = data[9];
                int intOpID = int.Parse(data[1]);
                int intUserID = int.Parse(data[0]);
                int intSourceData = int.Parse(data[10]);
                int intDesignationType = int.Parse(data[4]);
                int intCreatedByType = int.Parse(data[11]);
                double dblAmt = double.Parse(data[2]);
                double intActualUserID = double.Parse(data[12]);
                double dblCommissionAmt = double.Parse(data[13]);
                double dblTotalAmount = dblCommissionAmt + dblAmt;

                if ((intOpID > 0) && (intUserID == 1) && (dblAmt > 0) && (dblCommissionAmt > 0) && (intDesignationType != 0) && (strIPAddress != "" || strIPAddress != null) && (strMacAddress != "" || strMacAddress != null) && (strOSDetails != "" || strOSDetails != null) && (strIMEINo != "" || strIMEINo != null) && (strGcmID != "" || strGcmID != null) && (strAPPVersion != "" || strAPPVersion != null) && (intSourceData != 0) && (intCreatedByType != 0))
                {
                    if (intUserID == 1)
                    {

                        try
                        {
                            try
                            {
                                objODBCLOG.executeNonQuery("call activity_logs('" + intActualUserID + "','" + intDesignationType + "','" + intCreatedByType + "','" + strIPAddress + "','" + strMacAddress + "','" + strOSDetails + "','" + strIMEINo + "','" + strGcmID + "','" + strAPPVersion + "','" + intSourceData + "',' generate E Money Transaction ',34,'','" + strUserName + "')");

                                objCreateFile.CreateTextFile(strIPAddress, strMacAddress, strOSDetails, strIMEINo, strGcmID, strAPPVersion, intSourceData.ToString(), intCreatedByType, 34, " generate E Money Transaction  ", strUserName);
                            }
                            catch (Exception)
                            {

                            }

                            string strDate2 = objDT.getCurDateTimeString();
                            int intchkDuplicateEmoney = objODBC.executeScalar_int("Select Count(1) From graph_sys_wallet_track where operaterId='"+ intOpID + "' and emoney_amt='" + dblTotalAmount + "' and admin_amt='"+ dblAmt + "' And TIMESTAMPDIFF(MINUTE,created_on,'" + strDate2 + "')<=5");
                            if (intchkDuplicateEmoney == 0)
                            {

                                double dblCur = objODBC.executeScalar_dbl("SELECT amt FROM er_sys_wallet WHERE id=" + intOpID);

                                objODBC.executeNonQuery("UPDATE er_sys_wallet SET amt=amt+" + dblTotalAmount + " WHERE id=" + intOpID);
                                objODBC.executeNonQuery("UPDATE er_wallet SET ex_wallet=ex_wallet+" + dblAmt + " WHERE userid=1");
                                objODBC.executeNonQuery("INSERT INTO er_emoney_transaction(wallet_id,amt,created_on,admin_id,closing_balance,trans_type) VALUES('" + intOpID + "','" + dblTotalAmount + "','" + strDT + "','" + intUserID + "','" + (dblCur + dblTotalAmount) + "',1)");

                                objODBC.executeNonQuery("INSERT INTO `graph_sys_wallet_track`( `balance`, `operaterId`, `created_on`,`admin_amt`,`emoney_amt`,`comm_amt`,`prev_Em_balance`) VALUES ('" + dblTotalAmount + "','" + intOpID + "','" + strDT + "','" + dblAmt + "','" + dblTotalAmount + "','" + dblCommissionAmt + "','" + dblCur + "')");
                                objODBC.executeNonQuery("call update_OperatorWise_details('" + intOpID + "','" + dblTotalAmount + "')");

                                strMessage = "Emoney successfully added to the wallet !";
                            }
                            else
                            {
                                strMessage = "Emoney Already added to the wallet, You can not add Emoney with same amount more than once within 5 minutes";
                            }

                        }
                        catch (Exception ex)
                        {
                            strMessage = ex.Message.ToString();
                            return strMessage;
                        }
                    }
                    else
                        strMessage = "Invalid Details !";
                }
                else
                {
                    strMessage = "Kindly Enter Valid Details !";
                }
            }
            catch (Exception ex)
            {
                strMessage = ex.Message.ToString();
                return strMessage;
            }
            return strMessage;
        }



        // Activity Type=39
        [WebMethod]
        public ArrayList GetOperaterType(string strUserName, int intUserID, string strIPAddress, int intDesignationType, string strMacAddress, string strOSDetails, string strIMEINo, string strGcmID, string strAPPVersion, int intSource, int intCreatedByType)
        {
            DataSet ds = new DataSet();
            ArrayList arrUserParams = new ArrayList();
            try
            {
                if ((intUserID > 0))
                {
                    objCreateFile.CreateTextFile(strIPAddress, strMacAddress, strOSDetails, strIMEINo, strGcmID, strAPPVersion, intSource.ToString(), intCreatedByType, 39, "Get Operater Type", strUserName);

                    string strQuery = "SELECT id,operator_name from er_recharge_operator_mobile;";

                    try
                    {
                        ds = objODBC.getDataSet(strQuery);

                        if (ds.Tables[0].Rows.Count > 0)
                        {
                            arrUserParams.Add("TRUE");

                            System.Data.DataTable firstTable = ds.Tables[0];

                            for (int i = 0; i <= firstTable.Rows.Count - 1; i++)
                            {
                                for (int j = 0; j <= firstTable.Columns.Count - 1; j++)
                                {
                                    arrUserParams.Add(firstTable.Rows[i][j].ToString());
                                }
                            }




                            return arrUserParams;
                        }
                        else
                        {
                            arrUserParams.Add("False");
                            return arrUserParams;
                        }
                    }
                    catch (Exception ex)
                    {
                        arrUserParams.Add("False");
                        return arrUserParams;
                    }
                    finally
                    {
                        ds.Dispose();
                    }
                }
                else
                {
                    arrUserParams.Add("False");
                    return arrUserParams;
                }
            }

            catch (Exception ex)
            {
                arrUserParams.Add("False");
                return arrUserParams;
            }
            finally
            {
                ds.Dispose();
            }
        }

        // Activity Type=40
        [WebMethod]
        public ArrayList GetOperaterBalance(string strUserName, int intUserID, string strIPAddress, int intDesignationType, string strMacAddress, string strOSDetails, string strIMEINo, string strGcmID, string strAPPVersion, int intSource, int intCreatedByType)
        {
            DataSet ds = new DataSet();
            ArrayList arrUserParams = new ArrayList();
            try
            {
                if ((intUserID > 0))
                {
                    objCreateFile.CreateTextFile(strIPAddress, strMacAddress, strOSDetails, strIMEINo, strGcmID, strAPPVersion, intSource.ToString(), intCreatedByType, 40, "Get Operater Balance", strUserName);

                    string strQuery = "SELECT `id`, `amt` FROM `er_sys_wallet`;";

                    try
                    {
                        ds = objODBC.getDataSet(strQuery);

                        if (ds.Tables[0].Rows.Count > 0)
                        {
                            arrUserParams.Add("TRUE");

                            System.Data.DataTable firstTable = ds.Tables[0];

                            for (int i = 0; i <= firstTable.Rows.Count - 1; i++)
                            {
                                for (int j = 0; j <= firstTable.Columns.Count - 1; j++)
                                {
                                    arrUserParams.Add(firstTable.Rows[i][j].ToString());
                                }
                            }



                            return arrUserParams;
                        }
                        else
                        {
                            arrUserParams.Add("False");
                            return arrUserParams;
                        }
                    }
                    catch (Exception ex)
                    {
                        arrUserParams.Add("False");
                        return arrUserParams;
                    }
                    finally
                    {
                        ds.Dispose();
                    }
                }
                else
                {
                    arrUserParams.Add("False");
                    return arrUserParams;
                }
            }

            catch (Exception ex)
            {
                arrUserParams.Add("False");
                return arrUserParams;
            }
            finally
            {
                ds.Dispose();
            }
        }


        // Activity Type=47
        [WebMethod]
        public DataSet OperaterType(string strUserName, int intUserID, string strIPAddress, int intDesignationType, string strMacAddress, string strOSDetails, string strIMEINo, string strGcmID, string strAPPVersion, int intSource, int intCreatedByType)
        {
            DataSet ds = new DataSet();
            ArrayList arrUserParams = new ArrayList();
            try
            {
                if ((intUserID > 0))
                {
                    objCreateFile.CreateTextFile(strIPAddress, strMacAddress, strOSDetails, strIMEINo, strGcmID, strAPPVersion, intSource.ToString(), intCreatedByType, 47, "Get Operater Type", strUserName);

                    string strQuery = "SELECT id as Operator_ID,operator_name as Operator_Name from er_recharge_operator_mobile;";

                    try
                    {
                        ds = objODBC.getDataSet(strQuery);

                        if (ds.Tables[0].Rows.Count > 0)
                        {





                            return ds;
                        }
                        else
                        {
                            return objODBC.getresult("Operater not available!!!");
                        }
                    }
                    catch (Exception ex)
                    {
                        return objODBC.getresult(ex.Message.ToString());
                    }
                    finally
                    {
                        ds.Dispose();
                    }
                }
                else
                {
                    return objODBC.getresult("Invalid Details");
                }
            }

            catch (Exception ex)
            {
                return objODBC.getresult(ex.Message.ToString());
            }
            finally
            {
                dt.Dispose(); ds.Dispose();
            }
        }


        // Activity Type=48
        [WebMethod]
        public DataSet OperaterBalance(string strUserName, int intUserID, string strIPAddress, int intDesignationType, string strMacAddress, string strOSDetails, string strIMEINo, string strGcmID, string strAPPVersion, int intSource, int intCreatedByType)
        {
            objCreateFile.CreateTextFile(strIPAddress, strMacAddress, strOSDetails, strIMEINo, strGcmID, strAPPVersion, intSource.ToString(), intCreatedByType, 48, "Get Operater Balance", strUserName);
            DataSet ds = new DataSet();
            ArrayList arrUserParams = new ArrayList();
            try
            {
                if ((intUserID > 0))
                {


                    string strQuery = "SELECT b.id as Wallet_ID, a.amt as Wallet_Amount,b.operator_name as Wallet_Name,a.rollback_amt as Rollback_Wallet from er_sys_wallet a inner join er_recharge_operator_mobile b on a.id=b.id ;";

                    try
                    {
                        ds = objODBC.getDataSet(strQuery);

                        if (ds.Tables[0].Rows.Count > 0)
                        {





                            return ds;
                        }
                        else
                        {
                            return objODBC.getresult("Operater Balance not available !!!");
                        }
                    }
                    catch (Exception ex)
                    {
                        return objODBC.getresult(ex.Message.ToString());
                    }
                    finally
                    {
                        ds.Dispose();
                    }
                }
                else
                {
                    return objODBC.getresult("Operater Balance not available !!!");
                }
            }

            catch (Exception ex)
            {
                return objODBC.getresult(ex.Message.ToString());
            }
            finally
            {
                ds.Dispose();
            }
        }

        // new 


        // Activity Type=51

        //int intUserID=0, int intTID=1, int intStatus=2, string strComment=3, string strIPAddress=4, int intDesignationType=5, string strMacAddress=6, string strOSDetails=7, string strIMEINo=8, string strGcmID=9, string strAPPVersion=10, int intSource=11, int intCreatedByType=12,int intActualUserID=13
        [WebMethod]
        public string ApproveRejectWalletCommission(string strUserName, string strData, int intType, int intSource, int intUtype)
        {

            // ‘intStatus =1 : Approve and 2: Reject
            int intActivityType = 51;
            string strMessage = "";
            var data = ED.Decrypt(strUserName, strData, intUtype, intType, intSource).Split(',');
            try
            {

                int intUserID = int.Parse(data[0]);
                int intTID = int.Parse(data[1]);
                int intStatus = int.Parse(data[2]);
                string strComment = data[3];
                string strIPAddress = data[4];
                int intDesignationType = int.Parse(data[5]);
                string strMacAddress = data[6];
                string strOSDetails = data[7];
                string strIMEINo = data[8];
                string strGcmID = data[9];
                string strAPPVersion = data[10];
                int intSourceData = int.Parse(data[11]);
                int intCreatedByType = int.Parse(data[12]);

                int intActualUserID = int.Parse(data[13]);
                try
                {

                    objCreateFile.CreateTextFile(strIPAddress, strMacAddress, strOSDetails, strIMEINo, strGcmID, strAPPVersion, intSourceData.ToString() + ",Data:" + strData, intCreatedByType, intActivityType, strMessage, strUserName);
                }
                catch (Exception)
                {

                }
                if ((intTID > 0) && (strComment != "" || strComment != null) && (intUserID > 0))
                {
                    int intFStatus = objODBC.executeScalar_int("SELECT `status` FROM `er_status_manager` WHERE id=1");
                    if (intFStatus == 1)
                    {
                        try
                        {
                            string strDate = objDT.getCurDateTimeString();

                            string strQuery = "";

                            if (intStatus == 1)
                            {
                                string strQueryData = "SELECT a.userid,a.request_amt,(b.ex_wallet-b.min_wallet) as WalletBalance,a.trans_number From er_wallet_purchase a,er_wallet b,er_login c Where a.parent_id=b.userid and a.userid=c.userid and c.Active=1 and a.id=" + intTID;

                                System.Data.DataSet ds = new System.Data.DataSet();

                                try
                                {
                                    ds = objODBC.getDataSet(strQueryData);

                                    if (ds.Tables[0].Rows.Count > 0)
                                    {
                                        int intChildID = Convert.ToInt32(ds.Tables[0].Rows[0][0].ToString());

                                        double dblRequestAmount = System.Convert.ToDouble(ds.Tables[0].Rows[0][1].ToString());

                                        double dblAvailableAmount = System.Convert.ToDouble(ds.Tables[0].Rows[0][2].ToString());

                                        string strTransNumber = ds.Tables[0].Rows[0][3].ToString();

                                        if (dblRequestAmount > 0)
                                        {

                                            var dblFinalWallet = dblRequestAmount;

                                            if (dblFinalWallet > dblAvailableAmount)
                                            {
                                                try
                                                {
                                                    objODBCLOG.executeNonQuery("call activity_logs('" + intActualUserID + "','" + intDesignationType + "','" + intCreatedByType + "','" + strIPAddress + "','" + strMacAddress + "','" + strOSDetails + "','" + strIMEINo + "','" + strGcmID + "','" + strAPPVersion + "','" + intSourceData + "','Approve/Reject Wallet Commission Fail','" + intActivityType + "','','" + strUserName + "')");
                                                }
                                                catch (Exception)
                                                {

                                                }

                                                objCreateFile.CreateTextFile(strIPAddress, strMacAddress, strOSDetails, strIMEINo, strGcmID, strAPPVersion, intSourceData.ToString(), intCreatedByType, intActivityType, "Approve/Reject Wallet Commission", strUserName);
                                                strMessage = "Sorry Your Balance is Low";
                                            }
                                            else
                                            {
                                                objODBC.executeNonQuery("call ap_walletTransferWithTransNumber('" + intUserID + "','" + intChildID + "','" + dblFinalWallet + "','" + strTransNumber + "')");

                                                objODBC.executeNonQuery("UPDATE er_wallet_purchase SET status=1,confirmed_by=" + intUserID + ",confirmed_on='" + strDate + "',confirmed_comment='" + strComment + "',transfer_amt='" + dblFinalWallet + "' Where id=" + intTID);
                                                strMessage = "Wallet Request successfully Approved.";
                                                try
                                                {
                                                    double dblWalletBalance = ReturnUsableWalletAmount(intUserID);
                                                    // SMPP SMS Code Below
                                                    SMPP_SMS_Format objSMPP_SMS_Format = new SMPP_SMS_Format();
                                                    string strGetSMS = objSMPP_SMS_Format.FundRequestApproveWallet(strUserName, objODBC.executeScalar_str("SELECT full_name From er_login Where userid='" + intUserID + "'").ToString(), intSource, dblFinalWallet, strTransNumber, dblWalletBalance).ToString();
                                                    objComFun.er_insert_notification(intUserID, "Fund Request Approve", strGetSMS, 2);
                                                    SMPP1 objSMPP1 = new SMPP1();
                                                    string strSMPPResponse = "NA";
                                                    DataTable dtSMPP = new DataTable();
                                                    try
                                                    {
                                                        dtSMPP = objODBC.getDataTable("Select mobile,operator_id from er_mobile where userid='" + intUserID + "' and usertype=2 and mob_type=1");

                                                        if (dtSMPP.Rows.Count > 0)
                                                        {
                                                            string strMobile = dtSMPP.Rows[0][0].ToString();
                                                            int intOperaterID = int.Parse(dtSMPP.Rows[0][1].ToString());

                                                            objComFun.er_SMS_Alert(intUserID, 2, strMobile, intOperaterID, strGetSMS, 1, strSMPPResponse);

                                                        }
                                                    }
                                                    catch (Exception)
                                                    {

                                                    }
                                                    finally
                                                    {
                                                        dtSMPP.Dispose();
                                                    }
                                                }
                                                catch (Exception ex)
                                                {
                                                }

                                                objCreateFile.CreateTextFile(strIPAddress, strMacAddress, strOSDetails, strIMEINo, strGcmID, strAPPVersion, intSourceData.ToString(), intCreatedByType, intActivityType, "Approve/Reject Wallet Commission", strUserName);


                                            }
                                        }
                                        else
                                        {
                                            strMessage = "Invalid Amount !";
                                        }
                                    }
                                    else
                                    {

                                        objCreateFile.CreateTextFile(strIPAddress, strMacAddress, strOSDetails, strIMEINo, strGcmID, strAPPVersion, intSourceData.ToString(), intCreatedByType, intActivityType, "Kindly update commission amount.", strUserName);
                                        strMessage = "Kindly enter valid details";
                                    }
                                }
                                catch (Exception ex)
                                {
                                }

                                finally
                                {
                                    ds.Dispose();
                                }
                            }
                            else if (intStatus == 2)
                            {
                                strQuery = "UPDATE er_wallet_purchase SET status=2,confirmed_by=" + intUserID + ",confirmed_on='" + strDate + "',confirmed_comment='" + strComment + "' Where id=" + intTID;
                                objODBC.executeNonQuery(strQuery);
                                strMessage = "Wallet Request successfully Rejected.";
                            }
                        }
                        catch (Exception ex)
                        {
                            strMessage = ex.Message.ToString();


                        }
                    }
                    else
                    {
                        strMessage = "Permission denied by system !";
                    }
                }
                else
                {
                    strMessage = "Kindly Enter Valid Details";
                }
                try
                {
                    objODBCLOG.executeNonQuery("call activity_logs('" + intActualUserID + "','" + intDesignationType + "','" + intCreatedByType + "','" + strIPAddress + "','" + strMacAddress + "','" + strOSDetails + "','" + strIMEINo + "','" + strGcmID + "','" + strAPPVersion + "','" + intSourceData + "','" + strMessage + "','" + intActivityType + "','','" + strUserName + "')");

                }
                catch (Exception)
                {

                }
            }
            catch (Exception ex)
            {
                strMessage = ex.Message.ToString();
            }
            return strMessage;
        }

        // Activity Type=52

        [WebMethod]
        public ArrayList ExReturnPendingPurchaseHistory(string strUserName, int intUID, string strIPAddress, int intDesignationType, string strMacAddress, string strOSDetails, string strIMEINo, string strGcmID, string strAPPVersion, int intSource, int intCreatedByType)
        {

            string strQuery = "", strResponse = "";
            ArrayList arrUserParams = new ArrayList();
            int intActivityType = 52;
            objCreateFile.CreateTextFile(strIPAddress, strMacAddress, strOSDetails, strIMEINo, strGcmID, strAPPVersion, intSource.ToString(), intCreatedByType, intActivityType, strResponse, strUserName);
            try
            {
                int intCountHistory = objODBC.executeScalar_int("select count(1) From er_wallet_purchase a,er_login b Where a.userid=b.userid and a.status=0 and a.parent_id=" + intUID + "");


                strQuery = "SELECT a.id,a.trans_number,a.request_amt,a.payment_mode,a.rcpt_url,a.comment,b.username,b.email,b.mobile,b.full_name, a.created_on,a.userid," + intCountHistory + " as intCount From er_wallet_purchase a,er_login b Where a.userid=b.userid and a.status=0 and a.parent_id=" + intUID;

                objODBC.executeNonQuery(strQuery);

                System.Data.DataSet ds = new System.Data.DataSet();

                try
                {
                    ds = objODBC.getDataSet(strQuery);

                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        arrUserParams.Add("TRUE");

                        arrUserParams.Add((ds.Tables[0].Rows.Count).ToString());

                        arrUserParams.Add("12");

                        System.Data.DataTable firstTable = ds.Tables[0];

                        for (int i = 0; i <= firstTable.Rows.Count - 1; i++)
                        {
                            for (int j = 0; j <= firstTable.Columns.Count - 1; j++)

                                arrUserParams.Add(firstTable.Rows[i][j].ToString());
                        }
                        strResponse = "Return Pending Purchase History Fetch Sucessfully !";
                    }
                    else
                    {
                        strResponse = "Return Pending Purchase History Fetching Fail !";
                        arrUserParams.Add("FALSE");
                    }


                }
                catch (Exception ex)
                {
                    arrUserParams.Add(ex.Message.ToString());
                    strResponse = "Return Pending Purchase History Fetching Fail !" + ex.ToString();

                }

                finally
                {
                    ds.Dispose();
                }
            }
            catch (Exception ex)
            {
                arrUserParams.Add(ex.Message.ToString());
                strResponse = "Return Pending Purchase History Fetching Fail !" + ex.ToString();

            }



            return arrUserParams;
        }

        // Activity Type=53
        [WebMethod]
        public ArrayList ExReturnPendingPurchaseHistoryDetails(string strUserName, int intUID, int intTID, string strIPAddress, int intDesignationType, string strMacAddress, string strOSDetails, string strIMEINo, string strGcmID, string strAPPVersion, int intSource, int intCreatedByType)
        {
            string strQuery = "", strResponse = "";
            ArrayList arrUserParams = new ArrayList();
            int intActivityType = 53;
            objCreateFile.CreateTextFile(strIPAddress, strMacAddress, strOSDetails, strIMEINo, strGcmID, strAPPVersion, intSource.ToString(), intCreatedByType, intActivityType, strResponse, strUserName);
            try
            {
                strQuery = "SELECT trans_number,request_amt,payment_mode,rcpt_url,comment From er_wallet_purchase Where parent_id=" + intUID + " and id=" + intTID;

                objODBC.executeNonQuery(strQuery);

                System.Data.DataSet ds = new System.Data.DataSet();

                try
                {
                    ds = objODBC.getDataSet(strQuery);

                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        arrUserParams.Add("TRUE");

                        arrUserParams.Add((ds.Tables[0].Rows.Count).ToString());

                        arrUserParams.Add("5");

                        System.Data.DataTable firstTable = ds.Tables[0];

                        for (int i = 0; i <= firstTable.Rows.Count - 1; i++)
                        {
                            for (int j = 0; j <= firstTable.Columns.Count - 1; j++)

                                arrUserParams.Add(firstTable.Rows[i][j].ToString());
                        }
                        strResponse = "Return Pending Purchase History Details Sucessfully !";

                    }
                    else
                    {
                        strResponse = "Return Pending Purchase History Details Fetched Fails !";
                        arrUserParams.Add("FALSE");
                    }


                }
                catch (Exception ex)
                {
                    arrUserParams.Add(ex.Message.ToString());
                    strResponse = "Return Pending Purchase History Details Fetched Fails !" + ex.ToString();

                }

                finally
                {
                    ds.Dispose();
                }
            }
            catch (Exception ex)
            {
                arrUserParams.Add(ex.Message.ToString());
                strResponse = "Return Pending Purchase History Details Fetched Fails !" + ex.ToString();

            }




            return arrUserParams;
        }

        // int ActivityType =54
        //int intParentID, int intUserID, double dblAmount, string strIPAddress, int intDesignationType, string strMacAddress, string strOSDetails, string strIMEINo, string strGcmID, string strAPPVersion, int intSource, int intCreatedByType,int intActualUserID=12

        [WebMethod]
        public string ExWalletFundRevert(string strUserName, string strData, int intType, int intSource, int intUtype)
        {
            string strMessage = "";

            var data = ED.Decrypt(strUserName, strData, intUtype, 1, intSource).Split(',');
            int intActivityType = 54;

            try
            {
                int intParentID = int.Parse(data[0]);
                int intUserID = int.Parse(data[1]);
                double dblAmount = double.Parse(data[2]);
                string strIPAddress = data[3];
                int intDesignationType = int.Parse(data[4]);
                string strMacAddress = data[5];
                string strOSDetails = data[6];
                string strIMEINo = data[7];
                string strGcmID = data[8];
                string strAPPVersion = data[9];
                int intSourceData = int.Parse(data[10]);
                int intCreatedByType = int.Parse(data[11]);
                int intActualUserID = int.Parse(data[12]);

                try
                {


                    objCreateFile.CreateTextFile(strIPAddress, strMacAddress, strOSDetails, strIMEINo, strGcmID, strAPPVersion, intSourceData.ToString(), intCreatedByType, intActivityType, "Wallet Fund Revert", strUserName);
                }
                catch (Exception)
                {

                }

                double dblUserBalance = GetWalletAmount(intUserID);

                if ((dblUserBalance >= dblAmount) && (dblAmount > 0))
                {
                    objODBC.executeNonQuery("Call ap_WalletFundReverse(" + intParentID + "," + intUserID + "," + dblAmount + ")");

                    strMessage = "Wallet Fund Successfully Reverted.";


                    // Revert Balance
                    try
                    {
                        double dblWalletBalance = GetWalletAmount(intUserID);
                        // SMPP SMS Code Below
                        SMPP_SMS_Format objSMPP_SMS_Format = new SMPP_SMS_Format();
                        string strGetSMS = objSMPP_SMS_Format.ReverseFundRequestWallet(strUserName, objODBC.executeScalar_str("SELECT full_name From er_login Where userid='" + intUserID + "'").ToString(), intSource, dblAmount, dblWalletBalance).ToString();
                        objComFun.er_insert_notification(intUserID, "Fund Request Reverse", strGetSMS, 2);
                        SMPP1 objSMPP1 = new SMPP1();
                        string strSMPPResponse = "NA";
                        DataTable dtSMPP = new DataTable();
                        try
                        {
                            dtSMPP = objODBC.getDataTable("Select mobile,operator_id from er_mobile where userid='" + intUserID + "' and usertype=2 and mob_type=1");

                            if (dtSMPP.Rows.Count > 0)
                            {
                                string strMobile = dtSMPP.Rows[0][0].ToString();
                                int intOperaterID = int.Parse(dtSMPP.Rows[0][1].ToString());

                                objComFun.er_SMS_Alert(intUserID, 2, strMobile, intOperaterID, strGetSMS, 1, strSMPPResponse);

                            }
                        }
                        catch (Exception)
                        {

                        }
                        finally
                        {
                            dtSMPP.Dispose();
                        }
                    }
                    catch (Exception ex)
                    {
                    }



                }
                else
                {
                    strMessage = "Member do not have sufficient Balance in Wallet !";
                }
                try
                {
                    objODBCLOG.executeNonQuery("call activity_logs('" + intActualUserID + "','" + intDesignationType + "','" + intCreatedByType + "','" + strIPAddress + "','" + strMacAddress + "','" + strOSDetails + "','" + strIMEINo + "','" + strGcmID + "','" + strAPPVersion + "','" + intSourceData + "','Wallet Fund Revert','" + intActivityType + "','','')");


                }
                catch (Exception)
                {

                }
            }
            catch (Exception ex)
            {

                strMessage = "Please referesh the page and try again ";

            }
            return strMessage;
        }


        [WebMethod]
        public string ExWalletFundReverttoDirect(string strUserName, string strData, int intType, int intSource, int intUtype)
        {
            string strMessage = "";

            var data = ED.Decrypt(strUserName, strData, intUtype, 1, intSource).Split(',');
            int intActivityType = 54;

            try
            {
                int intParentID = int.Parse(data[0]);
                int intUserID = int.Parse(data[1]);
                double dblAmount = double.Parse(data[2]);
                string strIPAddress = data[3];
                int intDesignationType = int.Parse(data[4]);
                string strMacAddress = data[5];
                string strOSDetails = data[6];
                string strIMEINo = data[7];
                string strGcmID = data[8];
                string strAPPVersion = data[9];
                int intSourceData = int.Parse(data[10]);
                int intCreatedByType = int.Parse(data[11]);
                int intActualUserID = int.Parse(data[12]);

                try
                {
                    objCreateFile.CreateTextFile(strIPAddress, strMacAddress, strOSDetails, strIMEINo, strGcmID, strAPPVersion, intSourceData.ToString(), intCreatedByType, intActivityType, "Wallet Fund Revert", strUserName);
                }
                catch (Exception)
                {

                }

                double dblUserBalance = GetWalletAmount(intUserID);

                if ((dblUserBalance >= dblAmount) && (dblAmount > 0))
                {
                    objODBC.executeNonQuery("Call ap_WalletFundReverttoDirect(" + intUserID + "," + dblAmount + ")");

                    strMessage = "Wallet Fund Successfully Reverted.";


                    // Revert Balance
                    try
                    {
                        double dblWalletBalance = GetWalletAmount(intUserID);
                        // SMPP SMS Code Below
                        SMPP_SMS_Format objSMPP_SMS_Format = new SMPP_SMS_Format();
                        string strGetSMS = objSMPP_SMS_Format.ReverseFundRequestWallet(strUserName, objODBC.executeScalar_str("SELECT full_name From er_login Where userid='" + intUserID + "'").ToString(), intSource, dblAmount, dblWalletBalance).ToString();
                        objComFun.er_insert_notification(intUserID, "Fund Request Reverse", strGetSMS, 2);
                        SMPP1 objSMPP1 = new SMPP1();
                        string strSMPPResponse = "NA";
                        DataTable dtSMPP = new DataTable();
                        try
                        {
                            dtSMPP = objODBC.getDataTable("Select mobile,operator_id from er_mobile where userid='" + intUserID + "' and usertype=2 and mob_type=1");

                            if (dtSMPP.Rows.Count > 0)
                            {
                                string strMobile = dtSMPP.Rows[0][0].ToString();
                                int intOperaterID = int.Parse(dtSMPP.Rows[0][1].ToString());

                                objComFun.er_SMS_Alert(intUserID, 2, strMobile, intOperaterID, strGetSMS, 1, strSMPPResponse);

                            }
                        }
                        catch (Exception)
                        {

                        }
                        finally
                        {
                            dtSMPP.Dispose();
                        }
                        
                        int intPID = objODBC.executeScalar_int("SELECT parent_id FROM er_login WHERE userid='"+ intUserID + "'");
                        double dblReceiverBalance = GetWalletAmount(intPID);
                        // SMPP SMS Code Below Receiver                                      
                        string strGetReceiverSMS = objSMPP_SMS_Format.FundTransferWalletReceiver_reverse(objODBC.executeScalar_str("SELECT username From er_login Where userid='" + intPID + "'").ToString(), objODBC.executeScalar_str("SELECT full_name From er_login Where userid='" + intPID + "'").ToString(), intSource, dblAmount, objODBC.executeScalar_str("SELECT full_name From er_login Where userid='" + intUserID + "'").ToString(), "", dblReceiverBalance).ToString();
                        objComFun.er_insert_notification(intPID, "Fund Request", strGetReceiverSMS, intCreatedByType);

                        strSMPPResponse = "NA";
                        DataTable dtSMPP1 = new DataTable();
                        try
                        {


                            dtSMPP1 = objODBC.getDataTable("Select mobile,operator_id from er_mobile where userid='" + intPID + "' and usertype=2 and mob_type=1");

                            if (dtSMPP1.Rows.Count > 0)
                            {
                                string strMobile = dtSMPP1.Rows[0][0].ToString();
                                int intOperaterID = int.Parse(dtSMPP1.Rows[0][1].ToString());

                                objComFun.er_SMS_Alert(intPID, 2, strMobile, intOperaterID, strGetReceiverSMS, 1, strSMPPResponse);

                            }
                        }
                        catch (Exception)
                        {

                        }
                        finally
                        {
                            dtSMPP.Dispose();
                        }
                    }
                    catch (Exception ex)
                    {
                    }

                }
                else
                {
                    strMessage = "Member do not have sufficient Balance in Wallet !";
                }
                try
                {
                    objODBCLOG.executeNonQuery("call activity_logs('" + intActualUserID + "','" + intDesignationType + "','" + intCreatedByType + "','" + strIPAddress + "','" + strMacAddress + "','" + strOSDetails + "','" + strIMEINo + "','" + strGcmID + "','" + strAPPVersion + "','" + intSourceData + "','Wallet Fund Revert','" + intActivityType + "','','" + strUserName + "')");


                }
                catch (Exception)
                {

                }
            }
            catch (Exception ex)
            {

                strMessage = "Please referesh the page and try again ";

            }
            return strMessage;
        }


        // Activity Type=55
        [WebMethod]
        public DataSet PendingPurchaseHistory(string strUserName, int intUID, string strIPAddress, int intDesignationType, string strMacAddress, string strOSDetails, string strIMEINo, string strGcmID, string strAPPVersion, int intSource, int intCreatedByType, string strSearch)
        {
            int intActivityType = 55;
            string strQuery = "";
            ArrayList arrUserParams = new ArrayList(); DataSet ds = new DataSet();

            try
            {
                int intCount = objODBC.executeScalar_int("SELECT count(1) From er_wallet_purchase a,er_login b Where a.userid=b.userid and a.status=0 and a.parent_id=" + intUID + " " + strSearch + "");

                strQuery = "SELECT a.id as ReqID,b.full_name as Full_Name,b.emailid as Email_ID,b.mobile as Mobile_Number, case when a.payment_mode=1 then 'Cash' when a.payment_mode=2 then 'Bank Transfer' end as Payment_Mode,a.request_amt as Amount,date_format(a.created_on,'%d %b %Y %H:%i:%s') as created_on,a.rcpt_url as Receipt,a.userid,a.trans_number, a.comment,b.username as User_Name," + intCount + " as intCount From er_wallet_purchase a,er_login b Where a.userid=b.userid and a.status=0 and a.parent_id=" + intUID + " " + strSearch;

                try
                {
                    ds = objODBC.getDataSet(strQuery);



                    if (ds.Tables[0].Rows.Count > 0)
                    {


                        objCreateFile.CreateTextFile(strIPAddress, strMacAddress, strOSDetails, strIMEINo, strGcmID, strAPPVersion, intSource.ToString(), intCreatedByType, intActivityType, "Pending Purchase History Fetch Successfully", strUserName);

                        return ds;
                    }
                    else
                    {
                        return objODBC.getresult("Records not found !!!");
                    }
                }
                catch (Exception ex)
                {
                    return objODBC.getresult(ex.Message.ToString());
                }

                finally
                {
                    ds.Dispose();
                }
            }
            catch (Exception ex)
            {
                return objODBC.getresult(ex.Message.ToString());
            }
        }

        [WebMethod]
        public ArrayList er_android_PendingPurchaseHistory(string strUserName, int intUID, string strIPAddress, int intDesignationType, string strMacAddress, string strOSDetails, string strIMEINo, string strGcmID, string strAPPVersion, int intSource, int intCreatedByType, string strSearch)
        {
            ArrayList arlst = new ArrayList();
            DataSet ds = new DataSet();
            ds = PendingPurchaseHistory(strUserName, intUID, strIPAddress, intDesignationType, strMacAddress, strOSDetails, strIMEINo, strGcmID, strAPPVersion, intSource, intCreatedByType, strSearch);

            arlst = objComFun.Get_ArrayLlist_From_Dataset(ds);
            return arlst;

        }



        // Activity Type=56
        [WebMethod]
        public ArrayList GetReverseFundReport(string strUserName, int intUserID, string strSearch, int intPage, string strIPAddress, int intDesignationType, string strMacAddress, string strOSDetails, string strIMEINo, string strGcmID, string strAPPVersion, int intSource, int intCreatedByType)
        {
            string strQuery = "";
            string strLimit = "";
            ArrayList arrUserParams = new ArrayList();
            int intActivityType = 56;
            try
            {
                strLimit = " LIMIT " + 20 * (intPage - 1) + ",20";
                strQuery = "SELECT a.userid,b.full_name as name,b.username,b.mobile, a.trans_number,a.total_amt,DATE_FORMAT(a.created_on,'%d-%M-%Y %T') as created_on,c.ex_wallet as Balance From ap_reverse_fund a INNER JOIN er_login b ON a.userid=b.userid INNER JOIN er_wallet c on a.userid=c.userid Where a.parent_id= " + intUserID + strSearch + " Order by a.id DESC " + strLimit;

                arrUserParams = getReport(strQuery);



                objCreateFile.CreateTextFile(strIPAddress, strMacAddress, strOSDetails, strIMEINo, strGcmID, strAPPVersion, intSource.ToString(), intCreatedByType, intActivityType, "Get Reverse Fund Report Fetch Sucessfully", strUserName);
                return arrUserParams;
            }
            catch (Exception ex)
            {


                objCreateFile.CreateTextFile(strIPAddress, strMacAddress, strOSDetails, strIMEINo, strGcmID, strAPPVersion, intSource.ToString(), intCreatedByType, intActivityType, "Get Reverse Fund Report Fetch Fail", strUserName);
                return arrUserParams;
            }
        }

        public ArrayList getReport(string strQuery)
        {
            ODBC obj = new ODBC();
            ArrayList arrUserParams = new ArrayList();
            try
            {
                System.Data.DataSet ds = new System.Data.DataSet();
                try
                {
                    ds = obj.getDataSet(strQuery);

                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        arrUserParams.Add("TRUE");

                        System.Data.DataTable firstTable = ds.Tables[0];
                        arrUserParams.Add(firstTable.Rows.Count);
                        arrUserParams.Add(firstTable.Columns.Count);
                        for (int i = 0; i <= firstTable.Rows.Count - 1; i++)
                        {
                            for (int j = 0; j <= firstTable.Columns.Count - 1; j++)
                                arrUserParams.Add(firstTable.Rows[i][j].ToString());
                        }
                    }
                    else
                        arrUserParams.Add("FALSE");

                    return arrUserParams;
                }
                catch (Exception ex)
                {
                    arrUserParams.Add(ex.Message.ToString());

                    return arrUserParams;
                }

                finally
                {
                    ds.Dispose();
                }
            }


            catch (Exception ex)
            {
                arrUserParams.Add(ex.Message.ToString());

                return arrUserParams;
            }
        }


        // Activity Type=57
        [WebMethod]
        public ArrayList ManageGroupUser(string strUserName, int intGroupID, string strSearch, int intPage, string strIPAddress, int intDesignationType, string strMacAddress, string strOSDetails, string strIMEINo, string strGcmID, string strAPPVersion, int intSource, int intCreatedByType)
        {
            string strQuery = "";
            string strLimit = "";
            ArrayList arrUserParams = new ArrayList();
            int intActivityType = 57;
            try
            {
                strLimit = " LIMIT " + 20 * (intPage - 1) + ",20";

                strQuery = "SELECT b.username as UserName,b.full_name,b.emailid,b.mobile,b.userid,a.group_id From er_wallet_transfer_group_details a INNER JOIN er_login b ON a.userid=b.userid Where a.group_id= " + intGroupID + " " + strSearch + " Order by a.id DESC " + strLimit;

                arrUserParams = getReport(strQuery);


                objCreateFile.CreateTextFile(strIPAddress, strMacAddress, strOSDetails, strIMEINo, strGcmID, strAPPVersion, intSource.ToString(), intCreatedByType, intActivityType, "Manage Group User Fetch Sucessfully(Group ID)", strUserName);
                return arrUserParams;
            }
            catch (Exception ex)
            {

                objCreateFile.CreateTextFile(strIPAddress, strMacAddress, strOSDetails, strIMEINo, strGcmID, strAPPVersion, intSource.ToString(), intCreatedByType, intActivityType, "Manage Group User Fetch Fail(Group ID)", strUserName);
                return arrUserParams;
            }
        }

        [WebMethod]
        public ArrayList er_android_ManageGroupUser(string strUserName, int intParentID, int intGroupID, string strSearch, int intPage, string strIPAddress, int intDesignationType, string strMacAddress, string strOSDetails, string strIMEINo, string strGcmID, string strAPPVersion, int intSource, int intCreatedByType)
        {

            ArrayList arrUserParams = new ArrayList();
            DataSet ds = new DataSet();
            ds = ManageGroupUserDetails(strUserName, intParentID, intGroupID, strSearch, intPage, strIPAddress, intDesignationType, strMacAddress, strOSDetails, strIMEINo, strGcmID, strAPPVersion, intSource, intCreatedByType);
            arrUserParams = objComFun.Get_ArrayLlist_From_Dataset(ds);
            return arrUserParams;
        }

        // Activity Type=57
        [WebMethod]
        public DataSet ManageGroupUserDetails(string strUserName, int intParentID, int intGroupID, string strSearch, int intPage, string strIPAddress, int intDesignationType, string strMacAddress, string strOSDetails, string strIMEINo, string strGcmID, string strAPPVersion, int intSource, int intCreatedByType)
        {
            string strQuery = "";
            string strLimit = "";
            ArrayList arrUserParams = new ArrayList(); DataSet ds = new DataSet();
            int intActivityType = 57;
            try
            {
                strLimit = " LIMIT " + 20 * (intPage - 1) + ",20";

                if (String.IsNullOrEmpty(strSearch))
                {
                    strQuery = "SELECT b.username as UserName,b.full_name,b.emailid,b.mobile,b.userid,a.group_id,1 as Status From er_wallet_transfer_group_details a INNER JOIN er_login b ON a.userid=b.userid Where a.group_id= " + intGroupID + " and b.user_status=1 and b.parent_id='" + intParentID + "'  Order by a.id DESC " + strLimit;
                }
                else
                {
                    strQuery = "SELECT b.username as UserName,b.full_name,b.emailid,b.mobile,b.userid,a.group_id,case when isnull(a.group_id) then 2 when a.group_id <> " + intGroupID + " then 2 else 1 end as Status From er_wallet_transfer_group_details a right JOIN er_login b ON a.userid=b.userid Where b.Active= 1 and b.user_status=1 and b.parent_id='" + intParentID + "'  " + strSearch + " or (a.group_id = " + intGroupID + " and b.parent_id='" + intParentID + "')  group by b.username Order by a.id DESC " + strLimit;


                }

                ds = Report(strQuery);


                objCreateFile.CreateTextFile(strIPAddress, strMacAddress, strOSDetails, strIMEINo, strGcmID, strAPPVersion, intSource.ToString(), intCreatedByType, intActivityType, "Manage Group User Fetch Sucessfully(Group ID)", strUserName);
                return ds;
            }
            catch (Exception ex)
            {

                objCreateFile.CreateTextFile(strIPAddress, strMacAddress, strOSDetails, strIMEINo, strGcmID, strAPPVersion, intSource.ToString(), intCreatedByType, intActivityType, "Manage Group User Fetch Fail(Group ID)", strUserName);
                return ds;
            }
        }

        public DataSet Report(string strQuery)
        {
            ODBC obj = new ODBC();
            System.Data.DataSet ds = new System.Data.DataSet();

            try
            {
                ds = obj.getDataSet(strQuery);

                if (ds.Tables[0].Rows.Count > 0)
                {
                    return ds;
                }
                else
                {
                    return objODBC.getresult("Records not found !!!");
                }

            }
            catch (Exception ex)
            {
                return objODBC.getresult(ex.Message.ToString());
            }
            finally
            {
                ds.Dispose();
            }
        }

        // Activity Type=56
        [WebMethod]
        public DataSet ReverseFundReport(string strUserName, int intUserID, string strSearch, int intPage, string strIPAddress, int intDesignationType, string strMacAddress, string strOSDetails, string strIMEINo, string strGcmID, string strAPPVersion, int intSource, int intCreatedByType)
        {
            string strQuery = "";
            string strLimit = "";
            DataSet ds = new DataSet();
            int intActivityType = 56;
            try
            {
                strLimit = " LIMIT " + 20 * (intPage - 1) + ",20";
                strQuery = "SELECT a.userid,b.full_name as name,b.username,b.mobile, a.trans_number,a.total_amt,DATE_FORMAT(a.created_on,'%d-%M-%Y %T') as created_on,c.ex_wallet as Balance From ap_reverse_fund a INNER JOIN er_login b ON a.userid=b.userid INNER JOIN er_wallet c on a.userid=c.userid Where a.parent_id= " + intUserID + strSearch + " Order by a.id DESC " + strLimit;

                ds = Report(strQuery);


                objCreateFile.CreateTextFile(strIPAddress, strMacAddress, strOSDetails, strIMEINo, strGcmID, strAPPVersion, intSource.ToString(), intCreatedByType, intActivityType, "Get Reverse Fund Report Fetch Sucessfully", strUserName);
                return ds;
            }
            catch (Exception ex)
            {

                objCreateFile.CreateTextFile(strIPAddress, strMacAddress, strOSDetails, strIMEINo, strGcmID, strAPPVersion, intSource.ToString(), intCreatedByType, intActivityType, "Get Reverse Fund Report Fetch Fail", strUserName);
                return ds;
            }
        }

        // Activity Type=71

        [WebMethod]
        public DataSet Ex_wallet_threshold_all(string strUserName, int intUID, string strIPAddress, int intDesignationType, string strMacAddress, string strOSDetails, string strIMEINo, string strGcmID, string strAPPVersion, int intSource, int intCreatedByType)
        {
            string strQuery = "", strResponse = "";
            int intActivityType = 71;

            strQuery = "select usertype_id as UserType,amt as Amount,case when usertype_id=3 then 'Distributor'  when usertype_id=4 then 'Sub Distributor'  when usertype_id=5 then 'Retailer' end as user_type from er_wallet_threshold;";

            objODBC.executeNonQuery(strQuery);

            System.Data.DataSet ds = new System.Data.DataSet();

            try
            {
                ds = Report(strQuery);

                strResponse = "Ex Wallet Threshold Fetch Sucessfully";


                objCreateFile.CreateTextFile(strIPAddress, strMacAddress, strOSDetails, strIMEINo, strGcmID, strAPPVersion, intSource.ToString(), intCreatedByType, intActivityType, strResponse, strUserName);

                return ds;
            }
            catch (Exception ex)
            {

                strResponse = "Return Ex Wallet Threshold Fetch Failed !" + ex.ToString();


                objCreateFile.CreateTextFile(strIPAddress, strMacAddress, strOSDetails, strIMEINo, strGcmID, strAPPVersion, intSource.ToString(), intCreatedByType, intActivityType, strResponse, strUserName);
                return ds;
            }

            finally
            {
                ds.Dispose();
            }



        }


        //  int intActivityType = 72;
        [WebMethod]
        public string ws_er_wallet_threshold(string strUserName, int intUserID, int usertype_id, double dblAmt, string strIPAddress, int intDesignationType, string strMacAddress, string strOSDetails, string strIMEINo, string strGcmID, string strAPPVersion, int intSource, int intCreatedByType)
        {
            int intActivityType = 72;
            DataSet ds = new DataSet();
            string strMessage = string.Empty;
            string strLimit = string.Empty;
            if (usertype_id > 0 && intUserID > 0 && dblAmt > 0)
            {
                try
                {
                    string strQuery = "select count(1) from er_wallet_threshold where usertype_id=" + usertype_id + "";
                    int chkGroupIDCount = objODBC.executeScalar_int(strQuery);
                    if (chkGroupIDCount > 0)
                    {
                        string strQuery1 = "update er_wallet_threshold set amt=" + dblAmt + " where usertype_id=" + usertype_id + "";
                        objODBC.executeNonQuery(strQuery1);

                        strMessage = "Threshold updated successfully";
                    }
                }
                catch (Exception)
                {
                    strMessage = "somthing went wrong";

                }
            }
            else
            {
                strMessage = "please send valid details";

            }

            objCreateFile.CreateTextFile(strIPAddress, strMacAddress, strOSDetails, strIMEINo, strGcmID, strAPPVersion, intSource.ToString(), intCreatedByType, intActivityType, strMessage, strUserName);

            return strMessage;
        }


        // ActivityType=86
        [WebMethod]
        public DataSet er_AdminSystemWallet(string strUserName, int intUserID, string strSearch, string strIPAddress, int intDesignationType, string strMacAddress, string strOSDetails, string strIMEINo, string strGcmID, string strAPPVersion, int intSource, int intCreatedByType)
        {
            DataSet ds = new DataSet();
            string strLimit, strQuery = "";


            try
            {
                objCreateFile.CreateTextFile(strIPAddress, strMacAddress, strOSDetails, strIMEINo, strGcmID, strAPPVersion, intSource.ToString(), intCreatedByType, 83, " er_Denomination_Wise_Report ", strUserName);
            }
            catch (Exception)
            {


            }
            finally
            {

                if (intUserID > 0)
                {
                    strQuery = "select id,amt from er_sys_wallet where 1=1 " + strSearch + "";
                }
                ds = objODBC.Report(strQuery);
            }
            return ds;

        }


        // Activity Type=121
        [WebMethod]
        public ArrayList checkValidUser(string strUserName, int intUserID, string strUserDetails, string strIPAddress, int intDesignationType, string strMacAddress, string strOSDetails, string strIMEINo, string strGcmID, string strAPPVersion, int intSource, int intCreatedByType)
        {
            string strQuery = "";
            string strLimit = "";
            int intActivityType = 121;
            DataSet ds = new DataSet();
            ArrayList arrUserParams = new ArrayList();
            try
            {
                objCreateFile.CreateTextFile(strIPAddress, strMacAddress, strOSDetails, strIMEINo, strGcmID, strAPPVersion, intSource.ToString(), intCreatedByType, intActivityType, "check Valid User", strUserName);
            }
            catch (Exception)
            {

            }
            try
            {
                int intCountUser = objODBC.executeNonQuery("SELECT count(1) FROM er_login where (username='" + strUserDetails + "' or mobile='" + strUserDetails + "') and user_status=1 and Active=1 and parent_id='" + intUserID + "'");
                if (intCountUser > 0)
                {
                    ds = objODBC.getDataSet("SELECT userid,full_name,mobile,username FROM er_login where (username='" + strUserDetails + "' or mobile='" + strUserDetails + "') and user_status=1 and Active=1  and parent_id='" + intUserID + "' ");
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        string strUserID = ds.Tables[0].Rows[0]["userid"].ToString();
                        string strUName = ds.Tables[0].Rows[0]["username"].ToString();
                        string strFullName = ds.Tables[0].Rows[0]["full_name"].ToString();
                        string strMobile = ds.Tables[0].Rows[0]["mobile"].ToString();
                        arrUserParams.Add("TRUE");
                        arrUserParams.Add(strUserID);
                        arrUserParams.Add(strUName);
                        arrUserParams.Add(strFullName);
                        arrUserParams.Add(strMobile);
                    }
                    else
                    {
                        arrUserParams.Add("FALSE");
                        arrUserParams.Add("Member does not belong to login member");
                    }

                }
                else
                {

                    arrUserParams.Add("FALSE");
                    arrUserParams.Add("Member does not belong to login member");
                }
                return arrUserParams;
            }
            catch (Exception ex)
            {

            }
            finally
            {
                ds.Dispose();
            }
            return arrUserParams;
        }



        // Activity Type=125
        [WebMethod]
        public int getGroupIDCount(string strUserName, int intGroupID, string strIPAddress, int intDesignationType, string strMacAddress, string strOSDetails, string strIMEINo, string strGcmID, string strAPPVersion, int intSource, int intCreatedByType)
        /* TODO ERROR: Skipped SkippedTokensTrivia */
        {
            int intGroupCount = 0;
            string strMessage = string.Empty;
            try
            {
                objCreateFile.CreateTextFile(strIPAddress, strMacAddress, strOSDetails, strIMEINo, strGcmID, strAPPVersion, intSource.ToString(), intCreatedByType, 125, " getGroupIDCount", strUserName);
            }
            catch (Exception)
            { }
            try
            {
                if ((intDesignationType != 0) && (strIPAddress != "" || strIPAddress != null) && (strMacAddress != "" || strMacAddress != null) && (strOSDetails != "" || strOSDetails != null) && (strIMEINo != "" || strIMEINo != null) && (strGcmID != "" || strGcmID != null) && (strAPPVersion != "" || strAPPVersion != null) && (intSource != 0) && (intCreatedByType != 0))
                {

                    intGroupCount = objODBC.executeScalar_int("SELECT count(1) FROM er_wallet_transfer_group_details WHERE group_id=" + intGroupID);
                }
                else
                {
                    intGroupCount = 0;
                }
                return intGroupCount;
            }
            catch (Exception ex)
            {
                return intGroupCount;
            }
        }

        //select l.username,l.full_name,l.mobile,l.emailid from er_login l inner join er_wallet_transfer_group_details e on l.userid = e.userid where e.group_id = 5
        [WebMethod]
        public DataSet er_getGroupdetails_bulktransfer(string strUserName, int intGroupID, string strIPAddress, int intDesignationType, string strMacAddress, string strOSDetails, string strIMEINo, string strGcmID, string strAPPVersion, int intSource, int intCreatedByType)
        /* TODO ERROR: Skipped SkippedTokensTrivia */
        {
            DataSet ds = new DataSet();
            string strMessage = string.Empty;
            try
            {
                objCreateFile.CreateTextFile(strIPAddress, strMacAddress, strOSDetails, strIMEINo, strGcmID, strAPPVersion, intSource.ToString(), intCreatedByType, 125, " er_getGroupdetails_bulktransfer", strUserName);
            }
            catch (Exception)
            { }
            finally
            {
                try
                {
                    if (intGroupID > 0)
                    {

                        ds = objODBC.getDataSet("select l.username,l.full_name,l.mobile,l.emailid from er_login l inner join er_wallet_transfer_group_details e on l.userid = e.userid where e.group_id =" + intGroupID);

                    }
                    else
                    {
                        intGroupID = 0;
                    }

                }
                catch (Exception ex)
                {

                }
            }

            return ds;
        }


        // ActivityType=60
        [WebMethod]
        public DataSet AllOperatorWalletDetails(string strUserName, int intUserID, string strSearch, int intPage, int PageSize, string strIPAddress, int intDesignationType, string strMacAddress, string strOSDetails, string strIMEINo, string strGcmID, string strAPPVersion, int intSource, int intCreatedByType)
        {
            {
                string strQuery = "";
                string strLimit = "";
                DataSet ds = new DataSet();

                try
                {
                    string strCount = objODBC.executeScalar_str("select count(1) From er_login a inner join er_wallet b On a.userid = b.userid Where a.Active=1 and a.parent_id=1 " + strSearch + "");
                    //" + strCount + "
                    strLimit = " LIMIT " + PageSize * (intPage - 1) + "," + PageSize + "";
                    strQuery = "SELECT  a.userid,a.username,a.emailid,a.full_name,b.ex_wallet,b.wallet1,b.wallet2,b.wallet3,b.wallet4,b.wallet5," + strCount + " as intCount From er_login a inner join er_wallet b On a.userid = b.userid Where a.Active=1 and a.parent_id=1 " + strSearch + " ORDER BY a.userid DESC " + strLimit;
                    ds = objODBC.Report(strQuery);
                    return ds;
                }
                catch (Exception ex)
                {
                    return ds;
                }
                finally
                {
                    objCreateFile.CreateTextFile(strIPAddress, strMacAddress, strOSDetails, strIMEINo, strGcmID, strAPPVersion, intSource.ToString(), intCreatedByType, 60, " All Operator Wallet Details", strUserName);
                }
            }
        }


        [WebMethod]
        public string er_update_OperatorWallet(string strUserName, int intLoginID, int intUserID, int intStatus, double dblSalaamAmount, double dblEtisalatAmount, double dblRoshanAmount, double dblMTNAmount, double dblAWCCAmount, string strIPAddress, int intDesignationType, string strMacAddress, string strOSDetails, string strIMEINo, string strGcmID, string strAPPVersion, int intSource, int intCreatedByType)
        {
            DataTable dt = new DataTable();
            string strResult = string.Empty;
            try
            {
                objODBCLOG.executeNonQuery("call activity_logs('" + intLoginID + "','" + intDesignationType + "','" + intCreatedByType + "','" + strIPAddress + "','" + strMacAddress + "','" + strOSDetails + "','" + strIMEINo + "','" + strGcmID + "','" + strAPPVersion + "','" + intSource + "','Update Top up rights',601,'','" + strUserName + "')");

                objCreateFile.CreateTextFile(strIPAddress, strMacAddress, strOSDetails, strIMEINo, strGcmID, strAPPVersion, intSource.ToString(), intCreatedByType, 117, "er_update_Topup_rights", strUserName);
            }
            catch (Exception)
            {


            }

            try
            {
                if (intLoginID > 0 && intUserID > 0 && dblSalaamAmount >= 0 && dblEtisalatAmount >= 0 && dblRoshanAmount >= 0 && dblMTNAmount >= 0 && dblAWCCAmount >= 0 && (intStatus == 1 || intStatus == 2))
                {
                    int intFStatus = objODBC.executeScalar_int("SELECT `status` FROM `er_status_manager` WHERE id=1");
                    if (intFStatus == 1)
                    {
                        if (intStatus == 1) // Add
                        {


                            objODBC.executeNonQuery("call update_Operator_Wallet('" + intLoginID + "', '" + intUserID + "',1,'" + dblSalaamAmount + "',1,'Wallet Added by Admin','" + objComFun.GenrateRandomString().ToString() + "')");
                            objODBC.executeNonQuery("call update_Operator_Wallet('" + intLoginID + "', '" + intUserID + "',2,'" + dblEtisalatAmount + "',1,'Wallet Added by Admin','" + objComFun.GenrateRandomString().ToString() + "')");
                            objODBC.executeNonQuery("call update_Operator_Wallet('" + intLoginID + "', '" + intUserID + "',3,'" + dblRoshanAmount + "',1,'Wallet Added by Admin','" + objComFun.GenrateRandomString().ToString() + "')");
                            objODBC.executeNonQuery("call update_Operator_Wallet('" + intLoginID + "', '" + intUserID + "',4,'" + dblMTNAmount + "',1,'Wallet Added by Admin','" + objComFun.GenrateRandomString().ToString() + "')");
                            objODBC.executeNonQuery("call update_Operator_Wallet('" + intLoginID + "', '" + intUserID + "',5,'" + dblAWCCAmount + "',1,'Wallet Added by Admin','" + objComFun.GenrateRandomString().ToString() + "')");

                            strResult = "Operator Wallet Update Successfully!";
                        }
                        else if (intStatus == 2) // Deduct
                        {
                            objODBC.executeNonQuery("call update_Operator_Wallet('" + intLoginID + "', '" + intUserID + "',1,'" + dblSalaamAmount + "',2,'Wallet Deduct by Admin','" + objComFun.GenrateRandomString().ToString() + "')");
                            objODBC.executeNonQuery("call update_Operator_Wallet('" + intLoginID + "', '" + intUserID + "',2,'" + dblEtisalatAmount + "',2,'Wallet Deduct by Admin','" + objComFun.GenrateRandomString().ToString() + "')");
                            objODBC.executeNonQuery("call update_Operator_Wallet('" + intLoginID + "', '" + intUserID + "',3,'" + dblRoshanAmount + "',2,'Wallet Deduct by Admin','" + objComFun.GenrateRandomString().ToString() + "')");
                            objODBC.executeNonQuery("call update_Operator_Wallet('" + intLoginID + "', '" + intUserID + "',4,'" + dblMTNAmount + "',2,'Wallet Deduct by Admin','" + objComFun.GenrateRandomString().ToString() + "')");
                            objODBC.executeNonQuery("call update_Operator_Wallet('" + intLoginID + "', '" + intUserID + "',5,'" + dblAWCCAmount + "',2,'Wallet Deduct by Admin','" + objComFun.GenrateRandomString().ToString() + "')");

                            strResult = "Operator Wallet Update Successfully!";
                        }

                    }
                    else
                    {
                        strResult = "Permission denied by system !";
                    }

                }
                else
                {
                    strResult = "Invalid Details !";
                }

            }
            catch (Exception)
            {
                strResult = "Somthing went wrong";

            }
            finally
            {
                dt.Dispose();
            }
            return strResult;
        }
    }
}

