﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Etopup_Ws.old_App_Code;
using System.Web.Services;
using System.Collections;
using System.Data;


namespace Etopup_Ws
{
    /// <summary>
    /// Summary description for Admin
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class Admin : System.Web.Services.WebService
    {
        ODBC objODBC = new ODBC();
        ODBC_LOG objODBCLOG = new ODBC_LOG();
        DataSet ds;
        DataTable dt;
        DateFormat objDT = new DateFormat();
        Encrypt_Decrypt ED = new Encrypt_Decrypt();
        CreateFile objCreateFile = new CreateFile();
        CommonFunction objComFUN = new CommonFunction();
        Encryption objEncrypt = new Encryption();


        [WebMethod]
        public string er_update_recharge_status(string strUserName, int intStatus, string intID, string strIPAddress, int intDesignationType, string strMacAddress, string strOSDetails, string strIMEINo, string strGcmID, string strAPPVersion, int intSource, int intCreatedByType)
        {
            int intRechID = 0;
            int intActivityType = 101;
            string strResult = string.Empty;
            ds = new DataSet();
            try
            {
                objCreateFile.CreateTextFile(strIPAddress, strMacAddress, strOSDetails, strIMEINo, strGcmID, strAPPVersion, intSource.ToString(), intCreatedByType, intActivityType, "er_update_recharge_status", strUserName);

                int strUserid = objODBC.executeScalar_int("select userid from er_login_admin where username='" + strUserName + "'");

                objODBCLOG.executeNonQuery("call activity_logs('" + strUserid + "','" + intDesignationType + "','1','" + strIPAddress + "','" + strMacAddress + "','" + strOSDetails + "','" + strIMEINo + "','" + strGcmID + "','" + strAPPVersion + "','" + intSource + "','Modify Dispute Recharge Status','" + intActivityType + "','','')");
            }
            catch (Exception)
            {


            }
            finally
            {
                try
                {
                    if (intStatus == 1) //pending
                    {
                        intRechID = objODBC.executeScalar_int("select recharge_id from ticket_details where ticket_id ='" + intID + "'");

                        int intCount = objODBC.executeScalar_int("select count(1) from er_recharge where id =" + intRechID + "");

                        if (intCount > 0 && intStatus == 1 && intRechID > 0)
                        {
                            objODBC.executeNonQuery("update er_recharge set status=1 where id =" + intRechID + "");

                            strResult = "status updated successfully";
                        }
                        else
                        {
                            strResult = "Already status updated";

                        }
                    }
                    else if (intStatus == 2 || intStatus == 3) //2-approve and 3-reject
                    {

                        intRechID = objODBC.executeScalar_int("select recharge_id from ticket_details where ticket_id ='" + intID + "'");

                        int intCount = objODBC.executeScalar_int("select count(1) from er_recharge where id =" + intRechID + "");

                        if (intCount > 0 && intStatus > 1 && intStatus < 4)
                        {
                            if (intStatus == 2)
                            {
                                string intTopUpRecTransactionID = objODBC.executeScalar_str("select operator_transid from er_recharge where id='" + intID + "'");

                                objODBC.executeNonQuery("call `updateResponseV3`('" + intTopUpRecTransactionID + "','" + intID + "',3)");

                                objODBC.executeNonQuery("update ico_tckt_support set status=1 where ticket_no='" + intID + "'");
                                objODBC.executeNonQuery("update ticket_details set status=1 where ticket_id='" + intID + "'");

                            }

                            objODBC.executeNonQuery("update er_recharge set status=" + intStatus + ",dispute_status=1 where id =" + intID
                                + "");

                            strResult = "status updated successfully";
                        }
                        else
                        {
                            strResult = "Already status updated";
                        }
                    }

                }
                catch (Exception)
                {

                }
            }

            return strResult;

        }



        [WebMethod]
        public DataSet GetModuleDetails(string strUserName, int intUserID, int intDepartID, string strIPAddress, int intDesignationType, string strMacAddress, string strOSDetails, string strIMEINo, string strGcmID, string strAPPVersion, int intSource, int intCreatedByType)
        {
            int intActivityType = 101;
            ds = new DataSet();
            try
            {
                objCreateFile.CreateTextFile(strIPAddress, strMacAddress, strOSDetails, strIMEINo, strGcmID, strAPPVersion, intSource.ToString(), intCreatedByType, intActivityType, "GetModuleDetails", strUserName);
            }
            catch (Exception)
            {


            }
            finally
            {
                try
                {
                    ds = objODBC.getDataSet("select id as ModuleID,ModuleName,ViewStatus,AddStatus,EditStatus,DepartID,Active,case when ViewStatus=1 then 'true' else 'false' end as VStatus,case when AddStatus=1 then 'true' else 'false' end as AStatus,case when EditStatus=1 then 'true' else 'false' end as EStatus from er_module where DepartID =" + intDepartID + "");
                }
                catch (Exception)
                {

                }

            }

            return ds;

        }

        [WebMethod]
        public DataSet GetDepartmentDetails(string strUserName, int intID, int intAction, string strDeptname, string strIPAddress, int intDesignationType, string strMacAddress, string strOSDetails, string strIMEINo, string strGcmID, string strAPPVersion, int intSource, int intCreatedByType)
        {
            int intActivityType = 102;
            ds = new DataSet();
            try
            {
                objCreateFile.CreateTextFile(strIPAddress, strMacAddress, strOSDetails, strIMEINo, strGcmID, strAPPVersion, intSource.ToString(), intCreatedByType, intActivityType, "GetDepartmentDetails", strUserName);
            }
            catch (Exception)
            {


            }
            finally
            {
                try
                {
                    if (intAction == 1)
                    {
                        //ds = objODBC.getDataSet("update  er_department set Active=" + intAction + " where DepartID ='" + strDeptname + "'");
                    }
                    else if (intAction == 2) // insert
                    {
                        int intChk = objODBC.executeScalar_int("select count(1) from er_department where DepartName='" + strDeptname + "'");
                        if (intChk == 0)
                        {
                            objODBC.executeNonQuery("insert into  er_department(DepartName,Status,Active) values('" + strDeptname + "',1,1)");

                            int intDeptID = objODBC.executeScalar_int("select DepartID from  er_department where DepartName='" + strDeptname + "'");
                            ds = objComFUN.getDataSet_from_string(true, "Department created successfully");
                            strDeptname = "";
                            dt = objODBC.getDataTable("SELECT ModuleName,ViewStatus,AddStatus,EditStatus FROM  er_module where DepartID=1");
                            int intvistae = 0, intaddst = 0, intedist = 0;
                            for (int i = 0; i < dt.Rows.Count; i++)
                            {
                                intvistae = 0; intaddst = 0; intedist = 0;
                                //strDeptname = strDeptname + dt.Rows[i][0].ToString()+",";
                                //strDeptname = "('" + strDeptname + "')";
                                if (dt.Rows[i][1].ToString() == "2")
                                {
                                    intvistae = 2;
                                }
                                if (dt.Rows[i][2].ToString() == "2")
                                {
                                    intaddst = 2;
                                }
                                if (dt.Rows[i][3].ToString() == "2")
                                {
                                    intedist = 2;

                                }
                                objODBC.executeNonQuery("insert into  er_module(ModuleName,ViewStatus,AddStatus,EditStatus,DepartID) values('" + dt.Rows[i][0].ToString() + "'," + intvistae + "," + intaddst + "," + intedist + "," + intDeptID + ")");
                            }
                            //strDeptname= strDeptname.Remove(strDeptname.Length - 1, 1);

                        }
                        else
                        {
                            ds = objComFUN.getDataSet_from_string(false, "Department already exist");
                        }

                    }
                    else if (intAction == 3) //select
                    {
                        ds = objODBC.getDataSet("select DepartID,DepartName FROM  er_department where DepartID !=1 order by DepartName");
                    }
                    else if (intAction == 4) //update
                    {
                        objODBC.executeNonQuery("update  er_department set DepartName='" + strDeptname + "' where DepartID ='" + intID + "'");

                        ds = objComFUN.getDataSet_from_string(true, "Department updated successfully");
                    }
                }
                catch (Exception)
                {

                }
            }

            return ds;
        }



        [WebMethod]
        public ArrayList updaterolemodule(string strUserName, int intUserID, int intDepartID, int intModuleID, int intViewStatus, int intEditStatus, int intAddStatus, string strIPAddress, int intDesignationType, string strMacAddress, string strOSDetails, string strIMEINo, string strGcmID, string strAPPVersion, int intSource, int intCreatedByType)
        {
            int intActivityType = 103;
            ArrayList ar = new ArrayList();
            try
            {
                objODBCLOG.executeNonQuery("call activity_logs('" + intUserID + "','" + intDesignationType + "','" + intCreatedByType + "','" + strIPAddress + "','" + strMacAddress + "','" + strOSDetails + "','" + strIMEINo + "','" + strGcmID + "','" + strAPPVersion + "','" + intSource + "','Modify Role Model','" + intActivityType + "','','')");

                objCreateFile.CreateTextFile(strIPAddress, strMacAddress, strOSDetails, strIMEINo, strGcmID, strAPPVersion, intSource.ToString(), intCreatedByType, intActivityType, "updaterolemodule", strUserName);

            }
            catch (Exception)
            {
                ar.Add("FALSE");
                ar.Add("Somthing went wrong");

            }
            finally
            {
                try
                {
                    objODBC.executeNonQuery("update er_module set ViewStatus=" + intViewStatus + ",AddStatus=" + intAddStatus + ",EditStatus=" + intEditStatus + " where id=" + intModuleID + " and DepartID=" + intDepartID + "");
                    ar.Add("TRUE");
                    ar.Add("Role Updated successfully");

                }
                catch (Exception)
                {
                    ar.Add("FALSE");
                    ar.Add("Somthing went wrong");

                }
            }

            return ar;
        }


        // ActivityType = 104
        [WebMethod]
        public DataSet SubAdminManager(string strUserName, int intUserID, string strSearch, int intPage, int PageSize, string strIPAddress, int intDesignationType, string strMacAddress, string strOSDetails, string strIMEINo, string strGcmID, string strAPPVersion, int intSource, int intCreatedByType)
        {
            string strQuery = "";
            string strLimit = "";
            DataSet ds = new DataSet();
            try
            {

                objCreateFile.CreateTextFile(strIPAddress, strMacAddress, strOSDetails, strIMEINo, strGcmID, strAPPVersion, intSource.ToString(), intCreatedByType, 104, "Sub Admin Manager", strUserName);
            }
            catch (Exception)
            {


            }
            finally
            {
                try
                {
                    strLimit = " LIMIT " + PageSize * (intPage - 1) + "," + PageSize + "";

                    strQuery = "SELECT  a.userid,a.username,a.emailid,a.mobile,a.full_name,a.username,a.created_on,a.last_login_ip,a.last_login_datetime,a.status,b.ex_wallet,a.password,a.usertype_id,b.min_wallet,a.province_Name,a.city_name,a.Address,a.gender, case when a.gender=1 then 'Male' when a.gender=2 then 'Female' when a.gender=0 then 'Other' end as gender_text, c.DepartName as Utype,(select count(1) From er_login_admin a left join er_wallet b On a.userid = b.userid Where  a.userid!=1 " + strSearch + ") as intCount From er_login_admin a left join er_wallet b On a.userid = b.userid inner join er_department c On a.usertype_id=c.DepartID Where  a.userid!=1 " + strSearch + " ORDER BY a.userid DESC " + strLimit;



                    ds = objODBC.Report(strQuery);

                }
                catch (Exception ex)
                {

                }
            }
            return ds;
        }


        // **** Update Password For Admin*****
        // (string oldPwd - 0,string newPwd - 1,int intUserID - 2,int intLogID - 3,string strIPAddress - 4,int intDesignationType - 5,string strMacAddress - 6,string strOSDetails - 7,string strIMEINo - 8,string strGcmID - 9,string strAPPVersion - 10,int intSource - 11)
        [WebMethod]
        public string UpdateSubAdminPassword(string strUserName, string strData, int intType, int intSource)
        {
            string strDescription = "Change SubAdmin Password", strResponseFile = "";
            int intActivityType = 105;
            try
            {
                var data = ED.DecryptAdmin(strUserName, strData, 1, intType, intSource).Split(',');

                if ((data[0] != "" || data[0] != null) && (data[1] != "" || data[1] != null) && (data[2] != "" || data[2] != null))
                {
                    try
                    {
                        objCreateFile.CreateTextFile(data[4], data[6], data[7], data[8], data[9], data[10], data[11], 1, intActivityType, "SubAdmin,Data: " + strData, strUserName);
                    }
                    catch (Exception)
                    {

                    }
                    Encryption objEncrypt = new Encryption();
                    DateFormat objDateFormat = new DateFormat();


                    string passkey = objODBC.executeScalar_str("SELECT pass_key From er_login_admin Where userid='" + data[2] + "' and status=1 and usertype_id=1");
                    string strOLDPassward = objEncrypt.EncryptQueryString(data[0], passkey);

                    int intCount = objODBC.executeScalar_int("SELECT Count(1) From er_login_admin Where  userid=" + data[2] + " and password= '" + strOLDPassward + "' ");

                    if (intCount > 0)
                    {
                        string strPassward = objEncrypt.EncryptQueryString(data[1], passkey);
                        string strQuery = "UPDATE er_login_admin SET password='" + strPassward + "' Where userid=" + data[2] + "";
                        objODBC.executeNonQuery(strQuery);
                        objODBC.executeNonQuery("Insert Into er_password_history (userid,old_password,new_password,modified_by,modified_on,user_type,created_by_type,log_id) values (" + data[2] + ",'" + strOLDPassward + "','" + strPassward + "'," + data[2] + ",'" + objDateFormat.getCurDateTimeString() + "','" + data[5] + "',1,'" + data[3] + "')");
                        try
                        {
                            objODBCLOG.executeNonQuery("call activity_logs('" + data[2] + "','" + data[5] + "',1,'" + data[4] + "','" + data[6] + "','" + data[7] + "','" + data[8] + "','" + data[9] + "','" + data[10] + "','" + data[11] + "','" + strDescription + "','" + intActivityType + "','" + strOLDPassward + "','" + strPassward + "')");
                            strResponseFile = "Password successfully changed.";
                            //  string oldPwd - 0,string newPwd -1,int intUserID -2,int intLogID -3,string strIPAddress -4,int intDesignationType -5,string strMacAddress -6,string strOSDetails -7,string strIMEINo -8,string strGcmID -9,string strAPPVersion -10,int intSource -11)
                            objCreateFile.CreateTextFile(data[4], data[6], data[7], data[8], data[9], data[10], data[11], 1, intActivityType, "SubAdmin " + strResponseFile, strUserName);
                        }
                        catch (Exception)
                        {

                        }

                        // SMPP SMS Code Below
                        SMPP_SMS_Format objSMPP_SMS_Format = new SMPP_SMS_Format();
                        string strGetSMS = objSMPP_SMS_Format.ChangePassword(strUserName, objODBC.executeScalar_str("SELECT full_name From er_login_admin Where userid='" + data[2] + "'").ToString(), intSource, data[1]).ToString();
                        objComFUN.er_insert_notification(int.Parse(data[2]), "Change Password", strGetSMS, 1);
                        SMPP1 objSMPP1 = new SMPP1();
                        string strSMPPResponse = "NA";
                        DataTable dtSMPP = new DataTable();
                        try
                        {


                            dtSMPP = objODBC.getDataTable("Select mobile,operator_id from er_mobile where userid='" + data[2] + "' and usertype=1 and mob_type=1");

                            if (dtSMPP.Rows.Count > 0)
                            {
                                string strMobile = dtSMPP.Rows[0][0].ToString();
                                int intOperaterID = int.Parse(dtSMPP.Rows[0][1].ToString());


                                objComFUN.er_SMS_Alert(int.Parse(data[2]), 1, strMobile, intOperaterID, strGetSMS, 1, strSMPPResponse);

                            }
                        }
                        catch (Exception)
                        {

                        }
                        finally
                        {
                            dtSMPP.Dispose();
                        }

                        return strResponseFile;
                    }
                    else
                    {
                        strResponseFile = "Old Password in not matching.";
                        objCreateFile.CreateTextFile(data[4], data[6], data[7], data[8], data[9], data[10], data[11], 1, intActivityType, "SubAdmin " + strResponseFile, strUserName);
                        return strResponseFile;
                    }

                }
                else
                {
                    strResponseFile = "Kindly Enter valid Userid and Password.";
                    objCreateFile.CreateTextFile(data[4], data[6], data[7], data[8], data[9], data[10], data[11], 1, intActivityType, strResponseFile, strUserName);
                    return strResponseFile;
                }
            }
            catch (Exception ex)
            {
                return "OOPS! Something is wrong.";
            }
        }


        //(string strUserName=0,int intUserID=1, int intSlabid=2, int intOperatorid=3,double dblcommision=4,string strIPAddress=5, int intDesignationType=6, string strMacAddress=7, string strOSDetails=8, string strIMEINo=9, string strGcmID=10, string strAPPVersion=11, int intSource=12, int intCreatedByType=13)
        [WebMethod]
        public ArrayList er_update_commission(string strUserName, string strData, int intType, int intSource, int intUtype)
        {
            ArrayList arl = new ArrayList();
            string strQuery = "";
            int intActivityType = 106;

            var data = ED.DecryptAdmin(strUserName, strData, intUtype, intType, intSource).Split(',');


            //(string strUserName=0,int intUserID=1, int intSlabid=2, int intOperatorid=3,double dblcommision=4,string strIPAddress=5, int intDesignationType=6, string strMacAddress=7, string strOSDetails=8, string strIMEINo=9, string strGcmID=10, string strAPPVersion=11, int intSource=12, int intCreatedByType=13)

            string strUserNameData = data[0];
            int intUserID = int.Parse(data[1]);
            int intSLabID = int.Parse(data[2]);
            int intOperID = int.Parse(data[3]);
            double dblcommision = double.Parse(data[4]);
            string strIPAddress = data[5];
            int intDesignationType = int.Parse(data[6]);
            string strMacAddress = data[7];
            string strOSDetails = data[8];
            string strIMEINo = data[9];
            string strGcmID = data[10];
            string strAPPVersion = data[11];
            int intSourceData = int.Parse(data[12]);
            int intCreatedByType = int.Parse(data[13]);
            try
            {

                objCreateFile.CreateTextFile(strIPAddress, strMacAddress, strOSDetails, strIMEINo, strGcmID, strAPPVersion, intSourceData.ToString(), intCreatedByType, intActivityType, "update Commission,Data:" + strData, strUserName);


            }
            catch (Exception)
            {

            }
            finally
            {
                try
                {
                    strQuery = "update er_commission_structure set commission_per=" + dblcommision + " where slab_id=" + intSLabID + " and operator_id=" + intOperID + "";
                    objODBC.executeNonQuery(strQuery);

                    objODBCLOG.executeNonQuery("call activity_logs('" + intUserID + "','" + intDesignationType + "','" + intCreatedByType + "','" + strIPAddress + "','" + strMacAddress + "','" + strOSDetails + "','" + strIMEINo + "','" + strGcmID + "','" + strAPPVersion + "','" + intSource + "','Update commission structure','" + intActivityType + "','','')");

                    arl.Add("TRUE");
                    arl.Add("Update commission successfully");
                }
                catch (Exception)
                {
                    arl.Add("FALSE");
                    arl.Add("Somthing went wrong");

                }
            }
            return arl;
        }

        // aLL NOTIFICATION
        [WebMethod]
        public ArrayList er_get_notification(string strUserName, int intUserID, int PageSize, int intPage, string strSearch, string strIPAddress, int intDesignationType, string strMacAddress, string strOSDetails, string strIMEINo, string strGcmID, string strAPPVersion, int intSource, int intCreatedByType)
        {
            string strLimit = string.Empty;
            ArrayList al = new ArrayList();
            try
            {

                objCreateFile.CreateTextFile(strIPAddress, strMacAddress, strOSDetails, strIMEINo, strGcmID, strAPPVersion, intSource.ToString(), intCreatedByType, 116, "Get Notification", strUserName);
            }
            catch (Exception)
            {


            }
            finally
            {
                try
                {
                    int intNotiCOunt = objODBC.executeScalar_int("select count(1) from er_notification where userid =" + intUserID + " and read_status=0 and user_type='" + intCreatedByType + "'");

                    //  PageSize = intNotiCOunt < 5 ? 5 : intNotiCOunt;
                    strLimit = " LIMIT " + PageSize * (intPage - 1) + "," + PageSize + "";
                    ds = objODBC.getDataSet("select id as ID,title as Subject,description as Message,DATE_FORMAT(created_on,'%d %b %Y %H:%i:%s') as Notifi_Date,read_status as Status  from er_notification where userid =" + intUserID + " and user_type='" + intCreatedByType + "' order by created_on desc " + strLimit + "");
                    al = objComFUN.Get_ArrayLlist_From_Dataset(ds);
                    al.Add("Notification count:" + intNotiCOunt);
                    al.Add(intNotiCOunt);
                }
                catch (Exception)
                {


                }
            }

            return al;
        }
        // Unread Notification
        [WebMethod]
        public DataSet er_get_notification_details(string strUserName, int intUserID, int PageSize, int intPage, string strSearch, string strIPAddress, int intDesignationType, string strMacAddress, string strOSDetails, string strIMEINo, string strGcmID, string strAPPVersion, int intSource, int intCreatedByType)
        {
            string strLimit = string.Empty;
            DataSet ds = new DataSet();
            try
            {


                objCreateFile.CreateTextFile(strIPAddress, strMacAddress, strOSDetails, strIMEINo, strGcmID, strAPPVersion, intSource.ToString(), intCreatedByType, 116, "Get Notification", strUserName);
            }
            catch (Exception)
            {


            }
            finally
            {
                try
                {
                    int intNotiCOunt = objODBC.executeScalar_int("select count(1) from er_notification where userid =" + intUserID + " and user_type='" + intCreatedByType + "' " + strSearch + "");

                    strLimit = " LIMIT " + PageSize * (intPage - 1) + "," + PageSize + "";

                    // PageSize = intNotiCOunt < 5 ? 5 : intNotiCOunt;

                    ds = objODBC.getDataSet("select id as ID,title as Subject,description as Message,DATE_FORMAT(created_on,'%d %b %Y %H:%i:%s') as Notifi_Date,read_status as Status,'" + intNotiCOunt + "' as intcount from er_notification where userid =" + intUserID + " and user_type='" + intCreatedByType + "' " + strSearch + " order by created_on desc" + strLimit + "");

                }
                catch (Exception)
                {


                }
            }

            return ds;
        }

        [WebMethod]
        public ArrayList er_update_notification(string strUserName, int intUserID, string strIPAddress, int intDesignationType, string strMacAddress, string strOSDetails, string strIMEINo, string strGcmID, string strAPPVersion, int intSource, int intCreatedByType)
        {
            ArrayList al = new ArrayList();
            try
            {


                objCreateFile.CreateTextFile(strIPAddress, strMacAddress, strOSDetails, strIMEINo, strGcmID, strAPPVersion, intSource.ToString(), intCreatedByType, 117, "Update Notification", strUserName);
            }
            catch (Exception)
            {


            }
            finally
            {
                try
                {
                    if (intUserID == 1)
                    {
                        objODBC.executeNonQuery("update er_notification set read_status=1 where userid=" + intUserID + "");
                    }
                    else
                    {

                        objODBC.executeNonQuery("update er_notification set read_status=1 where userid=" + intUserID + " and user_type='" + intCreatedByType + "'");
                    }

                    al.Add("TRUE");
                    al.Add("Update successfully");
                }
                catch (Exception)
                {
                    al.Add("FALSE");
                    al.Add("Somthing went wrong");

                }
            }

            return al;
        }

        [WebMethod]
        public string er_update_threeshold(string strUserName, int intUserID, double dblAmt, string strIPAddress, int intDesignationType, string strMacAddress, string strOSDetails, string strIMEINo, string strGcmID, string strAPPVersion, int intSource, int intCreatedByType)
        {
            string strResult = string.Empty;
            try
            {
                objODBCLOG.executeNonQuery("call activity_logs('" + intUserID + "','" + intDesignationType + "','" + intCreatedByType + "','" + strIPAddress + "','" + strMacAddress + "','" + strOSDetails + "','" + strIMEINo + "','" + strGcmID + "','" + strAPPVersion + "','" + intSource + "','Update Threeshold',117,'','')");

                objCreateFile.CreateTextFile(strIPAddress, strMacAddress, strOSDetails, strIMEINo, strGcmID, strAPPVersion, intSource.ToString(), intCreatedByType, 117, "er_update_threeshold", strUserName);
            }
            catch (Exception)
            {


            }
            finally
            {
                try
                {
                    int intcoumnt = objODBC.executeScalar_int("select count(1) from er_login_admin where username ='" + strUserName + "'");

                    if (intcoumnt > 0 && dblAmt > 0)
                    {
                        objODBC.executeNonQuery("update er_wallet set min_wallet=" + dblAmt + " where userid =" + intUserID + "");

                        strResult = "Threshold updated successfully";
                    }
                    else
                    {
                        strResult = "invalid username or amount";
                    }

                }
                catch (Exception)
                {
                    strResult = "somthing went wrong";

                }
            }

            return strResult;
        }

        [WebMethod]
        public string er_update_rights(string strUserName, int intLoginID, int intUserID, int intStatus, int intUpdateFor, string strIPAddress, int intDesignationType, string strMacAddress, string strOSDetails, string strIMEINo, string strGcmID, string strAPPVersion, int intSource, int intCreatedByType)
        {
            string strResult = string.Empty;
            try
            {
                objODBCLOG.executeNonQuery("call activity_logs('" + intLoginID + "','" + intDesignationType + "','" + intCreatedByType + "','" + strIPAddress + "','" + strMacAddress + "','" + strOSDetails + "','" + strIMEINo + "','" + strGcmID + "','" + strAPPVersion + "','" + intSource + "','Update rights',117,'','')");

                objCreateFile.CreateTextFile(strIPAddress, strMacAddress, strOSDetails, strIMEINo, strGcmID, strAPPVersion, intSource.ToString(), intCreatedByType, 117, "er_update_rights", strUserName);
            }
            catch (Exception)
            {


            }
            finally
            {
                try
                {
                    if (intLoginID > 0 && intUserID > 0 && intUpdateFor > 0)
                    {
                        if (intUpdateFor == 1) // User Add Right
                        {
                            objODBC.executeNonQuery("update er_login set add_user_rights=" + intStatus + " where userid =" + intUserID + "");

                        }
                        else if (intUpdateFor == 2) // User Add Commission Right
                        {

                            objODBC.executeNonQuery("update er_login set commission_rights=" + intStatus + " where userid =" + intUserID + "");
                        }
                        else if (intUpdateFor == 3) // User Add Bulk Transfer 
                        {
                            objODBC.executeNonQuery("update er_login set bulk_transfer=" + intStatus + " where userid =" + intUserID + "");
                        }
                        else if (intUpdateFor == 4) // Group Topup
                        {
                            objODBC.executeNonQuery("update er_login set group_topup=" + intStatus + " where userid =" + intUserID + "");
                        }
                        else if (intUpdateFor == 5) // Fource Logout
                        {
                            objODBC.executeNonQuery("update er_login set fource_logout=" + intStatus + " where userid =" + intUserID + "");
                        }

                        strResult = "Rights updated successfully";
                    }
                    else
                    {
                        strResult = "invalid username or amount";
                    }

                }
                catch (Exception)
                {
                    strResult = "somthing went wrong";

                }
            }

            return strResult;
        }




        // int ActivityType=76
        [WebMethod]
        public DataSet er_Security_topup(string strUserName, int intUserID, string strIPAddress, int intDesignationType, string strMacAddress, string strOSDetails, string strIMEINo, string strGcmID, string strAPPVersion, int intSource, int intCreatedByType)
        {
            try
            {
                try
                {
                    objCreateFile.CreateTextFile(strIPAddress, strMacAddress, strOSDetails, strIMEINo, strGcmID, strAPPVersion, intSource.ToString(), intCreatedByType, 76, "Security Topup Report", strUserName);
                }
                catch (Exception)
                {

                }
                finally
                {
                    if (intUserID == 1)
                    {

                        string strQuery = "SELECT `id`, `operator_id`, `status`, `Operator_name` FROM `er_status_manager_operator` order by id ASC ";
                        ds = objODBC.Report(strQuery);

                    }
                }
                return ds;
            }
            catch (Exception)
            {

                throw;
            }
            finally
            {
                ds.Dispose();
            }
        }


        // int ActivityType=76
        [WebMethod]
        public DataSet er_status_manager(string strUserName, int intUserID, string strIPAddress, int intDesignationType, string strMacAddress, string strOSDetails, string strIMEINo, string strGcmID, string strAPPVersion, int intSource, int intCreatedByType)
        {
            try
            {
                try
                {
                    objCreateFile.CreateTextFile(strIPAddress, strMacAddress, strOSDetails, strIMEINo, strGcmID, strAPPVersion, intSource.ToString(), intCreatedByType, 76, "Security Status Manager", strUserName);
                }
                catch (Exception)
                {

                }
                finally
                {
                    if (intUserID == 1)
                    {

                        string strQuery = "SELECT `id`, `status_name`, `status` FROM `er_status_manager` order by id ASC ";
                        ds = objODBC.Report(strQuery);
                    }
                }
                return ds;
            }
            catch (Exception)
            {

                throw;
            }
            finally
            {
                ds.Dispose();
            }
        }


        [WebMethod]
        public string er_update_security_TopupRights(string strUserName, int intUserID, int intStatus, int intOperatorID, string strIPAddress, int intDesignationType, string strMacAddress, string strOSDetails, string strIMEINo, string strGcmID, string strAPPVersion, int intSource, int intCreatedByType)
        {
            string strResult = string.Empty;
            try
            {
                objODBCLOG.executeNonQuery("call activity_logs('" + intUserID + "','" + intDesignationType + "','" + intCreatedByType + "','" + strIPAddress + "','" + strMacAddress + "','" + strOSDetails + "','" + strIMEINo + "','" + strGcmID + "','" + strAPPVersion + "','" + intSource + "','Update Security Topup Rights',117,'','')");

                objCreateFile.CreateTextFile(strIPAddress, strMacAddress, strOSDetails, strIMEINo, strGcmID, strAPPVersion, intSource.ToString(), intCreatedByType, 117, "er_update_security_TopupRights", strUserName);
            }
            catch (Exception)
            {


            }
            finally
            {
                try
                {

                    if (intUserID == 1) // User Add Right
                    {
                        objODBC.executeNonQuery("update er_status_manager_operator set status=" + intStatus + " where operator_id=" + intOperatorID + "");
                        strResult = "Rights updated successfully";
                    }
                    else
                    {
                        strResult = "Invalid Details";
                    }
                }
                catch (Exception)
                {
                    strResult = "somthing went wrong";

                }
            }

            return strResult;
        }


        [WebMethod]
        public string er_update_security_manager(string strUserName, int intUserID, int intStatus,int intID, string strIPAddress, int intDesignationType, string strMacAddress, string strOSDetails, string strIMEINo, string strGcmID, string strAPPVersion, int intSource, int intCreatedByType)
        {
            string strResult = string.Empty;
            try
            {
                objODBCLOG.executeNonQuery("call activity_logs('" + intUserID + "','" + intDesignationType + "','" + intCreatedByType + "','" + strIPAddress + "','" + strMacAddress + "','" + strOSDetails + "','" + strIMEINo + "','" + strGcmID + "','" + strAPPVersion + "','" + intSource + "','Update Security Fund Transfer rights',117,'','')");

                objCreateFile.CreateTextFile(strIPAddress, strMacAddress, strOSDetails, strIMEINo, strGcmID, strAPPVersion, intSource.ToString(), intCreatedByType, 117, "er_update_security_manager", strUserName);
            }
            catch (Exception)
            {


            }
            finally
            {
                try
                {

                    if (intUserID == 1) // User Add Right
                    {
                        objODBC.executeNonQuery("update er_status_manager set status=" + intStatus + " where id=" + intID + "");
                        strResult = "Rights updated successfully";
                    }
                    else
                    {
                        strResult = "Invalid Details";
                    }
                }
                catch (Exception)
                {
                    strResult = "somthing went wrong";

                }
            }

            return strResult;
        }
    }
}
