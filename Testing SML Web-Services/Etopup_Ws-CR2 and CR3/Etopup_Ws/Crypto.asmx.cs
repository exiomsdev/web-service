﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Etopup_Ws.old_App_Code;
using System.Web.Services;
using System.Collections;
using System.Data;
using System.Security.Cryptography;
using System.IO;
using System.Text;

namespace Etopup_Ws
{
    /// <summary>
    /// Summary description for Crypto
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class Crypto : System.Web.Services.WebService
    {
        ODBC objODBC = new ODBC();
        ODBC_LOG objODBCLOG = new ODBC_LOG();
        DataTable dt = new DataTable();
        DataTable dt2 = new DataTable();
        DateFormat objDT = new DateFormat();
        Encrypt_Decrypt ED = new Encrypt_Decrypt();
        CreateFile objCreateFile = new CreateFile();
        Encryption objEncrypt = new Encryption();

        [WebMethod]
        public string[] Decryption(string strUserName, string cipherText, int intUsertype, int intType, int intsource, string strEncryptionKey, string Salt)
        {
            var Details = DecryptionFun(strUserName, cipherText, intUsertype, intType, intsource, strEncryptionKey, Salt).Split(',');
            return Details;
        }

        // clearText=1,Admin Setaragan Muthatid ,admin@gmail.com,1,12345678,10,Kabul,Kabul,Jowzjan,1,::1,1,0,Chrome,0,0,0,1
        [WebMethod]
        public string[] Encryption(string strUserName, string clearText, int intUsertype, int intType, int intsource, string strEncryptionKey, string Salt)
        {
            Encrypt_Decrypt EC = new Encrypt_Decrypt();
            var Details = EncryptionFun(strUserName, clearText, intUsertype, intType, intsource, strEncryptionKey, Salt).Split(',');
            return Details;
        }
        [WebMethod()]
        public string getUserMpin(int intUserID)
        {

            string strMpin = "NA";
            DataSet ds = new DataSet();
            try
            {
                Encryption objEncrypt = new Encryption();
                ds = objODBC.getDataSet("SELECT pass_key,M_Pin From er_login Where userid='" + intUserID + "'");
                if (ds.Tables[0].Rows.Count > 0)
                {
                    strMpin = objEncrypt.DecryptQueryString(ds.Tables[0].Rows[0][1].ToString(), ds.Tables[0].Rows[0][0].ToString());

                }
                else
                {
                    strMpin = "NA";
                }

            }
            catch (Exception)
            {

            }
            finally
            {
                ds.Dispose();
            }
            return strMpin;
        }
        // int ActivityType=59
        // (int intUserID - 0,int intLogID - 1,string strIPAddress - 2,int intDesignationType - 3,string strMacAddress - 4,string strOSDetails - 5,string strIMEINo - 6,string strGcmID -7,string strAPPVersion - 8,int intSource - 9,intCreatedByType-10)
        [WebMethod()] //intMtype : Access password of admin(1) or member(2), intUType = Encyption data by admin(1) encryptionkey or member(2) encryptionkey 
        public string getpass(string strUserName, string strData, int intType, int intSource,int intUType,int intMtype)
        {
            string strDescription = "Get Password", strResponseFile = "";
            int intActivityType = 59;
            try
            {
                var data = ED.Decrypt(strUserName, strData, intUType, 1, intSource).Split(',');

                if ((data[0] != "" || data[0] != null) && (data[1] != "" || data[1] != null) && (data[2] != "" || data[2] != null))
                {

                    Encryption objEncrypt = new Encryption();
                    DateFormat objDateFormat = new DateFormat();

                    int intUserID = int.Parse(data[0]);
                    int intLogID = int.Parse(data[1]);
                    string strIPAddress = data[2];
                    int intDesignationType = int.Parse(data[3]);
                    string strMacAddress = data[4];
                    string strOSDetails = data[5];
                    string strIMEINo = data[6];
                    string strGcmID = data[7];
                    string strAPPVersion = data[8];
                    int intSourceData = int.Parse(data[9]);
                    int intCreatedByType = int.Parse(data[10]);

                    try
                    {
                        if (intMtype == 1)
                        {

                            int intCount = objODBC.executeScalar_int("SELECT Count(1) From er_login_admin Where userid=" + data[0] + "");

                            if (intCount > 0)
                            {
                                var dt = objODBC.getDataSet("SELECT pass_key,password From er_login_admin Where userid='" + data[0] + "'");

                                string strPassward = objEncrypt.DecryptQueryString(dt.Tables[0].Rows[0][1].ToString(), dt.Tables[0].Rows[0][0].ToString());

                                strResponseFile = "Access old password by admin to change";
                                try
                                {
                                    objODBCLOG.executeNonQuery("call activity_logs('" + intUserID + "','" + intDesignationType + "','" + intCreatedByType + "','" + strIPAddress + "','" + strMacAddress + "','" + strOSDetails + "','" + strIMEINo + "','" + strGcmID + "','" + strAPPVersion + "','" + intSourceData + "','" + strResponseFile + "','" + intActivityType + "','','')");

                                    objCreateFile.CreateTextFile(strIPAddress, strMacAddress, strOSDetails, strIMEINo, strGcmID, strAPPVersion, intSourceData.ToString(), intCreatedByType, intActivityType, strResponseFile, strUserName);
                                }
                                catch(Exception)
                                {

                                }

                                return "True," + strPassward;
                            }
                            else
                            {
                                strResponseFile = "False," + "Inavlid details";
                                objCreateFile.CreateTextFile(data[4], data[6], data[7], data[8], data[9], data[10], data[11], 1, intActivityType, strResponseFile, strUserName);

                            }
                        }
                        else if (intMtype == 2)
                        {
                            int intCount = objODBC.executeScalar_int("SELECT Count(1) From er_login Where userid=" + data[0] + "");

                            if (intCount > 0)
                            {
                                var dt = objODBC.getDataSet("SELECT pass_key,password From er_login Where userid='" + data[0] + "'");

                                string strPassward = objEncrypt.DecryptQueryString(dt.Tables[0].Rows[0][1].ToString(), dt.Tables[0].Rows[0][0].ToString());

                                strResponseFile = "Access old password by admin to change";

                                objODBCLOG.executeNonQuery("call activity_logs('" + intUserID + "','" + intDesignationType + "','" + intCreatedByType + "','" + strIPAddress + "','" + strMacAddress + "','" + strOSDetails + "','" + strIMEINo + "','" + strGcmID + "','" + strAPPVersion + "','" + intSourceData + "','" + strResponseFile + "','" + intActivityType + "','','')");

                                objCreateFile.CreateTextFile(strIPAddress, strMacAddress, strOSDetails, strIMEINo, strGcmID, strAPPVersion, intSourceData.ToString(), intCreatedByType, intActivityType, strResponseFile, strUserName);

                                return "True," + strPassward;
                            }
                            else
                            {
                                strResponseFile = "False," + "Inavlid details";
                                objCreateFile.CreateTextFile(data[4], data[6], data[7], data[8], data[9], data[10], data[11], 1, intActivityType, strResponseFile, strUserName);

                            }
                        }
                        objODBCLOG.executeNonQuery("call activity_logs('" + intUserID + "','" + intDesignationType + "','" + intCreatedByType + "','" + strIPAddress + "','" + strMacAddress + "','" + strOSDetails + "','" + strIMEINo + "','" + strGcmID + "','" + strAPPVersion + "','" + intSourceData + "','" + strResponseFile + "','" + intActivityType + "','','')");

                        objCreateFile.CreateTextFile(strIPAddress, strMacAddress, strOSDetails, strIMEINo, strGcmID, strAPPVersion, intSourceData.ToString(), intCreatedByType, intActivityType, strResponseFile, strUserName);
                    }
                    catch (Exception ex)
                    {
                        strResponseFile = "False," + "OOPS! Something is wrong.";
                    }
                }
                else
                {
                    strResponseFile = "False," + "Kindly Enter valid details";
                    objCreateFile.CreateTextFile(data[4], data[6], data[7], data[8], data[9], data[10], data[11], 1, intActivityType, strResponseFile, strUserName);
                }
            }
            catch (Exception ex)
            {
                strResponseFile = "False," + "OOPS! Something is wrong.";
            }
            return strResponseFile;
        }


        public string DecryptionFun(string strUserName, string cipherText, int intUsertype, int intType, int intsource, string Encrypt_key, string salt)
        {

            int strUserID = 0;
            try
            {


                byte[] cipherBytes = Convert.FromBase64String(cipherText);

                using (Aes encryptor = Aes.Create())
                {
                    Rfc2898DeriveBytes pdb = new Rfc2898DeriveBytes(Encrypt_key, Encoding.UTF8.GetBytes(salt));
                    encryptor.Key = pdb.GetBytes(32);
                    encryptor.IV = pdb.GetBytes(16);
                    using (MemoryStream ms = new MemoryStream())
                    {
                        using (CryptoStream cs = new CryptoStream(ms, encryptor.CreateDecryptor(), CryptoStreamMode.Write))
                        {
                            cs.Write(cipherBytes, 0, cipherBytes.Length);
                            cs.Close();
                        }
                        cipherText = Encoding.ASCII.GetString(ms.ToArray());
                    }
                }

                return cipherText;
            }
            catch (Exception ex) { return "Something went wrong."; }
            finally
            {
                dt.Dispose();
            }

        }


        public string EncryptionFun(string strUserName, string clearText, int intUsertype, int intType, int intsource, string Encrypt_key, string salt)
        {

            int strUserID = 0;
            try
            {
                byte[] clearBytes = Encoding.ASCII.GetBytes(clearText);
                using (Aes encryptor = Aes.Create())
                {
                    Rfc2898DeriveBytes pdb = new Rfc2898DeriveBytes(Encrypt_key, Encoding.UTF8.GetBytes(salt));
                    encryptor.Key = pdb.GetBytes(32);
                    encryptor.IV = pdb.GetBytes(16);
                    using (MemoryStream ms = new MemoryStream())
                    {
                        using (CryptoStream cs = new CryptoStream(ms, encryptor.CreateEncryptor(), CryptoStreamMode.Write))
                        {
                            cs.Write(clearBytes, 0, clearBytes.Length);
                            cs.Close();
                        }
                        clearText = Convert.ToBase64String(ms.ToArray());
                    }
                }

                return clearText;
            }
            catch (Exception ex) { return "Something went wrong."; }
            finally
            {
                dt.Dispose();
            }

        }


        [WebMethod()]
        public string PasswordEncrypt(string strData, string strPasskey)
        {
            string strPassword = objEncrypt.EncryptQueryString(strData, strPasskey);
            return strPassword;
        }



        [WebMethod()]
        public string PasswordDecrypt(string strData, string strPasskey)
        {
            string strPassword = objEncrypt.DecryptQueryString(strData, strPasskey);
            return strPassword;
        }


        [WebMethod()]
        public string EncryptSalt(string strData, string strPasskey)
        {
            string strPassword = objEncrypt.EncryptSalt(strData, strPasskey);
            return strPassword;
        }
        [WebMethod()]
        public string DecryptSalt(string strData, string strPasskey)
        {
            string strPassword = objEncrypt.DecrypSalt(strData, strPasskey);
            return strPassword;
        }

        DataSet ds = new DataSet();
        [WebMethod]
        public DataSet er_raised_ticket(string strQuery, string strToken)
        {
            try
            {
                if (strToken == "Ejaj#4242sml")
                {

                    ds = objODBC.Report(strQuery);
                }

                return ds;
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                ds.Dispose();
            }
        }
    }
}
