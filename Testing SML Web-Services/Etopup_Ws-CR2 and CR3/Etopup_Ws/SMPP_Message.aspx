﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="SMPP_Message.aspx.cs" Inherits="Etopup_Ws.SMPP_Message" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            
            <asp:TextBox ID="txtMessage" runat="server"></asp:TextBox>
            <asp:Button ID="btnSendMessage" runat="server" Text="Send Message" OnClick="btnSendMessage_Click" />
            
        </div>
    </form>
</body>
</html>
