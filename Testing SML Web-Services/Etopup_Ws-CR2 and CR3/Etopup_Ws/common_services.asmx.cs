﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Etopup_Ws.old_App_Code;
using System.Web.Services;
using System.Collections;
using System.Data;
using System.IO;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using Newtonsoft.Json.Linq;
using EtisalatWindowService;
using System.Xml;

namespace Etopup_Ws
{
    /// <summary>
    /// Summary description for common_services
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class common_services : System.Web.Services.WebService
    {

        ODBC objODBC = new ODBC();
        ODBC_LOG objODBCLOG = new ODBC_LOG();
        DataTable dt = new DataTable();
        Encryption objENC = new Encryption();
        DateFormat objDt = new DateFormat();
        CreateFile objCreateFile = new CreateFile();
        CommonFunction objCommonFunction = new CommonFunction();
        DataSet ds = new DataSet();
        string strQuery = string.Empty;
        // int ActivityType = 65;
        [WebMethod]
        public DataSet filldropdown_af_province()
        {
            string strQuery = "SELECT `id` as ProvinceID, `state_name` as ProvinceName FROM `af_province`;";

            System.Data.DataSet ds = new System.Data.DataSet();

            try
            {
                ds = objODBC.getDataSet(strQuery);

                if (ds.Tables[0].Rows.Count > 0)
                {
                    System.Data.DataTable firstTable = ds.Tables[0];

                    return ds;
                }
                else
                {
                    return ds;
                }

            }
            catch (Exception ex)
            {
                return ds;
            }

            finally
            {
                ds.Dispose();
            }
        }

        [WebMethod]
        public void createfile()
        {

            CreateFile objCreateFile = new CreateFile();
            // objCreateFile.CreateTextFile();
        }



        [WebMethod]
        public string MD5HashNew(string strData)
        {
            string strValue = objENC.MD5Hash(strData);

            return strValue;
        }

        [WebMethod]
        public string SHA1HashNew(string strData)
        {
            string strValue = objENC.SHA1Hash(strData);

            return strValue;
        }

        [WebMethod]
        public ArrayList EasyPayBalance()
        {
            ArrayList arrUserParams = new ArrayList();
            EasyPay objEasyPay = new EasyPay();
            string strPassword = "set2018";
            string strTerminalID = "347922";
            string strLoginID = "mutahedapi";
            string MD5Password = objENC.MD5Hash(strPassword);
            arrUserParams = objEasyPay.getEasyPayBalance(strTerminalID, MD5Password, "5", strLoginID);


            return arrUserParams;
        }


        [WebMethod]
        public ArrayList RoshanBalance()
        {
            ArrayList arrUserParams = new ArrayList();
            Roshan objRoshan = new Roshan();
            string strMSISDN_Balance = "0799066145";
            arrUserParams = objRoshan.getRoshanBalanceBalance(strMSISDN_Balance);
            return arrUserParams;
        }



        // ActivityType =66 // strMobile : vaibhav:9860373352,prateek:2356458956
        [WebMethod]
        public ArrayList er_retailer_group_operation(string strUserName, int intUserID, string strGroup_name, int intOperID, int intMobileDeleteID, string strIPAddress, int intDesignationType, string strMacAddress, string strOSDetails, string strIMEINo, string strGcmID, string strAPPVersion, int intSource, int intCreatedByType, int intAction, int intGroupID)
        { // intAction = 1 :delete 2:insert 3:select 4:update 
            int intGroupCount, intPID = 0;
            ArrayList arrayLst = new ArrayList();
            string strMessage = string.Empty;
            string strDate = objDt.getCurDateTimeString();

            if (intUserID > 0 && strGroup_name != string.Empty && strGroup_name != "" && intAction > 0 && intAction < 5 && intGroupID >= 0 && intOperID > 0)
            {
                try
                {
                    try
                    {
                        objODBCLOG.executeNonQuery("call activity_logs('" + intUserID + "','" + intDesignationType + "','" + intCreatedByType + "','" + strIPAddress + "','" + strMacAddress + "','" + strOSDetails + "','" + strIMEINo + "','" + strGcmID + "','" + strAPPVersion + "','" + intSource + "','Retailer Group Operation',66,'','')");

                        objCreateFile.CreateTextFile(strIPAddress, strMacAddress, strOSDetails, strIMEINo, strGcmID, strAPPVersion, intSource.ToString(), intCreatedByType, 66, "Retailer Group Operation (er_retailer_group_operation)", strUserName);
                    }
                    catch (Exception)
                    {
                    }
                    finally
                    {
                        string strQueryGetParent = "select parent_id from er_login where userid =" + intUserID + "";

                        intPID = objODBC.executeScalar_int(strQueryGetParent);
                        if (intPID > 0)
                        {
                            if (intAction == 1)  // delete
                            {
                                string strQuery1 = "select count(1) from er_top_up_group where id =" + intMobileDeleteID + "";

                                intGroupCount = objODBC.executeScalar_int(strQuery1);
                                if (intGroupCount > 0)
                                {
                                    string strQuery = "delete from er_top_up_group where id=" + intMobileDeleteID + ")";
                                    objODBC.executeNonQuery(strQuery);
                                    strMessage = "group deleted successfully";
                                    arrayLst.Add("TRUE");
                                    arrayLst.Add(strMessage);
                                }
                                else
                                {
                                    strMessage = "Mobile Number not found";
                                    arrayLst.Add("FALSE");
                                    arrayLst.Add(strMessage);
                                }
                            }
                            else if (intAction == 2) // inserted
                            {
                                int intCOunt = objODBC.executeScalar_int("select count(1) from er_top_up_group where group_name ='" + strGroup_name + "' and parent_id=" + intPID + "");
                                if (intCOunt == 0)
                                {
                                    string strQuery = "insert into er_top_up_group(group_name,parent_id,operator_id,created_on) values ('" + strGroup_name.ToString() + "'," + intPID + "," + intOperID + ",'" + strDate.ToString() + "')";
                                    objODBC.executeNonQuery(strQuery);

                                    strMessage = "Group added successfully";
                                    arrayLst.Add("TRUE");
                                    arrayLst.Add(strMessage);
                                }
                                else
                                {
                                    strMessage = "Group already added";
                                    arrayLst.Add("FALSE");
                                    arrayLst.Add(strMessage);
                                }
                            }
                            else if (intAction == 3) //select
                            {
                                string strQuery = "select parent_id from er_login where userid =" + intUserID + "";

                                intPID = objODBC.executeScalar_int(strQuery);
                                if (intPID > 0)
                                {
                                    arrayLst = objCommonFunction.Get_ArrayLlist("SELECT g.id , g.group_name ,g.operator_id,ifnull(count(d.group_id),0) from  er_top_up_group g  left join er_top_up_group_details d on g.id = d.group_id where g.parent_id=" + intPID + " group by g.id ");
                                }
                                else
                                {
                                    strMessage = "Group not found";
                                    arrayLst.Add("FALSE");
                                    arrayLst.Add(strMessage);
                                }
                            }
                            else if (intAction == 4) //update
                            {
                                string strQuery = "select count(1) from er_top_up_group where id =" + intGroupID + "";

                                intGroupCount = objODBC.executeScalar_int(strQuery);
                                if (intGroupCount > 0)
                                {
                                    strQuery = "update er_top_up_group set group_name='" + strGroup_name + "' where id =" + intGroupID + "";
                                    objODBC.executeNonQuery(strQuery);
                                    strMessage = "group updated successfully";
                                    arrayLst.Add("TRUE");
                                    arrayLst.Add(strMessage);
                                }
                                else
                                {
                                    strMessage = "Group not found";
                                    arrayLst.Add("FALSE");
                                    arrayLst.Add(strMessage);
                                }
                            }
                        }
                        else
                        {
                            strMessage = "please send valid details For Group";
                            arrayLst.Add("FALSE");
                            arrayLst.Add(strMessage);
                        }
                    }
                }
                catch (Exception)
                {
                    strMessage = "Somthing went wrong";
                    arrayLst.Add("FALSE");
                    arrayLst.Add(strMessage);
                    throw;
                }
            }
            else
            {
                strMessage = "please send valid details For Group";
                arrayLst.Add("FALSE");
                arrayLst.Add(strMessage);
            }


            return arrayLst;
        }

        // ActivityType =67 // strMobile : vaibhav:9860373352,prateek:2356458956
        [WebMethod]
        public ArrayList er_retalier_group_member_operation(string strUserName, int intGroupID, string strMobile, int intOperID, string strIPAddress, int intDesignationType, string strMacAddress, string strOSDetails, string strIMEINo, string strGcmID, string strAPPVersion, int intSource, int intCreatedByType, int intAction, int intMemberTableID)
        {
            int chkGroupIDCount = 0;
            string strDate = objDt.getCurDateTimeString();
            string strQuery = string.Empty;
            ArrayList arrLst = new ArrayList();
            string strMessage = string.Empty;
            int intActivityType = 67;
            if (intGroupID > 0 && strMobile != string.Empty && strMobile != "" && !strMobile.Contains("+0123456789"))
            {
                // intAction = 1 :delete 2:insert 3:select 4:update 
                try
                {
                    try
                    {
                        objODBCLOG.executeNonQuery("call activity_logs('" + intGroupID + "','" + intDesignationType + "','" + intCreatedByType + "','" + strIPAddress + "','" + strMacAddress + "','" + strOSDetails + "','" + strIMEINo + "','" + strGcmID + "','" + strAPPVersion + "','" + intSource + "','Retalier Group Member Operation','" + intActivityType + "','','')");

                        objCreateFile.CreateTextFile(strIPAddress, strMacAddress, strOSDetails, strIMEINo, strGcmID, strAPPVersion, intSource.ToString(), intCreatedByType, intActivityType, "Retalier Group Member Operation (er_retalier_group_member_operation)", strUserName);
                    }
                    catch (Exception)
                    {

                        throw;
                    }
                    finally
                    {
                        if (intMemberTableID != 0 && intAction != 2)
                        {
                            strQuery = "select count(1) from er_top_up_group_details where group_id = " + intGroupID + " and id = " + intMemberTableID + "";
                            chkGroupIDCount = objODBC.executeScalar_int(strQuery);

                        }
                        else
                        {
                            chkGroupIDCount = 1;
                        }
                        if (chkGroupIDCount > 0)
                        {
                            if (intAction == 1) // delete
                            {
                                strQuery = "delete from er_top_up_group_details where group_id = " + intGroupID + " and id=" + intMemberTableID + "";
                                objODBC.executeNonQuery(strQuery);
                                strMessage = "Member deleted successfully";
                                arrLst.Add("TRUE");
                                arrLst.Add(strMessage);
                            }
                            else if (intAction == 2) //insert
                            {
                                int operID = objODBC.executeScalar_int("select operator_id from er_top_up_group where id =" + intGroupID + "");

                                if (operID == intOperID)
                                {
                                    int inTTrimedCOunt = 0;
                                    string strTrimedMobile = string.Empty;
                                    string strTrimedName = string.Empty;
                                    int j = strMobile.Length;
                                    string strConQry = string.Empty;
                                    for (int i = 0; i < j; i++)
                                    {
                                        string chk = strMobile[i].ToString();
                                        if (chk == ":")
                                        {
                                            strTrimedMobile = strMobile.Substring(inTTrimedCOunt + 1, 9); // need to be change
                                            inTTrimedCOunt += 11;
                                            if (!strTrimedMobile.Contains("+0123456789"))
                                            {
                                                int intcount = objODBC.executeScalar_int("select count(1) from er_top_up_group_details where mobile_no ='" + strTrimedMobile + "' and group_id =" + intGroupID + "");
                                                //int intcount = 0;
                                                if (intcount == 0)
                                                {
                                                    strConQry += "(" + intGroupID + ",'" + strTrimedMobile.ToString() + "','" + strTrimedName.ToString() + "'),";
                                                    if (inTTrimedCOunt > strMobile.Length)
                                                    {
                                                        string strRemoveComma = strConQry.Remove(strConQry.Length - 1, 1);
                                                        string strQuery1 = "insert into er_top_up_group_details(group_id,mobile_no,name) values " + strRemoveComma + "";

                                                        objODBC.executeNonQuery(strQuery1);
                                                        strMessage = "Member list added successfully";
                                                        arrLst.Add("TRUE");
                                                        arrLst.Add(strMessage);
                                                        break;
                                                    }
                                                    else
                                                    {
                                                        i += 10;
                                                        strTrimedName = string.Empty;
                                                        j += 10;
                                                    }
                                                }
                                                else
                                                {
                                                    strMessage = strTrimedMobile + " mobile number already present";
                                                    arrLst.Add("FALSE");
                                                    arrLst.Add(strMessage);
                                                    break;
                                                }
                                            }
                                            else
                                            {
                                                strMessage = "mobile number invalid";
                                                arrLst.Add("FALSE");
                                                arrLst.Add(strMessage);
                                                break;
                                            }

                                        }
                                        else
                                        {
                                            inTTrimedCOunt += 1;
                                            strTrimedName += chk;
                                        }
                                    }
                                }
                                else
                                {
                                    strMessage = "same operator is to be expected in one group";
                                    arrLst.Add("FALSE");
                                    arrLst.Add(strMessage);
                                }

                            }
                            else if (intAction == 3) //select
                            {
                                arrLst = objCommonFunction.Get_ArrayLlist("select concat(id,':',name,':',mobile_no) from er_top_up_group_details where group_id=" + intGroupID + " ");
                            }
                            else if (intAction == 4) //update
                            {
                                int inTTrimedCOunt = 0;
                                string strTrimedMobile = string.Empty;
                                string strTrimedName = string.Empty;
                                int j = strMobile.Length;
                                string strConQry = string.Empty;
                                for (int i = 0; i < j; i++)
                                {
                                    string chk = strMobile[i].ToString();
                                    if (chk == ":")
                                    {
                                        strTrimedMobile = strMobile.Substring(inTTrimedCOunt + 1, 10);
                                        inTTrimedCOunt += 12;
                                        if (!strTrimedMobile.Contains("+0123456789"))
                                        {
                                            if (inTTrimedCOunt > strMobile.Length)
                                            {
                                                string strQuery1 = "update er_top_up_group_details set name='" + strTrimedName.ToString() + "',mobile_no='" + strTrimedMobile.ToString() + "' where group_id=" + intGroupID + " and id =" + intMemberTableID + "";

                                                objODBC.executeNonQuery(strQuery1);
                                                strMessage = "Member updated successfully";
                                                break;
                                            }
                                            else
                                            {
                                                i += 11;
                                                strTrimedName = string.Empty;
                                                j += 11;
                                            }

                                        }
                                        else
                                        {
                                            strMessage = "mobile number invalid";
                                            arrLst.Add("FALSE");
                                            arrLst.Add(strMessage);
                                            break;
                                        }

                                    }
                                    else
                                    {
                                        inTTrimedCOunt += 1;
                                        strTrimedName += chk;
                                    }
                                }

                            }
                        }
                        else
                        {
                            strMessage = "Please enter valid details";
                            arrLst.Add("FALSE");
                            arrLst.Add(strMessage);

                        }
                    }
                }
                catch (Exception)
                {
                    strMessage = "somthing went wrong";
                    arrLst.Add("FALSE");
                    arrLst.Add(strMessage);
                    return arrLst;

                }
            }
            else
            {
                strMessage = "please send valid details";
                arrLst.Add("FALSE");
                arrLst.Add(strMessage);
                return arrLst;
            }
            return arrLst;

        }

        //  int intActivityType = 74;
        [WebMethod]
        public ArrayList er_Get_Group_Member_List(string strUserName, int intGroupID, int intUserID, string strIPAddress, int intDesignationType, string strMacAddress, string strOSDetails, string strIMEINo, string strGcmID, string strAPPVersion, int intSource, int intCreatedByType)
        {
            int intActivityType = 74;

            ArrayList arrUserParams = new ArrayList();

            try
            {
                arrUserParams = objCommonFunction.Get_ArrayLlist("select concat(id,':',name,':',mobile_no) from er_top_up_group_details where group_id=" + intGroupID + " ");


                objCreateFile.CreateTextFile(strIPAddress, strMacAddress, strOSDetails, strIMEINo, strGcmID, strAPPVersion, intSource.ToString(), intCreatedByType, intActivityType, "Android Retailer Dashboard Fetch Sucessfully", strUserName);

                return arrUserParams;
            }
            catch (Exception ex)
            {

                objCreateFile.CreateTextFile(strIPAddress, strMacAddress, strOSDetails, strIMEINo, strGcmID, strAPPVersion, intSource.ToString(), intCreatedByType, intActivityType, "Android Retailer Dashboard Fetch Failed", strUserName);

                return arrUserParams;
            }

        }
        [WebMethod]
        public string SendEmail(string strEmailBody, string strEmailSubject, string strHost, string strEmailTO)
        {
            SmtpEmail objSmtpEmail = new SmtpEmail();
            string strDetails = objSmtpEmail.SendEmailFunction(strEmailBody, strEmailSubject, strHost, strEmailTO);
            return strDetails;
        }
        [WebMethod]
        public string FileUplode(string strBase64Photo)
        {
            string strMessage = "";
            byte[] imageBytes;
            string imgURl = "";
            string strGenerateRandumName = "ABCD1";

            try
            {
                try
                {
                    strGenerateRandumName = strGenerateRandumName + ".jpg";
                    imageBytes = Convert.FromBase64String(strBase64Photo);
                    MemoryStream ms = new MemoryStream(imageBytes, 0, imageBytes.Length);
                    ms.Write(imageBytes, 0, imageBytes.Length);
                    System.Drawing.Image image = System.Drawing.Image.FromStream(ms, true);
                    image.Save(Server.MapPath("~/PurchaseWalletRequestImage/" + strGenerateRandumName));
                    imgURl = "http://topup.setaraganmutahed.com/PurchaseWalletRequestImage/" + strGenerateRandumName;

                }
                catch (Exception)
                {

                }
            }
            catch (Exception)
            {

            }
            return imgURl;
        }

        [WebMethod]
        public string SalaamBalance()
        {
            string strResposne = "";
            try
            {
                SalaamApi objSalaamApi = new SalaamApi();
                strResposne = objSalaamApi.getSalaamBalance();
            }
            catch (Exception)
            {

            }
            return strResposne;
        }

        //[WebMethod]
        //public string TopupSalam(string strRequestUniqueID, string strMobileNoTxReference, string strAmount, string strEmailID)
        //{
        //    string strResposne = "";
        //    try
        //    {
        //        SalaamApi objSalaamApi = new SalaamApi();
        //        strResposne = objSalaamApi.strTransactionTopup(strRequestUniqueID, strMobileNoTxReference, strAmount, strEmailID);
        //    }
        //    catch (Exception)
        //    {

        //    }
        //    return strResposne;
        //}


        //[WebMethod]
        //public string checkSplitMessage()
        //{
        //    string strResposne = "NA";
        //    try
        //    {
        //        string strRoshanMessage = "Transaction successful. Type : Buy Airtime,Mobile Number : 0799477575, Amount  25 AFA, Txn ID:RC190213.1615.A47287, New  balance: 50958.4276 AFA";
        //        string[] strCommissionStructure = strRoshanMessage.Split(new char[] { ',' });
        //        string strOpMessage = strCommissionStructure[4];
        //        var replacements = new[] { new { Find = " New  balance: ", Replace = "0" }, new { Find = " AFA", Replace = "" }, };
        //        var myLongString = strOpMessage;
        //        foreach (var set in replacements)
        //        {
        //            myLongString = myLongString.Replace(set.Find, set.Replace);
        //        }
        //        strResposne = myLongString;
        //    }
        //    catch (Exception)
        //    {

        //    }
        //    return strResposne;
        //}

        [WebMethod]
        public string password()
        {
            CommonFunction objComFUN = new CommonFunction();
            Encryption objEncrypt = new Encryption();

            string strPasswordDetail = "Specified key is not a valid size for this algorithm.";
            while (strPasswordDetail == "Specified key is not a valid size for this algorithm.")
            {
                try
                {
                    string strPassword = objComFUN.GenrateRandomKeyString();
                    string passkey = objComFUN.GenratePassKey();
                    strPasswordDetail = objEncrypt.EncryptQueryString(strPassword, passkey);
                }
                catch (Exception)
                {


                }

            }


            return strPasswordDetail;
        }



        [WebMethod]
        public DataSet er_ActivityLog_List_dropdown(string strUserName, int intUserID, string strIPAddress, int intDesignationType, string strMacAddress, string strOSDetails, string strIMEINo, string strGcmID, string strAPPVersion, int intSource, int intCreatedByType)
        {
            try
            {
                if (intUserID > 0)
                {
                    try
                    {
                        objCreateFile.CreateTextFile(strIPAddress, strMacAddress, strOSDetails, strIMEINo, strGcmID, strAPPVersion, intSource.ToString(), intCreatedByType, 75, " Admin Fetch Activity Log List Dropdown", strUserName);
                    }
                    catch (Exception)
                    {
                    }
                    finally
                    {
                        strQuery = "select DISTINCT description from er_activity_logs where userid='" + intUserID + "' and created_by_type='" + intCreatedByType + "'";
                    }
                }
                ds = objODBCLOG.Report(strQuery);
                return ds;
            }
            catch (Exception)
            {
                throw;

            }
            finally
            {
                ds.Dispose();
            }
        }

        public string TruncateAtWord(string input, int length)
        {
            if (input == null || input.Length < length)
                return input;
            int iNextSpace = input.LastIndexOf("", length, StringComparison.Ordinal);
            return string.Format("{0}", input.Substring(0, (iNextSpace > 0) ? iNextSpace : length).Trim());
        }

        [WebMethod]
        public string EtisalatTopup(string strRechargeTransactionID, string strMobileNumber, int dblAmount, int intEtisalatTableID, int intTransactionType, string strReverseID)
        {
            string strResposne = string.Empty;
            try
            {
                //Topup Here
                //   TopupEtisalat.Recharge.ETSLTopup(strRechargeTransactionID, strMobileNumber, dblAmount, intEtisalatTableID, intTransactionType, strReverseID);

                // var StrData = "`SC`009c1.00CCB00000PPSPHS0000000002DLGCONFFFF00000004TXEND FFFFACK:CHARGE CASH ACNT:RETN=0000,DESC=SUCCESS,BEFOROPER=2161,FEE=200,AFTEROPER=2361,CSVSTOP=2019-09-22C1C3CDAD"; // Topup SUccess

                var StrData = "`SC`008c1.00CCB00000PPSPHS0000000002DLGCONFFFF00000004TXEND FFFFACK:RLLBCK CHRG VOUCH:RETN=8,DESC=Illegal MML Parameter FEE cause:FEE is required.  DABAE0D5"; // Topup Failed
                                                                                                                                                                                              // var StrData = "`SC`009c1.00CCB00000PPSPHS0000000002DLGCONFFFF00000004TXEND FFFFACK:CHARGE CASH ACNT:RETN=0000,DESC=SUCCESS,BEFOROPER = 136,FEE = 100,AFTEROPER = 236,CSVSTOP=2019-09-22  C6C7D4B5";// Rever success

                if (Regex.IsMatch(StrData, string.Format(@"\b{0}\b", Regex.Escape("ACNT:RETN=0000,DESC=SUCCESS"))))
                {


                    strResposne = TruncateAtWord(StrData, 64); // Truncate From Position of Words
                    string output = StrData.Replace(strResposne, ""); // Replace Words From Data
                    output = output.Remove(output.Length - 8); // Deduct Words from Last                   
                    string StrCheckSum = StrData.Substring(StrData.Length - 8); // Getting Checksum

                    string[] strDescriptionResponse = output.Split(new char[] { ',' });
                    // strMessage = "2,OOPs! You do not have sufficient balance.,Failed,NA,NA";

                    string TRDStatus = strDescriptionResponse[0];
                    string TRDDescription = strDescriptionResponse[1]; ;
                    string BEFOROPER = strDescriptionResponse[2];
                    string FEE = strDescriptionResponse[3];
                    string AFTEROPER = strDescriptionResponse[4];
                    string CSVSTOP = strDescriptionResponse[5];



                    strResposne = output;
                }
                else
                {
                    strResposne = TruncateAtWord(StrData, 64); // Truncate From Position of Words
                    string output = StrData.Replace(strResposne, ""); // Replace Words From Data
                    output = output.Remove(output.Length - 8); // Deduct Words from Last                   
                    string StrCheckSum = StrData.Substring(StrData.Length - 8); // Getting Checksum

                    string[] strDescriptionResponse = output.Split(new char[] { ',' });
                    // strMessage = "2,OOPs! You do not have sufficient balance.,Failed,NA,NA";

                    string TRDStatus = strDescriptionResponse[0];
                    string TRDDescription = strDescriptionResponse[1]; ;


                    strResposne = "FAILED";
                }
            }
            catch (Exception)
            {

            }
            return strResposne;
        }


        [WebMethod]
        public int EtisalatTopupUpdateQuerry(string strQuerry)
        {
            int intCount = 0;
            try
            {
                intCount = objODBC.executeNonQuery(strQuerry);
            }
            catch (Exception)
            {

            }
            return intCount;
        }










        [WebMethod]
        public string OpenEXE(string dblAmount, string strMobile, string intTopUpRecTransactionID, string ReverseID, string intEtisalatID, string Transtype)
        {
            string str = "";
            //    EtisalatWindowService.EtisalatTopup etisalatTopup = new EtisalatWindowService.EtisalatTopup();
            // etisalatTopup.setData(dblAmount, strMobile, intTopUpRecTransactionID, ReverseID, intEtisalatID, Transtype);
            try
            {
                //System.Diagnostics.ProcessStartInfo proc = new System.Diagnostics.ProcessStartInfo();

                //proc.FileName = @"C:\inetpub\wwwroot\Etisalat\EtisalatTopup.exe";
                //// proc.FileName = @"C:\Users\Admin\Desktop\EtisalatTopup";
                //// proc.Arguments = @""" " + dblAmount + " " + strMobile + " " + intTopUpRecTransactionID + " " + ReverseID + " " + intEtisalatID + " " + Transtype + "";
                //string strData = dblAmount + "," + strMobile + "," + intTopUpRecTransactionID + "," + ReverseID + "," + intEtisalatID + "," + Transtype;
                //proc.Arguments = @"" + strData + "";

                //System.Diagnostics.Process.Start(proc);


                // testing

                System.Diagnostics.ProcessStartInfo proc = new System.Diagnostics.ProcessStartInfo();
                proc.FileName = @"C:\inetpub\wwwroot\EtisalatTopupapi\EtisalatTopup11.exe";
                //  proc.FileName = @"C:\inetpub\wwwroot\EtisalatTopupapi\EtisalatTopup.exe";
                // proc.FileName = @"C:\Users\Admin\Desktop\EtisalatTopup";
                // proc.Arguments = @""" " + dblAmount + " " + strMobile + " " + intTopUpRecTransactionID + " " + ReverseID + " " + intEtisalatID + " " + Transtype + "";
                string strData = dblAmount + "," + strMobile + "," + intTopUpRecTransactionID + "," + ReverseID + "," + intEtisalatID + "," + Transtype;
                proc.Arguments = @"" + strData + "";

                System.Diagnostics.Process.Start(proc);

            }
            catch (Exception ex)
            {
                str = ex.ToString();
            }
            return str;
        }

        [WebMethod]
        public ArrayList er_delete_alternate_number(string strUserName, int intUserID, string strAlterMobile, int intOperID, string strIPAddress, int intDesignationType, string strMacAddress, string strOSDetails, string strIMEINo, string strGcmID, string strAPPVersion, int intSource, int intCreatedByType)
        {
            ArrayList strResult = new ArrayList();
            try
            {
                objCreateFile.CreateTextFile(strIPAddress, strMacAddress, strOSDetails, strIMEINo, strGcmID, strAPPVersion, intSource.ToString(), intCreatedByType, 75, "Delete Alternate Number", strUserName);

                objODBCLOG.executeNonQuery("call activity_logs('" + intUserID + "','" + intDesignationType + "','" + intCreatedByType + "','" + strIPAddress + "','" + strMacAddress + "','" + strOSDetails + "','" + strIMEINo + "','" + strGcmID + "','" + strAPPVersion + "','" + intSource + "','Delete Alternate Number','78','','')");
            }
            catch (Exception)
            {
            }
            finally
            {

                objODBC.executeNonQuery("delete from er_mobile where userid=" + intUserID + " and mobile='" + strAlterMobile + "' and operator_id=" + intOperID + " and mob_type=2 and usertype=2");

                strResult.Add("TRUE");
                strResult.Add("Alternet Mobile number has been deleted successfully");

            }

            return strResult;
        }

        [WebMethod]
        public void Proce()
        {
            try
            {

                string url = "http://topup.setaraganmutahed.com/Recharge_pending_request.aspx";
                // Using WebRequest
                WebRequest request = WebRequest.Create(url);
                WebResponse response = request.GetResponse();
                string result = new StreamReader(response.GetResponseStream()).ReadToEnd();
                // Using WebClient
                string result1 = new WebClient().DownloadString(url);
            }
            catch (Exception)
            {

            }
        }

        [WebMethod]
        public string Send_SMS(string strMobile, int intOperaterID, string strSMS)
        {
            string str = "";
            //    EtisalatWindowService.EtisalatTopup etisalatTopup = new EtisalatWindowService.EtisalatTopup();
            // etisalatTopup.setData(dblAmount, strMobile, intTopUpRecTransactionID, ReverseID, intEtisalatID, Transtype);
            try
            {
                // SMPP SMS Code Below
                SMPP_SMS_Format objSMPP_SMS_Format = new SMPP_SMS_Format();
                string strGetSMS = "Dear Abdul Retailer, You have been logged to your account SML8141321 From Web at 2019-05-11 10:08:24 . Thankyou for choosing Unipay Setaragan Mutahed ! ";
                SMPP1 objSMPP1 = new SMPP1();
                string strSMPPResponse = "NA";
                try
                {
                    objCommonFunction.er_SMS_Alert(375, intOperaterID, strMobile, intOperaterID, strGetSMS, 1, strSMPPResponse);

                }
                catch (Exception)
                {

                }

            }
            catch (Exception ex)
            {
                str = ex.ToString();
            }
            return str;
        }


        [WebMethod]
        public string BoloroBalance()
        {
            ArrayList arrUserParams = new ArrayList();
            BoloroApi objBoloro = new BoloroApi();
            return objBoloro.getBoloroBalance();
        }

        [WebMethod]
        public string BoloroTopup(int intUserID, int intRechargeID, int intOperatortableID, string MSINDN_mobile, string dblAmount, string strTelco_Code)
        {
            ArrayList arrUserParams = new ArrayList();
            BoloroApi objBoloro = new BoloroApi();
            return objBoloro.BoloroTopup(intUserID, intRechargeID, intOperatortableID, MSINDN_mobile, dblAmount, dblAmount);
        }


        [WebMethod]
        public string BoloroCheckTransaction(string strTransactionNumber, int intUserID, int intRechargeID, string intOperatortableID)
        {
            ArrayList arrUserParams = new ArrayList();
            BoloroApi objBoloro = new BoloroApi();
            return objBoloro.Boloro_Check_Transaction(strTransactionNumber, intUserID, intRechargeID, intOperatortableID);
        }


        [WebMethod]
        public string MTNBalance()
        {
            MTNApi onjMTNAPI = new MTNApi();
            return onjMTNAPI.GetMTNAPIBalance();
        }

        [WebMethod]
        public string MTNAllResponseBalance()
        {
            MTNApi onjMTNAPI = new MTNApi();
            return onjMTNAPI.MTNAPIBalance();
        }

        [WebMethod]
        public string MTNTopup(string intMTNID, string strTransxID, string strMobileNumber, double dblAmount)
        {
            MTNApi onjMTNAPI = new MTNApi();
            return onjMTNAPI.MTNAPITopup(intMTNID, strTransxID, strMobileNumber, dblAmount);
        }


        [WebMethod]
        public string FetchBalance()
        {
            string strtext = "You topped up 10.00 AFA to 93764524139. Your balance is now 425,180.00 AFA. The transaction ID: 2020040617084358101298445";
            string[] strBalanceStructure = strtext.Split(new char[] { '.' });
            string strOpMessage = strBalanceStructure[1];
            var replacements = new[] { new { Find = "Your balance is now ", Replace = "0" }, new { Find = " AFA. The transaction ID", Replace = "" }, };
            var myLongString = strOpMessage;
            foreach (var set in replacements)
            {
                myLongString = myLongString.Replace(set.Find, set.Replace);
            }

            return myLongString;
        }

        //[WebMethod]
        //public string returnString(string strRechargeTransactionID)
        //{
        //    if (strRechargeTransactionID.Length > 5)
        //    {
        //        if (strRechargeTransactionID.Length == 6)
        //        {
        //            strRechargeTransactionID = strRechargeTransactionID.Substring(1);
        //        }
        //        if (strRechargeTransactionID.Length == 7)
        //        {
        //            strRechargeTransactionID = strRechargeTransactionID.Substring(2);
        //        }
        //        if (strRechargeTransactionID.Length == 8)
        //        {
        //            strRechargeTransactionID = strRechargeTransactionID.Substring(3);
        //        }
        //        if (strRechargeTransactionID.Length == 9)
        //        {
        //            strRechargeTransactionID = strRechargeTransactionID.Substring(4);
        //        }
        //        if (strRechargeTransactionID.Length == 10)
        //        {
        //            strRechargeTransactionID = strRechargeTransactionID.Substring(5);
        //        }

        //    }
        //    return strRechargeTransactionID;
        //}

        //[WebMethod]
        //public string MTNFileEdit(int intUserID, string strUserName, string strCousMobileNumber, string strOperaterName, int intOperaterID, double dblAmount, int intRechargeLogType, string TransactionID, string strPhone)
        //{
        //    try
        //    {
        //        Recharge objRecharge = new Recharge();
        //        objRecharge.SendSMPPSMS(intUserID, objODBC.executeScalar_str("select username FROM `er_login` where userid='" + intUserID + "'"), strCousMobileNumber, strOperaterName, intOperaterID, dblAmount, intRechargeLogType, TransactionID.ToString(), strPhone);
        //    }
        //    catch (Exception)
        //    { }
        //    return "Success";
        //}


        [WebMethod]
        public string sendSMS(int intUserID, int intCreatedbyType, string strMobileNumber, int intOperaterID, string strMessage, int intStatus, string strSMPPResposne)
        {
            CommonFunction objCMFN = new CommonFunction();
            bool status = objCMFN.er_SMS_Alert(intUserID, intCreatedbyType, strMobileNumber, intOperaterID, strMessage, intStatus, strSMPPResposne);
            if (status == true)
            {
                return "SMS send Successfully";
            }
            else
            {
                return "SMS send Failed";
            }


        }

        [WebMethod]
        public string AloginCheck(int userid,string Username,int source)
        {
            string strDate = objDt.getCurDateTimeString();
            try
            {
                int intLogStatusCount = objODBC.executeScalar_int("SELECT count(1) FROM `er_login_inactive` WHERE status=0 and userid=" + userid);
                if(intLogStatusCount==0)
                {
                    objODBC.executeNonQuery("INSERT INTO `er_login_inactive`(`userid`, `username`,`created_on`,`source`) VALUES ('"+ userid + "','"+ Username + "','"+ strDate + "','"+ source + "')");
                    return "Insert Successfully";
                }
                else
                {
                    return "Update Successfully";
                }
              
            }
            catch (Exception)
            {
                return "Error";

            }                    
        }
    }
}

