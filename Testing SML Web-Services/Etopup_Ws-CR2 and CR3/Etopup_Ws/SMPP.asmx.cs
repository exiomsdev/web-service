﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Threading;
using Etopup_Ws.old_App_Code;
using System.Data;
using System.IO;

namespace Etopup_Ws
{
    /// <summary>
    /// Summary description for SMPP1
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    [System.Web.Script.Services.ScriptService]
    public class SMPP1 : System.Web.Services.WebService
    {
        DataSet ds = new DataSet();
        string strQuery = string.Empty;
        string strLimit = string.Empty;
        ODBC objODBC = new ODBC();
        ODBC_LOG objODBCLOG = new ODBC_LOG();
        DataTable dt = new DataTable();
        DataTable dt2 = new DataTable();
        DateFormat objDT = new DateFormat();
        Encrypt_Decrypt ED = new Encrypt_Decrypt();
        CreateFile objCreateFile = new CreateFile();
        CommonFunction objcf = new CommonFunction();
        /// <summary>
        /// OLD APPLIATION COde
        /// </summary>
        /// <param name="strDestinationNumber"></param>
        /// <param name="strMessage"></param>
        /// <returns></returns>
        [WebMethod]
        [System.Runtime.InteropServices.ComVisible(true)]
        public string Etisalat_SMPP(string strDestinationNumber, string strMessage)
        {
            // strDestinationNumber=9 Digit Mobile
            // strsourceNumber=93543
            string strsourceNumber = "93543";
            EsmeGui.mainForm main = new EsmeGui.mainForm();
            string strResult = String.Empty;
            //strResult = main.mainForm_Shown(this, new EventArgs());
            //if (strResult == "True")
            //{
            strResult = main.set_data(strsourceNumber.ToString().Trim(), strDestinationNumber.ToString().Trim(), strMessage.ToString().Trim());

            // main.btnSendMessage_Click(this, e);
            //    strResult = main.btnSendMessage_Click(this, new EventArgs());
            //}

            return strResult;
        }



        [WebMethod]
        public string Roshan_SMPP(string strDestinationNumber, string strMessage)
        {
            // strDestinationNumber=9 Digit Mobile
            // strsourceNumber=543
            RsmeGui.mainForm RSMPP = new RsmeGui.mainForm();
            string strResult = String.Empty;
            //  strResult = RSMPP.mainForm_Shown(this, new EventArgs());
            //if (strResult == "True")
            //{
            strResult = RSMPP.set_data(strDestinationNumber.ToString().Trim(), strMessage.ToString().Trim());

            // main.btnSendMessage_Click(this, e);
            //  strResult = RSMPP.btnSendMessage_Click(this, new EventArgs());
            //}
            return strResult;



            //Process process = new Process();


            //process.StartInfo.FileName =Server.MapPath("~/Content/RsmeGui.exe");
            //process.StartInfo.Arguments = strDestinationNumber + " " + strMessage.Replace(' ','_');


            ////// These two optional flags ensure that no DOS window
            ////// appears
            ////process.StartInfo.UseShellExecute = false;
            ////process.StartInfo.CreateNoWindow = true;


            ////// Capture the output of the Console application
            ////process.StartInfo.RedirectStandardOutput = true;


            //process.Start();


            ////// Wait for the Console application to finish
            ////  process.WaitForExit();


            ////// Read the output
            ////  string result = process.StandardOutput.ReadToEnd();
            //process.Close();
            //return "True";


        }

        [WebMethod]
        public string Etisalat_SMPP_win(int intUid, int intUtype, string strDestinationNumber, string strMessage)
        {
            ODBC objodbc = new ODBC();
            //// strDestinationNumber=9 Digit Mobile
            //// strsourceNumber=93543
            //string strsourceNumber = "93543";
            //EsmeGui.mainForm main = new EsmeGui.mainForm();
            //string strResult = String.Empty;
            ////strResult = main.mainForm_Shown(this, new EventArgs());
            ////if (strResult == "True")
            ////{
            //    strResult = main.set_data(strsourceNumber.ToString().Trim(), strDestinationNumber.ToString().Trim(), strMessage.ToString().Trim());

            //// main.btnSendMessage_Click(this, e);
            ////    strResult = main.btnSendMessage_Click(this, new EventArgs());
            ////}
            objodbc.executeNonQuery("INSERT INTO `sms_smpp_service`(`userid`, `usertype`,`mobile`,  `message`,  `operater_id`,`status`, `smpp_respose`) VALUES ('" + intUid + "','" + intUtype + "','" + strDestinationNumber + "','" + strMessage + "',2,0,'')");
            return "True";
        }



        [WebMethod]
        public string Roshan_SMPP_win(int intUid, int intUtype, string strDestinationNumber, string strMessage)
        {
            ODBC objodbc = new ODBC();
            //// strDestinationNumber=9 Digit Mobile
            //// strsourceNumber=543
            //RsmeGui.mainForm RSMPP = new RsmeGui.mainForm();
            //string strResult = String.Empty;
            ////  strResult = RSMPP.mainForm_Shown(this, new EventArgs());
            ////if (strResult == "True")
            ////{
            //strResult = RSMPP.set_data(strDestinationNumber.ToString().Trim(), strMessage.ToString().Trim());

            //// main.btnSendMessage_Click(this, e);
            ////  strResult = RSMPP.btnSendMessage_Click(this, new EventArgs());
            ////}
            objodbc.executeNonQuery("INSERT INTO `sms_smpp_service`(`userid`, `usertype`,`mobile`,  `message`,  `operater_id`,`status`, `smpp_respose`) VALUES ('" + intUid + "','" + intUtype + "','" + strDestinationNumber + "','" + strMessage + "',3,0,'')");
            return "True";



            //Process process = new Process();


            //process.StartInfo.FileName =Server.MapPath("~/Content/RsmeGui.exe");
            //process.StartInfo.Arguments = strDestinationNumber + " " + strMessage.Replace(' ','_');


            ////// These two optional flags ensure that no DOS window
            ////// appears
            ////process.StartInfo.UseShellExecute = false;
            ////process.StartInfo.CreateNoWindow = true;


            ////// Capture the output of the Console application
            ////process.StartInfo.RedirectStandardOutput = true;


            //process.Start();


            ////// Wait for the Console application to finish
            ////  process.WaitForExit();


            ////// Read the output
            ////  string result = process.StandardOutput.ReadToEnd();
            //process.Close();
            //return "True";


        }


        public string sendSMPPMessage(int intOperaterID, string strMobile, string strSMS)
        {
            string strSMPPResponse = "NA";
            if (intOperaterID == 2)
            {
                try
                {
                    // strSMPPResponse = Etisalat_SMPP(strMobile, strSMS);
                    strSMPPResponse = "Bound";
                }
                catch (Exception)
                {

                }
            }
            else if (intOperaterID == 3)
            {
                try
                {
                    // strSMPPResponse = Roshan_SMPP(strMobile, strSMS);
                    strSMPPResponse = "Bound";
                }
                catch (Exception)
                {

                }
            }
            return strSMPPResponse;
        }

        /// <summary>
        /// Old Data Code
        /// </summary>
        /// <param name="intOperatorID"></param>
        /// <returns></returns>

        [WebMethod]
        public DataSet er_SMPP_SMS_Service(int intOperatorID)
        {
            try
            {
                try
                {
                    strQuery = "SELECT `id`, `userid`, `usertype`, `mobile`, `operater_id`, `message`, `created_on`, `status`, `smpp_respose` FROM `sms_smpp_service` WHERE  operater_id='" + intOperatorID + "' and status=0 order by id limit 50;";

                    ds = objODBC.Report(strQuery);
                }
                catch (Exception)
                {

                }
                return ds;
            }
            catch (Exception)
            {
                return ds;
            }
            finally
            {
                ds.Dispose();
            }
        }

        [WebMethod]
        public DataSet er_SMPP_SMS_update(int intID, string strsmpp_respose)
        {
            try
            {
                if (strsmpp_respose == "Bound")
                {
                    try
                    {
                        strQuery = "Update `sms_smpp_service` SET  status=1,`smpp_respose`='" + strsmpp_respose + "'  WHERE  `id`='" + intID + "'";
                        string strQuery1 = "Update `sms_smpp` SET status=1, `smpp_respose`='" + strsmpp_respose + "'  WHERE  `id`='" + intID + "'";

                        objODBC.executeNonQuery(strQuery);
                        objODBC.executeNonQuery(strQuery1);
                    }
                    catch (Exception)
                    {
                        return ds;
                    }
                }

                return ds;
            }
            catch (Exception)
            {
                return ds;
            }
            finally
            {
                ds.Dispose();
            }
        }


        [WebMethod]
        public DataSet er_SMPP_SMS_Querry_update(int intID, string strDileveryDatetime, string strPhoneNumber, string strResMessageID, string strResponseFinalStatus)
        {
            try
            {

                try
                {
                    if (strDileveryDatetime == "NA")
                    {
                        strDileveryDatetime = objDT.getCurDateTimeString();
                    }
                    //if (Convert.ToInt32(strResMessageID) > 6)
                    //{
                    strQuery = "Update `sms_smpp_service` SET final_status='" + strResponseFinalStatus + "', `delaverydatetime`='" + strDileveryDatetime + "',res_phonenumber='" + strPhoneNumber + "',res_message_id='" + strResMessageID + "'  WHERE  `id`='" + intID + "'";
                    //}
                    //else
                    //{
                    //    strQuery = "Update `sms_smpp_service` SET final_status='" + strResponseFinalStatus + "', `delaverydatetime`='" + strDileveryDatetime + "',res_phonenumber='" + strPhoneNumber + "',res_message_id='" + strResMessageID + "',status=0  WHERE  `id`='" + intID + "'";
                    //}

                    objODBC.executeNonQuery(strQuery);
                }
                catch (Exception)
                { }
                return ds;
            }
            catch (Exception)
            {
                return ds;
            }
            finally
            {
                ds.Dispose();
            }
            /*
                        try
                        {
                            if (strDileveryDatetime == "NA")
                            {
                                strDileveryDatetime = objDT.getCurDateTimeString();
                            }

                            string strName = "SMS_Log_" + objDT.getCurDateTime_Dayanamic_format("dd_MM_yyyy").ToString() + ".xml";
                            string fileName = @"C:\EtisalatSMSLogsFile\" + strName;
                            try
                            {

                                // Check if file already exists. If yes, Send SMS.   
                                if (File.Exists(fileName))
                                {
                                    ds.ReadXml(fileName);

                                    for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                                    {
                                        string strID = ds.Tables[0].Rows[i]["ID"].ToString();
                                        if (intID == int.Parse(strID))
                                        {
                                            string strTargetMobileNumber = ds.Tables[0].Rows[i]["mobile"].ToString();
                                            string strMessage = ds.Tables[0].Rows[i]["message"].ToString();
                                            ds.Tables[0].Rows[i]["smpp_respose"] = strresult.ToString();
                                            ds.Tables[0].Rows[i]["res_message_id"] = strResMessageID.ToString();
                                            ds.Tables[0].Rows[i]["res_phonenumber"] = strPhoneNumber.ToString();
                                            ds.Tables[0].Rows[i]["final_status"] = strResponseFinalStatus.ToString();
                                            ds.Tables[0].Rows[i]["delaverydatetime"] = strDileveryDatetime.ToString();
                                            //if (strResMessageID == "NA" || strResMessageID == "Pending")
                                            //{
                                            //}
                                            //else
                                            //{
                                            ds.Tables[0].Rows[i]["status"] = "Success";
                                            // }
                                            ds.Tables[0].Rows[i]["attempt"] = (int.Parse(ds.Tables[0].Rows[i]["attempt"].ToString()) + 1).ToString();
                                        }
                                    }
                                    ds.WriteXml(fileName);
                                }
                                else
                                {

                                }
                            }
                            catch (Exception)
                            { }
                            return ds;

                        }
                        catch (Exception)
                        {
                            return ds;
                        }
                        finally
                        {
                            ds.Dispose();
                        } */
        }

        [WebMethod]
        public DataSet er_SMPP_XML_update(int intID, string strDileveryDatetime, string strPhoneNumber, string strResMessageID, string strResponseFinalStatus, string strresult)
        {
            try
            {
                if (strDileveryDatetime == "NA")
                {
                    strDileveryDatetime = objDT.getCurDateTimeString();
                }

                string strName = "SMS_Log_" + objDT.getCurDateTime_Dayanamic_format("dd_MM_yyyy").ToString() + ".xml";
                string fileName = @"C:\EtisalatSMSLogsFile\" + strName;
                try
                {

                    // Check if file already exists. If yes, Send SMS.   
                    if (File.Exists(fileName))
                    {
                        ds.ReadXml(fileName);

                        for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                        {
                            string strID = ds.Tables[0].Rows[i]["ID"].ToString();
                            if (intID == int.Parse(strID))
                            {
                                string strTargetMobileNumber = ds.Tables[0].Rows[i]["mobile"].ToString();
                                string strMessage = ds.Tables[0].Rows[i]["message"].ToString();
                                ds.Tables[0].Rows[i]["smpp_respose"] = strresult.ToString();
                                ds.Tables[0].Rows[i]["res_message_id"] = strResMessageID.ToString();
                                ds.Tables[0].Rows[i]["res_phonenumber"] = strPhoneNumber.ToString();
                                ds.Tables[0].Rows[i]["final_status"] = strResponseFinalStatus.ToString();
                                ds.Tables[0].Rows[i]["delaverydatetime"] = strDileveryDatetime.ToString();
                                if (strResMessageID == "NA" || strResMessageID == "Pending")
                                {
                                }
                                else
                                {
                                    ds.Tables[0].Rows[i]["status"] = "Success";
                                }
                                ds.Tables[0].Rows[i]["attempt"] = "1";
                                ds.WriteXml(fileName);
                            }
                        }

                    }
                    else
                    {

                    }
                }
                catch (Exception)
                { }
                return ds;

            }
            catch (Exception)
            {
                return ds;
            }
            finally
            {
                ds.Dispose();
            }
        }
        [WebMethod]
        public DataSet er_SMPP_SMS_Check(string intID, string strResMessageID)
        {
            try
            {
                if (strResMessageID.Length > 6)
                {
                    strQuery = "Update `sms_smpp_service` SET status=1, res_message_id='" + strResMessageID + "'  WHERE  `res_message_id`='" + intID + "'";
                }
                else
                {
                    strQuery = "Update `sms_smpp_service` SET status=0, res_message_id='" + strResMessageID + "'  WHERE  `res_message_id`='" + intID + "'";
                }

                objODBC.executeNonQuery(strQuery);
                return ds;
            }
            catch (Exception)
            {
                return ds;
            }
            finally
            {
                ds.Dispose();
            }
        }



        [WebMethod]
        public string SendSMS_XML(int intUserID, int intCreatedbyType, string strMobileNumber, int intOperaterID, string strMessage, int intStatus, string strSMPPResposne)
        {
            string StrResponse = "NA";
            try
            {
                objcf.WriteToSMSFile(intUserID, intCreatedbyType, strMobileNumber, intOperaterID, strMessage, 0, strSMPPResposne, "NA", "NA", "NA", "NA");
                StrResponse = "Record Inserted Successfully";
            }
            catch (Exception)
            {

            }
            finally
            {

            }
            return StrResponse;
        }


        [WebMethod]
        public string UpdateSendSMS_XML()
        {
            string StrResponse = "NA";
            try
            {
                objcf.UpdateXML();
                StrResponse = "Record Update Successfully";
            }
            catch (Exception)
            {

            }
            finally
            {

            }
            return StrResponse;
        }



        /// <summary>
        /// New SMPP Code 2020-Sep-10
        /// </summary>
        /// <param name="intOperatorID"></param>
        /// <returns></returns> 


        [WebMethod]
        public DataSet er_SMPP_SMS_Service_New(int intOperatorID)
        {
            try
            {
                try
                {
                    ds = objODBCLOG.Report("SELECT `id`, `userid`, `username`, `mobile`, `operator_id`, `SMS`, `created_on`, `status`, `mobile1`, `operator_id1`, `mobile2`, `operator_id2` FROM `er_wallet_transfer_sms_log` WHERE status=0 and operator_id='" + intOperatorID + "' group by SMS order by id asc limit 10;");
                    return ds;
                }
                catch (Exception)
                {
                    ds = null;
                }
                return ds;
            }
            catch (Exception)
            {
                return ds;
            }
            finally
            {
                ds.Dispose();
            }
        }


        // For MTN
        [WebMethod]
        public DataSet er_SMPP_SMS_Service_MTN(int intOperatorID)
        {
            try
            {
                try
                {
                    ds = objODBCLOG.Report("SELECT `id`, `userid`, `username`, `mobile`, `operator_id`, `SMS`, `created_on`, `status`, `mobile1`, `operator_id1`, `mobile2`, `operator_id2` FROM `er_wallet_transfer_sms_log` WHERE status=0 and operator_id='" + intOperatorID + "' group by SMS order by id asc limit 12;");
                    return ds;
                }
                catch (Exception)
                {
                    ds = null;
                }
                return ds;
            }
            catch (Exception)
            {
                return ds;
            }
            finally
            {
                ds.Dispose();
            }
        }


        // For Roshan
        [WebMethod]
        public DataSet er_SMPP_SMS_Service_Roshan(int intOperatorID)
        {
            try
            {
                try
                {
                    ds = objODBCLOG.Report("SELECT `id`, `userid`, `username`, `mobile`, `operator_id`, `SMS`, `created_on`, `status`, `mobile1`, `operator_id1`, `mobile2`, `operator_id2` FROM `er_wallet_transfer_sms_log` WHERE status=0 and operator_id='" + intOperatorID + "' group by SMS order by id asc limit 10;");
                    return ds;
                }
                catch (Exception)
                {
                    ds = null;
                }
                return ds;
            }
            catch (Exception)
            {
                return ds;
            }
            finally
            {
                ds.Dispose();
            }
        }

        [WebMethod]
        public DataSet er_SMPP_SMS_update_New(int intID, string strsmpp_respose, int intUseID, string strUserName, string strSMS, string strOperatorID1, string strMobile1, string strOperatorID2, string strMobile2, string strOperatorID3, string strMobile3, string strOperatorID4, string strMobile4, string strOperatorID5, string strMobile5, string strDileveryDatetime, string strPhoneNumber, string strResMessageID, string strResponseFinalStatus)
        {
            try
            {                
                if (strDileveryDatetime == "NA")
                {
                    strDileveryDatetime = objDT.getCurDateTimeString();
                }
                if (strsmpp_respose == "Bound")
                {
                    try
                    {
                        try
                        {
                            objODBCLOG.executeNonQuery("Update `er_wallet_transfer_sms_log` SET  status=1,smpp_respose='" + strsmpp_respose + "',final_status='" + strResponseFinalStatus + "', `delaverydatetime`='" + strDileveryDatetime + "',res_phonenumber='" + strPhoneNumber + "',res_message_id='" + strResMessageID + "'  WHERE  `id`='" + intID + "'");
                        }
                        catch (Exception)
                        {

                        }
                        try
                        {
                            objODBCLOG.executeNonQuery("INSERT INTO `er_sms_success_log`(id,`userid`,`username`, `mobile`, `operator_id`, `SMS`, `created_on`,`status`, `mobile1`, `operator_id1`, `mobile2`, `operator_id2`,`smpp_respose`, `final_status`, `delaverydatetime`, `res_phonenumber`, `res_message_id`) VALUES ('" + intID + "','" + intUseID + "','" + strUserName + "','" + strMobile1 + "','" + strOperatorID1 + "','" + strSMS + "','" + objDT.getCurDateTimeString() + "',1,'" + strMobile2 + "','" + strOperatorID2 + "','" + strMobile3 + "','" + strOperatorID3 + "','"+ strsmpp_respose + "','"+ strResponseFinalStatus + "','"+ strDileveryDatetime + "','"+ strPhoneNumber + "','"+ strResMessageID + "')");                           
                        }
                        catch (Exception)
                        {

                        }
                    }
                    catch (Exception)
                    {
                        return ds;
                    }
                }
                //else
                //{
                //    if (strMobile2 != "NA" && strMobile3 != "NA")
                //    {
                //        objODBCLOG.executeNonQuery("Update `er_wallet_transfer_sms_log` SET  mobile='" + strMobile3 + "',operator_id='" + strOperatorID3 + "',status=0,mobile2='NA',operator_id2='NA'  WHERE  `id`='" + intID + "'");

                //    }
                //    if (strMobile2 != "NA" && strMobile3 == "NA")
                //    {
                //        objODBCLOG.executeNonQuery("Update `er_wallet_transfer_sms_log` SET  mobile='" + strMobile2 + "',operator_id='" + strOperatorID2 + "',status=0,mobile1='NA',operator_id1='NA' WHERE  `id`='" + intID + "'");                       
                //    }

                //}
                return ds;
            }
            catch (Exception)
            {
                return ds;
            }
            finally
            {
                ds.Dispose();
            }
        }


        // Used if MessageIDStatus = 1 then Send SMS SUccess otherwise if it is 2 then update status to 0
        [WebMethod]
        public DataSet er_SMPP_SMS_Check_New(string intID, string strResMessageID,int MessageIDStatus)
        {
            try
            {
                //try
                //{
                //    objODBC.executeNonQuery("INSERT INTO `temp`(`data`, `type`,  `request`) VALUES ('" + intID + "','" + MessageIDStatus + "','" + strResMessageID + "')");
                //}
                //catch (Exception)
                //{


                //}


                if (MessageIDStatus==1)
                {
                    try
                    {
                        objODBCLOG.executeNonQuery("Update `er_wallet_transfer_sms_log`  SET status=1,res_message_id='" + strResMessageID + "'  WHERE  `id`='" + intID + "'");
                    }
                    catch (Exception)
                    {

                    }
                    try
                    {
                        objODBCLOG.executeNonQuery("Update `er_sms_success_log`  SET status=1,res_message_id='" + strResMessageID + "'  WHERE  `id`='" + intID + "'");
                    }
                    catch (Exception)
                    {

                    }

                }
                else
                {
                    try
                    {
                        objODBCLOG.executeNonQuery("Update `er_wallet_transfer_sms_log`  SET res_message_id='" + strResMessageID + "' WHERE  `id`='" + intID + "'");
                    }
                    catch (Exception)
                    {

                    }
                    try
                    {
                        objODBCLOG.executeNonQuery("Update `er_sms_success_log`  SET res_message_id='" + strResMessageID + "'  WHERE  `id`='" + intID + "'");
                    }
                    catch (Exception)
                    {


                    }
                }
                return ds;
            }
            catch (Exception)
            {
                return ds;
            }
            finally
            {
                ds.Dispose();
            }
        }      

    }
}
