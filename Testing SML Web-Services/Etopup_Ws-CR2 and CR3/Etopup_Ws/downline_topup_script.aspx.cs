﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Etopup_Ws.old_App_Code;
using System.Data;
using System.Collections;

namespace Etopup_Ws
{
    public partial class downline_topup_script : System.Web.UI.Page
    {
        ODBC objODBC = new ODBC();

        CommonFunction objCOMFUN = new CommonFunction();

        DataTable dt_group = new DataTable();

        DataSet dsData = new DataSet();


        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                getDownlineTopup();

            }
            catch (Exception)
            {

            }
            Response.ContentType = "text/plain";
            Response.Write("Exicute Successfully !");
            Response.End();
        }

        public void getDownlineTopup()
        {
            try
            {

                dt_group = objODBC.getDataTable("select id,userid,childid,from_date,to_date from er_member_track_topup where status=0 order by id asc limit 0,1000");

                if (dt_group.Rows.Count > 0)
                {
                    for (int i = 0; i < dt_group.Rows.Count; i++)
                    {
                        string strID = dt_group.Rows[i]["id"].ToString();
                        string strUserID = dt_group.Rows[i]["userid"].ToString();
                        string strDownline = strUserID + "," + dt_group.Rows[i]["childid"].ToString();
                        Response.Write("\n\n Next");
                        Response.Write(strDownline);

                        Response.Write("\n\n Next");
                        string strStartDate = dt_group.Rows[i]["from_date"].ToString();
                        Response.Write("ABG" + strStartDate);
                        string strEndDate = dt_group.Rows[i]["to_date"].ToString();
                        Response.Write("ABGs" + strEndDate);


                        // `Salaam_topup`, `Etisalat_topup`, `Roshan_topup`, `MTN_topup`, `AWCC_topup`

                        double strSalaamDownline = objODBC.executeScalar_dbl("select COALESCE(sum(rM.Salaam_topup),0) from er_member_track_topup rm where (rm.userid in (" + strDownline + ")) and rm.id>'" + strID + "'");

                        double strEtisalatDownline = objODBC.executeScalar_dbl("select COALESCE(sum(rM.Etisalat_topup),0) from er_member_track_topup rm where (rm.userid in (" + strDownline + ")) and rm.id>'" + strID + "'");


                        double strRoshanDownline = objODBC.executeScalar_dbl("select COALESCE(sum(rM.Roshan_topup),0) from er_member_track_topup rm where (rm.userid in (" + strDownline + ")) and rm.id>'" + strID + "'");


                        double strMTNDownline = objODBC.executeScalar_dbl("select COALESCE(sum(rM.MTN_topup),0) from er_member_track_topup rm where (rm.userid in (" + strDownline + ")) and rm.id>'" + strID + "'");


                        double strAWCCDownline = objODBC.executeScalar_dbl("select COALESCE(sum(rM.AWCC_topup),0) from er_member_track_topup rm where (rm.userid in (" + strDownline + ")) and rm.id>'" + strID + "'");

                        objODBC.executeNonQuery("Update er_member_track_topup set status=1,Salaam_downline='" + strSalaamDownline + "',Etisalat_downline='" + strEtisalatDownline + "',Rohsan_downline='" + strRoshanDownline + "',MTN_donwnline='" + strMTNDownline + "',AWCC_downline='" + strAWCCDownline + "' where id=" + strID);
                    }


                }
            }
            catch (Exception)
            {

            }
            finally
            {
                dt_group.Dispose();
            }
        }
    }
}