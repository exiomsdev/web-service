﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Etopup_Ws.old_App_Code;
using System.Web.Services;
using System.Collections;
using System.Data;
using System.Text.RegularExpressions;

namespace Etopup_Ws
{
    /// <summary>
    /// Summary description for MemberManager
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class MemberManager : System.Web.Services.WebService
    {
        ODBC objODBC = new ODBC();
        ODBC_LOG objODBCLOG = new ODBC_LOG();
        DataTable dt = new DataTable();
        DataTable dt2 = new DataTable();
        DateFormat objDT = new DateFormat();
        Encrypt_Decrypt ED = new Encrypt_Decrypt();
        CreateFile objCreateFile = new CreateFile();
        CommonFunction objcf = new CommonFunction();


        public ArrayList getReport(string strQuery)
        {
            ODBC obj = new ODBC();
            ArrayList arrUserParams = new ArrayList();
            try
            {
                System.Data.DataSet ds = new System.Data.DataSet();
                try
                {
                    ds = obj.getDataSet(strQuery);

                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        arrUserParams.Add("TRUE");

                        System.Data.DataTable firstTable = ds.Tables[0];
                        arrUserParams.Add(firstTable.Rows.Count);
                        arrUserParams.Add(firstTable.Columns.Count);
                        for (int i = 0; i <= firstTable.Rows.Count - 1; i++)
                        {
                            for (int j = 0; j <= firstTable.Columns.Count - 1; j++)
                                arrUserParams.Add(firstTable.Rows[i][j].ToString());
                        }
                    }
                    else
                        arrUserParams.Add("FALSE");

                    return arrUserParams;
                }
                catch (Exception ex)
                {
                    arrUserParams.Add(ex.Message.ToString());

                    return arrUserParams;
                }

                finally
                {
                    ds.Dispose();
                }
            }


            catch (Exception ex)
            {
                arrUserParams.Add(ex.Message.ToString());

                return arrUserParams;
            }
        }


        // ActivityType=58
        [WebMethod]
        public ArrayList ReturnAllSuperAdminUsersV3(string strUserName, int intUserID, string strSearch, int intPage, int PageSize, string strIPAddress, int intDesignationType, string strMacAddress, string strOSDetails, string strIMEINo, string strGcmID, string strAPPVersion, int intSource, int intCreatedByType)
        {
            {
                string strQuery = "";
                string strLimit = "";
                ArrayList arrUserParams = new ArrayList();

                try
                {

                    if (intCreatedByType == 1)
                    {
                        strLimit = " LIMIT " + PageSize * (intPage - 1) + "," + PageSize + "";
                        strQuery = "SELECT  a.userid,a.emailid,a.mobile,a.full_name,a.username,a.created_on,a.last_login_ip,a.last_login_datetime,a.user_status,b.ex_wallet,a.password,a.usertype_id,a.province_Name,a.city_name,a.Address,a.gender From er_login a inner join er_wallet b On a.userid = b.userid Where a.Active=1 " + strSearch + " ORDER BY a.userid DESC " + strLimit;
                    }
                    else
                    {

                        strLimit = " LIMIT " + PageSize * (intPage - 1) + "," + PageSize + "";
                        strQuery = "SELECT  a.userid,a.emailid,a.mobile,a.full_name,a.username,a.created_on,a.last_login_ip,a.last_login_datetime,a.user_status,b.ex_wallet,a.password,a.usertype_id,a.province_Name,a.city_name,a.Address,a.gender From er_login a inner join er_wallet b On a.userid = b.userid Where a.Active=1 and a.reseller_id=" + intUserID + " " + strSearch + " ORDER BY a.userid DESC " + strLimit;
                    }



                    objCreateFile.CreateTextFile(strIPAddress, strMacAddress, strOSDetails, strIMEINo, strGcmID, strAPPVersion, intSource.ToString(), intCreatedByType, 58, " Return All Super Admin Users V3 ", strUserName);
                    arrUserParams = getReport(strQuery);
                    return arrUserParams;
                }
                catch (Exception ex)
                {
                    return arrUserParams;
                }
            }
        }

        // ActivityType=60
        [WebMethod]
        public DataSet AllSuperAdminUsersV3(string strUserName, int intUserID, string strSearch, int intPage, int PageSize, string strIPAddress, int intDesignationType, string strMacAddress, string strOSDetails, string strIMEINo, string strGcmID, string strAPPVersion, int intSource, int intCreatedByType)
        {
            {
                string strQuery = "";
                string strLimit = "";
                DataSet ds = new DataSet();

                try
                {

                    if (intCreatedByType == 1)
                    {
                        string strCount = objODBC.executeScalar_str("select count(1) From er_login a inner join er_wallet b On a.userid = b.userid Where a.Active=1 and a.userid!=1 " + strSearch + "");
                        //" + strCount + "
                        strLimit = " LIMIT " + PageSize * (intPage - 1) + "," + PageSize + "";
                        strQuery = "SELECT  a.userid,a.username,a.emailid,(SELECT mobile FROM `er_mobile` where userid=b.userid and mob_type=1 and usertype=2 order by id asc limit 1 ) as mobile,a.full_name,a.username,a.created_on,a.last_login_ip,a.last_login_datetime,a.user_status,b.ex_wallet,a.password,a.usertype_id,b.min_wallet,a.province_Name,a.city_name,a.Address,a.gender, case when a.gender=1 then 'Male' when a.gender=2 then 'Female' when a.gender=0 then 'Other' end as gender_text, case when a.usertype_id=3 then 'Distributor' when a.usertype_id=4 then 'Sub-Distributor' when a.usertype_id=5 then 'Retailer' end as Utype,status_authority,a.add_user_rights,a.commission_rights,a.bulk_transfer,a.group_topup,a.fource_logout,a.M_Pin as Mpin,a.update_mobile_status,date_format(a.created_on,'%d %b %Y %H:%i:%s') as reg_date,a.ussd_top_status," + strCount + " as intCount From er_login a inner join er_wallet b On a.userid = b.userid Where a.Active=1 and a.userid!=1 " + strSearch + " ORDER BY a.userid DESC " + strLimit;
                    }
                    else if (intCreatedByType == 2)
                    {
                        if (intDesignationType == 3)
                        {
                            string strDownline = objODBC.executeScalar_str("SELECT child_id FROM er_login WHERE userid='" + intUserID + "'");

                            // string strCount = objODBC.executeScalar_str("select count(1) From er_login a inner join er_wallet b On a.userid = b.userid Where a.Active=1  and a.userid!=1 and a.userid in (" + strDownline + ") " + strSearch);
                            string strAuthstatus = objODBC.executeScalar_str("select a.status_authority From er_login a  Where a.Active=1  and a.userid=" + intUserID + "");

                            strLimit = " LIMIT " + PageSize * (intPage - 1) + "," + PageSize + "";
                            strQuery = "SELECT  a.userid,a.username,a.emailid,(SELECT mobile FROM `er_mobile` where userid=b.userid and mob_type=1 and usertype=2 order by id asc limit 1 ) as mobile,a.full_name,a.username,a.created_on,a.last_login_ip,a.last_login_datetime,a.user_status,b.ex_wallet,a.password,a.usertype_id,b.min_wallet,a.province_Name,a.city_name,a.Address,a.gender, case when a.gender=1 then 'Male' when a.gender=2 then 'Female' when a.gender=0 then 'Other' end as gender_text,case when a.usertype_id=3 then 'Distributor' when a.usertype_id=4 then 'Sub-Distributor' when a.usertype_id=5 then 'Retailer' end as Utype,status_authority," + strAuthstatus + " as strAuthstatus,a.add_user_rights,a.commission_rights,a.bulk_transfer,a.group_topup,a.fource_logout,(select (CHAR_LENGTH(child_id) -CHAR_LENGTH(REPLACE(child_id, ',', '')) + 1) from er_login where userid='" + intUserID + "') as intCount From er_login a inner join er_wallet b On a.userid = b.userid Where a.Active=1 and a.userid in (" + strDownline + ") and a.userid!=1 " + strSearch + " ORDER BY a.userid DESC " + strLimit;

                            // sub_distributor_id=0

                        }

                        if (intDesignationType == 4)
                        {
                            string strDownline = objODBC.executeScalar_str("SELECT child_id FROM er_login WHERE userid='" + intUserID + "'");


                            //  string strCount = objODBC.executeScalar_str("select count(1) From er_login a inner join er_wallet b On a.userid = b.userid Where a.Active=1  and a.userid!=1 and a.userid in ('" + strDownline + "') " + strSearch);

                            string strAuthstatus = objODBC.executeScalar_str("select a.status_authority From er_login a  Where a.Active=1  and a.userid=" + intUserID + "");

                            strLimit = " LIMIT " + PageSize * (intPage - 1) + "," + PageSize + "";
                            strQuery = "SELECT  a.userid,a.username,a.emailid,(SELECT mobile FROM `er_mobile` where userid=b.userid and mob_type=1 and usertype=2 order by id asc limit 1 ) as mobile,a.full_name,a.username,a.created_on,a.last_login_ip,a.last_login_datetime,a.user_status,b.ex_wallet,a.password,a.usertype_id,b.min_wallet,a.province_Name,a.city_name,a.Address,a.gender, case when a.gender=1 then 'Male' when a.gender=2 then 'Female' when a.gender=0 then 'Other' end as gender_text,case when a.usertype_id=3 then 'Distributor' when a.usertype_id=4 then 'Sub-Distributor' when a.usertype_id=5 then 'Retailer' end as Utype,status_authority as strAuthstatus," + strAuthstatus + ",a.add_user_rights,a.commission_rights,a.bulk_transfer,a.group_topup,a.fource_logout,(select (CHAR_LENGTH(child_id) -CHAR_LENGTH(REPLACE(child_id, ',', '')) + 1) from er_login where userid='" + intUserID + "') as intCount From er_login a inner join er_wallet b On a.userid = b.userid Where a.Active=1 and a.userid in (" + strDownline + ") and a.userid!=1 " + strSearch + " ORDER BY a.userid DESC " + strLimit;
                        }
                    }

                    objCreateFile.CreateTextFile(strIPAddress, strMacAddress, strOSDetails, strIMEINo, strGcmID, strAPPVersion, intSource.ToString(), intCreatedByType, 60, " All Super Admin Users V3", strUserName);
                    ds = objODBC.Report(strQuery);
                    return ds;
                }
                catch (Exception ex)
                {
                    return ds;
                }
            }
        }



        // Activity Type=61
        [WebMethod]
        public ArrayList MemberStatus(string strUserName, int intUserID, int intStatusType, string strIPAddress, int intDesignationType, string strMacAddress, string strOSDetails, string strIMEINo, string strGcmID, string strAPPVersion, int intSource, int intCreatedByType, int intUType)
        {
            ArrayList arrUserParams = new ArrayList();
            string strResponseMessage = "";
            int intActivityType = 61;
            try
            {
                if ((intUserID > 0) && (intStatusType == 1 || intStatusType == 2) && (intUType != 0))
                {
                    if (intUType == 1)
                    {
                        int count = objODBC.executeNonQuery("update er_login_admin SET status='" + intStatusType + "'  where userid='" + intUserID + "'");
                        if (count > 0)
                        {
                            strResponseMessage = "Sub Admin Status Update Sucessfully";

                            arrUserParams.Add("TRUE");
                            arrUserParams.Add(strResponseMessage);

                            strResponseMessage = strResponseMessage + " = " + intStatusType;
                        }
                        else
                        {

                            if (intStatusType == 1)
                            {
                                strResponseMessage = "Sub Admin Status Already Active";
                                arrUserParams.Add("False");
                                arrUserParams.Add(strResponseMessage);
                            }
                            else
                            {
                                strResponseMessage = "Sub Admin Status Already DeActive ";
                                arrUserParams.Add("False");
                                arrUserParams.Add(strResponseMessage);
                            }
                        }
                    }
                    else
                    {


                        int count = objODBC.executeNonQuery("update er_login SET user_status='" + intStatusType + "'  where userid='" + intUserID + "'");
                        if (count > 0)
                        {
                            objODBC.executeNonQuery("update er_login set fource_logout=1 where userid =" + intUserID + "");
                            strResponseMessage = "Member Status Update Sucessfully";
                            arrUserParams.Add("TRUE");
                            arrUserParams.Add(strResponseMessage);

                            strResponseMessage = strResponseMessage + " = " + intStatusType;
                        }
                        else
                        {
                            if (intStatusType == 1)
                            {
                                strResponseMessage = "Member Status Already Active";
                                arrUserParams.Add("False");
                                arrUserParams.Add(strResponseMessage);
                            }
                            else
                            {
                                strResponseMessage = "Member Status Already DeActive";
                                arrUserParams.Add("False");
                                arrUserParams.Add(strResponseMessage);
                            }
                        }
                    }
                }
                else
                {
                    strResponseMessage = "Member Status Update Fail, Kindly Enter Valid Details";
                    arrUserParams.Add("False");
                    arrUserParams.Add(strResponseMessage);
                }

            }
            catch (Exception e1)
            {
                strResponseMessage = "Member Status, Invalid Details" + e1.ToString();
                arrUserParams.Add("False");
                arrUserParams.Add(strResponseMessage);
            }
            finally
            {
                objODBCLOG.executeNonQuery("call activity_logs('" + intUserID + "','" + intDesignationType + "','" + intCreatedByType + "','" + strIPAddress + "','" + strMacAddress + "','" + strOSDetails + "','" + strIMEINo + "','" + strGcmID + "','" + strAPPVersion + "','" + intSource + "','Modify Member Active Status','" + intActivityType + "','','')");

                objCreateFile.CreateTextFile(strIPAddress, strMacAddress, strOSDetails, strIMEINo, strGcmID, strAPPVersion, intSource.ToString(), intCreatedByType, intActivityType, strResponseMessage, strUserName);

            }
            return arrUserParams;
        }

        // int intActivityType = 69;
        [WebMethod]
        public DataSet ws_sms_manager(string strUserName, int intUserID, int intOperatorID, string strMobile, int IntAction, string strSearch, int intPage, int intPageSize, string strIPAddress, int intDesignationType, string strMacAddress, string strOSDetails, string strIMEINo, string strGcmID, string strAPPVersion, int intSource, int intCreatedByType)
        {
            int intActivityType = 69;
            DataSet ds = new DataSet();
            string strMessage = "SMS Setting";
            string strLimit = string.Empty;
            string strOperatorName = string.Empty;
            try
            {
                objODBCLOG.executeNonQuery("call activity_logs('" + intUserID + "','" + intDesignationType + "','" + intCreatedByType + "','" + strIPAddress + "','" + strMacAddress + "','" + strOSDetails + "','" + strIMEINo + "','" + strGcmID + "','" + strAPPVersion + "','" + intSource + "','SMS Setting','" + intActivityType + "','','')");

                objCreateFile.CreateTextFile(strIPAddress, strMacAddress, strOSDetails, strIMEINo, strGcmID, strAPPVersion, intSource.ToString(), intCreatedByType, intActivityType, strMessage, strUserName);
            }
            catch (Exception)
            {

            }
            if (intOperatorID >= 0 && intOperatorID < 6 && strMobile != string.Empty && strMobile != "" && IntAction > 0)
            {
                try
                {
                    if (intOperatorID == 1)
                    {
                        strOperatorName = "Salaam";
                    }
                    else if (intOperatorID == 2)
                    {
                        strOperatorName = "Etisalat";
                    }
                    else if (intOperatorID == 3)
                    {
                        strOperatorName = "Roshan";
                    }
                    else if (intOperatorID == 4)
                    {
                        strOperatorName = "MTN";
                    }
                    else if (intOperatorID == 5)
                    {
                        strOperatorName = "AWCC";

                    }

                    if (IntAction != 3)
                    {
                        if (strMobile.Length < 9)
                        {
                            strMessage = "Please enter valid mobile";
                            ds = objODBC.getresult(strMessage);
                            return ds;
                        }
                    }

                    if (IntAction == 1) // delete
                    {
                        string strQuery = "select count(1) from er_mobile_manager where mobile = '" + strMobile + "'";
                        int chkGroupIDCount = objODBC.executeScalar_int(strQuery);
                        if (chkGroupIDCount > 0)
                        {
                            string strQuery1 = "delete from er_mobile_manager where mobile = '" + strMobile + "'";
                            objODBC.executeNonQuery(strQuery1);
                            strMessage = "mobile number deleted successfully";
                            ds = er_getresult(strMessage);
                        }
                        else
                        {
                            strMessage = "Please enter valid mobile number";
                            ds = getresult(strMessage);
                        }
                    }
                    else if (IntAction == 2)// insert
                    {
                        string strCheck_Mobile = objcf.CheckRegixMobile(intOperatorID, strMobile);
                        if (strCheck_Mobile == "TRUE")
                        {
                            string strQuery = "select count(1) from er_mobile_manager where mobile = '" + strMobile + "'";
                            int chkGroupIDCount = objODBC.executeScalar_int(strQuery);
                            if (chkGroupIDCount == 0)
                            {
                                string strDate = objDT.getCurDateTimeString();
                                string strQuery1 = "insert into er_mobile_manager(op_id,op_name,mobile,created_on) values(" + intOperatorID + ",'" + strOperatorName.ToString() + "','" + strMobile.ToString() + "','" + strDate.ToString() + "')";
                                objODBC.executeNonQuery(strQuery1);
                                strMessage = "Mobile number inserted successfully";
                                ds = er_getresult(strMessage);
                            }
                            else
                            {
                                strMessage = "mobile Already exists";
                                ds = getresult(strMessage);
                            }
                        }
                        else
                        {
                            strMessage = "Mobile Number does not belongs to existing Operator";
                            ds = getresult(strMessage);
                        }

                    }
                    else if (IntAction == 3)//select
                    {
                        strLimit = " LIMIT " + intPageSize * (intPage - 1) + "," + intPageSize + "";
                        string strQuery = "select *,(select count(1) From er_mobile_manager  where id>0 " + strSearch + ") as intCount from er_mobile_manager where id>0 " + strSearch + "  order by id " + strLimit;
                        ds = Report(strQuery);
                    }
                    else if (IntAction == 4)//update
                    {
                        // if this then add one more param id 
                    }


                }
                catch (Exception)
                {
                    strMessage = "somthing went wrong";
                    ds = getresult(strMessage);

                }
            }
            else
            {
                strMessage = "please send valid details";
                ds = getresult(strMessage);
            }



            return ds;
        }

        // int intActivityType = 68;
        [WebMethod]
        public DataSet ws_email_manager(string strUserName, int intUserID, string strEmail, int IntAction, string strSearch, int intPage, int intPageSize, string strIPAddress, int intDesignationType, string strMacAddress, string strOSDetails, string strIMEINo, string strGcmID, string strAPPVersion, int intSource, int intCreatedByType)
        {
            int intActivityType = 68;
            DataSet ds = new DataSet();
            string strMessage = string.Empty;
            string strLimit = string.Empty;
            try
            {
                objODBCLOG.executeNonQuery("call activity_logs('" + intUserID + "','" + intDesignationType + "','" + intCreatedByType + "','" + strIPAddress + "','" + strMacAddress + "','" + strOSDetails + "','" + strIMEINo + "','" + strGcmID + "','" + strAPPVersion + "','" + intSource + "','Email Setting','" + intActivityType + "','','')");

                objCreateFile.CreateTextFile(strIPAddress, strMacAddress, strOSDetails, strIMEINo, strGcmID, strAPPVersion, intSource.ToString(), intCreatedByType, intActivityType, "Email Setting", strUserName);

            }
            catch (Exception)
            {

            }
            if (strEmail != string.Empty && strEmail != "" && IntAction > 0)
            {
                try
                {
                    if (IntAction != 3)
                    {
                        Regex regex = new Regex(@"^([\w\.\-]+)@([\w\-]+)((\.(\w){2,3})+)$");
                        Match match = regex.Match(strEmail);
                        if (!match.Success)
                        {
                            strMessage = "Please enter valid email id";
                            ds = getresult(strMessage);
                            return ds;
                        }
                    }

                    if (IntAction == 1) // delete
                    {
                        string strQuery = "select count(1) from er_email_manager where email = '" + strEmail + "'";
                        int chkGroupIDCount = objODBC.executeScalar_int(strQuery);
                        if (chkGroupIDCount > 0)
                        {
                            string strQuery1 = "delete from er_email_manager where email = '" + strEmail + "'";
                            objODBC.executeNonQuery(strQuery1);
                            strMessage = "Email deleted successfully";
                            ds = er_getresult(strMessage);
                        }
                        else
                        {
                            strMessage = "Please enter valid email id";
                            ds = getresult(strMessage);
                        }
                    }
                    else if (IntAction == 2)// insert
                    {
                        string strQuery = "select count(1) from er_email_manager where email = '" + strEmail + "'";
                        int chkGroupIDCount = objODBC.executeScalar_int(strQuery);
                        if (chkGroupIDCount == 0)
                        {
                            string strDate = objDT.getCurDateTimeString();
                            string strQuery1 = "insert into er_email_manager(email,created_on) values('" + strEmail.ToString() + "','" + strDate.ToString() + "')";
                            objODBC.executeNonQuery(strQuery1);
                            strMessage = "Email added successfully";
                            ds = er_getresult(strMessage);
                        }
                        else
                        {
                            strMessage = "Email Already exists";
                            ds = getresult(strMessage);
                        }

                    }
                    else if (IntAction == 3)//select
                    {
                        strLimit = " LIMIT " + intPageSize * (intPage - 1) + "," + intPageSize + "";
                        string strQuery = "select *,(select count(1) From er_email_manager where id>0 " + strSearch + ") as intCount from er_email_manager where id>0 " + strSearch + "  order by email " + strLimit;
                        ds = Report(strQuery);
                    }
                    else if (IntAction == 4)//update
                    {
                        // if this then add one more param id 
                    }


                }
                catch (Exception)
                {
                    strMessage = "somthing went wrong";
                    ds = getresult(strMessage);

                }
            }
            else
            {
                strMessage = "please send valid details";
                ds = getresult(strMessage);
            }

            return ds;
        }

        // int intActivityType=123;
        [WebMethod]
        public DataSet AllDistUsersV3(string strUserName, int intUserID, string strSearch, int intPage, int PageSize, string strIPAddress, int intDesignationType, string strMacAddress, string strOSDetails, string strIMEINo, string strGcmID, string strAPPVersion, int intSource, int intCreatedByType)
        {
            {
                string strQuery = "";
                string strLimit = "";
                DataSet ds = new DataSet();

                try
                {

                    if (intDesignationType == 3)
                    {
                        strLimit = " LIMIT " + PageSize * (intPage - 1) + "," + PageSize + "";
                        string strCount = objODBC.executeScalar_str("select count(1) From er_login a inner join er_wallet b On a.userid = b.userid Where a.Active=1 and a.distributor_id=" + intUserID + " " + strSearch);
                        //" + strCount + "
                        strQuery = "SELECT  a.userid,a.username,a.emailid,(SELECT mobile FROM `er_mobile` where userid=a.userid  and mob_type=1 and usertype=2 order by id asc limit 1 ) as mobile,a.full_name,a.created_on,a.last_login_ip,a.last_login_datetime,a.user_status,b.ex_wallet,a.usertype_id,b.min_wallet,a.province_Name,a.city_name,a.Address,a.gender, case when a.gender=1 then 'Male' when a.gender=2 then 'Female' when a.gender=0 then 'Other' end as gender_text, case when a.usertype_id=3 then 'Distributor' when a.usertype_id=4 then 'Sub-Distributor' when a.usertype_id=5 then 'Retailer' end as Utype," + strCount + " as intCount From er_login a inner join er_wallet b On a.userid = b.userid Where a.Active=1 and a.distributor_id=" + intUserID + " " + strSearch + " ORDER BY a.userid DESC " + strLimit;
                    }
                    else if (intDesignationType == 4)
                    {

                        strLimit = " LIMIT " + PageSize * (intPage - 1) + "," + PageSize + "";
                        string strCount = objODBC.executeScalar_str("select count(1) From er_login a inner join er_wallet b On a.userid = b.userid Where a.Active=1  and a.parent_id=" + intUserID + " " + strSearch);
                        strQuery = "SELECT  a.userid,a.username,a.emailid,(SELECT mobile FROM `er_mobile` where userid=a.userid  and mob_type=1 and usertype=2 order by id asc limit 1 ) as mobile,a.full_name,a.created_on,a.last_login_ip,a.last_login_datetime,a.user_status,b.ex_wallet,a.usertype_id,b.min_wallet,a.province_Name,a.city_name,a.Address,a.gender, case when a.gender=1 then 'Male' when a.gender=2 then 'Female' when a.gender=0 then 'Other' end as gender_text,case when a.usertype_id=3 then 'Distributor' when a.usertype_id=4 then 'Sub-Distributor' when a.usertype_id=5 then 'Retailer' end as Utype," + strCount + " as intCount From er_login a inner join er_wallet b On a.userid = b.userid Where a.Active=1 and a.parent_id=" + intUserID + " " + strSearch + " ORDER BY a.userid DESC " + strLimit;
                    }

                    try
                    {
                        objCreateFile.CreateTextFile(strIPAddress, strMacAddress, strOSDetails, strIMEINo, strGcmID, strAPPVersion, intSource.ToString(), intCreatedByType, 123, " All Dist Users V3", strUserName);
                    }
                    catch (Exception)
                    {

                    }
                    ds = objODBC.Report(strQuery);
                    return ds;
                }
                catch (Exception ex)
                {
                    return ds;
                }
            }
        }

        [WebMethod]
        public ArrayList er_android_AllDistUsersV3(string strUserName, int intUserID, string strSearch, int intPage, int PageSize, string strIPAddress, int intDesignationType, string strMacAddress, string strOSDetails, string strIMEINo, string strGcmID, string strAPPVersion, int intSource, int intCreatedByType)
        {
            ArrayList arrlst = new ArrayList();
            DataSet ds = new DataSet();

            try
            {

                ds = AllDistUsersV3(strUserName, intUserID, strSearch, intPage, PageSize, strIPAddress, intDesignationType, strMacAddress, strOSDetails, strIMEINo, strGcmID, strAPPVersion, intSource, intCreatedByType);

                arrlst = objcf.Get_ArrayLlist_From_Dataset(ds);

            }
            catch (Exception)
            {


            }
            finally
            {

            }
            return arrlst;

        }


        public DataSet Report(string strQuery)
        {
            ODBC obj = new ODBC();
            System.Data.DataSet ds = new System.Data.DataSet();

            try
            {
                ds = obj.getDataSet(strQuery);

                if (ds.Tables[0].Rows.Count > 0)
                {
                    return ds;
                }
                else
                {
                    return objODBC.getresult("Records not found !!!");
                }

            }
            catch (Exception ex)
            {
                return objODBC.getresult(ex.Message.ToString());
            }
            finally
            {
                ds.Dispose();
            }
        }



        public DataSet er_getresult(string stresult)
        {
            System.Data.DataSet ds = new System.Data.DataSet();
            try
            {
                DataTable dt = new DataTable("Data");
                dt.Columns.Add(new DataColumn("Status", typeof(string)));
                dt.Columns.Add(new DataColumn("Result", typeof(string)));

                DataRow dr = dt.NewRow();
                dr[0] = "True";
                dr[1] = stresult;
                dt.Rows.Add(dr);
                ds.Tables.Add(dt);
                return ds;
            }
            catch (Exception ex)
            {
                DataTable dt = new DataTable("Data");
                dt.Columns.Add(new DataColumn("Status", typeof(string)));
                dt.Columns.Add(new DataColumn("Result", typeof(string)));

                DataRow dr = dt.NewRow();
                dr[0] = "False";
                dr[1] = ex.Message.ToString();
                dt.Rows.Add(dr);
                ds.Tables.Add(dt);
                return ds;
            }
            finally
            {
                ds.Dispose();
            }
        }

        public DataSet getresult(string stresult)
        {
            System.Data.DataSet ds = new System.Data.DataSet();
            try
            {
                DataTable dt = new DataTable("Data");
                dt.Columns.Add(new DataColumn("Status", typeof(string)));
                dt.Columns.Add(new DataColumn("Result", typeof(string)));

                DataRow dr = dt.NewRow();
                dr[0] = "False";
                dr[1] = stresult;
                dt.Rows.Add(dr);
                ds.Tables.Add(dt);
                return ds;
            }
            catch (Exception ex)
            {
                DataTable dt = new DataTable("Data");
                dt.Columns.Add(new DataColumn("Status", typeof(string)));
                dt.Columns.Add(new DataColumn("Result", typeof(string)));

                DataRow dr = dt.NewRow();
                dr[0] = "False";
                dr[1] = ex.Message.ToString();
                dt.Rows.Add(dr);
                ds.Tables.Add(dt);
                return ds;
            }
            finally
            {
                ds.Dispose();
            }
        }


        [WebMethod]
        public ArrayList GetTreeDetails(string strUserName, int intUserID, string strIPAddress, int intDesignationType, string strMacAddress, string strOSDetails, string strIMEINo, string strGcmID, string strAPPVersion, int intSource, int intCreatedByType)
        {
            DataSet ds = new DataSet();
            ArrayList arrUserParams = new ArrayList();

            int intActivityType = 61;
            try
            {
                objCreateFile.CreateTextFile(strIPAddress, strMacAddress, strOSDetails, strIMEINo, strGcmID, strAPPVersion, intSource.ToString(), intCreatedByType, intActivityType, "Get Tree Details,Data:" + intUserID, strUserName);
            }
            catch (Exception)
            {

            }
            try
            {
                if ((intUserID > 0))
                {

                    ds = objODBC.getDataSet("select parent_id,reseller_id,distributor_id,sub_distributor_id,full_name,username,usertype_id from er_login where userid='" + intUserID + "'");
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        int intParentID = int.Parse(ds.Tables[0].Rows[0][0].ToString());
                        int intResellerID = int.Parse(ds.Tables[0].Rows[0][1].ToString());
                        int intDistributorID = int.Parse(ds.Tables[0].Rows[0][2].ToString());
                        int intSubDistributorID = int.Parse(ds.Tables[0].Rows[0][3].ToString());
                        string strMFullName = ds.Tables[0].Rows[0][4].ToString();
                        string strMUserID = ds.Tables[0].Rows[0][5].ToString();
                        int intUserType = int.Parse(ds.Tables[0].Rows[0][6].ToString());

                        if (intParentID == 1)
                        {
                            DataTable dt = new DataTable();

                            try
                            {
                                dt = objODBC.getDataTable("Select a.full_name as AdminName,a.username as AdminUserID from er_login_admin a where a.userid='" + intParentID + "'");
                                if (dt.Rows.Count > 0)
                                {
                                    arrUserParams.Add("TRUE");
                                    arrUserParams.Add(dt.Rows[0][0].ToString());
                                    arrUserParams.Add(dt.Rows[0][1].ToString());


                                    if (intUserType == 3)
                                    {
                                        arrUserParams.Add(strMFullName);
                                        arrUserParams.Add(strMUserID);
                                        arrUserParams.Add("NA");
                                        arrUserParams.Add("NA");
                                        arrUserParams.Add("NA");
                                        arrUserParams.Add("NA");
                                    }
                                    else if (intUserType == 4)
                                    {
                                        arrUserParams.Add("NA");
                                        arrUserParams.Add("NA");
                                        arrUserParams.Add(strMFullName);
                                        arrUserParams.Add(strMUserID);
                                        arrUserParams.Add("NA");
                                        arrUserParams.Add("NA");
                                    }
                                    else if (intUserType == 5)
                                    {
                                        arrUserParams.Add("NA");
                                        arrUserParams.Add("NA");
                                        arrUserParams.Add("NA");
                                        arrUserParams.Add("NA");
                                        arrUserParams.Add(strMFullName);
                                        arrUserParams.Add(strMUserID);
                                    }


                                }
                                else
                                {
                                    arrUserParams.Add("FALSE");
                                    arrUserParams.Add("NA");
                                    arrUserParams.Add("NA");
                                    arrUserParams.Add("NA");
                                    arrUserParams.Add("NA");
                                    arrUserParams.Add("NA");
                                    arrUserParams.Add("NA");
                                    arrUserParams.Add("NA");
                                    arrUserParams.Add("NA");

                                }
                            }
                            catch (Exception)
                            {
                                arrUserParams.Add("FALSE");
                                arrUserParams.Add("NA");
                                arrUserParams.Add("NA");
                                arrUserParams.Add("NA");
                                arrUserParams.Add("NA");
                                arrUserParams.Add("NA");
                                arrUserParams.Add("NA");
                                arrUserParams.Add("NA");
                                arrUserParams.Add("NA");
                            }
                            finally
                            {
                                dt.Dispose();
                            }
                        }
                        else
                        {
                            if (intSubDistributorID == 0)
                            {
                                DataTable dt = new DataTable();
                                DataTable dt2 = new DataTable();
                                try
                                {
                                    dt = objODBC.getDataTable("Select a.full_name as AdminName,a.username as AdminUserID from er_login_admin a where a.userid=1");
                                    if (dt.Rows.Count > 0)
                                    {
                                        arrUserParams.Add("TRUE");
                                        arrUserParams.Add(dt.Rows[0][0].ToString());
                                        arrUserParams.Add(dt.Rows[0][1].ToString());

                                        dt2 = objODBC.getDataTable("Select full_name,username from er_login where userid='" + intDistributorID + "'");
                                        if (dt2.Rows.Count > 0)
                                        {
                                            arrUserParams.Add(dt2.Rows[0][0].ToString());
                                            arrUserParams.Add(dt2.Rows[0][1].ToString());
                                        }
                                        else
                                        {
                                            arrUserParams.Add("NA");
                                            arrUserParams.Add("NA");
                                        }
                                        arrUserParams.Add("NA");
                                        arrUserParams.Add("NA");
                                        arrUserParams.Add(strMFullName);
                                        arrUserParams.Add(strMUserID);
                                    }
                                    else
                                    {
                                        arrUserParams.Add("FALSE");
                                        arrUserParams.Add("NA");
                                        arrUserParams.Add("NA");
                                        arrUserParams.Add("NA");
                                        arrUserParams.Add("NA");
                                        arrUserParams.Add("NA");
                                        arrUserParams.Add("NA");
                                        arrUserParams.Add("NA");
                                        arrUserParams.Add("NA");

                                    }
                                }
                                catch (Exception)
                                {
                                    arrUserParams.Add("FALSE");
                                    arrUserParams.Add("NA");
                                    arrUserParams.Add("NA");
                                    arrUserParams.Add("NA");
                                    arrUserParams.Add("NA");
                                    arrUserParams.Add("NA");
                                    arrUserParams.Add("NA");
                                    arrUserParams.Add("NA");
                                    arrUserParams.Add("NA");
                                }
                                finally
                                {
                                    dt.Dispose();
                                    dt2.Dispose();
                                }

                            }
                            else
                            {

                                DataTable dt = new DataTable();
                                DataTable dt2 = new DataTable();
                                DataTable dt3 = new DataTable();
                                try
                                {
                                    dt = objODBC.getDataTable("Select a.full_name as AdminName,a.username as AdminUserID from er_login_admin a where a.userid=1");
                                    if (dt.Rows.Count > 0)
                                    {
                                        arrUserParams.Add("TRUE");
                                        arrUserParams.Add(dt.Rows[0][0].ToString());
                                        arrUserParams.Add(dt.Rows[0][1].ToString());

                                        dt2 = objODBC.getDataTable("Select full_name,username from er_login where userid='" + intDistributorID + "'");
                                        if (dt2.Rows.Count > 0)
                                        {
                                            arrUserParams.Add(dt2.Rows[0][0].ToString());
                                            arrUserParams.Add(dt2.Rows[0][1].ToString());
                                        }
                                        else
                                        {
                                            arrUserParams.Add("NA");
                                            arrUserParams.Add("NA");
                                        }
                                        dt3 = objODBC.getDataTable("Select full_name,username from er_login where userid='" + intSubDistributorID + "'");
                                        if (dt3.Rows.Count > 0)
                                        {
                                            arrUserParams.Add(dt3.Rows[0][0].ToString());
                                            arrUserParams.Add(dt3.Rows[0][1].ToString());
                                        }
                                        else
                                        {
                                            arrUserParams.Add("NA");
                                            arrUserParams.Add("NA");
                                        }
                                        arrUserParams.Add(strMFullName);
                                        arrUserParams.Add(strMUserID);
                                    }
                                    else
                                    {
                                        arrUserParams.Add("FALSE");
                                        arrUserParams.Add("NA");
                                        arrUserParams.Add("NA");
                                        arrUserParams.Add("NA");
                                        arrUserParams.Add("NA");
                                        arrUserParams.Add("NA");
                                        arrUserParams.Add("NA");
                                        arrUserParams.Add("NA");
                                        arrUserParams.Add("NA");

                                    }
                                }
                                catch (Exception)
                                {
                                    arrUserParams.Add("FALSE");
                                    arrUserParams.Add("NA");
                                    arrUserParams.Add("NA");
                                    arrUserParams.Add("NA");
                                    arrUserParams.Add("NA");
                                    arrUserParams.Add("NA");
                                    arrUserParams.Add("NA");
                                    arrUserParams.Add("NA");
                                    arrUserParams.Add("NA");
                                }
                                finally
                                {
                                    dt.Dispose();
                                    dt2.Dispose();
                                    dt3.Dispose();
                                }
                            }
                        }

                    }
                    else
                    {
                        arrUserParams.Add("FALSE");
                        arrUserParams.Add("NA");
                        arrUserParams.Add("NA");
                        arrUserParams.Add("NA");
                        arrUserParams.Add("NA");
                        arrUserParams.Add("NA");
                        arrUserParams.Add("NA");
                        arrUserParams.Add("NA");
                        arrUserParams.Add("NA");
                    }
                }
                else
                {
                    arrUserParams.Add("FALSE");
                    arrUserParams.Add("NA");
                    arrUserParams.Add("NA");
                    arrUserParams.Add("NA");
                    arrUserParams.Add("NA");
                    arrUserParams.Add("NA");
                    arrUserParams.Add("NA");
                    arrUserParams.Add("NA");
                    arrUserParams.Add("NA");
                }
            }
            catch (Exception)
            {

            }
            finally
            {
                ds.Dispose();
            }
            return arrUserParams;
        }



        [WebMethod]
        public ArrayList GetTreeDetails_UplineAll(string strUserName, int intUserID, string strIPAddress, int intDesignationType, string strMacAddress, string strOSDetails, string strIMEINo, string strGcmID, string strAPPVersion, int intSource, int intCreatedByType)
        {
            DataSet ds = new DataSet();
            ArrayList arrUserParams = new ArrayList();

            int intActivityType = 61;
            try
            {
                objCreateFile.CreateTextFile(strIPAddress, strMacAddress, strOSDetails, strIMEINo, strGcmID, strAPPVersion, intSource.ToString(), intCreatedByType, intActivityType, "Get Tree Details,Data:" + intUserID, strUserName);
            }
            catch (Exception)
            {

            }
            try
            {
                if ((intUserID > 0))
                {
                    int intUID = intUserID;
                    arrUserParams.Add("TRUE");
                    try
                    {
                        dt = objODBC.getDataTable("Select a.full_name as AdminName,a.username as AdminUserID,case when a.usertype_id=3 then 'Distributor' when a.usertype_id=4 then 'Sub-Distributor' when a.usertype_id=5 then 'Retailer' end as Utype from er_login a where a.userid='" + intUID + "'");
                        if (dt.Rows.Count > 0)
                        {

                            arrUserParams.Add(dt.Rows[0][0].ToString());
                            arrUserParams.Add(dt.Rows[0][1].ToString());
                            arrUserParams.Add(dt.Rows[0][2].ToString());

                        }
                    }
                    catch (Exception)
                    {

                    }
                    finally
                    {
                        dt.Dispose();
                    }
                    while (intUID != 0)
                    {
                        int intParentID = objODBC.executeScalar_int("select parent_id from er_login where userid='" + intUID + "'");
                        if (intParentID != 1)
                        {
                            try
                            {
                                dt = objODBC.getDataTable("Select a.full_name as AdminName,a.username as AdminUserID,case when a.usertype_id=3 then 'Distributor' when a.usertype_id=4 then 'Sub-Distributor' when a.usertype_id=5 then 'Retailer' end as Utype from er_login a where a.userid='" + intParentID + "'");
                                if (dt.Rows.Count > 0)
                                {

                                    arrUserParams.Add(dt.Rows[0][0].ToString());
                                    arrUserParams.Add(dt.Rows[0][1].ToString());
                                    arrUserParams.Add(dt.Rows[0][2].ToString());
                                    intUID = intParentID;
                                }
                            }
                            catch (Exception)
                            {


                            }
                            finally
                            {
                                dt.Dispose();
                            }

                        }
                        else if (intParentID == 1)
                        {
                            intUID = intParentID;
                            arrUserParams.Add("ADMIN");
                            arrUserParams.Add("Setaragan Mutahed");
                            arrUserParams.Add("ADMIN");
                            return arrUserParams;

                        }
                        else
                        {
                            arrUserParams.Add("NA");

                        }
                    }
                }
            }
            catch (Exception)
            {
                arrUserParams.Add("FALSE");
                return arrUserParams;
            }
            finally
            {
                dt.Dispose();
            }


            return arrUserParams;
        }


        [WebMethod]
        public string er_update_Topup_rights(string strUserName, int intLoginID, int intUserID, int intStatus, int intOperatorID, string strIPAddress, int intDesignationType, string strMacAddress, string strOSDetails, string strIMEINo, string strGcmID, string strAPPVersion, int intSource, int intCreatedByType)
        {
            DataTable dt = new DataTable();
            string strResult = string.Empty;
            try
            {
                objODBCLOG.executeNonQuery("call activity_logs('" + intLoginID + "','" + intDesignationType + "','" + intCreatedByType + "','" + strIPAddress + "','" + strMacAddress + "','" + strOSDetails + "','" + strIMEINo + "','" + strGcmID + "','" + strAPPVersion + "','" + intSource + "','Update Top up rights',601,'','" + strUserName + "')");

                objCreateFile.CreateTextFile(strIPAddress, strMacAddress, strOSDetails, strIMEINo, strGcmID, strAPPVersion, intSource.ToString(), intCreatedByType, 117, "er_update_Topup_rights", strUserName);
            }
            catch (Exception)
            {


            }

            try
            {
                if (intLoginID > 0 && intUserID > 0 && intOperatorID > 0 && (intStatus == 0 || intStatus == 1))
                {
                    //dt = objODBC.getDataTable("call update_topup_wrights('" + intLoginID + "','" + intUserID + "','" + intOperatorID + "','" + intStatus + "')");
                    //if (dt.Rows.Count > 0)
                    //{
                    //    strResult = dt.Rows[0][1].ToString();
                    //}
                    //else
                    //{
                    //    strResult = "Invalid Details !";
                    //}

                    dt = objODBC.getDataTable("SELECT l1.parent_id,l1.usertype_id,lo.usertype_id,lo.salaam_status,lo.etisalat_status, lo.roshan_status,lo.mtn_status,lo.awcc_status from er_login l1 inner join er_login lo on l1.userid=lo.userid where l1.userid='" + intUserID + "' or lo.userid='" + intLoginID + "' order by l1.userid desc limit 1");
                    if (dt.Rows.Count > 0)
                    {

                        string intUserParentID = dt.Rows[0][0].ToString();
                        string intUserUtype = dt.Rows[0][1].ToString();
                        string intParentUserType = dt.Rows[0][2].ToString();
                        string intSalaamStatus = dt.Rows[0]["salaam_status"].ToString();
                        string intEtisalatStatus = dt.Rows[0]["etisalat_status"].ToString();
                        string intRoshanStatus = dt.Rows[0]["roshan_status"].ToString();
                        string intMTNStatus = dt.Rows[0]["mtn_status"].ToString();
                        string intAwccStatus = dt.Rows[0]["awcc_status"].ToString();

                        if (intUserParentID.ToString() == intLoginID.ToString())
                        {
                            //if (intUserParentID != "1")
                            //{
                            string strDownline = objODBC.executeScalar_str("SELECT child_id FROM er_login WHERE userid='" + intUserID + "'");
                            if (intOperatorID == 1) // Salaam
                            {
                                if (strDownline == "0")
                                {
                                    objODBC.executeNonQuery("UPDATE er_login SET salaam_status='" + intStatus + "' WHERE  userid='" + intUserID + "'");
                                }
                                else
                                {
                                    objODBC.executeNonQuery("UPDATE er_login SET salaam_status='" + intStatus + "' WHERE  userid='" + intUserID + "' or userid in (" + strDownline + ")");
                                }


                                objODBC.executeNonQuery("INSERT INTO er_topup_rights_change (userid,parent_id,usertype,parent_usertype,operator_id,status,created_on) VALUES ('" + intUserID + "','" + intUserParentID + "','" + intUserUtype + "','" + intParentUserType + "','" + intOperatorID + "','" + intStatus + "','" + objDT.getCurDateTimeString() + "');");

                                strResult = "Topup Rights updated successfully!";

                            }
                            else if (intOperatorID == 2) // ETisalat
                            {
                                if (strDownline == "0")
                                {
                                    objODBC.executeNonQuery("UPDATE er_login SET etisalat_status='" + intStatus + "' WHERE  userid='" + intUserID + "'");
                                }
                                else
                                {
                                    objODBC.executeNonQuery("UPDATE er_login SET etisalat_status='" + intStatus + "' WHERE  userid='" + intUserID + "' or userid in (" + strDownline + ")");
                                }



                                objODBC.executeNonQuery("INSERT INTO er_topup_rights_change (userid,parent_id,usertype,parent_usertype,operator_id,status,created_on) VALUES ('" + intUserID + "','" + intUserParentID + "','" + intUserUtype + "','" + intParentUserType + "','" + intOperatorID + "','" + intStatus + "','" + objDT.getCurDateTimeString() + "');");

                                strResult = "Topup Rights updated successfully!";

                            }
                            else if (intOperatorID == 3) // Roshan
                            {
                                if (strDownline == "0")
                                {
                                    objODBC.executeNonQuery("UPDATE er_login SET roshan_status='" + intStatus + "' WHERE  userid='" + intUserID + "'");
                                }
                                else
                                {
                                    objODBC.executeNonQuery("UPDATE er_login SET roshan_status='" + intStatus + "' WHERE  userid='" + intUserID + "' or userid in (" + strDownline + ")");
                                }


                                objODBC.executeNonQuery("INSERT INTO er_topup_rights_change (userid,parent_id,usertype,parent_usertype,operator_id,status,created_on) VALUES ('" + intUserID + "','" + intUserParentID + "','" + intUserUtype + "','" + intParentUserType + "','" + intOperatorID + "','" + intStatus + "','" + objDT.getCurDateTimeString() + "');");

                                strResult = "Topup Rights updated successfully!";

                            }
                            else if (intOperatorID == 4) // MTN
                            {
                                if (strDownline == "0")
                                {
                                    objODBC.executeNonQuery("UPDATE er_login SET mtn_status='" + intStatus + "' WHERE  userid='" + intUserID + "'");
                                }
                                else
                                {
                                    objODBC.executeNonQuery("UPDATE er_login SET mtn_status='" + intStatus + "' WHERE  userid='" + intUserID + "' or userid in (" + strDownline + ")");
                                }


                                objODBC.executeNonQuery("INSERT INTO er_topup_rights_change (userid,parent_id,usertype,parent_usertype,operator_id,status,created_on) VALUES ('" + intUserID + "','" + intUserParentID + "','" + intUserUtype + "','" + intParentUserType + "','" + intOperatorID + "','" + intStatus + "','" + objDT.getCurDateTimeString() + "');");

                                strResult = "Topup Rights updated successfully!";

                            }
                            else if (intOperatorID == 5) // awcc_status
                            {

                                if (strDownline == "0")
                                {
                                    objODBC.executeNonQuery("UPDATE er_login SET awcc_status='" + intStatus + "' WHERE  userid='" + intUserID + "'");
                                }
                                else
                                {
                                    objODBC.executeNonQuery("UPDATE er_login SET awcc_status='" + intStatus + "' WHERE  userid='" + intUserID + "' or userid in (" + strDownline + ")");
                                }

                                objODBC.executeNonQuery("INSERT INTO er_topup_rights_change (userid,parent_id,usertype,parent_usertype,operator_id,status,created_on) VALUES ('" + intUserID + "','" + intUserParentID + "','" + intUserUtype + "','" + intParentUserType + "','" + intOperatorID + "','" + intStatus + "','" + objDT.getCurDateTimeString() + "');");

                                strResult = "Topup Rights updated successfully!";

                            }
                            //}
                            //else
                            //{
                            //    // 
                            //    strResult = "User does not have permission to change the topup status !";


                            //}
                        }
                        else
                        {
                            strResult = "Member is not Belong to Login Member !";

                        }
                    }
                    else
                    {
                        strResult = "Invalid Details !";

                    }

                }
                else
                {
                    strResult = "Invalid Details !";
                }

            }
            catch (Exception)
            {
                strResult = "Somthing went wrong";

            }
            finally
            {
                dt.Dispose();
            }
            return strResult;
        }

        [WebMethod]
        public ArrayList MemberStatusAuthority(string strUserName, int intUserID, int intStatusType, string strIPAddress, int intDesignationType, string strMacAddress, string strOSDetails, string strIMEINo, string strGcmID, string strAPPVersion, int intSource, int intCreatedByType, int intUType)
        {
            ArrayList arrUserParams = new ArrayList();
            string strResponseMessage = "";
            int intActivityType = 61;
            try
            {
                if ((intUserID > 0) && (intStatusType == 1 || intStatusType == 2) && (intUType != 0))
                {



                    int count = objODBC.executeNonQuery("update er_login SET status_authority='" + intStatusType + "'  where userid='" + intUserID + "'");
                    if (count > 0)
                    {
                        strResponseMessage = "Permission Update Sucessfully";
                        arrUserParams.Add("TRUE");
                        arrUserParams.Add(strResponseMessage);

                        strResponseMessage = strResponseMessage + " = " + intStatusType;
                    }
                    else
                    {
                        if (intStatusType == 1)
                        {
                            strResponseMessage = "Permission Already Allow";
                            arrUserParams.Add("False");
                            arrUserParams.Add(strResponseMessage);
                        }
                        else
                        {
                            strResponseMessage = "Permission Already Deny";
                            arrUserParams.Add("False");
                            arrUserParams.Add(strResponseMessage);
                        }
                    }

                }
                else
                {
                    strResponseMessage = "Permission Update Fail, Kindly Enter Valid Details";
                    arrUserParams.Add("False");
                    arrUserParams.Add(strResponseMessage);
                }

            }
            catch (Exception e1)
            {
                strResponseMessage = "Permission, Invalid Details" + e1.ToString();
                arrUserParams.Add("False");
                arrUserParams.Add(strResponseMessage);
            }
            finally
            {
                try
                {
                    objODBCLOG.executeNonQuery("call activity_logs('" + intUserID + "','" + intDesignationType + "','" + intCreatedByType + "','" + strIPAddress + "','" + strMacAddress + "','" + strOSDetails + "','" + strIMEINo + "','" + strGcmID + "','" + strAPPVersion + "','" + intSource + "','Modify Member Status Authority','" + intActivityType + "','','')");

                    objCreateFile.CreateTextFile(strIPAddress, strMacAddress, strOSDetails, strIMEINo, strGcmID, strAPPVersion, intSource.ToString(), intCreatedByType, intActivityType, strResponseMessage, strUserName);
                }
                catch (Exception)
                {

                }
            }
            return arrUserParams;
        }


        // ActivityType=58
        [WebMethod]
        public ArrayList get_TopupRightsStatus(string strUserName, int intUserID, string strSearch, string strIPAddress, int intDesignationType, string strMacAddress, string strOSDetails, string strIMEINo, string strGcmID, string strAPPVersion, int intSource, int intCreatedByType)
        {
            {
                string strQuery = "";
                string strLimit = "";
                ArrayList arrUserParams = new ArrayList();


                try
                {




                    strQuery = "SELECT `etisalat_status`, `salaam_status`, `roshan_status`, `mtn_status`, `awcc_status`  From er_login Where userid = '" + intUserID + "'" + strSearch + " ORDER BY userid DESC";

                    try
                    {
                        objCreateFile.CreateTextFile(strIPAddress, strMacAddress, strOSDetails, strIMEINo, strGcmID, strAPPVersion, intSource.ToString(), intCreatedByType, 58, "get_TopupRightsStatus", strUserName);
                    }
                    catch (Exception)
                    {

                    }
                    arrUserParams = objcf.getReport(strQuery);
                    return arrUserParams;
                }
                catch (Exception ex)
                {
                    return arrUserParams;
                }
            }
        }


        // ActivityType=60
        [WebMethod]
        public DataSet AllTopupStatusReport(string strUserName, int intUserID, string strSearch, int intPage, int PageSize, string strIPAddress, int intDesignationType, string strMacAddress, string strOSDetails, string strIMEINo, string strGcmID, string strAPPVersion, int intSource, int intCreatedByType)
        {
            {
                string strQuery = "";
                string strLimit = "";
                DataSet ds = new DataSet();

                try
                {
                    string strCount = objODBC.executeScalar_str("select count(1) From er_login Where Active=1 and parent_id=1 " + strSearch + "");
                    //" + strCount + "
                    strLimit = " LIMIT " + PageSize * (intPage - 1) + "," + PageSize + "";
                    strQuery = "SELECT  userid,username,emailid,full_name,etisalat_status,salaam_status,roshan_status,mtn_status,awcc_status," + strCount + " as intCount From er_login Where Active=1 and parent_id=1 " + strSearch + " ORDER BY userid DESC " + strLimit;
                    ds = objODBC.Report(strQuery);
                    return ds;
                }
                catch (Exception ex)
                {
                    return ds;
                }
                finally
                {
                    objCreateFile.CreateTextFile(strIPAddress, strMacAddress, strOSDetails, strIMEINo, strGcmID, strAPPVersion, intSource.ToString(), intCreatedByType, 60, " All Operator Wallet Details", strUserName);
                }
            }
        }

        // ActivityType=60
        [WebMethod]
        public DataSet AllTopupStatusReport_DisSUb(string strUserName, int intUserID, string strSearch, int intPage, int PageSize, string strIPAddress, int intDesignationType, string strMacAddress, string strOSDetails, string strIMEINo, string strGcmID, string strAPPVersion, int intSource, int intCreatedByType)
        {
            {
                string strQuery = "";
                string strLimit = "";
                DataSet ds = new DataSet();

                try
                {
                    string strCount = objODBC.executeScalar_str("select count(1) From er_login Where Active=1 and parent_id='" + intUserID + "' " + strSearch + "");
                    //" + strCount + "
                    strLimit = " LIMIT " + PageSize * (intPage - 1) + "," + PageSize + "";
                    strQuery = "SELECT  userid,username,emailid,full_name,etisalat_status,salaam_status,roshan_status,mtn_status,awcc_status," + strCount + " as intCount From er_login Where Active=1 and parent_id='" + intUserID + "' " + strSearch + " ORDER BY userid DESC " + strLimit;
                    ds = objODBC.Report(strQuery);
                    return ds;
                }
                catch (Exception ex)
                {
                    return ds;
                }
                finally
                {
                    objCreateFile.CreateTextFile(strIPAddress, strMacAddress, strOSDetails, strIMEINo, strGcmID, strAPPVersion, intSource.ToString(), intCreatedByType, 60, " All Operator Wallet Details", strUserName);
                }
            }
        }


        [WebMethod]
        public ArrayList er_update_Topup_rights_AllOperator(string strUserName, int intLoginID, int intUserID, int intStatus, string strOperatorID, string strIPAddress, int intDesignationType, string strMacAddress, string strOSDetails, string strIMEINo, string strGcmID, string strAPPVersion, int intSource, int intCreatedByType)
        {
            DataTable dt = new DataTable();
            string strResult = string.Empty;
            ArrayList arrUserParams = new ArrayList();
            try
            {
                objODBCLOG.executeNonQuery("call activity_logs('" + intLoginID + "','" + intDesignationType + "','" + intCreatedByType + "','" + strIPAddress + "','" + strMacAddress + "','" + strOSDetails + "','" + strIMEINo + "','" + strGcmID + "','" + strAPPVersion + "','" + intSource + "','Update Top up rights',601,'','')");

                objCreateFile.CreateTextFile(strIPAddress, strMacAddress, strOSDetails, strIMEINo, strGcmID, strAPPVersion, intSource.ToString(), intCreatedByType, 117, "er_update_Topup_rights", strUserName);
            }
            catch (Exception)
            {


            }

            try
            {
                if (intLoginID > 0 && intUserID > 0 && strOperatorID != "" && (intStatus == 0 || intStatus == 1))
                {

                    int intCount = objODBC.executeScalar_int("SELECT count(1) FROM er_login where parent_id='" + intLoginID + "' and userid='" + intUserID + "'");
                    if (intUserID > 0)
                    {
                        // Taking a string 


                        char[] spearator = { ',', ' ' };
                        Int32 count = 5;

                        // Using the Method 
                        String[] strlist = strOperatorID.Split(spearator,
                               count, StringSplitOptions.None);

                        foreach (String intOperatorID in strlist)
                        {

                            dt = objODBC.getDataTable("call update_topup_wrights('" + intLoginID + "','" + intUserID + "','" + intOperatorID + "','" + intStatus + "')");
                            if (dt.Rows.Count > 0)
                            {
                                string intFlag = dt.Rows[0][0].ToString();
                                string intFeildError = dt.Rows[0][2].ToString();

                                strResult = dt.Rows[0][1].ToString();
                                arrUserParams.Clear();
                                arrUserParams.Add(intFeildError);
                                arrUserParams.Add(strResult);
                            }
                            else
                            {
                                strResult = "Invalid Details !";
                                arrUserParams.Add("False");
                                arrUserParams.Add(strResult);
                            }


                            //  `etisalat_status`, `salaam_status`, `roshan_status`, `mtn_status`, `awcc_status` 
                            //if (intOperatorID == "1")
                            //{
                            //    objODBC.executeNonQuery("update er_login set salaam_status='" + intStatus + "' where parent_id='" + intLoginID + "' and userid='" + intUserID + "'");
                            //    strResult = "Status Update Successfully !";
                            //}
                            //else if (intOperatorID == "2")
                            //{
                            //    objODBC.executeNonQuery("update er_login set etisalat_status='" + intStatus + "' where parent_id='" + intLoginID + "' and userid='" + intUserID + "'");
                            //    strResult = "Status Update Successfully !";
                            //}
                            //else if (intOperatorID == "3")
                            //{
                            //    objODBC.executeNonQuery("update er_login set roshan_status='" + intStatus + "' where parent_id='" + intLoginID + "' and userid='" + intUserID + "'");
                            //    strResult = "Status Update Successfully !";
                            //}
                            //else if (intOperatorID == "4")
                            //{
                            //    objODBC.executeNonQuery("update er_login set mtn_status='" + intStatus + "' where parent_id='" + intLoginID + "' and userid='" + intUserID + "'");
                            //    strResult = "Status Update Successfully !";
                            //}
                            //else if (intOperatorID == "5")
                            //{
                            //    objODBC.executeNonQuery("update er_login set awcc_status='" + intStatus + "' where parent_id='" + intLoginID + "' and userid='" + intUserID + "'");
                            //    strResult = "Status Update Successfully !";
                            //}
                        }
                    }
                    else
                    {
                        strResult = "Member does not belong to login member";
                        arrUserParams.Add("False");
                        arrUserParams.Add(strResult);
                    }
                }
                else
                {
                    strResult = "Invalid Details !";
                    arrUserParams.Add("False");
                    arrUserParams.Add(strResult);
                }

            }
            catch (Exception)
            {
                strResult = "Somthing went wrong";
                arrUserParams.Add("False");
                arrUserParams.Add(strResult);

            }
            finally
            {
                dt.Dispose();
            }
            return arrUserParams;
        }


        //  int intActivityType = 64;
        [WebMethod]
        public DataSet GetMembFilldroperdown(string strUserName, int intUserID, string strIPAddress, int intDesignationType, string strMacAddress, string strOSDetails, string strIMEINo, string strGcmID, string strAPPVersion, int intSource, int intCreatedByType)
        {
            int intActivityType = 64;
            string strMessage = "";
            string strQuery = "SELECT `userid` as ID, concat(`username`,'  (',full_name,')') as UserName FROM `er_login` where (usertype_id=3 or usertype_id=4 ) and user_status=1 ";

            System.Data.DataSet ds = new System.Data.DataSet();

            try
            {
                ds = objODBC.getDataSet(strQuery);

                if (ds.Tables[0].Rows.Count > 0)
                {
                    System.Data.DataTable firstTable = ds.Tables[0];
                    strMessage = "Get Member Filldropdown Fetch SuccessFully";


                    objCreateFile.CreateTextFile(strIPAddress, strMacAddress, strOSDetails, strIMEINo, strGcmID, strAPPVersion, intSource.ToString(), intCreatedByType, intActivityType, strMessage, strUserName);
                    return ds;
                }
                else
                {
                    return ds;

                }


            }
            catch (Exception ex)
            {

                objCreateFile.CreateTextFile(strIPAddress, strMacAddress, strOSDetails, strIMEINo, strGcmID, strAPPVersion, intSource.ToString(), intCreatedByType, intActivityType, strMessage, strUserName);
                return ds;
            }

            finally
            {
                ds.Dispose();
            }
        }


        // Activity Type=61
        [WebMethod]
        public ArrayList MemberMobileStatus(string strUserName, int intUserID, int intStatusType, string strIPAddress, int intDesignationType, string strMacAddress, string strOSDetails, string strIMEINo, string strGcmID, string strAPPVersion, int intSource, int intCreatedByType, int intUType)
        {
            ArrayList arrUserParams = new ArrayList();
            string strResponseMessage = "";
            int intActivityType = 61;
            try
            {
                if ((intUserID > 0) && (intStatusType == 0 || intStatusType == 1) && (intUType != 0))
                {



                    int count = objODBC.executeNonQuery("update er_login SET update_mobile_status='" + intStatusType + "'  where userid='" + intUserID + "'");
                    if (count > 0)
                    {
                        strResponseMessage = "Member Status Update Sucessfully";
                        arrUserParams.Add("TRUE");
                        arrUserParams.Add(strResponseMessage);

                        strResponseMessage = strResponseMessage + " = " + intStatusType;
                    }
                    else
                    {
                        if (intStatusType == 1)
                        {
                            strResponseMessage = "Member Status Already Active";
                            arrUserParams.Add("False");
                            arrUserParams.Add(strResponseMessage);
                        }
                        else
                        {
                            strResponseMessage = "Member Status Already DeActive";
                            arrUserParams.Add("False");
                            arrUserParams.Add(strResponseMessage);
                        }
                    }

                }
                else
                {
                    strResponseMessage = "Member Status Update Fail, Kindly Enter Valid Details";
                    arrUserParams.Add("False");
                    arrUserParams.Add(strResponseMessage);
                }

            }
            catch (Exception e1)
            {
                strResponseMessage = "Member Status, Invalid Details" + e1.ToString();
                arrUserParams.Add("False");
                arrUserParams.Add(strResponseMessage);
            }
            finally
            {
                objODBCLOG.executeNonQuery("call activity_logs('" + intUserID + "','" + intDesignationType + "','" + intCreatedByType + "','" + strIPAddress + "','" + strMacAddress + "','" + strOSDetails + "','" + strIMEINo + "','" + strGcmID + "','" + strAPPVersion + "','" + intSource + "','Modify Member Active Status','" + intActivityType + "','','')");

                objCreateFile.CreateTextFile(strIPAddress, strMacAddress, strOSDetails, strIMEINo, strGcmID, strAPPVersion, intSource.ToString(), intCreatedByType, intActivityType, strResponseMessage, strUserName);

            }
            return arrUserParams;
        }


        // Activity Type=26
        [WebMethod]
        public int getAlternameMobileID(string strUserName, int intUserID, string strAlternateMobile, string strIPAddress, int intDesignationType, string strMacAddress, string strOSDetails, string strIMEINo, string strGcmID, string strAPPVersion, int intSource, int intCreatedByType)
        {
            int intRUserID = 0;
            try
            {
                if ((intUserID > 0))
                {
                    intRUserID = objODBC.executeScalar_int("SELECT userid FROM er_mobile WHERE mobile='" + strAlternateMobile + "' order by id limit 1");
                }
                else
                {
                    intRUserID = 0;
                }
                return intRUserID;
            }
            catch (Exception ex)
            {
                return 0;
            }

        }

        // Activity Type=61
        [WebMethod]
        public ArrayList Ussd_Topup_Status(string strUserName, int intUserID, int intStatusType, string strIPAddress, int intDesignationType, string strMacAddress, string strOSDetails, string strIMEINo, string strGcmID, string strAPPVersion, int intSource, int intCreatedByType, int intUType)
        {
            ArrayList arrUserParams = new ArrayList();
            string strResponseMessage = "";
            int intActivityType = 61;
            try
            {
                if ((intUserID > 0) && (intStatusType == 0 || intStatusType == 1) && (intUType != 0))
                {



                    int count = objODBC.executeNonQuery("update er_login SET ussd_top_status='" + intStatusType + "'  where userid='" + intUserID + "'");
                    if (count > 0)
                    {
                        strResponseMessage = "Member USSD Status Update Sucessfully";
                        arrUserParams.Add("TRUE");
                        arrUserParams.Add(strResponseMessage);

                        strResponseMessage = strResponseMessage + " = " + intStatusType;
                    }
                    else
                    {
                        if (intStatusType == 1)
                        {
                            strResponseMessage = "Member USSD Status Already Active";
                            arrUserParams.Add("False");
                            arrUserParams.Add(strResponseMessage);
                        }
                        else
                        {
                            strResponseMessage = "Member USSD Status Already DeActive";
                            arrUserParams.Add("False");
                            arrUserParams.Add(strResponseMessage);
                        }
                    }

                }
                else
                {
                    strResponseMessage = "Member USSD Status Update Fail, Kindly Enter Valid Details";
                    arrUserParams.Add("False");
                    arrUserParams.Add(strResponseMessage);
                }

            }
            catch (Exception e1)
            {
                strResponseMessage = "Member USSD Status, Invalid Details" + e1.ToString();
                arrUserParams.Add("False");
                arrUserParams.Add(strResponseMessage);
            }
            finally
            {
                objODBCLOG.executeNonQuery("call activity_logs('" + intUserID + "','" + intDesignationType + "','" + intCreatedByType + "','" + strIPAddress + "','" + strMacAddress + "','" + strOSDetails + "','" + strIMEINo + "','" + strGcmID + "','" + strAPPVersion + "','" + intSource + "','Ussd_Topup_Status Status','" + intActivityType + "','','" + strUserName + "')");

                objCreateFile.CreateTextFile(strIPAddress, strMacAddress, strOSDetails, strIMEINo, strGcmID, strAPPVersion, intSource.ToString(), intCreatedByType, intActivityType, strResponseMessage, strUserName);

            }
            return arrUserParams;
        }




        //  int intActivityType = 1004;
        [WebMethod]
        public DataSet SubAdminDirectLoginDetails(string strUserName, int intUserID, string strLoginUSerID, string strIPAddress, int intDesignationType, string strMacAddress, string strOSDetails, string strIMEINo, string strGcmID, string strAPPVersion, int intSource, int intCreatedByType)
        {
            int intActivityType = 1004;
            string strMessage = "";
            string strLog = "0";
            Encrypt_Decrypt ED = new Encrypt_Decrypt();
            Encryption objEncrypt = new Encryption();
            System.Data.DataSet ds = new System.Data.DataSet();
            try
            {
                dt = objODBCLOG.getDataTable("call activity_logs('" + intUserID + "', '" + intDesignationType + "', '" + intCreatedByType + "', '" + strIPAddress + "', '" + strMacAddress + "', '" + strOSDetails + "', '" + strIMEINo + "', '" + strGcmID + "', '" + strAPPVersion + "', '" + intSource + "', 'Sub Admin Direct Login Details', '" + intActivityType + "', '"+ strUserName +"', '" + strLoginUSerID + "')");
                if (dt.Rows.Count > 0)
                {
                    strLog = dt.Rows[0][0].ToString();

                }
                objCreateFile.CreateTextFile(strIPAddress, strMacAddress, strOSDetails, strIMEINo, strGcmID, strAPPVersion, intSource.ToString(), intCreatedByType, intActivityType, strMessage, strLoginUSerID);
            }
            catch (Exception)
            {

            }
            finally
            {

                dt.Dispose();
                ds.Dispose();
            }
            try
            {
                string strQuery = "SELECT l.userid,l.full_name,l.usertype_id,l.emailid,l.username,l.encryption_key as EKey,l.salt1 as ESalt,l.pass_key as EPass," + strLog + " as Logs FROM `er_login` l where l.userid='" + intUserID + "' and (l.usertype_id=3 or l.usertype_id=4 ) and l.user_status=1 order by l.userid limit 1;";

                dt = objODBC.getDataTable(strQuery);

                if (dt.Rows.Count > 0)
                {
                    string strEncryptKey = objEncrypt.DecryptQueryString(dt.Rows[0][5].ToString(), dt.Rows[0][7].ToString());
                    string Salt = ED.Encrypt(strUserName, objEncrypt.DecrypSalt(dt.Rows[0][6].ToString(), dt.Rows[0][7].ToString()), 2, 2, 0);

                    dt.Rows[0]["EKey"] = strEncryptKey; //change the name                   
                    dt.Rows[0]["ESalt"] = Salt;
                    dt.Columns.Remove("EPass");
                    //  System.Data.DataTable firstTable = dt.Tables[0];
                    strMessage = "Sub Admin Direct Login Details";

                    dt.AcceptChanges();

                    ds.Tables.Add(dt);
                    return ds;
                }
                else
                {
                    return ds;
                }
            }
            catch (Exception ex)
            {
                return ds;
            }

            finally
            {
                dt.Dispose();
                ds.Dispose();
            }
        }

        // Move Downline 
        //  int intActivityType = 1001;
        [WebMethod]
        public DataSet GetMembDownlinedroperdown_1(string strUserName, int intUserID, string strLoginID, string strIPAddress, int intDesignationType, string strMacAddress, string strOSDetails, string strIMEINo, string strGcmID, string strAPPVersion, int intSource, int intCreatedByType)
        {
            int intActivityType = 1001;
            string strMessage = "";
            string strQuery = "SELECT `userid` as ID, concat(username,'  (',full_name,') - ',( case when usertype_id=1 then 'Reseller' when usertype_id=3 then 'Distributor' when usertype_id=4 then 'Sub - Distributor' when usertype_id=5 then 'Retailer' end )) as UserName FROM `er_login` where userid>1";

            System.Data.DataSet ds = new System.Data.DataSet();

            try
            {
                ds = objODBC.getDataSet(strQuery);

                if (ds.Tables[0].Rows.Count > 0)
                {
                    System.Data.DataTable firstTable = ds.Tables[0];
                    strMessage = "Fill Member Downline Droperdown";


                    objCreateFile.CreateTextFile(strIPAddress, strMacAddress, strOSDetails, strIMEINo, strGcmID, strAPPVersion, intSource.ToString(), intCreatedByType, intActivityType, strMessage, strLoginID);
                    return ds;
                }
                else
                {
                    return ds;

                }


            }
            catch (Exception ex)
            {

                objCreateFile.CreateTextFile(strIPAddress, strMacAddress, strOSDetails, strIMEINo, strGcmID, strAPPVersion, intSource.ToString(), intCreatedByType, intActivityType, strMessage, strLoginID);
                return ds;
            }

            finally
            {
                ds.Dispose();
            }
        }

        //  int intActivityType = 1002;
        [WebMethod]
        public DataSet GetMembDownlinedroperdown_2(string strUserName, int intUserID, string strLoginID, string strIPAddress, int intDesignationType, string strMacAddress, string strOSDetails, string strIMEINo, string strGcmID, string strAPPVersion, int intSource, int intCreatedByType)
        {
            int intActivityType = 1002;
            string strMessage = "";
            string strQuery = "SELECT `userid` as ID, concat(username,'  (',full_name,') - ',( case when usertype_id=1 then 'Reseller' when usertype_id=3 then 'Distributor' when usertype_id=4 then 'Sub - Distributor' when usertype_id=5 then 'Retailer' end )) as UserName FROM `er_login` where userid not in(1,"+ intUserID + ") and usertype_id<=" + intDesignationType + " ";

            System.Data.DataSet ds = new System.Data.DataSet();

            try
            {
                ds = objODBC.getDataSet(strQuery);

                if (ds.Tables[0].Rows.Count > 0)
                {
                    System.Data.DataTable firstTable = ds.Tables[0];
                    strMessage = "Fill Member Downline Droperdown";


                    objCreateFile.CreateTextFile(strIPAddress, strMacAddress, strOSDetails, strIMEINo, strGcmID, strAPPVersion, intSource.ToString(), intCreatedByType, intActivityType, strMessage, strLoginID);
                    return ds;
                }
                else
                {
                    return ds;

                }


            }
            catch (Exception ex)
            {

                objCreateFile.CreateTextFile(strIPAddress, strMacAddress, strOSDetails, strIMEINo, strGcmID, strAPPVersion, intSource.ToString(), intCreatedByType, intActivityType, strMessage, strLoginID);
                return ds;
            }

            finally
            {
                ds.Dispose();
            }
        }


        // Activity Type=1003
        [WebMethod]
        public ArrayList er_shift_member_downline(string strUserName, int intUserID, int intNewParentID, string strLoginID, string strIPAddress, int intDesignationType, string strMacAddress, string strOSDetails, string strIMEINo, string strGcmID, string strAPPVersion, int intSource, int intCreatedByType, int intUType)
        {
            ArrayList arrUserParams = new ArrayList();
            string strResponseMessage = "";
            int intActivityType = 1003;
            try
            {
                // intParentID= Move  Downline into this New parent && intUserID= Old parent

                if ((intUserID > 0) && (intNewParentID > 0) && (intUserID != intNewParentID))
                {
                    objODBC.executeNonQuery("call shift_member_downline('" + intUserID + "','" + intNewParentID + "')"); 
                    strResponseMessage = "Shifting Member Update Sucessfully";
                    arrUserParams.Add("TRUE");
                    arrUserParams.Add(strResponseMessage);
                }
                else
                {
                    strResponseMessage = " Kindly Enter Valid Details";
                    arrUserParams.Add("False");
                    arrUserParams.Add(strResponseMessage);
                }

            }
            catch (Exception e1)
            {
                strResponseMessage = "Invalid Details" + e1.ToString();
                arrUserParams.Add("False");
                arrUserParams.Add(strResponseMessage);
            }
            finally
            {
                int intLogID = objODBCLOG.executeScalar_int("call activity_logs('" + intUserID + "','" + intDesignationType + "','" + intCreatedByType + "','" + strIPAddress + "','" + strMacAddress + "','" + strOSDetails + "','" + strIMEINo + "','" + strGcmID + "','" + strAPPVersion + "','" + intSource + "',' shift member downline','887','" + intNewParentID + "','" + strLoginID + "')");

                objCreateFile.CreateTextFile(strIPAddress, strMacAddress, strOSDetails, strIMEINo, strGcmID, strAPPVersion, intSource.ToString(), intCreatedByType, intActivityType, strResponseMessage, strLoginID);

            }
            return arrUserParams;
        }
    }
}

