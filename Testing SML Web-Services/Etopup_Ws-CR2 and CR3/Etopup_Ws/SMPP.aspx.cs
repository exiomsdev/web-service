﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Etopup_Ws
{
    public partial class SMPP : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //  string strRoshanSMPPPath = @"C:\inetpub\wwwroot\WindowsServices\SMPP_Window_atomatic\RosanSMPPexe\SMPP_window_atomatic.exe";
            // string strMTNSMPPPath = @"C:\inetpub\wwwroot\WindowsServices\SMPP_Window_atomatic\MTNSMPPexe\SMPP_window_atomatic.exe";


          //   string strRoshanSMPPPath = @"D:\Git\sml-etopup-new\SMPP_window_atomatic_roshan\SMPP_window_atomatic\bin\Debug\SMPP_window_atomatic.exe";
           //  string strMTNSMPPPath = @"D:\Git\sml-etopup-new\SMPP_window_atomatic_mtn\SMPP_window_atomatic\bin\Debug\SMPP_window_atomatic.exe";

            string strRoshanSMPPPath = @"D:\SMPP_auto\RosanSMPPexe\SMPP_window_atomatic.exe";
            string strMTNSMPPPath = @"D:\SMPP_auto\MTNSMPPexe\SMPP_window_atomatic.exe";

            RoshanSMPPWindow(strRoshanSMPPPath);
            MTNSMPPWindow(strMTNSMPPPath);
        }

        public void RoshanSMPPWindow(string strRoshanSMPPPath)
        {
            try
            {
                //System.Diagnostics.ProcessStartInfo proc = new System.Diagnostics.ProcessStartInfo();
                //proc.FileName = strRoshanSMPPPath;
                //string strData = "args";
                //proc.Arguments = @"" + strData + "";
                //System.Diagnostics.Process.Start(proc);


              
                Process process = new Process();
                process.StartInfo.FileName = strRoshanSMPPPath;
                process.Start();

            }
            catch (Exception ex)
            {
                  Response.Write(ex.ToString());
            }
        }

        public void MTNSMPPWindow(string strMTNSMPPPath)
        {
            try
            {
                Process process = new Process();
                process.StartInfo.FileName = strMTNSMPPPath;
                process.Start();
            }
            catch (Exception ex)
            {
                Response.Write(ex.ToString());
            }
        }
    }
}