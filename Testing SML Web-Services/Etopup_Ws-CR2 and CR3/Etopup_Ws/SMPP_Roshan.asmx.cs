﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;

using JulMar.Smpp.Esme;
using JulMar.Smpp;
using JulMar.Smpp.Pdu;
using JulMar.Smpp.Elements;
using System.Timers;
using System.Web.Configuration;
using System.Configuration;
using static EsmeGui.mainForm;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Net;

namespace Etopup_Ws
{
    /// <summary>
    /// Summary description for SMPP_Roshan
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    [System.Web.Script.Services.ScriptService]
    public class SMPP_Roshan : System.Web.Services.WebService
    {
        public EsmeSession smppSession_ = null;
        public string smscAddress_;
        public int smscPort_;
        public string smscSystemId_;
        public string smscPassword_;
        public string smscSystemType_;
        public string submitServiceType_;
        public int enquireLinkSeconds_;
        public string destinationAdrPrefix_;
        public int submitResponseMessageIdBase_ = 10;
        public string strResult = String.Empty;
        public System.Timers.Timer enquireLinkTimer;

        [WebMethod]
        public string Roshan_SMPP(string strDestinationNumber, string strMessage)
        {
            string strResult = String.Empty;

            // Get the default settings from our configuration file
            smscAddress_ = GetAppSetting("RSmscAddress", "10.150.6.134");
            smscPort_ = GetAppSettingInt("RSmscPort", 5050);
            smscSystemId_ = GetAppSetting("RSystemId", "SetragaranM");
            smscPassword_ = GetAppSetting("RPassword", "SM@123");
            smscSystemType_ = GetAppSetting("SystemType", "");
            submitServiceType_ = GetAppSetting("ServiceType", "");
            enquireLinkSeconds_ = GetAppSettingInt("EnquireLinkSeconds", 30);
            destinationAdrPrefix_ = GetAppSetting("RDestinationAddressPrefix", "93");
            submitResponseMessageIdBase_ = GetAppSettingInt("SubmitResponseMessageIdBase", 10);

            // Create the ESME Smpp session - default version is 3.4
            smppSession_ = new EsmeSession(smscSystemId_);
            smppSession_.SmppVersion = SmppVersion.SMPP_V34;

            // Hook up all events we need
            smppSession_.OnSessionConnected += OnSessionConnected;
            smppSession_.OnSessionDisconnected += OnSessionDisconnected;
            smppSession_.OnBound += OnSessionBound;
            smppSession_.OnDeliverSm += OnDeliverSm;
         
            // Start the ball rolling by attempting to connect to the SMSC asynchronously

            strResult = ConnectToSmsc();

            string sourceNumber = "543";
            string rawTargetNumber = strDestinationNumber.Trim();
            string message = strMessage.Trim();
            if ((sourceNumber.Length < 3) || (rawTargetNumber.Length < 9) || (message.Length == 0))
            {
                return "Either a number is invalid or the message is blank";

            }
            // Add target address prefix
            string targetNumber = destinationAdrPrefix_ + rawTargetNumber;


            submit_sm submitPdu = new submit_sm();
            if (!string.IsNullOrEmpty(submitServiceType_))
                submitPdu.ServiceType = submitServiceType_;
            submitPdu.SourceAddress = new address(TypeOfNumber.UNKNOWN, NumericPlanIndicator.UNKNOWN, sourceNumber);
            submitPdu.DestinationAddress = new address(TypeOfNumber.UNKNOWN, NumericPlanIndicator.UNKNOWN, targetNumber);
            submitPdu.RegisteredDelivery = new registered_delivery(DeliveryReceiptType.FINAL_DELIVERY_RECEIPT, AcknowledgementType.DELIVERY_USER_ACK_REQUEST, true);
            // submitPdu.RegisteredDelivery = new registered_delivery(DeliveryReceiptType.FINAL_DELIVERY_RECEIPT, AcknowledgementType.USER_ACK_REQUEST, true);
            submitPdu.Message = message;
            smppSession_.BeginSubmitSm(submitPdu, new AsyncCallback(SubmitSmCallback));
            return strResult;
        }

        //// Called when the connection to SMSC is established
        private void OnSessionConnected(object sender, SmppEventArgs args)
        {


            // Send Bind PDU to SMSC asynchronously
            bind_transceiver bindPdu = new bind_transceiver(smscSystemId_, smscPassword_, "",
                                                            new interface_version(),    // Default is version 3.4
                                                            new address_range());
            bindPdu.SystemType = smscSystemType_;
            smppSession_.BeginBindTransceiver(bindPdu, new AsyncCallback(BindTransceiverCallback));

        }

        // Used to catch and display binding issues
        private void BindTransceiverCallback(IAsyncResult ar)
        {
            // Process the bind result
            EsmeSession session = (EsmeSession)ar.AsyncState;
            bind_transceiver_resp bindResp = session.EndBindTransceiver(ar);
            if (bindResp.Status != StatusCodes.ESME_ROK)
            {
                strResult = "Error binding to SMSC: " + bindResp.Status.ToString();

            }
        }


        public void OnSessionDisconnected(object sender, SmppEventArgs args)
        {
            SmppDisconnectEventArgs dea = (SmppDisconnectEventArgs)args;
            if (dea.Exception != null)
                strResult = "Socket error: " + ((dea.Exception.Message != null) ? dea.Exception.Message : dea.Exception.ToString());
            else
                strResult = "Smsc Session/Connection was dropped";


        }

        public string ConnectToSmsc()
        {
            try
            {
                smppSession_.Connect(smscAddress_, smscPort_);
                return "True";
            }
            catch (Exception ex)
            {
                return "Exception trying to connect to Smsc: " + ex.ToString();

            }
        }

        public void OnSessionBound(object sender, SmppEventArgs args)
        {
            // Set the session to bound
            // If the enquireLinkSeconds_ is non-zero then create a timer to send enquire link PDU's

            if (enquireLinkSeconds_ != 0)
            {
                enquireLinkTimer = new System.Timers.Timer((double)enquireLinkSeconds_ * 1000);
                enquireLinkTimer.Elapsed += new ElapsedEventHandler(OnEnquireLinkElapsedTimer);
                enquireLinkTimer.Enabled = true;
            }
        }


        // Used to send Enquire Link messages to SMSC every enquireLinkSeconds_ seconds
        public void OnEnquireLinkElapsedTimer(object source, ElapsedEventArgs e)
        {
            // Send if our session is still connected/bound
            if ((smppSession_.IsConnected) && (smppSession_.IsBound))
            {
                enquire_link enquirePdu = new enquire_link();
                smppSession_.BeginEnquireLink(enquirePdu, new AsyncCallback(EnquireLinkCallback));
            }
        }

        public void EnquireLinkCallback(IAsyncResult ar)
        {
            // Process the enquire link result
            EsmeSession session = (EsmeSession)ar.AsyncState;
            enquire_link_resp enquireResp = session.EndEnquireLink(ar);
            if (enquireResp.Status != StatusCodes.ESME_ROK)
            {
                strResult = "Error sending enquire link to SMSC: " + enquireResp.Status.ToString();
            }
        }


        // Used to get delivery ack.'s and MO messages from SMSC
        public void OnDeliverSm(object sender, SmppEventArgs args)
        {
            deliver_sm req = (deliver_sm)args.PDU;
            deliver_sm_resp resp = (deliver_sm_resp)args.ResponsePDU;
            esm_class esm = req.EsmClass;

        }

        public string GetAppSetting(string setting, string defaultValue)
        {
            string value = ConfigurationManager.AppSettings.Get(setting);
            return String.IsNullOrEmpty(value) ? defaultValue : value;
        }

        public int GetAppSettingInt(string setting, int defaultValue)
        {
            int intValue = defaultValue;
            string value = ConfigurationManager.AppSettings.Get(setting);
            if (!String.IsNullOrEmpty(value))
                Int32.TryParse(value, out intValue);
            return intValue;
        }

        public bool GetAppSettingBool(string setting, bool defaultValue)
        {
            bool boolValue = defaultValue;
            string value = ConfigurationManager.AppSettings.Get(setting);
            if (!String.IsNullOrEmpty(value))
                Boolean.TryParse(value, out boolValue);
            return boolValue;
        }

        public delegate void SubmitSmCallbackHandler(IAsyncResult ar);
        public void SubmitSmCallback(IAsyncResult ar)
        {
            // Process the send/submit result
            EsmeSession session = (EsmeSession)ar.AsyncState;
        
            SubmitSmCallbackHandler objsm = new SubmitSmCallbackHandler(SubmitSmCallback);
            ar = objsm.BeginInvoke(ar,SubmitSmCallback, session);
            submit_sm_resp submitResp = session.EndSubmitSm(ar);
            return;

        }
    }


    }
