﻿using Etopup_Ws.old_App_Code;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;


namespace Etopup_Ws
{
    /// <summary>
    /// Summary description for Token
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/Token.asmx")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
     [System.Web.Script.Services.ScriptService]
    public class Token : System.Web.Services.WebService
    {

        ODBC objodbc = new ODBC();  Encrypt_Decrypt ED = new Encrypt_Decrypt(); Encryption EC = new Encryption(); string strReturn = string.Empty; CommonFunction objComm = new CommonFunction();

        public Token()
        {

            //Uncomment the following line if using designed components 
            //InitializeComponent(); 
        }

        [WebMethod]// (intUserType 1 -Admin, 2- Member || strData - String Password - 0, String encryptionkey - 1) inttype 1- user 2- guest (it always 1)
        public string token_request(string strUserid, string strData, int inttype, int intUserType, int intSource, string strToken)
        {
            try
            {
                    if (intUserType == 1)
                    {

                        int intval2 = objodbc.executeScalar_int("SELECT COUNT(1) FROM er_login_admin Where userid='" + strUserid + "'");
                        if (intval2 == 0)
                        {
                            var data = ED.Decrypt(strUserid, strData, 1, inttype, intSource).Split(',');
                            string key = objodbc.executeScalar_str("SELECT pass_key FROM er_login_admin Where userid='" + strUserid + "' and encryption_key='" + data[1] + "'");

                            int intval3 = objodbc.executeScalar_int("SELECT COUNT(1) FROM er_login_admin Where userid='" + strUserid + "' AND password='" + EC.EncryptQueryString(data[0], key) + "' and encryption_key='" + data[1] + "'");
                            if (intval3 == 0)
                            {
                                string newkey = objComm.GenrateRandomString();
                                objodbc.executeNonQuery("update er_login_admin SET encryption_key='" + newkey + "' where userid='" + strUserid + "'");
                                strReturn = newkey;
                            }
                            else
                            {
                                strReturn = "Invalid Request!";
                            }
                        }
                        else
                        {
                            strReturn = "Invalid Request!";
                        }

                    }

                    if (intUserType == 2)
                    {

                        int intval2 = objodbc.executeScalar_int("SELECT COUNT(1) FROM er_login Where userid='" + strUserid + "'");
                        if (intval2 == 0)
                        {
                            var data = ED.Decrypt(strUserid, strData, 1, inttype, intSource).Split(',');
                            string key = objodbc.executeScalar_str("SELECT pass_key FROM er_login Where userid='" + strUserid + "' and encryption_key='" + data[1] + "'");

                            int intval3 = objodbc.executeScalar_int("SELECT COUNT(1) FROM er_login Where userid='" + strUserid + "' AND password='" + EC.EncryptQueryString(data[0], key) + "' and encryption_key='" + data[1] + "'");
                            if (intval3 == 0)
                            {
                                string newkey = objComm.GenrateRandomString();
                                objodbc.executeNonQuery("update er_login SET encryption_key='" + newkey + "' where userid='" + strUserid + "'");


                                strReturn = newkey;
                            }
                            else
                            {
                                strReturn = "Invalid Request!";
                            }
                        }
                        else
                        {
                            strReturn = "Invalid Request!";
                        }
                    }
                return ED.Encrypt(strUserid, strReturn, 1, inttype, intSource);
            }
            catch (Exception ex)
            {
                return "0";
            }
        }
    }
}
