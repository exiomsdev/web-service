﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Etopup_Ws.old_App_Code;
using System.Web.Services;
using System.Collections;
using System.Data;
using System.Text.RegularExpressions;

namespace Etopup_Ws
{//GetDistributerFilldropdown

    //GetSubDistributerFilldropdown
    /// <summary>
    /// Summary description for Registration
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class Registration : System.Web.Services.WebService
    {
        ODBC objODBC = new ODBC();
        ODBC_LOG objODBCLOG = new ODBC_LOG();
        DataSet ds = new DataSet();
        DataTable dt2 = new DataTable();
        DateFormat objDT = new DateFormat();
        Encrypt_Decrypt ED = new Encrypt_Decrypt();
        CreateFile objCreateFile = new CreateFile();
        CommonFunction objComFUN = new CommonFunction();
        Encryption objEncrypt = new Encryption();
        CommonFunction objcf = new CommonFunction();
        public string GetSalt(int maximumSaltLength)
        {
            Random rm = new Random();
            string result = string.Empty;
            for (int i = 0; i < maximumSaltLength; i++)
            {
                result += "0x" + rm.Next(11, 99) + ", ";
            }
            result += "0x" + rm.Next(11, 99);
            return result;
        }

        //string strUserName=0,int intResellerID=1,string strEmailID=2,string strMobile=3,int intGender=4,string strPassword=5,string strFullName=6,int intDesignationType=7,int intCreatedBy=8,int intStateID=9,string strStateName=10, string strCityName=11,string strAddress=12,int intOperaterID=13,string strIPAddress=14, string strMacAddress=15, string strOSDetails=16, string strIMEINo=17,string strGcmID=18, string strAPPVersion=19, int intSource=20, int intCreatedByType=21,int intActualUserID=22,string strAlternateMobile=23,int intAlternateOperaterID=24,int intSlabID=25,int intSlabPermission=26

        // Activity Type=59 here intEUserType = 1 
        [WebMethod]
        public ArrayList MemberRegistration(string strUserName, string strData, int intEUserType, int intType, int intSource)
        {
            string strSalt1 = "", strSalt2 = "", strSalt3 = "", strEncryptKey = "", passkey = "";
            ArrayList arrUserParams = new ArrayList();
            string strResponseMessage = "";
            int intActivityType = 59, intUserID = 0;

            string strMessage = "";
            var data = ED.Decrypt(strUserName, strData, intEUserType, intType, intSource).Split(',');
            try
            {
                string strUserNameData = data[0];
                int intResellerID = int.Parse(data[1]);
                string strEmailID = data[2];
                string strMobile = data[3];
                int intGender = int.Parse(data[4]);
                string strFullName = data[6];
                int intDesignationType = int.Parse(data[7]);
                string strPassword = "NA";

                strPassword = objComFUN.GenrateNumberString();

                int intCreatedBy = int.Parse(data[8]);
                int intStateID = int.Parse(data[9]);
                string strStateName = data[10];
                string strCityName = data[11];
                string strAddress = data[12];
                int intOperaterID = int.Parse(data[13]);
                string strIPAddress = data[14];
                string strMacAddress = data[15];
                string strOSDetails = data[16];
                string strIMEINo = data[17];
                string strGcmID = data[18];
                string strAPPVersion = data[19];
                int intSourceData = int.Parse(data[20]);
                int intCreatedByType = int.Parse(data[21]);
                int intActualUserID = int.Parse(data[22]);
                string strAlternateMobile = data[23];
                int intAlternateOperaterID = int.Parse(data[24]);
                int intSlabID = int.Parse(data[25]);
                string strMPin = "NA";
                //  int intSlabPermission = int.Parse(data[26]);
                int intSlabPermission =1;
                try
                {
                    objCreateFile.CreateTextFile(strIPAddress, strMacAddress, strOSDetails, strIMEINo, strGcmID, strAPPVersion, intSourceData.ToString(), intCreatedByType, intActivityType, "Member Registration,Data:" + strData, strUserName);
                }
                catch (Exception)
                {

                }

                try
                {
                    if (strMobile != strAlternateMobile)
                    {

                        if (objComFUN.CheckRegixMobile(intOperaterID, strMobile) == "TRUE")
                        {

                            if ((objComFUN.CheckRegixMobile(intAlternateOperaterID, strAlternateMobile) == "TRUE") || (strMobile == "0"))
                            {

                                if ((intResellerID > 0) && (strEmailID != "" || strEmailID != null) && (strMobile != "" || strMobile != null) && (strPassword != "" || strPassword != null) && (strFullName != "" || strFullName != null) && (strStateName != "" || strStateName != null) && (strCityName != "" || strCityName != null) && (strAddress != "" || strAddress != null))
                                {

                                    ds = objODBC.getDataSet("call add_register_member_Slab('" + intResellerID + "','" + strEmailID + "','" + intGender + "','" + strMobile + "','" + strPassword + "','" + strFullName + "','" + intDesignationType + "','" + intCreatedBy + "','" + intStateID + "','" + strStateName + "','" + strCityName + "','" + strAddress + "','" + intOperaterID + "','" + strAlternateMobile + "','" + intAlternateOperaterID + "','" + intSlabID + "','" + intSlabPermission + "')");

                                    if (ds.Tables[0].Rows.Count > 0)
                                    {
                                        string intFlag = ds.Tables[0].Rows[0]["Flag"].ToString();
                                        if (intFlag == "200")
                                        {
                                            strResponseMessage = ds.Tables[0].Rows[0]["FeildError"].ToString();
                                            intUserID = Convert.ToInt32(ds.Tables[0].Rows[0]["intUserID"].ToString());
                                            //passkey = ds.Tables[0].Rows[0]["strPassKey"].ToString();
                                            passkey = objComFUN.GenratePassKey();
                                            string strMemberUserName = ds.Tables[0].Rows[0]["strRandumUserName"].ToString();
                                            string strPasswordDetail = "";
                                            try
                                            {

                                                passkey = objComFUN.GenratePassKey();
                                                strPasswordDetail = objEncrypt.EncryptQueryString(strPassword, passkey);
                                            }
                                            catch (Exception)
                                            {


                                            }

                                            while (strPasswordDetail == "Specified key is not a valid size for this algorithm.")
                                            {
                                                try
                                                {

                                                    passkey = objComFUN.GenratePassKey();
                                                    strPasswordDetail = objEncrypt.EncryptQueryString(strPassword, passkey);
                                                }
                                                catch (Exception)
                                                {


                                                }

                                            }

                                            strSalt1 = objEncrypt.EncryptSalt(GetSalt(13), passkey);
                                            strSalt2 = objEncrypt.EncryptSalt(GetSalt(13), passkey);
                                            strSalt3 = objEncrypt.EncryptSalt(GetSalt(13), passkey);
                                            strEncryptKey = objEncrypt.EncryptQueryString(objComFUN.GenrateRandomKeyString(), passkey);
                                            /*strSalt1 = GetSalt(13);
                                            strSalt2 = GetSalt(13);
                                            strSalt3 = GetSalt(13);
                                            strEncryptKey = objComFUN.GenrateRandomKeyString(); */
                                            string strSMSMPin = objComFUN.GenrateNumber(4);
                                            strMPin = objEncrypt.EncryptQueryString(strSMSMPin, passkey);
                                            objODBC.executeNonQuery("update er_login set pass_key='" + passkey + "',salt1='" + strSalt1 + "',salt2='" + strSalt2 + "',salt3='" + strSalt3 + "',encryption_key='" + strEncryptKey + "',password='" + strPasswordDetail + "',M_Pin='" + strMPin + "' where userid='" + intUserID + "'");

                                            try
                                            {
                                                objODBC.executeNonQuery("INSERT INTO `er_login_counter`( `userid`, `count_attempt`) VALUES ('" + intUserID + "',0)");
                                            }
                                            catch (Exception)
                                            {

                                            }

                                            try
                                            {
                                                objODBCLOG.executeNonQuery("INSERT INTO er_login_log (userid,full_name,username,emailid,mobile,usertype,created_on,Created_by_username) VALUES ('" + intUserID + "','" + strFullName + "','" + strMemberUserName + "','" + strEmailID + "','" + strMobile + "','2','" + objDT.getCurDateTimeString() + "','" + strUserName + "')");
                                            }
                                            catch (Exception)
                                            {

                                            }
                                            arrUserParams.Add("TRUE");
                                            arrUserParams.Add(strResponseMessage);
                                            arrUserParams.Add(intUserID);

                                            // SMPP SMS Code Below
                                            SMPP_SMS_Format objSMPP_SMS_Format = new SMPP_SMS_Format();
                                            string strGetSMS = objSMPP_SMS_Format.MemberRegistration(strMemberUserName, strFullName, strPassword, strSMSMPin);
                                            
                                            string strSMPPResponse = "NA";
                                            try
                                            {
                                              //  objComFUN.WriteToSMSFile(intUserID, 2, strMobile, intOperaterID, strGetSMS, 0, strSMPPResponse, strSMPPResponse, strSMPPResponse, strSMPPResponse, strSMPPResponse);
                                                objComFUN.er_SMS_Alert(intUserID, 2, strMobile, intOperaterID, strGetSMS, 1, strSMPPResponse);

                                            }
                                            catch (Exception)
                                            {

                                            }


                                        }
                                        else
                                        {
                                            strResponseMessage = ds.Tables[0].Rows[0]["FeildError"].ToString();
                                            arrUserParams.Add("FALSE");
                                            arrUserParams.Add(strResponseMessage);
                                        }

                                    }


                                }
                                else
                                {
                                    strResponseMessage = "Kindly Enter Valid Details ,Kindly Enter Valid Details !";
                                    arrUserParams.Add("FALSE");
                                    arrUserParams.Add(strResponseMessage);
                                }
                            }
                            else
                            {
                                strResponseMessage = " Alternate Mobile Number does not belongs to selected operator , Alternate Mobile Number does not belongs to selected operator!";
                                arrUserParams.Add("FALSE");
                                arrUserParams.Add(strResponseMessage);
                            }
                        }
                        else
                        {
                            strResponseMessage = "Mobile Number does not belongs to selected operator,Mobile Number does not belongs to selected operator!";
                            arrUserParams.Add("FALSE");
                            arrUserParams.Add(strResponseMessage);
                        }
                    }
                    else
                    {
                        strResponseMessage = "Mobile Number and Alternate Number Does not be same ,Kindly Enter Valid Details !";
                        arrUserParams.Add("FALSE");
                        arrUserParams.Add(strResponseMessage);
                    }

                }
                catch (Exception e1)
                {
                    strResponseMessage = "Invalid Details,Invalid Details " + e1.ToString();
                    arrUserParams.Add("FALSE");
                    arrUserParams.Add(strResponseMessage);
                }
                finally
                {
                    try
                    {

                        objODBCLOG.executeNonQuery("call activity_logs('" + intActualUserID + "','" + intDesignationType + "','" + intCreatedByType + "','" + strIPAddress + "','" + strMacAddress + "','" + strOSDetails + "','" + strIMEINo + "','" + strGcmID + "','" + strAPPVersion + "','" + intSourceData + "','Member Registration Direct','" + intActivityType + "','','')");

                        objCreateFile.CreateTextFile(strIPAddress, strMacAddress, strOSDetails, strIMEINo, strGcmID, strAPPVersion, intSourceData.ToString(), intCreatedByType, intActivityType, strResponseMessage, strUserName);
                    }
                    catch (Exception)
                    {

                    }

                    ds.Dispose();
                }
            }
            catch (Exception e1)
            {
                strResponseMessage = " Invalid Details,Member Registration " + e1.ToString();
                arrUserParams.Add("FALSE");
                arrUserParams.Add(strResponseMessage);
            }
            return arrUserParams;
        }


        DataTable dt = new DataTable();

        //  int intActivityType = 64;
        [WebMethod]
        public DataSet GetDistributerFilldropdown(string strUserName, int intUserID, string strIPAddress, int intDesignationType, string strMacAddress, string strOSDetails, string strIMEINo, string strGcmID, string strAPPVersion, int intSource, int intCreatedByType)
        {
            int intActivityType = 64;
            string strMessage = "";
            string strQuery = "SELECT `userid` as ID, concat(`username`,'  (',full_name,')') as UserName FROM `er_login` where usertype_id=3 and user_status=1 and parent_id=" + intUserID + "";

            System.Data.DataSet ds = new System.Data.DataSet();

            try
            {
                ds = objODBC.getDataSet(strQuery);

                if (ds.Tables[0].Rows.Count > 0)
                {
                    System.Data.DataTable firstTable = ds.Tables[0];
                    strMessage = "Get SUB Distributer Filldropdown Fetch SuccessFully";


                    objCreateFile.CreateTextFile(strIPAddress, strMacAddress, strOSDetails, strIMEINo, strGcmID, strAPPVersion, intSource.ToString(), intCreatedByType, intActivityType, strMessage, strUserName);
                    return ds;
                }
                else
                {
                    strMessage = "Get SUB Distributer Filldropdown Fetch Failed";


                    objCreateFile.CreateTextFile(strIPAddress, strMacAddress, strOSDetails, strIMEINo, strGcmID, strAPPVersion, intSource.ToString(), intCreatedByType, intActivityType, strMessage, strUserName);
                    return ds;

                }


            }
            catch (Exception ex)
            {

                objCreateFile.CreateTextFile(strIPAddress, strMacAddress, strOSDetails, strIMEINo, strGcmID, strAPPVersion, intSource.ToString(), intCreatedByType, intActivityType, strMessage, strUserName);
                return ds;
            }

            finally
            {
                ds.Dispose();
            }
        }

        //  int intActivityType = 64;
        [WebMethod]
        public DataSet GetDistributerunderDistributorFilldropdown(string strUserName, int intUserID, string strIPAddress, int intDesignationType, string strMacAddress, string strOSDetails, string strIMEINo, string strGcmID, string strAPPVersion, int intSource, int intCreatedByType)
        {
            int intActivityType = 64;
            string strMessage = "";
            string strQuery = "";
            if (intCreatedByType == 1)
            {
                strQuery = "SELECT `userid` as ID, concat(`username`,'  (',full_name,')') as UserName FROM `er_login` where usertype_id=3 and user_status=1 ";
            }
            else
            {
                string strDownline = objODBC.executeScalar_str("SELECT child_id FROM er_login WHERE userid='"+ intUserID + "'");
                strQuery = "SELECT `userid` as ID, concat(`username`,'  (',full_name,')') as UserName FROM `er_login` where usertype_id=3 and user_status=1  and userid in ("+ strDownline + ")";
            }

            System.Data.DataSet ds = new System.Data.DataSet();

            try
            {
                ds = objODBC.getDataSet(strQuery);

                if (ds.Tables[0].Rows.Count > 0)
                {
                    System.Data.DataTable firstTable = ds.Tables[0];
                    strMessage = "Get SUB Distributer Filldropdown Fetch SuccessFully";


                    objCreateFile.CreateTextFile(strIPAddress, strMacAddress, strOSDetails, strIMEINo, strGcmID, strAPPVersion, intSource.ToString(), intCreatedByType, intActivityType, strMessage, strUserName);
                    return ds;
                }
                else
                {
                    strMessage = "Get SUB Distributer Filldropdown Fetch Failed";


                    objCreateFile.CreateTextFile(strIPAddress, strMacAddress, strOSDetails, strIMEINo, strGcmID, strAPPVersion, intSource.ToString(), intCreatedByType, intActivityType, strMessage, strUserName);
                    return ds;

                }


            }
            catch (Exception ex)
            {

                objCreateFile.CreateTextFile(strIPAddress, strMacAddress, strOSDetails, strIMEINo, strGcmID, strAPPVersion, intSource.ToString(), intCreatedByType, intActivityType, strMessage, strUserName);
                return ds;
            }

            finally
            {
                ds.Dispose();
            }
        }


        [WebMethod]
        public DataSet GetSubDistributerunderSubDistributorFilldropdown(string strUserName, int intUserID, string strIPAddress, int intDesignationType, string strMacAddress, string strOSDetails, string strIMEINo, string strGcmID, string strAPPVersion, int intSource, int intCreatedByType)
        {
            int intActivityType = 64;
            string strMessage = "";
            string strQuery = "";
            if (intCreatedByType == 1)
            {
                strQuery = "SELECT `userid` as ID, concat(`username`,'  (',full_name,')') as UserName FROM `er_login` where usertype_id=4 and user_status=1 ";
            }
            else
            {
                string strDownline = objODBC.executeScalar_str("SELECT child_id FROM er_login WHERE userid='" + intUserID + "'");
                strQuery = "SELECT `userid` as ID, concat(`username`,'  (',full_name,')') as UserName FROM `er_login` where usertype_id=4 and user_status=1  and userid in (" + strDownline + ")";
            }

            System.Data.DataSet ds = new System.Data.DataSet();

            try
            {
                ds = objODBC.getDataSet(strQuery);

                if (ds.Tables[0].Rows.Count > 0)
                {
                    System.Data.DataTable firstTable = ds.Tables[0];
                    strMessage = "Get SUB Distributer Filldropdown Fetch SuccessFully";


                    objCreateFile.CreateTextFile(strIPAddress, strMacAddress, strOSDetails, strIMEINo, strGcmID, strAPPVersion, intSource.ToString(), intCreatedByType, intActivityType, strMessage, strUserName);
                    return ds;
                }
                else
                {
                    strMessage = "Get SUB Distributer Filldropdown Fetch Failed";


                    objCreateFile.CreateTextFile(strIPAddress, strMacAddress, strOSDetails, strIMEINo, strGcmID, strAPPVersion, intSource.ToString(), intCreatedByType, intActivityType, strMessage, strUserName);
                    return ds;

                }


            }
            catch (Exception ex)
            {

                objCreateFile.CreateTextFile(strIPAddress, strMacAddress, strOSDetails, strIMEINo, strGcmID, strAPPVersion, intSource.ToString(), intCreatedByType, intActivityType, strMessage, strUserName);
                return ds;
            }

            finally
            {
                ds.Dispose();
            }
        }

        [WebMethod]
        public DataSet GetRetailerunderRetailerFilldropdown(string strUserName, int intUserID, string strIPAddress, int intDesignationType, string strMacAddress, string strOSDetails, string strIMEINo, string strGcmID, string strAPPVersion, int intSource, int intCreatedByType)
        {
            int intActivityType = 64;
            string strMessage = "";
            string strQuery = "";
            if (intCreatedByType == 1)
            {
                strQuery = "SELECT `userid` as ID, concat(`username`,'  (',full_name,')') as UserName FROM `er_login` where usertype_id=5 and user_status=1 ";
            }
            else
            {
                string strDownline = objODBC.executeScalar_str("SELECT child_id FROM er_login WHERE userid='" + intUserID + "'");
                strQuery = "SELECT `userid` as ID, concat(`username`,'  (',full_name,')') as UserName FROM `er_login` where usertype_id=5 and user_status=1  and userid in (" + strDownline + ")";
            }

            System.Data.DataSet ds = new System.Data.DataSet();

            try
            {
                ds = objODBC.getDataSet(strQuery);

                if (ds.Tables[0].Rows.Count > 0)
                {
                    System.Data.DataTable firstTable = ds.Tables[0];
                    strMessage = "Get SUB Distributer Filldropdown Fetch SuccessFully";


                    objCreateFile.CreateTextFile(strIPAddress, strMacAddress, strOSDetails, strIMEINo, strGcmID, strAPPVersion, intSource.ToString(), intCreatedByType, intActivityType, strMessage, strUserName);
                    return ds;
                }
                else
                {
                    strMessage = "Get SUB Distributer Filldropdown Fetch Failed";


                    objCreateFile.CreateTextFile(strIPAddress, strMacAddress, strOSDetails, strIMEINo, strGcmID, strAPPVersion, intSource.ToString(), intCreatedByType, intActivityType, strMessage, strUserName);
                    return ds;

                }


            }
            catch (Exception ex)
            {

                objCreateFile.CreateTextFile(strIPAddress, strMacAddress, strOSDetails, strIMEINo, strGcmID, strAPPVersion, intSource.ToString(), intCreatedByType, intActivityType, strMessage, strUserName);
                return ds;
            }

            finally
            {
                ds.Dispose();
            }
        }

        //  int intActivityType = 65;
        [WebMethod]
        public DataSet GetSubDistributerFilldropdown(string strUserName, int intUserID, int intSubDistributer, string strIPAddress, int intDesignationType, string strMacAddress, string strOSDetails, string strIMEINo, string strGcmID, string strAPPVersion, int intSource, int intCreatedByType)
        {
            int intActivityType = 65;
            string strMessage = "";

            string strQuery = "SELECT `userid` as ID, concat(`username`,'  (',full_name,')') as UserName FROM `er_login` where usertype_id=4 and user_status=1 and distributor_id='" + intSubDistributer + "';";

            System.Data.DataSet ds = new System.Data.DataSet();

            try
            {
                ds = objODBC.getDataSet(strQuery);

                if (ds.Tables[0].Rows.Count > 0)
                {
                    System.Data.DataTable firstTable = ds.Tables[0];
                    strMessage = "Get SUB Distributer Filldropdown Fetch SuccessFully";



                    objCreateFile.CreateTextFile(strIPAddress, strMacAddress, strOSDetails, strIMEINo, strGcmID, strAPPVersion, intSource.ToString(), intCreatedByType, intActivityType, strMessage, strUserName);
                    return ds;
                }
                else
                {
                    strMessage = "Get SUB Distributer Filldropdown Fetch Failed";

                    objCreateFile.CreateTextFile(strIPAddress, strMacAddress, strOSDetails, strIMEINo, strGcmID, strAPPVersion, intSource.ToString(), intCreatedByType, intActivityType, strMessage, strUserName);
                    return ds;

                }


            }
            catch (Exception ex)
            {


                objCreateFile.CreateTextFile(strIPAddress, strMacAddress, strOSDetails, strIMEINo, strGcmID, strAPPVersion, intSource.ToString(), intCreatedByType, intActivityType, strMessage, strUserName);
                return ds;
            }

            finally
            {


                ds.Dispose();
            }
        }

        //  int intActivityType = 65;
        [WebMethod]
        public DataSet GetSubDistributerFilldropdown_admin(string strUserName, int intUserID, int intSubDistributer, string strIPAddress, int intDesignationType, string strMacAddress, string strOSDetails, string strIMEINo, string strGcmID, string strAPPVersion, int intSource, int intCreatedByType)
        {
            int intActivityType = 65;
            string strMessage = "";

            string strQuery = "SELECT `userid` as ID, concat(`username`,'  (',full_name,')') as UserName FROM `er_login` where usertype_id=4 and user_status=1 ;";

            System.Data.DataSet ds = new System.Data.DataSet();

            try
            {
                ds = objODBC.getDataSet(strQuery);

                if (ds.Tables[0].Rows.Count > 0)
                {
                    System.Data.DataTable firstTable = ds.Tables[0];
                    strMessage = "Get SUB Distributer Filldropdown Fetch SuccessFully";



                    objCreateFile.CreateTextFile(strIPAddress, strMacAddress, strOSDetails, strIMEINo, strGcmID, strAPPVersion, intSource.ToString(), intCreatedByType, intActivityType, strMessage, strUserName);
                    return ds;
                }
                else
                {
                    strMessage = "Get SUB Distributer Filldropdown Fetch Failed";

                    objCreateFile.CreateTextFile(strIPAddress, strMacAddress, strOSDetails, strIMEINo, strGcmID, strAPPVersion, intSource.ToString(), intCreatedByType, intActivityType, strMessage, strUserName);
                    return ds;

                }


            }
            catch (Exception ex)
            {


                objCreateFile.CreateTextFile(strIPAddress, strMacAddress, strOSDetails, strIMEINo, strGcmID, strAPPVersion, intSource.ToString(), intCreatedByType, intActivityType, strMessage, strUserName);
                return ds;
            }

            finally
            {


                ds.Dispose();
            }
        }


        [WebMethod]
        public ArrayList er_android_GetSubDistributerFilldropdownn(string strUserName, int intUserID, int intSubDistributer, string strIPAddress, int intDesignationType, string strMacAddress, string strOSDetails, string strIMEINo, string strGcmID, string strAPPVersion, int intSource, int intCreatedByType)
        {

            try
            {
                objCreateFile.CreateTextFile(strIPAddress, strMacAddress, strOSDetails, strIMEINo, strGcmID, strAPPVersion, intSource.ToString(), intCreatedByType, 109, " er_android_GetSubDistributerFilldropdown ", strUserName);
            }
            catch (Exception)
            {


            }
            ArrayList arrlst = new ArrayList();

            try
            {
                ds = GetSubDistributerFilldropdown(strUserName, intUserID, intSubDistributer, strIPAddress, intDesignationType, strMacAddress, strOSDetails, strIMEINo, strGcmID, strAPPVersion, intSource, intCreatedByType);

                arrlst = objcf.Get_ArrayLlist_From_Dataset(ds);

            }
            catch (Exception)
            {


            }
            finally
            {

            }

            return arrlst;
        }



        //string strUserName=0,int intResellerID=1,string strEmailID=2,string strMobile=3,int intGender=4,string strPassword=5,string strFullName=6,int intDesignationType=7,int intCreatedBy=8,int intStateID=9,string strStateName=10, string strCityName=11,string strAddress=12,int intOperaterID=13,string strIPAddress=14, string strMacAddress=15, string strOSDetails=16, string strIMEINo=17,string strGcmID=18, string strAPPVersion=19, int intSource=20, int intCreatedByType=21,int intDistID=22,int intActualUserID=23,string strAlternateMobile=24,int intAlternateOperaterID=25

        // Activity Type=66
        [WebMethod]
        public ArrayList MemberRegistrationSubDistributer(string strUserName, string strData, int intEUserType, int intType, int intSource)
        {
            string strSalt1 = "", strSalt2 = "", strSalt3 = "", strEncryptKey = "", passkey = "";
            ArrayList arrUserParams = new ArrayList();
            string strResponseMessage = "";
            int intActivityType = 66, intUserID = 0;

            string strMessage = "";
            var data = ED.Decrypt(strUserName, strData, intEUserType, intType, intSource).Split(',');
            try
            {
                string strUserNameData = data[0];
                int intResellerID = int.Parse(data[1]);
                string strEmailID = data[2];
                string strMobile = data[3];
                int intGender = int.Parse(data[4]);

                string strFullName = data[6];
                int intDesignationType = int.Parse(data[7]);
                string strPassword = "NA";

                strPassword = objComFUN.GenrateNumberString();

                int intCreatedBy = int.Parse(data[8]);
                int intStateID = int.Parse(data[9]);
                string strStateName = data[10];
                string strCityName = data[11];
                string strAddress = data[12];
                int intOperaterID = int.Parse(data[13]);
                string strIPAddress = data[14];
                string strMacAddress = data[15];
                string strOSDetails = data[16];
                string strIMEINo = data[17];
                string strGcmID = data[18];
                string strAPPVersion = data[19];
                int intSourceData = int.Parse(data[20]);
                int intCreatedByType = int.Parse(data[21]);
                int intDistID = int.Parse(data[22]);
                int intActualUserID = int.Parse(data[23]);
                string strAlternateMobile = data[24];
                int intAlternateOperaterID = int.Parse(data[25]);

                try
                {
                    objCreateFile.CreateTextFile(strIPAddress, strMacAddress, strOSDetails, strIMEINo, strGcmID, strAPPVersion, intSourceData.ToString(), intCreatedByType, intActivityType, "SubDistributer Registration,Data:" + strData, strUserName);
                }
                catch (Exception)
                {

                }

                try
                {
                    if (strMobile != strAlternateMobile)
                    {
                        if (objComFUN.CheckRegixMobile(intOperaterID, strMobile) == "TRUE")
                        {

                            if ((objComFUN.CheckRegixMobile(intOperaterID, strMobile) == "TRUE") || (strMobile == "0"))
                            {

                                if ((intResellerID > 0) && (strEmailID != "" || strEmailID != null) && (strMobile != "" || strMobile != null) && (strPassword != "" || strPassword != null) && (strFullName != "" || strFullName != null) && (strStateName != "" || strStateName != null) && (strCityName != "" || strCityName != null) && (strAddress != "" || strAddress != null))
                                {
                                    ds = objODBC.getDataSet("call add_register_memberSubDistributor('" + intResellerID + "','" + strEmailID + "','" + intGender + "','" + strMobile + "','" + strPassword + "','" + strFullName + "','" + intDesignationType + "','" + intCreatedBy + "','" + intStateID + "','" + strStateName + "','" + strCityName + "','" + strAddress + "','" + intOperaterID + "','" + intDistID + "','" + strAlternateMobile + "','" + intAlternateOperaterID + "')");

                                    if (ds.Tables[0].Rows.Count > 0)
                                    {
                                        string intFlag = ds.Tables[0].Rows[0]["Flag"].ToString();
                                        if (intFlag == "200")
                                        {

                                            strResponseMessage = ds.Tables[0].Rows[0]["FeildError"].ToString();
                                            intUserID = Convert.ToInt32(ds.Tables[0].Rows[0]["intUserID"].ToString());
                                            //   passkey = ds.Tables[0].Rows[0]["strPassKey"].ToString();
                                            passkey = objComFUN.GenratePassKey();
                                            string strMemberUserName = ds.Tables[0].Rows[0]["strRandumUserName"].ToString();

                                            string strPasswordDetail = "";
                                            try
                                            {

                                                passkey = objComFUN.GenratePassKey();
                                                strPasswordDetail = objEncrypt.EncryptQueryString(strPassword, passkey);
                                            }
                                            catch (Exception)
                                            {

                                            }


                                            while (strPasswordDetail == "Specified key is not a valid size for this algorithm.")
                                            {
                                                try
                                                {

                                                    passkey = objComFUN.GenratePassKey();
                                                    strPasswordDetail = objEncrypt.EncryptQueryString(strPassword, passkey);
                                                }
                                                catch (Exception)
                                                {


                                                }

                                            }

                                            strSalt1 = objEncrypt.EncryptSalt(GetSalt(13), passkey);
                                            strSalt2 = objEncrypt.EncryptSalt(GetSalt(13), passkey);
                                            strSalt3 = objEncrypt.EncryptSalt(GetSalt(13), passkey);
                                            strEncryptKey = objEncrypt.EncryptQueryString(objComFUN.GenrateRandomKeyString(), passkey);
                                            //strSalt1 = GetSalt(13);
                                            //strSalt2 = GetSalt(13);
                                            //strSalt3 = GetSalt(13);
                                            //strEncryptKey = objComFUN.GenrateRandomKeyString();
                                            string strSMSMPin = objComFUN.GenrateNumber(4);
                                            string strMPin = objEncrypt.EncryptQueryString(strSMSMPin, passkey);
                                            objODBC.executeNonQuery("update er_login set pass_key='" + passkey + "',salt1='" + strSalt1 + "',salt2='" + strSalt2 + "',salt3='" + strSalt3 + "',encryption_key='" + strEncryptKey + "',password='" + strPasswordDetail + "',M_Pin='" + strMPin + "'  where userid='" + intUserID + "'");
                                            try
                                            {
                                                objODBC.executeNonQuery("INSERT INTO `er_login_counter`( `userid`, `count_attempt`) VALUES ('" + intUserID + "',0)");
                                            }
                                            catch (Exception)
                                            {

                                            }
                                            try
                                            {
                                                objODBCLOG.executeNonQuery("INSERT INTO er_login_log (userid,full_name,username,emailid,mobile,usertype,created_on,Created_by_username) VALUES ('" + intUserID + "','" + strFullName + "','" + strMemberUserName + "','" + strEmailID + "','" + strMobile + "','2','" + objDT.getCurDateTimeString() + "','" + strUserName + "')");
                                            }
                                            catch (Exception)
                                            {

                                            }
                                            arrUserParams.Add("TRUE");
                                            arrUserParams.Add(strResponseMessage);
                                            arrUserParams.Add(intUserID);
                                            // SMPP SMS Code Below
                                            SMPP_SMS_Format objSMPP_SMS_Format = new SMPP_SMS_Format();
                                            string strGetSMS = objSMPP_SMS_Format.MemberRegistration(strMemberUserName, strFullName, strPassword, strSMSMPin);
                                           
                                            string strSMPPResponse = "NA";
                                            try
                                            {
                                                try
                                                {
                                                //    objComFUN.WriteToSMSFile(intUserID, 2, strMobile, intOperaterID, strGetSMS, 0, strSMPPResponse, strSMPPResponse, strSMPPResponse, strSMPPResponse, strSMPPResponse);
                                                    objComFUN.er_SMS_Alert(intUserID, 2, strMobile, intOperaterID, strGetSMS, 1, strSMPPResponse);

                                                }
                                                catch (Exception)
                                                {

                                                }                                              
                                            }
                                            catch (Exception)
                                            {

                                            }


                                        }
                                        else
                                        {
                                            strResponseMessage = ds.Tables[0].Rows[0]["FeildError"].ToString();
                                            arrUserParams.Add("FALSE");
                                            arrUserParams.Add(strResponseMessage);
                                        }

                                    }
                                }

                                else
                                {
                                    strResponseMessage = "Member Registration , Kindly Enter Valid Details";
                                    arrUserParams.Add("FALSE");
                                    arrUserParams.Add(strResponseMessage);
                                }
                            }
                            else
                            {
                                strResponseMessage = "Member Registration , Alternate Mobile Number does not belongs to selected operator !";
                                arrUserParams.Add("FALSE");
                                arrUserParams.Add(strResponseMessage);
                            }
                        }
                        else
                        {
                            strResponseMessage = "Member Registration , Mobile Number does not belongs to selected operator !";
                            arrUserParams.Add("FALSE");
                            arrUserParams.Add(strResponseMessage);
                        }
                    }
                    else
                    {
                        strResponseMessage = "Mobile Number and Alternate Number Does not be same ,Kindly Enter Valid Details !";
                        arrUserParams.Add("FALSE");
                        arrUserParams.Add(strResponseMessage);
                    }

                }
                catch (Exception e1)
                {
                    strResponseMessage = "Member Registration, Invalid Details" + e1.ToString();
                    arrUserParams.Add("FALSE");
                    arrUserParams.Add(strResponseMessage);
                }
                finally
                {
                    try
                    {
                        objODBCLOG.executeNonQuery("call activity_logs('" + intActualUserID + "','" + intDesignationType + "','" + intCreatedByType + "','" + strIPAddress + "','" + strMacAddress + "','" + strOSDetails + "','" + strIMEINo + "','" + strGcmID + "','" + strAPPVersion + "','" + intSourceData + "','Member Registration Sub Distributer','" + intActivityType + "','','"+ strUserName + "')");

                        objCreateFile.CreateTextFile(strIPAddress, strMacAddress, strOSDetails, strIMEINo, strGcmID, strAPPVersion, intSourceData.ToString(), intCreatedByType, intActivityType, strResponseMessage, strUserName);
                    }
                    catch (Exception)
                    {

                    }

                    ds.Dispose();
                }
            }
            catch (Exception e1)
            {
                strResponseMessage = "Member Registration, Invalid Details " + e1.ToString();
                arrUserParams.Add("FALSE");
                arrUserParams.Add(strResponseMessage);
            }
            return arrUserParams;
        }



        //string strUserName=0,int intResellerID=1,string strEmailID=2,string strMobile=3,int intGender=4,string strPassword=5,string strFullName=6,int intDesignationType=7,int intCreatedBy=8,int intStateID=9,string strStateName=10, string strCityName=11,string strAddress=12,int intOperaterID=13,string strIPAddress=14, string strMacAddress=15, string strOSDetails=16, string strIMEINo=17,string strGcmID=18, string strAPPVersion=19, int intSource=20, int intCreatedByType=21,int intDistID=22,int intSubDistID=23,int intActualUserID=24,string strAlternateMobile = 25,int intAlternateOperaterID =26;

        // Activity Type=67
        [WebMethod]
        public ArrayList MemberRegistrationRetailer(string strUserName, string strData, int intEUserType, int intType, int intSource)
        {
            string strSalt1 = "", strSalt2 = "", strSalt3 = "", strEncryptKey = "", passkey = "";
            ArrayList arrUserParams = new ArrayList();
            string strResponseMessage = "";
            int intActivityType = 66, intUserID = 0;

            string strMessage = "";
            var data = ED.Decrypt(strUserName, strData, intEUserType, intType, intSource).Split(',');
            try
            {
                string strUserNameData = data[0];
                int intResellerID = int.Parse(data[1]);
                string strEmailID = data[2];
                string strMobile = data[3];
                int intGender = int.Parse(data[4]);

                string strFullName = data[6];
                int intDesignationType = int.Parse(data[7]);
                string strPassword = "NA";

                strPassword = objComFUN.GenrateNumberString();

                int intCreatedBy = int.Parse(data[8]);
                int intStateID = int.Parse(data[9]);
                string strStateName = data[10];
                string strCityName = data[11];
                string strAddress = data[12];
                int intOperaterID = int.Parse(data[13]);
                string strIPAddress = data[14];
                string strMacAddress = data[15];
                string strOSDetails = data[16];
                string strIMEINo = data[17];
                string strGcmID = data[18];
                string strAPPVersion = data[19];
                int intSourceData = int.Parse(data[20]);
                int intCreatedByType = int.Parse(data[21]);
                int intDistID = int.Parse(data[22]);
                int intSubDistID = int.Parse(data[23]);
                int intActualUserID = int.Parse(data[24]);

                string strAlternateMobile = data[25];
                int intAlternateOperaterID = int.Parse(data[26]);
                string strMPin = string.Empty;
                try
                {
                    objCreateFile.CreateTextFile(strIPAddress, strMacAddress, strOSDetails, strIMEINo, strGcmID, strAPPVersion, intSourceData.ToString(), intCreatedByType, intActivityType, "Retailer Registration,Data:" + strData, strUserName);
                }
                catch (Exception)
                {

                }
                try
                {
                    if (strMobile != strAlternateMobile)
                    {
                        if (objComFUN.CheckRegixMobile(intOperaterID, strMobile) == "TRUE")
                        {

                            if ((objComFUN.CheckRegixMobile(intAlternateOperaterID, strAlternateMobile) == "TRUE") || (strMobile == "0"))
                            {
                                if ((intResellerID > 0) && (strEmailID != "" || strEmailID != null) && (strMobile != "" || strMobile != null) && (strPassword != "" || strPassword != null) && (strFullName != "" || strFullName != null) && (strStateName != "" || strStateName != null) && (strCityName != "" || strCityName != null) && (strAddress != "" || strAddress != null))
                                {

                                    ds = objODBC.getDataSet("call add_register_memberRetailer('" + intResellerID + "','" + strEmailID + "','" + intGender + "','" + strMobile + "','" + strPassword + "','" + strFullName + "','" + intDesignationType + "','" + intCreatedBy + "','" + intStateID + "','" + strStateName + "','" + strCityName + "','" + strAddress + "','" + intOperaterID + "','" + intDistID + "','" + intSubDistID + "','" + strAlternateMobile + "','" + intAlternateOperaterID + "')");

                                    if (ds.Tables[0].Rows.Count > 0)
                                    {
                                        string intFlag = ds.Tables[0].Rows[0]["Flag"].ToString();
                                        if (intFlag == "200")
                                        {

                                            strResponseMessage = ds.Tables[0].Rows[0]["FeildError"].ToString();
                                            intUserID = Convert.ToInt32(ds.Tables[0].Rows[0]["intUserID"].ToString());
                                            //   passkey = ds.Tables[0].Rows[0]["strPassKey"].ToString();
                                            passkey = objComFUN.GenratePassKey();
                                            string strMemberUserName = ds.Tables[0].Rows[0]["strRandumUserName"].ToString();
                                            string strPasswordDetail = "";
                                            try
                                            {

                                                passkey = objComFUN.GenratePassKey();
                                                strPasswordDetail = objEncrypt.EncryptQueryString(strPassword, passkey);
                                            }
                                            catch (Exception)
                                            {


                                            }

                                            while (strPasswordDetail == "Specified key is not a valid size for this algorithm.")
                                            {
                                                try
                                                {

                                                    passkey = objComFUN.GenratePassKey();
                                                    strPasswordDetail = objEncrypt.EncryptQueryString(strPassword, passkey);
                                                }
                                                catch (Exception)
                                                {


                                                }

                                            }
                                            strSalt1 = objEncrypt.EncryptSalt(GetSalt(13), passkey);
                                            strSalt2 = objEncrypt.EncryptSalt(GetSalt(13), passkey);
                                            strSalt3 = objEncrypt.EncryptSalt(GetSalt(13), passkey);
                                            strEncryptKey = objEncrypt.EncryptQueryString(objComFUN.GenrateRandomKeyString(), passkey);
                                            //strSalt1 = GetSalt(13);
                                            //strSalt2 = GetSalt(13);
                                            //strSalt3 = GetSalt(13);
                                            //strEncryptKey = objComFUN.GenrateRandomKeyString();

                                            string strSMSMPin = objComFUN.GenrateNumber(4);
                                            strMPin = objEncrypt.EncryptQueryString(strSMSMPin, passkey);
                                            objODBC.executeNonQuery("update er_login set pass_key='" + passkey + "',salt1='" + strSalt1 + "',salt2='" + strSalt2 + "',salt3='" + strSalt3 + "',encryption_key='" + strEncryptKey + "',password='" + strPasswordDetail + "',M_Pin='" + strMPin + "' where userid='" + intUserID + "'");
                                            try
                                            {
                                                objODBC.executeNonQuery("INSERT INTO `er_login_counter`( `userid`, `count_attempt`) VALUES ('" + intUserID + "',0)");
                                            }
                                            catch (Exception)
                                            {

                                            }
                                            try
                                            {
                                                objODBCLOG.executeNonQuery("INSERT INTO er_login_log (userid,full_name,username,emailid,mobile,usertype,created_on,Created_by_username) VALUES ('" + intUserID + "','" + strFullName + "','" + strMemberUserName + "','" + strEmailID + "','" + strMobile + "','2','" + objDT.getCurDateTimeString() + "','" + strUserName + "')");
                                            }
                                            catch (Exception)
                                            {

                                            }
                                            arrUserParams.Add("TRUE");
                                            arrUserParams.Add(strResponseMessage + " ");
                                            arrUserParams.Add(intUserID);


                                            // SMPP SMS Code Below
                                            SMPP_SMS_Format objSMPP_SMS_Format = new SMPP_SMS_Format();
                                            string strGetSMS = objSMPP_SMS_Format.MemberRegistration(strMemberUserName, strFullName, strPassword, strSMSMPin);                                            
                                            string strSMPPResponse = "NA";
                                            try
                                            {

                                                try
                                                {
                                                 //   objComFUN.WriteToSMSFile(intUserID, 2, strMobile, intOperaterID, strGetSMS, 0, strSMPPResponse, strSMPPResponse, strSMPPResponse, strSMPPResponse, strSMPPResponse);
                                                    objComFUN.er_SMS_Alert(intUserID, 2, strMobile, intOperaterID, strGetSMS, 1, strSMPPResponse);

                                                }
                                                catch (Exception)
                                                {

                                                }

                                            }
                                            catch (Exception)
                                            {

                                            }

                                        }
                                        else
                                        {
                                            strResponseMessage = ds.Tables[0].Rows[0]["FeildError"].ToString();
                                            arrUserParams.Add("False");
                                            arrUserParams.Add(strResponseMessage + "");
                                        }

                                    }
                                }
                                else
                                {
                                    strResponseMessage = "Member Registration , Kindly Enter Valid Details ";
                                    arrUserParams.Add("False");
                                    arrUserParams.Add(strResponseMessage);
                                }
                            }
                            else
                            {
                                strResponseMessage = "Member Registration , Alternate Mobile Number does not belongs to selected operator !";
                                arrUserParams.Add("FALSE");
                                arrUserParams.Add(strResponseMessage);
                            }
                        }
                        else
                        {
                            strResponseMessage = "Member Registration , Mobile Number does not belongs to selected operator !";
                            arrUserParams.Add("FALSE");
                            arrUserParams.Add(strResponseMessage);
                        }

                    }
                    else
                    {
                        strResponseMessage = "Mobile Number and Alternate Number Does not be same ,Kindly Enter Valid Details !";
                        arrUserParams.Add("FALSE");
                        arrUserParams.Add(strResponseMessage);
                    }
                }
                catch (Exception e1)
                {
                    strResponseMessage = "Member Registration, Invalid Details " + e1.ToString();
                    arrUserParams.Add("False");
                    arrUserParams.Add(strResponseMessage);
                }
                finally
                {
                    try
                    {
                        objODBCLOG.executeNonQuery("call activity_logs('" + intActualUserID + "','" + intDesignationType + "','" + intCreatedByType + "','" + strIPAddress + "','" + strMacAddress + "','" + strOSDetails + "','" + strIMEINo + "','" + strGcmID + "','" + strAPPVersion + "','" + intSourceData + "','Member Registration Retailer','" + intActivityType + "','','')");
                    }
                    catch (Exception)
                    {

                    }
                    try
                    {
                        objCreateFile.CreateTextFile(strIPAddress, strMacAddress, strOSDetails, strIMEINo, strGcmID, strAPPVersion, intSourceData.ToString(), intCreatedByType, intActivityType, strResponseMessage, strUserName);
                    }
                    catch (Exception)
                    {

                    }

                    ds.Dispose();
                }
            }
            catch (Exception e1)
            {
                strResponseMessage = "Member Registration, Invalid Details " + e1.ToString();
                arrUserParams.Add("False");
                arrUserParams.Add(strResponseMessage);
            }
            return arrUserParams;
        }

        //  int intActivityType = 94
        //string strUserName=0,int intAdminID=1,string strEmailID=2,string strMobile=3,int intGender=4,string strPassword=5,string strFullName=6,int intDesignationType=7,int intCreatedBy=8,int intStateID=9,string strStateName=10, string strCityName=11,string strAddress=12,int intOperaterID=13,string strIPAddress=14, string strMacAddress=15, string strOSDetails=16, string strIMEINo=17,string strGcmID=18, string strAPPVersion=19, int intSource=20, int intCreatedByType=21,string strAlternateMobile=22,int intAlternateOperaterID=23
        [WebMethod]
        public ArrayList AdminRegistration(string strUserName, string strData, int intEUserType, int intType, int intSource)
        {
            string strSalt1 = "", strSalt2 = "", strSalt3 = "", strEncryptKey = "", passkey = "";
            ArrayList arrUserParams = new ArrayList();
            string strResponseMessage = "";
            int intActivityType = 94, intUserID = 0;

            var data = ED.Decrypt(strUserName, strData, intEUserType, intType, intSource).Split(',');
            try
            {
                string strUserNameData = data[0];
                int intAdminID = int.Parse(data[1]);
                string strEmailID = data[2];
                string strMobile = data[3];
                int intGender = int.Parse(data[4]);
                string strPassword = objComFUN.GenrateRandomKeyString();
                string strFullName = data[6];
                int intDesignationType = int.Parse(data[7]);
                int intCreatedBy = int.Parse(data[8]);
                int intStateID = int.Parse(data[9]);
                string strStateName = data[10];
                string strCityName = data[11];
                string strAddress = data[12];
                int intOperaterID = int.Parse(data[13]);
                string strIPAddress = data[14];
                string strMacAddress = data[15];
                string strOSDetails = data[16];
                string strIMEINo = data[17];
                string strGcmID = data[18];
                string strAPPVersion = data[19];
                int intSourceData = int.Parse(data[20]);
                int intCreatedByType = int.Parse(data[21]);
                string strAlternateMobile = data[22];
                int intAlternateOperaterID = int.Parse(data[23]);
                try
                {
                    objCreateFile.CreateTextFile(strIPAddress, strMacAddress, strOSDetails, strIMEINo, strGcmID, strAPPVersion, intSourceData.ToString(), intCreatedByType, 94, "Admin Registration,Data:" + strData, strUserName);
                }
                catch (Exception)
                {

                }
                try
                {
                    if ((intAdminID > 0) && (strEmailID != "" || strEmailID != null) && (strMobile != "" || strMobile != null) && (strPassword != "" || strPassword != null) && (strFullName != "" || strFullName != null) && (strStateName != "" || strStateName != null) && (strCityName != "" || strCityName != null) && (strAddress != "" || strAddress != null))
                    {
                        if (strMobile != strAlternateMobile)
                        {
                            ds = objODBC.getDataSet("call add_register_Admin('" + strEmailID + "','" + strMobile + "','" + strPassword + "'," + intGender + ",'" + strFullName + "'," + intDesignationType + "," + intCreatedBy + "," + intStateID + ",'" + strStateName + "','" + strCityName + "','" + strAddress + "')");

                            if (ds.Tables[0].Rows.Count > 0)
                            {
                                string intFlag = ds.Tables[0].Rows[0]["Flag"].ToString();
                                if (intFlag == "200")
                                {
                                    strResponseMessage = ds.Tables[0].Rows[0]["FeildError"].ToString();
                                    intUserID = Convert.ToInt32(ds.Tables[0].Rows[0]["intUserID"].ToString());
                                    //   passkey = ds.Tables[0].Rows[0]["strPassKey"].ToString();
                                    passkey = objComFUN.GenratePassKey();
                                    string strMemberUserName = ds.Tables[0].Rows[0]["strRandumUserName"].ToString();
                                    string strPasswordDetail = "";
                                    try
                                    {

                                        passkey = objComFUN.GenratePassKey();
                                        strPasswordDetail = objEncrypt.EncryptQueryString(strPassword, passkey);
                                    }
                                    catch (Exception)
                                    {


                                    }

                                    while (strPasswordDetail == "Specified key is not a valid size for this algorithm.")
                                    {
                                        try
                                        {

                                            passkey = objComFUN.GenratePassKey();
                                            strPasswordDetail = objEncrypt.EncryptQueryString(strPassword, passkey);
                                        }
                                        catch (Exception)
                                        {


                                        }

                                    }
                                    strSalt1 = objEncrypt.EncryptSalt(GetSalt(13), passkey);
                                    strSalt2 = objEncrypt.EncryptSalt(GetSalt(13), passkey);
                                    strSalt3 = objEncrypt.EncryptSalt(GetSalt(13), passkey);
                                    strEncryptKey = objEncrypt.EncryptQueryString(objComFUN.GenrateRandomKeyString(), passkey);
                                    //strSalt1 = GetSalt(13);
                                    //strSalt2 = GetSalt(13);
                                    //strSalt3 = GetSalt(13);
                                    //strEncryptKey = objComFUN.GenrateRandomKeyString();
                                    string strSMSMPin = objComFUN.GenrateRandoTpin();
                                    string strTPin = objEncrypt.EncryptQueryString(strSMSMPin, passkey);

                                    objODBC.executeNonQuery("update er_login_admin set tpin='" + strTPin + "',pass_key='" + passkey + "',salt1='" + strSalt1 + "',salt2='" + strSalt2 + "',salt3='" + strSalt3 + "',encryption_key='" + strEncryptKey + "',password='" + strPasswordDetail + "' where userid='" + intUserID + "'");

                                    objODBC.executeNonQuery("INSERT INTO `er_mobile`(`userid`, `mobile`, `operator_id`, `mob_type`, `usertype`) VALUES ('" + intUserID + "','" + strMobile + "','" + intOperaterID + "',1,1)");

                                    if (strAlternateMobile != "0" && intAlternateOperaterID != 0)
                                    {
                                        objODBC.executeNonQuery("INSERT INTO `er_mobile`(`userid`, `mobile`, `operator_id`, `mob_type`, `usertype`) VALUES ('" + intUserID + "','" + strAlternateMobile + "','" + intAlternateOperaterID + "',2,1)");
                                    }


                                    objODBCLOG.executeNonQuery("call activity_logs('" + intUserID + "','" + intDesignationType + "','" + intCreatedByType + "','" + strIPAddress + "','" + strMacAddress + "','" + strOSDetails + "','" + strIMEINo + "','" + strGcmID + "','" + strAPPVersion + "','" + intSourceData + "','Admin Registration',94,'','')");

                                    arrUserParams.Add("TRUE");
                                    arrUserParams.Add(strResponseMessage);

                                    // SMPP SMS Code Below
                                    SMPP_SMS_Format objSMPP_SMS_Format = new SMPP_SMS_Format();
                                    string strGetSMS = objSMPP_SMS_Format.MemberRegistration(strMemberUserName, strFullName, strPassword, strSMSMPin);
                                    SMPP1 objSMPP1 = new SMPP1();
                                    string strSMPPResponse = "NA";
                                    try
                                    {

                                        try
                                        {
                                         //   objComFUN.WriteToSMSFile(intUserID, 2, strMobile, intOperaterID, strGetSMS, 0, strSMPPResponse, strSMPPResponse, strSMPPResponse, strSMPPResponse, strSMPPResponse);
                                            objComFUN.er_SMS_Alert(intUserID, 2, strMobile, intOperaterID, strGetSMS, 1, strSMPPResponse);

                                        }
                                        catch (Exception)
                                        {

                                        }


                                    }
                                    catch (Exception)
                                    {

                                    }
                                }
                                else
                                {
                                    strResponseMessage = ds.Tables[0].Rows[0]["FeildError"].ToString();
                                    arrUserParams.Add("FALSE");
                                    arrUserParams.Add(strResponseMessage);
                                }

                            }

                        }
                        else
                        {
                            strResponseMessage = "Mobile Number and Alternate Number Does not be same ,Kindly Enter Valid Details !";
                            arrUserParams.Add("FALSE");
                            arrUserParams.Add(strResponseMessage);
                        }
                    }
                    else
                    {
                        strResponseMessage = "Admin Registration , Kindly Enter Valid Details !";
                        arrUserParams.Add("FALSE");
                        arrUserParams.Add(strResponseMessage);
                    }

                }
                catch (Exception e1)
                {
                    strResponseMessage = "Admin Registration, Invalid Details" + e1.ToString();
                    arrUserParams.Add("FALSE");
                    arrUserParams.Add(strResponseMessage);
                }
                finally
                {

                    objODBC.executeNonQuery("call activity_logs('" + intUserID + "','" + intDesignationType + "','" + intCreatedByType + "','" + strIPAddress + "','" + strMacAddress + "','" + strOSDetails + "','" + strIMEINo + "','" + strGcmID + "','" + strAPPVersion + "','" + intSourceData + "','" + strResponseMessage + "','" + intActivityType + "','','')");

                    objCreateFile.CreateTextFile(strIPAddress, strMacAddress, strOSDetails, strIMEINo, strGcmID, strAPPVersion, intSourceData.ToString(), intCreatedByType, intActivityType, strResponseMessage, strUserName);

                    ds.Dispose();
                }
            }
            catch (Exception e1)
            {
                strResponseMessage = "Admin Registration, Invalid Details" + e1.ToString();
                arrUserParams.Add("FALSE");
                arrUserParams.Add(strResponseMessage);
            }
            return arrUserParams;
        }

    }
}
