﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Etopup_Ws.old_App_Code;
using System.Data;
using System.Xml;
using System.Text.RegularExpressions;
using Newtonsoft.Json.Linq;

namespace Etopup_Ws
{
    public partial class Recharge_pending_request : System.Web.UI.Page
    {
        DateFormat objDF = new DateFormat();
        ODBC objODBC = new ODBC();
        ODBC_LOG objODBCLOG = new ODBC_LOG();
        DataSet ds = new DataSet();
        DataSet ds1 = new DataSet();
        DataSet dsData = new DataSet();
        CommonFunction objCOMFUN = new CommonFunction();
        protected void Page_Load(object sender, EventArgs e)
        {



            try
            {
                DateFormat objDF = new DateFormat();
                
                try
                {
                  // GetPendingRechargeDetailEtisalat();
                }
                catch (Exception)
                {

                }

            }
            catch (Exception)
            {

            }
            Response.ContentType = "text/plain";
            Response.Write("Exicute Successfully !");
            Response.End();
        }
        public string TruncateAtWord(string input, int length)
        {
            if (input == null || input.Length < length)
                return input;
            int iNextSpace = input.LastIndexOf("", length, StringComparison.Ordinal);
            return string.Format("{0}", input.Substring(0, (iNextSpace > 0) ? iNextSpace : length).Trim());
        }

        public void GetPendingRechargeDetailEtisalat()
        {
            try
            {
                ds = objODBC.getDataSet("Select id,userid,trans_number,operator_id,operator_name,deduct_amt,api_type,Operater_table_id,amount,mobile_number,request_mobile_no from er_recharge where status=1 and api_type=2  order by id Desc limit 30;");
                int intCount = Convert.ToInt32((ds.Tables[0].Rows.Count));
                if (intCount > 0)
                {
                    for (int i = 0; i <= intCount; i++)
                    {
                        int intInvestmentID = int.Parse(ds.Tables[0].Rows[i]["id"].ToString());
                        int intUserID = int.Parse(ds.Tables[0].Rows[i]["userid"].ToString());
                        //  string strTransactionNumber = ds.Tables[0].Rows[i]["trans_number"].ToString();
                        string strTransactionNumber = ds.Tables[0].Rows[i]["id"].ToString();
                        string strOperaterID = ds.Tables[0].Rows[i]["operator_id"].ToString();
                        string strOperaterName = ds.Tables[0].Rows[i]["operator_name"].ToString();
                        double dblDeductAmt = double.Parse(ds.Tables[0].Rows[i]["deduct_amt"].ToString());
                        string api_type = ds.Tables[0].Rows[i]["api_type"].ToString();
                        string strOperaterTableID = ds.Tables[0].Rows[i]["Operater_table_id"].ToString();
                        double dblActualAmt = double.Parse(ds.Tables[0].Rows[i]["amount"].ToString());
                        string strMobile = ds.Tables[0].Rows[i]["mobile_number"].ToString();
                        string strPhone = ds.Tables[0].Rows[i]["request_mobile_no"].ToString();

                        if (api_type == "1") // Salaam
                        {

                        }
                        else if (api_type == "2") // Etisalat
                        {

                            string strQ = "SELECT Transaction_Response FROM etisalat_topup_request WHERE recharge_id =" + intInvestmentID + " order by id desc limit 1";

                            string EtisalatResponse = objODBC.executeScalar_str(strQ);

                            if (EtisalatResponse.ToString() != "NA")
                            {
                                if (Regex.IsMatch(EtisalatResponse, string.Format(@"\b{0}\b", Regex.Escape("ACNT:RETN=0000,DESC=SUCCESS"))))
                                {
                                    string strResposne = TruncateAtWord(EtisalatResponse, 64); // Truncate From Position of Words
                                    string output = EtisalatResponse.Replace(strResposne, ""); // Replace Words From Data
                                    output = output.Remove(output.Length - 8); // Deduct Words from Last                   
                                    string StrCheckSum = EtisalatResponse.Substring(EtisalatResponse.Length - 8); // Getting Checksum

                                    string[] strDescriptionResponse = output.Split(new char[] { ',' });
                                    // strMessage = "2,OOPs! You do not have sufficient balance.,Failed,NA,NA";

                                    string TRDStatus = strDescriptionResponse[0];
                                    string TRDDescription = strDescriptionResponse[1]; ;
                                    string BEFOROPER = strDescriptionResponse[2];
                                    string FEE = strDescriptionResponse[3];
                                    string AFTEROPER = strDescriptionResponse[4];
                                    string CSVSTOP = strDescriptionResponse[5];

                                    string TXNID = objODBC.executeScalar_str("SELECT transaction_ID_recharge FROM etisalat_topup_request WHERE id='" + strOperaterTableID + "' order by id limit 1").ToString();
                                    try
                                    {
                                        objODBC.executeNonQuery("call `updateResponseV3`('" + TXNID + "','" + intInvestmentID + "',1)");
                                    }
                                    catch (Exception)
                                    {

                                    }

                                    try
                                    {
                                        string strBalance = objODBC.executeScalar_str("SELECT amt FROM `er_sys_wallet` where id=2;");
                                        objODBC.executeNonQuery("update er_recharge set comment='" + EtisalatResponse + "',operator_balance='" + strBalance + "' where id='" + intInvestmentID + "'");

                                        objODBC.executeNonQuery("update etisalat_topup_request set status=1 WHERE recharge_id =" + intInvestmentID + "");
                                    }
                                    catch (Exception)
                                    {

                                    }

                                    try
                                    {
                                        objODBC.executeNonQuery("update etisalat_topup_request set transaction_DESC='" + TRDDescription + "',Transactio_Status='" + TRDStatus + "',BEFOROPER='" + BEFOROPER + "',FEE='" + FEE + "',AFTEROPER='" + AFTEROPER + "','" + CSVSTOP + "',Trans_Receive_Checksum='" + StrCheckSum + "',status=1 WHERE recharge_id =" + intInvestmentID + "");
                                    }
                                    catch (Exception)
                                    {

                                    }


                                    try
                                    {
                                        Regex objMobilePatternMTN = new Regex(@"^(77|76)[\d]{7}$");
                                        Regex objMobilePatternAWCC = new Regex(@"^(70|71)[\d]{7}$");


                                        if (objMobilePatternMTN.IsMatch(strMobile))
                                        {
                                            strOperaterName = "MTN";
                                        }
                                        if (objMobilePatternAWCC.IsMatch(strMobile))
                                        {
                                            strOperaterName = "AWCC";
                                        }
                                        try
                                        {
                                            Recharge objRecharge = new Recharge();
                                            objRecharge.SendSMPPSMS(intUserID, objODBC.executeScalar_str("select username FROM `er_login` where userid='" + intUserID + "'"), strMobile, strOperaterName, int.Parse(strOperaterID), dblActualAmt, 1, strTransactionNumber.ToString(), strPhone);
                                        }
                                        catch (Exception)
                                        {

                                        }
                                        try
                                        {
                                            SMPP_SMS_Format objSMPP_SMS_Format = new SMPP_SMS_Format();
                                            CommonFunction objCOMFUN = new CommonFunction();
                                            string strGetSMS = objSMPP_SMS_Format.TopupSuccessEndUser(objODBC.executeScalar_str("SELECT full_name From er_login Where userid='" + intUserID + "'").ToString(), strMobile, dblActualAmt, intInvestmentID.ToString());
                                            objCOMFUN.er_SMS_Alert(intUserID, 2, strMobile, 2, strGetSMS, 9, "");
                                        }
                                        catch (Exception)
                                        { }
                                    }
                                    catch (Exception)
                                    {

                                    }
                                }
                                else if (Regex.IsMatch(EtisalatResponse, string.Format(@"\b{0}\b", Regex.Escape("ACNT:RETN=1099,DESC=S-ACT-00517,Call event rating time out"))))
                                {

                                    string strResposne = TruncateAtWord(EtisalatResponse, 64); // Truncate From Position of Words
                                    string output = EtisalatResponse.Replace(strResposne, ""); // Replace Words From Data
                                    output = output.Remove(output.Length - 8); // Deduct Words from Last                   
                                    string StrCheckSum = EtisalatResponse.Substring(EtisalatResponse.Length - 8); // Getting Checksum

                                    string[] strDescriptionResponse = output.Split(new char[] { ',' });
                                    // strMessage = "2,OOPs! You do not have sufficient balance.,Failed,NA,NA";

                                    string TRDStatus = strDescriptionResponse[0];
                                    string TRDDescription = strDescriptionResponse[1];


                                    string TXNID = objODBC.executeScalar_str("SELECT transaction_ID_recharge FROM etisalat_topup_request WHERE id='" + strOperaterTableID + "' order by id limit 1").ToString();
                                    try { 
                                    objODBC.executeNonQuery("call `updateResponseV3`('" + TXNID + "','" + intInvestmentID + "',2)");
                                    }
                                    catch (Exception)
                                    {

                                    }

                                    try
                                    {
                                        string strBalance = objODBC.executeScalar_str("SELECT amt FROM `er_sys_wallet` where id=2;");
                                        objODBC.executeNonQuery("update er_recharge set comment='" + EtisalatResponse + "',operator_balance='" + strBalance + "' where id='" + intInvestmentID + "'");
                                        objODBC.executeNonQuery("update etisalat_topup_request set status=2 WHERE recharge_id =" + intInvestmentID + "");
                                    }
                                    catch (Exception)
                                    {

                                    }

                                    try
                                    {
                                        objODBC.executeNonQuery("update etisalat_topup_request set transaction_DESC='" + TRDDescription + "',Transactio_Status='" + TRDStatus + "',Trans_Receive_Checksum='" + StrCheckSum + "',status=2 WHERE recharge_id =" + intInvestmentID + "");
                                    }
                                    catch (Exception)
                                    {

                                    }

                                    try
                                    {
                                        try
                                        {
                                            Recharge objRecharge = new Recharge();
                                            objRecharge.SendSMPPSMS(intUserID, objODBC.executeScalar_str("select username FROM `er_login` where userid='" + intUserID + "'"), strMobile, strOperaterName, int.Parse(strOperaterID), dblActualAmt, 2, strTransactionNumber.ToString(), strPhone);
                                        }
                                        catch (Exception)
                                        { }
                                        //try
                                        //{
                                        //    SMPP_SMS_Format objSMPP_SMS_Format = new SMPP_SMS_Format();
                                        //    CommonFunction objCOMFUN = new CommonFunction();
                                        //    string strGetSMS = objSMPP_SMS_Format.TopupFailedEndUser(objODBC.executeScalar_str("SELECT full_name From er_login Where userid='" + intUserID + "'").ToString(), strMobile, dblActualAmt, intInvestmentID.ToString());
                                        //    objCOMFUN.er_SMS_Alert(intUserID, 2, strMobile, 2, strGetSMS, 9, "");
                                        //}
                                        //catch (Exception)
                                        //{

                                        //}
                                    }
                                    catch (Exception)
                                    {

                                    }
                                }
                                else if (Regex.IsMatch(EtisalatResponse, string.Format(@"\b{0}\b", Regex.Escape("ACNT:RETN=1001,DESC=The user does not exist,BKID=0"))))
                                {

                                    string strResposne = TruncateAtWord(EtisalatResponse, 64); // Truncate From Position of Words
                                    string output = EtisalatResponse.Replace(strResposne, ""); // Replace Words From Data
                                    output = output.Remove(output.Length - 8); // Deduct Words from Last                   
                                    string StrCheckSum = EtisalatResponse.Substring(EtisalatResponse.Length - 8); // Getting Checksum

                                    string[] strDescriptionResponse = output.Split(new char[] { ',' });
                                    // strMessage = "2,OOPs! You do not have sufficient balance.,Failed,NA,NA";

                                    string TRDStatus = strDescriptionResponse[0];
                                    string TRDDescription = strDescriptionResponse[1];


                                    string TXNID = objODBC.executeScalar_str("SELECT transaction_ID_recharge FROM etisalat_topup_request WHERE id='" + strOperaterTableID + "' order by id limit 1").ToString();
                                    try { 
                                    objODBC.executeNonQuery("call `updateResponseV3`('" + TXNID + "','" + intInvestmentID + "',2)");
                                    }
                                    catch (Exception)
                                    {

                                    }

                                    try
                                    {
                                        string strBalance = objODBC.executeScalar_str("SELECT amt FROM `er_sys_wallet` where id=2;");
                                        objODBC.executeNonQuery("update er_recharge set comment='" + EtisalatResponse + "',operator_balance='" + strBalance + "' where id='" + intInvestmentID + "'");
                                        objODBC.executeNonQuery("update etisalat_topup_request set status=2 WHERE recharge_id =" + intInvestmentID + "");
                                    }
                                    catch (Exception)
                                    {

                                    }

                                    try
                                    {
                                        objODBC.executeNonQuery("update etisalat_topup_request set transaction_DESC='" + TRDDescription + "',Transactio_Status='" + TRDStatus + "',Trans_Receive_Checksum='" + StrCheckSum + "',status=2 WHERE recharge_id =" + intInvestmentID + "");
                                    }
                                    catch (Exception)
                                    {

                                    }

                                    try
                                    {
                                        try
                                        {
                                            Recharge objRecharge = new Recharge();
                                            objRecharge.SendSMPPSMS(intUserID, objODBC.executeScalar_str("select username FROM `er_login` where userid='" + intUserID + "'"), strMobile, strOperaterName, int.Parse(strOperaterID), dblActualAmt, 2, strTransactionNumber.ToString(), strPhone);
                                        }
                                        catch (Exception)
                                        { }
                                        //try
                                        //{
                                        //    SMPP_SMS_Format objSMPP_SMS_Format = new SMPP_SMS_Format();
                                        //    CommonFunction objCOMFUN = new CommonFunction();
                                        //    string strGetSMS = objSMPP_SMS_Format.TopupFailedEndUser(objODBC.executeScalar_str("SELECT full_name From er_login Where userid='" + intUserID + "'").ToString(), strMobile, dblActualAmt, intInvestmentID.ToString());
                                        //    objCOMFUN.er_SMS_Alert(intUserID, 2, strMobile, 2, strGetSMS, 9, "");
                                        //}
                                        //catch (Exception)
                                        //{

                                        //}
                                    }
                                    catch (Exception)
                                    {

                                    }
                                }

                            }
                            else
                            {
                                string strQ1 = "SELECT all_response FROM etisalat_topup_request WHERE recharge_id =" + intInvestmentID + " order by id desc limit 1";

                                EtisalatResponse = objODBC.executeScalar_str(strQ1);

                                if (Regex.IsMatch(EtisalatResponse, string.Format(@"\b{0}\b", Regex.Escape("ACNT:RETN=0000,DESC=SUCCESS,BEFOROPER"))))
                                {
                                    string strResposne = TruncateAtWord(EtisalatResponse, 64); // Truncate From Position of Words
                                    string output = EtisalatResponse.Replace(strResposne, ""); // Replace Words From Data
                                    output = output.Remove(output.Length - 8); // Deduct Words from Last                   
                                    string StrCheckSum = EtisalatResponse.Substring(EtisalatResponse.Length - 8); // Getting Checksum

                                    string[] strDescriptionResponse = output.Split(new char[] { ',' });
                                    // strMessage = "2,OOPs! You do not have sufficient balance.,Failed,NA,NA";

                                    string TRDStatus = strDescriptionResponse[0];
                                    string TRDDescription = strDescriptionResponse[1]; ;
                                    string BEFOROPER = strDescriptionResponse[2];
                                    string FEE = strDescriptionResponse[3];
                                    string AFTEROPER = strDescriptionResponse[4];
                                    string CSVSTOP = strDescriptionResponse[5];

                                    string TXNID = objODBC.executeScalar_str("SELECT transaction_ID_recharge FROM etisalat_topup_request WHERE id='" + strOperaterTableID + "' order by id limit 1").ToString();
                                    try { 
                                    objODBC.executeNonQuery("call `updateResponseV3`('" + TXNID + "','" + intInvestmentID + "',1)");
                                    }
                                    catch (Exception)
                                    {

                                    }
                                    try
                                    {
                                        string strBalance = objODBC.executeScalar_str("SELECT amt FROM `er_sys_wallet` where id=2;");
                                        objODBC.executeNonQuery("update er_recharge set comment='" + EtisalatResponse + "',operator_balance='" + strBalance + "' where id='" + intInvestmentID + "'");

                                        objODBC.executeNonQuery("update etisalat_topup_request set status=1 WHERE recharge_id =" + intInvestmentID + "");
                                    }
                                    catch (Exception)
                                    {

                                    }

                                    try
                                    {
                                        objODBC.executeNonQuery("update etisalat_topup_request set transaction_DESC='" + TRDDescription + "',Transactio_Status='" + TRDStatus + "',BEFOROPER='" + BEFOROPER + "',FEE='" + FEE + "',AFTEROPER='" + AFTEROPER + "','" + CSVSTOP + "',Trans_Receive_Checksum='" + StrCheckSum + "',status=1 WHERE recharge_id =" + intInvestmentID + "");
                                    }
                                    catch (Exception)
                                    {

                                    }


                                    try
                                    {
                                        Regex objMobilePatternMTN = new Regex(@"^(77|76)[\d]{7}$");
                                        Regex objMobilePatternAWCC = new Regex(@"^(70|71)[\d]{7}$");


                                        if (objMobilePatternMTN.IsMatch(strMobile))
                                        {
                                            strOperaterName = "MTN";
                                        }
                                        if (objMobilePatternAWCC.IsMatch(strMobile))
                                        {
                                            strOperaterName = "AWCC";
                                        }
                                        try
                                        {
                                            Recharge objRecharge = new Recharge();
                                            objRecharge.SendSMPPSMS(intUserID, objODBC.executeScalar_str("select username FROM `er_login` where userid='" + intUserID + "'"), strMobile, strOperaterName, int.Parse(strOperaterID), dblActualAmt, 1, strTransactionNumber.ToString(), strPhone);
                                        }
                                        catch (Exception)
                                        {

                                        }
                                        try
                                        {
                                            SMPP_SMS_Format objSMPP_SMS_Format = new SMPP_SMS_Format();
                                            CommonFunction objCOMFUN = new CommonFunction();
                                            string strGetSMS = objSMPP_SMS_Format.TopupSuccessEndUser(objODBC.executeScalar_str("SELECT full_name From er_login Where userid='" + intUserID + "'").ToString(), strMobile, dblActualAmt, intInvestmentID.ToString());
                                            objCOMFUN.er_SMS_Alert(intUserID, 2, strMobile, 2, strGetSMS, 9, "");
                                        }
                                        catch (Exception)
                                        {

                                        }
                                    }
                                    catch (Exception)
                                    {

                                    }
                                }
                                else if (Regex.IsMatch(EtisalatResponse, string.Format(@"\b{0}\b", Regex.Escape("ACNT:RETN=1099,DESC=S-ACT-00517,Call event rating time out"))))
                                {

                                    string strResposne = TruncateAtWord(EtisalatResponse, 64); // Truncate From Position of Words
                                    string output = EtisalatResponse.Replace(strResposne, ""); // Replace Words From Data
                                    output = output.Remove(output.Length - 8); // Deduct Words from Last                   
                                    string StrCheckSum = EtisalatResponse.Substring(EtisalatResponse.Length - 8); // Getting Checksum

                                    string[] strDescriptionResponse = output.Split(new char[] { ',' });
                                    // strMessage = "2,OOPs! You do not have sufficient balance.,Failed,NA,NA";

                                    string TRDStatus = strDescriptionResponse[0];
                                    string TRDDescription = strDescriptionResponse[1];


                                    string TXNID = objODBC.executeScalar_str("SELECT transaction_ID_recharge FROM etisalat_topup_request WHERE id='" + strOperaterTableID + "' order by id limit 1").ToString();
                                    try { 
                                    objODBC.executeNonQuery("call `updateResponseV3`('" + TXNID + "','" + intInvestmentID + "',2)");
                                    }
                                    catch (Exception)
                                    {

                                    }
                                    try
                                    {
                                        string strBalance = objODBC.executeScalar_str("SELECT amt FROM `er_sys_wallet` where id=2;");
                                        objODBC.executeNonQuery("update er_recharge set comment='" + EtisalatResponse + "',operator_balance='" + strBalance + "' where id='" + intInvestmentID + "'");
                                        objODBC.executeNonQuery("update etisalat_topup_request set status=2 WHERE recharge_id =" + intInvestmentID + "");
                                    }
                                    catch (Exception)
                                    {

                                    }

                                    try
                                    {
                                        objODBC.executeNonQuery("update etisalat_topup_request set transaction_DESC='" + TRDDescription + "',Transactio_Status='" + TRDStatus + "',Trans_Receive_Checksum='" + StrCheckSum + "',status=2 WHERE recharge_id =" + intInvestmentID + "");
                                    }
                                    catch (Exception)
                                    {

                                    }

                                    try
                                    {
                                        try
                                        {
                                            Recharge objRecharge = new Recharge();
                                            objRecharge.SendSMPPSMS(intUserID, objODBC.executeScalar_str("select username FROM `er_login` where userid='" + intUserID + "'"), strMobile, strOperaterName, int.Parse(strOperaterID), dblActualAmt, 2, strTransactionNumber.ToString(), strPhone);
                                        }
                                        catch (Exception)
                                        { }
                                        //try
                                        //{
                                        //    SMPP_SMS_Format objSMPP_SMS_Format = new SMPP_SMS_Format();
                                        //    CommonFunction objCOMFUN = new CommonFunction();
                                        //    string strGetSMS = objSMPP_SMS_Format.TopupFailedEndUser(objODBC.executeScalar_str("SELECT full_name From er_login Where userid='" + intUserID + "'").ToString(), strMobile, dblActualAmt, intInvestmentID.ToString());
                                        //    objCOMFUN.er_SMS_Alert(intUserID, 2, strMobile, 2, strGetSMS, 9, "");
                                        //}
                                        //catch (Exception)
                                        //{

                                        //}
                                    }
                                    catch (Exception)
                                    {

                                    }
                                }
                                
                            }
                        }
                        //else if (api_type == "3") // Roshan
                        //{

                        //}
                        //else if (api_type == "4") // Easy pay
                        //{

                        //    //<? xml version = "1.0" encoding = "utf-8" ?>
                        //    //< request >
                        //    //< request - type > 2 </ request - type >
                        //    //< terminal - id > 347922 </ terminal - id >
                        //    //< login > mutahedapi </ login >
                        //    //< password - md5 > 9b3ee0226cd2556f96eafbbdb1e5f398 </ password - md5 >
                        //    //< payment >
                        //    //< payid > 23577618 </ payid >
                        //    //</ payment >
                        //    //</ request >
                        //    string strPayID = "NA";
                        //    try
                        //    {
                        //        strPayID = objODBC.executeScalar_str("Select payid_responseid from er_easy_pay_request where id='" + strOperaterTableID + "'");
                        //    }
                        //    catch (Exception)
                        //    {

                        //    }
                        //    if (strPayID != "NA")
                        //    {
                        //        EasyPay objEasypay = new EasyPay();
                        //        Encryption objEncrypMD5SHA1 = new Encryption();
                        //        string strPassword = "set2018";
                        //        string strTerminalID = "347922";
                        //        string strLoginID = "mutahedapi";
                        //        string MD5Password = objEncrypMD5SHA1.MD5Hash(strPassword);

                        //        var EasyPayResponse = objEasypay.ESPY_Transaction_Pending(int.Parse(strOperaterTableID), intInvestmentID, strTerminalID, MD5Password, "2", strLoginID, strPayID);


                        //        //   objODBC.executeNonQuery("update er_recharge set comment='" + EasyPayResponse.ToString() + "' where id='" + intInvestmentID + "'");
                        //        XmlDocument xmlDoc = new XmlDocument();
                        //        xmlDoc.LoadXml(EasyPayResponse);
                        //        // objODBC.executeNonQuery(" insert into temp(`data`,`type`,`created_on`,`request`) values('1',1,'2019-01-29 19:58:58','5')");
                        //        XmlNode paymentItem = xmlDoc.DocumentElement.ChildNodes[0];

                        //        string strStatusCode = paymentItem.Attributes["status-code"].Value.ToString();
                        //        string strStatuTxnDate = paymentItem.Attributes["txn-date"].Value.ToString();
                        //        string strPayStatus = "NA";



                        //        try
                        //        {
                        //            strPayStatus = paymentItem.Attributes["payment-state"].Value;
                        //            strPayID = paymentItem.Attributes["payid"].Value.ToString();
                        //        }
                        //        catch (Exception)
                        //        {

                        //        }
                        //        if (strPayStatus == "1") // Success
                        //        {
                        //            try { 
                        //            objODBC.executeNonQuery("call `updateResponseV3`('" + strPayID + "','" + intInvestmentID + "',1)");
                        //            }
                        //            catch (Exception)
                        //            {

                        //            }
                        //            //    objODBC.executeNonQuery(" insert into temp(`data`,`type`,`created_on`,`request`) values(" + strPayID + "," + intInvestmentID + ",'2019-01-29 19:58:58','5')");


                        //            objODBC.executeNonQuery("update er_recharge set comment='" + EasyPayResponse + "',ap_transid='" + strPayID + "',operator_transid='" + strPayID + "' where id='" + intInvestmentID + "'");



                        //            string strStatusDescription = "N/A";
                        //            try
                        //            {
                        //                strStatusDescription = paymentItem.Attributes["payid"].Value;
                        //            }
                        //            catch (Exception)
                        //            {

                        //            }

                        //            string strRequestType_P = "N/A";
                        //            string strTerminalIDRes_P = "N/A";
                        //            double strCurrencyRate_P = 0;
                        //            double strBalance_P = 0;
                        //            double strOverdraft_P = 0;
                        //            double strWithheld_P = 0;
                        //            double strPayable_P = 0;

                        //            try
                        //            {
                        //                strRequestType_P = xmlDoc.DocumentElement.ChildNodes[1].ChildNodes[0].Value;
                        //                strTerminalIDRes_P = xmlDoc.DocumentElement.ChildNodes[2].ChildNodes[0].Value;
                        //                strCurrencyRate_P = double.Parse(xmlDoc.DocumentElement.ChildNodes[3].ChildNodes[0].Value);
                        //                strBalance_P = double.Parse(xmlDoc.DocumentElement.ChildNodes[4].ChildNodes[0].Value);
                        //                strOverdraft_P = double.Parse(xmlDoc.DocumentElement.ChildNodes[5].ChildNodes[0].Value);
                        //                strWithheld_P = double.Parse(xmlDoc.DocumentElement.ChildNodes[6].ChildNodes[0].Value);
                        //                strPayable_P = double.Parse(xmlDoc.DocumentElement.ChildNodes[7].ChildNodes[0].Value);
                        //            }
                        //            catch (Exception)
                        //            {

                        //            }

                        //            try
                        //            {
                        //                objODBC.executeNonQuery("update er_recharge set operator_balance='" + strBalance_P + "' where id='" + intInvestmentID + "'");
                        //            }
                        //            catch (Exception)
                        //            {

                        //            }
                        //            try
                        //            {
                        //                objODBC.executeNonQuery("UPDATE `er_easy_pay_request` set `terminal_id`='" + strTerminalIDRes_P + "', `request_type`='" + strRequestType_P + "', `net_amount`=0, `payid_responseid`='" + strPayID + "', `payment_state`='" + strPayStatus + "', `status_code`='" + strStatusCode + "', `status_desc`='', `txn_date`='" + strStatuTxnDate + "',`currency_rate`='" + strCurrencyRate_P + "', `balance`='" + strBalance_P + "', `overdraft`='" + strOverdraft_P + "', `withheld`='" + strWithheld_P + "', `payable`='" + strPayable_P + "', `Response`='" + EasyPayResponse + "',`status`=1 where id='" + strOperaterTableID + "'");
                        //                // Fun SMS
                        //            }
                        //            catch
                        //            {

                        //            }

                        //            try
                        //            {
                        //                Recharge objRecharge = new Recharge();
                        //                objRecharge.SendSMPPSMS(intUserID, objODBC.executeScalar_str("select username FROM `er_login` where userid='" + intUserID + "'"), strMobile, strOperaterName, int.Parse(strOperaterID), dblActualAmt, 1, strTransactionNumber.ToString(), strPhone);
                        //            }
                        //            catch (Exception)
                        //            {

                        //            }

                        //        }

                        //        else if (strPayStatus == "0")  // Pending
                        //        {


                        //        }
                        //        else if (strPayStatus == "2") // Failed
                        //        {
                        //            objODBC.executeNonQuery("call `updateResponseV3`('" + strPayID + "','" + intInvestmentID + "',2)");

                        //            string strRequestType_F = "N/A";
                        //            string strTerminalIDRes_F = "N/A";
                        //            string strDiscription_F = "N/A";
                        //            try
                        //            {
                        //                strRequestType_F = xmlDoc.DocumentElement.ChildNodes[1].ChildNodes[0].Value.ToString();
                        //                strTerminalIDRes_F = xmlDoc.DocumentElement.ChildNodes[2].ChildNodes[0].Value.ToString();
                        //                strDiscription_F = paymentItem.Attributes["status-desc"].Value.ToString();
                        //            }
                        //            catch (Exception)
                        //            {

                        //            }
                        //            try
                        //            {
                        //                objODBC.executeNonQuery("UPDATE `er_easy_Pay_request` set `payid_responseid`='" + strPayID + "',`terminal_id`='" + strTerminalIDRes_F + "', `request_type`='" + strRequestType_F + "', `net_amount`=0,`payment_state`='" + strPayStatus + "', `status_code`='" + strStatusCode + "', `status_desc`='" + strDiscription_F + "', `txn_date`='" + strStatuTxnDate + "',`Response`='" + EasyPayResponse + "',`status`=2 where id='" + strOperaterTableID + "'");
                        //            }
                        //            catch (Exception)
                        //            {
                        //            }

                        //            try
                        //            {
                        //                Recharge objRecharge = new Recharge();
                        //                objRecharge.SendSMPPSMS(intUserID, objODBC.executeScalar_str("select username FROM `er_login` where userid='" + intUserID + "'"), strMobile, strOperaterName, int.Parse(strOperaterID), dblActualAmt, 2, strTransactionNumber.ToString(), strPhone);
                        //            }
                        //            catch (Exception)
                        //            {

                        //            }

                        //        }
                        //        else
                        //        {
                        //            objODBC.executeNonQuery("call `updateResponseV3`('" + strPayID + "','" + intInvestmentID + "',2)");
                        //            objODBC.executeNonQuery("update er_recharge set ap_transid='" + strPayID + "',operator_transid='" + strPayID + "' where id='" + intInvestmentID + "'");

                        //            string strRequestType_F = "NA";
                        //            string strTerminalIDRes_F = "NA";
                        //            string strCurrencyRate_F = "NA";
                        //            string strBalance_F = "NA";
                        //            string strOverdraft_F = "NA";
                        //            string strWithheld_F = "NA";
                        //            string strPayable_F = "NA";
                        //            try
                        //            {
                        //                strRequestType_F = xmlDoc.DocumentElement.ChildNodes[1].ChildNodes[0].Value.ToString();
                        //                strTerminalIDRes_F = xmlDoc.DocumentElement.ChildNodes[2].ChildNodes[0].Value.ToString();
                        //                strCurrencyRate_F = xmlDoc.DocumentElement.ChildNodes[3].ChildNodes[0].Value.ToString();
                        //                strBalance_F = xmlDoc.DocumentElement.ChildNodes[4].ChildNodes[0].Value.ToString();
                        //                strOverdraft_F = xmlDoc.DocumentElement.ChildNodes[5].ChildNodes[0].Value.ToString();
                        //                strWithheld_F = xmlDoc.DocumentElement.ChildNodes[6].ChildNodes[0].Value.ToString();
                        //                strPayable_F = xmlDoc.DocumentElement.ChildNodes[7].ChildNodes[0].Value.ToString();
                        //            }
                        //            catch (Exception)
                        //            {

                        //            }

                        //            try
                        //            {
                        //                objODBC.executeNonQuery("update er_recharge set operator_balance='" + strBalance_F + "' where id='" + intInvestmentID + "'");
                        //            }
                        //            catch (Exception)
                        //            {

                        //            }
                        //            try
                        //            {
                        //                objODBC.executeNonQuery("UPDATE `er_easy_Pay_request` set `terminal_id`='" + strTerminalIDRes_F + "', `request_type`='" + strRequestType_F + "', `net_amount`=0, `payid_responseid`='" + strPayID + "', `payment_state`='" + strPayStatus + "', `status_code`='" + strStatusCode + "', `status_desc`='', `txn_date`='" + strStatuTxnDate + "',`currency_rate`='" + strCurrencyRate_F + "', `balance`='" + strBalance_F + "', `overdraft`='" + strOverdraft_F + "', `withheld`='" + strWithheld_F + "', `payable`='" + strPayable_F + "', `Response`='" + EasyPayResponse + "',`status`=1 where id='" + strOperaterTableID + "'");
                        //            }
                        //            catch (Exception)
                        //            {

                        //            }
                        //            try
                        //            {
                        //                Recharge objRecharge = new Recharge();
                        //                objRecharge.SendSMPPSMS(intUserID, objODBC.executeScalar_str("select username FROM `er_login` where userid='" + intUserID + "'"), strMobile, strOperaterName, int.Parse(strOperaterID), dblActualAmt, 2, strTransactionNumber.ToString(), strPhone);
                        //            }
                        //            catch (Exception)
                        //            {

                        //            }


                        //        }
                        //    }

                        //}
                        //else if (api_type == "5") // Boloro
                        //{
                        //    //  { "response_code":1,"response_desc":"Operation successful","data":{ "transaction_no":"5e09b2c14d388","status_code":"160","status_desc":"In Process Initiating Topup request","msisdn":"93778881117","amount":"13","sequence_no":"5e09b2f91711c"} }
                        //    //   { "response_code":1,"response_desc":"Operation successful","data":{ "transaction_no":"5e09c76598718","status_code":"170","status_desc":"In Process Topup Successful","msisdn":"93778881117","amount":"13","sequence_no":"5e09c788b6c9f"} }
                        //    //{"response_code":2,"response_desc":"Operation failed","data":{"errors":["AWCC telco is currently disabled."],"sequence_no":"5e4d099ed6ca1"}}

                        //    string strTransactionID = objODBC.executeScalar_str("SELECT transaction_no FROM er_boloro_api WHERE recharge_id =" + intInvestmentID + " order by id desc limit 1");
                        //    BoloroApi objBoloroAPi = new BoloroApi();
                        //    if (strTransactionID != "")
                        //    {
                        //        string BolotoResponse = objBoloroAPi.Boloro_Check_Transaction(strTransactionID, intUserID, intInvestmentID, strOperaterTableID);
                        //        try
                        //        {
                        //            string strRequest_Code = "";
                        //            string strResponse_desc = "";
                        //            string cttransaction_no = "";
                        //            string ctstatus_code = "";
                        //            string ctstatus_desc = "";
                        //            string ctmsisdn = "";
                        //            string ctamount = "";
                        //            string ctsequence_no = "";

                        //            if (BolotoResponse.ToString() != "NA")
                        //            {
                        //                try
                        //                {
                        //                    dynamic Tdata = JObject.Parse(BolotoResponse);
                        //                    strRequest_Code = Tdata.response_code;
                        //                    strResponse_desc = Tdata.response_desc;
                        //                    // string strResponseData = data.data;
                        //                    cttransaction_no = Tdata["data"]["transaction_no"].ToString();
                        //                    ctstatus_code = Tdata["data"]["status_code"].ToString();
                        //                    ctstatus_desc = Tdata["data"]["status_desc"].ToString();
                        //                    ctmsisdn = Tdata["data"]["msisdn"].ToString();
                        //                    ctamount = Tdata["data"]["amount"].ToString();
                        //                    ctsequence_no = Tdata["data"]["sequence_no"].ToString();

                        //                }
                        //                catch (Exception)
                        //                {

                        //                }



                        //                if (ctstatus_code == "170" && ctstatus_desc == "In Process Topup Successful")
                        //                {
                        //                    objODBC.executeNonQuery("call `updateResponseV3`('" + cttransaction_no + "','" + intInvestmentID + "',1)");
                        //                    //    objODBC.executeNonQuery(" insert into temp(`data`,`type`,`created_on`,`request`) values(" + strPayID + "," + intInvestmentID + ",'2019-01-29 19:58:58','5')");

                        //                    BolotoResponse = "Response=@" + BolotoResponse.ToString();
                        //                    objODBC.executeNonQuery("update er_recharge set comment='" + BolotoResponse + "',ap_transid='" + cttransaction_no + "',operator_transid='" + cttransaction_no + "' where id='" + intInvestmentID + "'");

                        //                    objODBC.executeNonQuery("Update er_boloro_api set `check_t_response`='" + BolotoResponse + "',`transaction_no`='" + strTransactionID + "',`modifyed_on`='" + objDF.getCurDateTimeString() + "',`check_t_response_code`='" + strRequest_Code + "',`check_t_response_desc`='" + strResponse_desc + "',`check_t_transaction_no`='" + cttransaction_no + "',`check_t_status_code`='" + ctstatus_code + "',`check_t_status_desc`='" + ctstatus_desc + "',`check_t_response_sequence_no`='" + ctsequence_no + "',`status`=1 where id='" + strOperaterTableID + "'");

                        //                    try
                        //                    {
                        //                        Recharge objRecharge = new Recharge();
                        //                        objRecharge.SendSMPPSMS(intUserID, objODBC.executeScalar_str("select username FROM `er_login` where userid='" + intUserID + "'"), strMobile, strOperaterName, int.Parse(strOperaterID), dblActualAmt, 1, strTransactionNumber.ToString(), strPhone);

                        //                        if (strOperaterID == "4")
                        //                        {
                        //                            SMPP_SMS_Format objSMPP_SMS_Format = new SMPP_SMS_Format();
                        //                            CommonFunction objCOMFUN = new CommonFunction();
                        //                            string strGetSMS = objSMPP_SMS_Format.TopupSuccessEndUser(objODBC.executeScalar_str("SELECT full_name From er_login Where userid='" + intUserID + "'").ToString(), strMobile, dblActualAmt, strTransactionNumber.ToString());
                        //                            objCOMFUN.er_SMS_Alert(intUserID, 2, strMobile, int.Parse(strOperaterID), strGetSMS, 9, "");
                        //                        }
                        //                    }
                        //                    catch (Exception)
                        //                    {

                        //                    }

                        //                }
                        //                else if (ctstatus_code == "1000" && ctstatus_desc == "Closed (Completed Successfully)")
                        //                {
                        //                    objODBC.executeNonQuery("call `updateResponseV3`('" + cttransaction_no + "','" + intInvestmentID + "',1)");
                        //                    //    objODBC.executeNonQuery(" insert into temp(`data`,`type`,`created_on`,`request`) values(" + strPayID + "," + intInvestmentID + ",'2019-01-29 19:58:58','5')");

                        //                    BolotoResponse = "Response=@" + BolotoResponse.ToString();
                        //                    objODBC.executeNonQuery("update er_recharge set comment='" + BolotoResponse + "',ap_transid='" + cttransaction_no + "',operator_transid='" + cttransaction_no + "' where id='" + intInvestmentID + "'");

                        //                    objODBC.executeNonQuery("Update er_boloro_api set `check_t_response`='" + BolotoResponse + "',`transaction_no`='" + strTransactionID + "',`modifyed_on`='" + objDF.getCurDateTimeString() + "',`check_t_response_code`='" + strRequest_Code + "',`check_t_response_desc`='" + strResponse_desc + "',`check_t_transaction_no`='" + cttransaction_no + "',`check_t_status_code`='" + ctstatus_code + "',`check_t_status_desc`='" + ctstatus_desc + "',`check_t_response_sequence_no`='" + ctsequence_no + "',`status`=1 where id='" + strOperaterTableID + "'");

                        //                    try
                        //                    {
                        //                        Recharge objRecharge = new Recharge();
                        //                        objRecharge.SendSMPPSMS(intUserID, objODBC.executeScalar_str("select username FROM `er_login` where userid='" + intUserID + "'"), strMobile, strOperaterName, int.Parse(strOperaterID), dblActualAmt, 1, strTransactionNumber.ToString(), strPhone);

                        //                        if (strOperaterID == "4")
                        //                        {
                        //                            SMPP_SMS_Format objSMPP_SMS_Format = new SMPP_SMS_Format();
                        //                            CommonFunction objCOMFUN = new CommonFunction();
                        //                            string strGetSMS = objSMPP_SMS_Format.TopupSuccessEndUser(objODBC.executeScalar_str("SELECT full_name From er_login Where userid='" + intUserID + "'").ToString(), strMobile, dblActualAmt, strTransactionNumber.ToString());
                        //                            objCOMFUN.er_SMS_Alert(intUserID, 2, strMobile, int.Parse(strOperaterID), strGetSMS, 9, "");
                        //                        }
                        //                    }
                        //                    catch (Exception)
                        //                    {

                        //                    }

                        //                }
                        //                else if (int.Parse(ctstatus_code) > 1000)
                        //                {
                        //                    objODBC.executeNonQuery("call `updateResponseV3`('" + cttransaction_no + "','" + intInvestmentID + "',2)");

                        //                    BolotoResponse = "Response=@" + BolotoResponse.ToString();
                        //                    objODBC.executeNonQuery("update er_recharge set comment='" + BolotoResponse + "',ap_transid='" + cttransaction_no + "',operator_transid='" + cttransaction_no + "' where id='" + intInvestmentID + "'");

                        //                    objODBC.executeNonQuery("Update er_boloro_api set `check_t_request`='NA',`check_t_response`='" + BolotoResponse + "',`transaction_no`='" + strTransactionID + "',`modifyed_on`='" + objDF.getCurDateTimeString() + "',`check_t_response_code`='" + strRequest_Code + "',`check_t_response_desc`='" + strResponse_desc + "',`check_t_transaction_no`='" + cttransaction_no + "',`check_t_status_code`='" + ctstatus_code + "',`check_t_status_desc`='" + ctstatus_desc + "',`check_t_response_sequence_no`='" + ctsequence_no + "',`status`=2 where id='" + strOperaterTableID + "'");

                        //                    try
                        //                    {
                        //                        Recharge objRecharge = new Recharge();
                        //                        objRecharge.SendSMPPSMS(intUserID, objODBC.executeScalar_str("select username FROM `er_login` where userid='" + intUserID + "'"), strMobile, strOperaterName, int.Parse(strOperaterID), dblActualAmt, 2, strTransactionNumber.ToString(), strPhone);

                        //                        //if (strOperaterID == "4")
                        //                        //{
                        //                        //    SMPP_SMS_Format objSMPP_SMS_Format = new SMPP_SMS_Format();
                        //                        //    CommonFunction objCOMFUN = new CommonFunction();
                        //                        //    string strGetSMS = objSMPP_SMS_Format.TopupFailedEndUser(objODBC.executeScalar_str("SELECT full_name From er_login Where userid='" + intUserID + "'").ToString(), strMobile, dblActualAmt, strTransactionNumber.ToString());
                        //                        //    objCOMFUN.er_SMS_Alert(intUserID, 2, strMobile, int.Parse(strOperaterID), strGetSMS, 9, "");
                        //                        //}
                        //                    }
                        //                    catch (Exception)
                        //                    {

                        //                    }
                        //                }


                        //            }
                        //        }
                        //        catch (Exception)
                        //        {

                        //        }
                        //    }
                        //}

                    }
                }
            }
            catch (Exception e1)
            {
                Response.ContentType = "text/plain";
                Response.Write("Exicute Successfully !" + e1);
                Response.End();
            }
            finally
            {
                ds.Dispose();
            }
        }


        public void PendingSMSsendAgain()
        {
            try
            {

                try
                {
                    dsData = objODBCLOG.getDataSet("SELECT `id`, `userid`, `username`, `mobile`, `operator_id`, `SMS`, `created_on`, `status`, `mobile1`, `operator_id1`, `mobile2`, `operator_id2` FROM `er_wallet_transfer_sms_log` WHERE status=0 order by id asc limit 150;");
                    if (dsData.Tables[0].Rows.Count > 0)
                    {
                        for (int i = 0; i < dsData.Tables[0].Rows.Count; i++)
                        {

                            string strSMPPResponse = "NA";
                            try
                            {
                                objCOMFUN.WriteToSMSFile(int.Parse(dsData.Tables[0].Rows[i]["userid"].ToString()), 2, dsData.Tables[0].Rows[i]["mobile"].ToString(), int.Parse(dsData.Tables[0].Rows[i]["operator_id"].ToString()), dsData.Tables[0].Rows[i]["SMS"].ToString(), 1, strSMPPResponse, dsData.Tables[0].Rows[i]["mobile1"].ToString(), dsData.Tables[0].Rows[i]["operator_id1"].ToString(), dsData.Tables[0].Rows[i]["mobile2"].ToString(), dsData.Tables[0].Rows[i]["operator_id2"].ToString());

                                // objComFUN.er_SMS_Alert(int.Parse(dsData.Tables[0].Rows[i]["userid"].ToString()), 2, dsData.Tables[0].Rows[i]["mobile"].ToString(), int.Parse(dsData.Tables[0].Rows[i]["operator_id1"].ToString()),dsData.Tables[0].Rows[i]["SMS"].ToString(), 1, strSMPPResponse);

                                objODBCLOG.executeNonQuery("Update er_wallet_transfer_sms_log set status=1 where id=" + int.Parse(dsData.Tables[0].Rows[i]["id"].ToString()));
                            }
                            catch (Exception)
                            {

                            }
                        }
                    }

                }
                catch (Exception)
                {

                }
                finally
                {
                    dsData.Dispose();
                }
            }
            catch (Exception)
            {


            }
            finally
            {
                dsData.Dispose();
            }
        }
    }
}