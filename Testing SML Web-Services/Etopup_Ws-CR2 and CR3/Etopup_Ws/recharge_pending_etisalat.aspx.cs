﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Etopup_Ws.old_App_Code;
using System.Data;
using System.Xml;
using System.Text.RegularExpressions;
using Newtonsoft.Json.Linq;

namespace Etopup_Ws
{
    public partial class recharge_pending_etisalat : System.Web.UI.Page
    {
        DateFormat objDF = new DateFormat();
        ODBC objODBC = new ODBC();
        ODBC_LOG objODBCLOG = new ODBC_LOG();
        DataSet ds = new DataSet();
        DataSet ds1 = new DataSet();
        DataSet dsData = new DataSet();
        CommonFunction objCOMFUN = new CommonFunction();
        protected void Page_Load(object sender, EventArgs e)
        {

            try
            {


                try
                {
                  
                    //  objODBC.executeNonQuery("INSERT INTO `temp`(`data`, `type`, `created_on`, `request`) VALUES ('Windows Services ',1,'" + objDF.getCurDateTimeString() + "','Recharge_pending_request')");
                    try
                    {
                      //  GetPendingRechargeDetails();
                    }
                    catch (Exception)
                    {

                    }
                    try
                    {
                       // GetPendingRechargeDetails_Boloro();
                    }
                    catch (Exception)
                    {


                    }

                    // PendingSMSsendAgain();
                }
                catch (Exception)
                {

                }

            }
            catch (Exception)
            {

            }
            Response.ContentType = "text/plain";
            Response.Write("Exicute Successfully !");
            Response.End();
        }
        public string TruncateAtWord(string input, int length)
        {
            if (input == null || input.Length < length)
                return input;
            int iNextSpace = input.LastIndexOf("", length, StringComparison.Ordinal);
            return string.Format("{0}", input.Substring(0, (iNextSpace > 0) ? iNextSpace : length).Trim());
        }

        public void GetPendingRechargeDetails()
        {
            try
            {
                string strDate2 = objDF.getCurDateTimeString();
                ds = objODBC.getDataSet("Select id,userid,trans_number,operator_id,operator_name,deduct_amt,api_type,Operater_table_id,amount,mobile_number,request_mobile_no from er_recharge where status=1 and api_type=2 And created_on < DATE_SUB('" + strDate2 + "',INTERVAL 3 MINUTE)  order by id ASC limit 100;");
                int intCount = Convert.ToInt32((ds.Tables[0].Rows.Count));
                if (intCount > 0)
                {
                    for (int i = 0; i <= intCount; i++)
                    {
                        int intInvestmentID = int.Parse(ds.Tables[0].Rows[i]["id"].ToString());
                        int intUserID = int.Parse(ds.Tables[0].Rows[i]["userid"].ToString());
                        string strTransactionNumber = ds.Tables[0].Rows[i]["id"].ToString();
                        string strOperaterID = ds.Tables[0].Rows[i]["operator_id"].ToString();
                        string strOperaterName = ds.Tables[0].Rows[i]["operator_name"].ToString();
                        double dblDeductAmt = double.Parse(ds.Tables[0].Rows[i]["deduct_amt"].ToString());
                        string api_type = ds.Tables[0].Rows[i]["api_type"].ToString();
                        string strOperaterTableID = ds.Tables[0].Rows[i]["Operater_table_id"].ToString();
                        double dblActualAmt = double.Parse(ds.Tables[0].Rows[i]["amount"].ToString());
                        string strMobile = ds.Tables[0].Rows[i]["mobile_number"].ToString();
                        string strPhone = ds.Tables[0].Rows[i]["request_mobile_no"].ToString();

                        if (api_type == "2") // Etisalat
                        {

                            string strQ = "SELECT Transaction_Response FROM etisalat_topup_request WHERE recharge_id =" + intInvestmentID + " order by id desc limit 1";

                            string EtisalatResponse = objODBC.executeScalar_str(strQ);

                            if (EtisalatResponse.ToString() != "NA")
                            {
                                if (Regex.IsMatch(EtisalatResponse, string.Format(@"\b{0}\b", Regex.Escape("ACNT:RETN=0000,DESC=SUCCESS"))))
                                {
                                    string strResposne = TruncateAtWord(EtisalatResponse, 64); // Truncate From Position of Words
                                    string output = EtisalatResponse.Replace(strResposne, ""); // Replace Words From Data
                                    output = output.Remove(output.Length - 8); // Deduct Words from Last                   
                                    string StrCheckSum = EtisalatResponse.Substring(EtisalatResponse.Length - 8); // Getting Checksum

                                    string[] strDescriptionResponse = output.Split(new char[] { ',' });
                                    // strMessage = "2,OOPs! You do not have sufficient balance.,Failed,NA,NA";

                                    string TRDStatus = strDescriptionResponse[0];
                                    string TRDDescription = strDescriptionResponse[1]; ;
                                    string BEFOROPER = strDescriptionResponse[2];
                                    string FEE = strDescriptionResponse[3];
                                    string AFTEROPER = strDescriptionResponse[4];
                                    string CSVSTOP = strDescriptionResponse[5];

                                    string TXNID = objODBC.executeScalar_str("SELECT transaction_ID_recharge FROM etisalat_topup_request WHERE id='" + strOperaterTableID + "' order by id limit 1").ToString();
                                    try { 
                                    objODBC.executeNonQuery("call `updateResponseV3`('" + TXNID + "','" + intInvestmentID + "',1)");
                                    }
                                    catch (Exception)
                                    {

                                    }
                                    try
                                    {
                                        string strBalance = objODBC.executeScalar_str("SELECT amt FROM `er_sys_wallet` where id=2;");
                                        objODBC.executeNonQuery("update er_recharge set comment='" + EtisalatResponse + "',operator_balance='" + strBalance + "' where id='" + intInvestmentID + "'");

                                        objODBC.executeNonQuery("update etisalat_topup_request set status=1 WHERE recharge_id =" + intInvestmentID + "");
                                    }
                                    catch (Exception)
                                    {

                                    }

                                    try
                                    {
                                        objODBC.executeNonQuery("update etisalat_topup_request set transaction_DESC='" + TRDDescription + "',Transactio_Status='" + TRDStatus + "',BEFOROPER='" + BEFOROPER + "',FEE='" + FEE + "',AFTEROPER='" + AFTEROPER + "','" + CSVSTOP + "',Trans_Receive_Checksum='" + StrCheckSum + "',status=1 WHERE recharge_id =" + intInvestmentID + "");
                                    }
                                    catch (Exception)
                                    {

                                    }


                                    try
                                    {
                                        Regex objMobilePatternMTN = new Regex(@"^(77|76)[\d]{7}$");
                                        Regex objMobilePatternAWCC = new Regex(@"^(70|71)[\d]{7}$");


                                        if (objMobilePatternMTN.IsMatch(strMobile))
                                        {
                                            strOperaterName = "MTN";
                                        }
                                        if (objMobilePatternAWCC.IsMatch(strMobile))
                                        {
                                            strOperaterName = "AWCC";
                                        }
                                        try
                                        {
                                            Recharge objRecharge = new Recharge();
                                            objRecharge.SendSMPPSMS(intUserID, objODBC.executeScalar_str("select username FROM `er_login` where userid='" + intUserID + "'"), strMobile, strOperaterName, int.Parse(strOperaterID), dblActualAmt, 1, strTransactionNumber.ToString(), strPhone);
                                        }
                                        catch (Exception)
                                        {

                                        }
                                        try
                                        {
                                            SMPP_SMS_Format objSMPP_SMS_Format = new SMPP_SMS_Format();
                                            CommonFunction objCOMFUN = new CommonFunction();
                                            string strGetSMS = objSMPP_SMS_Format.TopupSuccessEndUser(objODBC.executeScalar_str("SELECT full_name From er_login Where userid='" + intUserID + "'").ToString(), strMobile, dblActualAmt, intInvestmentID.ToString());
                                            objCOMFUN.er_SMS_Alert(intUserID, 2, strMobile, 2, strGetSMS, 9, "");
                                        }
                                        catch (Exception)
                                        { }
                                    }
                                    catch (Exception)
                                    {

                                    }
                                }
                               
                            }
                            else
                            {
                                string strQ1 = "SELECT all_response FROM etisalat_topup_request WHERE recharge_id =" + intInvestmentID + " order by id desc limit 1";

                                EtisalatResponse = objODBC.executeScalar_str(strQ1);

                                if (Regex.IsMatch(EtisalatResponse, string.Format(@"\b{0}\b", Regex.Escape("ACNT:RETN=0000,DESC=SUCCESS,BEFOROPER"))))
                                {
                                    string strResposne = TruncateAtWord(EtisalatResponse, 64); // Truncate From Position of Words
                                    string output = EtisalatResponse.Replace(strResposne, ""); // Replace Words From Data
                                    output = output.Remove(output.Length - 8); // Deduct Words from Last                   
                                    string StrCheckSum = EtisalatResponse.Substring(EtisalatResponse.Length - 8); // Getting Checksum

                                    string[] strDescriptionResponse = output.Split(new char[] { ',' });
                                    // strMessage = "2,OOPs! You do not have sufficient balance.,Failed,NA,NA";

                                    string TRDStatus = strDescriptionResponse[0];
                                    string TRDDescription = strDescriptionResponse[1]; ;
                                    string BEFOROPER = strDescriptionResponse[2];
                                    string FEE = strDescriptionResponse[3];
                                    string AFTEROPER = strDescriptionResponse[4];
                                    string CSVSTOP = strDescriptionResponse[5];

                                    string TXNID = objODBC.executeScalar_str("SELECT transaction_ID_recharge FROM etisalat_topup_request WHERE id='" + strOperaterTableID + "' order by id limit 1").ToString();
                                    try { 
                                    objODBC.executeNonQuery("call `updateResponseV3`('" + TXNID + "','" + intInvestmentID + "',1)");
                                    }
                                    catch (Exception)
                                    {

                                    }
                                    try
                                    {
                                        string strBalance = objODBC.executeScalar_str("SELECT amt FROM `er_sys_wallet` where id=2;");
                                        objODBC.executeNonQuery("update er_recharge set comment='" + EtisalatResponse + "',operator_balance='" + strBalance + "' where id='" + intInvestmentID + "'");

                                        objODBC.executeNonQuery("update etisalat_topup_request set status=1 WHERE recharge_id =" + intInvestmentID + "");
                                    }
                                    catch (Exception)
                                    {

                                    }

                                    try
                                    {
                                        objODBC.executeNonQuery("update etisalat_topup_request set transaction_DESC='" + TRDDescription + "',Transactio_Status='" + TRDStatus + "',BEFOROPER='" + BEFOROPER + "',FEE='" + FEE + "',AFTEROPER='" + AFTEROPER + "','" + CSVSTOP + "',Trans_Receive_Checksum='" + StrCheckSum + "',status=1 WHERE recharge_id =" + intInvestmentID + "");
                                    }
                                    catch (Exception)
                                    {

                                    }


                                    try
                                    {
                                        Regex objMobilePatternMTN = new Regex(@"^(77|76)[\d]{7}$");
                                        Regex objMobilePatternAWCC = new Regex(@"^(70|71)[\d]{7}$");


                                        if (objMobilePatternMTN.IsMatch(strMobile))
                                        {
                                            strOperaterName = "MTN";
                                        }
                                        if (objMobilePatternAWCC.IsMatch(strMobile))
                                        {
                                            strOperaterName = "AWCC";
                                        }
                                        try
                                        {
                                            Recharge objRecharge = new Recharge();
                                            objRecharge.SendSMPPSMS(intUserID, objODBC.executeScalar_str("select username FROM `er_login` where userid='" + intUserID + "'"), strMobile, strOperaterName, int.Parse(strOperaterID), dblActualAmt, 1, strTransactionNumber.ToString(), strPhone);
                                        }
                                        catch (Exception)
                                        {

                                        }
                                        try
                                        {
                                            SMPP_SMS_Format objSMPP_SMS_Format = new SMPP_SMS_Format();
                                            CommonFunction objCOMFUN = new CommonFunction();
                                            string strGetSMS = objSMPP_SMS_Format.TopupSuccessEndUser(objODBC.executeScalar_str("SELECT full_name From er_login Where userid='" + intUserID + "'").ToString(), strMobile, dblActualAmt, intInvestmentID.ToString());
                                            objCOMFUN.er_SMS_Alert(intUserID, 2, strMobile, 2, strGetSMS, 9, "");
                                        }
                                        catch (Exception)
                                        {

                                        }
                                    }
                                    catch (Exception)
                                    {

                                    }
                                }
                              
                              
                            }
                        }
                      
                    }
                }
            }
            catch (Exception e1)
            {
                Response.ContentType = "text/plain";
                Response.Write("Exicute Successfully !" + e1);
                Response.End();
            }
            finally
            {
                ds.Dispose();
            }
        }
    }
}