﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Etopup_Ws.old_App_Code;
using System.Data;
using System.Xml;
using System.Text.RegularExpressions;
using Newtonsoft.Json.Linq;
using System.Xml.Linq;

namespace Etopup_Ws
{
    public partial class Recharge_failedTopup : System.Web.UI.Page
    {
        DateFormat objDF = new DateFormat();
        old_App_Code.DateFormat objDateFor = new old_App_Code.DateFormat();
        ODBC objODBC = new ODBC();
        DataSet ds = new DataSet();
        DataSet ds1 = new DataSet();
        protected void Page_Load(object sender, EventArgs e)
        {

            try
            {

                try
                {
                   
                    //  objODBC.executeNonQuery("INSERT INTO `temp`(`data`, `type`, `created_on`, `request`) VALUES ('Windows Services ',1,'" + objDF.getCurDateTimeString() + "','Recharge_pending_request')");
                    GetPendingRechargeDetails();
                }
                catch (Exception)
                {

                }
               

            }
            catch (Exception ex)
            {
                Response.ContentType = "text/plain";
                Response.Write("Exicute Successfully ! \n" + ex.ToString());
                Response.End();
            }
            Response.ContentType = "text/plain";
            Response.Write("Exicute Successfully !");
            Response.End();
        }
        public string TruncateAtWord(string input, int length)
        {
            if (input == null || input.Length < length)
                return input;
            int iNextSpace = input.LastIndexOf("", length, StringComparison.Ordinal);
            return string.Format("{0}", input.Substring(0, (iNextSpace > 0) ? iNextSpace : length).Trim());
        }

        public void GetPendingRechargeDetails()
        {
            try
            {
                // Select id,userid,trans_number,operator_id,operator_name,deduct_amt,api_type,Operater_table_id,amount,mobile_number from er_recharge where status=1 and (api_type=1 or api_type=2 or api_type=3) And TIMESTAMPDIFF(MINUTE,created_on,'2020-03-15 14:05:04')>=10 order by id ASC limit 10;
                string strDate2 = objDateFor.getCurDateTimeString();
                ds = objODBC.getDataSet("Select id,userid,trans_number,operator_id,operator_name,deduct_amt,api_type,Operater_table_id,amount,mobile_number,request_mobile_no from er_recharge where status=1 and (api_type=1 or api_type=2 or api_type=3 or api_type=6) And created_on < DATE_SUB('" + strDate2 + "',INTERVAL 2 MINUTE) order by id ASC limit 50;");
                int intCount = Convert.ToInt32((ds.Tables[0].Rows.Count));
                if (intCount > 0)
                {
                    for (int i = 0; i < intCount; i++)
                    {
                        int intInvestmentID = int.Parse(ds.Tables[0].Rows[i]["id"].ToString());
                        int intUserID = int.Parse(ds.Tables[0].Rows[i]["userid"].ToString());
                        string strTransactionNumber = ds.Tables[0].Rows[i]["id"].ToString();
                        string strOperaterID = ds.Tables[0].Rows[i]["operator_id"].ToString();
                        string strOperaterName = ds.Tables[0].Rows[i]["operator_name"].ToString();
                        double dblDeductAmt = double.Parse(ds.Tables[0].Rows[i]["deduct_amt"].ToString());
                        string api_type = ds.Tables[0].Rows[i]["api_type"].ToString();
                        string strOperaterTableID = ds.Tables[0].Rows[i]["Operater_table_id"].ToString();
                        double dblActualAmt = double.Parse(ds.Tables[0].Rows[i]["amount"].ToString());
                        string strMobile = ds.Tables[0].Rows[i]["mobile_number"].ToString();
                        string strPhone = ds.Tables[0].Rows[i]["request_mobile_no"].ToString();

                        if (strOperaterTableID == "0")
                        {
                            try
                            {
                                objODBC.executeNonQuery("call `updateResponseV3`('" + intInvestmentID + "','" + intInvestmentID + "',2)");
                            }
                            catch (Exception)
                            {

                            }
                            try
                            {
                                string strBalance = objODBC.executeScalar_str("SELECT amt FROM `er_sys_wallet` where id='" + strOperaterID + "';");
                                objODBC.executeNonQuery("update er_recharge set operator_balance='" + strBalance + "',comment='No Response' where id='" + intInvestmentID + "'");
                                objODBC.executeNonQuery("update er_roshan_request set status=2 WHERE recharge_id =" + intInvestmentID + "");
                            }
                            catch (Exception)
                            {

                            }
                            try
                            {
                                try
                                {
                                    Recharge objRecharge = new Recharge();
                                    objRecharge.SendSMPPSMS(intUserID, objODBC.executeScalar_str("select username FROM `er_login` where userid='" + intUserID + "'"), strMobile, strOperaterName, int.Parse(strOperaterID), dblActualAmt, 2, strTransactionNumber.ToString(), strPhone);
                                }
                                catch (Exception)
                                { }
                            }
                            catch (Exception)
                            {
                            }
                        }
                        else
                        {
                            if (api_type == "1") // Salaam
                            {
                                string strQ = "SELECT encrypted_request FROM er_salaam WHERE recharge_id =" + intInvestmentID + " order by id desc limit 1";
                                string SalaamRequest = objODBC.executeScalar_str(strQ);

                                if (SalaamRequest.ToString() == "" || SalaamRequest.ToString() == null)
                                {
                                    try
                                    {
                                        objODBC.executeNonQuery("call `updateResponseV3`('" + intInvestmentID + "','" + intInvestmentID + "',2)");
                                    }
                                    catch (Exception)
                                    {

                                    }
                                    try
                                    {
                                        string strBalance = objODBC.executeScalar_str("SELECT amt FROM `er_sys_wallet` where id=1;");
                                        objODBC.executeNonQuery("update er_recharge set operator_balance='" + strBalance + "',comment='No Response' where id='" + intInvestmentID + "'");
                                        objODBC.executeNonQuery("update er_salaam set status=2 WHERE recharge_id =" + intInvestmentID + "");
                                    }
                                    catch (Exception)
                                    {

                                    }

                                    try
                                    {
                                        try
                                        {
                                            Recharge objRecharge = new Recharge();
                                            objRecharge.SendSMPPSMS(intUserID, objODBC.executeScalar_str("select username FROM `er_login` where userid='" + intUserID + "'"), strMobile, strOperaterName, int.Parse(strOperaterID), dblActualAmt, 2, strTransactionNumber.ToString(), strPhone);
                                        }
                                        catch (Exception)
                                        { }
                                        //try
                                        //{
                                        //    SMPP_SMS_Format objSMPP_SMS_Format = new SMPP_SMS_Format();
                                        //    CommonFunction objCOMFUN = new CommonFunction();
                                        //    string strGetSMS = objSMPP_SMS_Format.TopupFailedEndUser(objODBC.executeScalar_str("SELECT full_name From er_login Where userid='" + intUserID + "'").ToString(), strMobile, dblActualAmt, intInvestmentID.ToString());
                                        //    objCOMFUN.er_SMS_Alert(intUserID, 2, strMobile, 2, strGetSMS, 9, "");
                                        //}
                                        //catch (Exception)
                                        //{

                                        //}
                                    }
                                    catch (Exception)
                                    {

                                    }
                                }
                            }
                            else if (api_type == "2") // Etisalat
                            {
                                string strQ1 = "SELECT all_response FROM etisalat_topup_request WHERE recharge_id =" + intInvestmentID + " order by id desc limit 1";
                                string EtisalatRequestAllResponse = objODBC.executeScalar_str(strQ1);
                                //  @@  @@ `SC`005c1.00CCB00000PPSPHS0000000002DLGLGNFFFF00000001TXEND FFFFACK:LOGIN:RETN=0000,DESC="Success"  DFEFEDD6
                                if (Regex.IsMatch(EtisalatRequestAllResponse, string.Format(@"\b{0}\b", Regex.Escape("ACNT:RETN=0000,DESC=SUCCESS,BEFOROPER"))))
                                {
                                    string strResposne = TruncateAtWord(EtisalatRequestAllResponse, 64); // Truncate From Position of Words
                                    string output = EtisalatRequestAllResponse.Replace(strResposne, ""); // Replace Words From Data
                                    output = output.Remove(output.Length - 8); // Deduct Words from Last                   
                                    string StrCheckSum = EtisalatRequestAllResponse.Substring(EtisalatRequestAllResponse.Length - 8); // Getting Checksum

                                    string[] strDescriptionResponse = output.Split(new char[] { ',' });
                                    // strMessage = "2,OOPs! You do not have sufficient balance.,Failed,NA,NA";

                                    string TRDStatus = strDescriptionResponse[0];
                                    string TRDDescription = strDescriptionResponse[1]; ;
                                    string BEFOROPER = strDescriptionResponse[2];
                                    string FEE = strDescriptionResponse[3];
                                    string AFTEROPER = strDescriptionResponse[4];
                                    string CSVSTOP = strDescriptionResponse[5];

                                    string TXNID = objODBC.executeScalar_str("SELECT transaction_ID_recharge FROM etisalat_topup_request WHERE id='" + strOperaterTableID + "' order by id limit 1").ToString();
                                    try
                                    {
                                        objODBC.executeNonQuery("call `updateResponseV3`('" + TXNID + "','" + intInvestmentID + "',1)");
                                    }
                                    catch (Exception)
                                    {

                                    }
                                    try
                                    {
                                        string strBalance = objODBC.executeScalar_str("SELECT amt FROM `er_sys_wallet` where id=2;");
                                        objODBC.executeNonQuery("update er_recharge set comment='" + EtisalatRequestAllResponse + "',operator_balance='" + strBalance + "' where id='" + intInvestmentID + "'");

                                        objODBC.executeNonQuery("update etisalat_topup_request set status=1 WHERE recharge_id =" + intInvestmentID + "");
                                    }
                                    catch (Exception)
                                    {

                                    }

                                    try
                                    {
                                        objODBC.executeNonQuery("update etisalat_topup_request set transaction_DESC='" + TRDDescription + "',Transactio_Status='" + TRDStatus + "',BEFOROPER='" + BEFOROPER + "',FEE='" + FEE + "',AFTEROPER='" + AFTEROPER + "','" + CSVSTOP + "',Trans_Receive_Checksum='" + StrCheckSum + "',status=1 WHERE recharge_id =" + intInvestmentID + "");
                                    }
                                    catch (Exception)
                                    {

                                    }


                                    try
                                    {
                                        Regex objMobilePatternMTN = new Regex(@"^(77|76)[\d]{7}$");
                                        Regex objMobilePatternAWCC = new Regex(@"^(70|71)[\d]{7}$");


                                        if (objMobilePatternMTN.IsMatch(strMobile))
                                        {
                                            strOperaterName = "MTN";
                                        }
                                        if (objMobilePatternAWCC.IsMatch(strMobile))
                                        {
                                            strOperaterName = "AWCC";
                                        }
                                        try
                                        {
                                            Recharge objRecharge = new Recharge();
                                            objRecharge.SendSMPPSMS(intUserID, objODBC.executeScalar_str("select username FROM `er_login` where userid='" + intUserID + "'"), strMobile, strOperaterName, int.Parse(strOperaterID), dblActualAmt, 1, strTransactionNumber.ToString(), strPhone);
                                        }
                                        catch (Exception)
                                        {

                                        }
                                        try
                                        {
                                            SMPP_SMS_Format objSMPP_SMS_Format = new SMPP_SMS_Format();
                                            CommonFunction objCOMFUN = new CommonFunction();
                                            string strGetSMS = objSMPP_SMS_Format.TopupSuccessEndUser(objODBC.executeScalar_str("SELECT full_name From er_login Where userid='" + intUserID + "'").ToString(), strMobile, dblActualAmt, intInvestmentID.ToString());
                                            objCOMFUN.er_SMS_Alert(intUserID, 2, strMobile, 2, strGetSMS, 9, "");
                                        }
                                        catch (Exception)
                                        {

                                        }
                                    }
                                    catch (Exception)
                                    {

                                    }
                                }
                                else if (EtisalatRequestAllResponse.Contains("Fail to parse the calculated fee result") == true)
                                {
                                    try
                                    {
                                        objODBC.executeNonQuery("call `updateResponseV3`('" + intInvestmentID + "','" + intInvestmentID + "',2)");
                                    }
                                    catch (Exception)
                                    {

                                    }
                                    try
                                    {
                                        string strBalance = objODBC.executeScalar_str("SELECT amt FROM `er_sys_wallet` where id=2;");
                                        objODBC.executeNonQuery("update er_recharge set operator_balance='" + strBalance + "',comment='" + EtisalatRequestAllResponse.ToString() + "' where id='" + intInvestmentID + "'");
                                        objODBC.executeNonQuery("update etisalat_topup_request set status=2 WHERE recharge_id=" + intInvestmentID + "");
                                    }
                                    catch (Exception)
                                    {

                                    }

                                    try
                                    {
                                        try
                                        {
                                            Recharge objRecharge = new Recharge();
                                            objRecharge.SendSMPPSMS(intUserID, objODBC.executeScalar_str("select username FROM `er_login` where userid='" + intUserID + "'"), strMobile, strOperaterName, int.Parse(strOperaterID), dblActualAmt, 2, strTransactionNumber.ToString(), strPhone);
                                        }
                                        catch (Exception)
                                        { }
                                        //try
                                        //{
                                        //    SMPP_SMS_Format objSMPP_SMS_Format = new SMPP_SMS_Format();
                                        //    CommonFunction objCOMFUN = new CommonFunction();
                                        //    string strGetSMS = objSMPP_SMS_Format.TopupFailedEndUser(objODBC.executeScalar_str("SELECT full_name From er_login Where userid='" + intUserID + "'").ToString(), strMobile, dblActualAmt, intInvestmentID.ToString());
                                        //    objCOMFUN.er_SMS_Alert(intUserID, 2, strMobile, 2, strGetSMS, 9, "");
                                        //}
                                        //catch (Exception)
                                        //{

                                        //}
                                    }
                                    catch (Exception)
                                    {

                                    }
                                }
                                else if (EtisalatRequestAllResponse.Contains("ACNT:RETN=1001,DESC=The user does not exist") == true)
                                {
                                    try
                                    {
                                        objODBC.executeNonQuery("call `updateResponseV3`('" + intInvestmentID + "','" + intInvestmentID + "',2)");
                                    }
                                    catch (Exception)
                                    {

                                    }
                                    try
                                    {
                                        string strBalance = objODBC.executeScalar_str("SELECT amt FROM `er_sys_wallet` where id=2;");
                                        objODBC.executeNonQuery("update er_recharge set operator_balance='" + strBalance + "',comment='" + EtisalatRequestAllResponse.ToString() + "' where id='" + intInvestmentID + "'");
                                        objODBC.executeNonQuery("update etisalat_topup_request set status=2 WHERE recharge_id=" + intInvestmentID + "");
                                    }
                                    catch (Exception)
                                    {

                                    }

                                    try
                                    {
                                        try
                                        {
                                            Recharge objRecharge = new Recharge();
                                            objRecharge.SendSMPPSMS(intUserID, objODBC.executeScalar_str("select username FROM `er_login` where userid='" + intUserID + "'"), strMobile, strOperaterName, int.Parse(strOperaterID), dblActualAmt, 2, strTransactionNumber.ToString(), strPhone);
                                        }
                                        catch (Exception)
                                        { }
                                        //try
                                        //{
                                        //    SMPP_SMS_Format objSMPP_SMS_Format = new SMPP_SMS_Format();
                                        //    CommonFunction objCOMFUN = new CommonFunction();
                                        //    string strGetSMS = objSMPP_SMS_Format.TopupFailedEndUser(objODBC.executeScalar_str("SELECT full_name From er_login Where userid='" + intUserID + "'").ToString(), strMobile, dblActualAmt, intInvestmentID.ToString());
                                        //    objCOMFUN.er_SMS_Alert(intUserID, 2, strMobile, 2, strGetSMS, 9, "");
                                        //}
                                        //catch (Exception)
                                        //{

                                        //}
                                    }
                                    catch (Exception)
                                    {

                                    }
                                }
                                else if (EtisalatRequestAllResponse.Contains("ACNT:RETN=1006,DESC=The subscriber has been reported loss") == true)
                                {
                                    try
                                    {
                                        objODBC.executeNonQuery("call `updateResponseV3`('" + intInvestmentID + "','" + intInvestmentID + "',2)");
                                    }
                                    catch (Exception)
                                    {

                                    }
                                    try
                                    {
                                        string strBalance = objODBC.executeScalar_str("SELECT amt FROM `er_sys_wallet` where id=2;");
                                        objODBC.executeNonQuery("update er_recharge set operator_balance='" + strBalance + "',comment='" + EtisalatRequestAllResponse.ToString() + "' where id='" + intInvestmentID + "'");
                                        objODBC.executeNonQuery("update etisalat_topup_request set status=2 WHERE recharge_id=" + intInvestmentID + "");
                                    }
                                    catch (Exception)
                                    {

                                    }

                                    try
                                    {
                                        try
                                        {
                                            Recharge objRecharge = new Recharge();
                                            objRecharge.SendSMPPSMS(intUserID, objODBC.executeScalar_str("select username FROM `er_login` where userid='" + intUserID + "'"), strMobile, strOperaterName, int.Parse(strOperaterID), dblActualAmt, 2, strTransactionNumber.ToString(), strPhone);
                                        }
                                        catch (Exception)
                                        { }
                                        //try
                                        //{
                                        //    SMPP_SMS_Format objSMPP_SMS_Format = new SMPP_SMS_Format();
                                        //    CommonFunction objCOMFUN = new CommonFunction();
                                        //    string strGetSMS = objSMPP_SMS_Format.TopupFailedEndUser(objODBC.executeScalar_str("SELECT full_name From er_login Where userid='" + intUserID + "'").ToString(), strMobile, dblActualAmt, intInvestmentID.ToString());
                                        //    objCOMFUN.er_SMS_Alert(intUserID, 2, strMobile, 2, strGetSMS, 9, "");
                                        //}
                                        //catch (Exception)
                                        //{

                                        //}
                                    }
                                    catch (Exception)
                                    {

                                    }
                                }
                                else if (EtisalatRequestAllResponse.Contains("ACNT:RETN=1099,DESC=S-ACT-00037,The payment is already exist") == true)
                                {
                                    try
                                    {
                                        objODBC.executeNonQuery("call `updateResponseV3`('" + intInvestmentID + "','" + intInvestmentID + "',2)");
                                    }
                                    catch (Exception)
                                    {

                                    }
                                    try
                                    {
                                        string strBalance = objODBC.executeScalar_str("SELECT amt FROM `er_sys_wallet` where id=2;");
                                        objODBC.executeNonQuery("update er_recharge set operator_balance='" + strBalance + "',comment='" + EtisalatRequestAllResponse.ToString() + "' where id='" + intInvestmentID + "'");
                                        objODBC.executeNonQuery("update etisalat_topup_request set status=2 WHERE recharge_id=" + intInvestmentID + "");
                                    }
                                    catch (Exception)
                                    {

                                    }

                                    try
                                    {
                                        try
                                        {
                                            Recharge objRecharge = new Recharge();
                                            objRecharge.SendSMPPSMS(intUserID, objODBC.executeScalar_str("select username FROM `er_login` where userid='" + intUserID + "'"), strMobile, strOperaterName, int.Parse(strOperaterID), dblActualAmt, 2, strTransactionNumber.ToString(), strPhone);
                                        }
                                        catch (Exception)
                                        { }
                                        //try
                                        //{
                                        //    SMPP_SMS_Format objSMPP_SMS_Format = new SMPP_SMS_Format();
                                        //    CommonFunction objCOMFUN = new CommonFunction();
                                        //    string strGetSMS = objSMPP_SMS_Format.TopupFailedEndUser(objODBC.executeScalar_str("SELECT full_name From er_login Where userid='" + intUserID + "'").ToString(), strMobile, dblActualAmt, intInvestmentID.ToString());
                                        //    objCOMFUN.er_SMS_Alert(intUserID, 2, strMobile, 2, strGetSMS, 9, "");
                                        //}
                                        //catch (Exception)
                                        //{

                                        //}
                                    }
                                    catch (Exception)
                                    {

                                    }
                                }
                                else if (EtisalatRequestAllResponse.Contains("ACNT:RETN=1099,DESC=S-DAT-00010,error accurs getting connection!") == true)
                                {
                                    try
                                    {
                                        objODBC.executeNonQuery("call `updateResponseV3`('" + intInvestmentID + "','" + intInvestmentID + "',2)");
                                    }
                                    catch (Exception)
                                    {

                                    }
                                    try
                                    {
                                        string strBalance = objODBC.executeScalar_str("SELECT amt FROM `er_sys_wallet` where id=2;");
                                        objODBC.executeNonQuery("update er_recharge set operator_balance='" + strBalance + "',comment='" + EtisalatRequestAllResponse.ToString() + "' where id='" + intInvestmentID + "'");
                                        objODBC.executeNonQuery("update etisalat_topup_request set status=2 WHERE recharge_id=" + intInvestmentID + "");
                                    }
                                    catch (Exception)
                                    {

                                    }

                                    try
                                    {
                                        try
                                        {
                                            Recharge objRecharge = new Recharge();
                                            objRecharge.SendSMPPSMS(intUserID, objODBC.executeScalar_str("select username FROM `er_login` where userid='" + intUserID + "'"), strMobile, strOperaterName, int.Parse(strOperaterID), dblActualAmt, 2, strTransactionNumber.ToString(), strPhone);
                                        }
                                        catch (Exception)
                                        { }
                                        //try
                                        //{
                                        //    SMPP_SMS_Format objSMPP_SMS_Format = new SMPP_SMS_Format();
                                        //    CommonFunction objCOMFUN = new CommonFunction();
                                        //    string strGetSMS = objSMPP_SMS_Format.TopupFailedEndUser(objODBC.executeScalar_str("SELECT full_name From er_login Where userid='" + intUserID + "'").ToString(), strMobile, dblActualAmt, intInvestmentID.ToString());
                                        //    objCOMFUN.er_SMS_Alert(intUserID, 2, strMobile, 2, strGetSMS, 9, "");
                                        //}
                                        //catch (Exception)
                                        //{

                                        //}
                                    }
                                    catch (Exception)
                                    {

                                    }
                                }
                                else if (EtisalatRequestAllResponse.Contains("ACNT:RETN=1099,DESC=S-ACT-00144,Fail to deal with the accumulation") == true)
                                {
                                    // @@  @@ `SC`00941.00CCB00000PPSPHS0000000002DLGCONFFFF00000004TXEND FFFFACK:CHARGE CASH ACNT:RETN=1099,DESC=S-ACT-00144,Fail to deal with the accumulation,BKID=0   D6FFC397
                                    try
                                    {
                                        objODBC.executeNonQuery("call `updateResponseV3`('" + intInvestmentID + "','" + intInvestmentID + "',2)");
                                    }
                                    catch (Exception)
                                    {

                                    }
                                    try
                                    {
                                        string strBalance = objODBC.executeScalar_str("SELECT amt FROM `er_sys_wallet` where id=2;");
                                        objODBC.executeNonQuery("update er_recharge set operator_balance='" + strBalance + "',comment='" + EtisalatRequestAllResponse.ToString() + "' where id='" + intInvestmentID + "'");
                                        objODBC.executeNonQuery("update etisalat_topup_request set status=2 WHERE recharge_id=" + intInvestmentID + "");
                                    }
                                    catch (Exception)
                                    {

                                    }

                                    try
                                    {
                                        try
                                        {
                                            Recharge objRecharge = new Recharge();
                                            objRecharge.SendSMPPSMS(intUserID, objODBC.executeScalar_str("select username FROM `er_login` where userid='" + intUserID + "'"), strMobile, strOperaterName, int.Parse(strOperaterID), dblActualAmt, 2, strTransactionNumber.ToString(), strPhone);
                                        }
                                        catch (Exception)
                                        { }
                                        //try
                                        //{
                                        //    SMPP_SMS_Format objSMPP_SMS_Format = new SMPP_SMS_Format();
                                        //    CommonFunction objCOMFUN = new CommonFunction();
                                        //    string strGetSMS = objSMPP_SMS_Format.TopupFailedEndUser(objODBC.executeScalar_str("SELECT full_name From er_login Where userid='" + intUserID + "'").ToString(), strMobile, dblActualAmt, intInvestmentID.ToString());
                                        //    objCOMFUN.er_SMS_Alert(intUserID, 2, strMobile, 2, strGetSMS, 9, "");
                                        //}
                                        //catch (Exception)
                                        //{

                                        //}
                                    }
                                    catch (Exception)
                                    {

                                    }
                                }
                                else if (EtisalatRequestAllResponse.Contains("ACNT:RETN=1099,DESC=S-DAT-00021,update error") == true)
                                {

                                    // @@  @@ `SC`007c1.00CCB00000PPSPHS0000000002DLGCONFFFF00000004TXEND FFFFACK:CHARGE CASH ACNT:RETN=1099,DESC=S-DAT-00021,update error,BKID=0 F8F5D996

                                    try
                                    {
                                        objODBC.executeNonQuery("call `updateResponseV3`('" + intInvestmentID + "','" + intInvestmentID + "',2)");
                                    }
                                    catch (Exception)
                                    {

                                    }
                                    try
                                    {
                                        string strBalance = objODBC.executeScalar_str("SELECT amt FROM `er_sys_wallet` where id=2;");
                                        objODBC.executeNonQuery("update er_recharge set operator_balance='" + strBalance + "',comment='" + EtisalatRequestAllResponse.ToString() + "' where id='" + intInvestmentID + "'");
                                        objODBC.executeNonQuery("update etisalat_topup_request set status=2 WHERE recharge_id=" + intInvestmentID + "");
                                    }
                                    catch (Exception)
                                    {

                                    }

                                    try
                                    {
                                        try
                                        {
                                            Recharge objRecharge = new Recharge();
                                            objRecharge.SendSMPPSMS(intUserID, objODBC.executeScalar_str("select username FROM `er_login` where userid='" + intUserID + "'"), strMobile, strOperaterName, int.Parse(strOperaterID), dblActualAmt, 2, strTransactionNumber.ToString(), strPhone);
                                        }
                                        catch (Exception)
                                        { }
                                        //try
                                        //{
                                        //    SMPP_SMS_Format objSMPP_SMS_Format = new SMPP_SMS_Format();
                                        //    CommonFunction objCOMFUN = new CommonFunction();
                                        //    string strGetSMS = objSMPP_SMS_Format.TopupFailedEndUser(objODBC.executeScalar_str("SELECT full_name From er_login Where userid='" + intUserID + "'").ToString(), strMobile, dblActualAmt, intInvestmentID.ToString());
                                        //    objCOMFUN.er_SMS_Alert(intUserID, 2, strMobile, 2, strGetSMS, 9, "");
                                        //}
                                        //catch (Exception)
                                        //{

                                        //}
                                    }
                                    catch (Exception)
                                    {

                                    }
                                }
                                else if (EtisalatRequestAllResponse.Contains("ACNT:RETN=1099,DESC=S-ACT-00113,Fail to deal with the presen") == true)
                                {

                                    //  @@  @@ `SC`008c1.00CCB00000PPSPHS0000000002DLGCONFFFF00000004TXEND FFFFACK:CHARGE CASH ACNT:RETN=1099,DESC=S-ACT-00113,Fail to deal with the present,BKID=095F29ED0

                                    try
                                    {
                                        objODBC.executeNonQuery("call `updateResponseV3`('" + intInvestmentID + "','" + intInvestmentID + "',2)");
                                    }
                                    catch (Exception)
                                    {

                                    }
                                    try
                                    {
                                        string strBalance = objODBC.executeScalar_str("SELECT amt FROM `er_sys_wallet` where id=2;");
                                        objODBC.executeNonQuery("update er_recharge set operator_balance='" + strBalance + "',comment='" + EtisalatRequestAllResponse.ToString() + "' where id='" + intInvestmentID + "'");
                                        objODBC.executeNonQuery("update etisalat_topup_request set status=2 WHERE recharge_id=" + intInvestmentID + "");
                                    }
                                    catch (Exception)
                                    {

                                    }

                                    try
                                    {
                                        try
                                        {
                                            Recharge objRecharge = new Recharge();
                                            objRecharge.SendSMPPSMS(intUserID, objODBC.executeScalar_str("select username FROM `er_login` where userid='" + intUserID + "'"), strMobile, strOperaterName, int.Parse(strOperaterID), dblActualAmt, 2, strTransactionNumber.ToString(), strPhone);
                                        }
                                        catch (Exception)
                                        { }
                                        //try
                                        //{
                                        //    SMPP_SMS_Format objSMPP_SMS_Format = new SMPP_SMS_Format();
                                        //    CommonFunction objCOMFUN = new CommonFunction();
                                        //    string strGetSMS = objSMPP_SMS_Format.TopupFailedEndUser(objODBC.executeScalar_str("SELECT full_name From er_login Where userid='" + intUserID + "'").ToString(), strMobile, dblActualAmt, intInvestmentID.ToString());
                                        //    objCOMFUN.er_SMS_Alert(intUserID, 2, strMobile, 2, strGetSMS, 9, "");
                                        //}
                                        //catch (Exception)
                                        //{

                                        //}
                                    }
                                    catch (Exception)
                                    {

                                    }
                                }
                                else if (EtisalatRequestAllResponse.Contains("ACNT:RETN=1099,DESC=S-ACT-00454,The current account has been locked by another operation,please try later") == true)
                                {
                                    // @@  @@ `SC`00941.00CCB00000PPSPHS0000000002DLGCONFFFF00000004TXEND FFFFACK:CHARGE CASH ACNT:RETN=1099,DESC=S-ACT-00144,Fail to deal with the accumulation,BKID=0   D6FFC397
                                    try
                                    {
                                        objODBC.executeNonQuery("call `updateResponseV3`('" + intInvestmentID + "','" + intInvestmentID + "',2)");
                                    }
                                    catch (Exception)
                                    {

                                    }
                                    try
                                    {
                                        string strBalance = objODBC.executeScalar_str("SELECT amt FROM `er_sys_wallet` where id=2;");
                                        objODBC.executeNonQuery("update er_recharge set operator_balance='" + strBalance + "',comment='" + EtisalatRequestAllResponse.ToString() + "' where id='" + intInvestmentID + "'");
                                        objODBC.executeNonQuery("update etisalat_topup_request set status=2 WHERE recharge_id=" + intInvestmentID + "");
                                    }
                                    catch (Exception)
                                    {

                                    }

                                    try
                                    {
                                        try
                                        {
                                            Recharge objRecharge = new Recharge();
                                            objRecharge.SendSMPPSMS(intUserID, objODBC.executeScalar_str("select username FROM `er_login` where userid='" + intUserID + "'"), strMobile, strOperaterName, int.Parse(strOperaterID), dblActualAmt, 2, strTransactionNumber.ToString(), strPhone);
                                        }
                                        catch (Exception)
                                        { }
                                        //try
                                        //{
                                        //    SMPP_SMS_Format objSMPP_SMS_Format = new SMPP_SMS_Format();
                                        //    CommonFunction objCOMFUN = new CommonFunction();
                                        //    string strGetSMS = objSMPP_SMS_Format.TopupFailedEndUser(objODBC.executeScalar_str("SELECT full_name From er_login Where userid='" + intUserID + "'").ToString(), strMobile, dblActualAmt, intInvestmentID.ToString());
                                        //    objCOMFUN.er_SMS_Alert(intUserID, 2, strMobile, 2, strGetSMS, 9, "");
                                        //}
                                        //catch (Exception)
                                        //{

                                        //}
                                    }
                                    catch (Exception)
                                    {

                                    }
                                }
                                else if (EtisalatRequestAllResponse.Contains("ACNT:RETN=1099,DESC=S-DAT-00020,query error") == true)
                                {
                                    // @@  @@ `SC`00941.00CCB00000PPSPHS0000000002DLGCONFFFF00000004TXEND FFFFACK:CHARGE CASH ACNT:RETN=1099,DESC=S-ACT-00144,Fail to deal with the accumulation,BKID=0   D6FFC397
                                    try
                                    {
                                        objODBC.executeNonQuery("call `updateResponseV3`('" + intInvestmentID + "','" + intInvestmentID + "',2)");
                                    }
                                    catch (Exception)
                                    {

                                    }
                                    try
                                    {
                                        string strBalance = objODBC.executeScalar_str("SELECT amt FROM `er_sys_wallet` where id=2;");
                                        objODBC.executeNonQuery("update er_recharge set operator_balance='" + strBalance + "',comment='" + EtisalatRequestAllResponse.ToString() + "' where id='" + intInvestmentID + "'");
                                        objODBC.executeNonQuery("update etisalat_topup_request set status=2 WHERE recharge_id=" + intInvestmentID + "");
                                    }
                                    catch (Exception)
                                    {

                                    }

                                    try
                                    {
                                        try
                                        {
                                            Recharge objRecharge = new Recharge();
                                            objRecharge.SendSMPPSMS(intUserID, objODBC.executeScalar_str("select username FROM `er_login` where userid='" + intUserID + "'"), strMobile, strOperaterName, int.Parse(strOperaterID), dblActualAmt, 2, strTransactionNumber.ToString(), strPhone);
                                        }
                                        catch (Exception)
                                        { }
                                        //try
                                        //{
                                        //    SMPP_SMS_Format objSMPP_SMS_Format = new SMPP_SMS_Format();
                                        //    CommonFunction objCOMFUN = new CommonFunction();
                                        //    string strGetSMS = objSMPP_SMS_Format.TopupFailedEndUser(objODBC.executeScalar_str("SELECT full_name From er_login Where userid='" + intUserID + "'").ToString(), strMobile, dblActualAmt, intInvestmentID.ToString());
                                        //    objCOMFUN.er_SMS_Alert(intUserID, 2, strMobile, 2, strGetSMS, 9, "");
                                        //}
                                        //catch (Exception)
                                        //{

                                        //}
                                    }
                                    catch (Exception)
                                    {

                                    }
                                }
                                else if (EtisalatRequestAllResponse.Contains(":LOGIN:") == true)
                                {
                                    try
                                    {
                                        objODBC.executeNonQuery("call `updateResponseV3`('" + intInvestmentID + "','" + intInvestmentID + "',2)");
                                    }
                                    catch (Exception)
                                    {

                                    }
                                    try
                                    {
                                        string strBalance = objODBC.executeScalar_str("SELECT amt FROM `er_sys_wallet` where id=2;");
                                        objODBC.executeNonQuery("update er_recharge set operator_balance='" + strBalance + "',comment='No Response' where id='" + intInvestmentID + "'");
                                        objODBC.executeNonQuery("update etisalat_topup_request set status=2 WHERE recharge_id=" + intInvestmentID + "");
                                    }
                                    catch (Exception)
                                    {

                                    }

                                    try
                                    {
                                        try
                                        {
                                            Recharge objRecharge = new Recharge();
                                            objRecharge.SendSMPPSMS(intUserID, objODBC.executeScalar_str("select username FROM `er_login` where userid='" + intUserID + "'"), strMobile, strOperaterName, int.Parse(strOperaterID), dblActualAmt, 2, strTransactionNumber.ToString(), strPhone);
                                        }
                                        catch (Exception)
                                        { }
                                        //try
                                        //{
                                        //    SMPP_SMS_Format objSMPP_SMS_Format = new SMPP_SMS_Format();
                                        //    CommonFunction objCOMFUN = new CommonFunction();
                                        //    string strGetSMS = objSMPP_SMS_Format.TopupFailedEndUser(objODBC.executeScalar_str("SELECT full_name From er_login Where userid='" + intUserID + "'").ToString(), strMobile, dblActualAmt, intInvestmentID.ToString());
                                        //    objCOMFUN.er_SMS_Alert(intUserID, 2, strMobile, 2, strGetSMS, 9, "");
                                        //}
                                        //catch (Exception)
                                        //{

                                        //}
                                    }
                                    catch (Exception)
                                    {

                                    }
                                }
                                else if (EtisalatRequestAllResponse.ToString() == "")
                                {
                                    try
                                    {
                                        objODBC.executeNonQuery("call `updateResponseV3`('" + intInvestmentID + "','" + intInvestmentID + "',2)");
                                    }
                                    catch (Exception)
                                    {

                                    }
                                    try
                                    {
                                        string strBalance = objODBC.executeScalar_str("SELECT amt FROM `er_sys_wallet` where id=2;");
                                        objODBC.executeNonQuery("update er_recharge set operator_balance='" + strBalance + "',comment='No Response' where id='" + intInvestmentID + "'");
                                        objODBC.executeNonQuery("update etisalat_topup_request set status=2 WHERE recharge_id=" + intInvestmentID + "");
                                    }
                                    catch (Exception)
                                    {

                                    }

                                    try
                                    {
                                        try
                                        {
                                            Recharge objRecharge = new Recharge();
                                            objRecharge.SendSMPPSMS(intUserID, objODBC.executeScalar_str("select username FROM `er_login` where userid='" + intUserID + "'"), strMobile, strOperaterName, int.Parse(strOperaterID), dblActualAmt, 2, strTransactionNumber.ToString(), strPhone);
                                        }
                                        catch (Exception)
                                        { }
                                        //try
                                        //{
                                        //    SMPP_SMS_Format objSMPP_SMS_Format = new SMPP_SMS_Format();
                                        //    CommonFunction objCOMFUN = new CommonFunction();
                                        //    string strGetSMS = objSMPP_SMS_Format.TopupFailedEndUser(objODBC.executeScalar_str("SELECT full_name From er_login Where userid='" + intUserID + "'").ToString(), strMobile, dblActualAmt, intInvestmentID.ToString());
                                        //    objCOMFUN.er_SMS_Alert(intUserID, 2, strMobile, 2, strGetSMS, 9, "");
                                        //}
                                        //catch (Exception)
                                        //{

                                        //}
                                    }
                                    catch (Exception)
                                    {

                                    }
                                }
                                else
                                {
                                    string strQ = "SELECT Transaction_Response FROM etisalat_topup_request WHERE recharge_id =" + intInvestmentID + " order by id desc limit 1";
                                    string EtisalatRequest = objODBC.executeScalar_str(strQ);
                                    if (EtisalatRequest.Contains("NA") == true)
                                    {
                                        try
                                        {
                                            objODBC.executeNonQuery("call `updateResponseV3`('" + intInvestmentID + "','" + intInvestmentID + "',2)");
                                        }
                                        catch (Exception)
                                        {

                                        }
                                        try
                                        {
                                            string strBalance = objODBC.executeScalar_str("SELECT amt FROM `er_sys_wallet` where id=2;");
                                            objODBC.executeNonQuery("update er_recharge set operator_balance='" + strBalance + "',comment='No Response' where id='" + intInvestmentID + "'");
                                            objODBC.executeNonQuery("update etisalat_topup_request set status=2 WHERE recharge_id=" + intInvestmentID + "");
                                        }
                                        catch (Exception)
                                        {

                                        }

                                        try
                                        {
                                            try
                                            {
                                                Recharge objRecharge = new Recharge();
                                                objRecharge.SendSMPPSMS(intUserID, objODBC.executeScalar_str("select username FROM `er_login` where userid='" + intUserID + "'"), strMobile, strOperaterName, int.Parse(strOperaterID), dblActualAmt, 2, strTransactionNumber.ToString(), strPhone);
                                            }
                                            catch (Exception)
                                            { }
                                            //try
                                            //{
                                            //    SMPP_SMS_Format objSMPP_SMS_Format = new SMPP_SMS_Format();
                                            //    CommonFunction objCOMFUN = new CommonFunction();
                                            //    string strGetSMS = objSMPP_SMS_Format.TopupFailedEndUser(objODBC.executeScalar_str("SELECT full_name From er_login Where userid='" + intUserID + "'").ToString(), strMobile, dblActualAmt, intInvestmentID.ToString());
                                            //    objCOMFUN.er_SMS_Alert(intUserID, 2, strMobile, 2, strGetSMS, 9, "");
                                            //}
                                            //catch (Exception)
                                            //{

                                            //}
                                        }
                                        catch (Exception)
                                        {

                                        }

                                    }
                                    else
                                    {
                                        strQ = "SELECT Transaction_Request FROM etisalat_topup_request WHERE recharge_id =" + intInvestmentID + " order by id desc limit 1";
                                        EtisalatRequest = objODBC.executeScalar_str(strQ);
                                        if (EtisalatRequest.Contains("NA") == true)
                                        {
                                            try
                                            {
                                                objODBC.executeNonQuery("call `updateResponseV3`('" + intInvestmentID + "','" + intInvestmentID + "',2)");
                                            }
                                            catch (Exception)
                                            {

                                            }
                                            try
                                            {
                                                string strBalance = objODBC.executeScalar_str("SELECT amt FROM `er_sys_wallet` where id=2;");
                                                objODBC.executeNonQuery("update er_recharge set operator_balance='" + strBalance + "',comment='No Response' where id='" + intInvestmentID + "'");
                                                objODBC.executeNonQuery("update etisalat_topup_request set status=2 WHERE recharge_id=" + intInvestmentID + "");
                                            }
                                            catch (Exception)
                                            {

                                            }

                                            try
                                            {
                                                try
                                                {
                                                    Recharge objRecharge = new Recharge();
                                                    objRecharge.SendSMPPSMS(intUserID, objODBC.executeScalar_str("select username FROM `er_login` where userid='" + intUserID + "'"), strMobile, strOperaterName, int.Parse(strOperaterID), dblActualAmt, 2, strTransactionNumber.ToString(), strPhone);
                                                }
                                                catch (Exception)
                                                { }
                                                //try
                                                //{
                                                //    SMPP_SMS_Format objSMPP_SMS_Format = new SMPP_SMS_Format();
                                                //    CommonFunction objCOMFUN = new CommonFunction();
                                                //    string strGetSMS = objSMPP_SMS_Format.TopupFailedEndUser(objODBC.executeScalar_str("SELECT full_name From er_login Where userid='" + intUserID + "'").ToString(), strMobile, dblActualAmt, intInvestmentID.ToString());
                                                //    objCOMFUN.er_SMS_Alert(intUserID, 2, strMobile, 2, strGetSMS, 9, "");
                                                //}
                                                //catch (Exception)
                                                //{

                                                //}
                                            }
                                            catch (Exception)
                                            {

                                            }

                                        }

                                    }
                                }
                            }
                            else if (api_type == "3") // Roshan
                            {
                                string strQ = "SELECT top_up_response FROM er_roshan_request WHERE recharge_id =" + intInvestmentID + " order by id desc limit 1";
                                string RoshanRequest = objODBC.executeScalar_str(strQ);

                                if (RoshanRequest.ToString() == "" || RoshanRequest.ToString() == null || RoshanRequest.Contains("<html><head><title>") == true)
                                {
                                    try
                                    {
                                        objODBC.executeNonQuery("call `updateResponseV3`('" + intInvestmentID + "','" + intInvestmentID + "',2)");
                                    }
                                    catch (Exception)
                                    {

                                    }
                                    try
                                    {
                                        string strBalance = objODBC.executeScalar_str("SELECT amt FROM `er_sys_wallet` where id=3;");
                                        objODBC.executeNonQuery("update er_recharge set operator_balance='" + strBalance + "',comment='No Response' where id='" + intInvestmentID + "'");
                                        objODBC.executeNonQuery("update er_roshan_request set status=2 WHERE recharge_id =" + intInvestmentID + "");
                                    }
                                    catch (Exception)
                                    {

                                    }

                                    try
                                    {
                                        try
                                        {
                                            Recharge objRecharge = new Recharge();
                                            objRecharge.SendSMPPSMS(intUserID, objODBC.executeScalar_str("select username FROM `er_login` where userid='" + intUserID + "'"), strMobile, strOperaterName, int.Parse(strOperaterID), dblActualAmt, 2, strTransactionNumber.ToString(), strPhone);
                                        }
                                        catch (Exception)
                                        { }
                                        //try
                                        //{
                                        //    SMPP_SMS_Format objSMPP_SMS_Format = new SMPP_SMS_Format();
                                        //    CommonFunction objCOMFUN = new CommonFunction();
                                        //    string strGetSMS = objSMPP_SMS_Format.TopupFailedEndUser(objODBC.executeScalar_str("SELECT full_name From er_login Where userid='" + intUserID + "'").ToString(), strMobile, dblActualAmt, intInvestmentID.ToString());
                                        //    objCOMFUN.er_SMS_Alert(intUserID, 2, strMobile, 2, strGetSMS, 9, "");
                                        //}
                                        //catch (Exception)
                                        //{

                                        //}
                                    }
                                    catch (Exception)
                                    {

                                    }
                                }
                            }                           
                            else if (api_type == "6") // MTN
                            {
                                string MTNRequest = objODBC.executeScalar_str("SELECT response FROM er_mtn_topup WHERE recharge_id=" + intInvestmentID + " order by id desc limit 1");
                                if (MTNRequest.ToString() != "NA")
                                {
                                    try
                                    {
                                        XDocument xml = XDocument.Parse(MTNRequest);
                                        var soapResponse = xml.Descendants().Where(x => x.Name.LocalName == "return").Select(x => new MTNSoapResponseParse()
                                        {
                                            ersReference = (string)x.Element(x.Name.Namespace + "ersReference"),
                                            resultCode = (int)x.Element(x.Name.Namespace + "resultCode"),
                                            resultDescription = (string)x.Element(x.Name.Namespace + "resultDescription")
                                        }).FirstOrDefault();


                                        if (soapResponse.resultCode == 0) // Success
                                        {


                                            //<soap:Envelope xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/"><soap:Body><ns2:requestTopupResponse xmlns:ns2="http://external.interfaces.ers.seamless.com/"><return><ersReference>2020040111361978801000085</ersReference><resultCode>0</resultCode><resultDescription>You topped up 10 AFA to 93764524139. Your balance is now 957 AFA. The transaction ID: 2020040111361978801000085</resultDescription><requestedTopupAmount><currency>AFA</currency><value>10</value></requestedTopupAmount><senderPrincipal><principalId><id>SML</id><type>RESELLERID</type></principalId><principalName>SML</principalName><accounts><account><accountSpecifier><accountId>SML</accountId><accountTypeId>RESELLER_AIRTIME</accountTypeId></accountSpecifier><balance><currency>AFA</currency><value>967.00000</value></balance><creditLimit><currency>AFA</currency><value>0.00000</value></creditLimit></account></accounts><status>Active</status><msisdn>93773333985</msisdn></senderPrincipal><topupAccountSpecifier><accountId>764524139</accountId><accountTypeId>SUBSCRIBER_AIRTIME</accountTypeId></topupAccountSpecifier><topupAmount><currency>AFA</currency><value>10.00</value></topupAmount><topupPrincipal><principalId><id>93764524139</id><type>SUBSCRIBERMSISDN</type></principalId><principalName></principalName><accounts><account><accountSpecifier><accountId>764524139</accountId><accountTypeId>SUBSCRIBER_AIRTIME</accountTypeId></accountSpecifier></account></accounts></topupPrincipal></return></ns2:requestTopupResponse></soap:Body></soap:Envelope>


                                            string TXNID = soapResponse.ersReference; //TXNID = Transaction ID
                                            string strBalance = "NA";
                                            try
                                            {
                                                objODBC.executeNonQuery("call `updateResponseV3`('" + TXNID + "','" + intInvestmentID + "',1)");
                                            }
                                            catch (Exception)
                                            {

                                            }
                                            try
                                            {
                                                try
                                                {
                                                    // You topped up 10 AFA to 93764524139. Your balance is now 957 AFA. The transaction ID: 2020040111361978801000085
                                                    string[] strBalanceStructure = soapResponse.resultDescription.Split(new char[] { '.' });
                                                    string strOpMessage = strBalanceStructure[2];
                                                    var replacements = new[] { new { Find = "Your balance is now ", Replace = "0" }, new { Find = " AFA", Replace = "" }, };
                                                    var myLongString = strOpMessage;
                                                    foreach (var set in replacements)
                                                    {
                                                        myLongString = myLongString.Replace(set.Find, set.Replace);
                                                    }
                                                    strBalance = myLongString;
                                                }
                                                catch (Exception)
                                                {

                                                }
                                                try
                                                {
                                                    objODBC.executeNonQuery("update er_recharge set operator_balance='" + strBalance + "' where id='" + intInvestmentID + "'");

                                                }
                                                catch (Exception)
                                                {

                                                }
                                            }
                                            catch (Exception)
                                            {

                                            }


                                            try
                                            {
                                                objODBC.executeNonQuery("update er_mtn_topup set mobile='" + strMobile + "',amount='" + dblActualAmt + "',api_type='" + api_type + "',ersReference='" + TXNID + "',resultCode='" + soapResponse.resultCode + "',resultDescription='" + soapResponse.resultDescription + "',value='" + dblActualAmt + "',bal_api='" + strBalance + "',status=1  where id='" + strOperaterTableID + "'");

                                            }
                                            catch (Exception)
                                            {

                                            }
                                            try
                                            {
                                                try
                                                {
                                                    Recharge objRecharge = new Recharge();
                                                    objRecharge.SendSMPPSMS(intUserID, objODBC.executeScalar_str("select username FROM `er_login` where userid='" + intUserID + "'"), strMobile, strOperaterName, int.Parse(strOperaterID), dblActualAmt, 1, strTransactionNumber.ToString(), strPhone);
                                                }
                                                catch (Exception)
                                                {

                                                }

                                                SMPP_SMS_Format objSMPP_SMS_Format = new SMPP_SMS_Format();
                                                CommonFunction objCOMFUN = new CommonFunction();
                                                string strGetSMS = objSMPP_SMS_Format.TopupSuccessEndUser(objODBC.executeScalar_str("SELECT full_name From er_login Where userid='" + intUserID + "'").ToString(), strMobile, dblActualAmt, intInvestmentID.ToString());
                                                objCOMFUN.er_SMS_Alert(intUserID, 2, strMobile, 4, strGetSMS, 9, "");
                                            }
                                            catch (Exception)
                                            {

                                            }


                                        }
                                        else if (soapResponse.resultCode > 0) //Failed
                                        {


                                            // <soap:Envelope xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/"><soap:Body><ns2:requestTopupResponse xmlns:ns2="http://external.interfaces.ers.seamless.com/"><return><ersReference>2020040111320629301000083</ersReference><resultCode>11</resultCode><resultDescription>You provided an amount outside the margins of the contract. Please try again with a valid amount.</resultDescription><requestedTopupAmount><currency>AFA</currency><value>1</value></requestedTopupAmount><senderPrincipal><principalId><id>SML</id><type>RESELLERID</type></principalId><principalName>SML</principalName><accounts><account><accountSpecifier><accountId>SML</accountId><accountTypeId>RESELLER_AIRTIME</accountTypeId></accountSpecifier></account></accounts><status>Active</status><msisdn>93773333985</msisdn></senderPrincipal><topupAccountSpecifier><accountId>764524139</accountId><accountTypeId>SUBSCRIBER_AIRTIME</accountTypeId></topupAccountSpecifier><topupPrincipal><principalId><id>93764524139</id><type>SUBSCRIBERMSISDN</type></principalId><principalName></principalName><accounts><account><accountSpecifier><accountId>764524139</accountId><accountTypeId>SUBSCRIBER_AIRTIME</accountTypeId></accountSpecifier></account></accounts></topupPrincipal></return></ns2:requestTopupResponse></soap:Body></soap:Envelope>

                                            try
                                            {
                                                string TXNID = soapResponse.ersReference;
                                                try
                                                {
                                                    objODBC.executeNonQuery("call `updateResponseV3`('" + TXNID + "','" + intInvestmentID + "',2)");
                                                }
                                                catch (Exception)
                                                {

                                                }


                                            }
                                            catch (Exception)
                                            {

                                            }



                                            try
                                            {
                                                objODBC.executeNonQuery("update er_mtn_topup set mobile='" + strMobile + "',amount='" + dblActualAmt + "',api_type=6,ersReference='" + soapResponse.ersReference + "',resultCode='" + soapResponse.resultCode + "',resultDescription='" + soapResponse.resultDescription + "',status=2 where id='" + strOperaterTableID + "'");

                                            }
                                            catch (Exception)
                                            {

                                            }

                                            try
                                            {
                                                try
                                                {
                                                    Recharge objRecharge = new Recharge();
                                                    objRecharge.SendSMPPSMS(intUserID, objODBC.executeScalar_str("select username FROM `er_login` where userid='" + intUserID + "'"), strMobile, strOperaterName, int.Parse(strOperaterID), dblActualAmt, 2, strTransactionNumber.ToString(), strPhone);
                                                }
                                                catch (Exception)
                                                { }

                                                //SMPP_SMS_Format objSMPP_SMS_Format = new SMPP_SMS_Format();
                                                //CommonFunction objCOMFUN = new CommonFunction();
                                                //string strGetSMS = objSMPP_SMS_Format.TopupFailedEndUser(objODBC.executeScalar_str("SELECT full_name From er_login Where userid='" + intUserID + "'").ToString(), StrEndUserCoustomerMob, dblAmount, intTopUpRecTransactionID.ToString());
                                                //objCOMFUN.er_SMS_Alert(intUserID, 2, StrEndUserCoustomerMob, 4, strGetSMS, 9, "");
                                            }
                                            catch (Exception)
                                            {

                                            }


                                        }



                                    }
                                    catch (Exception)
                                    {

                                    }
                                }
                                else if (MTNRequest.ToString() == "NA")
                                {
                                    try
                                    {
                                        objODBC.executeNonQuery("call `updateResponseV3`('" + intInvestmentID + "','" + intInvestmentID + "',2)");
                                    }
                                    catch (Exception)
                                    {

                                    }
                                    try
                                    {
                                        string strBalance = objODBC.executeScalar_str("SELECT amt FROM `er_sys_wallet` where id=4;");
                                        objODBC.executeNonQuery("update er_recharge set operator_balance='" + strBalance + "',comment='No Response' where id='" + intInvestmentID + "'");
                                        objODBC.executeNonQuery("update er_mtn_topup set status=2 WHERE recharge_id=" + intInvestmentID + "");
                                    }
                                    catch (Exception)
                                    {

                                    }

                                    try
                                    {
                                        try
                                        {
                                            Recharge objRecharge = new Recharge();
                                            objRecharge.SendSMPPSMS(intUserID, objODBC.executeScalar_str("select username FROM `er_login` where userid='" + intUserID + "'"), strMobile, strOperaterName, int.Parse(strOperaterID), dblActualAmt, 2, strTransactionNumber.ToString(), strPhone);
                                        }
                                        catch (Exception)
                                        { }
                                        //try
                                        //{
                                        //    SMPP_SMS_Format objSMPP_SMS_Format = new SMPP_SMS_Format();
                                        //    CommonFunction objCOMFUN = new CommonFunction();
                                        //    string strGetSMS = objSMPP_SMS_Format.TopupFailedEndUser(objODBC.executeScalar_str("SELECT full_name From er_login Where userid='" + intUserID + "'").ToString(), strMobile, dblActualAmt, intInvestmentID.ToString());
                                        //    objCOMFUN.er_SMS_Alert(intUserID, 2, strMobile, 4, strGetSMS, 9, "");
                                        //}
                                        //catch (Exception)
                                        //{

                                        //}
                                    }
                                    catch (Exception)
                                    {

                                    }
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception)
            {

            }
            finally
            {
                ds.Dispose();
            }
        }
        public class MTNSoapResponseParse
        {
            public string ersReference { get; set; }
            public int resultCode { get; set; }
            public string resultDescription { get; set; }


        }

    }
}