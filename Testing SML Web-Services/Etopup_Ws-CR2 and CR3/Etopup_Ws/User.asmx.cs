﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Etopup_Ws.old_App_Code;
using System.Web.Services;
using System.Collections;
using System.Data;

namespace Etopup_Ws
{
    /// <summary>
    /// Summary description for User
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/User.asmx")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    [System.Web.Script.Services.ScriptService]
    public class User : System.Web.Services.WebService
    {
        ODBC objODBC = new ODBC(); Encrypt_Decrypt ED = new Encrypt_Decrypt();
        ODBC_LOG objODBCLOG = new ODBC_LOG();
        CreateFile objCreateFile = new CreateFile();
        CommonFunction objComFUN = new CommonFunction();
        DateTime objDT = new DateTime();
        DateFormat objDatetime =new DateFormat();
        [WebMethod]//intDesignationType = 1 -admin, 2- Member
                   // Activity Type=37
        public DataSet getUserDetails(string strUserName, int intUserId, int intType, string strIPAddress, int intDesignationType, string strMacAddress, string strOSDetails, string strIMEINo, string strGcmID, string strAPPVersion, int intSource, int intCreatedByType)
        {
            System.Data.DataSet ds = new System.Data.DataSet(); string strQuery = string.Empty;
            try
            {
                if (intDesignationType == 1)
                {
                    strQuery = "SELECT `full_name`,`emailid`, `gender`, `mobile`, `province_id`, `province_Name`, `city_name`,`Address`, `usertype_id`,`username` FROM `er_login_admin` where status = 1 and userid ='" + intUserId + "'";
                }
                else
                {
                    strQuery = "SELECT  a.`emailid`, a.`mobile`, a.`full_name`,a.`usertype_id`,a.`province_id`, a.`province_Name`,a.`city_name`, a.`Address`,a.`username`,a.`gender`,a.`commission_rights` FROM `er_login` a  where a.`Active`= 1 and a.userid ='" + intUserId + "'";
                }



                objCreateFile.CreateTextFile(strIPAddress, strMacAddress, strOSDetails, strIMEINo, strGcmID, strAPPVersion, intSource.ToString(), intCreatedByType, 37, "Get User Details", strUserName);
                try
                {
                    ds = objODBC.getDataSet(strQuery);
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        return ds;
                    }
                    else
                    {
                        return objODBC.getresult("Records not found!!!");
                    }
                }
                catch (Exception ex)
                {
                    return objODBC.getresult(ex.Message.ToString());
                }

                finally
                {
                    ds.Dispose();
                }
            }
            catch (Exception ex)
            {
                return objODBC.getresult(ex.Message.ToString());
            }
        }
        [WebMethod]
        public DataSet getUserDetailsAdmin(string strUserName, int intUserId, int intType, string strIPAddress, int intDesignationType, string strMacAddress, string strOSDetails, string strIMEINo, string strGcmID, string strAPPVersion, int intSource, int intCreatedByType)
        {
            System.Data.DataSet ds = new System.Data.DataSet(); string strQuery = string.Empty;
            try
            {
                strQuery = "SELECT `emailid`, `mobile`, `full_name`, `usertype_id`, `province_id`, `province_Name`, `city_name`,`Address`,`username`,`gender` FROM `er_login_admin` where status = 1 and userid ='" + intUserId + "'";

                objCreateFile.CreateTextFile(strIPAddress, strMacAddress, strOSDetails, strIMEINo, strGcmID, strAPPVersion, intSource.ToString(), intCreatedByType, 37, "Get User Details", strUserName);

                try
                {
                    ds = objODBC.getDataSet(strQuery);
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        return ds;
                    }
                    else
                    {
                        return objODBC.getresult("Records not found!!!");
                    }
                }
                catch (Exception ex)
                {
                    return objODBC.getresult(ex.Message.ToString());
                }

                finally
                {
                    ds.Dispose();
                }
            }
            catch (Exception ex)
            {
                return objODBC.getresult(ex.Message.ToString());
            }
        }
        [WebMethod]
        public DataSet getUserMobileDetails(string strUserName, int intUserId, int intType, string strIPAddress, int intDesignationType, string strMacAddress, string strOSDetails, string strIMEINo, string strGcmID, string strAPPVersion, int intSource, int intCreatedByType)
        {
            System.Data.DataSet ds = new System.Data.DataSet(); string strQuery = string.Empty;
            try
            {
                if (intCreatedByType == 1)
                {
                    strQuery = "SELECT `userid`, `mobile`, `operator_id`, `mob_type` FROM `er_mobile` WHERE  userid ='" + intUserId + "' and usertype=1 order by mob_type asc limit 2";
                }
                else
                {
                    strQuery = "SELECT `userid`, `mobile`, `operator_id`, `mob_type` FROM `er_mobile` WHERE  userid ='" + intUserId + "' and usertype=2 order by mob_type asc limit 2";
                }


                objCreateFile.CreateTextFile(strIPAddress, strMacAddress, strOSDetails, strIMEINo, strGcmID, strAPPVersion, intSource.ToString(), intCreatedByType, 37, "Get User Mobile Details", strUserName);
                try
                {
                    ds = objODBC.getDataSet(strQuery);
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        return ds;
                    }
                    else
                    {
                        return objODBC.getresult("Records not found!!!");
                    }
                }
                catch (Exception ex)
                {
                    return objODBC.getresult(ex.Message.ToString());
                }
                finally
                {
                    ds.Dispose();
                }
            }
            catch (Exception ex)
            {
                return objODBC.getresult(ex.Message.ToString());
            }
        }


        [WebMethod]
        public DataSet getUserMobileAndroidDetails(string strUserName, int intUserId, int intType, string strIPAddress, int intDesignationType, string strMacAddress, string strOSDetails, string strIMEINo, string strGcmID, string strAPPVersion, int intSource, int intCreatedByType)
        {
            System.Data.DataSet ds = new System.Data.DataSet(); string strQuery = string.Empty;
            try
            {

                strQuery = "SELECT `userid`, `mobile`, `operator_id`, `mob_type` FROM `er_mobile` WHERE  userid ='" + intUserId + "' and usertype=2 and mob_type='" + intType + "' order by mob_type asc ";

                objCreateFile.CreateTextFile(strIPAddress, strMacAddress, strOSDetails, strIMEINo, strGcmID, strAPPVersion, intSource.ToString(), intCreatedByType, 37, "Get User Mobile Details", strUserName);
                try
                {
                    ds = objODBC.getDataSet(strQuery);
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        return ds;
                    }
                    else
                    {
                        return objODBC.getresult("Records not found!!!");
                    }
                }
                catch (Exception ex)
                {
                    return objODBC.getresult(ex.Message.ToString());
                }
                finally
                {
                    ds.Dispose();
                }
            }
            catch (Exception ex)
            {
                return objODBC.getresult(ex.Message.ToString());
            }
        }

        [WebMethod]
        public ArrayList er_android_getUserMobileDetails(string strUserName, int intUserID, int intType, string strIPAddress, int intDesignationType, string strMacAddress, string strOSDetails, string strIMEINo, string strGcmID, string strAPPVersion, int intSource, int intCreatedByType)
        {
            DataSet ds = new DataSet();
            ArrayList arrlst = new ArrayList();
            try
            {
                ds = getUserMobileAndroidDetails(strUserName, intUserID, intType, strIPAddress, intDesignationType, strMacAddress, strOSDetails, strIMEINo, strGcmID, strAPPVersion, intSource, intCreatedByType);
                arrlst = objComFUN.Get_ArrayLlist_From_Dataset(ds);
            }
            catch (Exception)
            {
            }
            finally
            {
                ds.Dispose();
            }
            return arrlst;

        }







        [WebMethod]
        //intUserType = 1 -admin, 2- Member || intDesignationType = retailer, distributor, sub-distributor
        // int intUserId - 0, string strFullName - 1, string strEmailid - 2, int intGender - 3, string strmobile - 4, int intProvinceID - 5, string strProvinceName - 6, string strCityName - 7, string strAddress - 8, int intUserType - 9, string strIPAddress - 10, int intDesignationType - 11, string strMacAddress - 12, string strOSDetails - 13, string strIMEINo - 14, string strGcmID - 15, string strAPPVersion - 16, int intSource - 17, int intOpertorID - 18,int intOldOperatorID-19,intOldAlternOperatorID-20,string strAlternetMobileNumber-21,string strOldMobileNumber-22,string strOldAlternateMobileNumber-23,int intAlternetOperaterID-24

        // Activity Type=38
        public ArrayList update_profile(string strUserName, string strData, int intType, int intSource)
        {
            ArrayList arrUserParams = new ArrayList(); int intCreatedBYType = 0;
            System.Data.DataSet ds = new System.Data.DataSet(); string strDescription, strQuery = string.Empty; int intActivityType = 16;
            try
            {
                try
                {
                    objCreateFile.CreateTextFile(strData, "", "", "", "", "", "", 1, 37, "update_profile,Data:" + strData, strUserName);
                }
                catch (Exception)
                {

                }

                var data = ED.Decrypt(strUserName, strData, 1, intType, intSource).Split(',');
                //string strMobile = data[4];
                //int intOpertorID = int.Parse(data[18]);
                //int intOldOperatorID = 0;
                //int intOldAlternOperatorID = 0;
                //string strOldAlternateMobileNumber = "0";
                //try
                //{
                //    intOldOperatorID = int.Parse(data[19]);
                //    intOldAlternOperatorID = int.Parse(data[20]);
                //    strOldAlternateMobileNumber = data[23];
                //}
                //catch (Exception)
                //{
                //    intOldOperatorID = 0;
                //    intOldAlternOperatorID = 0;
                //}
                //string strAlternetMobileNumber = data[21];
                //string strOldMobileNumber = data[22];


                try
                {
                    objCreateFile.CreateTextFile(data[11], data[12], data[13], data[14], data[15], data[16], data[17].ToString(), Convert.ToInt32(data[9]), intActivityType, "Update Profile,Data:" + strData, strUserName);
                }
                catch (Exception)
                {

                }

                //int intAlternetOperaterID = int.Parse(data[24]);
                //if (objComFUN.CheckRegixMobile(intOpertorID, data[4]) == "TRUE")
                //{
                    if ((data[1] != null && data[1] != "") || (data[2] != null && data[2] != "") || (data[3] != null && data[3] != "") || (data[4] != null && data[4] != "") || (data[5] != null && data[5] != "") || (data[6] != null && data[6] != "") || (data[7] != null && data[7] != "") || (data[8] != null && data[8] != "") || (data[9] != null && data[9] != ""))
                    {

                        strQuery = "UPDATE er_login SET emailid='" + data[2] + "',gender='" + data[3] + "',full_name='" + data[1] + "',province_id='" + data[5] + "',province_Name='" + data[6] + "',city_name='" + data[7] + "', Address='" + data[8] + "' where userid ='" + data[0] + "'";
                        objODBC.executeNonQuery(strQuery);
                        intCreatedBYType = 2;

                        strDescription = "Profile Update Successfully";
                        //try
                        //{
                        //    objODBC.executeNonQuery("UPDATE er_login SET mobile='" + strMobile + "' where userid ='" + data[0] + "'");

                        //    strQuery = "select count(1) from er_mobile where mobile='" + strMobile + "'";
                        //    int intCHkCOuntExists = objODBC.executeScalar_int(strQuery);
                        //    if (intCHkCOuntExists == 0)
                        //    {
                        //        //objODBC.executeNonQuery("DELETE FROM er_mobile WHERE userid = " + data[0] + " and usertype=2");
                        //        //objODBC.executeNonQuery("INSERT INTO er_mobile(userid,mobile,operator_id,mob_type,usertype) VALUES ('" + data[0] + "','" + strMobile.ToString() + "','" + intOpertorID + "',1,2)");

                        //        objODBC.executeNonQuery("Update er_mobile set mobile='" + strMobile.ToString() + "',operator_id='" + intOpertorID + "' WHERE userid = " + data[0] + " and usertype=2 and mob_type=1");
                        //        objODBC.executeNonQuery("UPDATE er_login SET mobile='" + strMobile + "' where userid ='" + data[0] + "'");
                        //    }
                        //    else
                        //    {
                        //        if (strMobile == strOldMobileNumber)
                        //        {
                        //            strDescription = "Profile Update Successfully !";
                        //        }
                        //        else
                        //        {
                        //            strDescription = "Profile Update Successfully,Mobile Number Already Exist !";
                        //        }
                        //    }



                        //    if (intAlternetOperaterID != 0 && strAlternetMobileNumber != "0")
                        //    {
                        //        strQuery = "select count(1) from er_mobile where mobile='" + strAlternetMobileNumber + "'";

                        //        intCHkCOuntExists = objODBC.executeScalar_int(strQuery);
                        //        if (intCHkCOuntExists == 0)
                        //        {
                        //            if (strAlternetMobileNumber != "0")
                        //            {
                        //                objODBC.executeNonQuery("DELETE FROM er_mobile WHERE userid = " + data[0] + " and mob_type=2 and usertype=2");
                        //                objODBC.executeNonQuery("INSERT INTO er_mobile(userid,mobile,operator_id,mob_type,usertype) VALUES ('" + data[0] + "','" + strAlternetMobileNumber.ToString() + "','" + intAlternetOperaterID + "',2,2)");

                        //            }
                        //        }
                        //        else
                        //        {
                        //            if (strAlternetMobileNumber == strOldAlternateMobileNumber)
                        //            {
                        //                strDescription = "Profile Update Successfully !";
                        //            }
                        //            else
                        //            {
                        //                strDescription = "Profile Update Successfully,Alternet Mobile Number Already Exist !";
                        //            }
                        //        }
                        //    }

                        //}
                        //catch (Exception)
                        //{

                        //}


                        try
                        {
                            objODBCLOG.executeNonQuery("call activity_logs('" + data[0] + "','" + data[11] + "','" + intCreatedBYType + "','" + data[10] + "','" + data[12] + "','" + data[13] + "','" + data[14] + "','" + data[15] + "','" + data[16] + "','" + data[17] + "','" + strDescription + "','" + intActivityType + "','','"+ strUserName + "')");
                            int intCreatedByType = Convert.ToInt32(data[9]);

                            objCreateFile.CreateTextFile(data[11], data[12], data[13], data[14], data[15], data[16], data[17].ToString(), intCreatedByType, intActivityType, "Update Profile", strUserName);
                        }
                        catch (Exception)
                        {

                        }

                        objODBC.executeNonQuery(strQuery);

                        objODBCLOG.executeNonQuery("UPDATE `er_login_log` SET `full_name`='" + data[1] + "',`emailid`='" + data[2] + "' WHERE userid='" + data[0] + "' and usertype='" + data[9] + "'");


                        arrUserParams.Add("TRUE");
                        arrUserParams.Add(strDescription);

                        // SMPP SMS Code Below
                        SMPP_SMS_Format objSMPP_SMS_Format = new SMPP_SMS_Format();
                        string strGetSMS = objSMPP_SMS_Format.EditProfile(strUserName, data[1], intSource).ToString();
                        objComFUN.er_insert_notification(int.Parse(data[0]), "Profile Manager", strGetSMS, intCreatedBYType);
                        SMPP1 objSMPP1 = new SMPP1();
                        string strSMPPResponse = "NA";
                        DataTable dtSMPP = new DataTable();
                        try
                        {


                            dtSMPP = objODBC.getDataTable("Select mobile,operator_id from er_mobile where userid='" + data[0] + "' and usertype='" + data[9] + "' and mob_type=1");

                            if (dtSMPP.Rows.Count > 0)
                            {
                                string strMobile1 = dtSMPP.Rows[0][0].ToString();
                                int intOperaterID = int.Parse(dtSMPP.Rows[0][1].ToString());

                                objComFUN.er_SMS_Alert(int.Parse(data[0]), int.Parse(data[9]), strMobile1, intOperaterID, strGetSMS, 1, strSMPPResponse);

                            }
                        }
                        catch (Exception)
                        {

                        }
                        finally
                        {
                            dtSMPP.Dispose();
                        }

                    //}
                    //else
                    //{

                    //    strDescription = "Kindly enter valid details";
                    //    arrUserParams.Add("FALSE");
                    //    arrUserParams.Add(strDescription);

                    //}
                }
                else
                {

                    strDescription = "Kindly enter valid details";
                    arrUserParams.Add("FALSE");
                    arrUserParams.Add(strDescription);

                }
            }
            catch (Exception ex)
            {
                strDescription = "Profile updated successfully";

                arrUserParams.Add("FALSE");
                arrUserParams.Add(strDescription);
            }
            finally
            {
                ds.Dispose();
            }
            return arrUserParams;
        }






        [WebMethod]
        //intUserType = 1 -admin, 2- Member || intDesignationType = retailer, distributor, sub-distributor
        // int intUserId - 0, string strFullName - 1, string strEmailid - 2, int intGender - 3, string strmobile - 4, int intProvinceID - 5, string strProvinceName - 6, string strCityName - 7, string strAddress - 8, int intUserType - 9, string strIPAddress - 10, int intDesignationType - 11, string strMacAddress - 12, string strOSDetails - 13, string strIMEINo - 14, string strGcmID - 15, string strAPPVersion - 16, int intSource - 17, int intOpertorID = 18,  int intOldOperatorID = 19,int intOldAlternOperatorID=20,string strAlternetMobileNumber=21, string strOldMobileNumber 22,string strOldAlternateMobileNumber = 23,int intAlternetOperaterID = 24

        // Activity Type=38
        public ArrayList update_profileAdmin(string strUserName, string strData, int intType, int intSource, int intUtype)
        {
            ArrayList arrUserParams = new ArrayList();
            System.Data.DataSet ds = new System.Data.DataSet(); string strDescription, strQuery = string.Empty; int intActivityType = 16;
            try
            {
                var data = ED.DecryptAdmin(strUserName, strData, intUtype, intType, intSource).Split(',');
                if ((data[1] != null && data[1] != "") || (data[2] != null && data[2] != "") || (data[3] != null && data[3] != "") || (data[4] != null && data[4] != "") || (data[5] != null && data[5] != "") || (data[6] != null && data[6] != "") || (data[7] != null && data[7] != "") || (data[8] != null && data[8] != "") || (data[9] != null && data[9] != ""))
                {
                    int intOpertorID = int.Parse(data[18]);
                    int intOldOperatorID = int.Parse(data[19]);
                    int intOldAlternOperatorID = int.Parse(data[20]);
                    string strAlternetMobileNumber = data[21];
                    string strOldMobileNumber = data[22];
                    string strOldAlternateMobileNumber = data[23];
                    int intAlternetOperaterID = int.Parse(data[24]);
                    string strMobile = data[4];

                    strQuery = "UPDATE `er_login_admin` SET `full_name`='" + data[1] + "',`emailid`='" + data[2] + "',`gender`='" + data[3] + "', `mobile`='" + data[4] + "', `province_id`='" + data[5] + "', `province_Name`='" + data[6] + "', `city_name`='" + data[7] + "',`Address`='" + data[8] + "',usertype_id='" + data[9] + "'where `status`= 1 and userid ='" + data[0] + "'";
                    strDescription = "Admin profile details updation";


                    try
                    {
                        objODBCLOG.executeNonQuery("call activity_logs('" + data[0] + "','" + data[11] + "','1','" + data[10] + "','" + data[12] + "','" + data[13] + "','" + data[14] + "','" + data[15] + "','" + data[16] + "','" + data[17] + "','" + strDescription + "','" + intActivityType + "','','"+ strUserName + "')");
                        int intCreatedByType = Convert.ToInt32(data[9]);

                        objCreateFile.CreateTextFile(data[11], data[12], data[13], data[14], data[15], data[16], data[17].ToString(), intCreatedByType, intActivityType, "Update Profile", strUserName);
                    }
                    catch (Exception)
                    {

                    }

                    objODBC.executeNonQuery(strQuery);

                    objODBCLOG.executeNonQuery("UPDATE `er_login_log` SET `full_name`='" + data[1] + "',`emailid`='" + data[2] + "',`mobile`='" + strMobile + "' WHERE userid='" + data[0] + "' and usertype='" + data[9] + "'");

                    arrUserParams.Add("TRUE");
                    arrUserParams.Add("Profile updated successfully");


                    //try
                    //{
                    //    try
                    //    {

                    //        objODBC.executeNonQuery("DELETE FROM er_mobile WHERE userid = " + data[0] + "");
                    //        objODBC.executeNonQuery("INSERT INTO er_mobile(userid,mobile,operator_id,mob_type,usertype) VALUES ('" + data[0] + "','" + strMobile.ToString() + "','" + intOpertorID + "',1,1)");
                    //        if (intAlternetOperaterID != 0 && strAlternetMobileNumber != "0")
                    //        {
                    //            objODBC.executeNonQuery("INSERT INTO er_mobile(userid,mobile,operator_id,mob_type,usertype) VALUES ('" + data[0] + "','" + strAlternetMobileNumber.ToString() + "','" + intAlternetOperaterID + "',2,1)");
                    //        }
                    //    }
                    //    catch (Exception)
                    //    {

                    //    }
                    //}
                    //catch (Exception)
                    //{

                    //}







                    // SMPP SMS Code Below
                    SMPP_SMS_Format objSMPP_SMS_Format = new SMPP_SMS_Format();
                    string strGetSMS = objSMPP_SMS_Format.EditProfile(strUserName, data[1], intSource).ToString();
                    objComFUN.er_insert_notification(int.Parse(data[0]), "Profile Manager", strGetSMS, 1);
                    SMPP1 objSMPP1 = new SMPP1();
                    string strSMPPResponse = "NA";
                    DataTable dtSMPP = new DataTable();
                    try
                    {


                        dtSMPP = objODBC.getDataTable("Select mobile,operator_id from er_mobile where userid='" + data[0] + "' and usertype='" + data[9] + "' and mob_type=1");

                        if (dtSMPP.Rows.Count > 0)
                        {
                            string strMobile1 = dtSMPP.Rows[0][0].ToString();
                            int intOperaterID = int.Parse(dtSMPP.Rows[0][1].ToString());

                            objComFUN.er_SMS_Alert(int.Parse(data[0]), int.Parse(data[9]), strMobile1, intOperaterID, strGetSMS, 1, strSMPPResponse);

                        }
                    }
                    catch (Exception)
                    {

                    }
                    finally
                    {
                        dtSMPP.Dispose();
                    }


                }
                else
                {
                    strDescription = "Kindly enter valid details";
                    arrUserParams.Add("FALSE");
                    arrUserParams.Add(strDescription);
                }

            }
            catch (Exception ex)
            {
                strDescription = "OOPS! Something is wrong.";

                arrUserParams.Add("FALSE");
                arrUserParams.Add(strDescription);
            }
            finally
            {
                ds.Dispose();
            }
            return arrUserParams;
        }


        // ActivityType=59

        [WebMethod]
        //intUserType = 1 -admin, 2- Member || intDesignationType = retailer, distributor, sub-distributor
        // int intUserId - 0, string strFullName - 1, string strEmailid - 2, int intGender - 3, string strmobile - 4, int intProvinceID - 5, string strProvinceName - 6, string strCityName - 7, string strAddress - 8, int intUserType - 9, string strIPAddress - 10, int intDesignationType - 11, string strMacAddress - 12, string strOSDetails - 13, string strIMEINo - 14, string strGcmID - 15, string strAPPVersion - 16, int intSource - 17,  int intOpertorID - 18,int intOldOperatorID-19,intOldAlternOperatorID-20,string strAlternetMobileNumber-21,string strOldMobileNumber-22,string strOldAlternateMobileNumber-23,int intAlternetOperaterID-24
        public ArrayList update_profileMember(string strUserName, string strData, int intType, int intSource)
        {
            System.Data.DataSet ds = new System.Data.DataSet();
            string strDescription, strQuery = string.Empty;
            int intActivityType = 59;

            ArrayList arrUserParams = new ArrayList();
            try
            {
                var data = ED.Decrypt(strUserName, strData, 2, intType, intSource).Split(',');

                try
                {
                    objCreateFile.CreateTextFile(data[11], data[12], data[13], data[14], data[15], data[16], data[17].ToString(), Convert.ToInt32(data[9]), intActivityType, "Update Profile Member,Data:" + strData, strUserName);
                }
                catch (Exception)
                {

                }

                //if (objComFUN.CheckRegixMobile(intOpertorID, data[4]) == "TRUE")

                if ((data[1] != null && data[1] != "") || (data[2] != null && data[2] != "") || (data[3] != null && data[3] != "") || (data[4] != null && data[4] != "") || (data[5] != null && data[5] != "") || (data[6] != null && data[6] != "") || (data[7] != null && data[7] != "") || (data[8] != null && data[8] != "") || (data[9] != null && data[9] != ""))
                {
                    try
                    {
                        objODBCLOG.executeNonQuery("call activity_logs('" + data[0] + "','" + data[11] + "','2','" + data[10] + "','" + data[12] + "','" + data[13] + "','" + data[14] + "','" + data[15] + "','" + data[16] + "','" + data[17] + "','Update Member Profile','" + intActivityType + "','','"+ strUserName + "')");

                        objCreateFile.CreateTextFile(data[11], data[12], data[13], data[14], data[15], data[16], data[17].ToString(), 2, intActivityType, "update_profileMember,Data:  + " + strData + "", strUserName);
                    }
                    catch (Exception)
                    {
                    }

                    strQuery = "UPDATE `er_login` SET  `emailid`='" + data[2] + "',`gender`='" + data[3] + "', `full_name`='" + data[1] + "',`province_id`='" + data[5] + "', `province_Name`='" + data[6] + "',`city_name`='" + data[7] + "', `Address`='" + data[8] + "'  where  `Active`= 1 and userid ='" + data[0] + "'";



                    objODBC.executeNonQuery(strQuery);

                    arrUserParams.Add("TRUE");
                    strDescription = "Profile updated successfully.";
                    arrUserParams.Add(strDescription);



                    // SMPP SMS Code Below
                    SMPP_SMS_Format objSMPP_SMS_Format = new SMPP_SMS_Format();
                    string strGetSMS = objSMPP_SMS_Format.EditProfile(strUserName, data[1], intSource).ToString();
                    objComFUN.er_insert_notification(int.Parse(data[0]), "Profile Manager", strGetSMS, 2);
                    SMPP1 objSMPP1 = new SMPP1();
                    string strSMPPResponse = "NA";
                    DataTable dtSMPP = new DataTable();
                    try
                    {
                        dtSMPP = objODBC.getDataTable("Select mobile,operator_id from er_mobile where userid='" + data[0] + "' and usertype=2 and mob_type=1");

                        if (dtSMPP.Rows.Count > 0)
                        {
                            string strMobile = dtSMPP.Rows[0][0].ToString();
                            int intOperaterID = int.Parse(dtSMPP.Rows[0][1].ToString());

                            objComFUN.er_SMS_Alert(int.Parse(data[0]), int.Parse(data[9]), strMobile, intOperaterID, strGetSMS, 1, strSMPPResponse);


                        }
                    }
                    catch (Exception)
                    {

                    }
                    finally
                    {
                        dtSMPP.Dispose();
                    }



                    objODBCLOG.executeNonQuery("UPDATE `er_login_log` SET `full_name`='" + data[1] + "',`emailid`='" + data[2] + "',`mobile`='" + data[4] + "' WHERE userid='" + data[0] + "' and usertype='" + data[9] + "'");


                }
                else
                {
                    arrUserParams.Add("FALSE");
                    strDescription = "Kindly Enter Valid Details Profile Update For Member";
                    arrUserParams.Add(strDescription);

                }


            }
            catch (Exception ex)
            {
                arrUserParams.Add("FALSE");
                strDescription = "OOPS! Something is wrong.";
            }
            return arrUserParams;
        }



        //  int intActivityType = 73;
        public string ws_active_deactive_subadmin(string strUserName, int intUserID, int intStatus, string strIPAddress, int intDesignationType, string strMacAddress, string strOSDetails, string strIMEINo, string strGcmID, string strAPPVersion, int intSource, int intCreatedByType)
        {
            int intActivityType = 73;
            DataSet ds = new DataSet();
            string strMessage = string.Empty;
            string strLimit = string.Empty;
            if (intStatus > 0 && intStatus < 3 && intUserID > 0)
            {
                try
                {
                    string strQuery = "select count(1) from er_login_admin where userid=" + intUserID + "";
                    int chkGroupIDCount = objODBC.executeScalar_int(strQuery);
                    if (chkGroupIDCount > 0)
                    {
                        string strQuery1 = "update er_login_admin set status=" + intStatus + " where userid=" + intUserID + "";
                        objODBC.executeNonQuery(strQuery1);

                        strMessage = "status updated successfully";
                    }
                    else
                    {
                        strMessage = "please send valid details";
                    }
                }
                catch (Exception)
                {
                    strMessage = "somthing went wrong";

                }
            }
            else
            {
                strMessage = "please send valid details";

            }
            try
            {
                objODBCLOG.executeNonQuery("call activity_logs('" + intUserID + "','" + intDesignationType + "','" + intCreatedByType + "','" + strIPAddress + "','" + strMacAddress + "','" + strOSDetails + "','" + strIMEINo + "','" + strGcmID + "','" + strAPPVersion + "','" + intSource + "','Active / Deactive Sub admin','" + intActivityType + "','','')");

                objCreateFile.CreateTextFile(strIPAddress, strMacAddress, strOSDetails, strIMEINo, strGcmID, strAPPVersion, intSource.ToString(), intCreatedByType, intActivityType, strMessage, strUserName);
            }
            catch (Exception)
            {

            }
            return strMessage;
        }

        [WebMethod]
        public ArrayList er_ExpireMobileOTP(string strUserName, int intUserID, string strOldMobileNumber, int intOldMobileID, string strNewMobileNumber, int intNewOperaterID, int intMobileType, string strOTP, string strIPAddress, int intDesignationType, string strMacAddress, string strOSDetails, string strIMEINo, string strGcmID, string strAPPVersion, int intSource, int intCreatedByType)
        {
            ArrayList arrayList = new ArrayList();

            string strOTPData = "NA";
            DataSet ds = new DataSet();
            try
            {
                objCreateFile.CreateTextFile(strIPAddress, strMacAddress, strOSDetails, strIMEINo, strGcmID, strAPPVersion, intSource.ToString(), intCreatedByType, 326, "Update Mobile Number", strUserName);
            }
            catch (Exception)
            {

            }
            try
            {
                strOTPData = objComFUN.er_ExpireOTP(intUserID, strNewMobileNumber, strOTP, intCreatedByType);
                string[] strOTPArray = strOTPData.Split(new char[] { ',' });

                if (strOTPArray[0].ToString() == "TRUE")
                {



                    if (strNewMobileNumber != strOldMobileNumber)
                    {
                        // For Mobile
                        int intCountMob = objODBC.executeScalar_int("Select count(1) from er_mobile WHERE userid='" + intUserID + "' and `operator_id`='" + intNewOperaterID + "' and `mobile`='" + strNewMobileNumber + "'");
                        if (intCountMob == 0)
                        {
                            if (intMobileType == 1)
                            {
                                objODBCLOG.executeNonQuery("UPDATE `er_login_log` SET `mobile`='" + strNewMobileNumber + "' WHERE userid='" + intUserID + "' and usertype='" + intCreatedByType + "'");

                                objODBC.executeNonQuery("DELETE FROM er_mobile WHERE userid = " + intUserID + " and mob_type='" + intMobileType + "' and usertype=2");

                                try
                                {
                                    objODBC.executeNonQuery("INSERT INTO er_mobile(userid,mobile,operator_id,mob_type,usertype) VALUES ('" + intUserID + "','" + strNewMobileNumber.ToString() + "','" + intNewOperaterID + "','" + intMobileType + "',2)");
                                }
                                catch (Exception)
                                {

                                }
                            }
                            else
                            {
                                objODBC.executeNonQuery("DELETE FROM er_mobile WHERE userid = " + intUserID + " and mob_type='" + intMobileType + "' and usertype=2");

                                try
                                {
                                    objODBC.executeNonQuery("INSERT INTO er_mobile(userid,mobile,operator_id,mob_type,usertype) VALUES ('" + intUserID + "','" + strNewMobileNumber.ToString() + "','" + intNewOperaterID + "','" + intMobileType + "',2)");
                                }
                                catch (Exception)
                                {

                                }
                            }
                            try
                            {


                                objODBC.executeNonQuery("UPDATE `er_login` SET `mobile`='" + strNewMobileNumber + "'  WHERE  userid='" + intUserID + "'");


                            }
                            catch (Exception)
                            {

                            }



                            arrayList.Add(strOTPArray[0].ToString());
                            arrayList.Add("Mobile Details Update Successfully");
                        }

                        else
                        {
                            arrayList.Add("FALSE");
                            arrayList.Add("New Mobile Number Already Exist !");
                        }

                    }
                    else
                    {
                        arrayList.Add("FALSE");
                        arrayList.Add("Your Mobile Number Already Present!");
                    }
                }
                else
                {
                    arrayList.Add(strOTPArray[0].ToString());
                    arrayList.Add(strOTPArray[2].ToString());
                }
            }
            catch (Exception)
            {

                arrayList.Add("FALSE");
                arrayList.Add("Something went wrong !");

            }
            finally
            {
                ds.Dispose();
            }
            return arrayList;
        }

        [WebMethod]
        public ArrayList getOtpDataforMobile(string strUserName, int intUserID, string strMobile, int intOperaterID, int intMobType, string strIPAddress, int intDesignationType, string strMacAddress, string strOSDetails, string strIMEINo, string strGcmID, string strAPPVersion, int intSource, int intCreatedByType)
        {
            ArrayList arrayList = new ArrayList();
            string strOTP = "NA";
            try
            {
                objCreateFile.CreateTextFile(strIPAddress, strMacAddress, strOSDetails, strIMEINo, strGcmID, strAPPVersion, intSource.ToString(), intCreatedByType, 222, "get OTP for Mobile Number" + strMobile + " by user" + intUserID.ToString(), strUserName);
            }
            catch (Exception)
            {

            }
            try
            {
                if ((objComFUN.CheckRegixMobile(intOperaterID, strMobile) == "TRUE"))
                {

                    int intCountMob = objODBC.executeScalar_int("Select count(1) from er_mobile WHERE userid='" + intUserID + "' and `operator_id`='" + intOperaterID + "' and `mobile`='" + strMobile + "'");
                    if (intCountMob == 0)
                    {
                        string strExpireTime = "00:02:00";
                        strOTP = objComFUN.er_getOTP(intUserID, strMobile, intCreatedByType, strExpireTime);
                        if (strOTP != "NA")
                        {
                            arrayList.Add("TRUE");
                            arrayList.Add(strOTP);

                            // SMPP SMS Code Below
                            SMPP_SMS_Format objSMPP_SMS_Format = new SMPP_SMS_Format();

                            string strGetSMS = objSMPP_SMS_Format.GetOTPtoChangeMobile(strUserName, objODBC.executeScalar_str("SELECT full_name From er_login Where userid='" + intUserID + "'").ToString(), strMobile, strOTP, strExpireTime).ToString();

                            SMPP1 objSMPP1 = new SMPP1();
                            string strSMPPResponse = "NA";
                            DataTable dtSMPP = new DataTable();
                            try
                            {

                                objComFUN.er_SMS_Alert(intUserID, 2, strMobile, intOperaterID, strGetSMS, 1, strSMPPResponse);

                            }
                            catch (Exception)
                            {

                            }
                            finally
                            {
                                dtSMPP.Dispose();
                            }
                        }
                        else
                        {
                            arrayList.Add("FALSE");
                            arrayList.Add(strOTP);
                        }
                    }
                    else
                    {
                        arrayList.Add("FALSE");
                        arrayList.Add("Mobile number already exist !");
                    }
                }
                else
                {
                    if (intMobType == 1)
                    {
                        arrayList.Add("FALSE");
                        arrayList.Add("Mobile Number does not belongs to selected operator !");
                    }
                    else
                    {
                        arrayList.Add("FALSE");
                        arrayList.Add("Alternet Mobile Number does not belongs to selected operator !");
                    }

                }
            }
            catch (Exception)
            {
                arrayList.Add("FALSE");
                arrayList.Add(strOTP);
            }
            return arrayList;
        }




        [WebMethod]
        public ArrayList getOtpDataforAdminMobile(string strUserName, int intUserID, string strMobile, int intOperaterID, int intMobType, string strIPAddress, int intDesignationType, string strMacAddress, string strOSDetails, string strIMEINo, string strGcmID, string strAPPVersion, int intSource, int intCreatedByType)
        {
            ArrayList arrayList = new ArrayList();
            string strOTP = "NA";
            try
            {
                objCreateFile.CreateTextFile(strIPAddress, strMacAddress, strOSDetails, strIMEINo, strGcmID, strAPPVersion, intSource.ToString(), intCreatedByType, 222, "get OTP for Admin Mobile Number" + strMobile + " by user" + intUserID.ToString(), strUserName);
            }
            catch (Exception)
            {

            }
            try
            {
                if ((objComFUN.CheckRegixMobile(intOperaterID, strMobile) == "TRUE"))
                {

                    int intCountMob = objODBC.executeScalar_int("Select count(1) from er_mobile WHERE userid='" + intUserID + "' and `operator_id`='" + intOperaterID + "' and `mobile`='" + strMobile + "' and usertype=1");
                    if (intCountMob == 0)
                    {
                        string strExpireTime = "00:02:00";
                        intCreatedByType = 1;
                        strOTP = objComFUN.er_getOTP(intUserID, strMobile, intCreatedByType, strExpireTime);
                        if (strOTP != "NA")
                        {
                            arrayList.Add("TRUE");
                            arrayList.Add(strOTP);

                            // SMPP SMS Code Below
                            SMPP_SMS_Format objSMPP_SMS_Format = new SMPP_SMS_Format();

                            string strGetSMS = objSMPP_SMS_Format.GetOTPtoChangeMobile(strUserName, objODBC.executeScalar_str("SELECT full_name From er_login_admin Where userid='" + intUserID + "'").ToString(), strMobile, strOTP, strExpireTime).ToString();

                            SMPP1 objSMPP1 = new SMPP1();
                            string strSMPPResponse = "NA";
                            DataTable dtSMPP = new DataTable();
                            try
                            {

                                objComFUN.er_SMS_Alert(intUserID, 2, strMobile, intOperaterID, strGetSMS, 1, strSMPPResponse);

                            }
                            catch (Exception)
                            {

                            }
                            finally
                            {
                                dtSMPP.Dispose();
                            }
                        }
                        else
                        {
                            arrayList.Add("FALSE");
                            arrayList.Add(strOTP);
                        }
                    }
                    else
                    {
                        arrayList.Add("FALSE");
                        arrayList.Add("Mobile number already exist !");
                    }
                }
                else
                {
                    if (intMobType == 1)
                    {
                        arrayList.Add("FALSE");
                        arrayList.Add("Mobile Number does not belongs to selected operator !");
                    }
                    else
                    {
                        arrayList.Add("FALSE");
                        arrayList.Add("Alternet Mobile Number does not belongs to selected operator !");
                    }

                }
            }
            catch (Exception)
            {
                arrayList.Add("FALSE");
                arrayList.Add(strOTP);
            }
            return arrayList;
        }


        [WebMethod]
        public ArrayList er_ExpireAdminMobileOTP(string strUserName, int intUserID, string strOldMobileNumber, int intOldMobileID, string strNewMobileNumber, int intNewOperaterID, int intMobileType, string strOTP, string strIPAddress, int intDesignationType, string strMacAddress, string strOSDetails, string strIMEINo, string strGcmID, string strAPPVersion, int intSource, int intCreatedByType)
        {
            ArrayList arrayList = new ArrayList();

            string strOTPData = "NA";
            DataSet ds = new DataSet();
            try
            {
                objODBCLOG.executeNonQuery("call activity_logs('" + intUserID + "','" + intDesignationType + "','" + intCreatedByType + "','" + strIPAddress + "','" + strMacAddress + "','" + strOSDetails + "','" + strIMEINo + "','" + strGcmID + "','" + strAPPVersion + "','" + intSource + "','Update Mobile Number by Admin','887','','" + strUserName + "')");

                objCreateFile.CreateTextFile(strIPAddress, strMacAddress, strOSDetails, strIMEINo, strGcmID, strAPPVersion, intSource.ToString(), intCreatedByType, 326, "Update Mobile Number For Admin", strUserName);
            }
            catch (Exception)
            {

            }
            try
            {
                strOTPData = objComFUN.er_ExpireOTP(intUserID, strNewMobileNumber, strOTP, intCreatedByType);
                string[] strOTPArray = strOTPData.Split(new char[] { ',' });

                if (strOTPArray[0].ToString() == "TRUE")
                {



                    if (strNewMobileNumber != strOldMobileNumber)
                    {
                        // For Mobile
                        int intCountMob = objODBC.executeScalar_int("Select count(1) from er_mobile WHERE userid='" + intUserID + "' and `operator_id`='" + intNewOperaterID + "' and `mobile`='" + strNewMobileNumber + "' and usertype=1 ");
                        if (intCountMob == 0)
                        {
                            intCreatedByType = 1;
                            if (intMobileType == 1)
                            {
                                objODBCLOG.executeNonQuery("UPDATE `er_login_log` SET `mobile`='" + strNewMobileNumber + "' WHERE userid='" + intUserID + "' and usertype='" + intCreatedByType + "'");
                            }
                            try
                            {
                                objODBC.executeNonQuery("UPDATE `er_mobile` SET `operator_id`='" + intNewOperaterID + "',`mobile`='" + strNewMobileNumber + "' WHERE  userid='" + intUserID + "' and `operator_id`='" + intOldMobileID + "' and `mobile`='" + strOldMobileNumber + "'  and mob_type='" + intMobileType + "'");

                                if (intMobileType == 1)
                                {
                                    objODBC.executeNonQuery("UPDATE `er_login_admin` SET `mobile`='" + strNewMobileNumber + "' WHERE  userid='" + intUserID + "'");
                                }
                                if (intCreatedByType == 1 && intMobileType == 1)
                                {
                                    objODBC.executeNonQuery("UPDATE `er_login_admin` SET `mobile`='" + strNewMobileNumber + "' WHERE  userid='" + intUserID + "'");
                                }
                            }
                            catch (Exception)
                            {

                            }
                            try
                            {

                                objODBC.executeNonQuery("DELETE FROM er_mobile WHERE userid = " + intUserID + " and mob_type='" + intMobileType + "' and usertype=1");
                                objODBC.executeNonQuery("INSERT INTO er_mobile(userid,mobile,operator_id,mob_type,usertype) VALUES ('" + intUserID + "','" + strNewMobileNumber.ToString() + "','" + intNewOperaterID + "','" + intMobileType + "',1)");

                            }
                            catch (Exception)
                            {

                            }
                            arrayList.Add(strOTPArray[0].ToString());
                            arrayList.Add("Mobile Details Update Successfully");
                        }

                        else
                        {
                            arrayList.Add("FALSE");
                            arrayList.Add("New Mobile Number Already Exist !");
                        }

                    }
                    else
                    {
                        arrayList.Add("FALSE");
                        arrayList.Add("Your Mobile Number Already Present!");
                    }

                }
                else
                {
                    arrayList.Add(strOTPArray[0].ToString());
                    arrayList.Add(strOTPArray[2].ToString());
                }
            }
            catch (Exception)
            {

                arrayList.Add("FALSE");
                arrayList.Add("Something went wrong !");

            }
            finally
            {
                ds.Dispose();
            }
            return arrayList;
        }

        [WebMethod]
        public ArrayList getOtpDataForMPin(string strUserName, int intUserID, string strIPAddress, int intDesignationType, string strMacAddress, string strOSDetails, string strIMEINo, string strGcmID, string strAPPVersion, int intSource, int intCreatedByType)
        {
            ArrayList arrayList = new ArrayList();
            string strOTP = "NA";
            try
            {
                objCreateFile.CreateTextFile(strIPAddress, strMacAddress, strOSDetails, strIMEINo, strGcmID, strAPPVersion, intSource.ToString(), intCreatedByType, 222, "get OTP for userid" + intUserID + " by user" + intUserID.ToString(), strUserName);
            }
            catch (Exception)
            {

            }
            DataSet ds = new DataSet();
            try
            {
                ds = objODBC.getDataSet("SELECT `mobile`, `operator_id` FROM `er_mobile` WHERE userid='" + intUserID + "' and usertype='" + intCreatedByType + "' and mob_type=1");
                if (ds.Tables[0].Rows.Count > 0)
                {
                    string strMobile = ds.Tables[0].Rows[0][0].ToString();
                    int intOperaterID = int.Parse(ds.Tables[0].Rows[0][1].ToString());

                    if ((objComFUN.CheckRegixMobile(intOperaterID, strMobile) == "TRUE"))
                    {
                        string strExpireTime = "00:02:00";
                        strOTP = objComFUN.er_getOTP(intUserID, strMobile, intCreatedByType, strExpireTime);
                        if (strOTP != "NA")
                        {
                            arrayList.Add("TRUE");
                            arrayList.Add(strOTP);

                            // SMPP SMS Code Below
                            SMPP_SMS_Format objSMPP_SMS_Format = new SMPP_SMS_Format();

                            string strGetSMS = objSMPP_SMS_Format.GetOTPforgetMPIN(strUserName, objODBC.executeScalar_str("SELECT full_name From er_login Where userid='" + intUserID + "'").ToString(), strMobile, strOTP, strExpireTime).ToString();

                            SMPP1 objSMPP1 = new SMPP1();
                            string strSMPPResponse = "NA";
                            DataTable dtSMPP = new DataTable();
                            try
                            {

                                objComFUN.er_SMS_Alert(intUserID, 2, strMobile, intOperaterID, strGetSMS, 1, strSMPPResponse);

                            }
                            catch (Exception)
                            {

                            }
                            finally
                            {
                                dtSMPP.Dispose();
                            }
                        }
                        else
                        {
                            arrayList.Add("FALSE");
                            arrayList.Add(strOTP);
                        }

                    }
                    else
                    {
                        arrayList.Add("FALSE");
                        arrayList.Add("Mobile Number does not belongs to selected operator !");
                    }
                }
                else
                {
                    arrayList.Add("FALSE");
                    arrayList.Add("Kindly Enter Valid Details !");
                }
            }
            catch (Exception)
            {
                arrayList.Add("FALSE");
                arrayList.Add(strOTP);
            }
            finally
            {
                ds.Dispose();
            }
            return arrayList;
        }


        [WebMethod]
        public ArrayList er_ExpireMpinOTP(string strUserName, int intUserID, string strOTP, string strIPAddress, int intDesignationType, string strMacAddress, string strOSDetails, string strIMEINo, string strGcmID, string strAPPVersion, int intSource, int intCreatedByType)
        {
            ArrayList arrayList = new ArrayList();

            string strOTPData = "NA";

            try
            {
                objCreateFile.CreateTextFile(strIPAddress, strMacAddress, strOSDetails, strIMEINo, strGcmID, strAPPVersion, intSource.ToString(), intCreatedByType, 326, "Update Mobile Number", strUserName);
            }
            catch (Exception)
            {

            }
            DataSet dsData = new DataSet();
            try
            {
                dsData = objODBC.getDataSet("SELECT m.`mobile`,m.`operator_id`,l.pass_key FROM `er_mobile` m inner join er_login l on m.userid=l.userid WHERE m.userid='" + intUserID + "' and m.usertype='" + intCreatedByType + "' and m.mob_type=1");
                if (dsData.Tables[0].Rows.Count > 0)
                {
                    string strMobile = dsData.Tables[0].Rows[0][0].ToString();
                    int intOperaterID = int.Parse(dsData.Tables[0].Rows[0][1].ToString());
                    string strPassKey = dsData.Tables[0].Rows[0][2].ToString();

                    strOTPData = objComFUN.er_ExpireOTP(intUserID, strMobile, strOTP, intCreatedByType);
                    string[] strOTPArray = strOTPData.Split(new char[] { ',' });

                    if (strOTPArray[0].ToString() == "TRUE")
                    {
                        arrayList.Add(strOTPArray[0].ToString());
                        arrayList.Add("New M-Pin Send Successfully on your Mobile Number.");

                        // UpdateMpin
                        Encryption objEncrypt = new Encryption();
                        string strSMSMPin = objComFUN.GenrateNumber(4);
                        string MPIN = strSMSMPin;

                        strSMSMPin = objEncrypt.EncryptQueryString(MPIN, strPassKey);
                        objODBC.executeNonQuery("update er_login set M_Pin='" + strSMSMPin + "' where userid='" + intUserID + "'");

                        // SMPP SMS Code Below
                        SMPP_SMS_Format objSMPP_SMS_Format = new SMPP_SMS_Format();
                        string strGetSMS = objSMPP_SMS_Format.GetForgotMPIN(strUserName, objODBC.executeScalar_str("SELECT full_name From er_login Where userid='" + intUserID + "'").ToString(), strMobile, MPIN).ToString();

                        SMPP1 objSMPP1 = new SMPP1();
                        string strSMPPResponse = "NA";
                        DataTable dtSMPP = new DataTable();
                        try
                        {

                            objComFUN.er_SMS_Alert(intUserID, 2, strMobile, intOperaterID, strGetSMS, 1, strSMPPResponse);

                        }
                        catch (Exception)
                        {

                        }
                        finally
                        {
                            dtSMPP.Dispose();
                        }
                    }
                    else
                    {
                        arrayList.Add(strOTPArray[0].ToString());
                        arrayList.Add(strOTPArray[2].ToString());
                    }
                }
                else
                {
                    arrayList.Add("FALSE");
                    arrayList.Add("Kindly Enter Valid Details !");
                }
            }
            catch (Exception)
            {

                arrayList.Add("FALSE");
                arrayList.Add("Something went wrong !");

            }
            finally
            {
                dsData.Dispose();
            }
            return arrayList;
        }



        [WebMethod]
        public ArrayList getOtpDataForForgotPasswordWeb(string strUserName, string strIPAddress, int intDesignationType, string strMacAddress, string strOSDetails, string strIMEINo, string strGcmID, string strAPPVersion, int intSource, int intCreatedByType)
        {
            ArrayList arrayList = new ArrayList();
            DataTable dt2 = new DataTable();
            string strOTP = "NA";
            try
            {
                objCreateFile.CreateTextFile(strIPAddress, strMacAddress, strOSDetails, strIMEINo, strGcmID, strAPPVersion, intSource.ToString(), intCreatedByType, 222, "get OTP for user name Forgot Password" + strUserName + " by user name" + strUserName.ToString(), strUserName);
            }
            catch (Exception)
            {

            }
            try
            {

                string strQueryValidation = "SELECT Count(1) From er_login l inner join er_mobile m on l.userid=m.userid Where (l.username='" + strUserName + "'  or m.mobile='" + strUserName + "')";

                int intCount = objODBC.executeScalar_int(strQueryValidation);

                if (intCount > 0)
                {

                    int intForgotCount = objODBC.executeScalar_int("SELECT Count(1) From er_login l inner join er_mobile m on l.userid=m.userid Where (l.username='" + strUserName + "'  or m.mobile='" + strUserName + "') and l.user_status=1 and l.Active=1");

                    if (intForgotCount > 0)
                    {

                        dt2 = objODBC.getDataTable("SELECT l.userid,l.pass_key From er_login l inner join er_mobile m on l.userid=m.userid where (l.username='" + strUserName + "'  or m.mobile='" + strUserName + "') and l.user_status=1  and l.Active=1 limit 1");
                        if (dt2.Rows.Count > 0)
                        {

                            int intUserID = int.Parse(dt2.Rows[0][0].ToString());
                            string passkey = dt2.Rows[0][1].ToString();

                            DataSet ds = new DataSet();
                            try
                            {
                                ds = objODBC.getDataSet("SELECT `mobile`, `operator_id` FROM `er_mobile` WHERE userid='" + intUserID + "' and usertype='" + intCreatedByType + "' and mob_type=1");
                                if (ds.Tables[0].Rows.Count > 0)
                                {
                                    string strMobile = ds.Tables[0].Rows[0][0].ToString();
                                    int intOperaterID = int.Parse(ds.Tables[0].Rows[0][1].ToString());

                                    if ((objComFUN.CheckRegixMobile(intOperaterID, strMobile) == "TRUE"))
                                    {
                                        string strExpireTime = "00:02:00";
                                        strOTP = objComFUN.er_getOTP(intUserID, strMobile, intCreatedByType, strExpireTime);
                                        if (strOTP != "NA")
                                        {
                                            arrayList.Add("TRUE");
                                            arrayList.Add(strOTP);

                                            // SMPP SMS Code Below
                                            SMPP_SMS_Format objSMPP_SMS_Format = new SMPP_SMS_Format();

                                            string strGetSMS = objSMPP_SMS_Format.GetOTPforgetPassword(strUserName, objODBC.executeScalar_str("SELECT full_name From er_login Where userid='" + intUserID + "'").ToString(), strMobile, strOTP, strExpireTime).ToString();

                                            SMPP1 objSMPP1 = new SMPP1();
                                            string strSMPPResponse = "NA";
                                            DataTable dtSMPP = new DataTable();
                                            try
                                            {
                                                objComFUN.er_SMS_Alert(intUserID, 2, strMobile, intOperaterID, strGetSMS, 1, strSMPPResponse);
                                            }
                                            catch (Exception)
                                            {

                                            }
                                            finally
                                            {
                                                dtSMPP.Dispose();
                                            }
                                        }
                                        else
                                        {
                                            arrayList.Add("FALSE");
                                            arrayList.Add(strOTP);
                                        }

                                    }
                                    else
                                    {
                                        arrayList.Add("FALSE");
                                        arrayList.Add("Mobile Number does not belongs to selected operator !");
                                    }
                                }
                                else
                                {
                                    arrayList.Add("FALSE");
                                    arrayList.Add("Kindly Enter Valid Details !");
                                }
                            }
                            catch (Exception)
                            {
                                arrayList.Add("FALSE");
                                arrayList.Add(strOTP);
                            }
                            finally
                            {
                                ds.Dispose();
                            }
                        }
                        else
                        {
                            arrayList.Add("FALSE");
                            arrayList.Add("Invalid Details !");

                        }
                    }
                    else
                    {
                        arrayList.Add("FALSE");
                        arrayList.Add("Your account has been de-activated, for more information please call (+93-748436436) !");

                    }

                }
                else
                {
                    arrayList.Add("FALSE");
                    arrayList.Add("You are not a subscribe Member !");

                }
            }
            catch (Exception)
            {
                arrayList.Add("FALSE");
                arrayList.Add("Invalid Details !");
            }
            finally
            {
                dt2.Dispose();
            }
            return arrayList;
        }

        [WebMethod]
        public ArrayList getOtpDataForForgotPassword_Dis_SDIS(string strUserName, string strIPAddress, int intDesignationType, string strMacAddress, string strOSDetails, string strIMEINo, string strGcmID, string strAPPVersion, int intSource, int intCreatedByType)
        {
            ArrayList arrayList = new ArrayList();
            DataTable dt2 = new DataTable();
            string strOTP = "NA";
            try
            {
                objCreateFile.CreateTextFile(strIPAddress, strMacAddress, strOSDetails, strIMEINo, strGcmID, strAPPVersion, intSource.ToString(), intCreatedByType, 222, "get OTP for user name Forgot Password" + strUserName + " by user name" + strUserName.ToString(), strUserName);
            }
            catch (Exception)
            {

            }
            try
            {
                // and usertype_id='" + intDesignationType + "'
                string strQueryValidation = "SELECT Count(1) From er_login l inner join er_mobile m on l.userid=m.userid Where (l.username='" + strUserName + "'  or m.mobile='" + strUserName + "') and usertype_id>2 and usertype_id<5";

                int intCount = objODBC.executeScalar_int(strQueryValidation);

                if (intCount > 0)
                {

                    int intForgotCount = objODBC.executeScalar_int("SELECT Count(1) From er_login l inner join er_mobile m on l.userid=m.userid Where (l.username='" + strUserName + "'  or m.mobile='" + strUserName + "') and l.user_status=1 and l.Active=1");

                    if (intForgotCount > 0)
                    {

                        dt2 = objODBC.getDataTable("SELECT l.userid,l.pass_key From er_login l inner join er_mobile m on l.userid=m.userid where (l.username='" + strUserName + "'  or m.mobile='" + strUserName + "') and l.user_status=1  and l.Active=1 limit 1");
                        if (dt2.Rows.Count > 0)
                        {

                            int intUserID = int.Parse(dt2.Rows[0][0].ToString());
                            string passkey = dt2.Rows[0][1].ToString();

                            DataSet ds = new DataSet();
                            try
                            {
                                ds = objODBC.getDataSet("SELECT `mobile`, `operator_id` FROM `er_mobile` WHERE userid='" + intUserID + "' and usertype='" + intCreatedByType + "' and mob_type=1");
                                if (ds.Tables[0].Rows.Count > 0)
                                {
                                    string strMobile = ds.Tables[0].Rows[0][0].ToString();
                                    int intOperaterID = int.Parse(ds.Tables[0].Rows[0][1].ToString());

                                    if ((objComFUN.CheckRegixMobile(intOperaterID, strMobile) == "TRUE"))
                                    {
                                        string strExpireTime = "00:02:00";
                                        strOTP = objComFUN.er_getOTP(intUserID, strMobile, intCreatedByType, strExpireTime);
                                        if (strOTP != "NA")
                                        {
                                            arrayList.Add("TRUE");
                                            arrayList.Add(strOTP);

                                            // SMPP SMS Code Below
                                            SMPP_SMS_Format objSMPP_SMS_Format = new SMPP_SMS_Format();

                                            string strGetSMS = objSMPP_SMS_Format.GetOTPforgetPassword(strUserName, objODBC.executeScalar_str("SELECT full_name From er_login Where userid='" + intUserID + "'").ToString(), strMobile, strOTP, strExpireTime).ToString();

                                            SMPP1 objSMPP1 = new SMPP1();
                                            string strSMPPResponse = "NA";
                                            DataTable dtSMPP = new DataTable();
                                            try
                                            {
                                                objComFUN.er_SMS_Alert(intUserID, 2, strMobile, intOperaterID, strGetSMS, 1, strSMPPResponse);
                                            }
                                            catch (Exception)
                                            {

                                            }
                                            finally
                                            {
                                                dtSMPP.Dispose();
                                            }
                                        }
                                        else
                                        {
                                            arrayList.Add("FALSE");
                                            arrayList.Add(strOTP);
                                        }

                                    }
                                    else
                                    {
                                        arrayList.Add("FALSE");
                                        arrayList.Add("Mobile Number does not belongs to selected operator !");
                                    }
                                }
                                else
                                {
                                    arrayList.Add("FALSE");
                                    arrayList.Add("Kindly Enter Valid Details !");
                                }
                            }
                            catch (Exception)
                            {
                                arrayList.Add("FALSE");
                                arrayList.Add(strOTP);
                            }
                            finally
                            {
                                ds.Dispose();
                            }
                        }
                        else
                        {
                            arrayList.Add("FALSE");
                            arrayList.Add("Invalid Details !");

                        }
                    }
                    else
                    {
                        arrayList.Add("FALSE");
                        arrayList.Add("Your account has been de-activated, for more information please call (+93-748436436) !");

                    }

                }
                else
                {
                    arrayList.Add("FALSE");
                    arrayList.Add("You are not a subscribe Member !");

                }
            }
            catch (Exception)
            {
                arrayList.Add("FALSE");
                arrayList.Add("Invalid Details !");
            }
            finally
            {
                dt2.Dispose();
            }
            return arrayList;
        }


        [WebMethod]
        public ArrayList getOtpDataForForgotPassword(string strUserName, string strIPAddress, int intDesignationType, string strMacAddress, string strOSDetails, string strIMEINo, string strGcmID, string strAPPVersion, int intSource, int intCreatedByType)
        {
            ArrayList arrayList = new ArrayList();
            DataTable dt2 = new DataTable();
            string strOTP = "NA";
            try
            {
                objCreateFile.CreateTextFile(strIPAddress, strMacAddress, strOSDetails, strIMEINo, strGcmID, strAPPVersion, intSource.ToString(), intCreatedByType, 222, "get OTP for user name Forgot Password" + strUserName + " by user name" + strUserName.ToString(), strUserName);
            }
            catch (Exception)
            {

            }
            try
            {
                // and usertype_id='" + intDesignationType + "'
                string strQueryValidation = "SELECT Count(1) From er_login l inner join er_mobile m on l.userid=m.userid Where (l.username='" + strUserName + "'  or m.mobile='" + strUserName + "') and usertype_id=5";

                int intCount = objODBC.executeScalar_int(strQueryValidation);

                if (intCount > 0)
                {

                    int intForgotCount = objODBC.executeScalar_int("SELECT Count(1) From er_login l inner join er_mobile m on l.userid=m.userid Where (l.username='" + strUserName + "'  or m.mobile='" + strUserName + "') and l.user_status=1 and l.Active=1");

                    if (intForgotCount > 0)
                    {

                        dt2 = objODBC.getDataTable("SELECT l.userid,l.pass_key From er_login l inner join er_mobile m on l.userid=m.userid where (l.username='" + strUserName + "'  or m.mobile='" + strUserName + "') and l.user_status=1  and l.Active=1 limit 1");
                        if (dt2.Rows.Count > 0)
                        {

                            int intUserID = int.Parse(dt2.Rows[0][0].ToString());
                            string passkey = dt2.Rows[0][1].ToString();

                            DataSet ds = new DataSet();
                            try
                            {
                                ds = objODBC.getDataSet("SELECT `mobile`, `operator_id` FROM `er_mobile` WHERE userid='" + intUserID + "' and usertype='" + intCreatedByType + "' and mob_type=1");
                                if (ds.Tables[0].Rows.Count > 0)
                                {
                                    string strMobile = ds.Tables[0].Rows[0][0].ToString();
                                    int intOperaterID = int.Parse(ds.Tables[0].Rows[0][1].ToString());

                                    if ((objComFUN.CheckRegixMobile(intOperaterID, strMobile) == "TRUE"))
                                    {
                                        string strExpireTime = "00:02:00";
                                        strOTP = objComFUN.er_getOTP(intUserID, strMobile, intCreatedByType, strExpireTime);
                                        if (strOTP != "NA")
                                        {
                                            arrayList.Add("TRUE");
                                            arrayList.Add(strOTP);

                                            // SMPP SMS Code Below
                                            SMPP_SMS_Format objSMPP_SMS_Format = new SMPP_SMS_Format();

                                            string strGetSMS = objSMPP_SMS_Format.GetOTPforgetPassword(strUserName, objODBC.executeScalar_str("SELECT full_name From er_login Where userid='" + intUserID + "'").ToString(), strMobile, strOTP, strExpireTime).ToString();

                                            SMPP1 objSMPP1 = new SMPP1();
                                            string strSMPPResponse = "NA";
                                            DataTable dtSMPP = new DataTable();
                                            try
                                            {
                                                objComFUN.er_SMS_Alert(intUserID, 2, strMobile, intOperaterID, strGetSMS, 1, strSMPPResponse);
                                            }
                                            catch (Exception)
                                            {

                                            }
                                            finally
                                            {
                                                dtSMPP.Dispose();
                                            }
                                        }
                                        else
                                        {
                                            arrayList.Add("FALSE");
                                            arrayList.Add(strOTP);
                                        }

                                    }
                                    else
                                    {
                                        arrayList.Add("FALSE");
                                        arrayList.Add("Mobile Number does not belongs to selected operator !");
                                    }
                                }
                                else
                                {
                                    arrayList.Add("FALSE");
                                    arrayList.Add("Kindly Enter Valid Details !");
                                }
                            }
                            catch (Exception)
                            {
                                arrayList.Add("FALSE");
                                arrayList.Add(strOTP);
                            }
                            finally
                            {
                                ds.Dispose();
                            }
                        }
                        else
                        {
                            arrayList.Add("FALSE");
                            arrayList.Add("Invalid Details !");

                        }
                    }
                    else
                    {
                        arrayList.Add("FALSE");
                        arrayList.Add("Your account has been de-activated, for more information please call (+93-748436436) !");

                    }

                }
                else
                {
                    arrayList.Add("FALSE");
                    arrayList.Add("You are not a subscribe Member !");

                }
            }
            catch (Exception)
            {
                arrayList.Add("FALSE");
                arrayList.Add("Invalid Details !");
            }
            finally
            {
                dt2.Dispose();
            }
            return arrayList;
        }


        [WebMethod]
        public ArrayList er_ExpireForgotPasswordOTP(string strUserName, string strOTP, string strIPAddress, int intDesignationType, string strMacAddress, string strOSDetails, string strIMEINo, string strGcmID, string strAPPVersion, int intSource, int intCreatedByType)
        {
            ArrayList arrayList = new ArrayList();
            DataTable dt2 = new DataTable();
            string strOTPData = "NA";

            try
            {
                objCreateFile.CreateTextFile(strIPAddress, strMacAddress, strOSDetails, strIMEINo, strGcmID, strAPPVersion, intSource.ToString(), intCreatedByType, 326, "Update Forgot Passord", strUserName);
            }
            catch (Exception)
            {

            }

            try
            {

                string strQueryValidation = "SELECT Count(1) From er_login l inner join er_mobile m on l.userid=m.userid Where (l.username='" + strUserName + "'  or m.mobile='" + strUserName + "') and l.user_status=1 and l.Active=1";

                int intCount = objODBC.executeScalar_int(strQueryValidation);

                if (intCount > 0)
                {
                    dt2 = objODBC.getDataTable("SELECT l.userid,l.pass_key From er_login l inner join er_mobile m on l.userid=m.userid where (l.username='" + strUserName + "'  or m.mobile='" + strUserName + "') and l.user_status=1  and l.Active=1 limit 1");
                    if (dt2.Rows.Count > 0)
                    {

                        int intUserID = int.Parse(dt2.Rows[0][0].ToString());
                        string passkey = dt2.Rows[0][1].ToString();

                        DataSet dsData = new DataSet();
                        try
                        {
                            dsData = objODBC.getDataSet("SELECT m.`mobile`,m.`operator_id`,l.pass_key FROM `er_mobile` m inner join er_login l on m.userid=l.userid WHERE m.userid='" + intUserID + "' and m.usertype='" + intCreatedByType + "' and m.mob_type=1");
                            if (dsData.Tables[0].Rows.Count > 0)
                            {
                                string strMobile = dsData.Tables[0].Rows[0][0].ToString();
                                int intOperaterID = int.Parse(dsData.Tables[0].Rows[0][1].ToString());
                                string strPassKey = dsData.Tables[0].Rows[0][2].ToString();

                                strOTPData = objComFUN.er_ExpireOTP(intUserID, strMobile, strOTP, intCreatedByType);
                                string[] strOTPArray = strOTPData.Split(new char[] { ',' });

                                if (strOTPArray[0].ToString() == "TRUE")
                                {
                                    arrayList.Add(strOTPArray[0].ToString());
                                    arrayList.Add("New Password Send Successfully on your Mobile Number.");
                                    objODBC.executeNonQuery("update er_login set fource_logout=1 where userid =" + intUserID + "");
                                    // Update Password
                                    Encryption objEncrypt = new Encryption();
                                    string strPassword = objComFUN.GenrateNumber(6);
                                    string strActualPassword = strPassword;

                                    strPassword = objEncrypt.EncryptQueryString(strPassword, strPassKey);
                                    objODBC.executeNonQuery("update er_login set password='" + strPassword + "' where userid='" + intUserID + "'");
                                    try
                                    {
                                        objODBC.executeNonQuery("INSERT INTO `er_forget_password_request`(`userid`, `mobile`, `otp`, `created_on`, `status`) VALUES ('" + intUserID + "','" + strMobile + "','" + strOTP + "','" + objDT.GetDateTimeFormats().ToString() + "',1)");
                                    }
                                    catch (Exception)
                                    {
                                    }
                                    // SMPP SMS Code Below
                                    SMPP_SMS_Format objSMPP_SMS_Format = new SMPP_SMS_Format();
                                    string strExpireTime = "00:02:00";
                                    string strGetSMS = objSMPP_SMS_Format.GetForgotPassword(strUserName, objODBC.executeScalar_str("SELECT full_name From er_login Where userid='" + intUserID + "'").ToString(), strMobile, strActualPassword, strExpireTime).ToString();
                                    try
                                    {
                                        SMPP1 objSMPP1 = new SMPP1();
                                        string strSMPPResponse = "NA";
                                        DataTable dtSMPP = new DataTable();
                                        try
                                        {

                                            objComFUN.er_SMS_Alert(intUserID, 2, strMobile, intOperaterID, strGetSMS, 1, strSMPPResponse);

                                        }
                                        catch (Exception)
                                        {

                                        }
                                        finally
                                        {
                                            dtSMPP.Dispose();
                                        }
                                    }
                                    catch (Exception)
                                    {

                                    }
                                }
                                else
                                {
                                    arrayList.Add(strOTPArray[0].ToString());
                                    arrayList.Add(strOTPArray[2].ToString());
                                }
                            }
                            else
                            {
                                arrayList.Add("FALSE");
                                arrayList.Add("Kindly Enter Valid Details !");
                            }
                        }
                        catch (Exception)
                        {

                            arrayList.Add("FALSE");
                            arrayList.Add("Something went wrong !");

                        }
                        finally
                        {
                            dsData.Dispose();
                        }
                    }
                    else
                    {
                        arrayList.Add("FALSE");
                        arrayList.Add("Invalid Details !");

                    }
                }
                else
                {
                    arrayList.Add("FALSE");
                    arrayList.Add("Invalid Details !");

                }
            }
            catch (Exception)
            {
                arrayList.Add("FALSE");
                arrayList.Add("Invalid Details !");
            }
            finally
            {
                dt2.Dispose();
            }
            return arrayList;
        }





        [WebMethod]
        public ArrayList ResetMPIN(string strUserName, int intUserID, string strIPAddress, int intDesignationType, string strMacAddress, string strOSDetails, string strIMEINo, string strGcmID, string strAPPVersion, int intSource, int intCreatedByType)
        {
            ArrayList arrayList = new ArrayList();
            string strOTPData = "NA";
            try
            {
                objCreateFile.CreateTextFile(strIPAddress, strMacAddress, strOSDetails, strIMEINo, strGcmID, strAPPVersion, intSource.ToString(), intCreatedByType, 326, "Update Mobile Number", strUserName);
            }
            catch (Exception)
            {

            }
            DataSet dsData = new DataSet();
            try
            {
                dsData = objODBC.getDataSet("SELECT m.`mobile`,m.`operator_id`,l.pass_key FROM `er_mobile` m inner join er_login l on m.userid=l.userid WHERE m.userid='" + intUserID + "' and m.usertype='" + intCreatedByType + "' and m.mob_type=1");
                if (dsData.Tables[0].Rows.Count > 0)
                {
                    string strMobile = dsData.Tables[0].Rows[0][0].ToString();
                    int intOperaterID = int.Parse(dsData.Tables[0].Rows[0][1].ToString());
                    string strPassKey = dsData.Tables[0].Rows[0][2].ToString();

                    // UpdateMpin
                    Encryption objEncrypt = new Encryption();
                    string strSMSMPin = objComFUN.GenrateNumber(4);
                    string MPIN = strSMSMPin;

                    strSMSMPin = objEncrypt.EncryptQueryString(MPIN, strPassKey);
                    objODBC.executeNonQuery("update er_login set M_Pin='" + strSMSMPin + "' where userid='" + intUserID + "'");

                    arrayList.Add("TRUE");
                    arrayList.Add("New M-Pin Send Successfully on your Mobile Number.");
                    // SMPP SMS Code Below
                    SMPP_SMS_Format objSMPP_SMS_Format = new SMPP_SMS_Format();

                    string strGetSMS = objSMPP_SMS_Format.GetForgotMPIN(strUserName, objODBC.executeScalar_str("SELECT full_name From er_login Where userid='" + intUserID + "'").ToString(), strMobile, MPIN).ToString();

                    SMPP1 objSMPP1 = new SMPP1();
                    string strSMPPResponse = "NA";
                    DataTable dtSMPP = new DataTable();
                    try
                    {
                        objComFUN.er_SMS_Alert(intUserID, 2, strMobile, intOperaterID, strGetSMS, 1, strSMPPResponse);
                    }
                    catch (Exception)
                    {

                    }
                    finally
                    {
                        dtSMPP.Dispose();
                    }
                }
                else
                {
                    arrayList.Add("FALSE");
                    arrayList.Add("Invalid Details!");
                }
            }
            catch (Exception)
            {
                arrayList.Add("FALSE");
                arrayList.Add("Something went wrong !");
            }
            finally
            {
                dsData.Dispose();
            }
            return arrayList;
        }


        [WebMethod]
        public string SendRegistrationMessage(string strUserName, int intLoginUserID, int intUserID, string strIPAddress, int intDesignationType, string strMacAddress, string strOSDetails, string strIMEINo, string strGcmID, string strAPPVersion, int intSource, int intCreatedByType)
        {

            Encryption objEncrypt = new Encryption();

            try
            {
                objCreateFile.CreateTextFile(strIPAddress, strMacAddress, strOSDetails, strIMEINo, strGcmID, strAPPVersion, intSource.ToString(), intCreatedByType, 326, "SendRegistrationMessage", strUserName);

                int intLogID = objODBCLOG.executeScalar_int("call activity_logs('" + intUserID + "','" + intDesignationType + "','" + intCreatedByType + "','" + strIPAddress + "','" + strMacAddress + "','" + strOSDetails + "','" + strIMEINo + "','" + strGcmID + "','" + strAPPVersion + "','" + intSource + "','SendRegistrationMessage','887','" + intLoginUserID + "','" + strUserName + "')");
            }
            catch (Exception)
            {

            }
            DataSet dsData = new DataSet();
            try
            {
                dsData = objODBC.getDataSet("SELECT l.username,l.full_name,l.password,l.M_Pin,l.pass_key,m.mobile,m.operator_id FROM db_topup_quick_pay.er_login l inner join er_mobile m on l.userid=m.userid Where l.userid='" + intUserID + "' and m.mob_type=1 and m.usertype=2;");
                if (dsData.Tables[0].Rows.Count > 0)
                {

                    string strPasswordDetail = objEncrypt.DecryptQueryString(dsData.Tables[0].Rows[0]["password"].ToString(), dsData.Tables[0].Rows[0]["pass_key"].ToString());
                    string strMPin = objEncrypt.DecryptQueryString(dsData.Tables[0].Rows[0]["M_Pin"].ToString(), dsData.Tables[0].Rows[0]["pass_key"].ToString());
                    // SMPP SMS Code Below
                    SMPP_SMS_Format objSMPP_SMS_Format = new SMPP_SMS_Format();
                    string strGetSMS = objSMPP_SMS_Format.MemberRegistration(dsData.Tables[0].Rows[0]["username"].ToString(), dsData.Tables[0].Rows[0]["full_name"].ToString(), strPasswordDetail, strMPin);                  
                    string strSMPPResponse = "NA";
                    try
                    {
                      
                      //  objComFUN.er_SMS_Alert(intUserID, 2, dsData.Tables[0].Rows[0]["mobile"].ToString(), int.Parse(dsData.Tables[0].Rows[0]["operator_id"].ToString()), strGetSMS, 1, strSMPPResponse);

                        objODBCLOG.executeNonQuery("INSERT INTO `er_wallet_transfer_sms_log`(`userid`, `username`, `mobile`, `operator_id`, `SMS`, `created_on`,`status`, `mobile1`, `operator_id1`, `mobile2`, `operator_id2`) VALUES ('" + intUserID + "','" + strUserName + "','" + dsData.Tables[0].Rows[0]["mobile"].ToString() + "','" + int.Parse(dsData.Tables[0].Rows[0]["operator_id"].ToString()) + "','" + strGetSMS + "','" + objDatetime.getCurDateTimeString() + "',0,'NA','NA','NA','NA')");

                        return "Registration SMS Send Successfully !";
                    }
                    catch (Exception)
                    {
                        return "Something went wrong !";
                    }
                }
                else
                {
                    return "Registration SMS Not Send !";
                }
            }
            catch (Exception)
            {
                return "Something went wrong !";
            }
            finally
            {
                dsData.Dispose();
            }

        }


        [WebMethod]
        public string SendTopupMessage(string strUserName, int intLoginUserID, string strEndUserName, string intTransactionID, string strIPAddress, int intDesignationType, string strMacAddress, string strOSDetails, string strIMEINo, string strGcmID, string strAPPVersion, int intSource, int intCreatedByType)
        {

            Encryption objEncrypt = new Encryption();
            Recharge objrecharge = new Recharge();

            try
            {
                objCreateFile.CreateTextFile(strIPAddress, strMacAddress, strOSDetails, strIMEINo, strGcmID, strAPPVersion, intSource.ToString(), intCreatedByType, 326, "SendTopupMessage", strUserName);

                int intLogID = objODBCLOG.executeScalar_int("call activity_logs('" + intLoginUserID + "','" + intDesignationType + "','" + intCreatedByType + "','" + strIPAddress + "','" + strMacAddress + "','" + strOSDetails + "','" + strIMEINo + "','" + strGcmID + "','" + strAPPVersion + "','" + intSource + "','SendTopupMessage','887','" + strEndUserName + "','" + intTransactionID + "')");
            }
            catch (Exception)
            {

            }
            DataSet dsData = new DataSet();
            try
            {
                dsData = objODBC.getDataSet("SELECT l.userid,l.username,l.full_name,m.mobile,m.operator_id,r.amount,r.operator_id as recOID,r.mobile_number,r.status FROM db_topup_quick_pay.er_login l inner join er_mobile m on l.userid=m.userid inner join er_recharge r on l.userid=r.userid  Where l.username='" + strEndUserName + "' and r.trans_number='" + intTransactionID + "' and m.mob_type=1 and m.usertype=2;");
                if (dsData.Tables[0].Rows.Count > 0)
                {

                    try
                    {
                        try
                        {
                            int RechargeStatus = 0;
                            if (dsData.Tables[0].Rows[0]["status"].ToString() == "2")
                            {
                                RechargeStatus = 1;
                            }
                            else if (dsData.Tables[0].Rows[0]["status"].ToString() == "3")
                            {
                                RechargeStatus = 2;
                            }

                            if (dsData.Tables[0].Rows[0]["recOID"].ToString() == "2" || dsData.Tables[0].Rows[0]["recOID"].ToString() == "3" || dsData.Tables[0].Rows[0]["recOID"].ToString() == "4")
                            {


                                SMPP_SMS_Format objSMPP_SMS_Format = new SMPP_SMS_Format();
                                CommonFunction objCOMFUN = new CommonFunction();
                                string strGetSMS = objSMPP_SMS_Format.TopupSuccessEndUser(dsData.Tables[0].Rows[0]["full_name"].ToString(), dsData.Tables[0].Rows[0]["mobile_number"].ToString(), double.Parse(dsData.Tables[0].Rows[0]["amount"].ToString()), intTransactionID.ToString());
                                objCOMFUN.er_SMS_Alert(int.Parse(dsData.Tables[0].Rows[0]["userid"].ToString()), 2, dsData.Tables[0].Rows[0]["mobile_number"].ToString(), int.Parse(dsData.Tables[0].Rows[0]["recOID"].ToString()), strGetSMS, 9, "");
                            }


                            objrecharge.SendSMPPSMS(int.Parse(dsData.Tables[0].Rows[0]["userid"].ToString()), dsData.Tables[0].Rows[0]["username"].ToString(), dsData.Tables[0].Rows[0]["mobile_number"].ToString(), "", 0, double.Parse(dsData.Tables[0].Rows[0]["amount"].ToString()), RechargeStatus, intTransactionID, "0");


                            return "Topup SMS Send Successfully !";
                        }
                        catch (Exception)
                        {
                            return "Something went wrong !";
                        }
                    }
                    catch (Exception)
                    {
                        return "Something went wrong !";
                    }
                }
                else
                {
                    return "Topup SMS Not Send !";
                }
            }
            catch (Exception)
            {
                return "Something went wrong !";
            }
            finally
            {
                dsData.Dispose();
            }

        }



        [WebMethod]
        public string UpdateMemberMobile_AD(string strUserName, int intLoginUserID, int intUserID, int intMobID, string strMobileNumber, int intOperatorID, int intPrimaryStatus, int intMainStatus, string strIPAddress, int intDesignationType, string strMacAddress, string strOSDetails, string strIMEINo, string strGcmID, string strAPPVersion, int intSource, int intCreatedByType)
        {
            // intMainStatus=1 Insert , intMainStatus=2 Update,intMainStatus=3 Remove
            // intPrimaryStatus=1 Make as primary, intPrimaryStatus=2 No Primary           
            string strResponse = "";
            try
            {
                

                objCreateFile.CreateTextFile(strIPAddress, strMacAddress, strOSDetails, strIMEINo, strGcmID, strAPPVersion, intSource.ToString(), intCreatedByType, 601, "UpdateMobileNumber", strUserName);

                DataSet ds22 = new DataSet();
                try
                {
                    string strMobileAll = " ";
                    ds22 = objODBC.getDataSet("select mobile from er_mobile where userid='"+ intUserID + "' and usertype=2;");
                    int intCount = Convert.ToInt32((ds22.Tables[0].Rows.Count));
                    if (intCount > 0)
                    {
                        for (int i = 0; i < intCount; i++)
                        {
                            strMobileAll = ds22.Tables[0].Rows[i]["mobile"].ToString() + "," + strMobileAll;
                        }
                    }

                    int intLogID = objODBCLOG.executeScalar_int("call activity_logs('" + intUserID + "','" + intDesignationType + "','" + intCreatedByType + "','" + strIPAddress + "','" + strMacAddress + "','" + strOSDetails + "','" + strIMEINo + "','" + strGcmID + "','" + strAPPVersion + "','" + intSource + "','Update Mobile Number','887','" + strMobileAll + "','" + strUserName + "')");

                    objODBCLOG.executeNonQuery("INSERT INTO `er_mobile_number_lod`(`userid`, `username`, `mobileid`, `change_mobilenumber`, `operator_id`, `primary_status`, `m_status`, `log_id`, `all_mobile`, `login_userid`) VALUES('" + intUserID + "','" + strUserName + "','" + intMobID + "','" + strMobileNumber + "','" + intOperatorID + "','" + intPrimaryStatus + "','" + intMainStatus + "','" + intLogID + "','"+ strMobileAll + "','" + intLoginUserID + "')");
                }
                catch(Exception)
                {

                }
                finally
                {
                    ds22.Dispose();
                }
            }
            catch (Exception)
            {

            }
            DataSet dsData = new DataSet();
            try
            {
                if (intMainStatus == 1) // Insert
                {
                    int intCHkCOuntExists = objODBC.executeScalar_int("select count(1) from er_mobile where mobile='" + strMobileNumber + "'");
                    if (intCHkCOuntExists == 0)
                    {
                        if (intPrimaryStatus == 1)
                        {
                            objODBC.executeNonQuery("UPDATE er_login SET mobile='" + strMobileNumber + "' where userid ='" + intUserID + "'");

                          
                            objODBC.executeNonQuery("update er_mobile set mob_type=2 where userid='" + intUserID + "' and usertype=2");
                            objODBC.executeNonQuery("INSERT INTO er_mobile(userid,mobile,operator_id,mob_type,usertype) VALUES ('" + intUserID + "','" + strMobileNumber + "','" + intOperatorID + "',1,2)");
                            strResponse = "Mobile Number Inserted Successfully !";
                            objODBCLOG.executeNonQuery("UPDATE `er_login_log` SET mobile='" + strMobileNumber + "' where userid ='" + intUserID + "' and usertype=2");
                        }
                        else
                        {
                            objODBC.executeNonQuery("INSERT INTO er_mobile(userid,mobile,operator_id,mob_type,usertype) VALUES ('" + intUserID + "','" + strMobileNumber + "','" + intOperatorID + "',2,2)");
                            strResponse = "Alternate Mobile Number Inserted Successfully !";
                        }


                    }
                    else
                    {
                        if (intPrimaryStatus == 1)
                        {
                            strResponse = "Mobile Number Already Exist !";
                        }
                        else
                        {
                            strResponse = "Alternate Mobile Number Already Exist !";
                        }
                    }

                }
                else if (intMainStatus == 2) // Update
                {
                    int intCHkCOuntExists = objODBC.executeScalar_int("select count(1) from er_mobile where mobile='" + strMobileNumber + "'");
                    if (intCHkCOuntExists == 0)
                    {
                        if (intPrimaryStatus == 1)
                        {
                            objODBC.executeNonQuery("UPDATE er_login SET mobile='" + strMobileNumber + "' where userid ='" + intUserID + "'");


                            objODBC.executeNonQuery("update er_mobile set mob_type=2 where userid='" + intUserID + "' and usertype=2");

                            objODBC.executeNonQuery("update er_mobile set mob_type='" + intPrimaryStatus + "',mobile='" + strMobileNumber + "',operator_id='" + intOperatorID + "' where id=" + intMobID + " and userid=" + intUserID + " and usertype=2");
                            strResponse = "Mobile Number Updated Successfully !";

                            objODBCLOG.executeNonQuery("UPDATE `er_login_log` SET mobile='" + strMobileNumber + "' where userid ='" + intUserID + "' and usertype=2");                           
                        }
                        else
                        {
                            objODBC.executeNonQuery("update er_mobile set mob_type=2,mobile='" + strMobileNumber + "',operator_id='" + intOperatorID + "' where id=" + intMobID + " and userid=" + intUserID + " and usertype=2");

                            strResponse = "Alternate Mobile Number Updated Successfully !";
                        }


                    }
                    else
                    {
                        if (intPrimaryStatus == 1)
                        {
                            string strMobprim = objODBC.executeScalar_str("select mobile from  er_mobile where id=" + intMobID + "");
                            objODBC.executeNonQuery("update er_mobile set mob_type=2 where userid='" + intUserID + "' and usertype=2");
                           // objODBC.executeNonQuery("update er_mobile set mob_type='" + intPrimaryStatus + "',mobile='" + strMobileNumber + "',operator_id='" + intOperatorID + "' where id=" + intMobID + " and userid=" + intUserID + " and usertype=2");
                            objODBC.executeNonQuery("update er_mobile set mob_type='" + intPrimaryStatus + "' where id=" + intMobID + " and userid=" + intUserID + " and usertype=2");

                            strResponse = "Mobile Number Set as Primary Number !";
                            objODBCLOG.executeNonQuery("UPDATE `er_login_log` SET mobile='" + strMobprim + "' where userid ='" + intUserID + "' and usertype=2");
                            objODBC.executeNonQuery("UPDATE er_login SET mobile='" + strMobprim + "' where userid ='" + intUserID + "'");
                        }
                        else
                        {
                            strResponse = "Alternate Mobile Number Already Exist !";
                        }
                    }
                }
                else if (intMainStatus == 3) // Remove
                {
                    int intCHkCOuntExists = objODBC.executeScalar_int("select count(1) from er_mobile where mobile='" + strMobileNumber + "' and id='" + intMobID + "' and userid='" + intUserID + "'");
                    if (intCHkCOuntExists == 1)
                    {
                        int intPstatus = objODBC.executeScalar_int("select mob_type from er_mobile where mobile='" + strMobileNumber + "' and id='" + intMobID + "' and userid='" + intUserID + "'");
                        if (intPstatus == 1)
                        {
                            strResponse = "You can not remove Primary Mobile Number , First set Alternet Number as Primary !";
                        }
                        else
                        {
                            int intCHkCOuntExists2 = objODBC.executeScalar_int("select count(1) from er_mobile where mobile='" + strMobileNumber + "' and id='" + intMobID + "' and userid='" + intUserID + "' and mob_type=2 and usertype=2");
                            if (intCHkCOuntExists2 == 1)
                            {
                                objODBC.executeNonQuery("Delete from er_mobile where id='" + intMobID + "' and userid='" + intUserID + "' and mob_type=2 and usertype=2");
                                strResponse = "Alternate Mobile Number Deleted Successfully !";
                            }
                            else
                            {
                                strResponse = "You can not remove Mobile Number , First set Alternet Number !";
                            }
                        }
                    }
                    else
                    {
                        if (intPrimaryStatus == 1)
                        {
                            strResponse = "Mobile Number Not Exist !";
                        }
                        else
                        {
                            strResponse = "Alternate Mobile Number Not Exist !";
                        }
                    }
                }
            }
            catch (Exception)
            {

            }
            finally
            {

            }

            return strResponse;
        }



        [WebMethod]
        public DataSet getAllUserMobileDetails(string strUserName, int intUserId, int intType, string strIPAddress, int intDesignationType, string strMacAddress, string strOSDetails, string strIMEINo, string strGcmID, string strAPPVersion, int intSource, int intCreatedByType)
        {
            System.Data.DataSet ds = new System.Data.DataSet(); string strQuery = string.Empty;
            try
            {
                if (intCreatedByType == 1)
                {
                    strQuery = "SELECT id,`userid`, `mobile`, `operator_id`, `mob_type` FROM `er_mobile` WHERE  userid ='" + intUserId + "' and usertype=1 order by mob_type asc ";
                }
                else
                {
                    strQuery = "SELECT id,`userid`, `mobile`, `operator_id`, `mob_type` FROM `er_mobile` WHERE  userid ='" + intUserId + "' and usertype=2 order by mob_type asc ";
                }


                objCreateFile.CreateTextFile(strIPAddress, strMacAddress, strOSDetails, strIMEINo, strGcmID, strAPPVersion, intSource.ToString(), intCreatedByType, 37, "Get User Mobile Details", strUserName);
                try
                {
                    ds = objODBC.getDataSet(strQuery);
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        return ds;
                    }
                    else
                    {
                        return objODBC.getresult("Records not found!!!");
                    }
                }
                catch (Exception ex)
                {
                    return objODBC.getresult(ex.Message.ToString());
                }
                finally
                {
                    ds.Dispose();
                }
            }
            catch (Exception ex)
            {
                return objODBC.getresult(ex.Message.ToString());
            }
        }

        [WebMethod]
        public ArrayList er_ExpireMobileOTP_new(string strUserName, int intLoginUserID, int intUserID, int intMobID, string strMobileNumber, int intOperatorID, int intPrimaryStatus, int intMainStatus,string strOTP, string strIPAddress, int intDesignationType, string strMacAddress, string strOSDetails, string strIMEINo, string strGcmID, string strAPPVersion, int intSource, int intCreatedByType)
        {
            ArrayList arrayList = new ArrayList();

            string strOTPData = "NA";
            DataSet ds = new DataSet();
            try
            {
                objCreateFile.CreateTextFile(strIPAddress, strMacAddress, strOSDetails, strIMEINo, strGcmID, strAPPVersion, intSource.ToString(), intCreatedByType, 326, "Update Mobile Number", strUserName);
            }
            catch (Exception)
            {

            }
            try
            {
                strOTPData = objComFUN.er_ExpireOTP(intUserID, strMobileNumber, strOTP, intCreatedByType);
                string[] strOTPArray = strOTPData.Split(new char[] { ',' });
                if (strOTPArray[0].ToString() == "TRUE")             
                {

                    string strResponse = UpdateMemberMobile_AD(strUserName, intLoginUserID, intUserID, intMobID, strMobileNumber, intOperatorID, intPrimaryStatus, intMainStatus, strIPAddress, intDesignationType, strMacAddress, strOSDetails, strIMEINo, strGcmID, strAPPVersion, intSource, intCreatedByType);

                    arrayList.Add("TRUE");
                    arrayList.Add(strResponse);
                }
                else
                {
                    arrayList.Add(strOTPArray[0].ToString());
                    arrayList.Add(strOTPArray[2].ToString());
                }
            }
            catch (Exception)
            {

                arrayList.Add("FALSE");
                arrayList.Add("Something went wrong !");

            }
            finally
            {
                ds.Dispose();
            }
            return arrayList;
        }

        [WebMethod]
        public ArrayList getOtpDataforMobile_new(string strUserName, int intUserID, string strMobile, int intOperaterID, int intMobType, string strIPAddress, int intDesignationType, string strMacAddress, string strOSDetails, string strIMEINo, string strGcmID, string strAPPVersion, int intSource, int intCreatedByType)
        {
            ArrayList arrayList = new ArrayList();
            string strOTP = "NA";
            try
            {
                objCreateFile.CreateTextFile(strIPAddress, strMacAddress, strOSDetails, strIMEINo, strGcmID, strAPPVersion, intSource.ToString(), intCreatedByType, 222, "get OTP for Mobile Number" + strMobile + " by user" + intUserID.ToString(), strUserName);
            }
            catch (Exception)
            {

            }
            try
            {
                if ((objComFUN.CheckRegixMobile(intOperaterID, strMobile) == "TRUE"))
                {

                    int intCountMob = objODBC.executeScalar_int("Select count(1) from er_mobile WHERE userid='" + intUserID + "' and `operator_id`='" + intOperaterID + "' and `mobile`='" + strMobile + "' and mob_type=1");
                    if (intCountMob == 0)
                    {
                        string strExpireTime = "00:02:00";
                        strOTP = objComFUN.er_getOTP(intUserID, strMobile, intCreatedByType, strExpireTime);
                        if (strOTP != "NA")
                        {
                            arrayList.Add("TRUE");
                            arrayList.Add(strOTP);

                            // SMPP SMS Code Below
                            SMPP_SMS_Format objSMPP_SMS_Format = new SMPP_SMS_Format();

                            string strGetSMS = objSMPP_SMS_Format.GetOTPtoChangeMobile(strUserName, objODBC.executeScalar_str("SELECT full_name From er_login Where userid='" + intUserID + "'").ToString(), strMobile, strOTP, strExpireTime).ToString();

                            SMPP1 objSMPP1 = new SMPP1();
                            string strSMPPResponse = "NA";
                            DataTable dtSMPP = new DataTable();
                            try
                            {

                                objComFUN.er_SMS_Alert(intUserID, 2, strMobile, intOperaterID, strGetSMS, 1, strSMPPResponse);

                            }
                            catch (Exception)
                            {

                            }
                            finally
                            {
                                dtSMPP.Dispose();
                            }
                        }
                        else
                        {
                            arrayList.Add("FALSE");
                            arrayList.Add(strOTP);
                        }
                    }
                    else
                    {
                        arrayList.Add("FALSE");
                        arrayList.Add("Mobile number already exist !");
                    }
                }
                else
                {
                    if (intMobType == 1)
                    {
                        arrayList.Add("FALSE");
                        arrayList.Add("Mobile Number does not belongs to selected operator !");
                    }
                    else
                    {
                        arrayList.Add("FALSE");
                        arrayList.Add("Alternet Mobile Number does not belongs to selected operator !");
                    }

                }
            }
            catch (Exception)
            {
                arrayList.Add("FALSE");
                arrayList.Add(strOTP);
            }
            return arrayList;
        }
    }
}
