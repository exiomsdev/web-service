﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Etopup_Ws.old_App_Code;
using System.Web.Services;
using System.Collections;
using System.Data;
using System.Text.RegularExpressions;

namespace Etopup_Ws
{
    /// <summary>
    /// Summary description for Login
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class Login : System.Web.Services.WebService
    {
        ODBC objODBC = new ODBC();
        ODBC_LOG objODBCLOG = new ODBC_LOG();
        DataTable dt = new DataTable();
        DataTable dt2 = new DataTable();
        DateFormat objDT = new DateFormat();
        Encrypt_Decrypt ED = new Encrypt_Decrypt();
        CreateFile objCreateFile = new CreateFile();
        old_App_Code.DateFormat objDateFor = new old_App_Code.DateFormat();
        CommonFunction objComFUN = new CommonFunction();
        // ***** Admin Login ******
        //string strUserName - 0,string strPwd - 1,string strIPAddress - 2,int intDesignationType -3,string strMacAddress - 4,string strOSDetails - 5,string strIMEINo - 6,string strGcmID - 7,string strAPPVersion - 8,int intSource -9

        [WebMethod()]
        public ArrayList CheckAdminLogin(string strUserName, string strData, int intType)
        {
            ArrayList arrUserParams = new ArrayList();
            string strDescription = "Admin Login";
            string strResponseFile = "", strSalt = "";
            int intActivityType = 1;
            try
            {
                try
                {
                    objCreateFile.CreateTextFile(strData, "", "", "", "", "", "", 1, 37, "Admin Login,Data:" + strData, strUserName);
                }
                catch (Exception)
                {

                }
                var data = ED.DecryptAdmin(strUserName, strData, 1, 2, 0).Split(',');
                if ((data[0] != "" || data[0] != null) && (data[1] != "" || data[1] != null) && (strUserName == data[0].ToString()))
                {
                    Encryption objEncrypt = new Encryption();

                    string strQueryValidation = "SELECT Count(1) From er_login_admin Where (username='" + data[0] + "' or emailid='" + data[0] + "')  and status=1";

                    int intCount = objODBC.executeScalar_int(strQueryValidation);

                    if (intCount > 0)
                    {

                        dt2 = objODBC.getDataTable("SELECT pass_key,usertype_id From er_login_admin Where (username='" + data[0] + "' or emailid='" + data[0] + "') and status=1");
                        if (dt2.Rows.Count > 0)
                        {
                            string passkey = dt2.Rows[0][0].ToString();
                            string intDesignationType = dt2.Rows[0][1].ToString();
                            string strPassward = objEncrypt.EncryptQueryString(data[1], passkey);
                            string strQuery = "SELECT userid,full_name,usertype_id,last_login_ip,last_login_datetime,emailid,username,auth_status,tpin,encryption_key from er_login_admin Where (username='" + data[0] + "' or emailid='" + data[0] + "') and password='" + strPassward + "' and status=1 and usertype_id='" + intDesignationType + "'";


                            System.Data.DataSet ds = new System.Data.DataSet();

                            try
                            {
                                ds = objODBC.getDataSet(strQuery);
                                string strTPassward = objEncrypt.DecryptQueryString(ds.Tables[0].Rows[0][8].ToString(), passkey);

                                if (ds.Tables[0].Rows.Count > 0)
                                {
                                    arrUserParams.Add("TRUE");

                                    System.Data.DataTable firstTable = ds.Tables[0];

                                    for (int i = 0; i <= firstTable.Rows.Count - 1; i++)
                                    {
                                        for (int j = 0; j <= firstTable.Columns.Count - 1; j++)
                                        {
                                            if (j == 0 || j == 2)
                                                arrUserParams.Add(ED.EncryptAdmin(firstTable.Rows[i][6].ToString(), firstTable.Rows[i][j].ToString(), Convert.ToInt32(firstTable.Rows[i][2].ToString()), 1, Convert.ToInt32(data[9])));
                                            else if (j == (firstTable.Columns.Count - 2))
                                                arrUserParams.Add(ED.EncryptAdmin(firstTable.Rows[i][6].ToString(), strTPassward, Convert.ToInt32(firstTable.Rows[i][2].ToString()), 1, Convert.ToInt32(data[9])));
                                            else if (j == (firstTable.Columns.Count - 1))
                                                arrUserParams.Add(ED.EncryptAdmin(firstTable.Rows[i][6].ToString(), objEncrypt.DecryptQueryString(firstTable.Rows[i][j].ToString(),passkey), 1, 2, 0));
                                            else
                                                arrUserParams.Add(firstTable.Rows[i][j].ToString());


                                        }
                                    }


                                    int intUID = Convert.ToInt32(ds.Tables[0].Rows[0][0].ToString());
                                    if (data[9] == "1")
                                    {
                                        strSalt = objODBC.executeScalar_str("SELECT salt1 from er_login_admin Where userid='" + intUID + "'");
                                        arrUserParams.Add(ED.EncryptAdmin(strUserName, objEncrypt.DecrypSalt(strSalt, passkey), 1, 2, 0));

                                    }
                                    else if (data[9] == "2")
                                    {
                                        strSalt = objODBC.executeScalar_str("SELECT salt2 from er_login_admin Where userid='" + intUID + "'");
                                        arrUserParams.Add(ED.EncryptAdmin(strUserName, objEncrypt.DecrypSalt(strSalt, passkey), 1, 2, 0));
                                    }
                                    else if (data[9] == "3")
                                    {
                                        strSalt = objODBC.executeScalar_str("SELECT salt3 from er_login_admin Where userid='" + intUID + "'");
                                        arrUserParams.Add(ED.EncryptAdmin(strUserName, objEncrypt.DecrypSalt(strSalt, passkey), 1, 2, 0));
                                    }
                                    try
                                    {
                                        dt = objODBCLOG.getDataTable("call activity_logs('" + intUID + "','" + intDesignationType + "',1,'" + data[2] + "','" + data[4] + "','" + data[5] + "','" + data[6] + "','" + data[7] + "','" + data[8] + "','" + data[9] + "','" + strDescription + "','" + intActivityType + "','','')");

                                        if (dt.Rows.Count > 0)
                                        {
                                            arrUserParams.Add(dt.Rows[0][0].ToString());
                                            objODBC.executeNonQuery(" UPDATE er_login_admin SET last_login_ip='" + data[2] + "',last_login_datetime='" + objDT.getCurDateTimeString() + "',er_login_admin='" + dt.Rows[0][0].ToString() + "' Where userid='" + intUID + "'");
                                        }
                                    }
                                    catch (Exception)
                                    {

                                    }
                                    strResponseFile = "Login Successfully !";


                                   
                                        // SMPP SMS Code Below
                                        SMPP_SMS_Format objSMPP_SMS_Format = new SMPP_SMS_Format();
                                        string strGetSMS = objSMPP_SMS_Format.Login(strUserName, ds.Tables[0].Rows[0][1].ToString(), data[9]);
                                       
                                        string strSMPPResponse = "NA";
                                        DataTable dtSMPP = new DataTable();
                                        try
                                        {

                                            dtSMPP = objODBC.getDataTable("Select mobile,operator_id from er_mobile where userid='" + intUID + "' and usertype=1 and mob_type=1");

                                            if (dtSMPP.Rows.Count > 0)
                                            {
                                                string strMobile = dtSMPP.Rows[0][0].ToString();
                                                int intOperaterID = int.Parse(dtSMPP.Rows[0][1].ToString());
                                            try
                                            {
                                               // objODBCLOG.executeNonQuery("INSERT INTO `er_wallet_transfer_sms_log`(`userid`, `username`, `mobile`, `operator_id`, `SMS`, `created_on`) VALUES ('" + intUID + "','','" + strMobile + "','" + intOperaterID + "','" + strGetSMS + "','" + objDateFor.getCurDateTimeString() + "')");
                                            }
                                            catch (Exception)
                                            {

                                            }

                                            objComFUN.er_SMS_Alert(intUID, 1, strMobile, intOperaterID, strGetSMS, 1, strSMPPResponse);

                                            }
                                        }
                                        catch (Exception)
                                        {

                                        }
                                        finally
                                        {
                                            dtSMPP.Dispose();
                                        }
                                    

                                }
                                else
                                {
                                    arrUserParams.Add("FALSE");
                                    arrUserParams.Add("Kindly Enter Valid User ID /Password ");
                                }

                                return arrUserParams;
                            }
                            catch (Exception ex)
                            {
                                arrUserParams.Add("FALSE");
                                arrUserParams.Add("Kindly Enter Valid User ID /Password ");
                                return arrUserParams;
                            }

                            finally
                            {
                                ds.Dispose();
                                dt2.Dispose();
                            }
                        }
                    }
                    else
                    {
                        arrUserParams.Add("FALSE");
                        // arrUserParams.Add("Your account has been de-activated, for more information please call (+93-748436436) !");
                        arrUserParams.Add("Kindly Enter Valid User ID /Password ");
                    }
                    return arrUserParams;
                }
                else
                {
                    arrUserParams.Add("FALSE");
                    arrUserParams.Add("Kindly Enter Valid User ID /Password ");
                    return arrUserParams;
                }
            }
            catch (Exception ex)
            {
                arrUserParams.Add("FALSE");
                arrUserParams.Add("Kindly Enter Valid User ID /Password ");
                return arrUserParams;
            }
        }

        // ***** Member Login ******

        //string strUserName - 0,string strPwd - 1,string strIPAddress - 2,int intDesignationType -3,string strMacAddress - 4,string strOSDetails - 5,string strIMEINo - 6,string strGcmID - 7,string strAPPVersion - 8,int intSource -9         
        [WebMethod()]
        public ArrayList CheckMemberLogin(string strUserName, string strData, int intType)
        {
            ArrayList arrUserParams = new ArrayList();
            string strDescription = "Member Login";
            string strResponseFile = "", strSalt = "";
            int intActivityType = 2;
            try
            {
                try
                {
                    objCreateFile.CreateTextFile("", "", "", "", "", "", "", intType, 37, "Member Login,Data:" + strData, strUserName);
                }
                catch (Exception)
                {

                }

                var data = ED.Decrypt(strUserName, strData, intType, 2, 0).Split(',');

                if ((data[0] != "" || data[0] != null) && (data[1] != "" || data[1] != null) && (data[0] == strUserName))
                {
                    Encryption objEncrypt = new Encryption();



                    string strQueryValidation = "SELECT Count(1) From er_login l inner join er_mobile m on l.userid=m.userid Where (l.username='" + data[0] + "'  or m.mobile='" + data[0] + "') and l.user_status=1 and l.Active=1";
                    int intCount = objODBC.executeScalar_int(strQueryValidation);

                    if (intCount > 0)
                    {



                        dt2 = objODBC.getDataTable("SELECT l.pass_key,l.usertype_id,l.username,l.user_status From er_login l inner join er_mobile m on l.userid=m.userid where (l.username='" + data[0] + "'  or m.mobile='" + data[0] + "') and l.user_status=1 and l.usertype_id!=5  and l.Active=1 limit 1");
                        if (dt2.Rows.Count > 0)
                        {

                            string passkey = dt2.Rows[0][0].ToString();
                            string intDesignationType = dt2.Rows[0][1].ToString();
                            string strPassward = objEncrypt.EncryptQueryString(data[1], passkey);
                            strUserName = dt2.Rows[0][2].ToString();
                            int intActiveStatus = int.Parse(dt2.Rows[0][3].ToString());
                            if (intActiveStatus == 1) // 1=Active and 2=:Inactive
                            {
                                string strQuery = "SELECT l.userid,l.full_name,l.usertype_id,l.last_login_ip,l.last_login_datetime,l.emailid,l.username,l.encryption_key From er_login l inner join er_mobile m on l.userid=m.userid  Where (l.username='" + data[0] + "'  or m.mobile='" + data[0] + "') and l.password='" + strPassward + "' and l.user_status=1 and l.Active=1 and l.usertype_id='" + intDesignationType + "' limit 1";

                                System.Data.DataSet ds = new System.Data.DataSet();
                                int LogUID = objODBC.executeScalar_int("SELECT l.userid From er_login l inner join er_mobile m on l.userid=m.userid Where (l.username='" + data[0] + "'  or m.mobile='" + data[0] + "') and l.user_status=1 and l.Active=1");
                                int logAttemptCount = objODBC.executeScalar_int("SELECT `count_attempt` from er_login_counter Where userid='" + LogUID + "'");
                                if (logAttemptCount <= 3)
                                {
                                    try
                                    {
                                        ds = objODBC.getDataSet(strQuery);
                                        
                                        if (ds.Tables[0].Rows.Count > 0)
                                        {

                                            arrUserParams.Add("TRUE");

                                            System.Data.DataTable firstTable = ds.Tables[0];

                                            for (int i = 0; i <= firstTable.Rows.Count - 1; i++)
                                            {
                                                for (int j = 0; j <= firstTable.Columns.Count - 1; j++)
                                                {
                                                    if (j == 0 || j == 2)
                                                        arrUserParams.Add(ED.Encrypt(strUserName, firstTable.Rows[i][j].ToString(), Convert.ToInt32(firstTable.Rows[i][2].ToString()), 1, Convert.ToInt32(data[9])));
                                                    else if (j == (firstTable.Columns.Count - 1))
                                                        arrUserParams.Add(ED.Encrypt(strUserName, objEncrypt.DecryptQueryString(firstTable.Rows[i][j].ToString(), passkey), 1, 2, 0));
                                                    else
                                                        arrUserParams.Add(firstTable.Rows[i][j].ToString());
                                                }
                                            }
                                            int intUID = Convert.ToInt32(ds.Tables[0].Rows[0][0].ToString());

                                            objODBC.executeNonQuery("UPDATE `er_login_counter` SET `count_attempt`=0 Where userid='" + intUID + "'");

                                            objODBC.executeNonQuery("update er_login set fource_logout=0 where userid =" + intUID + "");
                                            if (data[9] == "1")
                                            {
                                                strSalt = objODBC.executeScalar_str("SELECT salt1 from er_login Where userid='" + intUID + "'");
                                                arrUserParams.Add(ED.Encrypt(strUserName, objEncrypt.DecrypSalt(strSalt, passkey), 2, 2, 0));

                                            }
                                            else if (data[9] == "2")
                                            {
                                                strSalt = objODBC.executeScalar_str("SELECT salt2 from er_login Where userid='" + intUID + "'");
                                                arrUserParams.Add(ED.Encrypt(strUserName, objEncrypt.DecrypSalt(strSalt, passkey), 2, 2, 0));

                                            }
                                            else if (data[9] == "3")
                                            {
                                                strSalt = objODBC.executeScalar_str("SELECT salt3 from er_login Where userid='" + intUID + "'");
                                                arrUserParams.Add(ED.Encrypt(strUserName, objEncrypt.DecrypSalt(strSalt, passkey), 2, 2, 0));
                                            }
                                            try
                                            {
                                                dt = objODBCLOG.getDataTable("call activity_logs('" + intUID + "','" + intDesignationType + "',2,'" + data[2] + "','" + data[4] + "','" + data[5] + "','" + data[6] + "','" + data[7] + "','" + data[8] + "','" + data[9] + "','" + strDescription + "','" + intActivityType + "','','')");
                                                if (dt.Rows.Count > 0)
                                                {
                                                    arrUserParams.Add(dt.Rows[0][0].ToString());
                                                    objODBC.executeNonQuery(" UPDATE er_login SET last_login_ip='" + data[2] + "',last_login_datetime='" + objDT.getCurDateTimeString() + "',last_log_id='" + dt.Rows[0][0].ToString() + "' Where userid='" + intUID + "'");
                                                }
                                            }
                                            catch (Exception)
                                            {

                                            }

                                            // SMPP SMS Code Below
                                            SMPP_SMS_Format objSMPP_SMS_Format = new SMPP_SMS_Format();
                                            string strGetSMS = objSMPP_SMS_Format.Login(strUserName, ds.Tables[0].Rows[0][1].ToString(), data[9]);
                                            SMPP1 objSMPP1 = new SMPP1();
                                            string strSMPPResponse = "NA";
                                            DataTable dtSMPP = new DataTable();
                                            try
                                            {

                                                dtSMPP = objODBC.getDataTable("Select mobile,operator_id from er_mobile where userid='" + intUID + "' and usertype=2 and mob_type=1");

                                                if (dtSMPP.Rows.Count > 0)
                                                {
                                                    string strMobile = dtSMPP.Rows[0][0].ToString();
                                                    int intOperaterID = int.Parse(dtSMPP.Rows[0][1].ToString());
                                                    try
                                                    {
                                                       // objODBCLOG.executeNonQuery("INSERT INTO `er_wallet_transfer_sms_log`(`userid`, `username`, `mobile`, `operator_id`, `SMS`, `created_on`) VALUES ('" + intUID + "','','" + strMobile + "','" + intOperaterID + "','" + strGetSMS + "','" + objDateFor.getCurDateTimeString() + "')");
                                                    }
                                                    catch (Exception)
                                                    {

                                                    }
                                                    objComFUN.er_SMS_Alert(intUID, 2, strMobile, intOperaterID, strGetSMS, 1, strSMPPResponse);

                                                }
                                            }
                                            catch (Exception)
                                            {

                                            }
                                            finally
                                            {
                                                dtSMPP.Dispose();
                                            }

                                        }
                                        else
                                        {
                                            try
                                            {

                                                objODBC.executeNonQuery("UPDATE `er_login_counter` SET `count_attempt`=count_attempt+1 Where userid='" + LogUID + "'");

                                                if (logAttemptCount == 2)
                                                {
                                                    dt = objODBCLOG.getDataTable("call activity_logs('" + LogUID + "','" + intDesignationType + "',2,'" + data[2] + "','" + data[4] + "','" + data[5] + "','" + data[6] + "','" + data[7] + "','" + data[8] + "','" + data[9] + "','Login Attempt Failed User Deactive','" + intActivityType + "','','')");
                                                    objODBC.executeNonQuery(" UPDATE er_login SET user_status=2 Where userid='" + LogUID + "'");
                                                    objODBC.executeNonQuery("UPDATE `er_login_counter` SET `count_attempt`=0 Where userid='" + LogUID + "'");
                                                    arrUserParams.Add("FALSE");
                                                    arrUserParams.Add("Your account has been Locked due to 3 wrong attempt");
                                                    try
                                                    {
                                                        int intLogStatusCount = objODBC.executeScalar_int("SELECT count(1) FROM `er_login_inactive` WHERE status=0 and userid=" + LogUID);
                                                        if (intLogStatusCount == 0)
                                                        {
                                                            objODBC.executeNonQuery("INSERT INTO `er_login_inactive`(`userid`, `username`,`created_on`,`source`) VALUES ('" + LogUID + "','" + data[0] + "','" + objDT.getCurDateTimeString() + "','" + int.Parse(data[9]) + "')");

                                                        }
                                                    }
                                                    catch (Exception)
                                                    {


                                                    }


                                                }
                                                else
                                                {

                                                    arrUserParams.Add("FALSE");
                                                    arrUserParams.Add("You Entered an incorrect User ID /Password you have " + (3 - (logAttemptCount + 1)).ToString() + " attempt left");
                                                }

                                                
                                            }
                                            catch (Exception)
                                            {

                                            }
                                        }

                                        return arrUserParams;
                                    }
                                    catch (Exception ex)
                                    {
                                        arrUserParams.Add("FALSE");
                                        arrUserParams.Add("Kindly Enter Valid User ID /Password ");

                                        return arrUserParams;
                                    }

                                    finally
                                    {
                                        ds.Dispose();
                                        dt.Dispose();
                                    }
                                }
                                else
                                {                                    

                                    if (logAttemptCount>3)
                                    {
                                        arrUserParams.Add("FALSE");
                                        arrUserParams.Add("User Deactive ! Entered an Incorrect User ID / Password , 0 attempt left, Kindly contact Admin ");
                                        return arrUserParams;
                                    }
                                    else
                                    {
                                        objODBC.executeNonQuery("UPDATE `er_login_counter` SET `count_attempt`=count_attempt+1 Where userid='" + LogUID + "'");

                                        if (logAttemptCount == 2)
                                        {
                                            dt = objODBCLOG.getDataTable("call activity_logs('" + LogUID + "','" + intDesignationType + "',2,'" + data[2] + "','" + data[4] + "','" + data[5] + "','" + data[6] + "','" + data[7] + "','" + data[8] + "','" + data[9] + "','Login Attempt Failed User Deactive','" + intActivityType + "','','')");
                                            objODBC.executeNonQuery(" UPDATE er_login SET user_status=2 Where userid='" + LogUID + "'");
                                            arrUserParams.Add("FALSE");
                                            arrUserParams.Add("Your account has been Locked due to 3 wrong attempt");
                                        }
                                        else
                                        {

                                            arrUserParams.Add("FALSE");
                                            arrUserParams.Add("You Entered an incorrect User ID /Password you have " + (3 - (logAttemptCount + 1)).ToString() + " attempt left");
                                        }
                                        return arrUserParams;
                                    }                                   
                                }
                            }
                            else
                            {
                                arrUserParams.Add("FALSE");
                                arrUserParams.Add("Your account has been de-activated, for more information please call (+93-748436436) !");
                                return arrUserParams;
                            }
                        }
                        else
                        {
                            arrUserParams.Add("FALSE");
                            arrUserParams.Add("Kindly Enter Valid User ID /Password ");
                            return arrUserParams;
                        }
                    }
                    else
                    {
                        arrUserParams.Add("FALSE");
                        arrUserParams.Add("Your account has been de-activated, for more information please call (+93-748436436) !");
                        return arrUserParams;
                    }
                }
                else
                {
                    arrUserParams.Add("FALSE");
                    arrUserParams.Add("Kindly Enter Valid User ID /Password ");
                    return arrUserParams;
                }

            }
            catch (Exception ex)
            {
                arrUserParams.Add("FALSE");
                arrUserParams.Add("Kindly Enter Valid User ID /Password ");
                return arrUserParams;
            }
        }

        // ***** Retailer Login ******

        //string strUserName - 0,string strPwd - 1,string strIPAddress - 2,int intDesignationType -3,string strMacAddress - 4,string strOSDetails - 5,string strIMEINo - 6,string strGcmID - 7,string strAPPVersion - 8,int intSource -9         
        [WebMethod()]
        public ArrayList RetailerLogin(string strUserName, string strData, int intType)
        {
            ArrayList arrUserParams = new ArrayList();
            string strDescription = "Retailer Login";
            string strResponseFile = "", strSalt = "";
            int intActivityType = 2;
            try
            {
                try
                {
                    objCreateFile.CreateTextFile("", "", "", "", "", "", "", intType, 37, "Member Login,Data:" + strData, strUserName);
                }
                catch (Exception)
                {

                }

                var data = ED.Decrypt(strUserName, strData, intType, 2, 0).Split(',');

                if ((data[0] != "" || data[0] != null) && (data[1] != "" || data[1] != null) && (data[0] == strUserName))
                {
                    Encryption objEncrypt = new Encryption();



                    string strQueryValidation = "SELECT Count(1) From er_login l inner join er_mobile m on l.userid=m.userid Where (l.username='" + data[0] + "'  or m.mobile='" + data[0] + "') and l.user_status=1 and l.Active=1";
                    int intCount = objODBC.executeScalar_int(strQueryValidation);

                    if (intCount > 0)
                    {



                        dt2 = objODBC.getDataTable("SELECT l.pass_key,l.usertype_id,l.username,l.user_status From er_login l inner join er_mobile m on l.userid=m.userid where (l.username='" + data[0] + "'  or m.mobile='" + data[0] + "') and l.user_status=1  and l.Active=1 and usertype_id=5 limit 1");
                        if (dt2.Rows.Count > 0)
                        {

                            string passkey = dt2.Rows[0][0].ToString();
                            string intDesignationType = dt2.Rows[0][1].ToString();
                            string strPassward = objEncrypt.EncryptQueryString(data[1], passkey);
                            strUserName = dt2.Rows[0][2].ToString();
                            int intActiveStatus = int.Parse(dt2.Rows[0][3].ToString());
                            if (intActiveStatus == 1) // 1=Active and 2=:Inactive
                            {
                                string strQuery = "SELECT l.userid,l.full_name,l.usertype_id,l.last_login_ip,l.last_login_datetime,l.emailid,l.username,l.encryption_key From er_login l inner join er_mobile m on l.userid=m.userid  Where (l.username='" + data[0] + "'  or m.mobile='" + data[0] + "') and l.password='" + strPassward + "' and l.user_status=1 and l.Active=1 and l.usertype_id='" + intDesignationType + "' limit 1";

                                System.Data.DataSet ds = new System.Data.DataSet();
                                int LogUID = objODBC.executeScalar_int("SELECT l.userid From er_login l inner join er_mobile m on l.userid=m.userid Where (l.username='" + data[0] + "'  or m.mobile='" + data[0] + "') and l.user_status=1 and l.Active=1");
                                int logAttemptCount = objODBC.executeScalar_int("SELECT `count_attempt` from er_login_counter Where userid='" + LogUID + "'");
                                if (logAttemptCount <= 3)
                                {
                                    try
                                    {
                                        ds = objODBC.getDataSet(strQuery);

                                        if (ds.Tables[0].Rows.Count > 0)
                                        {

                                            arrUserParams.Add("TRUE");

                                            System.Data.DataTable firstTable = ds.Tables[0];

                                            for (int i = 0; i <= firstTable.Rows.Count - 1; i++)
                                            {
                                                for (int j = 0; j <= firstTable.Columns.Count - 1; j++)
                                                {
                                                    if (j == 0 || j == 2)
                                                        arrUserParams.Add(ED.Encrypt(strUserName, firstTable.Rows[i][j].ToString(), Convert.ToInt32(firstTable.Rows[i][2].ToString()), 1, Convert.ToInt32(data[9])));
                                                    else if (j == (firstTable.Columns.Count - 1))
                                                        arrUserParams.Add(ED.Encrypt(strUserName, objEncrypt.DecryptQueryString(firstTable.Rows[i][j].ToString(), passkey), 1, 2, 0));
                                                    else
                                                        arrUserParams.Add(firstTable.Rows[i][j].ToString());
                                                }
                                            }
                                            int intUID = Convert.ToInt32(ds.Tables[0].Rows[0][0].ToString());

                                            objODBC.executeNonQuery("UPDATE `er_login_counter` SET `count_attempt`=0 Where userid='" + intUID + "'");

                                            objODBC.executeNonQuery("update er_login set fource_logout=0 where userid =" + intUID + "");
                                            if (data[9] == "1")
                                            {
                                                strSalt = objODBC.executeScalar_str("SELECT salt1 from er_login Where userid='" + intUID + "'");
                                                arrUserParams.Add(ED.Encrypt(strUserName, objEncrypt.DecrypSalt(strSalt, passkey), 2, 2, 0));

                                            }
                                            else if (data[9] == "2")
                                            {
                                                strSalt = objODBC.executeScalar_str("SELECT salt2 from er_login Where userid='" + intUID + "'");
                                                arrUserParams.Add(ED.Encrypt(strUserName, objEncrypt.DecrypSalt(strSalt, passkey), 2, 2, 0));

                                            }
                                            else if (data[9] == "3")
                                            {
                                                strSalt = objODBC.executeScalar_str("SELECT salt3 from er_login Where userid='" + intUID + "'");
                                                arrUserParams.Add(ED.Encrypt(strUserName, objEncrypt.DecrypSalt(strSalt, passkey), 2, 2, 0));
                                            }
                                            try
                                            {
                                                dt = objODBCLOG.getDataTable("call activity_logs('" + intUID + "','" + intDesignationType + "',2,'" + data[2] + "','" + data[4] + "','" + data[5] + "','" + data[6] + "','" + data[7] + "','" + data[8] + "','" + data[9] + "','" + strDescription + "','" + intActivityType + "','','')");
                                                if (dt.Rows.Count > 0)
                                                {
                                                    arrUserParams.Add(dt.Rows[0][0].ToString());
                                                    objODBC.executeNonQuery(" UPDATE er_login SET last_login_ip='" + data[2] + "',last_login_datetime='" + objDT.getCurDateTimeString() + "',last_log_id='" + dt.Rows[0][0].ToString() + "' Where userid='" + intUID + "'");
                                                }
                                            }
                                            catch (Exception)
                                            {

                                            }

                                            // SMPP SMS Code Below
                                            SMPP_SMS_Format objSMPP_SMS_Format = new SMPP_SMS_Format();
                                            string strGetSMS = objSMPP_SMS_Format.Login(strUserName, ds.Tables[0].Rows[0][1].ToString(), data[9]);
                                            SMPP1 objSMPP1 = new SMPP1();
                                            string strSMPPResponse = "NA";
                                            DataTable dtSMPP = new DataTable();
                                            try
                                            {

                                                dtSMPP = objODBC.getDataTable("Select mobile,operator_id from er_mobile where userid='" + intUID + "' and usertype=2 and mob_type=1");

                                                if (dtSMPP.Rows.Count > 0)
                                                {
                                                    string strMobile = dtSMPP.Rows[0][0].ToString();
                                                    int intOperaterID = int.Parse(dtSMPP.Rows[0][1].ToString());
                                                    try
                                                    {
                                                     //   objODBCLOG.executeNonQuery("INSERT INTO `er_wallet_transfer_sms_log`(`userid`, `username`, `mobile`, `operator_id`, `SMS`, `created_on`) VALUES ('" + intUID + "','','" + strMobile + "','" + intOperaterID + "','" + strGetSMS + "','" + objDateFor.getCurDateTimeString() + "')");
                                                    }
                                                    catch (Exception)
                                                    {

                                                    }
                                                    objComFUN.er_SMS_Alert(intUID, 2, strMobile, intOperaterID, strGetSMS, 1, strSMPPResponse);

                                                }
                                            }
                                            catch (Exception)
                                            {

                                            }
                                            finally
                                            {
                                                dtSMPP.Dispose();
                                            }

                                        }
                                        else
                                        {
                                            try
                                            {

                                                objODBC.executeNonQuery("UPDATE `er_login_counter` SET `count_attempt`=count_attempt+1 Where userid='" + LogUID + "'");

                                                if (logAttemptCount == 2)
                                                {
                                                    dt = objODBCLOG.getDataTable("call activity_logs('" + LogUID + "','" + intDesignationType + "',2,'" + data[2] + "','" + data[4] + "','" + data[5] + "','" + data[6] + "','" + data[7] + "','" + data[8] + "','" + data[9] + "','Login Attempt Failed User Deactive','" + intActivityType + "','','')");
                                                    objODBC.executeNonQuery(" UPDATE er_login SET user_status=2 Where userid='" + LogUID + "'");
                                                    objODBC.executeNonQuery("UPDATE `er_login_counter` SET `count_attempt`=0 Where userid='" + LogUID + "'");
                                                    arrUserParams.Add("FALSE");
                                                    arrUserParams.Add("Your account has been Locked due to 3 wrong attempt");

                                                    try
                                                    {
                                                        int intLogStatusCount = objODBC.executeScalar_int("SELECT count(1) FROM `er_login_inactive` WHERE status=0 and userid=" + LogUID);
                                                        if (intLogStatusCount == 0)
                                                        {
                                                            objODBC.executeNonQuery("INSERT INTO `er_login_inactive`(`userid`, `username`,`created_on`,`source`) VALUES ('" + LogUID + "','" + data[0] + "','" + objDT.getCurDateTimeString() + "','" + int.Parse(data[9]) + "')");

                                                        }
                                                    }
                                                    catch (Exception)
                                                    {


                                                    }
                                                }
                                                else
                                                {

                                                    arrUserParams.Add("FALSE");
                                                    arrUserParams.Add("You Entered an incorrect User ID /Password you have " + (3 - (logAttemptCount + 1)).ToString() + " attempt left");
                                                }


                                            }
                                            catch (Exception)
                                            {

                                            }
                                        }

                                        return arrUserParams;
                                    }
                                    catch (Exception ex)
                                    {
                                        arrUserParams.Add("FALSE");
                                        arrUserParams.Add("Kindly Enter Valid User ID /Password ");

                                        return arrUserParams;
                                    }

                                    finally
                                    {
                                        ds.Dispose();
                                        dt.Dispose();
                                    }
                                }
                                else
                                {

                                    if (logAttemptCount > 3)
                                    {
                                        arrUserParams.Add("FALSE");
                                        arrUserParams.Add("User Deactive ! Entered an Incorrect User ID / Password , 0 attempt left, Kindly contact Admin ");
                                        return arrUserParams;
                                    }
                                    else
                                    {
                                        objODBC.executeNonQuery("UPDATE `er_login_counter` SET `count_attempt`=count_attempt+1 Where userid='" + LogUID + "'");

                                        if (logAttemptCount == 2)
                                        {
                                            dt = objODBCLOG.getDataTable("call activity_logs('" + LogUID + "','" + intDesignationType + "',2,'" + data[2] + "','" + data[4] + "','" + data[5] + "','" + data[6] + "','" + data[7] + "','" + data[8] + "','" + data[9] + "','Login Attempt Failed User Deactive','" + intActivityType + "','','')");
                                            objODBC.executeNonQuery(" UPDATE er_login SET user_status=2 Where userid='" + LogUID + "'");
                                            arrUserParams.Add("FALSE");
                                            arrUserParams.Add("Your account has been Locked due to 3 wrong attempt");
                                        }
                                        else
                                        {

                                            arrUserParams.Add("FALSE");
                                            arrUserParams.Add("You Entered an incorrect User ID /Password you have " + (3 - (logAttemptCount + 1)).ToString() + " attempt left");
                                        }
                                        return arrUserParams;
                                    }
                                }
                            }
                            else
                            {
                                arrUserParams.Add("FALSE");
                                arrUserParams.Add("Your account has been de-activated, for more information please call (+93-748436436) !");
                                return arrUserParams;
                            }
                        }
                        else
                        {
                            arrUserParams.Add("FALSE");
                            arrUserParams.Add("Kindly Enter Valid User ID /Password ");
                            return arrUserParams;
                        }
                    }
                    else
                    {
                        arrUserParams.Add("FALSE");
                        arrUserParams.Add("Your account has been de-activated, for more information please call (+93-748436436) !");
                        return arrUserParams;
                    }
                }
                else
                {
                    arrUserParams.Add("FALSE");
                    arrUserParams.Add("Kindly Enter Valid User ID /Password ");
                    return arrUserParams;
                }

            }
            catch (Exception ex)
            {
                arrUserParams.Add("FALSE");
                arrUserParams.Add("Kindly Enter Valid User ID /Password ");
                return arrUserParams;
            }
        }



        // **** Update Password For Admin*****
        // (string oldPwd - 0,string newPwd - 1,int intUserID - 2,int intLogID - 3,string strIPAddress - 4,int intDesignationType - 5,string strMacAddress - 6,string strOSDetails - 7,string strIMEINo - 8,string strGcmID - 9,string strAPPVersion - 10,int intSource - 11)
        [WebMethod()]
        public string UpdateAdminPassword(string strUserName, string strData, int intType, int intSource)
        {
            string strDescription = "Change Admin Password", strResponseFile = "";
            int intActivityType = 3;
            try
            {
                var data = ED.DecryptAdmin(strUserName, strData, 1, intType, intSource).Split(',');
                try
                {
                    objCreateFile.CreateTextFile(data[4], data[6], data[7], data[8], data[9], data[10], data[11], 1, intActivityType, "SubAdmin,Data:" + strData, strUserName);
                }
                catch (Exception)
                {

                }
                if ((data[0] != "" || data[0] != null) && (data[1] != "" || data[1] != null) && (data[2] != "" || data[2] != null))
                {

                    Encryption objEncrypt = new Encryption();
                    DateFormat objDateFormat = new DateFormat();


                    string passkey = objODBC.executeScalar_str("SELECT pass_key From er_login_admin Where userid='" + data[2] + "' and status=1");
                    string strOLDPassward = objEncrypt.EncryptQueryString(data[0], passkey);

                    int intCount = objODBC.executeScalar_int("SELECT Count(1) From er_login_admin Where  userid=" + data[2] + " and password= '" + strOLDPassward + "' ");

                    if (intCount > 0)
                    {


                        string strPassward = objEncrypt.EncryptQueryString(data[1], passkey);
                        string strQuery = "UPDATE er_login_admin SET password='" + strPassward + "' Where userid=" + data[2] + "";
                        objODBC.executeNonQuery(strQuery);
                        objODBC.executeNonQuery("Insert Into er_password_history (userid,old_password,new_password,modified_by,modified_on,user_type,created_by_type,log_id) values (" + data[2] + ",'" + strOLDPassward + "','" + strPassward + "'," + data[2] + ",'" + objDateFormat.getCurDateTimeString() + "','" + data[5] + "',1,'" + data[3] + "')");
                       
                        strResponseFile = "Password successfully changed.";
                        //  string oldPwd - 0,string newPwd -1,int intUserID -2,int intLogID -3,string strIPAddress -4,int intDesignationType -5,string strMacAddress -6,string strOSDetails -7,string strIMEINo -8,string strGcmID -9,string strAPPVersion -10,int intSource -11)


                        // SMPP SMS Code Below
                        SMPP_SMS_Format objSMPP_SMS_Format = new SMPP_SMS_Format();
                        string strGetSMS = objSMPP_SMS_Format.ChangePassword(strUserName, objODBC.executeScalar_str("SELECT full_name From er_login_admin Where userid='" + data[2] + "'"), intSource, data[1].ToString());
                        SMPP1 objSMPP1 = new SMPP1();
                        string strSMPPResponse = "NA";
                        DataTable dtSMPP = new DataTable();
                        try
                        {
                            objComFUN.er_insert_notification(int.Parse(data[2]),"Change Password", strGetSMS,1);


                            dtSMPP = objODBC.getDataTable("Select mobile,operator_id from er_mobile where userid='" + data[2] + "' and usertype=1 and mob_type=1");

                            if (dtSMPP.Rows.Count > 0)
                            {
                                string strMobile = dtSMPP.Rows[0][0].ToString();
                                int intOperaterID = int.Parse(dtSMPP.Rows[0][1].ToString());
                               
                                   
                                objComFUN.er_SMS_Alert(int.Parse(data[2]), 1, strMobile, intOperaterID, strGetSMS, 1, strSMPPResponse);
                                   
                            }
                        }
                        catch (Exception)
                        {

                        }
                        finally
                        {
                            dtSMPP.Dispose();
                        }


                        try
                        {
                            objODBCLOG.executeNonQuery("call activity_logs('" + data[2] + "','" + data[5] + "',1,'" + data[4] + "','" + data[6] + "','" + data[7] + "','" + data[8] + "','" + data[9] + "','" + data[10] + "','" + data[11] + "','" + strDescription + "','" + intActivityType + "','" + strOLDPassward + "','" + strPassward + "')");

                            objCreateFile.CreateTextFile(data[4], data[6], data[7], data[8], data[9], data[10], data[11], 1, intActivityType, strResponseFile, strUserName);
                        }
                        catch (Exception)
                        {

                        }
                        return strResponseFile;



                    }
                    else
                    {
                        strResponseFile = "Old Password in not matching.";
                        objCreateFile.CreateTextFile(data[4], data[6], data[7], data[8], data[9], data[10], data[11], 1, intActivityType, strResponseFile, strUserName);
                        return strResponseFile;
                    }

                }
                else
                {
                    strResponseFile = "Kindly Enter valid Userid and Password.";
                    objCreateFile.CreateTextFile(data[4], data[6], data[7], data[8], data[9], data[10], data[11], 1, intActivityType, strResponseFile, strUserName);
                    return strResponseFile;
                }
            }
            catch (Exception ex)
            {
                return "OOPS! Something is wrong.";
            }
        }



        // **** Update Password For Member*****
        // (string oldPwd - 0,string newPwd - 1,int intUserID - 2,int intLogID - 3,string strIPAddress - 4,int intDesignationType - 5,string strMacAddress - 6,string strOSDetails - 7,string strIMEINo - 8,string strGcmID - 9,string strAPPVersion - 10,int intSource - 11)
        [WebMethod()]
        public ArrayList UpdateMPinPassword(string strUserName, string strData, int intEType, int intSource, int intUType)
        {
            strData = strData.Trim();

            ArrayList arrUserParams = new ArrayList();
            string strDescription = "Change M-Pin Password", strResponseFile = "";
            int intActivityType = 4;
            try
            {
                var data = ED.Decrypt(strUserName, strData, intUType, intEType, intSource).Split(',');
                if ((data[0] != "" || data[0] != null) && (data[1] != "" || data[1] != null) && (data[2] != "" || data[2] != null))
                {
                    if (data[1].Length == 4)
                    {
                        Encryption objEncrypt = new Encryption();
                        DateFormat objDateFormat = new DateFormat();

                        int intInActiveMemberCount = objODBC.executeScalar_int("SELECT Count(1) From er_login Where  userid=" + data[2] + " ");
                        if (intInActiveMemberCount > 0)
                        {
                            string passkey = objODBC.executeScalar_str("SELECT pass_key From er_login Where userid='" + data[2] + "'");

                            string strOLDPassward = objEncrypt.EncryptQueryString(data[0], passkey);
                            int intCount = objODBC.executeScalar_int("SELECT Count(1) From er_login Where  userid='" + data[2] + "' and M_Pin= '" + strOLDPassward + "'");
                            if (intCount > 0)
                            {


                                string strPassward = objEncrypt.EncryptQueryString(data[1], passkey);
                                string strQuery = "UPDATE er_login SET M_Pin='" + strPassward + "' Where userid='" + data[2] + "'";
                                objODBC.executeNonQuery(strQuery);


                                strResponseFile = "M-Pin successfully changed.";

                                //  string oldPwd - 0,string newPwd -1,int intUserID -2,int intLogID -3,string strIPAddress -4,int intDesignationType -5,string strMacAddress -6,string strOSDetails -7,string strIMEINo -8,string strGcmID -9,string strAPPVersion -10,int intSource -11)
                                try
                                {
                                    objODBCLOG.executeNonQuery("call activity_logs('" + data[2] + "','" + data[5] + "','" + intEType + "','" + data[4] + "','" + data[6] + "','" + data[7] + "','" + data[8] + "','" + data[9] + "','" + data[10] + "','" + data[11] + "','" + strDescription + "','" + intActivityType + "','" + strOLDPassward + "','" + strPassward + "')");
                                    objCreateFile.CreateTextFile(data[4], data[6], data[7], data[8], data[9], data[10], data[11], 1, intActivityType, strResponseFile + ",Data:" + strData, strUserName);
                                }
                                catch (Exception)
                                {

                                }
                                arrUserParams.Add("TRUE");
                                arrUserParams.Add(strResponseFile);




                                // SMPP SMS Code Below
                                SMPP_SMS_Format objSMPP_SMS_Format = new SMPP_SMS_Format();
                                string strGetSMS = objSMPP_SMS_Format.GetForgotMPIN(objODBC.executeScalar_str("SELECT username From er_login Where userid='" + data[2] + "'").ToString(), objODBC.executeScalar_str("SELECT full_name From er_login Where userid='" + data[2] + "'").ToString(), "", data[1]).ToString();
                                objComFUN.er_insert_notification(int.Parse(data[2].ToString()), "Reset M-Pin", strGetSMS, 2);
                                SMPP1 objSMPP1 = new SMPP1();
                                string strSMPPResponse = "NA";
                                DataTable dtSMPP = new DataTable();
                                try
                                {


                                    dtSMPP = objODBC.getDataTable("Select mobile,operator_id from er_mobile where userid='" + data[2] + "' and usertype=2 and mob_type=1");

                                    if (dtSMPP.Rows.Count > 0)
                                    {
                                        string strMobile = dtSMPP.Rows[0][0].ToString();
                                        int intOperaterID = int.Parse(dtSMPP.Rows[0][1].ToString());


                                        objComFUN.er_SMS_Alert(int.Parse(data[2]), 2, strMobile, intOperaterID, strGetSMS, 1, strSMPPResponse);

                                    }
                                }
                                catch (Exception)
                                {

                                }
                                finally
                                {
                                    dtSMPP.Dispose();
                                }
                            }
                            else
                            {
                                strResponseFile = "Old M-Pin in not matching.";

                                objCreateFile.CreateTextFile(data[4], data[6], data[7], data[8], data[9], data[10], data[11], 1, intActivityType, strResponseFile, strUserName);
                                arrUserParams.Add("FALSE");
                                arrUserParams.Add(strResponseFile);
                            }
                        }
                        else
                        {
                            strResponseFile = "Account is deactive all the account releted activity is suspended for this account !";
                            objCreateFile.CreateTextFile(data[4], data[6], data[7], data[8], data[9], data[10], data[11], 1, intActivityType, strResponseFile, strUserName);
                            arrUserParams.Add("FALSE");
                            arrUserParams.Add(strResponseFile);
                        }
                    }
                    else
                    {
                        strResponseFile = "Kindly Enter Valid M-Pin";
                        objCreateFile.CreateTextFile(data[4], data[6], data[7], data[8], data[9], data[10], data[11], 1, intActivityType, strResponseFile, strUserName);
                        arrUserParams.Add("FALSE");
                        arrUserParams.Add(strResponseFile);
                    }
                }
                else
                {
                    strResponseFile = "Kindly Enter valid Userid and Password.";
                    objCreateFile.CreateTextFile(data[4], data[6], data[7], data[8], data[9], data[10], data[11], 1, intActivityType, strResponseFile, strUserName);
                    arrUserParams.Add("FALSE");
                    arrUserParams.Add(strResponseFile);
                }

            }
            catch (Exception ex)
            {
                arrUserParams.Add("FALSE");
                arrUserParams.Add("Please referesh the page and try again");

            }
            return arrUserParams;
        }

        
        // **** Update Password For Member*****
        // (string oldPwd - 0,string newPwd - 1,int intUserID - 2,int intLogID - 3,string strIPAddress - 4,int intDesignationType - 5,string strMacAddress - 6,string strOSDetails - 7,string strIMEINo - 8,string strGcmID - 9,string strAPPVersion - 10,int intSource - 11)
        [WebMethod()]
        public ArrayList UpdateMemberPassword(string strUserName, string strData, int intEType, int intSource, int intUType)
        {
            strData = strData.Trim();

            ArrayList arrUserParams = new ArrayList();
            string strDescription = "Change Member Password", strResponseFile = "";
            int intActivityType = 4;
            try
            {
                var data = ED.Decrypt(strUserName, strData, intUType, intEType, intSource).Split(',');
                if ((data[0] != "" || data[0] != null) && (data[1] != "" || data[1] != null) && (data[2] != "" || data[2] != null))
                {
                    Encryption objEncrypt = new Encryption();
                    DateFormat objDateFormat = new DateFormat();

                    int intInActiveMemberCount = objODBC.executeScalar_int("SELECT Count(1) From er_login Where  userid=" + data[2] + " and user_status=1  and Active=1");
                    if (intInActiveMemberCount > 0)
                    {
                        string passkey = objODBC.executeScalar_str("SELECT pass_key From er_login Where userid='" + data[2] + "' and user_status=1  and Active=1");

                        string strOLDPassward = objEncrypt.EncryptQueryString(data[0], passkey);
                        int intCount = objODBC.executeScalar_int("SELECT Count(1) From er_login Where  userid=" + data[2] + " and password= '" + strOLDPassward + "'");

                        if (intCount > 0)
                        {


                            string strPassward = objEncrypt.EncryptQueryString(data[1], passkey);
                            string strQuery = "UPDATE er_login SET password='" + strPassward + "' Where userid=" + data[2] + "";
                            objODBC.executeNonQuery(strQuery);

                            objODBC.executeNonQuery("Insert Into er_password_history (userid,old_password,new_password,modified_by,modified_on,user_type,created_by_type,log_id) values (" + data[2] + ",'" + strOLDPassward + "','" + strPassward + "'," + data[2] + ",'" + objDateFormat.getCurDateTimeString() + "','" + data[5] + "','" + intEType + "','" + data[3] + "')");

                            objODBC.executeNonQuery("update er_login set fource_logout=1 where userid =" + data[2] + "");


                            strResponseFile = "Password successfully changed.";

                            //  string oldPwd - 0,string newPwd -1,int intUserID -2,int intLogID -3,string strIPAddress -4,int intDesignationType -5,string strMacAddress -6,string strOSDetails -7,string strIMEINo -8,string strGcmID -9,string strAPPVersion -10,int intSource -11)
                            try
                            {
                                objODBCLOG.executeNonQuery("call activity_logs('" + data[2] + "','" + data[5] + "','" + intEType + "','" + data[4] + "','" + data[6] + "','" + data[7] + "','" + data[8] + "','" + data[9] + "','" + data[10] + "','" + data[11] + "','" + strDescription + "','" + intActivityType + "','" + strOLDPassward + "','" + strPassward + "')");
                                objCreateFile.CreateTextFile(data[4], data[6], data[7], data[8], data[9], data[10], data[11], 1, intActivityType, strResponseFile + ",Data:" + strData, strUserName);
                            }
                            catch (Exception)
                            {

                            }
                            arrUserParams.Add("TRUE");
                            arrUserParams.Add(strResponseFile);




                            // SMPP SMS Code Below
                            SMPP_SMS_Format objSMPP_SMS_Format = new SMPP_SMS_Format();
                            string strGetSMS = objSMPP_SMS_Format.ChangePassword(objODBC.executeScalar_str("SELECT username From er_login Where userid='" + data[2] + "'").ToString(), objODBC.executeScalar_str("SELECT full_name From er_login Where userid='" + data[2] + "'").ToString(), intSource, data[1].ToString()).ToString();
                             objComFUN.er_insert_notification(int.Parse(data[2].ToString()), "Change Password", strGetSMS,2);
                            SMPP1 objSMPP1 = new SMPP1();
                            string strSMPPResponse = "NA";
                            DataTable dtSMPP = new DataTable();
                            try
                            {


                                dtSMPP = objODBC.getDataTable("Select mobile,operator_id from er_mobile where userid='" + data[2] + "' and usertype=2 and mob_type=1");

                                if (dtSMPP.Rows.Count > 0)
                                {
                                    string strMobile = dtSMPP.Rows[0][0].ToString();
                                    int intOperaterID = int.Parse(dtSMPP.Rows[0][1].ToString());
                                   
                                      
                                    objComFUN.er_SMS_Alert(int.Parse(data[2]), 2, strMobile, intOperaterID, strGetSMS, 1, strSMPPResponse);
                                          
                                }
                            }
                            catch (Exception)
                            {

                            }
                            finally
                            {
                                dtSMPP.Dispose();
                            }
                        }
                        else
                        {
                            strResponseFile = "Old Password in not matching.";

                            objCreateFile.CreateTextFile(data[4], data[6], data[7], data[8], data[9], data[10], data[11], 1, intActivityType, strResponseFile, strUserName);
                            arrUserParams.Add("FALSE");
                            arrUserParams.Add(strResponseFile);
                        }
                    }
                    else
                    {
                        strResponseFile = "Account is deactive all the account releted activity is suspended for this account !";
                        objCreateFile.CreateTextFile(data[4], data[6], data[7], data[8], data[9], data[10], data[11], 1, intActivityType, strResponseFile, strUserName);
                        arrUserParams.Add("FALSE");
                        arrUserParams.Add(strResponseFile);
                    }
                }
                else
                {
                    strResponseFile = "Kindly Enter valid Userid and Password.";
                    objCreateFile.CreateTextFile(data[4], data[6], data[7], data[8], data[9], data[10], data[11], 1, intActivityType, strResponseFile, strUserName);
                    arrUserParams.Add("FALSE");
                    arrUserParams.Add(strResponseFile);
                }

            }
            catch (Exception ex)
            {
                arrUserParams.Add("FALSE");
                arrUserParams.Add("Please referesh the page and try again");

            }
            return arrUserParams;
        }



        [WebMethod]
        public string er_forgetpass(string strUserName, string strEmailOrMob, string strIPAddress, int intDesignationType, string strMacAddress, string strOSDetails, string strIMEINo, string strGcmID, string strAPPVersion, int intSource, int intCreatedByType)
        {
            string strResult = string.Empty;
            bool chkpoint = false;
            try
            {
                objCreateFile.CreateTextFile(strIPAddress, strMacAddress, strOSDetails, strIMEINo, strGcmID, strAPPVersion, intSource.ToString(), intCreatedByType, 81, "er_forgetpass", strUserName);
            }
            catch (Exception)
            {

            }
            finally
            {
                if (strEmailOrMob != string.Empty && strUserName != string.Empty && strUserName != "" && strEmailOrMob != "")
                {
                    if (strEmailOrMob.Contains("@"))
                    {
                        Regex regex = new Regex(@"^([\w\.\-]+)@([\w\-]+)((\.(\w){2,3})+)$");
                        Match match = regex.Match(strEmailOrMob);
                        if (match.Success)
                        {
                            chkpoint = true;
                            strResult = "Email";
                        }
                        else
                        {
                            chkpoint = false;
                            strResult = "invalid email";
                        }
                    }
                    else
                    {
                        if (strEmailOrMob.Length >= 10 && strEmailOrMob.Contains("+0123456789"))
                        {
                            chkpoint = true;
                            strResult = "Phone";
                        }
                        else
                        {
                            chkpoint = false;
                            strResult = "invalid mobile";
                        }
                    }

                    if (chkpoint == true)
                    {
                        string strQuery = string.Empty;
                        string strEMail = string.Empty;
                        if (strResult == "Email")
                        {
                            strQuery = "select count(1) from er_login where email='" + strEmailOrMob.ToString() + "' and username ='" + strUserName.ToString() + "' and Active=1";

                            int chkValid = objODBC.executeScalar_int(strQuery);
                            if (chkValid > 0)
                            {
                                DataTable dt = new DataTable();
                                strQuery = "select userid,email from er_login where email='" + strEmailOrMob.ToString() + "' and username ='" + strUserName.ToString() + "' and Active=1";
                                dt = objODBC.getDataTable(strQuery);
                                strEMail = dt.Rows[0][1].ToString(); // dt.Rows[0][0] == userid ;
                                int chkEMailReq = objODBC.executeScalar_int("select count(1) from er_password_history where userid =" + Convert.ToInt32(dt.Rows[0][0]) + "");
                                if (chkEMailReq < 5)
                                {
                                    Random rand = new Random();
                                    string strOTP = (rand.Next(0, strEMail.Length - 1) + strEMail.Substring(0, 5)).ToString();
                                    while (chkpoint)
                                    {
                                        strQuery = "select count(1) from er_password_history where otp='" + strOTP.ToString() + "'";
                                        int chkExisit = objODBC.executeScalar_int(strQuery);
                                        if (chkExisit == 0)
                                        {
                                            chkpoint = false;
                                        }
                                        else
                                        {
                                            strOTP = (rand.Next(0, strEMail.Length - 1) + strEMail.Substring(0, 5)).ToString();
                                            chkpoint = true;
                                        }
                                    }

                                    if (chkpoint != true)
                                    {
                                        int chkSuccess = objODBC.executeNonQuery("insert into er_forget_password_request (userid,email,otp,created_on,status) values (" + Convert.ToInt32(dt.Rows[0][1]) + ",'" + strEMail + "','" + strOTP + "','" + objDateFor.getCurDateTimeString() + "',0)");
                                        if (chkSuccess > 0)
                                        {
                                            //integrate Email
                                        }
                                    }

                                }
                                else
                                {
                                    strResult = "you already request so many time kindly do after some time";
                                }

                            }
                            else
                            {
                                strResult = "invalid details or please contact admin";
                            }
                        }
                        else if (strResult == "Phone")
                        {
                            strQuery = "select count(1) from er_login where mobile='" + strEmailOrMob.ToString() + "' and username ='" + strUserName.ToString() + "' and Active=1";
                            int chkValid = objODBC.executeScalar_int(strQuery);
                            if (chkValid > 0)
                            {
                                DataTable dt = new DataTable();
                                strQuery = "select userid,mobile from er_login where username ='" + strUserName.ToString() + "' and mobile='" + strEmailOrMob.ToString() + "' and Active=1";
                                dt = objODBC.getDataTable(strQuery);
                                strEMail = dt.Rows[0][1].ToString(); // dt.Rows[0][0] == userid ;
                                int chkEMailReq = objODBC.executeScalar_int("select count(1) from er_password_history where userid =" + Convert.ToInt32(dt.Rows[0][0]) + "");
                                if (chkEMailReq < 5)
                                {
                                    Random rand = new Random();
                                    string strOTP = (rand.Next(0, strEMail.Length - 1) + strEMail.Substring(0, 5)).ToString();
                                    while (chkpoint)
                                    {
                                        strQuery = "select count(1) from er_password_history where otp='" + strOTP.ToString() + "'";
                                        int chkExisit = objODBC.executeScalar_int(strQuery);
                                        if (chkExisit == 0)
                                        {
                                            chkpoint = false;
                                        }
                                        else
                                        {
                                            strOTP = (rand.Next(0, strEMail.Length - 1) + strEMail.Substring(0, 5)).ToString();
                                            chkpoint = true;
                                        }
                                    }

                                    if (chkpoint != true)
                                    {
                                        int chkSuccess = objODBC.executeNonQuery("insert into er_forget_password_request (userid,email,otp,created_on,status) values (" + Convert.ToInt32(dt.Rows[0][1]) + ",'" + strEMail + "','" + strOTP + "','" + objDateFor.getCurDateTimeString() + "',0)");
                                        if (chkSuccess > 0)
                                        {
                                            //integrate Email
                                        }
                                    }

                                }
                                else
                                {
                                    strResult = "you already request so many time";
                                }
                            }
                        }

                    }
                    else
                    {
                        strResult = "invalid details";
                    }
                }
                else
                {
                    strResult = "please fill all valid details";
                }
            }
            return strResult;
        }

    }


}
