﻿Imports Microsoft.VisualBasic
Imports System.Net.Mail
Imports System.Xml
Imports System.Data
Imports RestSharp
Imports RestSharp.Authenticators
Namespace Etopup_Ws.old_App_Code
    Public Class clsCommunication

        Dim objOdbc As New ODBC
        Dim objCallWS As New CallService

        Dim website As String = System.Configuration.ConfigurationManager.AppSettings("crbturl")
        Dim mail As String = System.Configuration.ConfigurationManager.AppSettings("mail")
        Dim facebook As String = System.Configuration.ConfigurationManager.AppSettings("facebook")
        Dim twitter As String = System.Configuration.ConfigurationManager.AppSettings("twitter")
        Dim telegram As String = System.Configuration.ConfigurationManager.AppSettings("telegram")
        Dim youtube As String = System.Configuration.ConfigurationManager.AppSettings("youtube")

        Public Sub ico_Email_Verifiy_Mail(ByVal intID As Integer, ByVal FirstName As String, ByVal UserName As String, ByVal Email As String, ByVal Password As String, ByVal strLink As String)

            Dim mailSubject As String = "Email Verification for TBTC."
            Dim mailFrom As String = "care@tbtc.com"
            Dim mailFromName As String = "TBTC Admin"

            Dim mailMessage As String = SendEmailVerification(intID, FirstName, UserName, Email, Password, strLink)

            SendTestMail1(Email, mailFrom, mailSubject, mailMessage, intID)

        End Sub
        Public Function SendEmailVerification(ByVal intID As Integer, ByVal fname As String, ByVal txtUName As String, ByVal txtEmail As String, ByVal txtpassword As String, ByVal strLinkk As String) As String

            Dim strMailString As String = System.IO.File.ReadAllText(System.Web.HttpContext.Current.Server.MapPath("EmailerVerification1.html"))
            strMailString = strMailString.Replace("&&username&&", fname)
            strMailString = strMailString.Replace(" &&userid&&", txtUName)
            strMailString = strMailString.Replace("&&passeord&&", txtpassword)
            strMailString = strMailString.Replace("&&link&&", strLinkk)

            strMailString = strMailString.Replace("&&website&&", website)
            strMailString = strMailString.Replace("&&mail&&", mail)
            strMailString = strMailString.Replace("&&facebook&&", facebook)
            strMailString = strMailString.Replace("&&twitter&&", twitter)
            strMailString = strMailString.Replace("&&telegram&&", telegram)
            strMailString = strMailString.Replace("&&youtube&&", youtube)
            Return strMailString

        End Function
        Public Sub ico_erc_withdraw_Verifiy_Mail(ByVal intID As Integer, ByVal UserName As String, ByVal Email As String, ByVal strLink As String, ByVal strOtp As String)

            ' Dim mailSubject As String = "ERC20 withdraw Verification for TBTC."
            Dim mailSubject As String = "TBTC my profile verification OTP."
            Dim mailFrom As String = "care@tbtc.com"
            Dim mailFromName As String = "TBTC Admin"

            Dim mailMessage As String = SendwithdrawlVerification(intID, UserName, Email, strLink, strOtp)

            SendTestMail1(Email, mailFrom, mailSubject, mailMessage, intID)

        End Sub
        Public Function SendwithdrawlVerification(ByVal intID As Integer, ByVal txtUName As String, ByVal txtEmail As String, ByVal strLinkk As String, ByVal strottp As String) As String

            Dim strMailString As String = System.IO.File.ReadAllText(System.Web.HttpContext.Current.Server.MapPath("erc20withdraw.html"))
            strMailString = strMailString.Replace("&&username&&", txtUName)
            strMailString = strMailString.Replace("&&otp&&", strottp)
            strMailString = strMailString.Replace("&&link&&", strLinkk)
            strMailString = strMailString.Replace("&&website&&", website)
            strMailString = strMailString.Replace("&&mail&&", mail)
            strMailString = strMailString.Replace("&&facebook&&", facebook)
            strMailString = strMailString.Replace("&&twitter&&", twitter)
            strMailString = strMailString.Replace("&&telegram&&", telegram)
            strMailString = strMailString.Replace("&&youtube&&", youtube)
            Return strMailString

        End Function
        Public Sub ico_ercAddress_Verifiy_Mail(ByVal intID As Integer, ByVal UserName As String, ByVal Email As String, ByVal strLink As String)

            Dim mailSubject As String = "ERC20 Address Verification for TBTC."
            Dim mailFrom As String = "care@tbtc.com"
            Dim mailFromName As String = "TBTC Admin"

            Dim mailMessage As String = SendAddresslVerification(intID, UserName, Email, strLink)

            SendTestMail1(Email, mailFrom, mailSubject, mailMessage, intID)

        End Sub
        Public Function SendAddresslVerification(ByVal intID As Integer, ByVal txtUName As String, ByVal txtEmail As String, ByVal strLinkk As String) As String

            Dim strMailString As String = System.IO.File.ReadAllText(System.Web.HttpContext.Current.Server.MapPath("erc20Address.html"))
            strMailString = strMailString.Replace("&&username&&", txtUName)
            strMailString = strMailString.Replace("&&link&&", strLinkk)
            strMailString = strMailString.Replace("&&website&&", website)
            strMailString = strMailString.Replace("&&mail&&", mail)
            strMailString = strMailString.Replace("&&facebook&&", facebook)
            strMailString = strMailString.Replace("&&twitter&&", twitter)
            strMailString = strMailString.Replace("&&telegram&&", telegram)
            strMailString = strMailString.Replace("&&youtube&&", youtube)
            Return strMailString

        End Function

        Public Sub ico_Email_Btc_Confim(ByVal intID As Integer, ByVal FirstName As String, ByVal Email As String, ByVal BtcAmt As String, ByVal StrDate As String, ByVal strLink As String, ByVal strCurr As String)

            Dim mailSubject As String = "BTC deposit confirmation"
            Dim mailFrom As String = "care@tbtc.com"
            Dim mailFromName As String = "TBTC Admin"

            Dim mailMessage As String = SendBTCMAIl(intID, FirstName, Email, BtcAmt, StrDate, strLink, strCurr)

            SendTestMail1(Email, mailFrom, mailSubject, mailMessage, intID)

        End Sub
        Public Function SendBTCMAIl(ByVal intID As Integer, ByVal fname As String, ByVal Email As String, ByVal BtcAmt As String, ByVal StrDate As String, ByVal strLinkk As String, ByVal strCurry As String) As String

            Dim strMailString As String = System.IO.File.ReadAllText(System.Web.HttpContext.Current.Server.MapPath("btcReceiveEmailer.html"))

            strMailString = strMailString.Replace("&&username&&", fname)
            strMailString = strMailString.Replace(" &&BTCAmt&&", BtcAmt)
            strMailString = strMailString.Replace("&&DateTime&&", StrDate)
            strMailString = strMailString.Replace("&&link&&", strLinkk)
            strMailString = strMailString.Replace("&&CURR&&", strCurry)
            strMailString = strMailString.Replace("&&website&&", website)
            strMailString = strMailString.Replace("&&mail&&", mail)
            strMailString = strMailString.Replace("&&facebook&&", facebook)
            strMailString = strMailString.Replace("&&twitter&&", twitter)
            strMailString = strMailString.Replace("&&telegram&&", telegram)
            strMailString = strMailString.Replace("&&youtube&&", youtube)

            Return strMailString

        End Function
        Public Sub SendEmailForFrgtPass(ByVal intID As Integer, ByVal fname As String, ByVal strLink As String, ByVal txtEmail As String)

            Dim mailSubject As String = "Password Recovery Support Team"
            Dim mailFrom As String = "care@tbtc.com"
            Dim mailFromName As String = "TBTC Admin"

            Dim mailMessage As String = PrepareFrgtPassMail(fname, strLink)

            SendTestMail1(txtEmail, mailFrom, mailSubject, mailMessage, intID)

        End Sub
        Public Function PrepareFrgtPassMail(ByVal fname As String, ByVal strrLink As String) As String

            Dim strMailString As String = System.IO.File.ReadAllText(System.Web.HttpContext.Current.Server.MapPath("Frgt_Pass_Emailer.html"))
            strMailString = strMailString.Replace("&&username&&", fname)
            strMailString = strMailString.Replace("&&link&&", strrLink)

            strMailString = strMailString.Replace("&&website&&", website)
            strMailString = strMailString.Replace("&&mail&&", mail)
            strMailString = strMailString.Replace("&&facebook&&", facebook)
            strMailString = strMailString.Replace("&&twitter&&", twitter)
            strMailString = strMailString.Replace("&&telegram&&", telegram)
            strMailString = strMailString.Replace("&&youtube&&", youtube)



            Return strMailString

        End Function
        Public Sub SendEmail_TransactionPass(ByVal intID As Integer, ByVal fname As String, ByVal strTransPass As String, ByVal txtEmail As String, ByVal strrLink As String)

            Dim mailSubject As String = "Your Transaction Password"
            Dim mailFrom As String = "care@tbtc.com"
            Dim mailFromName As String = "TBTC Admin"

            Dim mailMessage As String = PrepareTransactionsMail(fname, strrLink, strTransPass)

            SendTestMail1(txtEmail, mailFrom, mailSubject, mailMessage, intID)

        End Sub
        Public Function PrepareTransactionsMail(ByVal fname As String, ByVal strrLink As String, ByVal strTransPass As String) As String

            Dim strMailString As String = System.IO.File.ReadAllText(System.Web.HttpContext.Current.Server.MapPath("~/changepassword.html"))
            strMailString = strMailString.Replace("&&username&&", fname)
            strMailString = strMailString.Replace("&&TransPass&&", strTransPass)
            strMailString = strMailString.Replace("&&link&&", strrLink)

            strMailString = strMailString.Replace("&&website&&", website)
            strMailString = strMailString.Replace("&&mail&&", mail)
            strMailString = strMailString.Replace("&&facebook&&", facebook)
            strMailString = strMailString.Replace("&&twitter&&", twitter)
            strMailString = strMailString.Replace("&&telegram&&", telegram)
            strMailString = strMailString.Replace("&&youtube&&", youtube)


            Return strMailString

        End Function



        Public Sub SendEmail_ChangePass(ByVal intID As Integer, ByVal fname As String, ByVal strTransPass As String, ByVal txtEmail As String)

            Dim mailSubject As String = "Your Password"
            Dim mailFrom As String = "care@tbtc.com"
            Dim mailFromName As String = "TBTC Admin"

            Dim mailMessage As String = PrepareChangePassMail(fname, strTransPass)

            SendTestMail1(txtEmail, mailFrom, mailSubject, mailMessage, intID)

        End Sub
        Public Function PrepareChangePassMail(ByVal fname As String, ByVal strTransPass As String) As String

            Dim strMailString As String = System.IO.File.ReadAllText(System.Web.HttpContext.Current.Server.MapPath("~/changepassword.html"))
            strMailString = strMailString.Replace("&&username&&", fname)
            strMailString = strMailString.Replace("&&TransPass&&", strTransPass)

            strMailString = strMailString.Replace("&&website&&", website)
            strMailString = strMailString.Replace("&&mail&&", mail)
            strMailString = strMailString.Replace("&&facebook&&", facebook)
            strMailString = strMailString.Replace("&&twitter&&", twitter)
            strMailString = strMailString.Replace("&&telegram&&", telegram)
            strMailString = strMailString.Replace("&&youtube&&", youtube)


            Return strMailString

        End Function


        Public Sub Email_API_for_Trsansaction_Email(ByVal strcid As String, ByVal strauthkey As String, ByVal intappid As Integer, ByVal strsubject As String, ByVal strfrommail As String, ByVal strfromname As String, ByVal strtomail As String, ByVal strtoname As String, ByVal strmessage As String, ByVal strsource As String, ByVal strcomment As String)

            Dim sURL As String


            sURL = "http://beapp.exioms.in/api/transactionalemailapi.php​?cid=" & strcid & "&authkey=" & strauthkey & " &appid=" & intappid & "&subject=" & strsubject & "&frommail=" & strfrommail & "&fromname=" & strfromname & "&tomail=" & strtomail & " &toname=" & strtoname & "&message=" & strmessage & "&source=" & strsource & "&comment=" & strcomment & ""

            Dim request As Net.WebRequest

            request = Net.WebRequest.Create(sURL)

            Dim resp As Net.WebResponse = request.GetResponse()

        End Sub
        '**** Sending Test Mail ****
        Public Function SendTestMail(ByVal strTo As String, ByVal strFrom As String, ByVal strSubject As String, ByVal strMessage As String, ByVal uid As Integer) As Boolean

            'Dim strHost, strPort, strUserName, strPasswrod As String
            'Dim ds As New Data.DataSet
            'Dim ds1 As New Data.DataSet
            'Dim intMailCount As Integer = objOdbc.executeScalar_int("SELECT Count(1) From mlm_send_mail_log ")

            '' ***** Per Day Maximum 50 E-mail can be sent. *****
            'If intMailCount < 100000 Then
            '    Dim Mailmsg As New MailMessage()

            '    'Get SMTP Details
            '    Dim xmlreader As New XmlTextReader(HttpContext.Current.Server.MapPath("~/send_mail.xml"))
            '    ds1.ReadXml(xmlreader)
            '    xmlreader.Close()

            '    If (ds1.Tables.Count <> 0) Then
            '        strHost = ds1.Tables(0).Rows(0).Item(0).ToString()
            '        strPort = ds1.Tables(0).Rows(0).Item(1).ToString()
            '        strUserName = ds1.Tables(0).Rows(0).Item(2).ToString()
            '        strPasswrod = ds1.Tables(0).Rows(0).Item(3).ToString()
            '    End If

            '    'End smtp Details

            '    Dim client As New System.Net.Mail.SmtpClient(strHost, strPort)
            '    Dim netwrkCrd As New System.Net.NetworkCredential()
            '    netwrkCrd.UserName = strUserName
            '    netwrkCrd.Password = strPasswrod
            '    client.Credentials = netwrkCrd

            '    Try
            '        Mailmsg.[To].Clear()
            '        Mailmsg.From = New MailAddress(strFrom)
            '        Mailmsg.[To].Add(New MailAddress(strTo))
            '        Mailmsg.Subject = strSubject
            '        Mailmsg.IsBodyHtml = True
            '        Mailmsg.Body = strMessage.ToString()
            '        client.Send(Mailmsg)

            '        objOdbc.executeNonQuery("INSERT INTO mlm_send_mail_log (mail_to,mail_subject,mail_message,mail_status,userid) VALUES ('" & strTo & "','" & strSubject & "','" & strMessage & "',1," & uid & ")")

            '        Return True

            '    Catch ex As Exception

            '        objOdbc.executeNonQuery("INSERT INTO mlm_send_mail_log (mail_to,mail_subject,mail_message,mail_status,userid) VALUES ('" & strTo & "','" & strSubject & "','" & strMessage & "',0," & uid & ")")

            '        Return False

            '    End Try

            'End If

            SendSimpleMessage(strTo, strFrom, strSubject, strMessage)
            Return True


        End Function



        Public Function getTime(ByVal strTimeZone As String) As DateTime
            Dim timeUtc As DateTime = DateTime.UtcNow

            Return DateTime.Now
        End Function



        Public Function SendSimpleMessage(ByVal strTo As String, ByVal strFrom As String, ByVal strSubject As String, ByVal strMessage As String) As String
            Dim client As New RestClient()
            Dim url As New Uri("https://api.mailgun.net/v3")
            client.BaseUrl = url
            client.Authenticator = New HttpBasicAuthenticator("api", "key-ec12b5075915181e992c817bc164c6b1")
            Dim request As New RestRequest()
            request.AddParameter("domain", "global9fx.com", ParameterType.UrlSegment)
            request.Resource = "{domain}/messages"
            request.AddParameter("from", strFrom)
            request.AddParameter("to", strTo)
            request.AddParameter("subject", strSubject)
            request.AddParameter("text", strMessage)
            request.Method = Method.POST
            Dim response = client.Execute(request)
            Dim strData As String = response.Content.ToString()

            Return strData


        End Function

        Public Function SendTestMail1(ByVal strTo As String, ByVal strFrom As String, ByVal strSubject As String, ByVal strMessage As String, ByVal uid As Integer) As String

            Dim strHost, strPort, strUserName, strPasswrod As String
            ' Dim ds As New Data.DataSet
            'Dim ds1 As New Data.DataSet
            Try
                Dim objErComm As New ercommission

                Dim al As New System.Collections.ArrayList()

                al.Add("strSubject")
                al.Add(strSubject)
                al.Add("strFromEmail")
                al.Add(strFrom)
                al.Add("strFromName")
                al.Add(strFrom)
                al.Add("strToEmail")
                al.Add(strTo)
                al.Add("strToName")
                al.Add(strTo)
                al.Add("strMessage")
                al.Add(strMessage)
                al.Add("strToken")
                al.Add("hslrhglsw629ls")



                'Dim strCommissionURL As String = ConfigurationManager.AppSettings("commissionService")
                'Public Function SendMailTransaction2(ByVal strSubject As String, ByVal strFromEmail As String, ByVal strFromName As String, ByVal strToEmail As String, ByVal strToName As String, ByVal strMessage As String) As Boolean

                Dim strCommissionURL As String = "http://bewebservices.exioms.in/sendmail.asmx"
                Dim strCommission As String = objErComm.dynamicWebMethod(7, al, "SendMailTransaction4", strCommissionURL)

                objOdbc.executeNonQuery("INSERT INTO ico_mail_status (mail_to,mail_subject,mail_status,userid) VALUES ('" & strTo & "','" & strSubject & "',1," & uid & ")")

                Return "Success"

            Catch ex As Exception

                objOdbc.executeNonQuery("INSERT INTO ico_mail_status (mail_to,mail_subject,mail_status,userid) VALUES ('" & strTo & "','" & strSubject & "',0," & uid & ")")

                Return ex.Message.ToString()
            End Try
        End Function


        'Add by Ejaj
        Public Sub SendReferralLink(ByVal intID As Integer, ByVal strUserName As String, ByVal strLink As String, ByVal txtEmail As String)

            Dim mailSubject As String = "Your Referral Link"
            Dim mailFrom As String = "care@tbtc.com"
            Dim mailFromName As String = "TBTC Admin"

            Dim mailMessage As String = PrepareReferralLink(strUserName, strLink)

            SendTestMail1(txtEmail, mailFrom, mailSubject, mailMessage, intID)

        End Sub
        Public Function PrepareReferralLink(ByVal strUserName As String, ByVal strLink As String) As String

            Dim strMailString As String = System.IO.File.ReadAllText(System.Web.HttpContext.Current.Server.MapPath("~/ReferralLink.html"))
            strMailString = strMailString.Replace("&&username&&", strUserName)
            strMailString = strMailString.Replace("&&link&&", strLink)

            strMailString = strMailString.Replace("&&website&&", website)
            strMailString = strMailString.Replace("&&mail&&", mail)
            strMailString = strMailString.Replace("&&facebook&&", facebook)
            strMailString = strMailString.Replace("&&twitter&&", twitter)
            strMailString = strMailString.Replace("&&telegram&&", telegram)
            strMailString = strMailString.Replace("&&youtube&&", youtube)


            Return strMailString

        End Function


    End Class
End Namespace

