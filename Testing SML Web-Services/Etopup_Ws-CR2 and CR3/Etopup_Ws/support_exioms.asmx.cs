﻿using Etopup_Ws.old_App_Code;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Services;
using RestSharp;

namespace Etopup_Ws
{
    /// <summary>
    /// Summary description for Support_exioms
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class Support_exioms : System.Web.Services.WebService
    {
        DataSet ds = new DataSet();
        string strQuery = string.Empty;
        string strLimit = string.Empty;
        ODBC objODBC = new ODBC();
        ODBC_LOG objODBCLOG = new ODBC_LOG();
        DataTable dt = new DataTable();
        DataTable dt2 = new DataTable();
        old_App_Code.DateFormat objDT = new old_App_Code.DateFormat();
        Encrypt_Decrypt ED = new Encrypt_Decrypt();
        CreateFile objCreateFile = new CreateFile();
        CommonFunction objcf = new CommonFunction();
        old_App_Code.DateFormat objDateFor = new old_App_Code.DateFormat();


        /* Member Registration Details*/
        [WebMethod]
        public DataSet sup_AllMemberRegistration(string strUserName, int intUserID, int usertype_id, int PageSize, int intPage, string strSearch, string strIPAddress, int intDesignationType, string strMacAddress, string strOSDetails, string strIMEINo, string strGcmID, string strAPPVersion, int intSource, int intCreatedByType)
        {
            try
            {
                if (intUserID > 0)
                {
                    try
                    {
                        objCreateFile.CreateTextFile(strIPAddress, strMacAddress, strOSDetails, strIMEINo, strGcmID, strAPPVersion, intSource.ToString(), intCreatedByType, 999, " Exioms Support- sup_AllMemberRegistration", strUserName);
                    }
                    catch (Exception)
                    {


                    }
                    finally
                    {
                        strLimit = " LIMIT " + PageSize * (intPage - 1) + "," + PageSize + "";


                        int intCount = objODBCLOG.executeScalar_int("SELECT * FROM `er_login` where 1=1 " + strSearch + "");

                        strQuery = "SELECT `userid`, `username`, `emailid`, `gender`, `mobile`, `password`, `full_name`, `usertype_id`, `user_status`, `parent_id`, `reseller_id`, `distributor_id`, `sub_distributor_id`, `tpin`, `last_login_datetime`, `last_login_ip`, `created_by`, `created_by_type`, `created_on`, `Active`, `province_id`, `province_Name`, `city_name`, `Address`, `salt1`, `salt2`, `salt3`, `pass_key`, `encryption_key`, `last_log_id`, `auth_status`, `M_Pin`, `pass`, `status_authority`, `add_user_rights`, `commission_rights`, `bulk_transfer`, `group_topup`, `slab_status`, `fource_logout` FROM `er_login` where 1=1 " + strSearch + " order by userid desc " + strLimit + "";

                    }
                    ds = objODBCLOG.Report(strQuery);
                }
                return ds;
            }
            catch (Exception)
            {
                throw;

            }
            finally
            {
                ds.Dispose();
            }
        }


        [WebMethod]
        public DataSet sup_AllMemberMobile(string strUserName, int intUserID, int usertype_id, int PageSize, int intPage, string strSearch, string strIPAddress, int intDesignationType, string strMacAddress, string strOSDetails, string strIMEINo, string strGcmID, string strAPPVersion, int intSource, int intCreatedByType)
        {
            try
            {
                if (intUserID > 0)
                {
                    try
                    {
                        objCreateFile.CreateTextFile(strIPAddress, strMacAddress, strOSDetails, strIMEINo, strGcmID, strAPPVersion, intSource.ToString(), intCreatedByType, 999, " Exioms Support- sup_AllMemberMobile", strUserName);
                    }
                    catch (Exception)
                    {


                    }
                    finally
                    {
                        strLimit = " LIMIT " + PageSize * (intPage - 1) + "," + PageSize + "";


                        int intCount = objODBCLOG.executeScalar_int("SELECT * FROM `er_login` l inner join er_mobile m on l.userid=m.userid where m.usertype=2 " + strSearch + "");

                        strQuery = "SELECT l.userid,l.username,l.emailid,m.mobile,m.operator_id,m.mob_type,m.usertype  FROM `er_login` l inner join er_mobile m on l.userid=m.userid where m.usertype=2 " + strSearch + " order by l.userid desc " + strLimit + "";

                    }
                    ds = objODBCLOG.Report(strQuery);
                }
                return ds;
            }
            catch (Exception)
            {
                throw;

            }
            finally
            {
                ds.Dispose();
            }
        }


        /* Commission Details*/
        [WebMethod]
        public DataSet sup_AllCommission(string strUserName, int intUserID, int usertype_id, int PageSize, int intPage, string strSearch, string strIPAddress, int intDesignationType, string strMacAddress, string strOSDetails, string strIMEINo, string strGcmID, string strAPPVersion, int intSource, int intCreatedByType)
        {
            try
            {
                if (intUserID > 0)
                {
                    try
                    {
                        objCreateFile.CreateTextFile(strIPAddress, strMacAddress, strOSDetails, strIMEINo, strGcmID, strAPPVersion, intSource.ToString(), intCreatedByType, 999, " Exioms Support- sup_AllMemberMobile", strUserName);
                    }
                    catch (Exception)
                    {


                    }
                    finally
                    {
                        strLimit = " LIMIT " + PageSize * (intPage - 1) + "," + PageSize + "";


                        int intCount = objODBCLOG.executeScalar_int("SELECT * FROM er_login l inner join er_commission_amount m on l.userid=m.userid inner join er_login p on l.userid=m.parent_id where 1=1 " + strSearch + "");

                        strQuery = "SELECT l.userid,l.username,l.emailid,m.recharge_id,m.recharge_amount,m.operator_id,m.commission_amount,m.comm_per,m.created_on,m.status,m.parent_id as P_userid,p.username as P_Username,p.emailid as P_emailid FROM er_login l inner join er_commission_amount m on l.userid=m.userid inner join er_login p on l.userid=m.parent_id where 1=1 " + strSearch + " order by m.id desc " + strLimit + "";

                    }
                    ds = objODBCLOG.Report(strQuery);
                }
                return ds;
            }
            catch (Exception)
            {
                throw;

            }
            finally
            {
                ds.Dispose();
            }
        }



        /* Recharge Activity Details*/

        [WebMethod]
        public DataSet sup_All_er_Recharge(string strUserName, int intUserID, int usertype_id, int PageSize, int intPage, string strSearch, string strIPAddress, int intDesignationType, string strMacAddress, string strOSDetails, string strIMEINo, string strGcmID, string strAPPVersion, int intSource, int intCreatedByType)
        {
            try
            {
                if (intUserID > 0)
                {
                    try
                    {
                        objCreateFile.CreateTextFile(strIPAddress, strMacAddress, strOSDetails, strIMEINo, strGcmID, strAPPVersion, intSource.ToString(), intCreatedByType, 999, " Exioms Support- sup_All_er_Recharge", strUserName);
                    }
                    catch (Exception)
                    {


                    }
                    finally
                    {
                        strLimit = " LIMIT " + PageSize * (intPage - 1) + "," + PageSize + "";


                        int intCount = objODBCLOG.executeScalar_int("SELECT * FROM er_login l inner join er_recharge m on l.userid=m.userid where 1=1 " + strSearch + "");

                        strQuery = "SELECT l.userid,l.username,l.emailid,m.id,m.trans_number,m.Operater_table_id,m.type_id,m.type_name,m.operator_id,m. operator_name,m.mobile_number,m.amount,m.deduct_amt,m.res_deduct_amt,m.comm_amt,m.comm_per,m.closing_balance,m.system_closing_balance,m. status,m.operator_transid,m.ap_transid,m.os_details,m.gcm_id,m.imei_no,m.created_on,m.modified_on,m.api_type,m.prev_api_type,m. prev_comment,m.dispute_status,m.comment,m.source,m.ip_address,m.getStatus,m.operator_balance,m.group_topup_id,m.ussd_topup_id,m. rollback_status FROM er_login l inner join er_recharge m on l.userid=m.userid where 1=1 " + strSearch + " order by m.id desc " + strLimit + "";

                    }
                    ds = objODBCLOG.Report(strQuery);
                }
                return ds;
            }
            catch (Exception)
            {
                throw;

            }
            finally
            {
                ds.Dispose();
            }
        }
        /* Recharge Activity Details for Etisalat*/
        [WebMethod]
        public DataSet sup_All_er_Recharge_Etisalat(string strUserName, int intUserID, int usertype_id, int PageSize, int intPage, string strSearch, string strIPAddress, int intDesignationType, string strMacAddress, string strOSDetails, string strIMEINo, string strGcmID, string strAPPVersion, int intSource, int intCreatedByType)
        {
            try
            {
                if (intUserID > 0)
                {
                    try
                    {
                        objCreateFile.CreateTextFile(strIPAddress, strMacAddress, strOSDetails, strIMEINo, strGcmID, strAPPVersion, intSource.ToString(), intCreatedByType, 999, " Exioms Support- sup_All_er_Recharge_Etisalat", strUserName);
                    }
                    catch (Exception)
                    {


                    }
                    finally
                    {
                        strLimit = " LIMIT " + PageSize * (intPage - 1) + "," + PageSize + "";


                        int intCount = objODBCLOG.executeScalar_int("SELECT * FROM er_login l inner join etisalat_topup_request m on l.userid=m.userid where 1=1 " + strSearch + "");

                        strQuery = "SELECT l.userid,l.username,l.emailid,m.id,m.userid,m.recharge_id,m.topup_mobile,m.amount,m.status,m.IP_PORT,m.login_request,m.login_response,m.balance,m.login_checksum,m.login_description,m.login_byte_received,m.login_status,m.login_operation_code,m.login_fixed_length,m.transaction_startFlag,m.transaction_message_header,m.transaction_convertation_Header,m.Transaction_header,m.transaction_operation_code,m.transaction_fixlength,m.transaction_ID_recharge,m.transaction_OperaterID,m.transaction_RETN,m.transaction_DESC,m.BEFOROPER,m.FEE,m.AFTEROPER,m.CSVSTOP,m.Transaction_Checksum,m.Trans_Receive_Checksum,m.Transaction_Request,m.Transaction_Response,m.Transactio_Status,m.ReverseID,m.ReverseID_operaterID,m.transaction_type,m.Topup_type,m.created_on,m.transaction_bytereceived,m.all_response FROM er_login l inner join etisalat_topup_request m on l.userid=m.userid where 1=1 " + strSearch + " order by m.id desc " + strLimit + "";

                    }
                    ds = objODBCLOG.Report(strQuery);
                }
                return ds;
            }
            catch (Exception)
            {
                throw;

            }
            finally
            {
                ds.Dispose();
            }
        }

        [WebMethod]
        public DataSet sup_All_er_Recharge_USSD_Etisalat(string strUserName, int intUserID, int usertype_id, int PageSize, int intPage, string strSearch, string strIPAddress, int intDesignationType, string strMacAddress, string strOSDetails, string strIMEINo, string strGcmID, string strAPPVersion, int intSource, int intCreatedByType)
        {
            try
            {
                if (intUserID > 0)
                {
                    try
                    {
                        objCreateFile.CreateTextFile(strIPAddress, strMacAddress, strOSDetails, strIMEINo, strGcmID, strAPPVersion, intSource.ToString(), intCreatedByType, 999, " Exioms Support- sup_All_er_Recharge_USSD_Etisalat", strUserName);
                    }
                    catch (Exception)
                    {

                    }
                    finally
                    {
                        strLimit = " LIMIT " + PageSize * (intPage - 1) + "," + PageSize + "";


                        int intCount = objODBCLOG.executeScalar_int("SELECT * FROM er_login l inner join er_etisalat_request_ussd m on l.userid=m.userid where 1=1 " + strSearch + "");

                        strQuery = "SELECT l.userid,l.username,l.emailid,m.id,m.userid,m.phone,m.coustomer_mobile,m.ussd,m.cdate,m.sign,m.request,m.response,m.created_on,m.USSD_type,m.recharge_ID,m.Operater_TransactionID,m.Balance FROM er_login l inner join er_etisalat_request_ussd m on l.userid=m.userid where 1=1 " + strSearch + " order by m.id desc " + strLimit + "";

                    }
                    ds = objODBCLOG.Report(strQuery);
                }
                return ds;
            }
            catch (Exception)
            {
                throw;

            }
            finally
            {
                ds.Dispose();
            }
        }


        [WebMethod]
        public DataSet sup_All_er_Recharge_Reverse_Etisalat(string strUserName, int intUserID, int usertype_id, int PageSize, int intPage, string strSearch, string strIPAddress, int intDesignationType, string strMacAddress, string strOSDetails, string strIMEINo, string strGcmID, string strAPPVersion, int intSource, int intCreatedByType)
        {
            try
            {
                if (intUserID > 0)
                {
                    try
                    {
                        objCreateFile.CreateTextFile(strIPAddress, strMacAddress, strOSDetails, strIMEINo, strGcmID, strAPPVersion, intSource.ToString(), intCreatedByType, 999, " Exioms Support- sup_All_er_Recharge_Reverse_Etisalat", strUserName);
                    }
                    catch (Exception)
                    {

                    }
                    finally
                    {
                        strLimit = " LIMIT " + PageSize * (intPage - 1) + "," + PageSize + "";


                        int intCount = objODBCLOG.executeScalar_int("SELECT * FROM er_login l inner join etisalat_reverse_topup m on l.userid=m.userid where 1=1 " + strSearch + "");

                        strQuery = "SELECT l.userid,l.username,l.emailid,m.id,m.userid,m.etisalat_table_topup_ID,m.mobile,m.amount,m.recharge_id,m.OperatorID,m.api_Operater_ID,m.created_on,m.status,m.request,m.resposne,m.all_response FROM er_login l inner join etisalat_reverse_topup m on l.userid=m.userid where 1=1 " + strSearch + " order by m.id desc " + strLimit + "";

                    }
                    ds = objODBCLOG.Report(strQuery);
                }
                return ds;
            }
            catch (Exception)
            {
                throw;

            }
            finally
            {
                ds.Dispose();
            }
        }


        /* Recharge Activity Details for Roshan*/

        [WebMethod]
        public DataSet sup_All_er_Recharge_Roshan(string strUserName, int intUserID, int usertype_id, int PageSize, int intPage, string strSearch, string strIPAddress, int intDesignationType, string strMacAddress, string strOSDetails, string strIMEINo, string strGcmID, string strAPPVersion, int intSource, int intCreatedByType)
        {
            try
            {
                if (intUserID > 0)
                {
                    try
                    {
                        objCreateFile.CreateTextFile(strIPAddress, strMacAddress, strOSDetails, strIMEINo, strGcmID, strAPPVersion, intSource.ToString(), intCreatedByType, 999, " Exioms Support- sup_All_er_Recharge_Roshan", strUserName);
                    }
                    catch (Exception)
                    {

                    }
                    finally
                    {
                        strLimit = " LIMIT " + PageSize * (intPage - 1) + "," + PageSize + "";


                        int intCount = objODBCLOG.executeScalar_int("SELECT * FROM er_login l inner join er_roshan_request m on l.userid=m.userid where 1=1 " + strSearch + "");

                        strQuery = "SELECT l.userid,l.username,l.emailid,m.id,m.userid,m.recharge_id,m.msisdn,m.provider,m.payID,m.pin,m.language1,m.status,m.type,m.txnID,m.balance,m.trID,m.message,m.TXNSTATUS,m.frbalance,m.amount,m.IVR_response,m.provider2,m.operaterID,m.payment_type,m.pay_ID_2,m.pay_ID,m.msidn_2,m.created_on,m.trans_date,m.new_balance,m.balance_request,m.balance_response,m.top_up_request,m.top_up_response,m.USSD_type FROM er_login l inner join er_roshan_request m on l.userid=m.userid where 1=1 " + strSearch + " order by m.id desc " + strLimit + "";

                    }
                    ds = objODBCLOG.Report(strQuery);
                }
                return ds;
            }
            catch (Exception)
            {
                throw;

            }
            finally
            {
                ds.Dispose();
            }
        }


        /* Recharge Activity Details for Roshan USSD*/

        [WebMethod]
        public DataSet sup_All_er_Recharge_Roshan_USSD(string strUserName, int intUserID, int usertype_id, int PageSize, int intPage, string strSearch, string strIPAddress, int intDesignationType, string strMacAddress, string strOSDetails, string strIMEINo, string strGcmID, string strAPPVersion, int intSource, int intCreatedByType)
        {
            try
            {
                if (intUserID > 0)
                {
                    try
                    {
                        objCreateFile.CreateTextFile(strIPAddress, strMacAddress, strOSDetails, strIMEINo, strGcmID, strAPPVersion, intSource.ToString(), intCreatedByType, 999, " Exioms Support- sup_All_er_Recharge_Roshan", strUserName);
                    }
                    catch (Exception)
                    {

                    }
                    finally
                    {
                        strLimit = " LIMIT " + PageSize * (intPage - 1) + "," + PageSize + "";


                        int intCount = objODBCLOG.executeScalar_int("SELECT * FROM er_login l inner join er_roshan_ussd m on l.userid=m.userid where 1=1 " + strSearch + "");

                        strQuery = "SELECT l.userid,l.username,l.emailid,m.id,m.userid,m.dlg_id,m.short_code,m.resp_text,m.req_text,m.msisdn,m.session_id,m.concat_req_string,m.date_time,m.str_circle_id,m.str_dcs,m.str_curr_menu,m.request_text,m.response_text,m.recharge_ID,m.created_on,m.USSD_type FROM er_login l inner join er_roshan_ussd m on l.userid=m.userid where 1=1 " + strSearch + " order by m.id desc " + strLimit + "";

                    }
                    ds = objODBCLOG.Report(strQuery);
                }
                return ds;
            }
            catch (Exception)
            {
                throw;

            }
            finally
            {
                ds.Dispose();
            }
        }


        /* Recharge Activity Details for Salaam*/

        [WebMethod]
        public DataSet sup_All_er_Recharge_Reverse_Salaam(string strUserName, int intUserID, int usertype_id, int PageSize, int intPage, string strSearch, string strIPAddress, int intDesignationType, string strMacAddress, string strOSDetails, string strIMEINo, string strGcmID, string strAPPVersion, int intSource, int intCreatedByType)
        {
            try
            {
                if (intUserID > 0)
                {
                    try
                    {
                        objCreateFile.CreateTextFile(strIPAddress, strMacAddress, strOSDetails, strIMEINo, strGcmID, strAPPVersion, intSource.ToString(), intCreatedByType, 999, " Exioms Support- sup_All_er_Recharge_Reverse_Salaam", strUserName);
                    }
                    catch (Exception)
                    {

                    }
                    finally
                    {
                        strLimit = " LIMIT " + PageSize * (intPage - 1) + "," + PageSize + "";


                        int intCount = objODBCLOG.executeScalar_int("SELECT * FROM er_login l inner join er_salaam m on l.userid=m.userid where 1=1 " + strSearch + "");

                        strQuery = "SELECT l.userid,l.username,l.emailid,m.id,m.userid,m.recharge_id,m.sign_key,m.encryption_key,m.activation_code,m.product_code,m.m_pin,m.request_unique_id,m.mobile_number,m.amount,m.email_id,m.request,m.response,m.response_code,m.response_description,m.transaction_id,m.Balance,m.confirmation_code,m.TransactionInfo,m.RequestDateTime,m.TransactionFee,m.Commission,m.status,m.created_on,m.to_surcharge,m.to_commission,m.sender_phone,m.sender_name,m.sender_id_type,m.sender_id_number,m.receiver_phone,m.receiver_name,m.receiver_id_type,m.receiver_id_number,m.encrypted_request,m.encrypted_response FROM er_login l inner join er_salaam m on l.userid=m.userid where 1=1 " + strSearch + " order by m.id desc " + strLimit + "";

                    }
                    ds = objODBCLOG.Report(strQuery);
                }
                return ds;
            }
            catch (Exception)
            {
                throw;

            }
            finally
            {
                ds.Dispose();
            }
        }

        /* Recharge Activity Details for EasyPay*/
        [WebMethod]
        public DataSet sup_All_er_Recharge_Reverse_EasyPay(string strUserName, int intUserID, int usertype_id, int PageSize, int intPage, string strSearch, string strIPAddress, int intDesignationType, string strMacAddress, string strOSDetails, string strIMEINo, string strGcmID, string strAPPVersion, int intSource, int intCreatedByType)
        {
            try
            {
                if (intUserID > 0)
                {
                    try
                    {
                        objCreateFile.CreateTextFile(strIPAddress, strMacAddress, strOSDetails, strIMEINo, strGcmID, strAPPVersion, intSource.ToString(), intCreatedByType, 999, " Exioms Support- sup_All_er_Recharge_Reverse_EasyPay", strUserName);
                    }
                    catch (Exception)
                    {

                    }
                    finally
                    {
                        strLimit = " LIMIT " + PageSize * (intPage - 1) + "," + PageSize + "";


                        int intCount = objODBCLOG.executeScalar_int("SELECT * FROM er_login l inner join er_easy_pay_request m on l.userid=m.userid where 1=1 " + strSearch + "");

                        strQuery = "SELECT l.userid,l.username,l.emailid,m.id,m.userid,m.recharge_id,m.terminal_id,m.request_type,m.sign,m.srvid,m.mobile_number,m.amount,m.net_amount,m.payid_responseid,m.payment_state,m.status_code,m.status_desc,m.txn_date,m.created_on,m.currency_rate,m.balance,m.overdraft,m.withheld,m.payable,m.Response,m.Request,m.status FROM er_login l inner join er_easy_pay_request m on l.userid=m.userid where 1=1 " + strSearch + " order by m.id desc " + strLimit + "";

                    }
                    ds = objODBCLOG.Report(strQuery);
                }
                return ds;
            }
            catch (Exception)
            {
                throw;

            }
            finally
            {
                ds.Dispose();
            }
        }

        /* Recharge Activity Details for EasyPay*/
        [WebMethod]
        public DataSet sup_All_er_Recharge_Reverse_EasyPay_Pending(string strUserName, int intUserID, int usertype_id, int PageSize, int intPage, string strSearch, string strIPAddress, int intDesignationType, string strMacAddress, string strOSDetails, string strIMEINo, string strGcmID, string strAPPVersion, int intSource, int intCreatedByType)
        {
            try
            {
                if (intUserID > 0)
                {
                    try
                    {
                        objCreateFile.CreateTextFile(strIPAddress, strMacAddress, strOSDetails, strIMEINo, strGcmID, strAPPVersion, intSource.ToString(), intCreatedByType, 999, " Exioms Support- sup_All_er_Recharge_Reverse_EasyPay_Pending", strUserName);
                    }
                    catch (Exception)
                    {

                    }
                    finally
                    {
                        strLimit = " LIMIT " + PageSize * (intPage - 1) + "," + PageSize + "";


                        int intCount = objODBCLOG.executeScalar_int("SELECT * FROM er_easy_pay_pending where 1=1 " + strSearch + "");

                        strQuery = "SELECT `id`, `payid`, `recharge_table_id`, `easypay_table_id`, `request`, `response` From er_easy_pay_pending where 1=1 " + strSearch + " order by id desc " + strLimit + "";

                    }
                    ds = objODBCLOG.Report(strQuery);
                }
                return ds;
            }
            catch (Exception)
            {
                throw;

            }
            finally
            {
                ds.Dispose();
            }
        }


        /* Recharge Activity Details for Generate E Money Wallet*/
        [WebMethod]
        public DataSet sup_All_SystemEmoney(string strUserName, int intUserID, int usertype_id, int PageSize, int intPage, string strSearch, string strIPAddress, int intDesignationType, string strMacAddress, string strOSDetails, string strIMEINo, string strGcmID, string strAPPVersion, int intSource, int intCreatedByType)
        {
            try
            {
                if (intUserID > 0)
                {
                    try
                    {
                        objCreateFile.CreateTextFile(strIPAddress, strMacAddress, strOSDetails, strIMEINo, strGcmID, strAPPVersion, intSource.ToString(), intCreatedByType, 999, " Exioms Support- sup_All_SystemEmoney", strUserName);
                    }
                    catch (Exception)
                    {

                    }
                    finally
                    {
                        strLimit = " LIMIT " + PageSize * (intPage - 1) + "," + PageSize + "";


                        int intCount = objODBCLOG.executeScalar_int("SELECT * FROM er_sys_wallet where 1=1 " + strSearch + "");

                        strQuery = "SELECT `id`, `amt` FROM `er_sys_wallet` where 1=1 " + strSearch + " order by id ASC " + strLimit + "";

                    }
                    ds = objODBCLOG.Report(strQuery);
                }
                return ds;
            }
            catch (Exception)
            {
                throw;

            }
            finally
            {
                ds.Dispose();
            }
        }


        /* Recharge Activity Details for EasyPay*/
        [WebMethod]
        public DataSet sup_All_UserWallet(string strUserName, int intUserID, int usertype_id, int PageSize, int intPage, string strSearch, string strIPAddress, int intDesignationType, string strMacAddress, string strOSDetails, string strIMEINo, string strGcmID, string strAPPVersion, int intSource, int intCreatedByType)
        {
            try
            {
                if (intUserID > 0)
                {
                    try
                    {
                        objCreateFile.CreateTextFile(strIPAddress, strMacAddress, strOSDetails, strIMEINo, strGcmID, strAPPVersion, intSource.ToString(), intCreatedByType, 999, " Exioms Support- sup_All_UserWallet", strUserName);
                    }
                    catch (Exception)
                    {

                    }
                    finally
                    {
                        strLimit = " LIMIT " + PageSize * (intPage - 1) + "," + PageSize + "";


                        int intCount = objODBCLOG.executeScalar_int("SELECT * FROM er_login l inner join er_wallet m on l.userid=m.userid where 1=1 " + strSearch + "");

                        strQuery = "SELECT l.userid,l.username,l.emailid,m.id,m.userid,m.ex_wallet,m.min_wallet,m.usertype FROM er_login l inner join er_wallet m on l.userid=m.userid where 1=1 " + strSearch + " order by m.id desc " + strLimit + "";

                    }
                    ds = objODBCLOG.Report(strQuery);
                }
                return ds;
            }
            catch (Exception)
            {
                throw;

            }
            finally
            {
                ds.Dispose();
            }
        }


        /* Recharge Activity Details for Threshold Amount*/
        [WebMethod]
        public DataSet sup_All_er_wallet_threshold(string strUserName, int intUserID, int usertype_id, int PageSize, int intPage, string strSearch, string strIPAddress, int intDesignationType, string strMacAddress, string strOSDetails, string strIMEINo, string strGcmID, string strAPPVersion, int intSource, int intCreatedByType)
        {
            try
            {
                if (intUserID > 0)
                {
                    try
                    {
                        objCreateFile.CreateTextFile(strIPAddress, strMacAddress, strOSDetails, strIMEINo, strGcmID, strAPPVersion, intSource.ToString(), intCreatedByType, 999, " Exioms Support- sup_All_er_wallet_threshold", strUserName);
                    }
                    catch (Exception)
                    {

                    }
                    finally
                    {
                        strLimit = " LIMIT " + PageSize * (intPage - 1) + "," + PageSize + "";


                        int intCount = objODBCLOG.executeScalar_int("SELECT * FROM er_wallet_threshold where 1=1 " + strSearch + "");

                        strQuery = "SELECT `id`, `usertype_id`, `amt` FROM `er_wallet_threshold` where 1=1 " + strSearch + " order by id ASC " + strLimit + "";

                    }
                    ds = objODBCLOG.Report(strQuery);
                }
                return ds;
            }
            catch (Exception)
            {
                throw;

            }
            finally
            {
                ds.Dispose();
            }
        }


        /* Recharge Activity Details for EasyPay*/
        [WebMethod]
        public DataSet sup_All_er_Purchase_Wallet(string strUserName, int intUserID, int usertype_id, int PageSize, int intPage, string strSearch, string strIPAddress, int intDesignationType, string strMacAddress, string strOSDetails, string strIMEINo, string strGcmID, string strAPPVersion, int intSource, int intCreatedByType)
        {
            try
            {
                if (intUserID > 0)
                {
                    try
                    {
                        objCreateFile.CreateTextFile(strIPAddress, strMacAddress, strOSDetails, strIMEINo, strGcmID, strAPPVersion, intSource.ToString(), intCreatedByType, 999, " Exioms Support- sup_All_er_Purchase_Wallet", strUserName);
                    }
                    catch (Exception)
                    {

                    }
                    finally
                    {
                        strLimit = " LIMIT " + PageSize * (intPage - 1) + "," + PageSize + "";


                        int intCount = objODBCLOG.executeScalar_int("SELECT * FROM er_login l inner join er_wallet_purchase m on l.userid=m.userid where 1=1 " + strSearch + "");

                        strQuery = "SELECT l.userid,l.username,l.emailid,m.id,m.trans_number,m.userid,m.parent_id,m.request_amt,m.payment_mode,m.reference_no,m.rcpt_url,m.created_on,m.comment,m.transfer_amt,m.status,m.confirmed_by,m.confirmed_on,m.confirmed_comment FROM er_login l inner join er_wallet_purchase m on l.userid=m.userid where 1=1 " + strSearch + " order by m.id desc " + strLimit + "";

                    }
                    ds = objODBCLOG.Report(strQuery);
                }
                return ds;
            }
            catch (Exception)
            {
                throw;

            }
            finally
            {
                ds.Dispose();
            }
        }


        /* Recharge Activity Details for All Transaction*/
        [WebMethod]
        public DataSet sup_er_All_Transaction(string strUserName, int intUserID, int usertype_id, int PageSize, int intPage, string strSearch, string strIPAddress, int intDesignationType, string strMacAddress, string strOSDetails, string strIMEINo, string strGcmID, string strAPPVersion, int intSource, int intCreatedByType)
        {
            try
            {
                if (intUserID > 0)
                {
                    try
                    {
                        objCreateFile.CreateTextFile(strIPAddress, strMacAddress, strOSDetails, strIMEINo, strGcmID, strAPPVersion, intSource.ToString(), intCreatedByType, 999, " Exioms Support- sup_All_er_Purchase_Wallet", strUserName);
                    }
                    catch (Exception)
                    {

                    }
                    finally
                    {
                        strLimit = " LIMIT " + PageSize * (intPage - 1) + "," + PageSize + "";


                        int intCount = objODBCLOG.executeScalar_int("SELECT * FROM er_login l inner join er_wallet_transaction m on l.userid=m.userid where 1=1 " + strSearch + "");

                        strQuery = "SELECT l.userid,l.username,l.emailid,m.id,m.userid,m.trans_number,m.trans_date_time,m.amount,m.trans_type,m.narration,m.balance_amount,m.trans_for FROM er_login l inner join er_wallet_transaction m on l.userid=m.userid where 1=1 " + strSearch + " order by m.id desc " + strLimit + "";

                    }
                    ds = objODBCLOG.Report(strQuery);
                }
                return ds;
            }
            catch (Exception)
            {
                throw;

            }
            finally
            {
                ds.Dispose();
            }
        }



        /* Recharge Activity Details for All Transaction*/
        [WebMethod]
        public DataSet sup_er_er_wallet_transfer_individual(string strUserName, int intUserID, int usertype_id, int PageSize, int intPage, string strSearch, string strIPAddress, int intDesignationType, string strMacAddress, string strOSDetails, string strIMEINo, string strGcmID, string strAPPVersion, int intSource, int intCreatedByType)
        {
            try
            {
                if (intUserID > 0)
                {
                    try
                    {
                        objCreateFile.CreateTextFile(strIPAddress, strMacAddress, strOSDetails, strIMEINo, strGcmID, strAPPVersion, intSource.ToString(), intCreatedByType, 999, " Exioms Support- sup_er_er_wallet_transfer_individual", strUserName);
                    }
                    catch (Exception)
                    {

                    }
                    finally
                    {
                        strLimit = " LIMIT " + PageSize * (intPage - 1) + "," + PageSize + "";


                        int intCount = objODBCLOG.executeScalar_int("SELECT * FROM er_login l inner join er_wallet_transfer_individual m on l.userid=m.userid where 1=1 " + strSearch + "");

                        strQuery = "SELECT l.userid,l.username,l.emailid,id,m.id,m.trans_number,m.userid,j.username as ParentID,j.userid,m.transfer_amt,m.total_amt,m.created_on,m.type FROM er_login l inner join er_wallet_transfer_individual m on l.userid=m.userid inner join er_login j on m.parent_id=j.userid where 1=1 " + strSearch + " order by m.id desc " + strLimit + "";

                    }
                    ds = objODBCLOG.Report(strQuery);
                }
                return ds;
            }
            catch (Exception)
            {
                throw;

            }
            finally
            {
                ds.Dispose();
            }
        }


        /* Recharge Activity Details for All Transaction*/
        [WebMethod]
        public DataSet sup_er_reverse_fund(string strUserName, int intUserID, int usertype_id, int PageSize, int intPage, string strSearch, string strIPAddress, int intDesignationType, string strMacAddress, string strOSDetails, string strIMEINo, string strGcmID, string strAPPVersion, int intSource, int intCreatedByType)
        {
            try
            {
                if (intUserID > 0)
                {
                    try
                    {
                        objCreateFile.CreateTextFile(strIPAddress, strMacAddress, strOSDetails, strIMEINo, strGcmID, strAPPVersion, intSource.ToString(), intCreatedByType, 999, " Exioms Support- sup_er_reverse_fund", strUserName);
                    }
                    catch (Exception)
                    {

                    }
                    finally
                    {
                        strLimit = " LIMIT " + PageSize * (intPage - 1) + "," + PageSize + "";


                        int intCount = objODBCLOG.executeScalar_int("SELECT * FROM er_login l inner join er_wallet_transfer_individual m on l.userid=m.userid where 1=1 " + strSearch + "");

                        strQuery = "SELECT l.userid,l.username,l.emailid,id,m.id,m.trans_number,m.userid,m.parent_id,j.username as ParentID,m.total_amt,m.created_on,m.type FROM er_login l inner join ap_reverse_fund m on l.userid=m.userid inner join er_login j on m.parent_id=j.userid where 1=1 " + strSearch + " order by m.id desc " + strLimit + "";

                    }
                    ds = objODBCLOG.Report(strQuery);
                }
                return ds;
            }
            catch (Exception)
            {
                throw;

            }
            finally
            {
                ds.Dispose();
            }
        }


        /* Recharge Activity Details for All Commission Amount*/
        [WebMethod]
        public DataSet sup_er_All_CommissionAmt(string strUserName, int intUserID, int usertype_id, int PageSize, int intPage, string strSearch, string strIPAddress, int intDesignationType, string strMacAddress, string strOSDetails, string strIMEINo, string strGcmID, string strAPPVersion, int intSource, int intCreatedByType)
        {
            try
            {
                if (intUserID > 0)
                {
                    try
                    {
                        objCreateFile.CreateTextFile(strIPAddress, strMacAddress, strOSDetails, strIMEINo, strGcmID, strAPPVersion, intSource.ToString(), intCreatedByType, 999, " Exioms Support- sup_er_All_CommissionAmt", strUserName);
                    }
                    catch (Exception)
                    {

                    }
                    finally
                    {
                        strLimit = " LIMIT " + PageSize * (intPage - 1) + "," + PageSize + "";


                        int intCount = objODBCLOG.executeScalar_int("SELECT * FROM er_login l inner join er_commission_amount m on l.userid=m.userid where 1=1 " + strSearch + "");

                        strQuery = "SELECT l.userid,l.username,l.emailid,id,m.id,m.userid,m.parent_id,j.username as ParentID,m.recharge_id,m.operator_id,m.recharge_amount,m.commission_amount,m.comm_per,m.created_on,m.status FROM er_login l inner join er_commission_amount m on l.userid=m.userid inner join er_login j on m.parent_id=j.userid where 1=1 " + strSearch + " order by m.id desc " + strLimit + "";

                    }
                    ds = objODBCLOG.Report(strQuery);
                }
                return ds;
            }
            catch (Exception)
            {
                throw;

            }
            finally
            {
                ds.Dispose();
            }
        }


        /* Recharge Activity Details for AllE Money Transaction*/
        [WebMethod]
        public DataSet sup_er_All_EmoneyTransaction(string strUserName, int intUserID, int usertype_id, int PageSize, int intPage, string strSearch, string strIPAddress, int intDesignationType, string strMacAddress, string strOSDetails, string strIMEINo, string strGcmID, string strAPPVersion, int intSource, int intCreatedByType)
        {
            try
            {
                if (intUserID > 0)
                {
                    try
                    {
                        objCreateFile.CreateTextFile(strIPAddress, strMacAddress, strOSDetails, strIMEINo, strGcmID, strAPPVersion, intSource.ToString(), intCreatedByType, 999, " Exioms Support- sup_er_All_E_Money_Transaction", strUserName);
                    }
                    catch (Exception)
                    {

                    }
                    finally
                    {
                        strLimit = " LIMIT " + PageSize * (intPage - 1) + "," + PageSize + "";


                        int intCount = objODBCLOG.executeScalar_int("SELECT * FROM er_emoney_transaction where 1=1 " + strSearch + "");

                        strQuery = "SELECT `id`, `recharge_id`, `wallet_id`, `amt`, `created_on`, `admin_id`, `closing_balance`, `trans_type` FROM `er_emoney_transaction` where 1=1 " + strSearch + " order by id desc " + strLimit + "";

                    }
                    ds = objODBCLOG.Report(strQuery);
                }
                return ds;
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                ds.Dispose();
            }
        }

        /* Recharge Activity Details for AllE Money Transaction*/
        [WebMethod]
        public DataSet sup_er_All_CheckAllTransaction_EmoneyTransaction(string strUserName, int intUserID, int usertype_id, int PageSize, int intPage, string strSearch, string strIPAddress, int intDesignationType, string strMacAddress, string strOSDetails, string strIMEINo, string strGcmID, string strAPPVersion, int intSource, int intCreatedByType)
        {
            try
            {
                if (intUserID > 0)
                {
                    try
                    {
                        objCreateFile.CreateTextFile(strIPAddress, strMacAddress, strOSDetails, strIMEINo, strGcmID, strAPPVersion, intSource.ToString(), intCreatedByType, 999, " Exioms Support- sup_er_All_CheckAllTransaction_EmoneyTransaction", strUserName);
                    }
                    catch (Exception)
                    {

                    }
                    finally
                    {
                        strLimit = " LIMIT " + PageSize * (intPage - 1) + "," + PageSize + "";


                        int intCount = objODBCLOG.executeScalar_int("SELECT * FROM er_check_transaction_current where 1=1 " + strSearch + "");

                        strQuery = "SELECT m.id,m.userid,m.trans_number,m.idlower,m.idupper,m.blnclower,m.blncupper,m.expectedupperblnc,m.amount,m.amt_differ,m.trans_datetime,m.created_on FROM `er_check_transaction_current` m where 1=1 " + strSearch + " order by id desc " + strLimit + "";

                    }
                    ds = objODBCLOG.Report(strQuery);
                }
                return ds;
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                ds.Dispose();
            }
        }



        /* Recharge Activity Details for Notification*/

        [WebMethod]
        public DataSet sup_All_er_Notification(string strUserName, int intUserID, int usertype_id, int PageSize, int intPage, string strSearch, string strIPAddress, int intDesignationType, string strMacAddress, string strOSDetails, string strIMEINo, string strGcmID, string strAPPVersion, int intSource, int intCreatedByType)
        {
            try
            {
                if (intUserID > 0)
                {
                    try
                    {
                        objCreateFile.CreateTextFile(strIPAddress, strMacAddress, strOSDetails, strIMEINo, strGcmID, strAPPVersion, intSource.ToString(), intCreatedByType, 999, " Exioms Support- sup_All_er_Notification", strUserName);
                    }
                    catch (Exception)
                    {

                    }
                    finally
                    {
                        strLimit = " LIMIT " + PageSize * (intPage - 1) + "," + PageSize + "";


                        int intCount = objODBCLOG.executeScalar_int("SELECT * FROM er_login l inner join er_notification m on l.userid=m.userid where 1=1 " + strSearch + "");

                        strQuery = "SELECT l.userid,l.username,l.emailid,m.id,m.userid,m.title,m.description,m.created_on,m.read_status,m.user_type FROM er_login l inner join er_notification m on l.userid=m.userid where 1=1 " + strSearch + " order by m.id desc " + strLimit + "";

                    }
                    ds = objODBCLOG.Report(strQuery);
                }
                return ds;
            }
            catch (Exception)
            {
                throw;

            }
            finally
            {
                ds.Dispose();
            }
        }
    }
}
