﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using JulMar.Smpp.Esme;
using JulMar.Smpp;
using JulMar.Smpp.Pdu;
using JulMar.Smpp.Elements;
using System.Configuration;
using System.Timers;
using JulMar.Smpp.Utility;
using System.Web.Services.Protocols;

namespace Etopup_Ws
{
    /// <summary>
    /// Summary description for Etisalat_SMPP
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class Etisalat_SMPP : System.Web.Services.WebService
    {

        public enum SmscConnectionStatus
        {
            Disconnected = 0,
            Connected,
            Binding,
            Bound
        };

        public static string strResult = String.Empty;
        public EsmeSession smppSession_ = null;
        public static SmscConnectionStatus smscConnectionStatus_ = SmscConnectionStatus.Disconnected;
        public string smscAddress_;
        public int smscPort_;
        public string smscSystemId_;
        public string smscPassword_;
        public string smscSystemType_;
        public string submitServiceType_;
        public int enquireLinkSeconds_;
        public string destinationAdrPrefix_;
        public int submitResponseMessageIdBase_ = 10;
        public string steResult = string.Empty;
        public System.Timers.Timer enquireLinkTimer;
        public static string connstatus = String.Empty;

       

        public string ConnectToSmsc()
        {
            try
            {
                smppSession_.Connect(smscAddress_, smscPort_);
                return "True";
            }
            catch (Exception ex)
            {
               
                return "Exception trying to connect to Smsc: " + ex.ToString();

            }
        }

       
        // Called when the connection to SMSC is established for transmitter
        public void OnSessionConnected(object sender, SmppEventArgs args)
        {
            // Send Bind PDU to SMSC asynchronously
            bind_transmitter bindPdu = new bind_transmitter(smscSystemId_, smscPassword_, "",
                                                            new interface_version(),    // Default is version 3.4
                                                            new address_range());
            bindPdu.SystemType = smscSystemType_;
            smppSession_.BeginBindTransmitter(bindPdu, new AsyncCallback(BindTransmitterCallback));
          
        }


        //// Called when the connection to SMSC is established
        /* private void OnSessionConnected(object sender, SmppEventArgs args)
         {
             // Update the SMSC connection status and related controls
             SetSmscConnectionStatus(SmscConnectionStatus.Connected);

             // Send Bind PDU to SMSC asynchronously
             bind_transceiver bindPdu = new bind_transceiver(smscSystemId_, smscPassword_, "",
                                                             new interface_version(),	// Default is version 3.4
                                                             new address_range());
             bindPdu.SystemType = smscSystemType_;
             smppSession_.BeginBindTransceiver(bindPdu, new AsyncCallback(BindTransceiverCallback));
             SetSmscConnectionStatus(SmscConnectionStatus.Bound);
         }*/

      
        // Used to catch and display binding issues
        public void BindTransmitterCallback(IAsyncResult ar)
        {
            // Process the bind result
            EsmeSession session = (EsmeSession)ar.AsyncState;
            bind_transmitter_resp bindResp = session.EndBindTransmitter(ar);
            if (bindResp.Status != StatusCodes.ESME_ROK)
            {
                strResult = "Error binding to SMSC: " + bindResp.Status.ToString();
                //                Close();
            }
        }

        public void OnSessionBound(object sender, SmppEventArgs args)
        {
            // Set the session to bound
            // If the enquireLinkSeconds_ is non-zero then create a timer to send enquire link PDU's
          
            if (enquireLinkSeconds_ != 0)
            {
                enquireLinkTimer = new System.Timers.Timer((double)enquireLinkSeconds_ * 1000);
                enquireLinkTimer.Elapsed += new ElapsedEventHandler(OnEnquireLinkElapsedTimer);
                enquireLinkTimer.Enabled = true;
            }
        }

        // Used to send Enquire Link messages to SMSC every enquireLinkSeconds_ seconds
        public void OnEnquireLinkElapsedTimer(object source, ElapsedEventArgs e)
        {
            // Send if our session is still connected/bound
            if ((smppSession_.IsConnected) && (smppSession_.IsBound))
            {
                enquire_link enquirePdu = new enquire_link();
                smppSession_.BeginEnquireLink(enquirePdu, new AsyncCallback(EnquireLinkCallback));
            }
        }

        public void EnquireLinkCallback(IAsyncResult ar)
        {
            // Process the enquire link result
            EsmeSession session = (EsmeSession)ar.AsyncState;
            enquire_link_resp enquireResp = session.EndEnquireLink(ar);
            if (enquireResp.Status != StatusCodes.ESME_ROK)
            {
                strResult = "Error sending enquire link to SMSC: " + enquireResp.Status.ToString();
            }
        }

      
        public void OnSessionDisconnected(object sender, SmppEventArgs args)
        {
            SmppDisconnectEventArgs dea = (SmppDisconnectEventArgs)args;
            if (dea.Exception != null)
                strResult = "Socket error: " + ((dea.Exception.Message != null) ? dea.Exception.Message : dea.Exception.ToString());
            else
                strResult = "Smsc Session/Connection was dropped";
             
            
        }
        [WebMethod]
        public string set_data(string strTarget, string strMessage)
        {
            string strSource = "93543";

            // Get the default settings from our configuration file 
            smscAddress_ ="196.195.250.205";
            smscPort_ =5016;
            smscSystemId_ = "S_MUTAHED";
            smscPassword_ ="s@m1!#";
            smscSystemType_ = "";
            submitServiceType_ ="";
            enquireLinkSeconds_ =  30;
            destinationAdrPrefix_ = "1";
            submitResponseMessageIdBase_ = 10;

            // Create the ESME Smpp session - default version is 3.4
            smppSession_ = new EsmeSession(smscSystemId_);
            smppSession_.SmppVersion = SmppVersion.SMPP_V34;

            // Hook up all events we need
            smppSession_.OnSessionConnected += OnSessionConnected;
            smppSession_.OnSessionDisconnected += OnSessionDisconnected;
            smppSession_.OnBound += OnSessionBound;
            ConnectToSmsc();
          
            strResult = connstatus;

            // Confirm all settings
            string sourceNumber = strSource.Trim();
            string rawTargetNumber = strTarget.Trim();
            string message = strMessage.Trim();
            if ((sourceNumber.Length < 5) || (rawTargetNumber.Length < 9) || (message.Length == 0))
            {
                return "Either a number is invalid or the message is blank";

            }
            // Add target address prefix
            string targetNumber = destinationAdrPrefix_ + rawTargetNumber;

      
            submit_sm submitPdu = new submit_sm();
            if (!string.IsNullOrEmpty(submitServiceType_))
                submitPdu.ServiceType = submitServiceType_;
            submitPdu.SourceAddress = new address(TypeOfNumber.UNKNOWN, NumericPlanIndicator.UNKNOWN, sourceNumber);
            submitPdu.DestinationAddress = new address(TypeOfNumber.UNKNOWN, NumericPlanIndicator.UNKNOWN, targetNumber);
            submitPdu.RegisteredDelivery = new registered_delivery(DeliveryReceiptType.FINAL_DELIVERY_RECEIPT, AcknowledgementType.DELIVERY_USER_ACK_REQUEST, true);
            // submitPdu.RegisteredDelivery = new registered_delivery(DeliveryReceiptType.FINAL_DELIVERY_RECEIPT, AcknowledgementType.USER_ACK_REQUEST, true);
            submitPdu.Message = message;
            smppSession_.BeginSubmitSm(submitPdu, new AsyncCallback(SubmitSmCallback));
          
            return strResult;

        }

        public delegate void SubmitSmCallbackHandler(IAsyncResult ar);
        public void SubmitSmCallback(IAsyncResult ar)
        {
            SubmitSmCallbackHandler objdele = new SubmitSmCallbackHandler(SubmitSmCallback);
            objdele.BeginInvoke(ar,SubmitSmCallback,objdele);
            EsmeSession session = (EsmeSession)ar.AsyncState;
                submit_sm_resp submitResp = session.EndSubmitSm(ar);
                return;
           
            // Process the send/submit result
          
        }

    }
}
