﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Etopup_Ws.old_App_Code;
using System.Web.Services;
using System.Collections;
using System.Data;

namespace Etopup_Ws
{
    /// <summary>
    /// Summary description for commission
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    [System.Web.Script.Services.ScriptService]
    public class commission : System.Web.Services.WebService
    {
        ODBC objODBC = new ODBC();
        ODBC_LOG objODBCLOG = new ODBC_LOG();
        CreateFile objCreateFile = new CreateFile();
        DataTable dt = new DataTable();
        DateFormat objDF = new DateFormat();
        CommonFunction objCommonFunction = new CommonFunction();
        CommonFunction objcf = new CommonFunction();
        DataSet ds = new DataSet();

        // Activity Type=5

        [WebMethod]
        public ArrayList Create_group_package_distributer(string strUserName, int intParentID, string strPackageName, double dblCommOP1, double dblCommOP2, double dblCommOP3, double dblCommOP4, double dblCommOP5, double dblCommOP1_1, double dblCommOP2_1, double dblCommOP3_1, double dblCommOP4_1, double dblCommOP5_1, double dblCommOP1_2, double dblCommOP2_2, double dblCommOP3_2, double dblCommOP4_2, double dblCommOP5_2, string strIPAddress, int intDesignationType, string strMacAddress, string strOSDetails, string strIMEINo, string strGcmID, string strAPPVersion, int intSource, int intCreatedByType)
        {
            string strDate = objDF.getCurDateTimeString();
            ArrayList arrdata = new ArrayList();
            string strQuery = string.Empty;
            string strDistributor = "Distributor";
            string strSubDistributor = "SubDistributor";
            string strRetailer = "Retailer";
            string strPackName = strPackageName;
            strPackageName = strPackageName + " " + strDistributor;
            int intchkCount = objODBC.executeScalar_int("select count(1) from er_slab_group where name = '" + strPackName + "'");
            if (intchkCount == 0)
            {
                strQuery = "INSERT INTO `er_slab_group`(`name`, `slab_type`, `created_on`) VALUES ('" + strPackName + "',1,'" + strDate + "')";
                objODBC.executeNonQuery(strQuery);

                int intLastID = objODBC.executeScalar_int("select id from er_slab_group where name='" + strPackName + "' and slab_type=1 order by id desc limit 1");

                int int_parent_SlabId = objODBC.executeScalar_int("SELECT slab_id FROM er_slab_user WHERE userid='" + intParentID + "'");

                objODBC.executeNonQuery("INSERT into er_commission_slab (parent_id, slab_name, commission_per, surcharge_amt, created_on, parent_slab_id,group_id) VALUES('" + intParentID + "','" + strPackageName + "', 100, 0, '" + objDF.getCurDateTimeString().ToString() + "', '" + int_parent_SlabId + "'," + intLastID + ")");

                int intCommSlabID = objODBC.executeScalar_int("select id from er_commission_slab where parent_id='" + intParentID + "' and slab_name='" + strPackageName + "' and parent_slab_id='" + int_parent_SlabId + "'");

                objODBC.executeNonQuery("INSERT INTO er_commission_structure(parent_id,slab_id,operator_id,commission_per,target_amt,target_reward,target_status,api_type,group_id) VALUES('" + intParentID + "','" + intCommSlabID + "',1,'" + dblCommOP1 + "','0','0','0',1," + intLastID + "),('" + intParentID + "','" + intCommSlabID + "',2,'" + dblCommOP2 + "','0','0','0',2," + intLastID + "),('" + intParentID + "','" + intCommSlabID + "',3,'" + dblCommOP3 + "','0','0','0',3," + intLastID + "),('" + intParentID + "','" + intCommSlabID + "',4,'" + dblCommOP4 + "','0','0','0',6," + intLastID + "),('" + intParentID + "','" + intCommSlabID + "',5,'" + dblCommOP5 + "','0','0','0',5," + intLastID + ")");

                objODBC.executeNonQuery("call update_slab_tree('" + int_parent_SlabId + "' ,'" + intCommSlabID + "')");

                objODBC.executeNonQuery(" call `update_er_referral_slab_user`('" + intParentID + "' ,'" + intCommSlabID + "')");

                // --------------------------------------------------------

                strPackageName = "";
                strPackageName = strPackName + " " + strSubDistributor;

                objODBC.executeNonQuery("INSERT into er_commission_slab (parent_id, slab_name, commission_per, surcharge_amt, created_on, parent_slab_id,group_id) VALUES('" + intParentID + "','" + strPackageName + "', 100, 0, '" + objDF.getCurDateTimeString().ToString() + "', '" + intCommSlabID + "'," + intLastID + ")");

                int intCommSlabID1 = objODBC.executeScalar_int("select id from er_commission_slab where parent_id='" + intParentID + "' and slab_name='" + strPackageName + "' and parent_slab_id='" + intCommSlabID + "'");

                objODBC.executeNonQuery("INSERT INTO er_commission_structure(parent_id,slab_id,operator_id,commission_per,target_amt,target_reward,target_status,api_type,group_id) VALUES('" + intParentID + "','" + intCommSlabID1 + "',1,'" + dblCommOP1_1 + "','0','0','0',1," + intLastID + "),('" + intParentID + "','" + intCommSlabID1 + "',2,'" + dblCommOP2_1 + "','0','0','0',2," + intLastID + "),('" + intParentID + "','" + intCommSlabID1 + "',3,'" + dblCommOP3_1 + "','0','0','0',3," + intLastID + "),('" + intParentID + "','" + intCommSlabID1 + "',4,'" + dblCommOP4_1 + "','0','0','0',6," + intLastID + "),('" + intParentID + "','" + intCommSlabID1 + "',5,'" + dblCommOP5_1 + "','0','0','0',5," + intLastID + ")");

                objODBC.executeNonQuery("call update_slab_tree('" + intCommSlabID + "' ,'" + intCommSlabID1 + "')");

                objODBC.executeNonQuery(" call `update_er_referral_slab_user`('" + intParentID + "' ,'" + intCommSlabID1 + "')");


                // --------------------------------------------------------------------------


                strPackageName = "";
                strPackageName = strPackName + " " + strRetailer;

                objODBC.executeNonQuery("INSERT into er_commission_slab (parent_id, slab_name, commission_per, surcharge_amt, created_on, parent_slab_id,group_id) VALUES('" + intParentID + "','" + strPackageName + "', 100, 0, '" + objDF.getCurDateTimeString().ToString() + "', '" + intCommSlabID1 + "'," + intLastID + ")");

                int intCommSlabID2 = objODBC.executeScalar_int("select id from er_commission_slab where parent_id='" + intParentID + "' and slab_name='" + strPackageName + "' and parent_slab_id='" + intCommSlabID1 + "'");

                objODBC.executeNonQuery("INSERT INTO er_commission_structure(parent_id,slab_id,operator_id,commission_per,target_amt,target_reward,target_status,api_type,group_id) VALUES('" + intParentID + "','" + intCommSlabID2 + "',1,'" + dblCommOP1_2 + "','0','0','0',1," + intLastID + "),('" + intParentID + "','" + intCommSlabID2 + "',2,'" + dblCommOP2_2 + "','0','0','0',2," + intLastID + "),('" + intParentID + "','" + intCommSlabID2 + "',3,'" + dblCommOP3_2 + "','0','0','0',3," + intLastID + "),('" + intParentID + "','" + intCommSlabID2 + "',4,'" + dblCommOP4_2 + "','0','0','0',6," + intLastID + "),('" + intParentID + "','" + intCommSlabID2 + "',5,'" + dblCommOP5_2 + "','0','0','0',5," + intLastID + ")");

                objODBC.executeNonQuery("call update_slab_tree('" + intCommSlabID1 + "' ,'" + intCommSlabID2 + "')");

                objODBC.executeNonQuery(" call `update_er_referral_slab_user`('" + intParentID + "' ,'" + intCommSlabID2 + "')");

                objODBCLOG.executeNonQuery("call activity_logs('" + intParentID + "','" + intDesignationType + "','" + intCreatedByType + "','" + strIPAddress + "','" + strMacAddress + "','" + strOSDetails + "','" + strIMEINo + "','" + strGcmID + "','" + strAPPVersion + "','" + intSource + "','Create group package distributor ',5,'','')");

                objCreateFile.CreateTextFile(strIPAddress, strMacAddress, strOSDetails, strIMEINo, strGcmID, strAPPVersion, intSource.ToString(), intCreatedByType, 5, "Create group package distributor", strUserName);

                arrdata.Add("TRUE");
                arrdata.Add("Slab Created Successfully!");
                arrdata.Add(intCommSlabID);
            }
            else
            {
                arrdata.Add("TRUE");
                arrdata.Add("Slab already exists!");
            }
            return arrdata;
        }

        [WebMethod]
        public ArrayList Create_group_package_sub_distributer(string strUserName, int intParentID, string strPackageName, double dblCommOP1, double dblCommOP2, double dblCommOP3, double dblCommOP4, double dblCommOP5, double dblCommOP1_1, double dblCommOP2_1, double dblCommOP3_1, double dblCommOP4_1, double dblCommOP5_1, string strIPAddress, int intDesignationType, string strMacAddress, string strOSDetails, string strIMEINo, string strGcmID, string strAPPVersion, int intSource, int intCreatedByType)
        {
            string strDate = objDF.getCurDateTimeString();
            ArrayList arrdata = new ArrayList();
            string strQuery = string.Empty;
            string strSubDistributor = "SubDistributor";
            string strRetailer = "Retailer";
            string strPackName = strPackageName;
            strPackageName = strPackageName + " " + strSubDistributor;

            int intchkCount = objODBC.executeScalar_int("select count(1) from er_slab_group where name = '" + strPackName + "'");

            if (intchkCount == 0)
            {
                strQuery = "INSERT INTO `er_slab_group`(`name`, `slab_type`, `created_on`) VALUES ('" + strPackName + "',2,'" + strDate + "')";

                objODBC.executeNonQuery(strQuery);

                int intLastID = objODBC.executeScalar_int("select id from er_slab_group where name='" + strPackName + "' and slab_type=2 order by id desc limit 1");

                int int_parent_SlabId = objODBC.executeScalar_int("SELECT slab_id FROM er_slab_user WHERE userid='" + intParentID + "'");

                objODBC.executeNonQuery("INSERT into er_commission_slab (parent_id, slab_name, commission_per, surcharge_amt, created_on, parent_slab_id,group_id) VALUES('" + intParentID + "','" + strPackageName + "', 100, 0, '" + objDF.getCurDateTimeString().ToString() + "', '" + int_parent_SlabId + "'," + intLastID + ")");

                int intCommSlabID = objODBC.executeScalar_int("select id from er_commission_slab where parent_id='" + intParentID + "' and slab_name='" + strPackageName + "' and parent_slab_id='" + int_parent_SlabId + "'");

                objODBC.executeNonQuery("INSERT INTO er_commission_structure(parent_id,slab_id,operator_id,commission_per,target_amt,target_reward,target_status,api_type,group_id) VALUES('" + intParentID + "','" + intCommSlabID + "',1,'" + dblCommOP1 + "','0','0','0',1," + intLastID + "),('" + intParentID + "','" + intCommSlabID + "',2,'" + dblCommOP2 + "','0','0','0',2," + intLastID + "),('" + intParentID + "','" + intCommSlabID + "',3,'" + dblCommOP3 + "','0','0','0',3," + intLastID + "),('" + intParentID + "','" + intCommSlabID + "',4,'" + dblCommOP4 + "','0','0','0',6," + intLastID + "),('" + intParentID + "','" + intCommSlabID + "',5,'" + dblCommOP5 + "','0','0','0',5," + intLastID + ")");

                objODBC.executeNonQuery("call update_slab_tree('" + int_parent_SlabId + "' ,'" + intCommSlabID + "')");

                objODBC.executeNonQuery(" call `update_er_referral_slab_user`('" + intParentID + "' ,'" + intCommSlabID + "')");

                // --------------------------------------------------------
                strPackageName = "";
                strPackageName = strPackName + " " + strRetailer;

                objODBC.executeNonQuery("INSERT into er_commission_slab (parent_id, slab_name, commission_per, surcharge_amt, created_on, parent_slab_id,group_id) VALUES('" + intParentID + "','" + strPackageName + "', 100, 0, '" + objDF.getCurDateTimeString().ToString() + "', '" + intCommSlabID + "'," + intLastID + ")");

                int intCommSlabID1 = objODBC.executeScalar_int("select id from er_commission_slab where parent_id='" + intParentID + "' and slab_name='" + strPackageName + "' and parent_slab_id='" + intCommSlabID + "'");

                objODBC.executeNonQuery("INSERT INTO er_commission_structure(parent_id,slab_id,operator_id,commission_per,target_amt,target_reward,target_status,api_type,group_id) VALUES('" + intParentID + "','" + intCommSlabID1 + "',1,'" + dblCommOP1_1 + "','0','0','0',1," + intLastID + "),('" + intParentID + "','" + intCommSlabID1 + "',2,'" + dblCommOP2_1 + "','0','0','0',2," + intLastID + "),('" + intParentID + "','" + intCommSlabID1 + "',3,'" + dblCommOP3_1 + "','0','0','0',3," + intLastID + "),('" + intParentID + "','" + intCommSlabID1 + "',4,'" + dblCommOP4_1 + "','0','0','0',6," + intLastID + "),('" + intParentID + "','" + intCommSlabID1 + "',5,'" + dblCommOP5_1 + "','0','0','0',5," + intLastID + ")");

                objODBC.executeNonQuery("call update_slab_tree('" + intCommSlabID + "' ,'" + intCommSlabID1 + "')");

                objODBC.executeNonQuery(" call `update_er_referral_slab_user`('" + intParentID + "' ,'" + intCommSlabID1 + "')");


                objODBCLOG.executeNonQuery("call activity_logs('" + intParentID + "','" + intDesignationType + "','" + intCreatedByType + "','" + strIPAddress + "','" + strMacAddress + "','" + strOSDetails + "','" + strIMEINo + "','" + strGcmID + "','" + strAPPVersion + "','" + intSource + "','Create group package sub distributor ',5,'','')");

                objCreateFile.CreateTextFile(strIPAddress, strMacAddress, strOSDetails, strIMEINo, strGcmID, strAPPVersion, intSource.ToString(), intCreatedByType, 5, "Create group package sub distributor", strUserName);

                arrdata.Add("TRUE");
                arrdata.Add("Slab Created Successfully!");
                arrdata.Add(intCommSlabID);
            }
            else
            {
                arrdata.Add("False");
                arrdata.Add("Slab already exist!");

            }
            return arrdata;
        }

        [WebMethod]
        public ArrayList Create_package(string strUserName, int intParentID, string strPackageName, double dblCommOP1, double dblCommOP2, double dblCommOP3, double dblCommOP4, double dblCommOP5, double dblTargetAmtOP1, double dblTargetAmtOP2, double dblTargetAmtOP3, double dblTargetAmtOP4, double dblTargetAmtOP5, double dblTargetRewardOP1, double dblTargetRewardOP2, double dblTargetRewardOP3, double dblTargetRewardOP4, double dblTargetRewardOP5, int intTarStatusOP1, int intTarStatusOP2, int intTarStatusOP3, int intTarStatusOP4, int intTarStatusOP5, string strIPAddress, int intDesignationType, string strMacAddress, string strOSDetails, string strIMEINo, string strGcmID, string strAPPVersion, int intSource, int intCreatedByType)
        {
            ArrayList arrdata = new ArrayList();
            int intCommSlabID = 0;
            if ((intParentID != 0) && (strPackageName != "" || strPackageName != null))
            {
                int valid = objODBC.executeScalar_int("SELECT count(1) from  er_commission_slab where slab_name='" + strPackageName + "' and parent_id='" + intParentID + "'");
                if (valid == 0)
                {
                    int int_parent_SlabId = objODBC.executeScalar_int("SELECT slab_id FROM er_slab_user WHERE userid='" + intParentID + "'");

                    objODBC.executeNonQuery("INSERT into er_commission_slab (parent_id, slab_name, commission_per, surcharge_amt, created_on, parent_slab_id) VALUES('" + intParentID + "','" + strPackageName + "', 100, 0, '" + objDF.getCurDateTimeString().ToString() + "', '" + int_parent_SlabId + "')");

                    intCommSlabID = objODBC.executeScalar_int("select id from er_commission_slab where parent_id='" + intParentID + "' and slab_name='" + strPackageName + "' and parent_slab_id='" + int_parent_SlabId + "'");

                    objODBC.executeNonQuery("INSERT INTO er_commission_structure(parent_id,slab_id,operator_id,commission_per,target_amt,target_reward,target_status,api_type) VALUES('" + intParentID + "','" + intCommSlabID + "',1,'" + dblCommOP1 + "','" + dblTargetAmtOP1 + "','" + dblTargetRewardOP1 + "','" + intTarStatusOP1 + "',1),('" + intParentID + "','" + intCommSlabID + "',2,'" + dblCommOP2 + "','" + dblTargetAmtOP2 + "','" + dblTargetRewardOP2 + "','" + intTarStatusOP2 + "',2),('" + intParentID + "','" + intCommSlabID + "',3,'" + dblCommOP3 + "','" + dblTargetAmtOP3 + "','" + dblTargetRewardOP3 + "','" + intTarStatusOP3 + "',3),('" + intParentID + "','" + intCommSlabID + "',4,'" + dblCommOP4 + "','" + dblTargetAmtOP4 + "','" + dblTargetRewardOP4 + "','" + intTarStatusOP4 + "',6),('" + intParentID + "','" + intCommSlabID + "',5,'" + dblCommOP5 + "','" + dblTargetAmtOP5 + "','" + dblTargetRewardOP5 + "','" + intTarStatusOP5 + "',5)");

                    objODBC.executeNonQuery("call update_slab_tree('" + int_parent_SlabId + "' ,'" + intCommSlabID + "')");


                    objODBC.executeNonQuery(" call `update_er_referral_slab_user`('" + intParentID + "' ,'" + intCommSlabID + "')");

                    objODBCLOG.executeNonQuery("call activity_logs('" + intParentID + "','" + intDesignationType + "','" + intCreatedByType + "','" + strIPAddress + "','" + strMacAddress + "','" + strOSDetails + "','" + strIMEINo + "','" + strGcmID + "','" + strAPPVersion + "','" + intSource + "','Create Package For Commission ',5,'','')");

                    objCreateFile.CreateTextFile(strIPAddress, strMacAddress, strOSDetails, strIMEINo, strGcmID, strAPPVersion, intSource.ToString(), intCreatedByType, 5, "Package Created Successfully", strUserName);

                    arrdata.Add("TRUE");
                    arrdata.Add("Slab Created Successfully!");
                    arrdata.Add(intCommSlabID);

                    return arrdata;

                }
                else
                {
                    objCreateFile.CreateTextFile(strIPAddress, strMacAddress, strOSDetails, strIMEINo, strGcmID, strAPPVersion, intSource.ToString(), intCreatedByType, 5, "Slab already exist!", strUserName);
                    arrdata.Add("FALSE");
                    arrdata.Add("Slab already exist!");
                    arrdata.Add(intCommSlabID);
                    return arrdata;
                }
            }
            else
            {
                arrdata.Add("FALSE");
                arrdata.Add("Kindly Enter Valid Slab details !");
                arrdata.Add(intCommSlabID);
                return arrdata;
            }

        }

        // Activity Type=6
        [WebMethod]
        public ArrayList packageDetailsDropdown(string strUserName, int reseller_id, string strIPAddress, int intDesignationType, string strMacAddress, string strOSDetails, string strIMEINo, string strGcmID, string strAPPVersion, int intSource, int intCreatedByType)
        {
            ArrayList packages = new ArrayList();
            try
            {
                if (reseller_id != 0)
                {
                    packages = objCommonFunction.Get_ArrayLlist("SELECT id,slab_name as package_name FROM er_commission_slab WHERE parent_id=" + reseller_id);

                    objCreateFile.CreateTextFile(strIPAddress, strMacAddress, strOSDetails, strIMEINo, strGcmID, strAPPVersion, intSource.ToString(), intCreatedByType, 5, "Create Package Details for Dropdown fetch Sucessfully ", strUserName);
                }
                else
                {
                    objCreateFile.CreateTextFile(strIPAddress, strMacAddress, strOSDetails, strIMEINo, strGcmID, strAPPVersion, intSource.ToString(), intCreatedByType, 5, "Create Package Details for Dropdown Fail ", strUserName);
                    packages.Add("FALSE");
                }
            }
            catch (Exception ex)
            {
                packages.Add(ex.Message.ToString());
            }
            return packages;
        }

        // Activity Type=7
        [WebMethod]
        public ArrayList getPackageDetailsDistributor(string strUserName, int intParentID, int intSlabID, string strIPAddress, int intDesignationType, string strMacAddress, string strOSDetails, string strIMEINo, string strGcmID, string strAPPVersion, int intSource, int intCreatedByType)
        {
            ArrayList packages = new ArrayList();
            DataSet ds2 = new DataSet();
            try
            {
                if ((intParentID != 0) && (intSlabID != 0))
                {

                    int intSalaamStatus, intEtisalatStatus, intRoshanStatus, intMTNStatus, intAWCCstatus;
                    string strOperatorID = "0,";
                    try
                    {
                        ds2 = objODBC.getDataSet("select  `salaam_status`, `etisalat_status`,`roshan_status`, `mtn_status`, `awcc_status` FROM `er_login` WHERE userid=" + intParentID + "");
                        if (ds2.Tables[0].Rows.Count > 0)
                        {
                            intSalaamStatus = int.Parse(ds2.Tables[0].Rows[0][0].ToString());
                            if (intSalaamStatus == 1)
                            {
                                strOperatorID = strOperatorID + "'1',";
                            }
                            intEtisalatStatus = int.Parse(ds2.Tables[0].Rows[0][1].ToString());
                            if (intEtisalatStatus == 1)
                            {
                                strOperatorID = strOperatorID + "'2',";
                            }
                            intRoshanStatus = int.Parse(ds2.Tables[0].Rows[0][2].ToString());
                            if (intRoshanStatus == 1)
                            {
                                strOperatorID = strOperatorID + "'3',";
                            }
                            intMTNStatus = int.Parse(ds2.Tables[0].Rows[0][3].ToString());
                            if (intMTNStatus == 1)
                            {
                                strOperatorID = strOperatorID + "'4',";
                            }
                            intAWCCstatus = int.Parse(ds2.Tables[0].Rows[0][4].ToString());
                            if (intAWCCstatus == 1)
                            {
                                strOperatorID = strOperatorID + "'5'";
                            }

                        }
                    }
                    catch (Exception)
                    {

                    }
                    finally
                    {
                        ds2.Dispose();
                        strOperatorID = strOperatorID.TrimEnd(',');
                    }

                    var intResellerSlabid = objODBC.executeScalar_int("SELECT slab_id from er_slab_user where userid= " + intParentID);
                    if (intResellerSlabid > 0)
                    {
                        packages = objCommonFunction.Get_ArrayLlist("Select a.id, a.operator_id, a.commission_per as curr_commission_per, res.commission_per as res_commission_per, b.operator_name FROM er_commission_structure a, er_commission_structure res , er_recharge_operator_mobile b where a.operator_id=b.id  and a.parent_id=" + intParentID + " and a.slab_id=" + intSlabID + " and res.slab_id=" + intResellerSlabid + " and res.operator_id=b.id and a.operator_id in (" + strOperatorID + ")");

                        objCreateFile.CreateTextFile(strIPAddress, strMacAddress, strOSDetails, strIMEINo, strGcmID, strAPPVersion, intSource.ToString(), intCreatedByType, 7, "Get Package Details for Distributor", strUserName);

                    }

                    else
                    {
                        packages.Add("slab Not Available!");
                        objCreateFile.CreateTextFile(strIPAddress, strMacAddress, strOSDetails, strIMEINo, strGcmID, strAPPVersion, intSource.ToString(), intCreatedByType, 7, "slab Not Available!", strUserName);
                    }
                }
                else
                {
                    packages.Add("Kindly Enter valid details !");
                }
            }
            catch (Exception ex)
            {
                packages.Add(ex.Message.ToString());
            }

            return packages;
        }


        // Activity Type=8
        [WebMethod]
        public string getCommissionDetailsOperators(string strUserName, int intUserID, int intOpTypeID, int intOP_ID, string strIPAddress, int intDesignationType, string strMacAddress, string strOSDetails, string strIMEINo, string strGcmID, string strAPPVersion, int intSource, int intCreatedByType)
        {
            string str;
            try
            {
                if (((intUserID > 0)) && (intOpTypeID != 0) && (intOP_ID != 0))
                {
                    str = objODBC.executeScalar_str("call getCommissionForRecharge(" + intUserID + ", " + intOpTypeID + "," + intOP_ID + " );").ToString();



                    objCreateFile.CreateTextFile(strIPAddress, strMacAddress, strOSDetails, strIMEINo, strGcmID, strAPPVersion, intSource.ToString(), intCreatedByType, 8, "get Commission Details Operators", strUserName);
                    return str;
                }
                else
                {
                    return "Kindly Enter Valid Details";
                }
            }
            catch (Exception ex)
            {
                return ex.Message.ToString();
            }
        }

        // Activity Type=9
        [WebMethod]
        public string updatePackageCommissionSurcharge(string strUserName, int intResellerID, int intSlabID, int intOperatorID, double dblComm, double dblTraget, int intTragetstatus, double dblRewards, string strIPAddress, int intDesignationType, string strMacAddress, string strOSDetails, string strIMEINo, string strGcmID, string strAPPVersion, int intSource, int intCreatedByType)
        {
            try
            {
                // if ((intResellerID != 0) && (intSlabID != 0) && (intOperatorID != 0) )
                if ((intResellerID != 0) && (intSlabID != 0) && (intOperatorID != 0))
                {
                    int Val;
                    string str = "call updatePackageCommissionSurcharge(" + intResellerID + ", " + intSlabID + ", " + intOperatorID + ", " + dblComm + ", " + dblTraget + ", " + intTragetstatus + ", " + dblRewards + ");";

                    Val = objODBC.executeNonQuery(str);
                    try
                    {
                        objODBCLOG.executeNonQuery("call activity_logs('" + intResellerID + "','" + intDesignationType + "','" + intCreatedByType + "','" + strIPAddress + "','" + strMacAddress + "','" + strOSDetails + "','" + strIMEINo + "','" + strGcmID + "','" + strAPPVersion + "','" + intSource + "','Modify Package Commission',9,'','')");
                        objCreateFile.CreateTextFile(strIPAddress, strMacAddress, strOSDetails, strIMEINo, strGcmID, strAPPVersion, intSource.ToString(), intCreatedByType, 9, "update Package Commission Surcharge Sucessfully", strUserName);
                    }
                    catch (Exception)
                    {

                    }
                    return "Commission / Surcharge set Successfully!";
                }
                else
                {
                    objCreateFile.CreateTextFile(strIPAddress, strMacAddress, strOSDetails, strIMEINo, strGcmID, strAPPVersion, intSource.ToString(), intCreatedByType, 9, "update Package Commission Surcharge Fail", strUserName);
                    return "Kindly Enter Valid Details";
                }
            }
            catch (Exception ex)
            {
                return "Invalid Details";
            }
        }

        [WebMethod]
        public string updatePackageCommissionSurchargeAdmin(string strUserName, int intResellerID, int intSlabID, int intOperatorID, double dblComm, double dblTraget, int intTragetstatus, double dblRewards, string strIPAddress, int intDesignationType, string strMacAddress, string strOSDetails, string strIMEINo, string strGcmID, string strAPPVersion, int intSource, int intCreatedByType)
        {
            try
            {
                //   if ((intResellerID == 1) && (intSlabID == 1) && (intOperatorID != 0) && (dblComm != 0))
                if ((intResellerID == 1) && (intSlabID == 1) && (intOperatorID != 0) && (dblComm != 0))
                {
                    int Val;
                    //string str = "call updatePackageCommissionSurcharge(" + intResellerID + ", " + intSlabID + ", " + intOperatorID + ", " + dblComm + ", " + dblTraget + ", " + intTragetstatus + ", " + dblRewards + ");";
                    string str = "UPDATE er_commission_structure SET commission_per=" + dblComm + " WHERE slab_id=1 and operator_id=" + intOperatorID;
                    Val = objODBC.executeNonQuery(str);

                    objODBCLOG.executeNonQuery("call activity_logs('" + intResellerID + "','" + intDesignationType + "','" + intCreatedByType + "','" + strIPAddress + "','" + strMacAddress + "','" + strOSDetails + "','" + strIMEINo + "','" + strGcmID + "','" + strAPPVersion + "','" + intSource + "','Modify Package Commission by Admin',9,'','')");

                    objCreateFile.CreateTextFile(strIPAddress, strMacAddress, strOSDetails, strIMEINo, strGcmID, strAPPVersion, intSource.ToString(), intCreatedByType, 9, "update Package Commission Surcharge Admin Sucessfully", strUserName);
                    return "Commission / Surcharge set Successfully!";
                }
                else
                {
                    objCreateFile.CreateTextFile(strIPAddress, strMacAddress, strOSDetails, strIMEINo, strGcmID, strAPPVersion, intSource.ToString(), intCreatedByType, 9, "update Package Commission Surcharge Fail", strUserName);
                    return "Kindly Enter Valid Details";
                }
            }
            catch (Exception ex)
            {
                return "Invalid Details";
            }
        }

        // Activity Type=10
        [WebMethod]
        public ArrayList getDefaultSlabs(string strUserName, int intResellerID, string strIPAddress, int intDesignationType, string strMacAddress, string strOSDetails, string strIMEINo, string strGcmID, string strAPPVersion, int intSource, int intCreatedByType)
        {
            ArrayList array_defaultSlabs = new ArrayList();
            try
            {
                if (intResellerID != 0)
                {
                    array_defaultSlabs = objCommonFunction.Get_ArrayLlist("SELECT res_slab, md_slab, d_slab, ret_slab, api_slab from er_default_slab where parent_id=" + intResellerID);



                    objCreateFile.CreateTextFile(strIPAddress, strMacAddress, strOSDetails, strIMEINo, strGcmID, strAPPVersion, intSource.ToString(), intCreatedByType, 10, "get Default Slabs Sucessfully", strUserName);

                    return array_defaultSlabs;
                }
                else
                {

                    objCreateFile.CreateTextFile(strIPAddress, strMacAddress, strOSDetails, strIMEINo, strGcmID, strAPPVersion, intSource.ToString(), intCreatedByType, 10, "get Default Slabs Sucessfully", strUserName);

                    array_defaultSlabs.Add("Kindly Enter Valid Details");
                    return array_defaultSlabs;
                }
            }
            catch (Exception ex)
            {
                array_defaultSlabs.Add(ex.Message.ToString());
                return array_defaultSlabs;
            }
        }


        // Activity Type=11
        [WebMethod]
        public string setDefaultSlabs(string strUserName, int intResellerID, int intUserType, int intNewSlabId, string strIPAddress, int intDesignationType, string strMacAddress, string strOSDetails, string strIMEINo, string strGcmID, string strAPPVersion, int intSource, int intCreatedByType)
        {
            string strquery = "";
            string returnStr = "";
            int val = 0;
            try
            {
                if ((intResellerID != 0) && (intUserType != 0) && (intNewSlabId != 0))
                {
                    switch (intUserType)
                    {
                        case 1:
                            {
                                strquery = "Update er_default_slab set res_slab=" + intNewSlabId + " where parent_id=" + intResellerID;
                                break;
                            }

                        case 3:
                            {
                                strquery = "Update er_default_slab set md_slab=" + intNewSlabId + " where parent_id=" + intResellerID;
                                break;
                            }

                        case 4:
                            {
                                strquery = "Update er_default_slab set d_slab=" + intNewSlabId + " where parent_id=" + intResellerID;
                                break;
                            }

                        case 5:
                            {
                                strquery = "Update er_default_slab set ret_slab=" + intNewSlabId + " where parent_id=" + intResellerID;
                                break;
                            }

                        case 6:
                            {
                                strquery = "Update er_default_slab set api_slab=" + intNewSlabId + " where parent_id=" + intResellerID;
                                break;
                            }
                    }
                    if (intUserType < 6)
                    {
                        val = objODBC.executeNonQuery(strquery);
                    }
                    if (val == 1)
                    {
                        objODBCLOG.executeNonQuery("call activity_logs('" + intResellerID + "','" + intDesignationType + "','" + intCreatedByType + "','" + strIPAddress + "','" + strMacAddress + "','" + strOSDetails + "','" + strIMEINo + "','" + strGcmID + "','" + strAPPVersion + "','" + intSource + "','Set Default Slabs',11,'','')");

                        objCreateFile.CreateTextFile(strIPAddress, strMacAddress, strOSDetails, strIMEINo, strGcmID, strAPPVersion, intSource.ToString(), intCreatedByType, 11, "set Default Slabsset Default Slabs", strUserName);
                        returnStr = "Default Slab Set Sucessfully!";
                        return returnStr;
                    }
                    else
                    {

                        objCreateFile.CreateTextFile(strIPAddress, strMacAddress, strOSDetails, strIMEINo, strGcmID, strAPPVersion, intSource.ToString(), intCreatedByType, 11, "Default Packages Set Fail", strUserName);
                        return "Kindly Select Slab !";
                    }
                }
                else
                {

                    return "Kindly Enter Valid Information !";
                }

            }
            catch (Exception ex)
            {
                returnStr = ex.Message.ToString();
                return returnStr;
            }
        }

        // Activity Type=12
        [WebMethod]
        public int updateCommissionDistributionUpward(string strUserName, int intUserID, int intOperator_ID, double dblAmt, string strRechargeID, string strRechargeTransNo, string strMobile, string strIPAddress, int intDesignationType, string strMacAddress, string strOSDetails, string strIMEINo, string strGcmID, string strAPPVersion, int intSource, int intCreatedByType)
        {
            if (((intUserID > 0)) && (strUserName != "" || strUserName != null) && (intOperator_ID != 0) && (dblAmt != 0) && (strRechargeID != "" || strRechargeID != null) && (strMobile != "" || strMobile != null))
            {

                int intCount = 0;
                intCount = objODBC.executeNonQuery("CALL Update_Commission_amount( " + intUserID + ", '" + intOperator_ID + "' ," + dblAmt + ", '" + strRechargeID + "')");

                objODBCLOG.executeNonQuery("call activity_logs('" + intUserID + "','" + intDesignationType + "','" + intCreatedByType + "','" + strIPAddress + "','" + strMacAddress + "','" + strOSDetails + "','" + strIMEINo + "','" + strGcmID + "','" + strAPPVersion + "','" + intSource + "','Modify Commission Distribution Upward',12,'','')");

                objCreateFile.CreateTextFile(strIPAddress, strMacAddress, strOSDetails, strIMEINo, strGcmID, strAPPVersion, intSource.ToString(), intCreatedByType, 12, "update Commission Distribution Upward", strUserName);

                if (intCount == 1)
                {
                    return 1;
                }
                else
                {
                    return 0;
                }
            }
            else
            {
                return 0;
            }
        }

        // Activity Type=13
        [WebMethod]
        public int updateCommissionDistributionUpwardSelf(string strUserName, int intUserID, int intOperator_ID, double dblAmt, string strRechargeID, string strIPAddress, int intDesignationType, string strMacAddress, string strOSDetails, string strIMEINo, string strGcmID, string strAPPVersion, int intSource, int intCreatedByType)
        {
            int intCount = 0;
            if (((intUserID > 0)) && (strRechargeID != "" || strRechargeID != null) && (intOperator_ID != 0) && (dblAmt != 0))
            {
                intCount = objODBC.executeNonQuery("CALL Update_Commission_amount_self( " + intUserID + ", '" + intOperator_ID + "' ," + dblAmt + ", '" + strRechargeID + "')");

                objODBCLOG.executeNonQuery("call activity_logs('" + intUserID + "','" + intDesignationType + "','" + intCreatedByType + "','" + strIPAddress + "','" + strMacAddress + "','" + strOSDetails + "','" + strIMEINo + "','" + strGcmID + "','" + strAPPVersion + "','" + intSource + "','Modify Self Commission Distribution',13,'','')");

                objCreateFile.CreateTextFile(strIPAddress, strMacAddress, strOSDetails, strIMEINo, strGcmID, strAPPVersion, intSource.ToString(), intCreatedByType, 13, "Update Commission Distribution for Upward Self", strUserName);

                if (intCount == 1)
                    return 1;
                else
                    return 0;
            }
            else
            {
                return 0;
            }
        }

        // Activity Type=14
        [WebMethod]
        public int Update_Commission_amount_Upward(string strUserName, int intUserID, int intOperator_ID, double dblAmt, string strRechargeID, string strIPAddress, int intDesignationType, string strMacAddress, string strOSDetails, string strIMEINo, string strGcmID, string strAPPVersion, int intSource, int intCreatedByType)
        {
            int intCount = 0;
            if (((intUserID > 0)) && (strRechargeID != "" || strRechargeID != null) && (intOperator_ID != 0) && (dblAmt != 0))
            {
                intCount = objODBC.executeNonQuery("CALL Update_Commission_amount_Upward( " + intUserID + ", '" + intOperator_ID + "' ," + dblAmt + ", '" + strRechargeID + "')");

                objODBCLOG.executeNonQuery("call activity_logs('" + intUserID + "','" + intDesignationType + "','" + intCreatedByType + "','" + strIPAddress + "','" + strMacAddress + "','" + strOSDetails + "','" + strIMEINo + "','" + strGcmID + "','" + strAPPVersion + "','" + intSource + "','Modify Commission Amount Upward',14,'','')");

                objCreateFile.CreateTextFile(strIPAddress, strMacAddress, strOSDetails, strIMEINo, strGcmID, strAPPVersion, intSource.ToString(), intCreatedByType, 14, "Update Commission Amount Upward", strUserName);

                if (intCount == 1)
                    return 1;
                else
                    return 0;
            }
            else
                return 0;
        }

        // Activity Type=15
        [WebMethod]
        public string UpdateSlabForUsers(string strUserName, int intResellerID, int intUserID, int intSlabID, string strIPAddress, int intDesignationType, string strMacAddress, string strOSDetails, string strIMEINo, string strGcmID, string strAPPVersion, int intSource, int intCreatedByType)
        {
            string strMsg;
            if (((intUserID > 0)) && (intResellerID != 0) && (intSlabID != 0))
            {

                int intUserCount = objODBC.executeScalar_int("SELECT Count(1) From er_login Where userid=" + intUserID);

                if (intUserCount == 1)
                {
                    int intSlabCount = objODBC.executeScalar_int("SELECT Count(1) From er_commission_slab Where id=" + intSlabID + " and parent_id=" + intResellerID);

                    if (intSlabCount == 1)
                    {
                        objODBC.executeNonQuery("UPDATE er_slab_user SET slab_id=" + intSlabID + " WHERE userid=" + intUserID);

                        objODBCLOG.executeNonQuery("call activity_logs('" + intUserID + "','" + intDesignationType + "','" + intCreatedByType + "','" + strIPAddress + "','" + strMacAddress + "','" + strOSDetails + "','" + strIMEINo + "','" + strGcmID + "','" + strAPPVersion + "','" + intSource + "','Modify Slab For Users',15,'','')");

                        objCreateFile.CreateTextFile(strIPAddress, strMacAddress, strOSDetails, strIMEINo, strGcmID, strAPPVersion, intSource.ToString(), intCreatedByType, 15, "Update Slab For Users", strUserName);

                        strMsg = "Slab Successfully Assign !";
                    }
                    else
                    {

                        objCreateFile.CreateTextFile(strIPAddress, strMacAddress, strOSDetails, strIMEINo, strGcmID, strAPPVersion, intSource.ToString(), intCreatedByType, 15, "You are not authorize to assign this slab ", strUserName);
                        strMsg = "You are not authorize to assign this slab !";
                    }
                }
                else
                {

                    objCreateFile.CreateTextFile(strIPAddress, strMacAddress, strOSDetails, strIMEINo, strGcmID, strAPPVersion, intSource.ToString(), intCreatedByType, 15, "YYou are not authorize to assign slab to this User", strUserName);

                    strMsg = "You are not authorize to assign slab to this User !";
                }

                return strMsg;
            }
            else
            {
                strMsg = "Kindly Enter Valid Details !";

                return strMsg;
            }
        }


        // Activity Type=23
        [WebMethod]
        public DataSet PackageDetails(string strUserName, int reseller_id, string strIPAddress, int intDesignationType, string strMacAddress, string strOSDetails, string strIMEINo, string strGcmID, string strAPPVersion, int intSource, int intCreatedByType)
        {
            DataSet ds = new DataSet();
            try
            {
                if (reseller_id != 0)
                {
                    ds = objODBC.getDataSet("SELECT id as Slab_ID,slab_name as Slab_Name FROM er_commission_slab WHERE parent_id=" + reseller_id + " and id !=1 and group_id=0");

                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        objCreateFile.CreateTextFile(strIPAddress, strMacAddress, strOSDetails, strIMEINo, strGcmID, strAPPVersion, intSource.ToString(), intCreatedByType, 23, "Package Details fetch Sucessfully ", strUserName);
                        return ds;
                    }
                    else
                    {
                        return objODBC.getresult("Records not found!!!");
                    }
                }
                else
                {
                    objCreateFile.CreateTextFile(strIPAddress, strMacAddress, strOSDetails, strIMEINo, strGcmID, strAPPVersion, intSource.ToString(), intCreatedByType, 23, "Package Details fetch Fail ", strUserName);
                    return objODBC.getresult("Records not found!!!");
                }
            }
            catch (Exception ex)
            {
                return objODBC.getresult(ex.Message.ToString());
            }
            finally
            {
                dt.Dispose(); ds.Dispose();
            }

        }

        [WebMethod]
        public DataSet PackageDetailsMegaSlab(string strUserName, int reseller_id, string strIPAddress, int intDesignationType, string strMacAddress, string strOSDetails, string strIMEINo, string strGcmID, string strAPPVersion, int intSource, int intCreatedByType, int intUSertype)
        {
            DataSet ds = new DataSet();
            try
            {
                if (reseller_id != 0)
                {
                    //ds = objODBC.getDataSet("SELECT id as Slab_ID,slab_name as Slab_Name FROM er_commission_slab WHERE parent_id=" + reseller_id + " and id !=1 and group_id>0");
                    ds = objODBC.getDataSet("SELECT id as Slab_ID,slab_name as Slab_Name FROM er_commission_slab WHERE id=295 and group_id=0");
                    //  ds = objODBC.getDataSet("SELECT id as Slab_ID,slab_name as Slab_Name FROM er_commission_slab WHERE id=4 and group_id=0");
                    //   ds = objODBC.getDataSet("SELECT id as Slab_ID,name as Slab_Name FROM er_slab_group WHERE slab_type=" + (intUSertype - 2) + "");

                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        objCreateFile.CreateTextFile(strIPAddress, strMacAddress, strOSDetails, strIMEINo, strGcmID, strAPPVersion, intSource.ToString(), intCreatedByType, 23, "Mega Package Details fetch Sucessfully ", strUserName);
                        return ds;
                    }
                    else
                    {
                        return objODBC.getresult("Records not found!!!");
                    }
                }
                else
                {
                    objCreateFile.CreateTextFile(strIPAddress, strMacAddress, strOSDetails, strIMEINo, strGcmID, strAPPVersion, intSource.ToString(), intCreatedByType, 23, "Mega Package Details fetch Fail ", strUserName);
                    return objODBC.getresult("Records not found!!!");
                }
            }
            catch (Exception ex)
            {
                return objODBC.getresult(ex.Message.ToString());
            }
            finally
            {
                dt.Dispose(); ds.Dispose();
            }

        }


        [WebMethod]
        public DataSet PackageDetailsMegaSlab_admin(string strUserName, int reseller_id, string strSearch, string strIPAddress, int intDesignationType, string strMacAddress, string strOSDetails, string strIMEINo, string strGcmID, string strAPPVersion, int intSource, int intCreatedByType)
        {
            DataSet ds = new DataSet();
            try
            {
                if (reseller_id != 0)
                {
                    ds = objODBC.getDataSet("SELECT id as Slab_ID,slab_name as Slab_Name FROM er_commission_slab WHERE parent_id=1 and id !=1 and group_id=0");
                   // ds = objODBC.getDataSet("SELECT id as Slab_ID,slab_name as Slab_Name FROM er_commission_slab WHERE id=4 and group_id=0");
                    // ds = objODBC.getDataSet("SELECT id as Slab_ID,name as Slab_Name FROM er_slab_group where id>0  " + strSearch + "");

                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        objCreateFile.CreateTextFile(strIPAddress, strMacAddress, strOSDetails, strIMEINo, strGcmID, strAPPVersion, intSource.ToString(), intCreatedByType, 23, "Mega Package Details fetch Sucessfully ", strUserName);

                        return ds;
                    }
                    else
                    {
                        return objODBC.getresult("Records not found!!!");
                    }
                }
                else
                {
                    objCreateFile.CreateTextFile(strIPAddress, strMacAddress, strOSDetails, strIMEINo, strGcmID, strAPPVersion, intSource.ToString(), intCreatedByType, 23, "Mega Package Details fetch Fail ", strUserName);
                    return objODBC.getresult("Records not found!!!");
                }
            }
            catch (Exception ex)
            {
                return objODBC.getresult(ex.Message.ToString());
            }
            finally
            {
                ds.Dispose();
            }

        }
        [WebMethod]
        public ArrayList er_android_DefaultSlabs(string strUserName, int intResellerID, string strIPAddress, int intDesignationType, string strMacAddress, string strOSDetails, string strIMEINo, string strGcmID, string strAPPVersion, int intSource, int intCreatedByType)
        {
            ArrayList arrlst = new ArrayList();
            try
            {
                try
                {
                    ds = DefaultSlabs(strUserName, intResellerID, strIPAddress, intDesignationType, strMacAddress, strOSDetails, strIMEINo, strGcmID, strAPPVersion, intSource, intCreatedByType);

                    arrlst = objcf.Get_ArrayLlist_From_Dataset(ds);

                }
                catch (Exception)
                {


                }
                finally
                {

                }

            }
            catch (Exception)
            {

            }
            return arrlst;
        }

        // Activity Type=19
        [WebMethod]
        public DataSet DefaultSlabs(string strUserName, int intResellerID, string strIPAddress, int intDesignationType, string strMacAddress, string strOSDetails, string strIMEINo, string strGcmID, string strAPPVersion, int intSource, int intCreatedByType)
        {
            ArrayList array_defaultSlabs = new ArrayList(); DataSet ds = new DataSet();
            try
            {
                if (intResellerID != 0)
                {
                    array_defaultSlabs = objCommonFunction.Get_ArrayLlist("SELECT res_slab, md_slab, d_slab, ret_slab, api_slab from er_default_slab where parent_id=" + intResellerID);



                    DataTable dt = new DataTable("DefaultSlabs");
                    dt.Columns.Add(new DataColumn("User_Type", typeof(int)));
                    dt.Columns.Add(new DataColumn("User_Type_Name", typeof(string)));
                    dt.Columns.Add(new DataColumn("Slab_ID", typeof(int)));
                    dt.Columns.Add(new DataColumn("My_SlabID", typeof(int)));
                    int intmySlabID = objODBC.executeScalar_int("select slab_id from er_slab_user where  userid = " + intResellerID);



                    DataRow dr = dt.NewRow();
                    dr["User_Type"] = 3;
                    dr["User_Type_Name"] = "Distributor";
                    dr["Slab_ID"] = array_defaultSlabs[2];
                    dr["My_SlabID"] = intmySlabID;
                    dt.Rows.Add(dr);

                    DataRow dr1 = dt.NewRow();
                    dr1["User_Type"] = 4;
                    dr1["User_Type_Name"] = "Sub-Distributor";
                    dr1["Slab_ID"] = array_defaultSlabs[3];
                    dr1["My_SlabID"] = intmySlabID;
                    dt.Rows.Add(dr1);

                    DataRow dr2 = dt.NewRow();
                    dr2["User_Type"] = 5;
                    dr2["User_Type_Name"] = "Retailer";
                    dr2["Slab_ID"] = array_defaultSlabs[4];
                    dr2["My_SlabID"] = intmySlabID;
                    dt.Rows.Add(dr2);

                    ds.Tables.Add(dt);


                    objCreateFile.CreateTextFile(strIPAddress, strMacAddress, strOSDetails, strIMEINo, strGcmID, strAPPVersion, intSource.ToString(), intCreatedByType, 19, "Get Default Slabs", strUserName);

                    return ds;
                }
                else
                {
                    objCreateFile.CreateTextFile(strIPAddress, strMacAddress, strOSDetails, strIMEINo, strGcmID, strAPPVersion, intSource.ToString(), intCreatedByType, 19, "Get Default Slabs Fail", strUserName);
                    return objODBC.getresult("Slab not available!!!");
                }
            }
            catch (Exception ex)
            {
                return objODBC.getresult(ex.Message.ToString());
            }
            finally
            {
                dt.Dispose();
            }
        }

        // Activity Type=35
        [WebMethod]
        public DataSet MySlab(string strUserName, int intUserID, string strIPAddress, int intDesignationType, string strMacAddress, string strOSDetails, string strIMEINo, string strGcmID, string strAPPVersion, int intSource, int intCreatedByType)
        {
            ArrayList packages = new ArrayList(); DataSet ds = new DataSet();
            try
            {
                objCreateFile.CreateTextFile(strIPAddress, strMacAddress, strOSDetails, strIMEINo, strGcmID, strAPPVersion, intSource.ToString(), intCreatedByType, 35, "Get Default Slabs", strUserName);
            }
            catch (Exception)
            { }
            try
            {
                int intChkRights = objODBC.executeScalar_int("select commission_rights from er_login where userid =" + intUserID + " ");

                if (intChkRights != 1) // mega slab
                {
                    int intResellerSlabid = objODBC.executeScalar_int("SELECT slab_id from er_slab_user where userid= " + intUserID);

                    int intGroupID = objODBC.executeScalar_int("select ifnull(max(group_id),0) from er_commission_structure where slab_id=" + intResellerSlabid + " order by id asc limit 1");

                    if (intResellerSlabid > 0 && intGroupID > 0 && intDesignationType == 3)
                    {

                        ds = objODBC.getDataSet("Select a.id, a.operator_id as Operator_ID, a.commission_per as My_Commission, b.operator_name as Operator_Name," + intResellerSlabid + " as slabID FROM er_commission_structure a, er_recharge_operator_mobile b where a.operator_id=b.id  and a.group_id=" + intGroupID + " and a.operator_id=b.id ");

                        if (ds.Tables[0].Rows.Count > 0)
                        {
                            return ds;
                        }
                        else
                        {
                            return objODBC.getresult("Records not found!!!");
                        }

                    }
                    else if (intResellerSlabid > 0 && intGroupID > 0 && intDesignationType == 4)
                    {

                        int intChkCount = objODBC.executeScalar_int("select count(1) from er_commission_structure where  group_id =" + intGroupID + "");

                        if (intChkCount == 15)
                        {
                            ds = objODBC.getDataSet("Select a.id, a.operator_id as Operator_ID, a.commission_per as My_Commission, b.operator_name as Operator_Name," + intResellerSlabid + " as slabID FROM er_commission_structure a, er_recharge_operator_mobile b where a.operator_id=b.id  and a.group_id=" + intGroupID + " and a.operator_id=b.id order by a.id asc limit 5,10");
                        }
                        else
                        {
                            ds = objODBC.getDataSet("Select a.id, a.operator_id as Operator_ID, a.commission_per as My_Commission, b.operator_name as Operator_Name," + intResellerSlabid + " as slabID FROM er_commission_structure a, er_recharge_operator_mobile b where a.operator_id=b.id  and a.group_id=" + intGroupID + " and a.operator_id=b.id order by a.id asc");
                        }



                        if (ds.Tables[0].Rows.Count > 0)
                        {
                            return ds;
                        }
                        else
                        {
                            return objODBC.getresult("Records not found!!!");
                        }

                    }
                    else if (intResellerSlabid > 0 && intGroupID > 0 && intDesignationType == 5)
                    {
                        int intChkCount = objODBC.executeScalar_int("select count(1) from er_commission_structure where  group_id =" + intGroupID + "");

                        if (intChkCount == 15)
                        {
                            ds = objODBC.getDataSet("Select a.id, a.operator_id as Operator_ID, a.commission_per as My_Commission, b.operator_name as Operator_Name," + intResellerSlabid + " as slabID FROM er_commission_structure a, er_recharge_operator_mobile b where a.operator_id=b.id  and a.group_id=" + intGroupID + " and a.operator_id=b.id order by a.id asc limit 10,15");
                        }
                        else
                        {
                            ds = objODBC.getDataSet("Select a.id, a.operator_id as Operator_ID, a.commission_per as My_Commission, b.operator_name as Operator_Name," + intResellerSlabid + " as slabID FROM er_commission_structure a, er_recharge_operator_mobile b where a.operator_id=b.id  and a.group_id=" + intGroupID + " and a.operator_id=b.id order by a.id asc limit 5,10");
                        }

                        if (ds.Tables[0].Rows.Count > 0)
                        {
                            return ds;
                        }
                        else
                        {
                            return objODBC.getresult("Records not found!!!");
                        }

                    }
                    return ds;
                }
                else if (intChkRights == 1) // normal slab
                {
                    var intResellerSlabid = objODBC.executeScalar_int("SELECT slab_id from er_slab_user where userid= " + intUserID);
                    if (intResellerSlabid > 0)
                    {
                        ds = objODBC.getDataSet("Select a.id, a.operator_id as Operator_ID, a.commission_per as My_Commission, b.operator_name as Operator_Name," + intResellerSlabid + " as slabID FROM er_commission_structure a, er_recharge_operator_mobile b where a.operator_id=b.id  and a.slab_id=" + intResellerSlabid + " and a.operator_id=b.id and a.group_id =0");

                        if (ds.Tables[0].Rows.Count > 0)
                        {
                            return ds;
                        }
                        else
                        {
                            return objODBC.getresult("Records not found!!!");
                        }
                    }
                    else
                    {
                        return objODBC.getresult("Slab not available!!!");
                    }
                }
                else if (intChkRights > 2)
                {

                    return objODBC.getresult("Records not found!!!");
                }
                return ds;
            }
            catch (Exception ex)
            {
                return objODBC.getresult(ex.Message.ToString());
            }
            finally
            {
                dt.Dispose();
            }
        }
        // Activity Type=35
        [WebMethod]
        public DataSet MySlab_Cr2(string strUserName, int intUserID, string strIPAddress, int intDesignationType, string strMacAddress, string strOSDetails, string strIMEINo, string strGcmID, string strAPPVersion, int intSource, int intCreatedByType)
        {
            ArrayList packages = new ArrayList(); DataSet ds = new DataSet(); DataSet ds2 = new DataSet();
            try
            {
                objCreateFile.CreateTextFile(strIPAddress, strMacAddress, strOSDetails, strIMEINo, strGcmID, strAPPVersion, intSource.ToString(), intCreatedByType, 35, "Get Default Slabs", strUserName);
            }
            catch (Exception)
            { }
            try
            {
                int intSalaamStatus, intEtisalatStatus, intRoshanStatus, intMTNStatus, intAWCCstatus;
                string strOperatorID = "0,";
                try
                {
                    ds2 = objODBC.getDataSet("select  `salaam_status`, `etisalat_status`,`roshan_status`, `mtn_status`, `awcc_status` FROM `er_login` WHERE userid="+ intUserID + "");
                    if (ds2.Tables[0].Rows.Count > 0)
                    {
                        intSalaamStatus = int.Parse(ds2.Tables[0].Rows[0][0].ToString());
                        if(intSalaamStatus==1)
                        {
                            strOperatorID = strOperatorID + "'1',";
                        }
                        intEtisalatStatus = int.Parse(ds2.Tables[0].Rows[0][1].ToString());
                        if (intEtisalatStatus == 1)
                        {
                            strOperatorID = strOperatorID + "'2',";
                        }
                        intRoshanStatus = int.Parse(ds2.Tables[0].Rows[0][2].ToString());
                        if (intRoshanStatus == 1)
                        {
                            strOperatorID = strOperatorID + "'3',";
                        }
                        intMTNStatus = int.Parse(ds2.Tables[0].Rows[0][3].ToString());
                        if (intMTNStatus == 1)
                        {
                            strOperatorID = strOperatorID + "'4',";
                        }
                        intAWCCstatus = int.Parse(ds2.Tables[0].Rows[0][4].ToString());
                        if (intAWCCstatus == 1)
                        {
                            strOperatorID = strOperatorID + "'5'";
                        }

                    }                   
                }
                catch(Exception)
                {

                }
                finally
                {
                    ds2.Dispose();
                    strOperatorID = strOperatorID.TrimEnd(',');
                    strOperatorID = "1,2,3,4,5";
                }

                int intChkRights = objODBC.executeScalar_int("select commission_rights from er_login where userid =" + intUserID + " ");

                if (intChkRights != 1) // mega slab
                {
                    int intResellerSlabid = objODBC.executeScalar_int("SELECT slab_id from er_slab_user where userid= " + intUserID);

                    int intGroupID = objODBC.executeScalar_int("select ifnull(max(group_id),0) from er_commission_structure where slab_id=" + intResellerSlabid + " order by id asc limit 1");

                    if (intResellerSlabid > 0 && intGroupID > 0 && intDesignationType == 3)
                    {

                        ds = objODBC.getDataSet("Select a.id, a.operator_id as Operator_ID, a.commission_per as My_Commission, b.operator_name as Operator_Name," + intResellerSlabid + " as slabID,'Mega-Slab' as SlabType FROM er_commission_structure a, er_recharge_operator_mobile b where a.operator_id=b.id  and a.group_id=" + intGroupID + " and a.operator_id=b.id and a.operator_id IN (" + strOperatorID + ")");

                        if (ds.Tables[0].Rows.Count > 0)
                        {
                            return ds;
                        }
                        else
                        {
                            return objODBC.getresult("Records not found!!!");
                        }

                    }
                    else if (intResellerSlabid > 0 && intGroupID > 0 && intDesignationType == 4)
                    {

                        int intChkCount = objODBC.executeScalar_int("select count(1) from er_commission_structure where  group_id =" + intGroupID + "");

                        if (intChkCount == 15)
                        {
                            ds = objODBC.getDataSet("Select a.id, a.operator_id as Operator_ID, a.commission_per as My_Commission, b.operator_name as Operator_Name," + intResellerSlabid + " as slabID,'Mega-Slab' as SlabType FROM er_commission_structure a, er_recharge_operator_mobile b where a.operator_id=b.id  and a.group_id=" + intGroupID + " and a.operator_id=b.id and a.operator_id IN (" + strOperatorID + ") order by a.id asc limit 5,10");
                        }
                        else
                        {
                            ds = objODBC.getDataSet("Select a.id, a.operator_id as Operator_ID, a.commission_per as My_Commission, b.operator_name as Operator_Name," + intResellerSlabid + " as slabID,'Mega-Slab' as SlabType FROM er_commission_structure a, er_recharge_operator_mobile b where a.operator_id=b.id  and a.group_id=" + intGroupID + " and a.operator_id=b.id and a.operator_id IN (" + strOperatorID + ") order by a.id asc");
                        }



                        if (ds.Tables[0].Rows.Count > 0)
                        {
                            return ds;
                        }
                        else
                        {
                            return objODBC.getresult("Records not found!!!");
                        }

                    }
                    else if (intResellerSlabid > 0 && intGroupID > 0 && intDesignationType == 5)
                    {
                        int intChkCount = objODBC.executeScalar_int("select count(1) from er_commission_structure where  group_id =" + intGroupID + "");

                        if (intChkCount == 15)
                        {
                            ds = objODBC.getDataSet("Select a.id, a.operator_id as Operator_ID, a.commission_per as My_Commission, b.operator_name as Operator_Name," + intResellerSlabid + " as slabID,'Mega-Slab' as SlabType FROM er_commission_structure a, er_recharge_operator_mobile b where a.operator_id=b.id  and a.group_id=" + intGroupID + " and a.operator_id=b.id and a.operator_id IN (" + strOperatorID + ") order by a.id asc limit 10,15");
                        }
                        else
                        {
                            ds = objODBC.getDataSet("Select a.id, a.operator_id as Operator_ID, a.commission_per as My_Commission, b.operator_name as Operator_Name," + intResellerSlabid + " as slabID,'Mega-Slab' as SlabType FROM er_commission_structure a, er_recharge_operator_mobile b where a.operator_id=b.id  and a.group_id=" + intGroupID + " and a.operator_id=b.id and a.operator_id IN (" + strOperatorID + ") order by a.id asc limit 5,10");
                        }

                        if (ds.Tables[0].Rows.Count > 0)
                        {
                            return ds;
                        }
                        else
                        {
                            return objODBC.getresult("Records not found!!!");
                        }

                    }
                    else if (intResellerSlabid > 0 && intDesignationType == 5)
                    {
                        ds = objODBC.getDataSet("Select a.id, a.operator_id as Operator_ID, a.commission_per as My_Commission, b.operator_name as Operator_Name," + intResellerSlabid + " as slabID,'Normal-Slab' as SlabType FROM er_commission_structure a, er_recharge_operator_mobile b where a.operator_id=b.id  and a.slab_id=" + intResellerSlabid + " and a.operator_id=b.id and a.group_id =0 AND a.operator_id IN (" + strOperatorID + ")");

                        if (ds.Tables[0].Rows.Count > 0)
                        {
                            return ds;
                        }
                        else
                        {
                            return objODBC.getresult("Records not found!!!");
                        }
                    }
                    return ds;
                }
                else if (intChkRights == 1) // normal slab
                {
                    var intResellerSlabid = objODBC.executeScalar_int("SELECT slab_id from er_slab_user where userid= " + intUserID);
                    if (intResellerSlabid > 0)
                    {
                        ds = objODBC.getDataSet("Select a.id, a.operator_id as Operator_ID, a.commission_per as My_Commission, b.operator_name as Operator_Name," + intResellerSlabid + " as slabID,'Normal-Slab' as SlabType FROM er_commission_structure a, er_recharge_operator_mobile b where a.operator_id=b.id  and a.slab_id=" + intResellerSlabid + " and a.operator_id=b.id and a.group_id =0 AND a.operator_id IN (" + strOperatorID + ")");

                        if (ds.Tables[0].Rows.Count > 0)
                        {
                            return ds;
                        }
                        else
                        {
                            return objODBC.getresult("Records not found!!!");
                        }
                    }
                    else
                    {
                        return objODBC.getresult("Slab not available!!!");
                    }
                }
                else if (intChkRights > 2)
                {

                    return objODBC.getresult("Records not found!!!");
                }
                return ds;
            }
            catch (Exception ex)
            {
                return objODBC.getresult(ex.Message.ToString());
            }
            finally
            {
                dt.Dispose();
            }
        }
        [WebMethod]
        public ArrayList er_android_MySlab(string strUserName, int intUserID, string strIPAddress, int intDesignationType, string strMacAddress, string strOSDetails, string strIMEINo, string strGcmID, string strAPPVersion, int intSource, int intCreatedByType)
        {
            ArrayList arrlst = new ArrayList();

            try
            {
                ds = MySlab(strUserName, intUserID, strIPAddress, intDesignationType, strMacAddress, strOSDetails, strIMEINo, strGcmID, strAPPVersion, intSource, intCreatedByType);
                ds.Tables[0].Columns.Remove(ds.Tables[0].Columns[4]);
                arrlst = objcf.Get_ArrayLlist_From_Dataset(ds);

            }
            catch (Exception)
            {
            }
            finally
            {

            }
            return arrlst;

        }
        [WebMethod]
        public string er_android_MySlab_Cr2(string strUserName, int intUserID, string strIPAddress, int intDesignationType, string strMacAddress, string strOSDetails, string strIMEINo, string strGcmID, string strAPPVersion, int intSource, int intCreatedByType)

        {
            DataSet ds = new DataSet(); DataSet ds2 = new DataSet();
            CommonFunction objCF = new CommonFunction();
            string strReturn = string.Empty;
            int intFlag = 1;
            string strErrorMsg = string.Empty;
            try
            {
                objCreateFile.CreateTextFile(strIPAddress, strMacAddress, strOSDetails, strIMEINo, strGcmID, strAPPVersion, intSource.ToString(), intCreatedByType, 35, "Get Default Slabs", strUserName);
            }
            catch (Exception)
            { }
            try
            {
                int intSalaamStatus, intEtisalatStatus, intRoshanStatus, intMTNStatus, intAWCCstatus;
                string strOperatorID = "0,";
                try
                {
                    ds2 = objODBC.getDataSet("select  `salaam_status`, `etisalat_status`,`roshan_status`, `mtn_status`, `awcc_status` FROM `er_login` WHERE userid=" + intUserID + "");
                    if (ds2.Tables[0].Rows.Count > 0)
                    {
                        intSalaamStatus = int.Parse(ds2.Tables[0].Rows[0][0].ToString());
                        if (intSalaamStatus == 1)
                        {
                            strOperatorID = strOperatorID + "'1',";
                        }
                        intEtisalatStatus = int.Parse(ds2.Tables[0].Rows[0][1].ToString());
                        if (intEtisalatStatus == 1)
                        {
                            strOperatorID = strOperatorID + "'2',";
                        }
                        intRoshanStatus = int.Parse(ds2.Tables[0].Rows[0][2].ToString());
                        if (intRoshanStatus == 1)
                        {
                            strOperatorID = strOperatorID + "'3',";
                        }
                        intMTNStatus = int.Parse(ds2.Tables[0].Rows[0][3].ToString());
                        if (intMTNStatus == 1)
                        {
                            strOperatorID = strOperatorID + "'4',";
                        }
                        intAWCCstatus = int.Parse(ds2.Tables[0].Rows[0][4].ToString());
                        if (intAWCCstatus == 1)
                        {
                            strOperatorID = strOperatorID + "'5'";
                        }

                    }
                }
                catch (Exception)
                {

                }
                finally
                {
                    ds2.Dispose();
                    strOperatorID = strOperatorID.TrimEnd(',');
                }

                int intChkRights = objODBC.executeScalar_int("select commission_rights from er_login where userid =" + intUserID + " ");

                if (intChkRights != 1) // mega slab
                {
                    int intResellerSlabid = objODBC.executeScalar_int("SELECT slab_id from er_slab_user where userid= " + intUserID);

                    int intGroupID = objODBC.executeScalar_int("select ifnull(max(group_id),0) from er_commission_structure where slab_id=" + intResellerSlabid + " order by id asc limit 1");

                    if (intResellerSlabid > 0 && intGroupID > 0 && intDesignationType == 3)
                    {

                        int strCount = objODBC.executeScalar_int("Select count(1) FROM er_commission_structure a, er_recharge_operator_mobile b where a.operator_id=b.id  and a.group_id=" + intGroupID + " and a.operator_id=b.id and a.operator_id IN (" + strOperatorID + ")");

                        string strQuerry = "Select a.id, a.operator_id as Operator_ID, a.commission_per as My_Commission, b.operator_name as Operator_Name," + intResellerSlabid + " as slabID,'Mega-Slab' as SlabType FROM er_commission_structure a, er_recharge_operator_mobile b where a.operator_id=b.id  and a.group_id=" + intGroupID + " and a.operator_id=b.id and a.operator_id IN (" + strOperatorID + ")";

                        if (strCount > 0)
                        {
                            intFlag = 1;
                            strErrorMsg = "Slab Data View";
                            strReturn = objCF.DataTableToGetCategory(strQuerry, "er_android_MySlab", intFlag, strErrorMsg);
                        }
                        else
                        {
                            intFlag = 0;
                            strErrorMsg = "Record Not Found";
                            strReturn = objCF.DataTableToGetCategory(strQuerry, "er_android_MySlab", intFlag, strErrorMsg);
                        }

                    }
                    else if (intResellerSlabid > 0 && intGroupID > 0 && intDesignationType == 4)
                    {

                        int intChkCount = objODBC.executeScalar_int("select count(1) from er_commission_structure where  group_id =" + intGroupID + "");

                        if (intChkCount == 15)
                        {

                            int strCount = objODBC.executeScalar_int("Select count(1) FROM er_commission_structure a, er_recharge_operator_mobile b where a.operator_id=b.id  and a.group_id=" + intGroupID + " and a.operator_id=b.id and a.operator_id IN (" + strOperatorID + ") order by a.id asc limit 5,10");

                            string strQuerry = "Select a.id, a.operator_id as Operator_ID, a.commission_per as My_Commission, b.operator_name as Operator_Name," + intResellerSlabid + " as slabID,'Mega-Slab' as SlabType FROM er_commission_structure a, er_recharge_operator_mobile b where a.operator_id=b.id  and a.group_id=" + intGroupID + " and a.operator_id=b.id and a.operator_id IN (" + strOperatorID + ")";


                            if (strCount > 0)
                            {
                                intFlag = 1;
                                strErrorMsg = "Slab Data View";
                                strReturn = objCF.DataTableToGetCategory(strQuerry, "er_android_MySlab", intFlag, strErrorMsg);
                            }
                            else
                            {
                                intFlag = 0;
                                strErrorMsg = "Record Not Found";
                                strReturn = objCF.DataTableToGetCategory(strQuerry, "er_android_MySlab", intFlag, strErrorMsg);
                            }


                        }
                        else
                        {
                            int strCount = objODBC.executeScalar_int("Select count(1) FROM er_commission_structure a, er_recharge_operator_mobile b where a.operator_id=b.id  and a.group_id=" + intGroupID + " and a.operator_id=b.id and a.operator_id IN (" + strOperatorID + ") order by a.id asc");

                            string strQuerry = "Select a.id, a.operator_id as Operator_ID, a.commission_per as My_Commission, b.operator_name as Operator_Name," + intResellerSlabid + " as slabID,'Mega-Slab' as SlabType FROM er_commission_structure a, er_recharge_operator_mobile b where a.operator_id=b.id  and a.group_id=" + intGroupID + " and a.operator_id=b.id and a.operator_id IN (" + strOperatorID + ") order by a.id asc";


                            if (strCount > 0)
                            {
                                intFlag = 1;
                                strErrorMsg = "Slab Data View";
                                strReturn = objCF.DataTableToGetCategory(strQuerry, "er_android_MySlab", intFlag, strErrorMsg);
                            }
                            else
                            {
                                intFlag = 0;
                                strErrorMsg = "Record Not Found";
                                strReturn = objCF.DataTableToGetCategory(strQuerry, "er_android_MySlab", intFlag, strErrorMsg);
                            }
                        }
                    }
                    else if (intResellerSlabid > 0 && intGroupID > 0 && intDesignationType == 5)
                    {
                        int intChkCount = objODBC.executeScalar_int("select count(1) from er_commission_structure where  group_id =" + intGroupID + "");

                        if (intChkCount == 15)
                        {
                            int strCount = objODBC.executeScalar_int("Select count(1) FROM er_commission_structure a, er_recharge_operator_mobile b where a.operator_id=b.id  and a.group_id=" + intGroupID + " and a.operator_id=b.id and a.operator_id IN (" + strOperatorID + ") order by a.id asc limit 10,15");

                            string strQuerry = "Select a.id, a.operator_id as Operator_ID, a.commission_per as My_Commission, b.operator_name as Operator_Name," + intResellerSlabid + " as slabID,'Mega-Slab' as SlabType FROM er_commission_structure a, er_recharge_operator_mobile b where a.operator_id=b.id  and a.group_id=" + intGroupID + " and a.operator_id=b.id and a.operator_id IN (" + strOperatorID + ") order by a.id asc limit 10,15";


                            if (strCount > 0)
                            {
                                intFlag = 1;
                                strErrorMsg = "Slab Data View";
                                strReturn = objCF.DataTableToGetCategory(strQuerry, "er_android_MySlab", intFlag, strErrorMsg);
                            }
                            else
                            {
                                intFlag = 0;
                                strErrorMsg = "Record Not Found";
                                strReturn = objCF.DataTableToGetCategory(strQuerry, "er_android_MySlab", intFlag, strErrorMsg);
                            }
                        }
                        else
                        {
                            int strCount = objODBC.executeScalar_int("Select count(1) FROM er_commission_structure a, er_recharge_operator_mobile b where a.operator_id=b.id  and a.group_id=" + intGroupID + " and a.operator_id=b.id and a.operator_id IN (" + strOperatorID + ") order by a.id asc limit 5,10");

                            string strQuerry = "Select a.id, a.operator_id as Operator_ID, a.commission_per as My_Commission, b.operator_name as Operator_Name," + intResellerSlabid + " as slabID,'Mega-Slab' as SlabType FROM er_commission_structure a, er_recharge_operator_mobile b where a.operator_id=b.id  and a.group_id=" + intGroupID + " and a.operator_id=b.id and a.operator_id IN (" + strOperatorID + ") order by a.id asc limit 5,10";

                            if (strCount > 0)
                            {
                                intFlag = 1;
                                strErrorMsg = "Slab Data View";
                                strReturn = objCF.DataTableToGetCategory(strQuerry, "er_android_MySlab", intFlag, strErrorMsg);
                            }
                            else
                            {
                                intFlag = 0;
                                strErrorMsg = "Record Not Found";
                                strReturn = objCF.DataTableToGetCategory(strQuerry, "er_android_MySlab", intFlag, strErrorMsg);
                            }
                        }



                    }
                    else if (intResellerSlabid > 0 && intDesignationType == 5)
                    {
                        int strCount = objODBC.executeScalar_int("Select count(1) FROM er_commission_structure a, er_recharge_operator_mobile b where a.operator_id=b.id  and a.slab_id=" + intResellerSlabid + " and a.operator_id=b.id and a.group_id =0 AND a.operator_id IN (" + strOperatorID + ")");

                        string strQuerry = "Select a.id, a.operator_id as Operator_ID, a.commission_per as My_Commission, b.operator_name as Operator_Name," + intResellerSlabid + " as slabID,'Normal-Slab' as SlabType FROM er_commission_structure a, er_recharge_operator_mobile b where a.operator_id=b.id  and a.slab_id=" + intResellerSlabid + " and a.operator_id=b.id and a.group_id =0 AND a.operator_id IN (" + strOperatorID + ")";

                        if (strCount > 0)
                        {
                            intFlag = 1;
                            strErrorMsg = "Slab Data View";
                            strReturn = objCF.DataTableToGetCategory(strQuerry, "er_android_MySlab", intFlag, strErrorMsg);
                        }
                        else
                        {
                            intFlag = 0;
                            strErrorMsg = "Record Not Found";
                            strReturn = objCF.DataTableToGetCategory(strQuerry, "er_android_MySlab", intFlag, strErrorMsg);
                        }
                    }
                    return strReturn;
                }
                else if (intChkRights == 1) // normal slab
                {
                    var intResellerSlabid = objODBC.executeScalar_int("SELECT slab_id from er_slab_user where userid= " + intUserID);
                    if (intResellerSlabid > 0)
                    {

                        int strCount = objODBC.executeScalar_int("Select count(1) FROM er_commission_structure a, er_recharge_operator_mobile b where a.operator_id=b.id  and a.slab_id=" + intResellerSlabid + " and a.operator_id=b.id and a.group_id =0 AND a.operator_id IN (" + strOperatorID + ")");

                        string strQuerry = "Select a.id, a.operator_id as Operator_ID, a.commission_per as My_Commission, b.operator_name as Operator_Name," + intResellerSlabid + " as slabID,'Normal-Slab' as SlabType FROM er_commission_structure a, er_recharge_operator_mobile b where a.operator_id=b.id  and a.slab_id=" + intResellerSlabid + " and a.operator_id=b.id and a.group_id =0 AND a.operator_id IN (" + strOperatorID + ")";

                        if (strCount > 0)
                        {
                            intFlag = 1;
                            strErrorMsg = "Slab Data View";
                            strReturn = objCF.DataTableToGetCategory(strQuerry, "er_android_MySlab", intFlag, strErrorMsg);
                        }
                        else
                        {
                            intFlag = 0;
                            strErrorMsg = "Record Not Found";
                            strReturn = objCF.DataTableToGetCategory(strQuerry, "er_android_MySlab", intFlag, strErrorMsg);
                        }
                    }
                    else
                    {
                        intFlag = 0;
                        strErrorMsg = "Slab not available!!!";
                        strReturn = objCF.DataTableToGetCategory("", "er_android_MySlab", intFlag, strErrorMsg);
                        
                    }
                }
                else if (intChkRights > 2)
                {
                    intFlag = 0;
                    strErrorMsg = "Records not found!!!";
                    strReturn = objCF.DataTableToGetCategory("", "er_android_MySlab", intFlag, strErrorMsg);

                }
                return strReturn;
            }
            catch (Exception ex)
            {
                intFlag = 0;
                strErrorMsg = ex.ToString();
                strReturn = objCF.DataTableToGetCategory("", "er_android_MySlab", intFlag, strErrorMsg);
                return strReturn;
            }
            finally
            {
                dt.Dispose();
                ds.Dispose();
                ds2.Dispose();
            }
        }
        // Activity Type=36
        [WebMethod]
        public DataSet PackageDetailsAdmin(string strUserName, int intParentID, int intSlabID, string strIPAddress, int intDesignationType, string strMacAddress, string strOSDetails, string strIMEINo, string strGcmID, string strAPPVersion, int intSource, int intCreatedByType)
        {
            ArrayList packages = new ArrayList(); DataSet ds = new DataSet();
            try
            {
                if ((intParentID != 0) && (intSlabID != 0))
                {

                    var intResellerSlabid = objODBC.executeScalar_int("SELECT slab_id from er_slab_user where userid= " + intParentID);

                    if (intResellerSlabid > 0)
                    {
                        ds = objODBC.getDataSet("Select a.id, a.operator_id as Operator_ID, a.commission_per as Set_Commission, res.commission_per as My_Commission, b.operator_name as Operator_Name,a.target_amt as Target ,a.target_reward as Reward,a.target_status as Target_Status FROM er_commission_structure a, er_commission_structure res , er_recharge_operator_mobile b where a.operator_id=b.id  and a.parent_id=" + intParentID + " and a.slab_id=" + intSlabID + " and res.slab_id=" + intResellerSlabid + " and res.operator_id=b.id");

                        if (ds.Tables[0].Rows.Count > 0)
                        {
                            objCreateFile.CreateTextFile(strIPAddress, strMacAddress, strOSDetails, strIMEINo, strGcmID, strAPPVersion, intSource.ToString(), intCreatedByType, 36, "Get Package Details Admin", strUserName);
                            return ds;

                        }
                        else
                        {
                            return objODBC.getresult("Records not found!!!");
                        }
                    }
                    else
                    {
                        return objODBC.getresult("Slab not available!!!");
                    }

                }
                else
                {
                    return objODBC.getresult("Invalid details");
                }
            }
            catch (Exception ex)
            {
                return objODBC.getresult(ex.Message.ToString());
            }
            finally
            {
                dt.Dispose(); ds.Dispose();
            }
        }

        // Activity Type=36
        [WebMethod]
        public DataSet PackageDetails_CR2(string strUserName, int intParentID, int intSlabID, string strIPAddress, int intDesignationType, string strMacAddress, string strOSDetails, string strIMEINo, string strGcmID, string strAPPVersion, int intSource, int intCreatedByType)
        {
            ArrayList packages = new ArrayList(); DataSet ds = new DataSet(); DataSet ds2 = new DataSet();
            try
            {
                if ((intParentID != 0) && (intSlabID != 0))
                {

                    int intSalaamStatus, intEtisalatStatus, intRoshanStatus, intMTNStatus, intAWCCstatus;
                    string strOperatorID = "0,";
                    try
                    {
                        ds2 = objODBC.getDataSet("select  `salaam_status`, `etisalat_status`,`roshan_status`, `mtn_status`, `awcc_status` FROM `er_login` WHERE userid=" + intParentID + "");
                        if (ds2.Tables[0].Rows.Count > 0)
                        {
                            intSalaamStatus = int.Parse(ds2.Tables[0].Rows[0][0].ToString());
                            if (intSalaamStatus == 1)
                            {
                                strOperatorID = strOperatorID + "'1',";
                            }
                            intEtisalatStatus = int.Parse(ds2.Tables[0].Rows[0][1].ToString());
                            if (intEtisalatStatus == 1)
                            {
                                strOperatorID = strOperatorID + "'2',";
                            }
                            intRoshanStatus = int.Parse(ds2.Tables[0].Rows[0][2].ToString());
                            if (intRoshanStatus == 1)
                            {
                                strOperatorID = strOperatorID + "'3',";
                            }
                            intMTNStatus = int.Parse(ds2.Tables[0].Rows[0][3].ToString());
                            if (intMTNStatus == 1)
                            {
                                strOperatorID = strOperatorID + "'4',";
                            }
                            intAWCCstatus = int.Parse(ds2.Tables[0].Rows[0][4].ToString());
                            if (intAWCCstatus == 1)
                            {
                                strOperatorID = strOperatorID + "'5'";
                            }

                        }
                    }
                    catch (Exception)
                    {

                    }
                    finally
                    {
                        ds2.Dispose();
                        strOperatorID = strOperatorID.TrimEnd(',');
                    }


                    var intResellerSlabid = objODBC.executeScalar_int("SELECT slab_id from er_slab_user where userid= " + intParentID);

                    if (intResellerSlabid > 0)
                    {
                        ds = objODBC.getDataSet("Select a.id, a.operator_id as Operator_ID, a.commission_per as Set_Commission, res.commission_per as My_Commission, b.operator_name as Operator_Name,a.target_amt as Target ,a.target_reward as Reward,a.target_status as Target_Status FROM er_commission_structure a, er_commission_structure res , er_recharge_operator_mobile b where a.operator_id=b.id  and a.parent_id=" + intParentID + " and a.slab_id=" + intSlabID + " and res.slab_id=" + intResellerSlabid + " and res.operator_id=b.id and a.operator_id in ("+ strOperatorID + ")");

                        if (ds.Tables[0].Rows.Count > 0)
                        {
                            objCreateFile.CreateTextFile(strIPAddress, strMacAddress, strOSDetails, strIMEINo, strGcmID, strAPPVersion, intSource.ToString(), intCreatedByType, 36, "Get Package Details Admin", strUserName);
                            return ds;

                        }
                        else
                        {
                            return objODBC.getresult("Records not found!!!");
                        }
                    }
                    else
                    {
                        return objODBC.getresult("Slab not available!!!");
                    }

                }
                else
                {
                    return objODBC.getresult("Invalid details");
                }
            }
            catch (Exception ex)
            {
                return objODBC.getresult(ex.Message.ToString());
            }
            finally
            {
                dt.Dispose(); ds.Dispose();
            }
        }

        [WebMethod]
        public DataSet PackageDetailsAdmin_ForMegaSlab(string strUserName, int intParentID, string strPackageName, string strIPAddress, int intDesignationType, string strMacAddress, string strOSDetails, string strIMEINo, string strGcmID, string strAPPVersion, int intSource, int intCreatedByType)
        {
            ArrayList packages = new ArrayList(); DataSet ds = new DataSet();
            try
            {
                if ((intParentID != 0) && (strPackageName != ""))
                {
                    //var intGroupIDD = objODBC.executeScalar_int("SELECT group_id FROM er_commission_slab WHERE parent_id=" + intParentID + " and slab_name ='" + strPackageName + "'");

                    int intGroupIDD = objODBC.executeScalar_int("SELECT id FROM er_slab_group WHERE name='" + strPackageName + "' order by id asc limit 1");

                    if (intGroupIDD > 0)
                    {
                        ds = objODBC.getDataSet("Select a.id, a.operator_id as Operator_ID, a.commission_per as Set_Commission, res.commission_per as My_Commission, b.operator_name as Operator_Name,a.target_amt as Target ,a.target_reward as Reward,a.target_status as Target_Status FROM er_commission_structure a, er_commission_structure res , er_recharge_operator_mobile b, er_commission_slab c where a.operator_id=b.id and c.id=a.slab_id and res.slab_id=c.parent_slab_id and res.operator_id=b.id and a.group_id=" + intGroupIDD + " ");


                        //ds = objODBC.getDataSet("Select a.id, a.operator_id as Operator_ID, a.commission_per as Set_Commission, a.commission_per as My_Commission, b.operator_name as Operator_Name,a.target_amt as Target ,a.target_reward as Reward,a.target_status as Target_Status FROM er_commission_structure a inner join er_recharge_operator_mobile b on a.operator_id=b.id  where a.parent_id=" + intParentID + " and a.group_id=" + intGroupIDD + " order by a.id asc");

                        if (ds.Tables[0].Rows.Count > 0)
                        {
                            objCreateFile.CreateTextFile(strIPAddress, strMacAddress, strOSDetails, strIMEINo, strGcmID, strAPPVersion, intSource.ToString(), intCreatedByType, 36, "Get Package Details Admin", strUserName);
                            return ds;

                        }
                        else
                        {
                            return objODBC.getresult("Records not found!!!");
                        }
                    }
                    else
                    {
                        return objODBC.getresult("Slab not available!!!");
                    }


                }
                else
                {
                    return objODBC.getresult("Invalid details");
                }
            }
            catch (Exception ex)
            {
                return objODBC.getresult(ex.Message.ToString());
            }
            finally
            {
                dt.Dispose(); ds.Dispose();
            }
        }



        [WebMethod]
        public ArrayList er_android_PackageDetailsAdmin(string strUserName, int intParentID, int intSlabID, string strIPAddress, int intDesignationType, string strMacAddress, string strOSDetails, string strIMEINo, string strGcmID, string strAPPVersion, int intSource, int intCreatedByType)
        {
            ArrayList arrlst = new ArrayList();

            try
            {
                ds = PackageDetailsAdmin(strUserName, intParentID, intSlabID, strIPAddress, intDesignationType, strMacAddress, strOSDetails, strIMEINo, strGcmID, strAPPVersion, intSource, intCreatedByType);

                arrlst = objcf.Get_ArrayLlist_From_Dataset(ds);

            }
            catch (Exception)
            {


            }
            finally
            {

            }


            return arrlst;

        }
        // Activity Type=43
        [WebMethod]
        public ArrayList getGroupDetailsDropdown(string strUserName, int intUserID, string strIPAddress, int intDesignationType, string strMacAddress, string strOSDetails, string strIMEINo, string strGcmID, string strAPPVersion, int intSource, int intCreatedByType)
        {
            ArrayList array_defaultSlabs = new ArrayList();
            try
            {

                array_defaultSlabs = objCommonFunction.Get_ArrayLlist("SELECT id as GroupID, group_name as GroupName from er_top_up_group;");


                objCreateFile.CreateTextFile(strIPAddress, strMacAddress, strOSDetails, strIMEINo, strGcmID, strAPPVersion, intSource.ToString(), intCreatedByType, 43, "'get Group Details Dropdown Value Fetch Successfully", strUserName);

                return array_defaultSlabs;

            }
            catch (Exception ex)
            {
                array_defaultSlabs.Add(ex.Message.ToString());
                return array_defaultSlabs;
            }
        }

        //  Activity Type=50
        [WebMethod]
        public ArrayList getMySlab(string strUserName, int intUserID, string strIPAddress, int intDesignationType, string strMacAddress, string strOSDetails, string strIMEINo, string strGcmID, string strAPPVersion, int intSource, int intCreatedByType)
        {
            ArrayList packages = new ArrayList();
            string strResposne = "";
            try
            {
                var intResellerSlabid = objODBC.executeScalar_int("SELECT slab_id from er_slab_user where userid= " + intUserID);
                if (intResellerSlabid > 0)
                {
                    packages = objCommonFunction.Get_ArrayLlist("Select a.id, a.operator_id, a.commission_per as curr_commission_per, b.operator_name,a.target_amt,a.target_reward,a.target_status FROM er_commission_structure a, er_recharge_operator_mobile b where a.operator_id=b.id  and a.parent_id=" + intUserID + " and a.slab_id=" + intResellerSlabid + " and a.operator_id=b.id ");

                    strResposne = "getMySlab Fetch Successfully!";
                }
                else
                {
                    strResposne = "slab Not Available!";
                    packages.Add("slab Not Available!");
                }



                objCreateFile.CreateTextFile(strIPAddress, strMacAddress, strOSDetails, strIMEINo, strGcmID, strAPPVersion, intSource.ToString(), intCreatedByType, 50, strResposne, strUserName);
                return packages;
            }
            catch (Exception ex)
            {
                packages.Add(ex.Message.ToString());
                return packages;
            }
        }

        // ActivityType=49
        [WebMethod]
        public ArrayList getPackageDetailsAdmin(string strUserName, int intParentID, int intSlabID, string strIPAddress, int intDesignationType, string strMacAddress, string strOSDetails, string strIMEINo, string strGcmID, string strAPPVersion, int intSource, int intCreatedByType)
        {
            ArrayList packages = new ArrayList();
            string strResposne = "";
            if (intParentID == 1)
            {
                try
                {
                    var intResellerSlabid = objODBC.executeScalar_int("SELECT slab_id from er_slab_user where userid= " + intParentID);
                    if (intResellerSlabid > 0)
                    {
                        packages = objCommonFunction.Get_ArrayLlist("Select a.id, a.operator_id, a.commission_per as curr_commission_per, res.commission_per as res_commission_per, b.operator_name,a.target_amt,a.target_reward,a.target_status FROM er_commission_structure a, er_commission_structure res , er_recharge_operator_mobile b where a.operator_id=b.id  and a.parent_id=" + intParentID + " and a.slab_id=" + intSlabID + " and res.slab_id=" + intResellerSlabid + " and res.operator_id=b.id ");
                        strResposne = "Package Details Admin fetch successfully";
                    }
                    else
                    {
                        strResposne = "Package Details Admin Slab not Available";
                        packages.Add("slab Not Available!");
                    }
                }
                catch (Exception ex)
                {
                    strResposne = ex.ToString();
                    packages.Add(ex.Message.ToString());
                }
            }
            else
            {
                strResposne = "Kindly Enter Valid Details";
                packages.Add("Kindly Enter Valid Details");
            }


            objCreateFile.CreateTextFile(strIPAddress, strMacAddress, strOSDetails, strIMEINo, strGcmID, strAPPVersion, intSource.ToString(), intCreatedByType, 49, strResposne, strUserName);
            return packages;
        }


        // Activity Type=43
        [WebMethod]
        public DataSet GroupDetailsDropdown(string strUserName, int intUserID, string strIPAddress, int intDesignationType, string strMacAddress, string strOSDetails, string strIMEINo, string strGcmID, string strAPPVersion, int intSource, int intCreatedByType)
        {
            ArrayList array_defaultSlabs = new ArrayList(); DataSet ds = new DataSet();
            try
            {
                int intPID = intUserID;
                if (intSource == 2)
                {
                    intPID = objODBC.executeScalar_int("select parent_id from er_login where userid =" + intUserID + "");
                }

                ds = objODBC.getDataSet("SELECT g.id as GroupID, g.group_name as GroupName,ifnull(count(d.group_id),0) from  er_wallet_transfer_group g  left join er_wallet_transfer_group_details d on g.id = d.group_id where g.parent_id='" + intUserID + "' group by g.id;");

                if (ds.Tables[0].Rows.Count > 0)
                {
                    objCreateFile.CreateTextFile(strIPAddress, strMacAddress, strOSDetails, strIMEINo, strGcmID, strAPPVersion, intSource.ToString(), intCreatedByType, 43, "'get Group Details Dropdown Value Fetch Successfully", strUserName);

                    return ds;
                }
                else
                {
                    return objODBC.getresult("Records not found !!!");
                }
            }
            catch (Exception ex)
            {
                return objODBC.getresult(ex.Message.ToString());
            }
        }

        [WebMethod]
        public ArrayList er_android_GroupDetailsDropdown(string strUserName, int intUserID, string strIPAddress, int intDesignationType, string strMacAddress, string strOSDetails, string strIMEINo, string strGcmID, string strAPPVersion, int intSource, int intCreatedByType)
        {
            ArrayList arlst = new ArrayList();
            ds = GroupDetailsDropdown(strUserName, intUserID, strIPAddress, intDesignationType, strMacAddress + " " + "from android", strOSDetails, strIMEINo, strGcmID, strAPPVersion, intSource, intCreatedByType);

            //int intGrpCount = objODBC.executeScalar_int("select count(1) from er_wallet_transfer_group;");
            arlst = objcf.Get_ArrayLlist_From_Dataset(ds);
            //arlst.Add(intGrpCount);
            return arlst;
        }
        // android GroupDetailsDropdown arraylist


        // Activity Type=44
        [WebMethod]
        public ArrayList er_update_commission(string strUserName, int intUserID, int intNewSlabID, string strIPAddress, int intDesignationType, string strMacAddress, string strOSDetails, string strIMEINo, string strGcmID, string strAPPVersion, int intSource, int intCreatedByType)
        {
            ArrayList array_defaultSlabs = new ArrayList(); DataSet ds = new DataSet();
            try
            {
                try
                {
                    objODBCLOG.executeNonQuery("call activity_logs('" + intUserID + "','" + intDesignationType + "','" + intCreatedByType + "','" + strIPAddress + "','" + strMacAddress + "','" + strOSDetails + "','" + strIMEINo + "','" + strGcmID + "','" + strAPPVersion + "','" + intSource + "','Update Commission',44,'','')");

                    objCreateFile.CreateTextFile(strIPAddress, strMacAddress, strOSDetails, strIMEINo, strGcmID, strAPPVersion, intSource.ToString(), intCreatedByType, 43, "er_update_commission", strUserName);
                }
                catch (Exception)
                {

                }
                objODBC.executeNonQuery("call er_update_commission_slab(" + intUserID + "," + intNewSlabID + ")");
                array_defaultSlabs.Add("Slab updated successfully");
                return array_defaultSlabs;

            }
            catch (Exception ex)
            {
                array_defaultSlabs.Add("somthing is wrong");
                return array_defaultSlabs;
            }
        }

        [WebMethod]
        public string er_update_mega_package(int intID, double dblcomm, double dblTrgtAmt, int intTrgStatus, double dblTrgtReward, string strUserName, int intParentID, string strIPAddress, int intDesignationType, string strMacAddress, string strOSDetails, string strIMEINo, string strGcmID, string strAPPVersion, int intSource, int intCreatedByType)
        {
            string strResult = string.Empty;
            ArrayList packages = new ArrayList(); DataSet ds = new DataSet();
            try
            {

                if ((intParentID != 0) && (dblcomm >= 0))
                {
                    try
                    {
                        objCreateFile.CreateTextFile(strIPAddress, strMacAddress, strOSDetails, strIMEINo, strGcmID, strAPPVersion, intSource.ToString(), intCreatedByType, 36, "er_update_mega_package", strUserName);

                    }
                    catch (Exception)
                    {

                    }
                    finally
                    {
                        objODBC.executeNonQuery("update er_commission_structure set commission_per=" + dblcomm + ",target_amt=" + dblTrgtAmt + ",target_reward=" + dblTrgtReward + ",target_status=" + intTrgStatus + " where id=" + intID + "");

                        strResult = "Slab updated successfully";


                    }
                }
                else
                {
                    return strResult;
                }
            }
            catch (Exception ex)
            {
                return ex.ToString();
            }
            return strResult;
        }

        [WebMethod]
        public string er_update_Slab(int intUserID, int intSlabType, int intSlabID, string strIPAddress, int intDesignationType, string strMacAddress, string strOSDetails, string strIMEINo, string strGcmID, string strAPPVersion, int intSource, int intCreatedByType)
        {
            string strResult = string.Empty;
            ArrayList packages = new ArrayList(); DataSet ds = new DataSet();
            try
            {

                if ((intSlabType >= 0) && (intUserID > 0))
                {
                    try
                    {
                        objCreateFile.CreateTextFile(strIPAddress, strMacAddress, strOSDetails, strIMEINo, strGcmID, strAPPVersion, intSource.ToString(), intCreatedByType, 36, "update Slab", "Admin");

                    }
                    catch (Exception)
                    {

                    }
                    finally
                    {
                        if(intSlabType==1)
                        {
                            intSlabType = 2;
                        }

                        objODBC.executeNonQuery("call er_update_slab(" + intSlabType + "," + intUserID + "," + intSlabID + ")");

                        strResult = "Slab updated successfully";

                    }
                }
                else
                {
                    return strResult;
                }
            }
            catch (Exception ex)
            {
                return ex.ToString();
            }
            return strResult;
        }


        // Activity Type=43
        [WebMethod]
        public DataSet GetSystemStaibleBalance(string strUserName, int intUserID, string strIPAddress, int intDesignationType, string strMacAddress, string strOSDetails, string strIMEINo, string strGcmID, string strAPPVersion, int intSource, int intCreatedByType)
        {
            ArrayList array_defaultSlabs = new ArrayList();
            try
            {
                string strQuery = "Select (SELECT IFNULL(sum(ex_wallet), 0)  FROM er_wallet where userid>1) as UserWallet,(SELECT ex_wallet FROM er_wallet where userid=1) as AdminWallet,(SELECT sum(ex_wallet) FROM er_wallet) as TotalWallet";              
                ds = objODBC.Report(strQuery);
                return ds;
            }
            catch (Exception ex)
            {
                return ds;
            }
            finally
            {
                objCreateFile.CreateTextFile(strIPAddress, strMacAddress, strOSDetails, strIMEINo, strGcmID, strAPPVersion, intSource.ToString(), intCreatedByType, 43, "System Staible Balance Generate EMoney", strUserName);
                ds.Dispose();

            }
        }
    }
}
