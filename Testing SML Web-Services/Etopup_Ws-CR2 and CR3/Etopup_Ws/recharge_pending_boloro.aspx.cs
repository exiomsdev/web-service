﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Etopup_Ws.old_App_Code;
using System.Data;
using System.Xml;
using System.Text.RegularExpressions;
using Newtonsoft.Json.Linq;

namespace Etopup_Ws
{
    public partial class recharge_pending_boloro : System.Web.UI.Page
    {
        DateFormat objDF = new DateFormat();
        ODBC objODBC = new ODBC();
        ODBC_LOG objODBCLOG = new ODBC_LOG();
        DataSet ds = new DataSet();
        DataSet ds1 = new DataSet();
        DataSet dsData = new DataSet();
        CommonFunction objCOMFUN = new CommonFunction();
        protected void Page_Load(object sender, EventArgs e)
        {

            try
            {


                try
                {
                    DateFormat objDF = new DateFormat();
                    //  objODBC.executeNonQuery("INSERT INTO `temp`(`data`, `type`, `created_on`, `request`) VALUES ('Windows Services ',1,'" + objDF.getCurDateTimeString() + "','Recharge_pending_request')");
                    try
                    {
                        GetPendingRechargeDetails_Boloro();
                    }
                    catch (Exception)
                    {

                    }

                    // PendingSMSsendAgain();
                }
                catch (Exception)
                {

                }

            }
            catch (Exception)
            {

            }
            Response.ContentType = "text/plain";
            Response.Write("Exicute Successfully !");
            Response.End();
        }
        public string TruncateAtWord(string input, int length)
        {
            if (input == null || input.Length < length)
                return input;
            int iNextSpace = input.LastIndexOf("", length, StringComparison.Ordinal);
            return string.Format("{0}", input.Substring(0, (iNextSpace > 0) ? iNextSpace : length).Trim());
        }



        public void GetPendingRechargeDetails_Boloro()
        {
            try
            {
                string strDate2 = objDF.getCurDateTimeString();
                ds = objODBC.getDataSet("Select id,userid,trans_number,operator_id,operator_name,deduct_amt,api_type,Operater_table_id,amount,mobile_number,request_mobile_no from er_recharge where status=1 and  api_type=5  And created_on < DATE_SUB('" + strDate2 + "',INTERVAL 30 SECOND) order by id asc limit 10;");
                int intCount = Convert.ToInt32((ds.Tables[0].Rows.Count));
                if (intCount > 0)
                {
                    for (int i = 0; i <= intCount; i++)
                    {
                        int intInvestmentID = int.Parse(ds.Tables[0].Rows[i]["id"].ToString());
                        int intUserID = int.Parse(ds.Tables[0].Rows[i]["userid"].ToString());
                        string strTransactionNumber = ds.Tables[0].Rows[i]["id"].ToString();
                        string strOperaterID = ds.Tables[0].Rows[i]["operator_id"].ToString();
                        string strOperaterName = ds.Tables[0].Rows[i]["operator_name"].ToString();
                        double dblDeductAmt = double.Parse(ds.Tables[0].Rows[i]["deduct_amt"].ToString());
                        string api_type = ds.Tables[0].Rows[i]["api_type"].ToString();
                        string strOperaterTableID = ds.Tables[0].Rows[i]["Operater_table_id"].ToString();
                        double dblActualAmt = double.Parse(ds.Tables[0].Rows[i]["amount"].ToString());
                        string strMobile = ds.Tables[0].Rows[i]["mobile_number"].ToString();
                        string strPhone = ds.Tables[0].Rows[i]["request_mobile_no"].ToString();

                        if (api_type == "5") // Boloro
                        {
                            //  { "response_code":1,"response_desc":"Operation successful","data":{ "transaction_no":"5e09b2c14d388","status_code":"160","status_desc":"In Process Initiating Topup request","msisdn":"93778881117","amount":"13","sequence_no":"5e09b2f91711c"} }
                            //   { "response_code":1,"response_desc":"Operation successful","data":{ "transaction_no":"5e09c76598718","status_code":"170","status_desc":"In Process Topup Successful","msisdn":"93778881117","amount":"13","sequence_no":"5e09c788b6c9f"} }
                            //{"response_code":2,"response_desc":"Operation failed","data":{"errors":["AWCC telco is currently disabled."],"sequence_no":"5e4d099ed6ca1"}}

                            string strTransactionID = objODBC.executeScalar_str("SELECT transaction_no FROM er_boloro_api WHERE recharge_id =" + intInvestmentID + " order by id desc limit 1");
                            BoloroApi objBoloroAPi = new BoloroApi();
                            if (strTransactionID == "NA")
                            {
                                DataTable dt = new DataTable();
                                try
                                {
                                    strDate2 = objDF.getCurDateTimeString();
                                    string strQ = "SELECT topup_request,topup_response FROM er_boloro_api WHERE recharge_id =" + intInvestmentID + " And created_on < DATE_SUB('" + strDate2 + "',INTERVAL 3 MINUTE) order by id asc limit 1";
                                    dt = objODBC.getDataTable(strQ);
                                    if (dt.Rows.Count > 0)
                                    {
                                        if (dt.Rows[0][0].ToString() == "NA" || dt.Rows[0][1].ToString() == "NA")
                                        {

                                            try
                                            {
                                                objODBC.executeNonQuery("call `updateResponseV3`('" + intInvestmentID + "','" + intInvestmentID + "',2)");
                                            }
                                            catch (Exception)
                                            {

                                            }
                                            string BolotoResponse = "Response=No Response Details";
                                            try
                                            {
                                                objODBC.executeNonQuery("update er_recharge set comment='" + BolotoResponse + "' where id='" + intInvestmentID + "'");

                                                objODBC.executeNonQuery("Update er_boloro_api set `check_t_request`='NA',`check_t_response`='" + BolotoResponse + "',`modifyed_on`='" + objDF.getCurDateTimeString() + "',`status`=2 where id='" + strOperaterTableID + "'");
                                            }
                                            catch (Exception)
                                            {

                                            }

                                            try
                                            {
                                                Recharge objRecharge = new Recharge();
                                                objRecharge.SendSMPPSMS(intUserID, objODBC.executeScalar_str("select username FROM `er_login` where userid='" + intUserID + "'"), strMobile, strOperaterName, int.Parse(strOperaterID), dblActualAmt, 2, strTransactionNumber.ToString(), strPhone);

                                                //if (strOperaterID == "4")
                                                //{
                                                //    SMPP_SMS_Format objSMPP_SMS_Format = new SMPP_SMS_Format();
                                                //    CommonFunction objCOMFUN = new CommonFunction();
                                                //    string strGetSMS = objSMPP_SMS_Format.TopupFailedEndUser(objODBC.executeScalar_str("SELECT full_name From er_login Where userid='" + intUserID + "'").ToString(), strMobile, dblActualAmt, strTransactionNumber.ToString());
                                                //    objCOMFUN.er_SMS_Alert(intUserID, 2, strMobile, int.Parse(strOperaterID), strGetSMS, 9, "");
                                                //}
                                            }
                                            catch (Exception)
                                            {

                                            }
                                        }

                                    }
                                }
                                catch (Exception)
                                {

                                }
                                finally
                                {
                                    dt.Dispose();
                                }
                            }
                            else
                            {
                                string BolotoResponse = objBoloroAPi.Boloro_Check_Transaction(strTransactionID, intUserID, intInvestmentID, strOperaterTableID);
                                try
                                {
                                    string strRequest_Code = "";
                                    string strResponse_desc = "";
                                    string cttransaction_no = "";
                                    string ctstatus_code = "";
                                    string ctstatus_desc = "";
                                    string ctmsisdn = "";
                                    string ctamount = "";
                                    string ctsequence_no = "";

                                    if (BolotoResponse.ToString() != "NA")
                                    {
                                        try
                                        {
                                            dynamic Tdata = JObject.Parse(BolotoResponse);
                                            strRequest_Code = Tdata.response_code;
                                            strResponse_desc = Tdata.response_desc;
                                            // string strResponseData = data.data;
                                            cttransaction_no = Tdata["data"]["transaction_no"].ToString();
                                            ctstatus_code = Tdata["data"]["status_code"].ToString();
                                            ctstatus_desc = Tdata["data"]["status_desc"].ToString();
                                            ctmsisdn = Tdata["data"]["msisdn"].ToString();
                                            ctamount = Tdata["data"]["amount"].ToString();
                                            ctsequence_no = Tdata["data"]["sequence_no"].ToString();

                                        }
                                        catch (Exception)
                                        {

                                        }
                                                                               
                                        if (ctstatus_code == "170" && ctstatus_desc == "In Process Topup Successful")
                                        {
                                            try
                                            {
                                                objODBC.executeNonQuery("call `updateResponseV3`('" + cttransaction_no + "','" + intInvestmentID + "',1)");
                                            }
                                            catch (Exception)
                                            {

                                            }
                                            //    objODBC.executeNonQuery(" insert into temp(`data`,`type`,`created_on`,`request`) values(" + strPayID + "," + intInvestmentID + ",'2019-01-29 19:58:58','5')");

                                            BolotoResponse = "Response=@" + BolotoResponse.ToString();
                                            objODBC.executeNonQuery("update er_recharge set comment='" + BolotoResponse + "',ap_transid='" + cttransaction_no + "',operator_transid='" + cttransaction_no + "' where id='" + intInvestmentID + "'");

                                            objODBC.executeNonQuery("Update er_boloro_api set `check_t_response`='" + BolotoResponse + "',`transaction_no`='" + strTransactionID + "',`modifyed_on`='" + objDF.getCurDateTimeString() + "',`check_t_response_code`='" + strRequest_Code + "',`check_t_response_desc`='" + strResponse_desc + "',`check_t_transaction_no`='" + cttransaction_no + "',`check_t_status_code`='" + ctstatus_code + "',`check_t_status_desc`='" + ctstatus_desc + "',`check_t_response_sequence_no`='" + ctsequence_no + "',`status`=1 where id='" + strOperaterTableID + "'");

                                            try
                                            {
                                                Recharge objRecharge = new Recharge();
                                                objRecharge.SendSMPPSMS(intUserID, objODBC.executeScalar_str("select username FROM `er_login` where userid='" + intUserID + "'"), strMobile, strOperaterName, int.Parse(strOperaterID), dblActualAmt, 1, strTransactionNumber.ToString(), strPhone);

                                                //if (strOperaterID == "4")
                                                //{
                                                //    SMPP_SMS_Format objSMPP_SMS_Format = new SMPP_SMS_Format();
                                                //    CommonFunction objCOMFUN = new CommonFunction();
                                                //    string strGetSMS = objSMPP_SMS_Format.TopupSuccessEndUser(objODBC.executeScalar_str("SELECT full_name From er_login Where userid='" + intUserID + "'").ToString(), strMobile, dblActualAmt, strTransactionNumber.ToString());
                                                //    objCOMFUN.er_SMS_Alert(intUserID, 2, strMobile, int.Parse(strOperaterID), strGetSMS, 9, "");
                                                //}
                                            }
                                            catch (Exception)
                                            {

                                            }

                                        }
                                        else if (ctstatus_code == "1000" && ctstatus_desc == "Closed (Completed Successfully)")
                                        {
                                            try
                                            {
                                                objODBC.executeNonQuery("call `updateResponseV3`('" + cttransaction_no + "','" + intInvestmentID + "',1)");
                                            }
                                            catch (Exception)
                                            {

                                            }
                                            //    objODBC.executeNonQuery(" insert into temp(`data`,`type`,`created_on`,`request`) values(" + strPayID + "," + intInvestmentID + ",'2019-01-29 19:58:58','5')");

                                            BolotoResponse = "Response=@" + BolotoResponse.ToString();
                                            objODBC.executeNonQuery("update er_recharge set comment='" + BolotoResponse + "',ap_transid='" + cttransaction_no + "',operator_transid='" + cttransaction_no + "' where id='" + intInvestmentID + "'");
                                            objODBC.executeNonQuery("Update er_boloro_api set `check_t_response`='" + BolotoResponse + "',`transaction_no`='" + strTransactionID + "',`modifyed_on`='" + objDF.getCurDateTimeString() + "',`check_t_response_code`='" + strRequest_Code + "',`check_t_response_desc`='" + strResponse_desc + "',`check_t_transaction_no`='" + cttransaction_no + "',`check_t_status_code`='" + ctstatus_code + "',`check_t_status_desc`='" + ctstatus_desc + "',`check_t_response_sequence_no`='" + ctsequence_no + "',`status`=1 where id='" + strOperaterTableID + "'");

                                            try
                                            {
                                                Recharge objRecharge = new Recharge();
                                                objRecharge.SendSMPPSMS(intUserID, objODBC.executeScalar_str("select username FROM `er_login` where userid='" + intUserID + "'"), strMobile, strOperaterName, int.Parse(strOperaterID), dblActualAmt, 1, strTransactionNumber.ToString(), strPhone);

                                                //if (strOperaterID == "4")
                                                //{
                                                //    SMPP_SMS_Format objSMPP_SMS_Format = new SMPP_SMS_Format();
                                                //    CommonFunction objCOMFUN = new CommonFunction();
                                                //    string strGetSMS = objSMPP_SMS_Format.TopupSuccessEndUser(objODBC.executeScalar_str("SELECT full_name From er_login Where userid='" + intUserID + "'").ToString(), strMobile, dblActualAmt, strTransactionNumber.ToString());
                                                //    objCOMFUN.er_SMS_Alert(intUserID, 2, strMobile, int.Parse(strOperaterID), strGetSMS, 9, "");
                                                //}
                                            }
                                            catch (Exception)
                                            {

                                            }
                                        }
                                        else if (int.Parse(ctstatus_code) > 1000)
                                        {
                                            try
                                            {
                                                objODBC.executeNonQuery("call `updateResponseV3`('" + cttransaction_no + "','" + intInvestmentID + "',2)");
                                            }
                                            catch (Exception)
                                            {

                                            }
                                            BolotoResponse = "Response=@" + BolotoResponse.ToString();
                                            objODBC.executeNonQuery("update er_recharge set comment='" + BolotoResponse + "',ap_transid='" + cttransaction_no + "',operator_transid='" + cttransaction_no + "' where id='" + intInvestmentID + "'");

                                            objODBC.executeNonQuery("Update er_boloro_api set `check_t_request`='NA',`check_t_response`='" + BolotoResponse + "',`transaction_no`='" + strTransactionID + "',`modifyed_on`='" + objDF.getCurDateTimeString() + "',`check_t_response_code`='" + strRequest_Code + "',`check_t_response_desc`='" + strResponse_desc + "',`check_t_transaction_no`='" + cttransaction_no + "',`check_t_status_code`='" + ctstatus_code + "',`check_t_status_desc`='" + ctstatus_desc + "',`check_t_response_sequence_no`='" + ctsequence_no + "',`status`=2 where id='" + strOperaterTableID + "'");

                                            try
                                            {
                                                Recharge objRecharge = new Recharge();
                                                objRecharge.SendSMPPSMS(intUserID, objODBC.executeScalar_str("select username FROM `er_login` where userid='" + intUserID + "'"), strMobile, strOperaterName, int.Parse(strOperaterID), dblActualAmt, 2, strTransactionNumber.ToString(), strPhone);

                                                //if (strOperaterID == "4")
                                                //{
                                                //    SMPP_SMS_Format objSMPP_SMS_Format = new SMPP_SMS_Format();
                                                //    CommonFunction objCOMFUN = new CommonFunction();
                                                //    string strGetSMS = objSMPP_SMS_Format.TopupFailedEndUser(objODBC.executeScalar_str("SELECT full_name From er_login Where userid='" + intUserID + "'").ToString(), strMobile, dblActualAmt, strTransactionNumber.ToString());
                                                //    objCOMFUN.er_SMS_Alert(intUserID, 2, strMobile, int.Parse(strOperaterID), strGetSMS, 9, "");
                                                //}
                                            }
                                            catch (Exception)
                                            {

                                            }
                                        }



                                    }

                                }
                                catch (Exception)
                                {

                                }
                            }

                        }

                    }
                }
            }
            catch (Exception)
            {

            }
            finally
            {
                ds.Dispose();
            }
        }

        public void PendingSMSsendAgain()
        {
            try
            {

                try
                {
                    dsData = objODBCLOG.getDataSet("SELECT `id`, `userid`, `username`, `mobile`, `operator_id`, `SMS`, `created_on`, `status`, `mobile1`, `operator_id1`, `mobile2`, `operator_id2` FROM `er_wallet_transfer_sms_log` WHERE status=0 order by id asc limit 150;");
                    if (dsData.Tables[0].Rows.Count > 0)
                    {
                        for (int i = 0; i < dsData.Tables[0].Rows.Count; i++)
                        {

                            string strSMPPResponse = "NA";
                            try
                            {
                                objCOMFUN.WriteToSMSFile(int.Parse(dsData.Tables[0].Rows[i]["userid"].ToString()), 2, dsData.Tables[0].Rows[i]["mobile"].ToString(), int.Parse(dsData.Tables[0].Rows[i]["operator_id"].ToString()), dsData.Tables[0].Rows[i]["SMS"].ToString(), 1, strSMPPResponse, dsData.Tables[0].Rows[i]["mobile1"].ToString(), dsData.Tables[0].Rows[i]["operator_id1"].ToString(), dsData.Tables[0].Rows[i]["mobile2"].ToString(), dsData.Tables[0].Rows[i]["operator_id2"].ToString());

                                // objComFUN.er_SMS_Alert(int.Parse(dsData.Tables[0].Rows[i]["userid"].ToString()), 2, dsData.Tables[0].Rows[i]["mobile"].ToString(), int.Parse(dsData.Tables[0].Rows[i]["operator_id1"].ToString()),dsData.Tables[0].Rows[i]["SMS"].ToString(), 1, strSMPPResponse);

                                objODBCLOG.executeNonQuery("Update er_wallet_transfer_sms_log set status=1 where id=" + int.Parse(dsData.Tables[0].Rows[i]["id"].ToString()));
                            }
                            catch (Exception)
                            {

                            }
                        }
                    }

                }
                catch (Exception)
                {

                }
                finally
                {
                    dsData.Dispose();
                }
            }
            catch (Exception)
            {


            }
            finally
            {
                dsData.Dispose();
            }
        }
    }
}