﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Etopup_Ws.old_App_Code;
using System.Data;
using System.Xml;
using System.Text.RegularExpressions;
using RestSharp;
using Etopup_Ws.old_App_Code;

namespace Etopup_Ws
{
    public partial class reverse_topup_script : System.Web.UI.Page
    {
        old_App_Code.DateFormat objDF = new old_App_Code.DateFormat();
        ODBC objODBC = new ODBC();
        DataSet ds = new DataSet();
        DataSet ds1 = new DataSet();
        protected void Page_Load(object sender, EventArgs e)
        {
            /*  try
              {
                  DateFormat objDF = new DateFormat();
                  objODBC.executeNonQuery("INSERT INTO `temp`(`data`, `type`, `created_on`, `request`) VALUES ('Windows Services ',6,'" + objDF.getCurDateTimeString() + "','reverse_topup_script')");
              }
              catch (Exception)
              {

              }
              */
            try
            {
                GetPendingRechargeDetails();
            }
            catch (Exception)
            {

            }
            Response.ContentType = "text/plain";
            Response.Write("Exicute Successfully !");
            Response.End();
        }
        public string TruncateAtWord(string input, int length)
        {
            if (input == null || input.Length < length)
                return input;
            int iNextSpace = input.LastIndexOf("", length, StringComparison.Ordinal);
            return string.Format("{0}", input.Substring(0, (iNextSpace > 0) ? iNextSpace : length).Trim());
        }

        public void GetPendingRechargeDetails()
        {
            try
            {
                ds = objODBC.getDataSet("SELECT t.id,t.userid,t.recharge_id,t.topup_mobile,t.amount,t.Transaction_Response,t.ReverseID, t.ReverseID_operaterID FROM etisalat_topup_request t inner join etisalat_reverse_topup r on t.ReverseID=r.id WHERE t.transaction_type=2 and r.status=0 order by t.id asc limit 10;");
                int intCount = Convert.ToInt32((ds.Tables[0].Rows.Count));
                if (intCount > 0)
                {
                    for (int i = 0; i <= intCount; i++)
                    {
                        int strOperaterTableID = int.Parse(ds.Tables[0].Rows[i]["id"].ToString());
                        int intUserID = int.Parse(ds.Tables[0].Rows[i]["userid"].ToString());
                        string intInvestmentID = ds.Tables[0].Rows[i]["recharge_id"].ToString();
                        string strMobile = ds.Tables[0].Rows[i]["topup_mobile"].ToString();
                        string dblActualAmt = ds.Tables[0].Rows[i]["amount"].ToString();
                        string EtisalatResponse = ds.Tables[0].Rows[i]["Transaction_Response"].ToString();
                        string intReverseID = ds.Tables[0].Rows[i]["ReverseID"].ToString();
                        string strReverseID_operaterID = ds.Tables[0].Rows[i]["ReverseID_operaterID"].ToString();


                        if (EtisalatResponse.ToString() != "NA")
                        {
                            if (Regex.IsMatch(EtisalatResponse, string.Format(@"\b{0}\b", Regex.Escape("RLLBCK CHRG VOUCH:RETN=0000,DESC=SUCCESS")))) // Success
                            {
                                string strResposne = TruncateAtWord(EtisalatResponse, 64); // Truncate From Position of Words
                                string output = EtisalatResponse.Replace(strResposne, ""); // Replace Words From Data
                                output = output.Remove(output.Length - 8); // Deduct Words from Last                   
                                string StrCheckSum = EtisalatResponse.Substring(EtisalatResponse.Length - 8); // Getting Checksum

                                string[] strDescriptionResponse = output.Split(new char[] { ',' });
                                // strMessage = "2,OOPs! You do not have sufficient balance.,Failed,NA,NA";

                                string TRDStatus = strDescriptionResponse[0];
                                string TRDDescription = strDescriptionResponse[1];

                                string TXNID = objODBC.executeScalar_str("SELECT transaction_ID_recharge FROM etisalat_topup_request WHERE id='" + strOperaterTableID + "'").ToString();

                                objODBC.executeNonQuery("update er_recharge set getStatus=0,status=1 where id='" + intInvestmentID + "'");
                                try
                                {
                                    objODBC.getDataTable("call `ap_rollbackRechargeProcessing`('" + intInvestmentID + "','1')");
                                    objODBC.executeNonQuery("call `ap_rollbackRechargeComplete`('" + intUserID + "','" + intInvestmentID + "','" + dblActualAmt + "')");
                                    // objODBC.executeNonQuery("call `updateResponseV3`('" + TXNID + "','" + intInvestmentID + "',4)");
                                    
                                }
                                catch (Exception)
                                {

                                }

                                try
                                {
                                    string strBalance = objODBC.executeScalar_str("SELECT amt FROM `er_sys_wallet` where id=2;");
                                    objODBC.executeNonQuery("update er_recharge set comment='" + EtisalatResponse + "',operator_balance='" + strBalance + "' where id='" + intInvestmentID + "'");
                                    objODBC.executeNonQuery("update etisalat_topup_request set status=1 WHERE recharge_id =" + intInvestmentID + "");
                                    objODBC.executeNonQuery("update etisalat_reverse_topup set resposne='"+ EtisalatResponse + "',status=2,api_Operater_ID='"+ strReverseID_operaterID + "',etisalat_table_topup_ID='"+ strOperaterTableID + "' WHERE recharge_id =" + intInvestmentID + "");

                                }
                                catch (Exception)
                                {

                                }

                                try
                                {
                                    objODBC.executeNonQuery("update etisalat_topup_request set transaction_DESC='" + TRDDescription + "',Transactio_Status='" + TRDStatus + "',Trans_Receive_Checksum='" + StrCheckSum + "',status=1 WHERE recharge_id =" + intInvestmentID + "");
                                }
                                catch (Exception)
                                {

                                }


                                try
                                {
                                    Recharge objRecharge = new Recharge();
                                    objRecharge.SendSMPPSMS(intUserID, objODBC.executeScalar_str("select username FROM `er_login` where userid='" + intUserID + "'"), strMobile, "Etisalat", 2, double.Parse(dblActualAmt), 5, intReverseID.ToString(),"0"); // 5 Success Reverse
                                }
                                catch (Exception)
                                {

                                }
                            }
                            else // FAIL
                            {
                                string strResposne = TruncateAtWord(EtisalatResponse, 64); // Truncate From Position of Words
                                string output = EtisalatResponse.Replace(strResposne, ""); // Replace Words From Data
                                output = output.Remove(output.Length - 8); // Deduct Words from Last                   
                                string StrCheckSum = EtisalatResponse.Substring(EtisalatResponse.Length - 8); // Getting Checksum

                                string[] strDescriptionResponse = output.Split(new char[] { ',' });
                                // strMessage = "2,OOPs! You do not have sufficient balance.,Failed,NA,NA";

                                string TRDStatus = strDescriptionResponse[0];
                                string TRDDescription = strDescriptionResponse[1];


                                string TXNID = objODBC.executeScalar_str("SELECT transaction_ID_recharge FROM etisalat_topup_request WHERE id='" + strOperaterTableID + "'").ToString();


                                try
                                {
                                    string strBalance = objODBC.executeScalar_str("SELECT amt FROM `er_sys_wallet` where id=2;");
                                    objODBC.executeNonQuery("update er_recharge set operator_balance='" + strBalance + "' where id='" + intInvestmentID + "'");
                                    objODBC.executeNonQuery("update etisalat_topup_request set status=2 WHERE recharge_id =" + intInvestmentID + "");
                                    objODBC.executeNonQuery("call `ap_rollbackRechargeProcessing`('" + intInvestmentID + "','2')");
                                    objODBC.executeNonQuery("update etisalat_reverse_topup set resposne='" + EtisalatResponse + "',status=1,api_Operater_ID='" + strReverseID_operaterID + "',etisalat_table_topup_ID='" + strOperaterTableID + "' WHERE recharge_id =" + intInvestmentID + " where status=0");

                               
                                }
                                catch (Exception)
                                {

                                }

                                try
                                {
                                    objODBC.executeNonQuery("update etisalat_topup_request set transaction_DESC='" + TRDDescription + "',Transactio_Status='" + TRDStatus + "',Trans_Receive_Checksum='" + StrCheckSum + "',status=2 WHERE recharge_id =" + intInvestmentID + "");
                                }
                                catch (Exception)
                                {

                                }

                                try
                                {
                                    Recharge objRecharge = new Recharge();
                                    objRecharge.SendSMPPSMS(intUserID, objODBC.executeScalar_str("select username FROM `er_login` where userid='" + intUserID + "'"), strMobile, "Etisalat", 2, double.Parse(dblActualAmt), 6, intReverseID.ToString(),"0"); //// 6 FAILED Reverse
                                }
                                catch (Exception)
                                {

                                }
                            }
                        }                       

                    }
                }
            }
            catch (Exception)
            {

            }
            finally
            {
                ds.Dispose();
            }
        }
    }
}