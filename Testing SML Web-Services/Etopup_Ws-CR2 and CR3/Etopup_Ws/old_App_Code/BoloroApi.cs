﻿using RestSharp;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Data;
using Etopup_Ws.old_App_Code;
using System.Xml;
using Newtonsoft.Json.Linq;

namespace Etopup_Ws.old_App_Code
{
    public class BoloroApi
    {
        //        Merchant code: 297262
        //API login: API_a
        //API Key: fVF4v8burQG7a

        //Terminal Id:

        //297262_2
        //297262_W


       // Ahttp://10.120.0.20:8020/index.php/merchantapi/v1/login
        string terminal_id = "297262_2";
        // string terminal_id = "297262_W";
        string API_17656p = "API_a";
        string api_key = "fVF4v8burQG7a";
        string merchant_code = "297262";
        DateFormat objDF = new DateFormat();
        ODBC objODBC = new ODBC();


        /**************** Check Transaction *********************/
        public Tuple<string, string> getLoginSequenceTransactionCheck()
        {
            terminal_id = "297262_W";
            var RequestJSONLogin = "{ \"terminal_id\": \"" + terminal_id + "\", \"api_login\": \"" + API_17656p + "\", \"api_key\": \"" + api_key + "\", \"merchant_code\": \"" + merchant_code + "\"}";
            var client = new RestClient("http://10.120.0.20:8020/index.php/merchantapi/v1/login");
            var request = new RestRequest(Method.POST);
            request.AddHeader("Postman-Token", "1d0c7ce1-bd0e-48d6-8738-2e0cdcc2af1e");
            request.AddHeader("cache-control", "no-cache");
            request.AddHeader("content-type", "multipart/form-data; boundary=----WebKitFormBoundary7MA4YWxkTrZu0gW");
            request.AddParameter("multipart/form-data; boundary=----WebKitFormBoundary7MA4YWxkTrZu0gW", "------WebKitFormBoundary7MA4YWxkTrZu0gW\r\nContent-Disposition: form-data; name=\"data\"\r\n\r\n" + RequestJSONLogin + "\r\n------WebKitFormBoundary7MA4YWxkTrZu0gW--", ParameterType.RequestBody);
            IRestResponse response = client.Execute(request);
            string JSONDatalogin = response.Content.ToString();

            return new Tuple<string, string>(RequestJSONLogin, JSONDatalogin);
        }


        public string Boloro_Check_Transaction(string strTransactionNumber, int intUserID, int intRechargeID, string intOperatorID)
        {
            terminal_id = "297262_W";
            try
            {
                var tuple = getLoginSequenceTransactionCheck();
                string strLoginRequest = tuple.Item1;
                string JSONDatalogin = tuple.Item2;

                //string strLoginRequest = objODBC.executeScalar_str("SELECT `data` FROM `temp` where id=1");
                //string JSONDatalogin = objODBC.executeScalar_str("SELECT `data` FROM `temp` where id=2");

                string strAccessToken = "";
                string strSequenceNo = "";
                try
                {
                    dynamic data = JObject.Parse(JSONDatalogin);
                    string strRequest_Code = data.response_code;
                    string strResponse_desc = data.response_desc;
                    // string strResponseData = data.data;
                    strAccessToken = data["data"]["access_token"].Value;
                    strSequenceNo = data["data"]["sequence_no"].Value;

                }
                catch (Exception)
                {

                }

                if (strAccessToken != "" && strSequenceNo != "")
                {

                    //  string varRequestJSONTopupCheckTransaction = objODBC.executeScalar_str("SELECT `data` FROM `temp` where id=17");
                    //     string JSONCheck_TransactionData = objODBC.executeScalar_str("SELECT `data` FROM `temp` where id=17");

                    var varRequestJSONTopupCheckTransaction = "{\"terminal_id\":\"" + terminal_id + "\",\"access_token\": \"" + strAccessToken + "\",\"merchant_code\":\"" + merchant_code + "\",\"sequence_no\":\"" + strSequenceNo + "\",\"transaction_no\":\"" + strTransactionNumber + "\"}";

                    var client = new RestClient("http://10.120.0.20:8020/index.php/merchantapi/v1/gettransactionupdate");
                    var request = new RestRequest(Method.POST);
                    request.AddHeader("Postman-Token", "7944ca3e-b9d7-4455-9992-de773f18c9eb");
                    request.AddHeader("cache-control", "no-cache");
                    request.AddHeader("content-type", "multipart/form-data; boundary=----WebKitFormBoundary7MA4YWxkTrZu0gW");
                    request.AddParameter("multipart/form-data; boundary=----WebKitFormBoundary7MA4YWxkTrZu0gW", "------WebKitFormBoundary7MA4YWxkTrZu0gW\r\nContent-Disposition: form-data; name=\"data\"\r\n\r\n" + varRequestJSONTopupCheckTransaction + "\r\n------WebKitFormBoundary7MA4YWxkTrZu0gW--", ParameterType.RequestBody);
                    IRestResponse response = client.Execute(request);
                    string JSONCheck_TransactionData = response.Content.ToString();

                    try
                    {


                        // {"response_code":1,"response_desc":"Operation successful","data":{"transaction_no":"5e09b2c14d388","status_code":"1130","status_desc":"Closed (Duplicate TXN within 3 minutes)","msisdn":"93778881117","amount":"13","sequence_no":"5e09c6fce5485"}}

                        //   { "response_code":1,"response_desc":"Operation successful","data":{ "transaction_no":"5e09b2c14d388","status_code":"160","status_desc":"In Process Initiating Topup request","msisdn":"93778881117","amount":"13","sequence_no":"5e09b2f91711c"} }
                        //  { "response_code":1,"response_desc":"Operation successful","data":{ "transaction_no":"5e09c76598718","status_code":"170","status_desc":"In Process Topup Successful","msisdn":"93778881117","amount":"13","sequence_no":"5e09c788b6c9f"} }



                        string cttransaction_no = "";
                        string ctstatus_code = "";
                        string ctstatus_desc = "";
                        string ctmsisdn = "";
                        string ctamount = "";
                        string ctsequence_no = "";
                        try
                        {
                            dynamic data = JObject.Parse(JSONCheck_TransactionData);
                            string strRequest_Code = data.response_code;
                            string strResponse_desc = data.response_desc;
                            // string strResponseData = data.data;
                            cttransaction_no = data["data"]["transaction_no"].ToString();
                            ctstatus_code = data["data"]["status_code"].ToString();
                            ctstatus_desc = data["data"]["status_desc"].ToString();
                            ctmsisdn = data["data"]["msisdn"].ToString();
                            ctamount = data["data"]["amount"].ToString();
                            ctsequence_no = data["data"]["sequence_no"].ToString();

                        }
                        catch (Exception)
                        {

                        }


                        objODBC.executeNonQuery("Update er_boloro_api set check_t_logresponse='" + strLoginRequest + "',check_t_request='" + varRequestJSONTopupCheckTransaction + "',check_t_access_tocken='" + strAccessToken + "',check_t_sequence_no='" + strSequenceNo + "',topup_msisdn='" + ctmsisdn + "' where recharge_id='" + intRechargeID + "'");

                        objODBC.executeNonQuery("INSERT INTO `er_boloro_pending_topup`(`userid`, `recharge_id`,`operator_table_id`,`login_request`, `login_response`, `check_transaction_request`, `check_transaction_response`, `transaction_no`, `status_code`, `status_desc`, `sequence_no`, `amount`, `msisdn`, `created_on`) VALUES ('" + intUserID + "','" + intRechargeID + "','" + intOperatorID + "','" + strLoginRequest + "','" + JSONDatalogin + "','" + varRequestJSONTopupCheckTransaction + "','" + JSONCheck_TransactionData + "','" + cttransaction_no + "','" + ctstatus_code + "','" + ctstatus_desc + "','" + ctsequence_no + "','" + ctamount + "','" + ctmsisdn + "','" + objDF.getCurDateTimeString() + "')");
                    }
                    catch (Exception)
                    {

                    }


                    return JSONCheck_TransactionData;
                }
                else
                {
                    return JSONDatalogin;
                }
            }
            catch (Exception)
            {


            }
            return "";
        }

        /**************** Check Balance *********************/

        public string getBoloroBalance()
        {
            string strBalance = "0";
            terminal_id = "297262_W";
            try
            {
                var tuple = getLoginSequenceTransactionCheck();

                // { "response_code":1,"response_desc":"Operation successful","data":{ "access_token":"17656_o5e09e04223d21","sequence_no":"5e09e042287ca"} }

                //  string JSONDatalogin = "{\"response_code\":1,\"response_desc\":\"Operation successful\",\"data\":{\"access_token\":\"7656_o5e09e04223d21\",\"sequence_no\":\"5e09e042287ca\"}}";
               

                string JSONDatalogin = tuple.Item2;
                string strAccessToken = "";
                string strSequenceNo = "";
                try
                {
                    dynamic data = JObject.Parse(JSONDatalogin);
                    string strRequest_Code = data.response_code;
                    string strResponse_desc = data.response_desc;
                    // string strResponseData = data.data;
                    strAccessToken = data["data"]["access_token"].Value;
                    strSequenceNo = data["data"]["sequence_no"].Value;

                }
                catch (Exception)
                {
                    strBalance = "0";
                }


                if (strAccessToken != "" && strSequenceNo != "")
                {

                    var varRequestJSONGetbalance = "{ \"terminal_id\": \"" + terminal_id + "\",\"access_token\": \"" + strAccessToken + "\",\"merchant_code\": \"" + merchant_code + " \" }";
                    var clientGetbalance = new RestClient("http://10.120.0.20:8020/index.php/merchantapi/v1/getmerchantbalance");
                    var requestGetBalance = new RestRequest(Method.POST);
                    requestGetBalance.AddHeader("Postman-Token", "576b94c0-3bc4-4973-83bc-7e7ab504502e");
                    requestGetBalance.AddHeader("cache-control", "no-cache");
                    requestGetBalance.AddHeader("content-type", "multipart/form-data; boundary=----WebKitFormBoundary7MA4YWxkTrZu0gW");
                    requestGetBalance.AddParameter("multipart/form-data; boundary=----WebKitFormBoundary7MA4YWxkTrZu0gW", "------WebKitFormBoundary7MA4YWxkTrZu0gW\r\nContent-Disposition: form-data; name=\"data\"\r\n\r\n" + varRequestJSONGetbalance + "\r\n------WebKitFormBoundary7MA4YWxkTrZu0gW--", ParameterType.RequestBody);
                    IRestResponse responseGetbalance = clientGetbalance.Execute(requestGetBalance);
                    string JSONDataGetBalance = responseGetbalance.Content.ToString();

                    // {"response_code":1,"response_desc":"Operation successful","data":{"merchant_code":"17656","balance":"5000.0000"}}

                    try
                    {
                        dynamic data = JObject.Parse(JSONDataGetBalance);
                        string strRequest_Code = data.response_code;
                        string strResponse_desc = data.response_desc;
                        if (strRequest_Code == "1" && strResponse_desc == "Operation successful")
                        {
                            // string strResponseData = data.data;
                            strAccessToken = data["data"]["merchant_code"].Value;
                            strBalance = data["data"]["balance"].Value;
                        }

                    }
                    catch (Exception)
                    {
                        strBalance = "0";
                    }
                }
                else
                {
                    strBalance = "0";
                }
            }
            catch (Exception)
            {

                strBalance = "0";
            }

            return strBalance;
        }


        /**************** Topup Request *********************/

        public Tuple<string, string> getLoginSequence()
        {

            var RequestJSONLogin = "{ \"terminal_id\": \"" + terminal_id + "\", \"api_login\": \"" + API_17656p + "\", \"api_key\": \"" + api_key + "\", \"merchant_code\": \"" + merchant_code + "\"}";
            var client = new RestClient("http://10.120.0.20:8020/index.php/merchantapi/v1/login");
            var request = new RestRequest(Method.POST);
            request.AddHeader("Postman-Token", "1d0c7ce1-bd0e-48d6-8738-2e0cdcc2af1e");
            request.AddHeader("cache-control", "no-cache");
            request.AddHeader("content-type", "multipart/form-data; boundary=----WebKitFormBoundary7MA4YWxkTrZu0gW");
            request.AddParameter("multipart/form-data; boundary=----WebKitFormBoundary7MA4YWxkTrZu0gW", "------WebKitFormBoundary7MA4YWxkTrZu0gW\r\nContent-Disposition: form-data; name=\"data\"\r\n\r\n" + RequestJSONLogin + "\r\n------WebKitFormBoundary7MA4YWxkTrZu0gW--", ParameterType.RequestBody);
            IRestResponse response = client.Execute(request);
            string JSONDatalogin = response.Content.ToString();

            return new Tuple<string, string>(RequestJSONLogin, JSONDatalogin);
        }

        //Telco codes:
        //Etisalat: 78
        //Roshan: 79
        //AWCC: 70
        //MTN: 77
        //Salaam: 74

        //            AWCC:
        //            Min Limit: 50
        //Max Limit: 5001

        //Etisalat:
        //            Min Limit: 5
        //Max Limit: 5001

        //MTN:
        //            Min Limit: 12
        //Max Limit: 20000

        //Roshan:
        //            Min Limit: 25
        //Max Limit: 5001

        //Salaam:
        //            Min Limit: 10
        //Max Limit: 5000

        public string BoloroTopup(int intUserID,int intRechargeID,int intOperatortableID,string MSINDN_mobile,string dblAmount,string strTelco_Code)
        {
            try
            {
                //   var RequestJSONLogin = "{ \"terminal_id\": \"" + terminal_id + "\", \"api_login\": \"" + API_17656p + "\", \"api_key\": \"" + api_key + "\", \"merchant_code\": \"" + merchant_code + "\"}";



                var tuple = getLoginSequence();
                string strLoginRequest = tuple.Item1;
                string JSONDatalogin = tuple.Item2;

                //string strLoginRequest = objODBC.executeScalar_str("SELECT `data` FROM `temp` where id=1");
                //string JSONDatalogin = objODBC.executeScalar_str("SELECT `data` FROM `temp` where id=2");

                string strAccessToken = "";
                string strSequenceNo = "";
                string strRequest_Code = "";
                string strResponse_desc = "";
                try
                {
                    dynamic data = JObject.Parse(JSONDatalogin);
                    strRequest_Code = data.response_code;
                    strResponse_desc = data.response_desc;
                    // string strResponseData = data.data;
                    strAccessToken = data["data"]["access_token"].Value;
                    strSequenceNo = data["data"]["sequence_no"].Value;

                }
                catch (Exception)
                {
                    
                }

                if (strAccessToken != "" && strSequenceNo != "")
                {


                    // {"response_code":1,"response_desc":"Operation successful","data":{"transaction_no":"5e0af26a9868f","status_code":160,"status_desc":"In Process Initiating Topup request","msisdn":"93711707015","amount":50,"balance":"5000.0000","sequence_no":"5e0af26a87738"}}

                    var varRequestJSONTopup = "{ \"terminal_id\": \"" + terminal_id + "\", \"access_token\":\"" + strAccessToken + "\", \"merchant_code\": \"" + merchant_code + "\", \"sequence_no\": \"" + strSequenceNo + "\", \"service_code\": \"1\", \"msisdn\": \"" + MSINDN_mobile + "\", \"amount\": \"" + dblAmount + "\", \"telco_code\": \"" + strTelco_Code + "\" }";

                    var client = new RestClient("http://10.120.0.20:8020/index.php/merchantapi/v1/registertransaction");
                    var request = new RestRequest(Method.POST);
                    request.AddHeader("Postman-Token", "ec17d40b-2a11-4149-aed3-27fe313e5ada");
                    request.AddHeader("cache-control", "no-cache");
                    request.AddHeader("content-type", "multipart/form-data; boundary=----WebKitFormBoundary7MA4YWxkTrZu0gW");
                    request.AddParameter("multipart/form-data; boundary=----WebKitFormBoundary7MA4YWxkTrZu0gW", "------WebKitFormBoundary7MA4YWxkTrZu0gW\r\nContent-Disposition: form-data; name=\"data\"\r\n\r\n" + varRequestJSONTopup + "\r\n------WebKitFormBoundary7MA4YWxkTrZu0gW--", ParameterType.RequestBody);
                    IRestResponse response = client.Execute(request);
                    string JSONDataTopupData = response.Content.ToString();

                    //string varRequestJSONTopup = objODBC.executeScalar_str("SELECT `data` FROM `temp` where id=3");
                    //string JSONDataTopupData = objODBC.executeScalar_str("SELECT `data` FROM `temp` where id=4");


                    try
                    {
                        // ,`check_t_request`=[value - 23],`check_t_response`=[value - 24],`transaction_no`=[value - 25],`topup_response_code`=[value - 26],`topup_response_desc`=[value - 27],`topup_status_code`=[value - 28],`topup_status_desc`=[value - 29],`topup_msisdn`=[value - 30],`topup_amount`=[value - 31],`topup_operator_balance`=[value - 32],`topup_sequence_no`=[value - 33],`created_on`=[value - 34],`modifyed_on`=[value - 35],`check_t_response_code`=[value - 36],`check_t_response_desc`=[value - 37],`check_t_transaction_no`=[value - 38],`check_t_status_code`=[value - 39],`check_t_status_desc`=[value - 40],`check_t_response_sequence_no`=[value - 41],`check_t_all_response`=[value - 42],`status`=[value - 43]


                       

                        objODBC.executeNonQuery("Update er_boloro_api set `terminal_id`='"+terminal_id+"',`api_login`='"+ API_17656p+"',`api_key`='"+ api_key +"',`login_request`='"+ strLoginRequest+"',`login_response`='"+ JSONDatalogin + "',`login_response_code`='"+ strRequest_Code + "',`login_response_desc`='"+ strResponse_desc + "',`login_access_token`='"+ strAccessToken + "',`login_sequence_no`='"+ strSequenceNo + "',`merchant_code`='"+ merchant_code + "',`msisdn`='"+ MSINDN_mobile + "',`amount`='"+ dblAmount + "',`telco_code`='"+ strTelco_Code + "',`check_t_logRequest`='"+ strLoginRequest + "',`topup_request`='"+ varRequestJSONTopup + "',`topup_response`='"+ JSONDataTopupData + "' where id='" + intOperatortableID + "'");
                    }
                    catch (Exception)
                    {

                    }


                    return JSONDataTopupData;
                }
                else
                {
                    return "NA";
                }
            }
            catch (Exception)
            {


            }
            return "NA";
        }

    }
}