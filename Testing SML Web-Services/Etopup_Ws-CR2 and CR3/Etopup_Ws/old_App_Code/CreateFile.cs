﻿using Microsoft.VisualBasic;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Web;
using System.Data.Odbc;
using System.IO;
using System.Security.Cryptography;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using MySql.Data;
using Etopup_Ws.old_App_Code;
using System.Security.AccessControl;
using System.Text;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;

namespace Etopup_Ws.old_App_Code
{
    public class CreateFile
    {
        DateFormat objDTF = new DateFormat();
        ODBC_LOG objODBCLOG = new ODBC_LOG();
        public void CreateTextFile(string strIPAddress , string strMacAddress , string strOSDetails ,string strIMEINo, string strGcmID, string strAPPVersion, string intSource, int intCreatedByType,int intActivityType,string strResponse,string strUserName)
        {
            try
            {
                string strParameter = "";string strFunctionName = "";string strWebServiceName = "";
                string strName = (objDTF.getCurDateTime_Dayanamic_format("dd MM yyyy")).ToString()+" log.txt";
                string fileName = @"C:\LogsFile\"+ strName;

                // Check if file already exists. If yes, delete it.   
                if (File.Exists(fileName))
                {
                    // File.Delete(fileName);

                    using (var tw = new StreamWriter(fileName, true))
                    {
                        tw.WriteLine("{ UserName: "+strUserName + ", Date :"+ objDTF.getCurDateTimeString() + ",IP Address: "+ strIPAddress+", Mac Addres : "+ strMacAddress +", OS Details : "+ strOSDetails+ ", IMEI No. :"+ strIMEINo+ ", GCM ID.: "+ strGcmID+", Version :"+ strAPPVersion+", Source :"+ intSource + ", C B Type :"+ intCreatedByType +", A Type :"+ intActivityType + ", Message :"+ strResponse+ ",Function Name : "+ strFunctionName + ",WebService Name : "+ strWebServiceName + ",Parameter:" + strParameter + "}\n\n");

                        try
                        {

                            //JObject objJSONLogData = new JObject(new JProperty("UserName", strUserName), new JProperty("Date", objDTF.getCurDateTimeString()), new JProperty("IP Address", strIPAddress), new JProperty("Mac Address", strMacAddress), new JProperty("OS Details", strOSDetails), new JProperty("IMEI No", strIMEINo), new JProperty("GCM ID", strGcmID), new JProperty("Version", strAPPVersion), new JProperty("Source", intSource), new JProperty("Created By Type", intCreatedByType), new JProperty("Activity Type", intActivityType), new JProperty("Message", strResponse), new JProperty("Function Name ", strFunctionName), new JProperty("WebService Name ", strWebServiceName), new JProperty("Parameter ", strParameter));
                            //int intUserID = objODBCLOG.executeScalar_int("SELECT userid From er_login_log Where username='" + strUserName + "'");
                            //if (intUserID > 0)
                            //{
                            //    objODBCLOG.executeNonQuery("CALL `insertJSONLogsActivity`('" + intUserID + "','" + objJSONLogData + "');");
                            //}
                            //else
                            //{
                            //    objODBCLOG.executeNonQuery("CALL `insertJSONLogsActivity`('0','" + objJSONLogData + "');");
                            //}

                            //File.WriteAllText(@"c:\videogames.json", videogameRatings.ToString());

                            //// write JSON directly to a file
                            //using (StreamWriter file = File.CreateText(@"c:\videogames.json"))
                            //using (JsonTextWriter writer = new JsonTextWriter(file))
                            //{
                            //    videogameRatings.WriteTo(writer);
                            //}
                        }
                        catch (Exception)
                        {

                           
                        }

                    }
                }
                else
                {  
                    // Create a new file   
                    using (FileStream fs = File.Create(fileName))
                    {
                        // Add some text to file  
                        Byte[] title = new UTF8Encoding(true).GetBytes("Setaragan Muthed Quick Pay Log File \n ");
                        fs.Write(title, 0, title.Length);
                        byte[] author = new UTF8Encoding(true).GetBytes("Logs Track for Date: " + (objDTF.getCurDateTime_Dayanamic_format("dd-MM-yyyy") + " \n\n\n\n").ToString());
                        fs.Write(author, 0, author.Length);
                    }
                    using (var tw = new StreamWriter(fileName, true))
                    {
                        tw.WriteLine("{UserName: " + strUserName + ", Date :" + objDTF.getCurDateTimeString() + ",IP Address: " + strIPAddress + ", Mac Addres : " + strMacAddress + ", OS Details : " + strOSDetails + ", IMEI No. :" + strIMEINo + ", GCM ID.: " + strGcmID + ", Version :" + strAPPVersion + ", Source :" + intSource + ", C B Type :" + intCreatedByType + ", A Type :" + intActivityType + ", Message :" + strResponse + ",Function Name : " + strFunctionName + ",WebService Name : " + strWebServiceName + ",Parameter:" + strParameter + "}\n\n");


                        try
                        {

                            //JObject objJSONLogData = new JObject(new JProperty("UserName", strUserName), new JProperty("Date", objDTF.getCurDateTimeString()), new JProperty("IP Address", strIPAddress), new JProperty("Mac Address", strMacAddress), new JProperty("OS Details", strOSDetails), new JProperty("IMEI No", strIMEINo), new JProperty("GCM ID", strGcmID), new JProperty("Version", strAPPVersion), new JProperty("Source", intSource), new JProperty("Created By Type", intCreatedByType), new JProperty("Activity Type", intActivityType), new JProperty("Message", strResponse), new JProperty("Function Name ", strFunctionName), new JProperty("WebService Name ", strWebServiceName), new JProperty("Parameter ", strParameter));
                            //int intUserID = objODBCLOG.executeScalar_int("SELECT userid From er_login_log Where username='" + strUserName + "'");
                            //if (intUserID > 0)
                            //{
                            //    objODBCLOG.executeNonQuery("CALL `insertJSONLogsActivity`('" + intUserID + "','" + objJSONLogData + "');");
                            //}
                            //else
                            //{
                            //    objODBCLOG.executeNonQuery("CALL `insertJSONLogsActivity`('0','" + objJSONLogData + "');");
                            //}

                            //File.WriteAllText(@"c:\videogames.json", videogameRatings.ToString());

                            //// write JSON directly to a file
                            //using (StreamWriter file = File.CreateText(@"c:\videogames.json"))
                            //using (JsonTextWriter writer = new JsonTextWriter(file))
                            //{
                            //    videogameRatings.WriteTo(writer);
                            //}
                        }
                        catch (Exception)
                        {


                        }

                    }                
                }
            }
            catch (Exception e)
            {
              
            }

        }
    }
}