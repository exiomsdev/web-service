﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Script.Serialization;


using System.Web.Script.Services;
using System.Web.Services;
using System.Text;
using Newtonsoft.Json;
using RestSharp;
using System.Web.Configuration;
using System.Configuration;
using System.Security.AccessControl;
using System.Security.Principal;

/// <summary>
/// Summary description for ico_commonfunction
/// </summary>

namespace Etopup_Ws.old_App_Code
{
    public class ico_commonfunction
    {
        SortedList<string, string> parmss;
        SortedList<string, string> parmss_bal;
        private static readonly Encoding encoding = Encoding.UTF8;
       // clsCommunication clscomm = new clsCommunication();
        ODBC clsodbc = new ODBC();
        smtpclass clssmtp = new smtpclass();
        string chkToken = System.Configuration.ConfigurationManager.AppSettings["token"];
        Object valid = string.Empty;
        string NotFound = "Record Not Found";

        public string ChkTokenValid(string strtoken)
        {
            string result = string.Empty;
            if (strtoken == chkToken)
            {
                result = "Valid";
            }
            else
            {
                valid = new
                {
                    strResult = "Fail",
                    strdata = "Invalid Token"

                };

                result = new JavaScriptSerializer().Serialize(new { RequestData = valid });
            }

            return result;
        }

        public string GetJsonData(string strtoken, string strquery)
        {
            ODBC clsodbc = new ODBC();
            Object valid = "";
            DataTable dt = new DataTable();
            try
            {
                if (strtoken == chkToken)
                {
                    dt = clsodbc.getDataTable(strquery);
                    JavaScriptSerializer jsSerializer = new JavaScriptSerializer();
                    List<Dictionary<string, object>> parentRow = new List<Dictionary<string, object>>();
                    Dictionary<string, object> childRow;

                    if (dt.Rows.Count > 0)
                    {
                        foreach (DataRow row in dt.Rows)
                        {
                            childRow = new Dictionary<string, object>();
                            foreach (DataColumn col in dt.Columns)
                            {
                                childRow.Add(col.ColumnName, row[col]);
                            }
                            parentRow.Add(childRow);

                        }
                        if (dt.Rows[0][0].ToString() != "0")
                        {
                            valid = new
                            {
                                strResult = "Success",
                                strdata = parentRow

                            };
                        }
                        else
                        {
                            valid = new
                            {
                                strResult = "Fail",
                                fieldError = dt.Rows[0][1].ToString(),
                                strdata = dt.Rows[0][2].ToString()

                            };
                        }

                    }

                    else
                    {
                        valid = new
                        {
                            strResult = "Fail",
                            strdata = "No Record Found"

                        };

                    }

                }
                else
                {
                    valid = new
                    {
                        strResult = "Fail",
                        strdata = "Invalid token"
                    };
                }
            }
            catch (Exception e)
            {
                valid = new
                {
                    strResult = "Fail",
                    strdata = e.Message

                };
            }
            finally
            {
                dt.Dispose();
            }

            string json = new JavaScriptSerializer().Serialize(new { RequestData = valid });
            return json;

        }

        public string GetData(string strtoken, string strquery)
        {
            string strResult = ChkTokenValid(strtoken);
            string result = string.Empty;
            if (strResult == "Valid")
            {
                result = clsodbc.executeScalar_str(strquery);
            }
            else
            {
                result = strResult;
            }
            return result;
        }

        public string GetUserCount(int intUserID, string strTableName)
        {
            int intCount = 0;
            string strResult = string.Empty;
            intCount = clsodbc.executeScalar_int("select count(1) from " + strTableName + " where userid = " + intUserID + "");
            if (intCount != 0)
            {
                strResult = "1";
            }
            else
            {
                valid = new
                {
                    strResult = "Fail",
                    intFlag = 0,
                    fieldError = "UserID",
                    strResponse = "UserID is not exists"

                };
                strResult = new JavaScriptSerializer().Serialize(new { RequestData = valid });
            }
            return strResult;
        }

        public string ico_Report_Buy_Btc(int intUserID, string strTableName, string strSpName, string strToken) //only for userid
        {
            string result = string.Empty;
            result = ChkTokenValid(strToken);
            if (result == "Valid")
            {
                result = GetUserCount(intUserID, strTableName);
                if (result == "1")
                {
                    string strQuery = "call " + strSpName + "(" + intUserID + ")";
                    result = GetJsonData(strToken, strQuery);
                }
                else
                {
                    valid = new
                    {
                        strResult = "Fail",
                        fieldError = "",
                        strdata = NotFound


                    };
                    result = new JavaScriptSerializer().Serialize(new { RequestData = valid });

                }
            }


            return result;
        }

        public string GetAddressFrom_BlockChain(string strIncoice, int intUserID)
        {
            string QrCode = string.Empty;
            try
            {
                string xpub = System.Configuration.ConfigurationManager.AppSettings["XPub"];
                string key = System.Configuration.ConfigurationManager.AppSettings["BlockChainKey"];
                string urlAddress = "https://api.blockchain.info/v2/receive/checkgap?xpub=" + xpub + "&key=" + key + "";

                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(urlAddress);
                HttpWebResponse response = (HttpWebResponse)request.GetResponse();

                if (response.StatusCode == HttpStatusCode.OK)
                {

                    Stream receiveStream = response.GetResponseStream();
                    StreamReader readStream = null;

                    readStream = new StreamReader(receiveStream);

                    string source = readStream.ReadToEnd();
                    dynamic data1 = JObject.Parse(source);

                    int int_gap = data1.gap;


                    if (int_gap < 17)
                    {

                        // Genrate Address and Genrate QR code 

                        string strcallback = HttpContext.Current.Server.UrlEncode(System.Configuration.ConfigurationManager.AppSettings["CallbackUrl"] + "?invoice_id=" + strIncoice + "&secret=ayelsgtksda");
                        string strUrl = System.Configuration.ConfigurationManager.AppSettings["receive_url"] + "xpub=" + System.Configuration.ConfigurationManager.AppSettings["XPub"] + "&callback=" + strcallback + "&key=" + System.Configuration.ConfigurationManager.AppSettings["BlockChainKey"];


                        //clsodbc.executeNonQuery("insert into temp_chk(`value`) values ('" + strcallback + "')");
                        //clsodbc.executeNonQuery("insert into temp_chk(`value`) values ('" + strUrl + "')");


                        WebRequest request1 = default(WebRequest);

                        request1 = WebRequest.Create(strUrl);

                        WebResponse resp = request1.GetResponse();
                        //{ "address" : "1BeRHmxDrjx6QrzjV1Lx6t5uswSu34KUrp", "index" : 59, "callback" : "http://rgc.exioms.me/CallBackUrl.aspx?invoice_id=46822&secret=ayelsgtksda" }

                        StreamReader reader = new StreamReader(resp.GetResponseStream());
                        string responseString = reader.ReadToEnd();

                        var objTrackLink = JObject.Parse(responseString);
                        var strResult = objTrackLink["address"].ToString(); ;



                        reader.Close();
                        resp.Close();

                        /*
                          If amount also want in QR code then --

                          string strQrCode = "https://chart.googleapis.com/chart?chs=300x300&cht=qr&chl=bitcoin:" + strAddress + "?amount=" + amount + "";
                         */
                        int count = clsodbc.executeNonQuery("update ico_bitcoin_address set address='" + strResult.ToString() + "' where invoice_id =" + strIncoice + " and userid=" + intUserID + "  and addtype=1");
                        if (count > 0)
                        {
                            QrCode = strResult.ToString();

                        }
                        else
                        {
                            QrCode = "Somthing went wronge";
                        }

                    }
                    else
                    {
                        QrCode = "0"; // Gap reached upto limit
                    }
                }
                //response.Close();
                //readStream.Close();


            }
            catch (Exception)
            {

            }
            finally
            {

            }
            return QrCode;
        }

        public string GetAddressFrom_CoinPay(int intUserID, int inttAddrType)
        {
            string strAddress = string.Empty;
            strAddress = CallAPI("get_callback_address", inttAddrType, parmss);
            if (strAddress != "0")
            {
                int count = clsodbc.executeNonQuery("update ico_bitcoin_address set address='" + strAddress.ToString() + "' where userid=" + intUserID + " and addtype=" + inttAddrType + "");
                if (count > 0)
                {
                    // strAddress = strResult.ToString();

                }
                else
                {
                    strAddress = "Somthing went wronge";
                }
            }
            else
            {
                if (inttAddrType == 1)
                {
                    strAddress = "BTC Blockchain is under maintainance, so right now we cant genrate your address";
                }
                else
                {
                    strAddress = "ETH Blockchain is under maintainance, so right now we cant genrate your address";

                }

            }
            return strAddress;
        }

        public string CallAPI(string cmd, int INteAddType, SortedList<string, string> parms = null)
        {
            dynamic ret = new Dictionary<string, dynamic>();
            try
            {
                if (parms == null)
                {
                    parms = new SortedList<string, string>();
                }
                parms["version"] = "1";
                parms["cmd"] = cmd;

                if (INteAddType == 1)
                {
                    parms["currency"] = "BTC";
                }
                else if (INteAddType == 2)
                {
                    parms["currency"] = "ETH";
                }
                parms["ipn_url"] = System.Configuration.ConfigurationManager.AppSettings["CallBack_CoinPay"];
                parms["key"] = System.Configuration.ConfigurationManager.AppSettings["publicKey_CoinPay"];


                string post_data = "";
                foreach (KeyValuePair<string, string> parm in parms)
                {
                    if (post_data.Length > 0) { post_data += "&"; }
                    post_data += parm.Key + "=" + Uri.EscapeDataString(parm.Value);
                }

                byte[] keyBytes = encoding.GetBytes(System.Configuration.ConfigurationManager.AppSettings["privateKey_CoinPay"]);
                byte[] postBytes = encoding.GetBytes(post_data);
                var hmacsha512 = new System.Security.Cryptography.HMACSHA512(keyBytes);
                string hmac = BitConverter.ToString(hmacsha512.ComputeHash(postBytes)).Replace("-", string.Empty);

                // do the post:
                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
                System.Net.WebClient cl = new System.Net.WebClient();
                cl.Headers.Add("Content-Type", "application/x-www-form-urlencoded");
                cl.Headers.Add("HMAC", hmac);
                cl.Encoding = encoding;
                try
                {
                    // string a = "{\"error\":\"ok\",\"result\":{\"address\":\"3J2hoyso1sXDcWib5D4aTN5A5hHNmVEqbH\" }}";

                    var resp = cl.UploadString("https://www.coinpayments.net/api.php", post_data);
                    var decoder = new System.Web.Script.Serialization.JavaScriptSerializer();
                    ret = decoder.Deserialize<Dictionary<string, dynamic>>(resp);
                    ret = JsonToString(resp);

                }
                catch (System.Net.WebException e)
                {
                    ret["error"] = "Exception while contacting CoinPayments.net: " + e.Message;
                }
                catch (Exception e)
                {
                    ret["error"] = "Unknown exception: " + e.Message;
                }
            }
            catch (Exception)
            {

                 
            }

            return ret;

        }

        public string JsonToString(string jsonString)
        {
            string strAddress = string.Empty;
            try
            {
                string a = JObject.Parse(jsonString).ToString();
                var RequestData = JObject.Parse(a);
                var status = RequestData["error"];
                if (status.ToString() == "ok")
                {
                    var req = RequestData["result"];
                    var reqAddress = req["address"];
                    strAddress = reqAddress.ToString();
                }
                else
                {
                    strAddress = "0";
                }
            }
            catch (Exception)
            {
                strAddress = "Exception Found";
                 
            }
            return strAddress;
        }

        public string getBalanceString(int intUserID, string guid, string password)
        {
            string strBal = string.Empty;
            try
            {
                dynamic client = new RestClient();
                string sURL = null;
                //sURL = ((RechargeBlastURL + "Recharge.aspx?Username=" + RechargeBlastUsername + "&Password=" + RechargeBlastPassword + "&Amount=" + dblAmount + "&OperatorCode=" + strOperatorCodeRechargeBlast + "&Number=" + strMobile + "&ClientId=" + intID + "&CircleCode=" + strcircleCodeRechargeBlast +""));
                sURL = ((WebConfigurationManager.AppSettings["blockChainURL"] + "/merchant/" + guid + "/balance?password=" + password));
                WebRequest request = default(WebRequest);
                request = WebRequest.Create(sURL);
                WebResponse resp = request.GetResponse();
                StreamReader reader = new StreamReader(resp.GetResponseStream());
                string responseString = reader.ReadToEnd();
                var a = JObject.Parse(responseString.ToString());
                var balance = a["balance"];
                //  clsodbc.executeNonQuery("INSERT INTO `temp_chk`(`value`) VALUES ('" + balance.ToString() + "')");

                strBal = balance.ToString();
                //"{\"balance\":54374229}"

            }
            catch (Exception ex)
            {
                strBal = "0";

            }
            return strBal;
        }

        public string SendWithdrawReq(int intuserID, double amt, string strAddr, int intLastID)
        {
            string strResponse = string.Empty;
            JavaScriptSerializer serializer = new JavaScriptSerializer();
            try
            {
                double dblReqAmt = (amt - Convert.ToDouble(WebConfigurationManager.AppSettings["Withdraw_Tax"])) * 100000000;

                if (dblReqAmt > 0) //payable abt snd by sneha
                {
                    dynamic client = new RestClient();
                    string sURL = null;
                    sURL = ((WebConfigurationManager.AppSettings["blockChainURL"] + "/merchant/" + WebConfigurationManager.AppSettings["guid"] + "/payment?password=" + WebConfigurationManager.AppSettings["block_pass"] + "&to=" + strAddr + "&amount=" + amt + "&from=0"));
                    WebRequest request = default(WebRequest);
                    request = WebRequest.Create(sURL);
                    WebResponse resp = request.GetResponse();
                    StreamReader reader = new StreamReader(resp.GetResponseStream());
                    string responseString = reader.ReadToEnd();
                    // string responseString = "{\"to\":[\"1BB2s4M3FRVk2pimkUZnQAR5AxfKqKerYC\"],\"amounts\":[200000],\"from\":[\"xpub6Cspr6JeNtDy9P2wGhwm3yYySyRPVwSp8AY5gCHrihWB3ziAsnXWyZ6C2y4fMiZ1J3SexxdKEchR4XVe7xbPR5bnby44CsKWpBdfLR6F26M\"],\"fee\":100000,\"txid\":\"80e64ae8e497049f3bd10cca11d8a36a1207a20609d90d0b0ce99b3ce4cbf384\",\"tx_hash\":\"80e64ae8e497049f3bd10cca11d8a36a1207a20609d90d0b0ce99b3ce4cbf384\",\"message\":\"Payment Sent\",\"success\":true,\"warning\":\"Using a static fee amount may cause large transactions to confirm slowly\"}";

                    var objTrackLink = JObject.Parse(responseString.ToString());
                    // clsodbc.executeNonQuery("call ico_first_Response_Withdraw(" + intuserID + ",'" + objTrackLink.ToString() + "',"+intLastID+")");
                    //clsodbc.executeNonQuery("INSERT INTO `temp_chk`(`value`) VALUES ('" + objTrackLink.ToString() + "')");

                    if (responseString.Contains("error"))
                    {
                        //strerror = objTrackLink["error"].ToString();
                        //strneeded = objTrackLink["needed"].ToString();
                        //stravailable = objTrackLink["available"].ToString();

                        //strReturn = strerror;
                        //intResponsetype = 2;
                        int fail = 1;
                        string a = "error" + " " + " lastinsterderid = '" + intLastID + "'" + objTrackLink["error"].ToString() + objTrackLink["needed"].ToString() + objTrackLink["available"].ToString();

                        clsodbc.executeNonQuery("call ico_first_Response_Withdraw(" + intuserID + ",'" + a.ToString() + "'," + intLastID + "," + fail + ")");
                        // clsodbc.executeNonQuery("INSERT INTO `temp_chk`(`value`) VALUES ('" + a.ToString() + "')");
                        valid = new
                        {
                            strResult = "Fail",
                            strdata = "withdrawal process is stop for a while please try after some time"

                        };
                        strResponse = new JavaScriptSerializer().Serialize(new { RequestData = valid });
                    }

                    if (responseString.Contains("success"))
                    {
                        // clsodbc.executeNonQuery("INSERT INTO `temp_chk`(`value`) VALUES ('ENter in success')");
                        string strsuccess = objTrackLink["success"].ToString();
                        string strTo = objTrackLink["to"].ToString();
                        // double amst = Convert.ToDouble(objTrackLink["amounts"].ToString());
                        // clsodbc.executeNonQuery("INSERT INTO `temp_chk`(`value`) VALUES ('getpar')");

                        //string a = "call ico_withdraw_response_api(" + intuserID + ", " + objTrackLink["to"].ToString() + ", " + objTrackLink["amounts"].ToString() + ", " + objTrackLink["from"].ToString() + ", " + objTrackLink["fee"].ToString() + ", " + objTrackLink["txid"].ToString() + ", " + objTrackLink["tx_hash"].ToString() + ", " + objTrackLink["success"].ToString() + ")";

                        //clsodbc.executeNonQuery("INSERT INTO `temp_chk`(`value`) VALUES ('" + a.ToString() + "')");

                        int chk = clsodbc.executeNonQuery("call ico_withdraw_response_api(" + intuserID + ",'" + objTrackLink["to"].ToString() + "','" + objTrackLink["amounts"].ToString() + "','" + objTrackLink["from"].ToString() + "','" + objTrackLink["fee"].ToString() + "','" + objTrackLink["txid"].ToString() + "','" + objTrackLink["tx_hash"].ToString() + "','" + objTrackLink["success"].ToString() + "'," + intLastID + ")");
                        //   clsodbc.executeNonQuery("INSERT INTO `temp_chk`(`value`) VALUES ('done')");
                        if (chk > 0)
                        {
                            valid = new
                            {
                                strResult = "Success",
                                strdata = "Transaction Successfully"

                            };
                            strResponse = new JavaScriptSerializer().Serialize(new { RequestData = valid });

                        }
                        else
                        {
                            valid = new
                            {
                                strResult = "Fail",
                                strdata = "Transaction Fail"

                            };
                            strResponse = new JavaScriptSerializer().Serialize(new { RequestData = valid });
                        }
                    }

                }
                else
                {
                    valid = new
                    {
                        strResult = "Fail",
                        strdata = "Insufficient Balance"

                    };
                    strResponse = new JavaScriptSerializer().Serialize(new { RequestData = valid });
                }

            }
            catch (Exception ee)
            {
                int fail = 1;
                strResponse = "catch" + ee.ToString();
                clsodbc.executeNonQuery("call ico_first_Response_Withdraw(" + intuserID + ",'" + strResponse.ToString() + "'," + intLastID + "," + fail + ")");
                valid = new
                {
                    strResult = "Fail",
                    strdata = "withdrawal process is stop for a while please try after some time"

                };
                strResponse = new JavaScriptSerializer().Serialize(new { RequestData = valid });
            }
            return strResponse;
        }
        public string GetStringValues_FromJson(DataTable dt, int intUserID)
        {

            string strResponse = string.Empty;
            if (dt.Rows.Count > 3)
            {
                int chk = clsodbc.executeNonQuery("call ico_withdraw_response_api(" + intUserID + ",'" + dt.Rows[0]["to"].ToString() + "','" + dt.Rows[0]["amounts"].ToString() + "','" + dt.Rows[0]["from"].ToString() + "','" + dt.Rows[0]["txid"].ToString() + "','" + dt.Rows[0]["tx_hash"].ToString() + "','" + dt.Rows[0]["message"].ToString() + "','" + dt.Rows[0]["success"].ToString() + "')");
                if (chk > 0)
                {
                    valid = new
                    {
                        strResult = "Success",
                        strdata = "Transaction Successfully"

                    };
                    strResponse = new JavaScriptSerializer().Serialize(new { RequestData = valid });

                }
                else
                {
                    valid = new
                    {
                        strResult = "Fail",
                        strdata = "Transaction Fail"

                    };
                    strResponse = new JavaScriptSerializer().Serialize(new { RequestData = valid });
                }
            }
            else
            {
                valid = new
                {
                    strResult = "Fail",
                    strdata = dt.Rows[0]["error"].ToString()

                };
                strResponse = new JavaScriptSerializer().Serialize(new { RequestData = valid });
            }

            return strResponse;
        }

        public string descypt_pass(string encrypttoken)
        {
            string decryptpwd = string.Empty;
            UTF8Encoding encodepwd = new UTF8Encoding();
            Decoder Decode = encodepwd.GetDecoder();
            byte[] todecode_byte = Convert.FromBase64String(encrypttoken);
            int charCount = Decode.GetCharCount(todecode_byte, 0, todecode_byte.Length);
            char[] decoded_char = new char[charCount];
            Decode.GetChars(todecode_byte, 0, todecode_byte.Length, decoded_char, 0);
            decryptpwd = new String(decoded_char);
            return decryptpwd;
        }

        public string NumberToText(int number, bool isUK)
        {
            if (number == 0) return "Zero";
            string and = isUK ? "and " : ""; // deals with UK or US numbering
            if (number == -2147483648) return "Minus Two Billion One Hundred " + and +
            "Forty Seven Million Four Hundred " + and + "Eighty Three Thousand " +
            "Six Hundred " + and + "Forty Eight";
            int[] num = new int[4];
            int first = 0;
            int u, h, t;
            System.Text.StringBuilder sb = new System.Text.StringBuilder();
            if (number < 0)
            {
                sb.Append("Minus ");
                number = -number;
            }
            string[] words0 = { "", "One ", "Two ", "Three ", "Four ", "Five ", "Six ", "Seven ", "Eight ", "Nine " };
            string[] words1 = { "Ten ", "Eleven ", "Twelve ", "Thirteen ", "Fourteen ", "Fifteen ", "Sixteen ", "Seventeen ", "Eighteen ", "Nineteen " };
            string[] words2 = { "Twenty ", "Thirty ", "Forty ", "Fifty ", "Sixty ", "Seventy ", "Eighty ", "Ninety " };
            string[] words3 = { "Thousand ", "Million ", "Billion " };
            num[0] = number % 1000;           // units
            num[1] = number / 1000;
            num[2] = number / 1000000;
            num[1] = num[1] - 1000 * num[2];  // thousands
            num[3] = number / 1000000000;     // billions
            num[2] = num[2] - 1000 * num[3];  // millions
            for (int i = 3; i > 0; i--)
            {
                if (num[i] != 0)
                {
                    first = i;
                    break;
                }
            }
            for (int i = first; i >= 0; i--)
            {
                if (num[i] == 0) continue;
                u = num[i] % 10;              // ones
                t = num[i] / 10;
                h = num[i] / 100;             // hundreds
                t = t - 10 * h;               // tens
                if (h > 0) sb.Append(words0[h] + "Hundred ");
                if (u > 0 || t > 0)
                {
                    if (h > 0 || i < first) sb.Append(and);
                    if (t == 0)
                        sb.Append(words0[u]);
                    else if (t == 1)
                        sb.Append(words1[u]);
                    else
                        sb.Append(words2[t - 2] + words0[u]);
                }
                if (i != 0) sb.Append(words3[i - 1]);
            }
            return sb.ToString().TrimEnd();
        }


        //-------------------->> WithDrwal COinpayment <-------------------//

        public string Get_Withdraw_From_CoinPay(int inttAddrType) //------Balnce chk of admin
        {
            string strBalance = string.Empty;
            strBalance = CallAPI2("balances", inttAddrType, parmss_bal);
            if (strBalance == "Error")
            {
                strBalance = "Error";
            }
            return strBalance;
        }

        public string CallAPI2(string cmd, int INteAddType, SortedList<string, string> parms = null) //------Balnce chk of admin
        {
            dynamic ret = new Dictionary<string, dynamic>();
            try
            {
                if (parms == null)
                {
                    parms = new SortedList<string, string>();
                }
                parms["version"] = "1";
                parms["cmd"] = cmd;


                parms["currency"] = "1";
                // parms["ipn_url"] = System.Configuration.ConfigurationManager.AppSettings["CallBack_CoinPay"];
                parms["key"] = System.Configuration.ConfigurationManager.AppSettings["publicKey_CoinPay"];


                string post_data = "";
                foreach (KeyValuePair<string, string> parm in parms)
                {
                    if (post_data.Length > 0) { post_data += "&"; }
                    post_data += parm.Key + "=" + Uri.EscapeDataString(parm.Value);
                }

                byte[] keyBytes = encoding.GetBytes(System.Configuration.ConfigurationManager.AppSettings["privateKey_CoinPay"]);
                byte[] postBytes = encoding.GetBytes(post_data);
                var hmacsha512 = new System.Security.Cryptography.HMACSHA512(keyBytes);
                string hmac = BitConverter.ToString(hmacsha512.ComputeHash(postBytes)).Replace("-", string.Empty);

                // do the post:
                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
                System.Net.WebClient cl = new System.Net.WebClient();
                cl.Headers.Add("Content-Type", "application/x-www-form-urlencoded");
                cl.Headers.Add("HMAC", hmac);
                cl.Encoding = encoding;
                try
                {
                    // string a = "{\"error\":\"ok\",\"result\":{\"address\":\"3J2hoyso1sXDcWib5D4aTN5A5hHNmVEqbH\" }}";

                    var resp = cl.UploadString("https://www.coinpayments.net/api.php", post_data);
                    var decoder = new System.Web.Script.Serialization.JavaScriptSerializer();
                    ret = decoder.Deserialize<Dictionary<string, dynamic>>(resp);
                    ret = JsonToString_bal(resp, INteAddType);

                }
                catch (System.Net.WebException e)
                {
                    ret["error"] = "Exception while contacting CoinPayments.net: " + e.Message;
                }
                catch (Exception e)
                {
                    ret["error"] = "Unknown exception: " + e.Message;
                }
            }
            catch (Exception)
            {

                 
            }

            return ret;

        }

        public string JsonToString_bal(string jsonString, int Balnce_of) //---->> Balance fetch
        {
            string strAddress = string.Empty;
            try
            {
                string a = JObject.Parse(jsonString).ToString();
                var RequestData = JObject.Parse(a);
                var status = RequestData["error"];
                if (status.ToString() == "ok")
                {
                    if (Balnce_of == 2)
                    {
                        var req = RequestData["result"];
                        var reqAddress = req["BTC"];
                        var reqBalancee = reqAddress["balancef"];
                        strAddress = reqBalancee.Value<string>();
                    }
                    else if (Balnce_of == 3)
                    {
                        var req = RequestData["result"];
                        var reqAddress = req["ETH"];
                        var reqBalancee = reqAddress["balancef"];
                        strAddress = reqBalancee.Value<string>();
                    }

                }
                else
                {
                    strAddress = "Error";
                }
            }
            catch (Exception)
            {
                strAddress = "Error";
                 
            }
            return strAddress;
        }

        //--Withraw Req  -->>

        public string Send_Withdraw_Req_Using_CoinPay(double dblAMt, int intCurrency, string strAddress, int intUserid, int Withdraw_id) //------Send withdraw from coinpayment
        {
            string strBalance = string.Empty;
            strBalance = CallAPI3("create_withdrawal", dblAMt.ToString(), intCurrency, strAddress, intUserid, Withdraw_id, parmss);

            return strBalance;
        }

        public string CallAPI3(string cmd, string dblamt, int WithDrwal_currency, string strAddress_user, int intUserid, int Withdraw_id, SortedList<string, string> parms = null) //------Send withdraw from coinpayment
        {
            dynamic ret = new Dictionary<string, dynamic>();
            try
            {
                if (parms == null)
                {
                    parms = new SortedList<string, string>();
                }
                parms["version"] = "1";
                parms["cmd"] = cmd;

                parms["amount"] = dblamt;
                if (WithDrwal_currency == 1)
                {
                    parms["currency"] = "BTC";
                    parms["currency2"] = "BTC";
                }
                else
                {
                    parms["currency"] = "ETH";
                    parms["currency2"] = "ETh";

                }

                parms["address"] = strAddress_user;
                parms["auto_confirm"] = "1";

                // parms["ipn_url"] = System.Configuration.ConfigurationManager.AppSettings["CallBack_CoinPay"];
                parms["key"] = System.Configuration.ConfigurationManager.AppSettings["publicKey_CoinPay"];


                string post_data = "";
                foreach (KeyValuePair<string, string> parm in parms)
                {
                    if (post_data.Length > 0) { post_data += "&"; }
                    post_data += parm.Key + "=" + Uri.EscapeDataString(parm.Value);
                }

                byte[] keyBytes = encoding.GetBytes(System.Configuration.ConfigurationManager.AppSettings["privateKey_CoinPay"]);
                byte[] postBytes = encoding.GetBytes(post_data);
                var hmacsha512 = new System.Security.Cryptography.HMACSHA512(keyBytes);
                string hmac = BitConverter.ToString(hmacsha512.ComputeHash(postBytes)).Replace("-", string.Empty);

                // do the post:
                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
                System.Net.WebClient cl = new System.Net.WebClient();
                cl.Headers.Add("Content-Type", "application/x-www-form-urlencoded");
                cl.Headers.Add("HMAC", hmac);
                cl.Encoding = encoding;
                try
                {
                    // string a = "{\"error\":\"ok\",\"result\":{\"address\":\"3J2hoyso1sXDcWib5D4aTN5A5hHNmVEqbH\" }}";
                    // var a = "{\"error\":\"ok\",\"result\":{\"id\":\"3J2hoyso1sXDcWib5D4aTN5A5hHNmVEqbH\",\"status\":\"1\",\"amount\":\"1000\" }}";

                    var resp = cl.UploadString("https://www.coinpayments.net/api.php", post_data);
                    var decoder = new System.Web.Script.Serialization.JavaScriptSerializer();
                    ret = decoder.Deserialize<Dictionary<string, dynamic>>(resp);
                    ret = JsonToString_withdraw(resp, intUserid, Withdraw_id);

                }
                catch (System.Net.WebException e)
                {
                    ret["error"] = "Exception while contacting CoinPayments.net: " + e.Message;
                }
                catch (Exception e)
                {
                    ret["error"] = "Unknown exception: " + e.Message;
                }
            }
            catch (Exception)
            {

                 
            }

            return ret;

        }

        public string JsonToString_withdraw(string jsonString, int intUserid, int Withdraw_id)
        {
            string strAddress = string.Empty;
            try
            {
                string a = JObject.Parse(jsonString).ToString();
                var RequestData = JObject.Parse(a);
                var status = RequestData["error"];
                if (status.ToString() == "ok")
                {
                    var req = RequestData["result"];
                    var hash = req["id"];
                    var withdraw_status = req["status"];
                    if (withdraw_status.ToString() == "1")
                    {
                        strAddress = GetJsonData(chkToken, "call ico_first_Response_Withdraw(" + intUserid + ",'" + jsonString.ToString() + "'," + Withdraw_id + ",1)");
                        clsodbc.executeNonQuery("update ico_Withdraw_Report set tran_hash='" + hash.ToString() + "' where id = " + Withdraw_id + "");

                    }
                    else
                    {
                        strAddress = GetJsonData(chkToken, "call ico_first_Response_Withdraw(" + intUserid + ",'" + jsonString.ToString() + "'," + Withdraw_id + ",2)");
                        clsodbc.executeNonQuery("update ico_Withdraw_Report set tran_hash='" + hash.ToString() + "' where id = " + Withdraw_id + "");

                    }
                    var amount = req["amount"];

                }
                else
                {
                    strAddress = GetJsonData(chkToken, "call ico_first_Response_Withdraw(" + intUserid + ",'" + jsonString.ToString() + "'," + Withdraw_id + ",2)");

                }
            }
            catch (Exception)
            {
                strAddress = "Exception Found";
                 
            }
            return strAddress;
        }

        //<------------------------ ERC 20 ------------------------------------>>

        public string GetERC20Bal(string strContractAdd, string strEtherAddrFrom)
        {
            string Balance = string.Empty;
            dynamic client = new RestClient();
            string sURL = null;
            // sURL = ((WebConfigurationManager.AppSettings["blockChainURL"] + "/merchant/" + WebConfigurationManager.AppSettings["guid"] + "/payment?password=" + WebConfigurationManager.AppSettings["block_pass"] + "&to=" + strAddr + "&amount=" + amt + "&from=0"));

            sURL = "http://134.213.205.4:4000/api/token/" + strContractAdd + "/" + strEtherAddrFrom + "/totalBalance";

            WebRequest request = default(WebRequest);
            request = WebRequest.Create(sURL);
            WebResponse resp = request.GetResponse();
            StreamReader reader = new StreamReader(resp.GetResponseStream());
            string responseString = reader.ReadToEnd();

            var objTrackLink = JObject.Parse(responseString.ToString());

            if (objTrackLink["status"].ToString() == "404")
            {

                Balance = "Error received";
            }
            else if (objTrackLink["status"].ToString() == "200")
            {

                string strsuccess = objTrackLink["res"].ToString();
                Balance = strsuccess;
            }


            return Balance;
        }

        public string GetERC2_Transfer(string strContract, string strFromEtherAddr, string strToEtherAddr, string strPrivateKey, double dblAmt) // this api use for transfer addre to addr , withdrawal (admin to mem) , deposite (by mem)
        {
            string strhash = string.Empty;
            dynamic client = new RestClient();
            string sURL = null;
            // sURL = ((WebConfigurationManager.AppSettings["blockChainURL"] + "/merchant/" + WebConfigurationManager.AppSettings["guid"] + "/payment?password=" + WebConfigurationManager.AppSettings["block_pass"] + "&to=" + strAddr + "&amount=" + amt + "&from=0"));
            sURL = "http://134.213.205.4:4000/api/transfer/" + strContract + "/" + strFromEtherAddr + "/" + strToEtherAddr + "/" + strPrivateKey + "/" + dblAmt + "";

            //WebRequest request = default(WebRequest);
            //request = WebRequest.Create(sURL);
            //WebResponse resp = request.GetResponse();
            //StreamReader reader = new StreamReader(resp.GetResponseStream());
            //string responseString = reader.ReadToEnd();

            string responseString = "{\"status\":200,\"message\":\"Tx Hash:\",\"res\":\"0x4e303a4df945a9ae50d46cd7a920f9f3c6a23d30df5c07c58d509da8b775025e\"}";
            var objTrackLink = JObject.Parse(responseString.ToString());
            if (objTrackLink["status"].ToString() == "404")
            {

                strhash = "Error received";
            }
            else if (objTrackLink["status"].ToString() == "200")
            {

                string strsuccess = objTrackLink["res"].ToString();
                strhash = strsuccess;
            }


            return strhash;
        }

        public string GetERC20WithdrawalUpdate(string hash, int param)
        {
            //here param = 1 -- > hash chek for deposite
            //here param = 2 -- > hash chek for tranfr
            //here param = 3 -- > hash chek for withdr

            string strApiPrivateKey = System.Configuration.ConfigurationManager.AppSettings["APIPriveteKey"];
            string Balance = string.Empty;
            dynamic client = new RestClient();
            string sURL = null;
            // sURL = ((WebConfigurationManager.AppSettings["blockChainURL"] + "/merchant/" + WebConfigurationManager.AppSettings["guid"] + "/payment?password=" + WebConfigurationManager.AppSettings["block_pass"] + "&to=" + strAddr + "&amount=" + amt + "&from=0"));

            sURL = "https://api.etherscan.io/api?module=transaction&action=getstatus&txhash=" + hash + "&apikey=" + strApiPrivateKey + "";


            WebRequest request = default(WebRequest);
            request = WebRequest.Create(sURL);
            WebResponse resp = request.GetResponse();
            StreamReader reader = new StreamReader(resp.GetResponseStream());
            string responseString = reader.ReadToEnd();
            //var objTrackLink = JObject.Parse(responseString.ToString());
            Balance = JsonToString2(responseString, hash, param);


            return Balance;
        }

        public string JsonToString2(string jsonString, string strhash, int param)
        {
            string strAddress = string.Empty;
            try
            {
                string a = JObject.Parse(jsonString).ToString();
                var RequestData = JObject.Parse(a);
                var status = RequestData["status"];
                if (status.ToString() == "1")
                {
                    var req = RequestData["result"];
                    var reqAddress = req["isError"];
                    strAddress = reqAddress.ToString();
                    if (strAddress == "1")
                    {
                        var reqDesc = req["errDescription"];
                        if (param == 3) // param 3 = withdr
                        {
                            clsodbc.executeNonQuery("call ERC20_transaction_history('" + strhash + "',4,'" + reqDesc.ToString() + "')"); //status 4 - rejecct by ERC20 .
                        }
                        else if (param == 1) // param 1 = deposi
                        {
                            string strQuery1 = "call erc_update_deposite_hash_status('" + strhash.ToString() + "',3,0)"; //3-reject
                            clsodbc.executeNonQuery(strQuery1);
                        }
                        else if (param == 2) // param 2 = trsnfr
                        {
                            string strQuery1 = "call erc_update_deposite_hash_status('" + strhash.ToString() + "',3,0)"; //3-reject
                            clsodbc.executeNonQuery(strQuery1);
                        }

                    }
                    else if (strAddress == "0")
                    {
                        GetFinalStatusFormERC20_Hash(strhash, param);
                    }

                }
                else
                {
                    strAddress = "2";
                }
            }
            catch (Exception)
            {
                strAddress = "Exception Found";
                 
            }
            return strAddress;
        }

        public string GetFinalStatusFormERC20_Hash(string strHash, int param)
        {
            string strApiPrivateKey = System.Configuration.ConfigurationManager.AppSettings["APIPriveteKey"];
            string Balance = string.Empty;
            dynamic client = new RestClient();
            string sURL = null;
            // sURL = ((WebConfigurationManager.AppSettings["blockChainURL"] + "/merchant/" + WebConfigurationManager.AppSettings["guid"] + "/payment?password=" + WebConfigurationManager.AppSettings["block_pass"] + "&to=" + strAddr + "&amount=" + amt + "&from=0"));

            sURL = "https://api.etherscan.io/api?module=transaction&action=gettxreceiptstatus&txhash=" + strHash + "&apikey=" + strApiPrivateKey + "";

            WebRequest request = default(WebRequest);
            request = WebRequest.Create(sURL);
            WebResponse resp = request.GetResponse();
            StreamReader reader = new StreamReader(resp.GetResponseStream());
            string responseString = reader.ReadToEnd();
            //var objTrackLink = JObject.Parse(responseString.ToString());
            Balance = JsonToString3(responseString, strHash, param);


            return Balance;
        }

        public string JsonToString3(string jsonString, string strhash, int param)
        {
            string strAddress = string.Empty;
            try
            {
                string a = JObject.Parse(jsonString).ToString();
                var RequestData = JObject.Parse(a);
                var status = RequestData["status"];
                if (status.ToString() == "1")
                {
                    var req = RequestData["result"];
                    var reqAddress = req["status"];
                    strAddress = reqAddress.ToString();
                    if (strAddress == "1")
                    {
                        int status1 = 0; // successfully
                        if (param == 3) // param = 2 means withdr
                        {
                            clsodbc.executeNonQuery("call update_trans_hash_ERC20('" + strhash.ToString() + "'," + status1 + ")");

                        }
                        else if (param == 1)// param = 1 means deposi
                        {
                            string strQuery1 = "call erc_update_deposite_hash_status('" + strhash.ToString() + "',2,0)"; //2-success
                            clsodbc.executeNonQuery(strQuery1);
                        }
                        else if (param == 2)// param = 1 means transfr
                        {
                            string strQuery1 = "call erc_update_deposite_hash_status('" + strhash.ToString() + "',2,0)"; //2-success
                            clsodbc.executeNonQuery(strQuery1);
                        }
                        //DataTable dt = new DataTable();
                        //dt = clsodbc.getDataTable("call ico_get_withdraw_mail_details('" + strhash.ToString() + "')");
                        //if (dt.Rows.Count > 0)
                        //{
                        //    clssmtp.send_WithdrawConfrm(dt.Rows[0][0].ToString(), "http://cruisebit.com/", dt.Rows[0][2].ToString(), DateTime.Now.ToShortDateString(), "CRBT", dt.Rows[0][1].ToString(), strhash, dt.Rows[0][3].ToString());
                        //}


                    }
                    else if (strAddress == "")
                    {

                    }

                }
                else
                {
                    strAddress = "2";
                }
            }
            catch (Exception)
            {
                strAddress = "Exception Found";
                 
            }
            return strAddress;
        }

        public string getTosken(int NumBer)
        {

            string readfile = File.ReadAllText(@"C:\inetpub\wwwroot\ws_tbtc\read.txt");

            // string updatedata = readfile.Remove(NumBer, 5);
            string updatedata = readfile.Substring(NumBer, 5);

            // File.WriteAllText(@"C:\Users\Exioms\Desktop\read.txt", updatedata);


            return updatedata;

        }


        private void GrantAccess(string fullPath)
        {
            DirectoryInfo dInfo = new DirectoryInfo(fullPath);
            DirectorySecurity dSecurity = dInfo.GetAccessControl();
            dSecurity.AddAccessRule(new FileSystemAccessRule(new SecurityIdentifier(WellKnownSidType.WorldSid, null), FileSystemRights.FullControl, InheritanceFlags.ObjectInherit | InheritanceFlags.ContainerInherit, PropagationFlags.NoPropagateInherit, AccessControlType.Allow));
            dInfo.SetAccessControl(dSecurity);
        }
    }
}
