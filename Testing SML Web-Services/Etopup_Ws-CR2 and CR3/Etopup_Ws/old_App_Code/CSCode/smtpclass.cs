﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using System.Net;
using System.Net.Mail;
using System.Configuration;

/// <summary>
/// s     
/// </summary> 
/// 
namespace Etopup_Ws.old_App_Code
{
    public class smtpclass
    {
        string website = System.Configuration.ConfigurationManager.AppSettings["crbturl"];
        string mail = System.Configuration.ConfigurationManager.AppSettings["mail"];
        string facebook = System.Configuration.ConfigurationManager.AppSettings["facebook"];
        string twitter = System.Configuration.ConfigurationManager.AppSettings["twitter"];
        string telegram = System.Configuration.ConfigurationManager.AppSettings["telegram"];
        string youtube = System.Configuration.ConfigurationManager.AppSettings["youtube"];


        public void send_EmailVerify(string strEmail, string strLink, string strUName, string strName, string strpass)
        {

            string to = strEmail;


            string subject = "Account verification";


            string strMailString = System.IO.File.ReadAllText(System.Web.HttpContext.Current.Server.MapPath("~/EmailerVerification1.html"));
            strMailString = strMailString.Replace("&&username&&", strUName);
            strMailString = strMailString.Replace("&&link&&", strLink);
            strMailString = strMailString.Replace("&&userid&&", strName);
            strMailString = strMailString.Replace("&&passeord&&", strpass);

            strMailString = strMailString.Replace("&&website&&", website);
            strMailString = strMailString.Replace("&&mail&&", mail);
            strMailString = strMailString.Replace("&&facebook&&", facebook);
            strMailString = strMailString.Replace("&&twitter&&", twitter);
            strMailString = strMailString.Replace("&&telegram&&", telegram);
            strMailString = strMailString.Replace("&&youtube&&", youtube);


            using (MailMessage mm = new MailMessage(ConfigurationManager.AppSettings["FromEmail"], to))
            {
                mm.Subject = subject;
                mm.Body = strMailString;
                mm.IsBodyHtml = true;
                SmtpClient smtp = new SmtpClient();
                smtp.Host = ConfigurationManager.AppSettings["Host"];
                smtp.EnableSsl = true;
                NetworkCredential NetworkCred = new NetworkCredential(ConfigurationManager.AppSettings["Username"], ConfigurationManager.AppSettings["Password"]);
                smtp.UseDefaultCredentials = true;
                smtp.Credentials = NetworkCred;
                smtp.Port = int.Parse(ConfigurationManager.AppSettings["Port"]);

                smtp.Send(mm);

                System.Threading.Thread.Sleep(3000);

            }
        }



        public void send_FrgtPass(string strEmail, string strLink, string fname)
        {

            string to = strEmail;


            string subject = "Reset Password";


            string strMailString = System.IO.File.ReadAllText(System.Web.HttpContext.Current.Server.MapPath("~/Frgt_Pass_Emailer.html"));
            strMailString = strMailString.Replace("&&username&&", fname);
            strMailString = strMailString.Replace("&&link&&", strLink);
            //strMailString = strMailString.Replace("&&passeord&&", "chkkahdc");
            //strMailString = strMailString.Replace("&&trans&&", "sfsgsg");

            strMailString = strMailString.Replace("&&website&&", website);
            strMailString = strMailString.Replace("&&mail&&", mail);
            strMailString = strMailString.Replace("&&facebook&&", facebook);
            strMailString = strMailString.Replace("&&twitter&&", twitter);
            strMailString = strMailString.Replace("&&telegram&&", telegram);
            strMailString = strMailString.Replace("&&youtube&&", youtube);

            using (MailMessage mm = new MailMessage(ConfigurationManager.AppSettings["FromEmail"], to))
            {
                mm.Subject = subject;
                mm.Body = strMailString;
                mm.IsBodyHtml = true;
                SmtpClient smtp = new SmtpClient();
                smtp.Host = ConfigurationManager.AppSettings["Host"];
                smtp.EnableSsl = true;
                NetworkCredential NetworkCred = new NetworkCredential(ConfigurationManager.AppSettings["Username"], ConfigurationManager.AppSettings["Password"]);
                smtp.UseDefaultCredentials = true;
                smtp.Credentials = NetworkCred;
                smtp.Port = int.Parse(ConfigurationManager.AppSettings["Port"]);

                smtp.Send(mm);

                System.Threading.Thread.Sleep(3000);

            }
        }


        public void send_AccountSummary(string strEmail, string strLink, string BtcAmt, string StrDate, string strCurry, string fname)
        {

            string to = strEmail;


            string subject = "Account Summary";


            string strMailString = System.IO.File.ReadAllText(System.Web.HttpContext.Current.Server.MapPath("~/btcReceiveEmailer.html"));

            strMailString = strMailString.Replace("&&username&&", fname);
            strMailString = strMailString.Replace(" &&BTCAmt&&", BtcAmt);
            strMailString = strMailString.Replace("&&DateTime&&", StrDate);
            strMailString = strMailString.Replace("&&link&&", strLink);
            strMailString = strMailString.Replace("&&CURR&&", strCurry);
            strMailString = strMailString.Replace("&&website&&", website);
            strMailString = strMailString.Replace("&&mail&&", mail);
            strMailString = strMailString.Replace("&&facebook&&", facebook);
            strMailString = strMailString.Replace("&&twitter&&", twitter);
            strMailString = strMailString.Replace("&&telegram&&", telegram);
            strMailString = strMailString.Replace("&&youtube&&", youtube);

            using (MailMessage mm = new MailMessage(ConfigurationManager.AppSettings["FromEmail"], to))
            {
                mm.Subject = subject;
                mm.Body = strMailString;
                mm.IsBodyHtml = true;
                SmtpClient smtp = new SmtpClient();
                smtp.Host = ConfigurationManager.AppSettings["Host"];
                smtp.EnableSsl = true;
                NetworkCredential NetworkCred = new NetworkCredential(ConfigurationManager.AppSettings["Username"], ConfigurationManager.AppSettings["Password"]);
                smtp.UseDefaultCredentials = true;
                smtp.Credentials = NetworkCred;
                smtp.Port = int.Parse(ConfigurationManager.AppSettings["Port"]);

                smtp.Send(mm);

                System.Threading.Thread.Sleep(3000);

            }
        }


        public void send_TransactionPass(string strEmail, string fname, string strTranspas, string strlink)
        {

            string to = strEmail;


            string subject = "Transaction Password";


            string strMailString = System.IO.File.ReadAllText(System.Web.HttpContext.Current.Server.MapPath("~/Transaction_Pass_Emailer.html"));

            strMailString = strMailString.Replace("&&username&&", fname);
            strMailString = strMailString.Replace("&&TransPass&&", strTranspas);
            strMailString = strMailString.Replace("&&link&&", strlink);

            strMailString = strMailString.Replace("&&website&&", website);
            strMailString = strMailString.Replace("&&mail&&", mail);
            strMailString = strMailString.Replace("&&facebook&&", facebook);
            strMailString = strMailString.Replace("&&twitter&&", twitter);
            strMailString = strMailString.Replace("&&telegram&&", telegram);
            strMailString = strMailString.Replace("&&youtube&&", youtube);

            using (MailMessage mm = new MailMessage(ConfigurationManager.AppSettings["FromEmail"], to))
            {
                mm.Subject = subject;
                mm.Body = strMailString;
                mm.IsBodyHtml = true;
                SmtpClient smtp = new SmtpClient();
                smtp.Host = ConfigurationManager.AppSettings["Host"];
                smtp.EnableSsl = true;
                NetworkCredential NetworkCred = new NetworkCredential(ConfigurationManager.AppSettings["Username"], ConfigurationManager.AppSettings["Password"]);
                smtp.UseDefaultCredentials = true;
                smtp.Credentials = NetworkCred;
                smtp.Port = int.Parse(ConfigurationManager.AppSettings["Port"]);

                smtp.Send(mm);

                System.Threading.Thread.Sleep(3000);

            }
        }

        public void send_PassSucc(string strEmail, string strUName, string strName, string strpass)
        {

            string to = strEmail;


            string subject = "Password Recovery";


            string strMailString = System.IO.File.ReadAllText(System.Web.HttpContext.Current.Server.MapPath("~/passwordchngsucc.html"));
            strMailString = strMailString.Replace("&&username&&", strUName);

            strMailString = strMailString.Replace("&&userid&&", strName);
            strMailString = strMailString.Replace("&&passeord&&", strpass);

            strMailString = strMailString.Replace("&&website&&", website);
            strMailString = strMailString.Replace("&&mail&&", mail);
            strMailString = strMailString.Replace("&&facebook&&", facebook);
            strMailString = strMailString.Replace("&&twitter&&", twitter);
            strMailString = strMailString.Replace("&&telegram&&", telegram);
            strMailString = strMailString.Replace("&&youtube&&", youtube);

            using (MailMessage mm = new MailMessage(ConfigurationManager.AppSettings["FromEmail"], to))
            {
                mm.Subject = subject;
                mm.Body = strMailString;
                mm.IsBodyHtml = true;
                SmtpClient smtp = new SmtpClient();
                smtp.Host = ConfigurationManager.AppSettings["Host"];
                smtp.EnableSsl = true;
                NetworkCredential NetworkCred = new NetworkCredential(ConfigurationManager.AppSettings["Username"], ConfigurationManager.AppSettings["Password"]);
                smtp.UseDefaultCredentials = true;
                smtp.Credentials = NetworkCred;
                smtp.Port = int.Parse(ConfigurationManager.AppSettings["Port"]);

                smtp.Send(mm);



            }
        }


        public void send()
        {

            string to = "vaibhav.nagulwar@exioms.com";


            string subject = " chk";


            string strMailString = System.IO.File.ReadAllText(System.Web.HttpContext.Current.Server.MapPath("~/EmailerVerification.html"));
            strMailString = strMailString.Replace("&&username&&", "vaibhav");


            using (MailMessage mm = new MailMessage(ConfigurationManager.AppSettings["FromEmail"], to))
            {
                mm.Subject = subject;
                mm.Body = strMailString;
                mm.IsBodyHtml = true;
                SmtpClient smtp = new SmtpClient();
                smtp.Host = ConfigurationManager.AppSettings["Host"];
                smtp.EnableSsl = true;
                NetworkCredential NetworkCred = new NetworkCredential(ConfigurationManager.AppSettings["Username"], ConfigurationManager.AppSettings["Password"]);
                smtp.UseDefaultCredentials = true;
                smtp.Credentials = NetworkCred;
                smtp.Port = int.Parse(ConfigurationManager.AppSettings["Port"]);
                Console.WriteLine("Sending Email......");
                smtp.Send(mm);
                Console.WriteLine("Email Sent.");
                System.Threading.Thread.Sleep(3000);

            }
        }

    }
}