﻿using RestSharp;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Data;
using Etopup_Ws.old_App_Code;
using System.Xml;
using Newtonsoft.Json.Linq;

namespace Etopup_Ws.old_App_Code
{
    public class SalaamApi
    {
        // http://localhost:8080/HSMEncryptionSample/GetBalance.php?SignKey=1cUyIjEXXYekcIXS4iRgig==&EncryptionKey=GFoD9s3n7g27nJ7G84d/7A==&ActivationCode=1767167451&RequestUniqueID=40193330143354
        ODBC objODBC = new ODBC();
        DateFormat dtDateFormat = new DateFormat();
        // Credintials
        string strSignKey = "1cUyIjEXXYekcIXS4iRgig==";
        string strEncryptionKey = "6l+4254XD/Qzszi+TN6kwA==";
        string strActivationCode = "3648206280";
      //  string strURL = "http://localhost/HSMEncryptionSample/"; // Local
        string strURL = "http://localhost:8080/HSMEncryptionSample/"; // Server
        public string getSalaamBalance()
        {
            string strBalance = "0";
            CommonFunction objCommonFun = new CommonFunction();

            string RequestUniqueID = objCommonFun.GenrateNumber(2) + dtDateFormat.getCurDateTime_Dayanamic_format("yymmddHHmmss").ToString();
            // string strRequest = strURL + "GetBalance.php?SignKey=" + strSignKey + "&EncryptionKey=" + strEncryptionKey + "&ActivationCode=" + strActivationCode + "&RequestUniqueID=" + RequestUniqueID;
            string strRequest = strURL + "GetBalance.php?RequestUniqueID=" + RequestUniqueID;
            var client = new RestClient(strRequest);
            var request = new RestRequest(Method.GET);
            request.AddHeader("Postman-Token", "79ef2ade-3d57-4140-bf9d-b72261aa9a8c");
            request.AddHeader("cache-control", "no-cache");
            IRestResponse response = client.Execute(request);
            string JSONData = response.Content.ToString();
            try
            {
                dynamic data = JObject.Parse(JSONData);
                string strRequestEncrypted = data.Request;
                string strResponseEncrypted = data.Response;
                string strResponseCode = data.ResponseCode;
                string strResponseDescription = data.ResponseDescription;
                if (strResponseCode == "000")
                {
                    string strPocketBalance = "NA";
                    strBalance = data.Balance;
                    string strAvaliableBalance = data.AvaliableBalance;
                    try
                    {
                        strPocketBalance = data.PocketBalance;
                    }
                    catch (Exception)
                    {

                    }
                }
                else
                {
                    strBalance = "0";
                }
            }
            catch (Exception)
            {
                strBalance = "0";
            }
            return strBalance;
            // data.id
        }
        public string SalaamTopup(int intInvestmentID,int intSalaamID,string strRequestUniqueID,string strMobileNoTxReference, string strAmount,string strEmailID)
        {
            string strProductCode = "P_4B8E30V";
            string strMPin = "3217";
            string JSONResponse = "NA";
           
            //   // $param = array('RequestUniqueID' => '12345077875','ProductCode'=>'P_4B8E30V','TxReference'=>'0744148888','Amount'=>'10.00','MPin'=>'3217','Email'=>'mohammad.ejaj@exioms.com','ANI'=>'','MethodName' => 'TopUp');
            // http://localhost:8080/HSMEncryptionSample/TopUp.php?SignKey=1cUyIjEXXYekcIXS4iRgig==&EncryptionKey=GFoD9s3n7g27nJ7G84d/7A==&ActivationCode=1767167451&RequestUniqueID=458721&ProductCode=P_4B8E30V&TxReference=0744148888&Amount=10&MPin=3217&Email=ejaj@exioms.com

            try
            {
              //   string strRequest = strURL + "TopUp.php?SignKey=" + strSignKey + "&EncryptionKey=" + strEncryptionKey + "&ActivationCode=" + strActivationCode + "&RequestUniqueID=" + strRequestUniqueID + "&ProductCode=" + strProductCode + "&TxReference=" + "0" + strMobileNoTxReference + "&Amount=" + strAmount + "&MPin=" + strMPin + "&Email=" + strEmailID;
                string strRequest = strURL + "TopUp.php?RequestUniqueID=" + strRequestUniqueID + "&ProductCode=" + strProductCode + "&TxReference=" + "0" + strMobileNoTxReference + "&Amount=" + strAmount + "&MPin=" + strMPin + "&Email=" + strEmailID;

                var client = new RestClient(strRequest);
                var request = new RestRequest(Method.GET);
                request.AddHeader("Postman-Token", "79ef2ade-3d57-4140-bf9d-b72261aa9a8c");
                request.AddHeader("cache-control", "no-cache");
                IRestResponse response = client.Execute(request);
                JSONResponse = response.Content.ToString();
                try
                {
                    objODBC.executeNonQuery("update er_salaam set sign_key='" + strSignKey + "',encryption_key='" + strEncryptionKey + "',activation_code='" + strActivationCode + "',product_code='" + strProductCode + "',m_pin='" + strMPin + "',request_unique_id='" + strRequestUniqueID + "',mobile_number='" + strMobileNoTxReference + "',amount='" + strAmount + "',email_id='" + strEmailID + "',request='" + strRequest + "',response='" + JSONResponse + "' where id='" + intSalaamID + "'");
                }
                catch(Exception)
                {

                }
            }
            catch(Exception)
            {

            }

            return JSONResponse;
        }
    }
}