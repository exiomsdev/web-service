﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml;
using Etopup_Ws.old_App_Code;
using System.Data;
using System.Collections;
using System.Text.RegularExpressions;

namespace Etopup_Ws.old_App_Code
{
    public class FundTransferUSSD
    {
        DateFormat objDF = new DateFormat();
        ODBC objODBC = new ODBC();
        ODBC_LOG objODBCLOG = new ODBC_LOG();
        DataTable dt = new DataTable();
        DataTable dt2 = new DataTable();
        DateFormat objDT = new DateFormat();
        Encrypt_Decrypt ED = new Encrypt_Decrypt();
        CreateFile objCreateFile = new CreateFile();
        SMPP_SMS_Format objSMPPSMS = new SMPP_SMS_Format();
        public string FundTransferMessage = "NA";


        public string FundTransfer(int intSenderID, string strReceiverUserName, string dblTransferAmountAFN, string strPhone)
        {
            string strMessage = "";
            double dblTransferAmount = Convert.ToDouble(dblTransferAmountAFN);
            DataSet dsDet = new DataSet();
            DataTable dt = new DataTable();
            CommonFunction objComFun = new CommonFunction();
            try
            {
                if (strReceiverUserName.Length == 11)
                {
                    strReceiverUserName = strReceiverUserName.Substring(2, strReceiverUserName.Length - 2);
                }
                if (strReceiverUserName.Length == 10)
                {
                    strReceiverUserName = strReceiverUserName.Substring(1, strReceiverUserName.Length - 1);
                }


                if (strReceiverUserName.Length == 9)
                {

                    // int intUserID = objODBC.getDataTable("SELECT IFNULL(SUM(l.userid), 0) as userid,l.usertype_id From er_login l inner join er_mobile m on l.userid=m.userid Where (l.username='" + strReceiverUserName + "'  or m.mobile='" + strReceiverUserName + "') and l.user_status=1 and l.Active=1");


                    //  int intDesignationType = objODBC.executeScalar_int("SELECT l.usertype_id From er_login l inner join er_mobile m on l.userid=m.userid Where (l.username='" + strReceiverUserName + "'  or m.mobile='" + strReceiverUserName + "') and l.user_status=1 and l.Active=1");



                    dt = objODBC.getDataTable("SELECT l.userid as userid,l.usertype_id From er_login l inner join er_mobile m on l.userid=m.userid Where (l.username='" + strReceiverUserName + "'  or m.mobile='" + strReceiverUserName + "') and l.user_status=1 and l.Active=1 order by l.userid limit 1");
                    if (dt.Rows.Count > 0)
                    {

                        int intUserID = int.Parse(dt.Rows[0][0].ToString());
                        int intDesignationType = int.Parse(dt.Rows[0][1].ToString());

                        string strIPAddress = "NA";
                        string strMacAddress = "NA";
                        string strOSDetails = "NA";
                        string strIMEINo = "NA";
                        string strGcmID = "NA";
                        string strAPPVersion = "NA";
                        int intSourceData = 2;
                        int intCreatedByType = 2;
                        int intActualUserID = intSenderID;
                        string strDate2 = objDT.getCurDateTimeString();
                        try
                        {

                            objCreateFile.CreateTextFile(strIPAddress, strMacAddress, strOSDetails, strIMEINo, strGcmID, strAPPVersion, intSourceData.ToString(), intCreatedByType, 30, " Wallet Fund Transfer USSD" + ",Sender User Name: " + intSenderID + "Receiver User Name:" + strReceiverUserName + "Transfer Amount : " + dblTransferAmount, intSenderID.ToString());
                        }
                        catch (Exception)
                        {

                        }
                        finally
                        {
                            dt.Dispose();
                            dsDet.Dispose();
                            dt.Dispose();
                        }
                        try
                        {
                            if ((intSenderID != intUserID) && (intUserID > 0) && (dblTransferAmount > 0) && (intSenderID > 0) && (intDesignationType != 0) && (strIPAddress != "" || strIPAddress != null) && (strMacAddress != "" || strMacAddress != null) && (strOSDetails != "" || strOSDetails != null) && (strIMEINo != "" || strIMEINo != null) && (strGcmID != "" || strGcmID != null) && (strAPPVersion != "" || strAPPVersion != null) && (intSourceData != 0) && (intCreatedByType != 0))
                            {
                                purchase_wallet1 objPurchaseWallet = new purchase_wallet1();
                                try
                                {
                                    objODBCLOG.executeNonQuery("call activity_logs('" + intSenderID + "','" + intDesignationType + "','" + intCreatedByType + "','" + strIPAddress + "','" + strMacAddress + "','" + strOSDetails + "','" + strIMEINo + "','" + strGcmID + "','" + strAPPVersion + "','" + intSourceData + "',' Wallet Fund Transfer From USSD',30,'','"+ strReceiverUserName + "')");


                                }
                                catch (Exception)
                                {

                                }

                                int intCount = objODBC.executeScalar_int("Select count(1) from er_login where userid='" + intUserID + "' and user_status=1 and Active=1 and parent_id='" + intSenderID + "'");
                                if (intCount > 0)
                                {

                                    int intCountCheckTransaction = objODBC.executeScalar_int("SELECT count(1) FROM `er_wallet_transfer_individual` WHERE parent_id='" + intSenderID + "' and userid='" + intUserID + "' and transfer_amt='" + dblTransferAmount + "' and TIMESTAMPDIFF(MINUTE,created_on,'" + strDate2 + "')<=5");
                                    if (intCountCheckTransaction == 0)
                                    {
                                        double dblUserBalance = objPurchaseWallet.ReturnUsableWalletAmount(intSenderID);

                                        if (dblUserBalance >= dblTransferAmount)
                                        {
                                            dt = objODBC.getDataTable("Call ap_walletTransfer(" + intSenderID + "," + intUserID + "," + dblTransferAmount + ")");
                                            string strTransID = dt.Rows[0][0].ToString();
                                            if (strTransID != "")
                                            {
                                                double dblSenderBalance = objPurchaseWallet.GetWalletAmount(intSenderID);
                                                double dblReceiverBalance = objPurchaseWallet.GetWalletAmount(intUserID);

                                                strMessage = "1,TRUE," + dblSenderBalance + "," + intSenderID + "," + intUserID + "," + dblTransferAmount + ",Wallet fund transferred successfully.Tnx-No:'" + strTransID + "'";

                                                // SMPP1 SMS Send
                                                DataSet ds = new DataSet();
                                                try
                                                {

                                                    ds = objODBC.getDataSet("SELECT l.full_name,l.username,m.mobile,m.operator_id From er_login l inner join er_mobile m on l.userid=m.userid Where l.userid='" + intSenderID + "' and m.usertype='" + intCreatedByType + "' and m.mob_type=1");
                                                    if (ds.Tables[0].Rows.Count > 0)
                                                    {
                                                        string strSenderFullName = ds.Tables[0].Rows[0][0].ToString();
                                                        string strSenderUserName = ds.Tables[0].Rows[0][1].ToString();
                                                        string strSenderMobileNumber = ds.Tables[0].Rows[0][2].ToString();
                                                        int strSenderOperaterID = int.Parse(ds.Tables[0].Rows[0][3].ToString());


                                                        if (strPhone.Length == 11)
                                                        {
                                                            strPhone = strPhone.Substring(2, strPhone.Length - 2);
                                                        }
                                                        Regex objMobilePatternMTN = new Regex(@"^(77|076)[\d]{7}$");
                                                        Regex objMobilePatternAWCC = new Regex(@"^(70|071)[\d]{7}$");
                                                        Regex objMobilePatternEtisalat = new Regex(@"^(78|73)[\d]{7}$");
                                                        Regex objMobilePatternSalaam = new Regex(@"^74[\d]{7}$");
                                                        Regex objMobilePatternRoshan = new Regex(@"^(79|72)[\d]{7}$");



                                                        if (objMobilePatternSalaam.IsMatch(strPhone))
                                                        {
                                                            strSenderMobileNumber = strPhone;
                                                            strSenderOperaterID = 1;

                                                        }
                                                        if (objMobilePatternEtisalat.IsMatch(strPhone))
                                                        {
                                                            strSenderMobileNumber = strPhone;
                                                            strSenderOperaterID = 2;

                                                        }
                                                        if (objMobilePatternRoshan.IsMatch(strPhone))
                                                        {
                                                            strSenderMobileNumber = strPhone;
                                                            strSenderOperaterID = 3;

                                                        }
                                                        if (objMobilePatternMTN.IsMatch(strPhone))
                                                        {
                                                            strSenderMobileNumber = strPhone;
                                                            strSenderOperaterID = 4;

                                                        }
                                                        if (objMobilePatternAWCC.IsMatch(strPhone))
                                                        {
                                                            strSenderMobileNumber = strPhone;
                                                            strSenderOperaterID = 5;
                                                        }
                                                        try
                                                        {
                                                            // SMPP SMS Code Below Sender
                                                            SMPP_SMS_Format objSMPP_SMS_Format = new SMPP_SMS_Format();
                                                            string strGetSMS = objSMPP_SMS_Format.FundTransferWalletSender(objODBC.executeScalar_str("SELECT username From er_login Where userid='" + intUserID + "'").ToString(), objODBC.executeScalar_str("SELECT full_name From er_login Where userid='" + intUserID + "'").ToString(), 4, dblTransferAmount, strSenderFullName, strSenderUserName.Trim(), dblSenderBalance, strTransID).ToString();
                                                            objComFun.er_insert_notification(intSenderID, "USSD Fund Request", strGetSMS, 2);
                                                            FundTransferMessage = strGetSMS;

                                                            string strSMPPResponse = "NA";

                                                            try
                                                            {
                                                                // objODBCLOG.executeNonQuery("INSERT INTO `er_wallet_transfer_sms_log`(`userid`, `username`, `mobile`, `operator_id`, `SMS`, `created_on`) VALUES ('" + intSenderID + "','" + objODBC.executeScalar_str("SELECT username From er_login Where userid='" + intSenderID + "'").ToString() + "','" + strSenderMobileNumber + "','" + strSenderOperaterID + "','" + strGetSMS + "','" + strDate2 + "')");

                                                                objComFun.er_SMS_Alert(intSenderID, 2, strSenderMobileNumber, strSenderOperaterID, strGetSMS, 1, strSMPPResponse);

                                                            }
                                                            catch (Exception)
                                                            {
                                                                //  objComFun.er_SMS_Alert(intSenderID, 2, strSenderMobileNumber, strSenderOperaterID, strGetSMS, 1, strSMPPResponse);
                                                            }
                                                            finally
                                                            {

                                                            }

                                                            // SMPP SMS Code Below Receiver                                      
                                                            string strGetReceiverSMS = objSMPP_SMS_Format.FundTransferWalletReceiver(objODBC.executeScalar_str("SELECT username From er_login Where userid='" + intUserID + "'").ToString(), objODBC.executeScalar_str("SELECT full_name From er_login Where userid='" + intUserID + "'").ToString(), 4, dblTransferAmount, strSenderFullName, strSenderUserName, dblReceiverBalance, strTransID).ToString();
                                                            objComFun.er_insert_notification(intUserID, "USSD Fund Request", strGetReceiverSMS, 2);

                                                            strSMPPResponse = "NA";
                                                            DataTable dtSMPP = new DataTable();
                                                            try
                                                            {


                                                                dtSMPP = objODBC.getDataTable("Select mobile,operator_id from er_mobile where userid='" + intUserID + "' and usertype=2 and mob_type=1");

                                                                if (dtSMPP.Rows.Count > 0)
                                                                {
                                                                    string strMobile = dtSMPP.Rows[0][0].ToString();
                                                                    int intOperaterID = int.Parse(dtSMPP.Rows[0][1].ToString());
                                                                    try
                                                                    {
                                                                        // objODBCLOG.executeNonQuery("INSERT INTO `er_wallet_transfer_sms_log`(`userid`, `username`, `mobile`, `operator_id`, `SMS`, `created_on`) VALUES ('" + intUserID + "','" + objODBC.executeScalar_str("SELECT username From er_login Where userid='" + intUserID + "'").ToString() + "','" + strMobile + "','" + intOperaterID + "','" + strGetReceiverSMS + "','" + strDate2 + "')");
                                                                        objComFun.er_SMS_Alert(intUserID, 2, strMobile, intOperaterID, strGetReceiverSMS, 1, strSMPPResponse);
                                                                    }
                                                                    catch (Exception)
                                                                    {
                                                                        // objComFun.er_SMS_Alert(intUserID, 2, strMobile, intOperaterID, strGetReceiverSMS, 1, strSMPPResponse);
                                                                    }
                                                                }
                                                            }
                                                            catch (Exception)
                                                            {

                                                            }
                                                            finally
                                                            {
                                                                dtSMPP.Dispose();
                                                            }
                                                        }
                                                        catch (Exception ex)
                                                        {
                                                            ds.Dispose();
                                                            dt.Dispose();
                                                        }
                                                    }
                                                }
                                                catch (Exception ex)
                                                {
                                                    ds.Dispose();
                                                    dt.Dispose();
                                                }
                                            }
                                            else
                                            {
                                                strMessage = strMessage = "2,FALSE,NA,NA,NA," + dblTransferAmount + ",Your Transaction  is in process   ";

                                                return strMessage;
                                            }

                                        }
                                        else
                                        {
                                            SMPP_SMS_Format objSMPP_SMS_Format = new SMPP_SMS_Format();
                                            string strGetReceiverSMS = objSMPP_SMS_Format.LowBalance(objODBC.executeScalar_str("SELECT username From er_login Where userid='" + intUserID + "' and user_status=1 and Active=1 and parent_id='" + intSenderID + "'").ToString(), objODBC.executeScalar_str("SELECT full_name From er_login Where userid='" + intUserID + "' and user_status=1 and Active=1 and parent_id='" + intSenderID + "'").ToString(), dblTransferAmount).ToString();
                                            objComFun.er_insert_notification(intUserID, "USSD Fund Request", strGetReceiverSMS, 2);

                                            strMessage = "2,FALSE,NA,NA,NA," + dblTransferAmount + ",You do not have sufficient balance in your wallet ";
                                            return strMessage;

                                        }
                                    }
                                    else
                                    {
                                        strMessage = "2,FALSE,NA,NA,NA," + dblTransferAmount + ",You can not Transfer Balance with same amount more than once within 5 minutes!";
                                        return strMessage;
                                    }

                                }
                                else
                                {
                                    strMessage = "2,FALSE,NA,NA,NA," + dblTransferAmount + ",You can not Transfer balance to this user ";
                                    return strMessage;
                                }
                            }
                            else
                            {
                                strMessage = "2,FALSE,NA,NA,NA," + dblTransferAmount + ",Kindly enter valid user details ";
                                return strMessage;
                            }
                        }
                        catch (Exception ex)
                        {
                            strMessage = strMessage = "2,FALSE,NA,NA,NA," + dblTransferAmount + ",Your Transaction  is in process   ";

                            return strMessage;
                        }
                    }
                    else
                    {
                        strMessage = strMessage = "2,FALSE,NA,NA,NA," + dblTransferAmount + ",Kindly Enter Valid Mobile Number ";
                        return strMessage;
                    }
                }
                else
                {
                    strMessage = strMessage = "2,FALSE,NA,NA,NA," + dblTransferAmount + ",Kindly Enter Valid Mobile Number ";
                    return strMessage;
                }
            }
            catch (Exception ex)
            {
                strMessage = strMessage = "2,FALSE,NA,NA,NA," + dblTransferAmount + ",Something Went Wrong.";

                return strMessage;
            }
            finally
            {
                dsDet.Dispose();
                dt.Dispose();
            }

            return strMessage;
        }

    }
}