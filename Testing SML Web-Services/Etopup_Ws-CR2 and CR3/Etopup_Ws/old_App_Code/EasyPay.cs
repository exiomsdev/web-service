﻿using RestSharp;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Data;
using Etopup_Ws.old_App_Code;
using System.Xml;

namespace Etopup_Ws.old_App_Code
{
    public class EasyPay
    {
        ODBC objODBC = new ODBC();
        public string getBalance(string strTerminalID, string strPassword, string strRequestType, string strLoginID)
        {

            var client = new RestClient("http://gate.payvand.af/xml/");
            var request = new RestRequest(Method.POST);
            request.AddHeader("Postman-Token", "ec35ceed-264b-43da-8be4-5183155bed74");
            request.AddHeader("cache-control", "no-cache");
            request.AddHeader("Content-Type", "text/xml");
            request.AddParameter("undefined", "<?xml version=\"1.0\" encoding=\"utf-8\"?>\r\n<request>\r\n    <request-type>" + strRequestType + "</request-type>\r\n    <terminal-id>" + strTerminalID + "</terminal-id>\r\n    <login>" + strLoginID + "</login>    <password-md5>" + strPassword + "</password-md5>\r\n</request>\r\n", ParameterType.RequestBody);
            IRestResponse response = client.Execute(request);           

            return response.Content.ToString();

        }

        // '"+ strRequestType + "'=1 For TopUP
        public string ESPY_Transaction(int intEasypayID, string strTerminalID, string strPassword, string strRequestType, string strLoginID, string strTransactionID, string strSign, string strSrVid, string strMobileAccount, string strAmount, string strNetAmount)
        {
            // Sign = sha1 hash of the "md5(password) + txn-id + account” string.
            var varRequestparameter = "<?xml version=\"1.0\" encoding=\"utf-8\"?>\n<request>\n <request-type>" + strRequestType + "</request-type>\n    <terminal-id>" + strTerminalID + "</terminal-id>\n <login>" + strLoginID + "</login>\n <password-md5>" + strPassword + "</password-md5>\n    <payment txn-id=\"" + strTransactionID + "\" sign=\"" + strSign + "\">\n <to>\n <srvid>" + strSrVid + "</srvid>\n            <account>" + strMobileAccount + "</account>\n <amount>" + strAmount + "</amount>\n<amount-net>" + strAmount + "</amount-net>\n </to>\n </payment>\n</request>";

            var client = new RestClient("http://gate.payvand.af/xml/");
            var request = new RestRequest(Method.POST);
            request.AddHeader("Postman-Token", "ee83791a-ce71-4682-8072-d95ea3583ad0");
            request.AddHeader("cache-control", "no-cache");
            request.AddHeader("user_id", "");
            request.AddHeader("Content-Type", "text/xml");
            request.AddParameter("undefined", varRequestparameter, ParameterType.RequestBody);
            //  var response = objODBC.executeScalar_str("SELECT request FROM `temp` where id=3");
            IRestResponse response = client.Execute(request);


            try
            {
                objODBC.executeNonQuery("Update er_easy_pay_request set Request='" + varRequestparameter.ToString() + "',`Response`='" + response.Content.ToString() + "' where id='" + intEasypayID + "'");
            }
            catch (Exception)
            {

            }

            return response.Content.ToString();

            /*   string Res = objODBC.executeScalar_str("Select Response from er_easy_pay_request where id=4");
               objODBC.executeNonQuery("Update er_easy_pay_request set `Response`='" + Res.ToString() + "' where id='" + intEasypayID + "'");
               return Res; */
        }


        public string getEasyPayBalanceAPI()
        {
            try
            {
                string strPassword = "set2018";
                string strTerminalID = "347922";
                string strLoginID = "mutahedapi";
                string MD5Password = "9b3ee0226cd2556f96eafbbdb1e5f398";
                string strRequestType = "5";
                var reqParameter = "<?xml version=\"1.0\" encoding=\"utf-8\"?>\r\n<request>\r\n    <request-type>" + strRequestType + "</request-type>\r\n    <terminal-id>" + strTerminalID + "</terminal-id>\r\n    <login>" + strLoginID + "</login>    <password-md5>" + MD5Password + "</password-md5>\r\n</request>\r\n";
                var client = new RestClient("http://gate.payvand.af/xml/");
                var request = new RestRequest(Method.POST);
                request.AddHeader("Postman-Token", "ec35ceed-264b-43da-8be4-5183155bed74");
                request.AddHeader("cache-control", "no-cache");
                request.AddHeader("Content-Type", "text/xml");
                request.AddParameter("undefined", reqParameter, ParameterType.RequestBody);
                IRestResponse response = client.Execute(request);
                XmlDocument xmlDoc = new XmlDocument();
                xmlDoc.LoadXml(response.Content.ToString());
                XmlNode nodeList1 = xmlDoc.DocumentElement.SelectSingleNode("response");
                var balance = xmlDoc.ChildNodes[1].ChildNodes[2].InnerXml.ToString();
                return balance.ToString();
            }
            catch(Exception)
            {
                return "0";
            }
        }

            public ArrayList getEasyPayBalance(string strTerminalID, string strPassword, string strRequestType, string strLoginID)
        {

            ArrayList arrUserParams = new ArrayList();

            try
            {
                var reqParameter = "<?xml version=\"1.0\" encoding=\"utf-8\"?>\r\n<request>\r\n    <request-type>" + strRequestType + "</request-type>\r\n    <terminal-id>" + strTerminalID + "</terminal-id>\r\n    <login>" + strLoginID + "</login>    <password-md5>" + strPassword + "</password-md5>\r\n</request>\r\n";
                var client = new RestClient("http://gate.payvand.af/xml/");
                var request = new RestRequest(Method.POST);
                request.AddHeader("Postman-Token", "ec35ceed-264b-43da-8be4-5183155bed74");
                request.AddHeader("cache-control", "no-cache");
                request.AddHeader("Content-Type", "text/xml");
                request.AddParameter("undefined", reqParameter, ParameterType.RequestBody);
                IRestResponse response = client.Execute(request);

                
            
                XmlDocument xmlDoc = new XmlDocument();
                xmlDoc.LoadXml(response.Content.ToString());
                XmlNode nodeList1 = xmlDoc.DocumentElement.SelectSingleNode("response");




                arrUserParams.Add("True");
                arrUserParams.Add("RequestType");
                var requesttype = xmlDoc.ChildNodes[1].ChildNodes[0].InnerXml.ToString();
                arrUserParams.Add(requesttype);

                arrUserParams.Add("Terminal ID");
                var terminalid = xmlDoc.ChildNodes[1].ChildNodes[1].InnerXml.ToString();
                arrUserParams.Add(terminalid);

                arrUserParams.Add("Balance");
                var balance = xmlDoc.ChildNodes[1].ChildNodes[2].InnerXml.ToString();
                arrUserParams.Add(balance);

                arrUserParams.Add("Overdraft");
                var overdraft = xmlDoc.ChildNodes[1].ChildNodes[3].InnerXml.ToString();
                arrUserParams.Add(overdraft);

                arrUserParams.Add("Withheld");
                var withheld = xmlDoc.ChildNodes[1].ChildNodes[4].InnerXml.ToString();
                arrUserParams.Add(withheld);

                arrUserParams.Add("Payable");
                var payable = xmlDoc.ChildNodes[1].ChildNodes[5].InnerXml.ToString();
                arrUserParams.Add(payable);



            }
            catch (Exception e1)
            {
                arrUserParams.Add(e1.ToString());
            }
            return arrUserParams;
        }


        // '"+ strRequestType + "'=1 For TopUP
        public string ESPY_Transaction_Pending(int intEasypayID,int intRechargeTableID ,string strTerminalID, string strPassword, string strRequestType, string strLoginID,string payid)
        {
            // Sign = sha1 hash of the "md5(password) + txn-id + account” string.
            var RequestParameter = "<?xml version=\"1.0\" encoding=\"utf-8\"?>\r\n<request> \r\n<request-type>" + strRequestType + "</request-type>\r\n<terminal-id>" + strTerminalID + "</terminal-id>\r\n<login>" + strLoginID + "</login>\r\n<password-md5>" + strPassword + "</password-md5>\r\n<payment>\r\n<payid>" + payid + "</payid>\r\n</payment>\r\n</request>\r\n";
            var client = new RestClient("http://gate.payvand.af/xml/");
            var request = new RestRequest(Method.POST);
            request.AddHeader("Postman-Token", "ee83791a-ce71-4682-8072-d95ea3583ad0");
            request.AddHeader("cache-control", "no-cache");
            request.AddHeader("user_id", "");
            request.AddHeader("Content-Type", "text/xml");
            
            request.AddParameter("undefined", RequestParameter, ParameterType.RequestBody);
            //  var response = objODBC.executeScalar_str("SELECT request FROM `temp` where id=3");
            IRestResponse response = client.Execute(request);

            try
            {              
                objODBC.executeNonQuery("INSERT INTO `er_easy_pay_pending`(`payid`,`recharge_table_id`,`easypay_table_id`,`request`,`response`) VALUES ('"+ payid + "','"+ intRechargeTableID + "','"+ intEasypayID + "','"+ RequestParameter + "','"+ response.Content.ToString() + "')");
            }
            catch (Exception)
            {

            }
            return response.Content.ToString();

            /*   string Res = objODBC.executeScalar_str("Select Response from er_easy_pay_request where id=4");
               objODBC.executeNonQuery("Update er_easy_pay_request set `Response`='" + Res.ToString() + "' where id='" + intEasypayID + "'");
               return Res; */
        }

    }
}