﻿using Microsoft.VisualBasic;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Web;
using System.Data.Odbc;
using System.IO;
using System.Security.Cryptography;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using MySql.Data;
using Etopup_Ws.old_App_Code;
using System.Text.RegularExpressions;
using System.Xml;
using System.Text;
using Newtonsoft.Json;

namespace Etopup_Ws.old_App_Code
{
    public class CommonFunction
    {
        ODBC objODBC = new ODBC();
        ODBC_LOG objODBCLOG = new ODBC_LOG();
        DataSet ds = new DataSet();
        DateFormat objDT = new DateFormat();
        // **** Generate Random String *****
        public string GenrateRandomString()
        {
            string allowedChars = "";
            allowedChars = "1,2,3,4,5,6,7,8,9,0";
            allowedChars += "A,B,C,D,E,F,G,H,I,J,K,L,M,N,O,P,Q,R,S,T,U,V,W,X,Y,Z,";
            allowedChars += "1,2,3,4,5,6,7,8,9,0";
            char[] sep = new[] { ',' };
            string[] arr = allowedChars.Split(sep);
            string passwordString = "";
            string temp = "";
            Random rand = new Random();
            for (int i = 0; i <= Convert.ToInt32(6) - 1; i++)
            {
                temp = arr[rand.Next(0, arr.Length)];
                passwordString += temp;
            }

            return passwordString;
        }
        public DataSet getDataSet_from_string(bool bValue, string strResult)
        {
            DataSet ds = new DataSet();

            DataTable dt = new DataTable("MyTable");
            dt.Columns.Add(new DataColumn("values", typeof(bool)));
            dt.Columns.Add(new DataColumn("result", typeof(string)));

            DataRow dr = dt.NewRow();
            dr["values"] = bValue;
            dr["result"] = strResult;
            dt.Rows.Add(dr);
            ds.Tables.Add(dt);

            return ds;
        }
        public string GenrateRandoTpin()
        {
            string allowedChars = "";
            allowedChars = "1,2,3,4,5,6,7,8,9,0";
            allowedChars += "A,B,C,D,E,F,G,H,I,J,K,L,M,N,O,P,Q,R,S,T,U,V,W,X,Y,Z,";
            allowedChars += "1,2,3,4,5,6,7,8,9,0";
            char[] sep = new[] { ',' };
            string[] arr = allowedChars.Split(sep);
            string passwordString = "";
            string temp = "";
            Random rand = new Random();
            for (int i = 0; i <= Convert.ToInt32(4) - 1; i++)
            {
                temp = arr[rand.Next(0, arr.Length)];
                passwordString += temp;
            }

            return passwordString;
        }
        public string GenrateNumberString()
        {
            string allowedChars = "";
            allowedChars = "1,2,3,4,5,6,7,8,9,0";
            // allowedChars += "A,B,C,D,E,F,G,H,I,J,K,L,M,N,O,P,Q,R,S,T,U,V,W,X,Y,Z,";
            // allowedChars += "1,2,3,4,5,6,7,8,9,0";
            char[] sep = new[] { ',' };
            string[] arr = allowedChars.Split(sep);
            string passwordString = "";
            string temp = "";
            Random rand = new Random();
            for (int i = 0; i <= Convert.ToInt32(6) - 1; i++)
            {
                temp = arr[rand.Next(0, arr.Length)];
                passwordString += temp;
            }
            return passwordString;
        }
        public string GenrateNumber(int intCount)
        {
            string allowedChars = "";
            allowedChars = "1,2,3,4,5,6,7,8,9,0";
            // allowedChars += "A,B,C,D,E,F,G,H,I,J,K,L,M,N,O,P,Q,R,S,T,U,V,W,X,Y,Z,";
            // allowedChars += "1,2,3,4,5,6,7,8,9,0";
            char[] sep = new[] { ',' };
            string[] arr = allowedChars.Split(sep);
            string passwordString = "";
            string temp = "";
            Random rand = new Random();
            for (int i = 0; i <= Convert.ToInt32(intCount) - 1; i++)
            {
                temp = arr[rand.Next(0, arr.Length)];
                passwordString += temp;
            }
            return passwordString;
        }

        public string GenrateRandomKeyString()
        {
            string allowedChars = "";
            allowedChars = "A,H,J,K,L,O,P,Q,S,D,F";
            allowedChars += "A,B,C,D,E,F,G,H,I,J,K,L,M,N,O,P,Q,R,S,T,U,V,W,X,Y,Z,";
            allowedChars += "a,b,c,d,e,f,g,h,i,j,k,l,m,n,p,q,r,s,t,u,v,w,x,y,z";
            char[] sep = new[] { ',' };
            string[] arr = allowedChars.Split(sep);
            string passwordString = "";
            string temp = "";
            Random rand = new Random();
            for (int i = 0; i <= Convert.ToInt32(8) - 1; i++)
            {
                temp = arr[rand.Next(0, arr.Length)];
                passwordString += temp;
            }

            return passwordString;
        }
        public ArrayList Get_ArrayLlist(string strQuery)
        {
            ArrayList arrUserParams = new ArrayList();
            try
            {
                ds = objODBC.getDataSet(strQuery);
                if (ds.Tables[0].Rows.Count > 0)
                {
                    arrUserParams.Add("TRUE");
                    System.Data.DataTable firstTable = ds.Tables[0];
                    for (int i = 0; i <= firstTable.Rows.Count - 1; i++)
                    {
                        for (int j = 0; j <= firstTable.Columns.Count - 1; j++)
                        {
                            arrUserParams.Add(firstTable.Rows[i][j].ToString());
                        }
                    }
                }
                else
                {
                    arrUserParams.Add("FALSE");
                }
                return arrUserParams;
            }
            catch (Exception ex)
            {
                arrUserParams.Add(ex.Message.ToString());
                return arrUserParams;
            }
            finally
            {
                ds.Dispose();
            }
        }
        public string GenratePassKey()
        {
            string allowedChars = "";
            allowedChars = "1,2,3,4,5,6,7,8,9,0";
            allowedChars += "A,B,C,D,E,F,G,H,I,J,K,L,M,N,O,P,Q,R,S,T,U,V,W,X,Y,Z,";
            allowedChars += "a,b,c,d,e,f,g,h,i,j,k,l,m,n,p,q,r,s,t,u,v,w,x,y,z,";
            allowedChars += "1,2,3,4,5,6,7,8,9,0";
            char[] sep = new[] { ',' };
            string[] arr = allowedChars.Split(sep);
            string passwordString = "";
            string temp = "";
            Random rand = new Random();
            for (int i = 0; i <= Convert.ToInt32(7); i++)
            {
                temp = arr[rand.Next(0, arr.Length)];
                passwordString += temp;
            }

            return passwordString;
        }
        public ArrayList Get_ArrayLlist_From_Dataset(DataSet ds)
        {
            ArrayList arrUserParams = new ArrayList();
            try
            {
                if (ds.Tables[0].Rows.Count > 0)
                {
                    if (ds.Tables[0].Rows[0][0].ToString() != "False")
                    {
                        arrUserParams.Add("TRUE");
                    }

                    System.Data.DataTable firstTable = ds.Tables[0];
                    for (int i = 0; i <= firstTable.Rows.Count - 1; i++)
                    {
                        for (int j = 0; j <= firstTable.Columns.Count - 1; j++)
                        {
                            if (firstTable.Rows[i][j].ToString() == "" || firstTable.Rows[i][j].ToString() == null || firstTable.Rows[i][j].ToString() == " " || ((firstTable.Rows[i][j].ToString()).Length < 0))
                            {
                                arrUserParams.Add("NA");
                            }
                            else
                            {
                                arrUserParams.Add(firstTable.Rows[i][j].ToString());
                            }
                        }
                    }
                }
                else
                {
                    arrUserParams.Add("FALSE");
                }
                return arrUserParams;
            }
            catch (Exception ex)
            {
                arrUserParams.Add(ex.Message.ToString());
                return arrUserParams;
            }
            finally
            {
                ds.Dispose();
            }
        }


        public bool er_insert_notification(int intUserID, string strtitle, string strdescription, int intCreatedBYType)
        {
            bool strResult;
            string strDate = objDT.getCurDateTimeString();
            try
            {

                int intCount = objODBC.executeScalar_int("Select count(1) from er_notification where userid='" + intUserID + "' and description='" + strdescription + "' and read_status=0");
                if (intCount == 0)
                {
                    objODBC.executeNonQuery("insert into er_notification(userid,title,description,created_on,read_status,user_type)values(" + intUserID + ",'" + strtitle + "','" + strdescription + "','" + strDate + "',0,'" + intCreatedBYType + "')");
                }

              strResult = true;
               
            }
            catch(Exception)
            {
                strResult = true;
            }
            return strResult;
        }
        // intCreatedbyType 1:Admin 2:Member
        // Status : 0: Pending , 1: Send ,2:Not Send


        public bool er_SMS_Alert(int intUserID, int intCreatedbyType, string strMobileNumber, int intOperaterID, string strMessage, int intStatus, string strSMPPResposne)
        {
            bool strResult = false;          

            string strMobile1 = "NA", strOperatorID1 = "NA", strMobile2 = "NA", strOperatorID2 = "NA";
            string strUserName = objODBC.executeScalar_str("SELECT username from er_login where userid='" + intUserID + "'");
            try
            {
                if (intStatus != 9)
                {
                    try
                    {
                        if (strMobileNumber.Length == 11)
                        {
                            strMobileNumber = strMobileNumber.Substring(2, strMobileNumber.Length - 2);
                        }
                        if (strMobileNumber.Length == 10)
                        {
                            strMobileNumber = strMobileNumber.Substring(1, strMobileNumber.Length - 1);
                        }
                        ds = objODBC.getDataSet("SELECT `mobile`, `operator_id` FROM `er_mobile` WHERE userid='" + intUserID + "' and usertype=2 and mobile!='" + strMobileNumber + "' and operator_id!='" + intOperaterID + "' GROUP BY `operator_id`");
                        if (ds.Tables[0].Rows.Count > 0)
                        {
                            if (ds.Tables[0].Rows.Count == 1)
                            {
                                strMobile1 = ds.Tables[0].Rows[0]["mobile"].ToString();
                                strMobile2 = "NA";
                                strOperatorID1 = ds.Tables[0].Rows[0]["operator_id"].ToString();
                                strOperatorID2 = "NA";
                            }
                            else if (ds.Tables[0].Rows.Count == 2)
                            {
                                strMobile1 = ds.Tables[0].Rows[0]["mobile"].ToString();
                                strMobile2 = ds.Tables[0].Rows[1]["mobile"].ToString();
                                strOperatorID1 = ds.Tables[0].Rows[0]["operator_id"].ToString();
                                strOperatorID2 = ds.Tables[0].Rows[1]["operator_id"].ToString();
                            }
                            else
                            {
                                strMobile1 = "NA";
                                strOperatorID1 = "NA";
                                strMobile2 = "NA";
                                strOperatorID2 = "NA";
                            }
                        }
                        else
                        {
                            strMobile1 = "NA";
                            strOperatorID1 = "NA";
                            strMobile2 = "NA";
                            strOperatorID2 = "NA";


                        }
                    }
                    catch(Exception)
                    {

                    }

                    try
                    {
                        string strDate = objDT.getCurDateTimeString();
                        int intCount = objODBCLOG.executeScalar_int("Select count(1) from er_wallet_transfer_sms_log where userid='" + intUserID + "' and SMS='" + strMessage + "' and mobile='" + strMobileNumber + "' And TIMESTAMPDIFF(MINUTE,created_on,'" + strDate + "')<=20");
                        if (intCount == 0)
                        {
                            objODBCLOG.executeNonQuery("INSERT INTO `er_wallet_transfer_sms_log`(`userid`, `username`, `mobile`, `operator_id`, `SMS`, `created_on`,`status`, `mobile1`, `operator_id1`, `mobile2`, `operator_id2`) VALUES ('" + intUserID + "','" + strUserName + "','" + strMobileNumber + "','" + intOperaterID + "','" + strMessage + "','" + objDT.getCurDateTimeString() + "',0,'" + strMobile1 + "','" + strOperatorID1 + "','" + strMobile2 + "','" + strOperatorID2 + "')");
                        }
                        strResult = true;
                    }
                    catch (Exception)
                    {
                        strResult = false;
                    }

                }
                else
                {
                    strMobile1 = "NA";
                    strOperatorID1 = "NA";
                    strMobile2 = "NA";
                    strOperatorID2 = "NA";
                    try
                    {
                        string strDate = objDT.getCurDateTimeString();
                        int intCount = objODBCLOG.executeScalar_int("Select count(1) from er_wallet_transfer_sms_log where userid='" + intUserID + "' and SMS='" + strMessage + "' and mobile='" + strMobileNumber + "' And TIMESTAMPDIFF(MINUTE,created_on,'" + strDate + "')<=20");
                        if (intCount == 0)
                        {
                            objODBCLOG.executeNonQuery("INSERT INTO `er_wallet_transfer_sms_log`(`userid`, `username`, `mobile`, `operator_id`, `SMS`, `created_on`,`status`, `mobile1`, `operator_id1`, `mobile2`, `operator_id2`) VALUES ('" + intUserID + "','" + strUserName + "','" + strMobileNumber + "','" + intOperaterID + "','" + strMessage + "','" + objDT.getCurDateTimeString() + "',0,'" + strMobile1 + "','" + strOperatorID1 + "','" + strMobile2 + "','" + strOperatorID2 + "')");
                        }
                        strResult = true;
                    }
                    catch (Exception)
                    {

                    }
                }
            }
            catch (Exception)
            {
                strMobile1 = "NA";
                strOperatorID1 = "NA";
                strMobile2 = "NA";
                strOperatorID2 = "NA";

                //try
                //{
                //    string strDate = objDT.getCurDateTimeString();
                //    int intCount = objODBCLOG.executeScalar_int("Select count(1) from er_wallet_transfer_sms_log where userid='" + intUserID + "' and SMS='" + strMessage + "' and mobile='" + strMobileNumber + "' And TIMESTAMPDIFF(MINUTE,created_on,'" + strDate + "')<=20");
                //    if (intCount == 0)
                //    {
                //        objODBCLOG.executeNonQuery("INSERT INTO `er_wallet_transfer_sms_log`(`userid`, `username`, `mobile`, `operator_id`, `SMS`, `created_on`,`status`, `mobile1`, `operator_id1`, `mobile2`, `operator_id2`) VALUES ('" + intUserID + "','" + strUserName + "','" + strMobileNumber + "','" + intOperaterID + "','" + strMessage + "','" + objDT.getCurDateTimeString() + "',0,'" + strMobile1 + "','" + strOperatorID1 + "','" + strMobile2 + "','" + strOperatorID2 + "')");
                //    }
                //    strResult = true;
                //}
                //catch (Exception)
                //{

                //}
            }
            finally
            {
                ds.Dispose();
            }
            return strResult;
        }


        public void WriteToSMSFile(int intUserID, int intCreatedbyType, string strMobileNumber, int intOperaterID, string strMessage, int intStatus, string strSMPPResposne,string strMobile1, string strOperatorID1, string strMobile2, string strOperatorID2)
        {
            
            try
            {
                string strName = "SMS_Log_" + (objDT.getCurDateTime_Dayanamic_format("dd_MM_yyyy")).ToString() + ".xml";
                string fileName = "NA";

                if (strMobileNumber.Length == 11)
                {
                    strMobileNumber = strMobileNumber.Substring(2, strMobileNumber.Length - 2);
                }
                if (strMobileNumber.Length == 10)
                {
                    strMobileNumber = strMobileNumber.Substring(1, strMobileNumber.Length - 1);
                }

                Regex objMobilePatternSalaam = new Regex(@"^74[\d]{7}$");
                Regex objMobilePatternEtisalat = new Regex(@"^(78|73)[\d]{7}$");
                Regex objMobilePatternRoshan = new Regex(@"^(79|72)[\d]{7}$");
                Regex objMobilePatternMTN = new Regex(@"^(77|76)[\d]{7}$");
                Regex objMobilePatternAWCC = new Regex(@"^(70|71)[\d]{7}$");
                if (objMobilePatternSalaam.IsMatch(strMobileNumber))
                {
                    intOperaterID = 1; // Salaam

                }
                else if (objMobilePatternEtisalat.IsMatch(strMobileNumber))
                {
                    intOperaterID = 2; // Etisalat
                }
                else if (objMobilePatternRoshan.IsMatch(strMobileNumber))
                {
                    intOperaterID = 3; // Roshan
                }
                else if (objMobilePatternMTN.IsMatch(strMobileNumber))
                {
                    intOperaterID = 4; // MTN
                }
                else if (objMobilePatternAWCC.IsMatch(strMobileNumber))
                {
                    intOperaterID = 5; // AWCC
                }

                if (intOperaterID == 3)
                {
                    fileName = @"C:\RoshanSMSLogsFile\" + strName;

                    if (File.Exists(fileName)) // Roshan
                    {
                        // File.Delete(fileName);

                        using (var tw = new StreamWriter(fileName, true))
                        {
                            try
                            {
                                tw.Dispose();
                                InsertXMLFileData_Roshan(fileName, intUserID.ToString(), intCreatedbyType.ToString(), strMobileNumber, intOperaterID, strMessage, intStatus, strSMPPResposne, strMobile1, strOperatorID1, strMobile2, strOperatorID2);
                            }
                            catch (Exception)
                            {
                                //try
                                //{
                                //    objODBCLOG.executeNonQuery("INSERT INTO `er_wallet_transfer_sms_log`(`userid`, `username`, `mobile`, `operator_id`, `SMS`, `created_on`,`status`) VALUES ('" + intUserID + "','','" + strMobileNumber + "','" + intOperaterID + "','" + strMessage + "','" + objDT.getCurDateTimeString() + "',0)");
                                //}
                                //catch (Exception)
                                //{

                                //}
                            }

                        }
                    }
                    else
                    {
                        // Create a new file   
                        using (FileStream fs = File.Create(fileName))
                        {
                            XmlWriter w = XmlWriter.Create(fs);

                            w.WriteStartDocument();
                            w.WriteStartElement("SMSData");
                            w.WriteEndDocument();
                            w.Flush();
                            fs.Close();

                            fs.Dispose();

                        }
                        using (var tw = new StreamWriter(fileName, true))
                        {
                            try
                            {
                                tw.Dispose();
                                InsertXMLFileData_Roshan(fileName, intUserID.ToString(), intCreatedbyType.ToString(), strMobileNumber, intOperaterID, strMessage, intStatus, strSMPPResposne, strMobile1, strOperatorID1, strMobile2, strOperatorID2);

                            }
                            catch (Exception)
                            {
                                //try
                                //{
                                //    objODBCLOG.executeNonQuery("INSERT INTO `er_wallet_transfer_sms_log`(`userid`, `username`, `mobile`, `operator_id`, `SMS`, `created_on`,`status`) VALUES ('" + intUserID + "','','" + strMobileNumber + "','" + intOperaterID + "','" + strMessage + "','" + objDT.getCurDateTimeString() + "',0)");
                                //}
                                //catch (Exception)
                                //{

                                //}
                            }

                        }
                    }

                }
                else if (intOperaterID == 2) // Etisalat
                {
                    fileName = @"C:\EtisalatSMSLogsFile\" + strName;

                    if (File.Exists(fileName))
                    {
                        // File.Delete(fileName);

                        using (var tw = new StreamWriter(fileName, true))
                        {
                            try
                            {
                                tw.Dispose();
                                InsertXMLFileData_Etisalat(fileName, intUserID.ToString(), intCreatedbyType.ToString(), strMobileNumber, intOperaterID, strMessage, intStatus, strSMPPResposne, strMobile1, strOperatorID1, strMobile2, strOperatorID2);
                            }
                            catch (Exception)
                            {
                                //try
                                //{
                                //    objODBCLOG.executeNonQuery("INSERT INTO `er_wallet_transfer_sms_log`(`userid`, `username`, `mobile`, `operator_id`, `SMS`, `created_on`,`status`) VALUES ('" + intUserID + "','','" + strMobileNumber + "','" + intOperaterID + "','" + strMessage + "','" + objDT.getCurDateTimeString() + "',0)");
                                //}
                                //catch (Exception)
                                //{

                                //}
                            }

                        }
                    }
                    else
                    {
                        // Create a new file   
                        using (FileStream fs = File.Create(fileName))
                        {
                            XmlWriter w = XmlWriter.Create(fs);

                            w.WriteStartDocument();
                            w.WriteStartElement("SMSData");
                            w.WriteEndDocument();
                            w.Flush();
                            fs.Close();

                            fs.Dispose();

                        }
                        using (var tw = new StreamWriter(fileName, true))
                        {
                            try
                            {
                                tw.Dispose();
                                InsertXMLFileData_Etisalat(fileName, intUserID.ToString(), intCreatedbyType.ToString(), strMobileNumber, intOperaterID, strMessage, intStatus, strSMPPResposne, strMobile1, strOperatorID1, strMobile2, strOperatorID2);

                            }
                            catch (Exception)
                            {
                                //try
                                //{
                                //    objODBCLOG.executeNonQuery("INSERT INTO `er_wallet_transfer_sms_log`(`userid`, `username`, `mobile`, `operator_id`, `SMS`, `created_on`,`status`) VALUES ('" + intUserID + "','','" + strMobileNumber + "','" + intOperaterID + "','" + strMessage + "','" + objDT.getCurDateTimeString() + "',0)");
                                //}
                                //catch (Exception)
                                //{

                                //}
                            }

                        }
                    }
                }
                else if (intOperaterID == 4)
                {
                    fileName = @"C:\MTNSMSLogsFile\" + strName;

                    if (File.Exists(fileName))
                    {
                        // File.Delete(fileName);

                        using (var tw = new StreamWriter(fileName, true))
                        {
                            try
                            {
                                tw.Dispose();
                                InsertXMLFileData_MTN(fileName, intUserID.ToString(), intCreatedbyType.ToString(), strMobileNumber, intOperaterID, strMessage, intStatus, strSMPPResposne, strMobile1, strOperatorID1, strMobile2, strOperatorID2);
                            }
                            catch (Exception)
                            {
                                //try
                                //{
                                //    objODBCLOG.executeNonQuery("INSERT INTO `er_wallet_transfer_sms_log`(`userid`, `username`, `mobile`, `operator_id`, `SMS`, `created_on`,`status`) VALUES ('" + intUserID + "','','" + strMobileNumber + "','" + intOperaterID + "','" + strMessage + "','" + objDT.getCurDateTimeString() + "',0)");
                                //}
                                //catch (Exception)
                                //{

                                //}
                            }

                        }
                    }
                    else
                    {
                        // Create a new file   
                        using (FileStream fs = File.Create(fileName))
                        {
                            XmlWriter w = XmlWriter.Create(fs);

                            w.WriteStartDocument();
                            w.WriteStartElement("SMSData");
                            w.WriteEndDocument();
                            w.Flush();
                            fs.Close();

                            fs.Dispose();

                        }
                        using (var tw = new StreamWriter(fileName, true))
                        {
                            try
                            {
                                tw.Dispose();
                                InsertXMLFileData_MTN(fileName, intUserID.ToString(), intCreatedbyType.ToString(), strMobileNumber, intOperaterID, strMessage, intStatus, strSMPPResposne, strMobile1, strOperatorID1, strMobile2, strOperatorID2);

                            }
                            catch (Exception)
                            {
                                //try
                                //{
                                //    objODBCLOG.executeNonQuery("INSERT INTO `er_wallet_transfer_sms_log`(`userid`, `username`, `mobile`, `operator_id`, `SMS`, `created_on`,`status`) VALUES ('" + intUserID + "','','" + strMobileNumber + "','" + intOperaterID + "','" + strMessage + "','" + objDT.getCurDateTimeString() + "',0)");
                                //}
                                //catch (Exception)
                                //{

                                //}
                            }

                        }
                    }
                } // MTN

                // Check if file already exists. If yes, delete it.   
               
            }
            catch (Exception)
            {
                
            }
           
        }
        public void InsertXMLFileData_Etisalat(string strFileName, string intUserID, string intCreatedbyType, string strMobileNumber, int intOperaterID, string strMessage, int intStatus, string strSMPPResposne, string strMobile1, string strOperatorID1, string strMobile2, string strOperatorID2)
        {
            try
            {
                XmlDocument xmlDoc = new System.Xml.XmlDocument();
                xmlDoc.Load(strFileName.ToString());

                XmlNodeList nodeList = xmlDoc.SelectNodes("SMSData/SMS");
                int TID = Convert.ToInt32(nodeList.Count);

                if (TID == 0)
                {
                    TID = 1;
                }
                else
                {
                    TID = TID + 1;
                }



                XmlElement parentElement = xmlDoc.CreateElement("SMS");

                XmlElement id = xmlDoc.CreateElement("id");
                id.InnerText = TID.ToString();

                XmlElement userid = xmlDoc.CreateElement("userid");
                userid.InnerText = intUserID.ToString();

                XmlElement usertype = xmlDoc.CreateElement("usertype");
                usertype.InnerText = intCreatedbyType.ToString();

                XmlElement mobile = xmlDoc.CreateElement("mobile");
                mobile.InnerText = strMobileNumber;

                XmlElement operater_id = xmlDoc.CreateElement("operater_id");
                operater_id.InnerText = intOperaterID.ToString();

                XmlElement message = xmlDoc.CreateElement("message");
                message.InnerText = strMessage.ToString();

                XmlElement status = xmlDoc.CreateElement("status");
                status.InnerText = "Pending";

                XmlElement created_on = xmlDoc.CreateElement("created_on");
                created_on.InnerText = objDT.getCurDateTimeString();


                XmlElement smpp_respose = xmlDoc.CreateElement("smpp_respose");
                smpp_respose.InnerText = "NA";

                XmlElement res_message_id = xmlDoc.CreateElement("res_message_id");
                res_message_id.InnerText = "NA";


                XmlElement res_phonenumber = xmlDoc.CreateElement("res_phonenumber");
                res_phonenumber.InnerText = "NA";


                XmlElement final_status = xmlDoc.CreateElement("final_status");
                final_status.InnerText = "NA";


                XmlElement delaverydatetime = xmlDoc.CreateElement("delaverydatetime");
                delaverydatetime.InnerText = "NA";

                XmlElement attempt = xmlDoc.CreateElement("attempt");
                attempt.InnerText = "0";

                XmlElement mobile1 = xmlDoc.CreateElement("mobile1");
                mobile1.InnerText = strMobile1.ToString();

                XmlElement operatorID1 = xmlDoc.CreateElement("operatorID1");
                operatorID1.InnerText = strOperatorID1.ToString();

                XmlElement mobile2 = xmlDoc.CreateElement("mobile2");
                mobile2.InnerText = strMobile2.ToString();

                XmlElement operatorID2 = xmlDoc.CreateElement("operatorID2");
                operatorID2.InnerText = strOperatorID2.ToString();

                parentElement.AppendChild(id);
                parentElement.AppendChild(userid);
                parentElement.AppendChild(usertype);
                parentElement.AppendChild(operater_id);
                parentElement.AppendChild(mobile);
                parentElement.AppendChild(message);
                parentElement.AppendChild(status);
                parentElement.AppendChild(created_on);
                parentElement.AppendChild(smpp_respose);
                parentElement.AppendChild(res_message_id);
                parentElement.AppendChild(res_phonenumber);
                parentElement.AppendChild(final_status);
                parentElement.AppendChild(delaverydatetime);
                parentElement.AppendChild(attempt);
                parentElement.AppendChild(mobile1);
                parentElement.AppendChild(operatorID1);
                parentElement.AppendChild(mobile2);
                parentElement.AppendChild(operatorID2);

                xmlDoc.DocumentElement.AppendChild(parentElement);
                xmlDoc.Save(strFileName);
            }
            catch (Exception)
            {
                try
                {
                  //  objODBCLOG.executeNonQuery("INSERT INTO `er_wallet_transfer_sms_log`(`userid`, `username`, `mobile`, `operator_id`, `SMS`, `created_on`,`status`) VALUES ('" + intUserID + "','','" + strMobileNumber + "','" + intOperaterID + "','" + strMessage + "','" + objDT.getCurDateTimeString() + "',0)");
                }
                catch (Exception)
                {

                }
            }

        }

        public void InsertXMLFileData_Roshan(string strFileName, string intUserID, string intCreatedbyType, string strMobileNumber, int intOperaterID, string strMessage, int intStatus, string strSMPPResposne, string strMobile1, string strOperatorID1, string strMobile2, string strOperatorID2)
        {
            try
            {
                XmlDocument xmlDoc = new System.Xml.XmlDocument();
                xmlDoc.Load(strFileName.ToString());

                XmlNodeList nodeList = xmlDoc.SelectNodes("SMSData/SMS");
                int TID = Convert.ToInt32(nodeList.Count);

                if (TID == 0)
                {
                    TID = 1;
                }
                else
                {
                    TID = TID + 1;
                }



                XmlElement parentElement = xmlDoc.CreateElement("SMS");

                XmlElement id = xmlDoc.CreateElement("id");
                id.InnerText = TID.ToString();

                XmlElement userid = xmlDoc.CreateElement("userid");
                userid.InnerText = intUserID.ToString();

                XmlElement usertype = xmlDoc.CreateElement("usertype");
                usertype.InnerText = intCreatedbyType.ToString();

                XmlElement mobile = xmlDoc.CreateElement("mobile");
                mobile.InnerText = strMobileNumber;

                XmlElement operater_id = xmlDoc.CreateElement("operater_id");
                operater_id.InnerText = intOperaterID.ToString();

                XmlElement message = xmlDoc.CreateElement("message");
                message.InnerText = strMessage.ToString();

                XmlElement status = xmlDoc.CreateElement("status");
                status.InnerText = "Pending";

                XmlElement created_on = xmlDoc.CreateElement("created_on");
                created_on.InnerText = objDT.getCurDateTimeString();


                XmlElement smpp_respose = xmlDoc.CreateElement("smpp_respose");
                smpp_respose.InnerText = "NA";

                XmlElement res_message_id = xmlDoc.CreateElement("res_message_id");
                res_message_id.InnerText = "NA";


                XmlElement res_phonenumber = xmlDoc.CreateElement("res_phonenumber");
                res_phonenumber.InnerText = "NA";


                XmlElement final_status = xmlDoc.CreateElement("final_status");
                final_status.InnerText = "NA";


                XmlElement delaverydatetime = xmlDoc.CreateElement("delaverydatetime");
                delaverydatetime.InnerText = "NA";

                XmlElement attempt = xmlDoc.CreateElement("attempt");
                attempt.InnerText = "0";

                XmlElement mobile1 = xmlDoc.CreateElement("mobile1");
                mobile1.InnerText = strMobile1.ToString();

                XmlElement operatorID1 = xmlDoc.CreateElement("operatorID1");
                operatorID1.InnerText = strOperatorID1.ToString();

                XmlElement mobile2 = xmlDoc.CreateElement("mobile2");
                mobile2.InnerText = strMobile2.ToString();

                XmlElement operatorID2 = xmlDoc.CreateElement("operatorID2");
                operatorID2.InnerText = strOperatorID2.ToString();

                parentElement.AppendChild(id);
                parentElement.AppendChild(userid);
                parentElement.AppendChild(usertype);
                parentElement.AppendChild(operater_id);
                parentElement.AppendChild(mobile);
                parentElement.AppendChild(message);
                parentElement.AppendChild(status);
                parentElement.AppendChild(created_on);
                parentElement.AppendChild(smpp_respose);
                parentElement.AppendChild(res_message_id);
                parentElement.AppendChild(res_phonenumber);
                parentElement.AppendChild(final_status);
                parentElement.AppendChild(delaverydatetime);
                parentElement.AppendChild(attempt);
                parentElement.AppendChild(mobile1);
                parentElement.AppendChild(operatorID1);
                parentElement.AppendChild(mobile2);
                parentElement.AppendChild(operatorID2);

                xmlDoc.DocumentElement.AppendChild(parentElement);
                xmlDoc.Save(strFileName);
            }
            catch (Exception)
            {
                try
                {
                 //   objODBCLOG.executeNonQuery("INSERT INTO `er_wallet_transfer_sms_log`(`userid`, `username`, `mobile`, `operator_id`, `SMS`, `created_on`,`status`) VALUES ('" + intUserID + "','','" + strMobileNumber + "','" + intOperaterID + "','" + strMessage + "','" + objDT.getCurDateTimeString() + "',0)");
                }
                catch (Exception)
                {

                }
            }

        }

        public void InsertXMLFileData_MTN(string strFileName, string intUserID, string intCreatedbyType, string strMobileNumber, int intOperaterID, string strMessage, int intStatus, string strSMPPResposne, string strMobile1, string strOperatorID1, string strMobile2, string strOperatorID2)
        {
            try
            {
                XmlDocument xmlDoc = new System.Xml.XmlDocument();
                xmlDoc.Load(strFileName.ToString());

                XmlNodeList nodeList = xmlDoc.SelectNodes("SMSData/SMS");
                int TID = Convert.ToInt32(nodeList.Count);

                if (TID == 0)
                {
                    TID = 1;
                }
                else
                {
                    TID = TID + 1;
                }



                XmlElement parentElement = xmlDoc.CreateElement("SMS");

                XmlElement id = xmlDoc.CreateElement("id");
                id.InnerText = TID.ToString();

                XmlElement userid = xmlDoc.CreateElement("userid");
                userid.InnerText = intUserID.ToString();

                XmlElement usertype = xmlDoc.CreateElement("usertype");
                usertype.InnerText = intCreatedbyType.ToString();

                XmlElement mobile = xmlDoc.CreateElement("mobile");
                mobile.InnerText = strMobileNumber;

                XmlElement operater_id = xmlDoc.CreateElement("operater_id");
                operater_id.InnerText = intOperaterID.ToString();

                XmlElement message = xmlDoc.CreateElement("message");
                message.InnerText = strMessage.ToString();

                XmlElement status = xmlDoc.CreateElement("status");
                status.InnerText = "Pending";

                XmlElement created_on = xmlDoc.CreateElement("created_on");
                created_on.InnerText = objDT.getCurDateTimeString();


                XmlElement smpp_respose = xmlDoc.CreateElement("smpp_respose");
                smpp_respose.InnerText = "NA";

                XmlElement res_message_id = xmlDoc.CreateElement("res_message_id");
                res_message_id.InnerText = "NA";


                XmlElement res_phonenumber = xmlDoc.CreateElement("res_phonenumber");
                res_phonenumber.InnerText = "NA";


                XmlElement final_status = xmlDoc.CreateElement("final_status");
                final_status.InnerText = "NA";


                XmlElement delaverydatetime = xmlDoc.CreateElement("delaverydatetime");
                delaverydatetime.InnerText = "NA";

                XmlElement attempt = xmlDoc.CreateElement("attempt");
                attempt.InnerText = "0";

                XmlElement mobile1 = xmlDoc.CreateElement("mobile1");
                mobile1.InnerText = strMobile1.ToString();

                XmlElement operatorID1 = xmlDoc.CreateElement("operatorID1");
                operatorID1.InnerText = strOperatorID1.ToString();

                XmlElement mobile2 = xmlDoc.CreateElement("mobile2");
                mobile2.InnerText = strMobile2.ToString();

                XmlElement operatorID2 = xmlDoc.CreateElement("operatorID2");
                operatorID2.InnerText = strOperatorID2.ToString();

                parentElement.AppendChild(id);
                parentElement.AppendChild(userid);
                parentElement.AppendChild(usertype);
                parentElement.AppendChild(operater_id);
                parentElement.AppendChild(mobile);
                parentElement.AppendChild(message);
                parentElement.AppendChild(status);
                parentElement.AppendChild(created_on);
                parentElement.AppendChild(smpp_respose);
                parentElement.AppendChild(res_message_id);
                parentElement.AppendChild(res_phonenumber);
                parentElement.AppendChild(final_status);
                parentElement.AppendChild(delaverydatetime);
                parentElement.AppendChild(attempt);
                parentElement.AppendChild(mobile1);
                parentElement.AppendChild(operatorID1);
                parentElement.AppendChild(mobile2);
                parentElement.AppendChild(operatorID2);

                xmlDoc.DocumentElement.AppendChild(parentElement);
                xmlDoc.Save(strFileName);                
            }
            catch (Exception)
            {
                try
                {
                  //  objODBCLOG.executeNonQuery("INSERT INTO `er_wallet_transfer_sms_log`(`userid`, `username`, `mobile`, `operator_id`, `SMS`, `created_on`,`status`) VALUES ('" + intUserID + "','','" + strMobileNumber + "','" + intOperaterID + "','" + strMessage + "','" + objDT.getCurDateTimeString() + "',0)");
                }
                catch (Exception)
                {

                }
            }

        }

        public void UpdateXML()
        {
            DataSet ds = new DataSet();
            string strName = "SMS_Log_" + (objDT.getCurDateTime_Dayanamic_format("dd_MM_yyyy")).ToString() + ".xml";
            string fileName = @"C:\SMSLogsFile\" + strName;

            // Check if file already exists. If yes, delete it.   
            if (File.Exists(fileName))
            {
                ds.ReadXml(fileName);
                int intCount = 50;
                if (ds.Tables[0].Rows.Count < 50)
                {
                    intCount = ds.Tables[0].Rows.Count;
                }
                for (int i = 0; i < intCount; i++)
                {
                    string strMID = ds.Tables[0].Rows[i]["status"].ToString();
                    if (strMID == "Pending")
                    {
                        string strID = ds.Tables[0].Rows[i]["ID"].ToString();
                        ds.Tables[0].Rows[i]["smpp_respose"] = "m changed";
                        ds.Tables[0].Rows[i]["status"] = "Success";
                        ds.Tables[0].Rows[i]["attempt"] = int.Parse(ds.Tables[0].Rows[i]["attempt"].ToString()) + 1;
                        ds.Tables[0].Rows[i]["res_message_id"] = "m changed";
                        ds.Tables[0].Rows[i]["res_phonenumber"] = "m changed";
                        ds.Tables[0].Rows[i]["final_status"] = "m changed";
                        ds.Tables[0].Rows[i]["delaverydatetime"] = DateTime.Now.ToString();
                    }
                }
                ds.WriteXml(fileName);
            }
            else
            {

            }
        }

        public string CheckRegixMobile(int intOperaterID, string strMobile)
        {
            string strReturn = "";
            if (strMobile != "0")
            {
                try
                {
                    switch (intOperaterID)
                    {
                        case 1: // Salam
                            {
                                Regex objMobilePattern = new Regex(@"^74[\d]{7}$");
                                if (objMobilePattern.IsMatch(strMobile))
                                {
                                    strReturn = "TRUE";
                                    break;
                                }
                                else
                                {
                                    strReturn = "FALSE";
                                    break;
                                }
                            }

                        case 2: // Etisalat
                            {
                                Regex objMobilePattern = new Regex(@"^(78|73)[\d]{7}$");
                                if (objMobilePattern.IsMatch(strMobile))
                                {
                                    strReturn = "TRUE";
                                    break;
                                }
                                else
                                {
                                    strReturn = "FALSE";
                                    break;
                                }
                            }
                        case 3: // Roshan
                            {

                                Regex objMobilePattern = new Regex(@"^(79|72)[\d]{7}$");
                                if (objMobilePattern.IsMatch(strMobile))
                                {
                                    strReturn = "TRUE";
                                    break;
                                }
                                else
                                {
                                    strReturn = "FALSE";
                                    break;
                                }

                            }
                        case 4: // MTN
                            {

                                Regex objMobilePattern = new Regex(@"^(77|76)[\d]{7}$");
                                if (objMobilePattern.IsMatch(strMobile))
                                {
                                    strReturn = "TRUE";
                                    break;
                                }
                                else
                                {
                                    strReturn = "FALSE";
                                    break;
                                }

                            }
                        case 5: //AWCC
                            {

                                Regex objMobilePattern = new Regex(@"^(70|71)[\d]{7}$");
                                if (objMobilePattern.IsMatch(strMobile))
                                {
                                    strReturn = "TRUE";
                                    break;
                                }
                                else
                                {
                                    strReturn = "FALSE";
                                    break;
                                }

                            }
                        default:
                            {

                                strReturn = "FALSE";
                                break;

                            }
                    }
                }
                catch (Exception)
                {

                }
            }
            else
            {
                strReturn = "TRUE";
            }
            return strReturn;
        }

        // strExpireTime="00:10:00" 10 Min Expire time
        public string er_getOTP(int intUserID, string strMobileNumber, int intCreatedbyType, string strExpireTime)
        {
            string strOTPData = "NA";
            try
            {
                strOTPData = objODBC.executeScalar_str("call `er_Get_Otp`('" + intUserID + "','" + strMobileNumber + "','" + intCreatedbyType + "','" + strExpireTime + "')");
                if (strOTPData.Length == 5)
                {
                    string strOldOTP = strOTPData;
                    strOTPData = strOTPData + GenrateNumber(1);
                    objODBC.executeNonQuery("update ico_otp_password set OTP_code='" + strOTPData + "' where OTP_code='" + strOldOTP + "' and mobile='" + strMobileNumber + "' and status=0");
                }
                if (strOTPData.Length == 7)
                {
                    string strOldOTP = strOTPData;
                    strOTPData = strOTPData.Remove(strOTPData.Length - 1);
                    objODBC.executeNonQuery("update ico_otp_password set OTP_code='" + strOTPData + "' where OTP_code='" + strOldOTP + "' and mobile='" + strMobileNumber + "' and status=0");
                }
                if (strOTPData.Length == 4)
                {
                    string strOldOTP = strOTPData;
                    strOTPData = strOTPData + GenrateNumber(2);
                    objODBC.executeNonQuery("update ico_otp_password set OTP_code='" + strOTPData + "' where OTP_code='" + strOldOTP + "' and mobile='" + strMobileNumber + "' and status=0");
                }
                if (strOTPData.Length == 3)
                {
                    string strOldOTP = strOTPData;
                    strOTPData = strOTPData + GenrateNumber(3);
                    objODBC.executeNonQuery("update ico_otp_password set OTP_code='" + strOTPData + "' where OTP_code='" + strOldOTP + "' and mobile='" + strMobileNumber + "' and status=0");
                }
                if (strOTPData.Length == 8)
                {
                    string strOldOTP = strOTPData;
                    strOTPData = strOTPData.Remove(strOTPData.Length - 2);
                    objODBC.executeNonQuery("update ico_otp_password set OTP_code='" + strOTPData + "' where OTP_code='" + strOldOTP + "' and mobile='" + strMobileNumber + "' and status=0");
                }

            }
            catch (Exception)
            {

            }
            return strOTPData;
        }
        public ArrayList getReport(string strQuery)
        {
            ODBC obj = new ODBC();
            ArrayList arrUserParams = new ArrayList();
            try
            {
                System.Data.DataSet ds = new System.Data.DataSet();
                try
                {
                    ds = obj.getDataSet(strQuery);

                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        arrUserParams.Add("TRUE");

                        System.Data.DataTable firstTable = ds.Tables[0];
                        // arrUserParams.Add(firstTable.Rows.Count);
                        //  arrUserParams.Add(firstTable.Columns.Count);
                        for (int i = 0; i <= firstTable.Rows.Count - 1; i++)
                        {
                            for (int j = 0; j <= firstTable.Columns.Count - 1; j++)
                                arrUserParams.Add(firstTable.Rows[i][j].ToString());
                        }
                    }
                    else
                        arrUserParams.Add("FALSE");

                    return arrUserParams;
                }
                catch (Exception ex)
                {
                    arrUserParams.Add(ex.Message.ToString());

                    return arrUserParams;
                }

                finally
                {
                    ds.Dispose();
                }
            }


            catch (Exception ex)
            {
                arrUserParams.Add(ex.Message.ToString());

                return arrUserParams;
            }
        }


        public string er_ExpireOTP(int intUserID, string strMobileNumber, string strOTP, int intCreatedByType)
        {

            string strOTPData = "NA";
            DataSet ds = new DataSet();
            try
            {

                ds = objODBC.getDataSet("call er_expireOTP('" + intUserID + "','" + strOTP + "','" + intCreatedByType + "','" + strMobileNumber + "')");
                if (ds.Tables[0].Rows.Count > 0)
                {
                    string strFlag = ds.Tables[0].Rows[0][0].ToString();
                    string strResponse = ds.Tables[0].Rows[0][1].ToString();
                    if (strFlag == "1")
                    {
                        strOTPData = "TRUE,1," + strResponse;

                    }
                    else
                    {
                        strOTPData = "FALSE,2," + strResponse;
                    }
                }
            }
            catch (Exception)
            {

                strOTPData = "FALSE,2,Something went wrong !";

            }
            finally
            {
                ds.Dispose();
            }
            return strOTPData;
        }


        public string DataTableToGetCategory(string strQuery, string strMethodName, int intFlag, string strErrorMsg)
        {
            ODBC objodbc = new ODBC();
            DataTable table = new DataTable();
            string newJson = string.Empty;
            var NewJSONString = new StringBuilder();
            if (intFlag == 1)
            {
                table = objodbc.getDataTable(strQuery);
                string JSONString = string.Empty;
                JSONString = JsonConvert.SerializeObject(table);
                JSONString = JSONString.Remove(0, 1);
                string ssJSONString = JSONString.Remove(JSONString.Length - 1, 1);
                NewJSONString.Append("],");
                NewJSONString.Append("\"error\":{" + "\"code\":" + "\"" + intFlag + "\",");
                NewJSONString.Append("\"status\" :" + "\"" + intFlag + "\",");
                NewJSONString.Append("\"message\" :" + "\"" + strErrorMsg + "\"");
                NewJSONString.Append("}");
                newJson = "{" + @"""" + strMethodName + @"""" + ":[" + ssJSONString + NewJSONString + "}";
            }
            else if (intFlag == 0)
            {
                NewJSONString.Append("{" + "\"" + strMethodName + "\":[{\"result\":\"No Records Found\"}]" + ",");
                NewJSONString.Append("\"error\":{" + "\"code\":" + "\"" + intFlag + "\",");
                NewJSONString.Append("\"status\" :" + "\"" + intFlag + "\",");
                NewJSONString.Append("\"message\" :" + "\"" + strErrorMsg + "\"");
                NewJSONString.Append("}}");
                newJson = NewJSONString.ToString();
            }
            return newJson;
        }

    }
}