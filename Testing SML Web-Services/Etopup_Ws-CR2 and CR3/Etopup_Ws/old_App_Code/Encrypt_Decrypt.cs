﻿using System;
using System.IO;
using System.Security.Cryptography;
using System.Text;
using System.Data;
using System.Configuration;
using System.Collections;
using Etopup_Ws.old_App_Code;
namespace Etopup_Ws.old_App_Code
{
    public class Encrypt_Decrypt
    {
        ODBC objODBC = new ODBC(); DataTable dt = new DataTable();
        Encryption objEncrypt = new Encryption();
        /*  For Client side  */
        // intType 1-User having Userid,2-guest user send unique id like android id 
        public string Encrypt_client(string clearText)
        {
            try
            {
                if (!String.IsNullOrEmpty(clearText))
                {
                    byte[] clearBytes = Encoding.ASCII.GetBytes(clearText);
                    using (Aes encryptor = Aes.Create())
                    {
                        Rfc2898DeriveBytes pdb = new Rfc2898DeriveBytes(ConfigurationSettings.AppSettings["Encrypt_key"].ToString(), Encoding.UTF8.GetBytes(ConfigurationSettings.AppSettings["salt"].ToString()));
                        encryptor.Key = pdb.GetBytes(32);
                        encryptor.IV = pdb.GetBytes(16);
                        using (MemoryStream ms = new MemoryStream())
                        {
                            using (CryptoStream cs = new CryptoStream(ms, encryptor.CreateEncryptor(), CryptoStreamMode.Write))
                            {
                                cs.Write(clearBytes, 0, clearBytes.Length);
                                cs.Close();
                            }
                            clearText = Convert.ToBase64String(ms.ToArray());
                        }
                    }
                }
                return clearText;
            }
            catch (Exception ex) { return "Something went wrong."; }
            finally
            {
                dt.Dispose();
            }

        }
        public string Decrypt1(string cipherText)
        {
            try
            {
                if (!String.IsNullOrEmpty(cipherText))
                {

                    byte[] cipherBytes = Convert.FromBase64String(cipherText);

                    using (Aes encryptor = Aes.Create())
                    {
                        Rfc2898DeriveBytes pdb = new Rfc2898DeriveBytes(ConfigurationSettings.AppSettings["Encrypt_key"].ToString(), Encoding.UTF8.GetBytes(ConfigurationSettings.AppSettings["salt"].ToString()));
                        encryptor.Key = pdb.GetBytes(32);
                        encryptor.IV = pdb.GetBytes(16);
                        using (MemoryStream ms = new MemoryStream())
                        {
                            using (CryptoStream cs = new CryptoStream(ms, encryptor.CreateDecryptor(), CryptoStreamMode.Write))
                            {
                                cs.Write(cipherBytes, 0, cipherBytes.Length);
                                cs.Close();
                            }
                            cipherText = Encoding.ASCII.GetString(ms.ToArray());
                        }
                    }
                }
                return cipherText;
            }
            catch (Exception ex) { return "Something went wrong."; }
            finally
            {
                dt.Dispose();
            }

        }


        /*  For server side  */
        // intUsertype 1-Admin,2-Member and 0:Other send unique id like android id  intType= 1-user 2- guest, intsource - 1-web,2-android,3-IOS
        public string Encrypt(string strUserName, string clearText, int intUsertype, int intType, int intsource)
        {
            string Encrypt_key = "", salt = "";
            int strUserID = 0;
            try
            {

                if (intType == 1)
                {
                    if (intUsertype == 1)
                    {
                        strUserID = objODBC.executeScalar_int("select userid from er_login_admin where username='" + strUserName + "' or emailid='" + strUserName +"' ");
                        if (strUserID > 0)
                        {
                            if (intsource == 1)
                            {
                                dt = objODBC.getDataTable("select encryption_key,salt1,pass_key from er_login_admin where userid=" + strUserID);
                            }
                            if (intsource == 2)
                            {
                                dt = objODBC.getDataTable("select encryption_key,salt2,pass_key from er_login_admin where userid=" + strUserID);
                            }
                            if (intsource == 3)
                            {
                                dt = objODBC.getDataTable("select encryption_key,salt3,pass_key from er_login_admin where userid=" + strUserID);
                            }

                            if (dt.Rows.Count > 0)
                            {
                                  Encrypt_key = objEncrypt.DecryptQueryString(dt.Rows[0][0].ToString(), dt.Rows[0][2].ToString());
                                 salt = objEncrypt.DecrypSalt(dt.Rows[0][1].ToString(), dt.Rows[0][2].ToString());

                              //  Encrypt_key = dt.Rows[0][0].ToString();
                              //  salt = dt.Rows[0][1].ToString();
                            }
                        }
                        else
                        {
                            Encrypt_key = "";
                            salt = "";
                        }
                    }
                    else
                    {
                        strUserID = objODBC.executeScalar_int("select userid from er_login where username='" + strUserName + "' or mobile='" + strUserName + "' ");
                        if (strUserID > 0)
                        {
                            if (intsource == 1)
                            {
                                dt = objODBC.getDataTable("select encryption_key,salt1,pass_key from er_login where userid=" + strUserID);
                            }
                            if (intsource == 2)
                            {
                                dt = objODBC.getDataTable("select encryption_key,salt2,pass_key from er_login where userid=" + strUserID);
                            }
                            if (intsource == 3)
                            {
                                dt = objODBC.getDataTable("select encryption_key,salt3,pass_key from er_login where userid=" + strUserID);
                            }

                            if (dt.Rows.Count > 0)
                            {
                                  Encrypt_key = objEncrypt.DecryptQueryString(dt.Rows[0][0].ToString(), dt.Rows[0][2].ToString());
                                salt = objEncrypt.DecrypSalt(dt.Rows[0][1].ToString(), dt.Rows[0][2].ToString());

                               // Encrypt_key = dt.Rows[0][0].ToString();
                               // salt = dt.Rows[0][1].ToString();

                            }
                        }
                        else
                        {
                            Encrypt_key = "";
                            salt = "";
                        }
                    }

                }
                if (intType == 2)
                {

                    dt = objODBC.getDataTable("select encryptionkey,salt,pass_key from er_guest_encrptionkey where id=1");
                    if (dt.Rows.Count > 0)
                    {
                         Encrypt_key = objEncrypt.DecryptQueryString(dt.Rows[0][0].ToString(), dt.Rows[0][2].ToString());
                         salt = objEncrypt.DecrypSalt(dt.Rows[0][1].ToString(), dt.Rows[0][2].ToString());


                     //  Encrypt_key = dt.Rows[0][0].ToString();
                      //  salt = dt.Rows[0][1].ToString();
                    }
                }
                if (dt.Rows.Count > 0)
                {
                    byte[] clearBytes = Encoding.ASCII.GetBytes(clearText);
                    using (Aes encryptor = Aes.Create())
                    {
                        Rfc2898DeriveBytes pdb = new Rfc2898DeriveBytes(Encrypt_key, Encoding.UTF8.GetBytes(salt));
                        encryptor.Key = pdb.GetBytes(32);
                        encryptor.IV = pdb.GetBytes(16);
                        using (MemoryStream ms = new MemoryStream())
                        {
                            using (CryptoStream cs = new CryptoStream(ms, encryptor.CreateEncryptor(), CryptoStreamMode.Write))
                            {
                                cs.Write(clearBytes, 0, clearBytes.Length);
                                cs.Close();
                            }
                            clearText = Convert.ToBase64String(ms.ToArray());
                        }
                    }
                }
                return clearText;
            }
            catch (Exception ex) { return "Something went wrong."; }
            finally
            {
                dt.Dispose();
            }

        }

        // intUsertype 1-Admin,2-Member and 0:Other send unique id like android id  intType= 1-user 2- guest
        public string Decrypt(string strUserName, string cipherText, int intUsertype, int intType, int intsource)
        {
            string Encrypt_key = "", salt = "";
            int strUserID = 0;
            try
            {
                if (intType == 1)
                {
                    if (intUsertype == 1) 
                    {
                        strUserID = objODBC.executeScalar_int("select userid from er_login_admin where username='" + strUserName + "' or emailid='" + strUserName + "' ");
                        if (strUserID > 0)
                        {
                            if (intsource == 1)
                            {
                                dt = objODBC.getDataTable("select encryption_key,salt1,pass_key from er_login_admin where userid=" + strUserID);
                            }
                            if (intsource == 2)
                            {
                                dt = objODBC.getDataTable("select encryption_key,salt2,pass_key from er_login_admin where userid=" + strUserID);
                            }
                            if (intsource == 3)
                            {
                                dt = objODBC.getDataTable("select encryption_key,salt3,pass_key from er_login_admin where userid=" + strUserID);
                            }

                            if (dt.Rows.Count > 0)
                            {
                                Encrypt_key = objEncrypt.DecryptQueryString(dt.Rows[0][0].ToString(), dt.Rows[0][2].ToString());
                                salt = objEncrypt.DecrypSalt(dt.Rows[0][1].ToString(), dt.Rows[0][2].ToString());


                                //Encrypt_key = dt.Rows[0][0].ToString();
                                //salt = dt.Rows[0][1].ToString();
                            }
                        }
                        else
                        {
                            Encrypt_key = "";
                            salt = "";
                        }
                    }
                    else if (intUsertype == 2)
                    {
                        strUserID = objODBC.executeScalar_int("select userid from er_login where username='" + strUserName + "' or mobile='" + strUserName + "' ");
                        if (strUserID > 0)
                        {

                            if (intsource == 1)
                            {
                                dt = objODBC.getDataTable("select encryption_key,salt1,pass_key from er_login where userid=" + strUserID);
                            }
                            if (intsource == 2)
                            {
                                dt = objODBC.getDataTable("select encryption_key,salt2,pass_key from er_login where userid=" + strUserID);
                            }
                            if (intsource == 3)
                            {
                                dt = objODBC.getDataTable("select encryption_key,salt3,pass_key from er_login where userid=" + strUserID);
                            }

                            if (dt.Rows.Count > 0)
                            {
                                Encrypt_key = objEncrypt.DecryptQueryString(dt.Rows[0][0].ToString(), dt.Rows[0][2].ToString());
                                salt = objEncrypt.DecrypSalt(dt.Rows[0][1].ToString(), dt.Rows[0][2].ToString());


                                //Encrypt_key = dt.Rows[0][0].ToString();
                                //salt = dt.Rows[0][1].ToString();
                            }
                        }
                        else
                        {
                            Encrypt_key = "";
                            salt = "";
                        }
                    }
                }
                if (intType == 2)
                {
                    dt = objODBC.getDataTable("select encryptionkey,salt,pass_key from er_guest_encrptionkey where id=1");
                    if (dt.Rows.Count > 0)
                    {
                        Encrypt_key = objEncrypt.DecryptQueryString(dt.Rows[0][0].ToString(), dt.Rows[0][2].ToString());
                        salt = objEncrypt.DecrypSalt(dt.Rows[0][1].ToString(), dt.Rows[0][2].ToString());
                        //"IaOb23PiLdopKh6ybbv" "0x49, 0x76, 0x61, 0x6e, 0x20, 0x4d, 0x65, 0x64, 0x76, 0x65, 0x64, 0x65, 0x76"

                        //Encrypt_key = dt.Rows[0][0].ToString();
                        //salt = dt.Rows[0][1].ToString();
                    }
                }
                if (dt.Rows.Count > 0)
                {

                    byte[] cipherBytes = Convert.FromBase64String(cipherText);

                    using (Aes encryptor = Aes.Create())
                    {
                        Rfc2898DeriveBytes pdb = new Rfc2898DeriveBytes(Encrypt_key, Encoding.UTF8.GetBytes(salt));
                        encryptor.Key = pdb.GetBytes(32);
                        encryptor.IV = pdb.GetBytes(16);
                        using (MemoryStream ms = new MemoryStream())
                        {
                            using (CryptoStream cs = new CryptoStream(ms, encryptor.CreateDecryptor(), CryptoStreamMode.Write))
                            {
                                cs.Write(cipherBytes, 0, cipherBytes.Length);
                                //cs.Close();
                            }
                            //"Retailer,123456,101.85.196.251,2,02:00:00:00:00:00,26,358240051111110,0,1.0,2"
                            cipherText = Encoding.ASCII.GetString(ms.ToArray());
                        }
                    }
                }
                return cipherText;
            }
            catch (Exception ex) { return "Something went wrong."; }
            finally
            {
                dt.Dispose();
            }

        }


        /*  For server side  */
        // intUsertype 1-Admin,2-Member and 0:Other send unique id like android id  intType= 1-user 2- guest, intsource - 1-web,2-android,3-IOS
        public string EncryptAdmin(string strUserName, string clearText, int intUsertype, int intType, int intsource)
        {
            string Encrypt_key = "", salt = "";
            int strUserID = 0;
            try
            {

                if (intType == 1)
                {
                   
                        strUserID = objODBC.executeScalar_int("select userid from er_login_admin where username='" + strUserName + "' or emailid='" + strUserName + "' ");
                        if (strUserID > 0)
                        {
                            if (intsource == 1)
                            {
                                dt = objODBC.getDataTable("select encryption_key,salt1,pass_key from er_login_admin where userid=" + strUserID);
                            }
                            if (intsource == 2)
                            {
                                dt = objODBC.getDataTable("select encryption_key,salt2,pass_key from er_login_admin where userid=" + strUserID);
                            }
                            if (intsource == 3)
                            {
                                dt = objODBC.getDataTable("select encryption_key,salt3,pass_key from er_login_admin where userid=" + strUserID);
                            }

                            if (dt.Rows.Count > 0)
                            {
                            Encrypt_key = objEncrypt.DecryptQueryString(dt.Rows[0][0].ToString(), dt.Rows[0][2].ToString());
                            salt = objEncrypt.DecrypSalt(dt.Rows[0][1].ToString(), dt.Rows[0][2].ToString());

                            //Encrypt_key = dt.Rows[0][0].ToString();
                            //salt = dt.Rows[0][1].ToString();
                        }
                        }
                        else
                        {
                            Encrypt_key = "";
                            salt = "";
                        }
                    
                  

                }
                if (intType == 2)
                {

                    dt = objODBC.getDataTable("select encryptionkey,salt,pass_key from er_guest_encrptionkey where id=1");
                    if (dt.Rows.Count > 0)
                    {
                        Encrypt_key = objEncrypt.DecryptQueryString(dt.Rows[0][0].ToString(), dt.Rows[0][2].ToString());
                        salt = objEncrypt.DecrypSalt(dt.Rows[0][1].ToString(), dt.Rows[0][2].ToString());


                        //Encrypt_key = dt.Rows[0][0].ToString();
                        //salt = dt.Rows[0][1].ToString();
                    }
                }
                if (dt.Rows.Count > 0)
                {
                    byte[] clearBytes = Encoding.ASCII.GetBytes(clearText);
                    using (Aes encryptor = Aes.Create())
                    {
                        Rfc2898DeriveBytes pdb = new Rfc2898DeriveBytes(Encrypt_key, Encoding.UTF8.GetBytes(salt));
                        encryptor.Key = pdb.GetBytes(32);
                        encryptor.IV = pdb.GetBytes(16);
                        using (MemoryStream ms = new MemoryStream())
                        {
                            using (CryptoStream cs = new CryptoStream(ms, encryptor.CreateEncryptor(), CryptoStreamMode.Write))
                            {
                                cs.Write(clearBytes, 0, clearBytes.Length);
                                cs.Close();
                            }
                            clearText = Convert.ToBase64String(ms.ToArray());
                        }
                    }
                }
                return clearText;
            }
            catch (Exception ex) { return "Something went wrong."; }
            finally
            {
                dt.Dispose();
            }

        }

        // intUsertype 1-Admin,2-Member and 0:Other send unique id like android id  intType= 1-user 2- guest
        public string DecryptAdmin(string strUserName, string cipherText, int intUsertype, int intType, int intsource)
        {
            string Encrypt_key = "", salt = "";
            int strUserID = 0;
            try
            {
                if (intType == 1)
                {
                   
                        strUserID = objODBC.executeScalar_int("select userid from er_login_admin where username='" + strUserName + "' or emailid='" + strUserName + "' ");
                        if (strUserID > 0)
                        {
                            if (intsource == 1)
                            {
                                dt = objODBC.getDataTable("select encryption_key,salt1,pass_key from er_login_admin where userid=" + strUserID);
                            }
                            if (intsource == 2)
                            {
                                dt = objODBC.getDataTable("select encryption_key,salt2,pass_key from er_login_admin where userid=" + strUserID);
                            }
                            if (intsource == 3)
                            {
                                dt = objODBC.getDataTable("select encryption_key,salt3,pass_key from er_login_admin where userid=" + strUserID);
                            }

                            if (dt.Rows.Count > 0)
                            {
                            Encrypt_key = objEncrypt.DecryptQueryString(dt.Rows[0][0].ToString(), dt.Rows[0][2].ToString());
                            salt = objEncrypt.DecrypSalt(dt.Rows[0][1].ToString(), dt.Rows[0][2].ToString());


                            //Encrypt_key = dt.Rows[0][0].ToString();
                            //salt = dt.Rows[0][1].ToString();
                        }
                        }
                        else
                        {
                            Encrypt_key = "";
                            salt = "";
                        }
                   
                }
                if (intType == 2)
                {
                    dt = objODBC.getDataTable("select encryptionkey,salt,pass_key from er_guest_encrptionkey where id=1");
                    if (dt.Rows.Count > 0)
                    {
                        Encrypt_key = objEncrypt.DecryptQueryString(dt.Rows[0][0].ToString(), dt.Rows[0][2].ToString());
                        salt = objEncrypt.DecrypSalt(dt.Rows[0][1].ToString(), dt.Rows[0][2].ToString());
                        //"IaOb23PiLdopKh6ybbv" "0x49, 0x76, 0x61, 0x6e, 0x20, 0x4d, 0x65, 0x64, 0x76, 0x65, 0x64, 0x65, 0x76"

                        //Encrypt_key = dt.Rows[0][0].ToString();
                        //salt = dt.Rows[0][1].ToString();
                    }
                }
                if (dt.Rows.Count > 0)
                {

                    byte[] cipherBytes = Convert.FromBase64String(cipherText);

                    using (Aes encryptor = Aes.Create())
                    {
                        Rfc2898DeriveBytes pdb = new Rfc2898DeriveBytes(Encrypt_key, Encoding.UTF8.GetBytes(salt));
                        encryptor.Key = pdb.GetBytes(32);
                        encryptor.IV = pdb.GetBytes(16);
                        using (MemoryStream ms = new MemoryStream())
                        {
                            using (CryptoStream cs = new CryptoStream(ms, encryptor.CreateDecryptor(), CryptoStreamMode.Write))
                            {
                                cs.Write(cipherBytes, 0, cipherBytes.Length);
                                //cs.Close();
                            }
                            //"Retailer,123456,101.85.196.251,2,02:00:00:00:00:00,26,358240051111110,0,1.0,2"
                            cipherText = Encoding.ASCII.GetString(ms.ToArray());
                        }
                    }
                }
                return cipherText;
            }
            catch (Exception ex) { return "Something went wrong."; }
            finally
            {
                dt.Dispose();
            }

        }
    }
}