﻿using RestSharp;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Web;
using System.Web.Services;
using System.Data;
using Etopup_Ws.old_App_Code;
using System.Xml;

namespace Etopup_Ws.old_App_Code
{
    public class Roshan
    {
        ODBC objODBC = new ODBC();


        public string GetBalanceRoshan(string strMSISDN)
        {
            strMSISDN = "0793444628";
            var varRequestparameter = "<?xml version=\"1.0\"?>\r\n<COMMAND>\r\n<TYPE>RBEREQ</TYPE>\r\n<MSISDN>" + strMSISDN + "</MSISDN>\r\n<PROVIDER>101</PROVIDER>\r\n<PAYID>12</PAYID>\r\n<PIN>1345</PIN>\r\n<LANGUAGE1>1</LANGUAGE1>\r\n</COMMAND>\r\n";
            var client = new RestClient("http://192.168.25.176:7891/Txn/CelliciumSelector?LOGIN=Ussd_Bearer1&PASSWORD=MPtc1ToayCkCMZZeHUu0snA3aUaPbSFQ9UzIkNGbVRU=&REQUEST_GATEWAY_CODE=CELLUSSD&REQUEST_GATEWAY_TYPE=USSD&requestText=");
            var request = new RestRequest(Method.POST);
            request.AddHeader("Postman-Token", "679b003d-e11b-47ca-9419-41426adbea84");
            request.AddHeader("cache-control", "no-cache");
            request.AddHeader("Content-Type", "application/xml");
            request.AddParameter("undefined", varRequestparameter, ParameterType.RequestBody);
            IRestResponse response = client.Execute(request);

            return response.Content.ToString();
        }

        public string RoshanTopUp(int intRoshanTopupID, string MSISDN2MobileCus, string strAmount)
        {
            var varRequestParameter = "<?xml version=\"1.0\"?>\r\n<COMMAND>\r\n\t<PROVIDER2>101</PROVIDER2>\r\n\t<OPERATORID>OP1310281104</OPERATORID>\r\n\t<AMOUNT>" + strAmount + "</AMOUNT>\r\n\t<PAYMENTTYPE>WALLET</PAYMENTTYPE>\r\n\t<PAYID2>12</PAYID2>\r\n\t<PAYID>12</PAYID>\r\n\t<LANGUAGE1>1</LANGUAGE1>\r\n\t<MSISDN2>" + MSISDN2MobileCus + "</MSISDN2>\r\n\t<PIN>1345</PIN>\r\n\t<PROVIDER>101</PROVIDER>\r\n\t<MSISDN>0793444628</MSISDN>\r\n\t<TYPE>RTMMREQ</TYPE>\r\n</COMMAND>\r\n\r\n";


            var varRequestParameterData = "<?xml version=\"1.0\"?>\r\n<COMMAND>\r\n\t<PROVIDER2>101</PROVIDER2>\r\n\t<OPERATORID>OP1310281104</OPERATORID>\r\n\t<AMOUNT>" + strAmount + "</AMOUNT>\r\n\t<PAYMENTTYPE>WALLET</PAYMENTTYPE>\r\n\t<PAYID2>12</PAYID2>\r\n\t<PAYID>12</PAYID>\r\n\t<LANGUAGE1>1</LANGUAGE1>\r\n\t<MSISDN2>" + MSISDN2MobileCus + "</MSISDN2>\r\n\t<PIN>1345</PIN>\r\n\t<PROVIDER>101</PROVIDER>\r\n\t<MSISDN>0793444628</MSISDN>\r\n\t<TYPE>RTMMREQ</TYPE>\r\n</COMMAND>\r\n\r\n";

            try
            {
                objODBC.executeNonQuery("Update er_roshan_request set top_up_request='" + varRequestParameterData.ToString() + "' where id='" + intRoshanTopupID + "'");
            }
            catch (Exception)
            {

            }
            // objODBC.executeNonQuery("INSERT INTO `temp`( `data`, `type`, `request`) VALUES('4','3','RoshanService')");
            var client = new RestClient("http://192.168.25.176:7891/Txn/CelliciumSelector?LOGIN=Ussd_Bearer1&PASSWORD=MPtc1ToayCkCMZZeHUu0snA3aUaPbSFQ9UzIkNGbVRU=&REQUEST_GATEWAY_CODE=CELLUSSD&REQUEST_GATEWAY_TYPE=USSD&requestText=");

            var request = new RestRequest(Method.POST);

            request.AddHeader("Postman-Token", "ee11dfc8-0dea-4f30-8afd-720219487cb0");
            request.AddHeader("cache-control", "no-cache");
            request.AddHeader("Content-Type", "application/xml");
            request.AddParameter("undefined", varRequestParameter, ParameterType.RequestBody);

            IRestResponse response = client.Execute(request);
            //  objODBC.executeNonQuery("INSERT INTO `temp`( `data`, `type`, `request`) VALUES('4','3','"+ response.Content.ToString() + "')");
            try
            {


                objODBC.executeNonQuery("Update er_roshan_request set msisdn='0793444628',provider=101,payID=12,pin=1345,language1=1,type='RTMMREQ',provider2=101,operaterID='OP1310281104',amount='" + strAmount + "',payment_type='WALLET',pay_ID_2=12,msidn_2='" + MSISDN2MobileCus + "',`top_up_response`='" + response.Content.ToString() + "' where id='" + intRoshanTopupID + "'");

             
            }
            catch (Exception)
            {

            }
            return response.Content.ToString();

        }
        public ArrayList getRoshanBalanceBalance(string strMSISDN)
        {

            ArrayList arrUserParams = new ArrayList();

            try
            {
                var varRequestparameter = "<?xml version=\"1.0\"?>\r\n<COMMAND>\r\n<TYPE>RBEREQ</TYPE>\r\n<MSISDN>" + strMSISDN + "</MSISDN>\r\n<PROVIDER>101</PROVIDER>\r\n<PAYID>12</PAYID>\r\n<PIN>1345</PIN>\r\n<LANGUAGE1>1</LANGUAGE1>\r\n</COMMAND>\r\n";
                var client = new RestClient("http://192.168.25.176:7891/Txn/CelliciumSelector?LOGIN=Ussd_Bearer1&PASSWORD=MPtc1ToayCkCMZZeHUu0snA3aUaPbSFQ9UzIkNGbVRU=&REQUEST_GATEWAY_CODE=CELLUSSD&REQUEST_GATEWAY_TYPE=USSD&requestText=");
                var request = new RestRequest(Method.POST);
                request.AddHeader("Postman-Token", "679b003d-e11b-47ca-9419-41426adbea84");
                request.AddHeader("cache-control", "no-cache");
                request.AddHeader("Content-Type", "application/xml");
                request.AddParameter("undefined", varRequestparameter, ParameterType.RequestBody);
                IRestResponse response = client.Execute(request);



                XmlDocument xmlDoc = new XmlDocument();
                xmlDoc.LoadXml(response.Content.ToString());
                XmlNode nodeList1 = xmlDoc.DocumentElement.SelectSingleNode("COMMAND");




                arrUserParams.Add("True");
                arrUserParams.Add("TYPE");
                var requesttype = xmlDoc.ChildNodes[1].ChildNodes[0].InnerXml.ToString();
                arrUserParams.Add(requesttype);

                arrUserParams.Add("TXNID");
                var TransactionID = xmlDoc.ChildNodes[1].ChildNodes[1].InnerXml.ToString();
                arrUserParams.Add(TransactionID);

                arrUserParams.Add("TXNSTATUS");
                var TXNSTATUS = xmlDoc.ChildNodes[1].ChildNodes[1].InnerXml.ToString();
                arrUserParams.Add(TXNSTATUS);

                arrUserParams.Add("BALANCE");
                var Balance = xmlDoc.ChildNodes[1].ChildNodes[2].InnerXml.ToString();
                if (Balance == null || Balance == "")
                {
                    Balance = "0";
                }
                arrUserParams.Add(Balance);

                arrUserParams.Add("MESSAGE");
                var Message = xmlDoc.ChildNodes[1].ChildNodes[3].InnerXml.ToString();
                arrUserParams.Add(Message);

                arrUserParams.Add("FRBALANCE");
                var FRBALANCE = xmlDoc.ChildNodes[1].ChildNodes[4].InnerXml.ToString();
                arrUserParams.Add(FRBALANCE);

                arrUserParams.Add("IVR-RESPONSE");
                var IVR_RESPONSE = xmlDoc.ChildNodes[1].ChildNodes[5].InnerXml.ToString();
                arrUserParams.Add(IVR_RESPONSE);



            }
            catch (Exception e1)
            {
                arrUserParams.Add(e1.ToString());
            }
            return arrUserParams;
        }

        public string getRoshanBalanceBalanceAPI()
        {
            string strMSISDN = "0793444628";
            
            try
            {
                var varRequestparameter = "<?xml version=\"1.0\"?>\r\n<COMMAND>\r\n<TYPE>RBEREQ</TYPE>\r\n<MSISDN>" + strMSISDN + "</MSISDN>\r\n<PROVIDER>101</PROVIDER>\r\n<PAYID>12</PAYID>\r\n<PIN>1345</PIN>\r\n<LANGUAGE1>1</LANGUAGE1>\r\n</COMMAND>\r\n";
                var client = new RestClient("http://192.168.25.176:7891/Txn/CelliciumSelector?LOGIN=Ussd_Bearer1&PASSWORD=MPtc1ToayCkCMZZeHUu0snA3aUaPbSFQ9UzIkNGbVRU=&REQUEST_GATEWAY_CODE=CELLUSSD&REQUEST_GATEWAY_TYPE=USSD&requestText=");
                var request = new RestRequest(Method.POST);
                request.AddHeader("Postman-Token", "679b003d-e11b-47ca-9419-41426adbea84");
                request.AddHeader("cache-control", "no-cache");
                request.AddHeader("Content-Type", "application/xml");
                request.AddParameter("undefined", varRequestparameter, ParameterType.RequestBody);
                IRestResponse response = client.Execute(request);



                XmlDocument xmlDoc = new XmlDocument();
                xmlDoc.LoadXml(response.Content.ToString());
                XmlNode nodeList1 = xmlDoc.DocumentElement.SelectSingleNode("COMMAND");




                var Balance = xmlDoc.ChildNodes[1].ChildNodes[3].InnerXml.ToString();
                if(Balance==null || Balance=="")
                {
                    Balance = "0";
                }
               return Balance;

            }
            catch (Exception e1)
            {
                return "0";
            }           
        }


    }
}