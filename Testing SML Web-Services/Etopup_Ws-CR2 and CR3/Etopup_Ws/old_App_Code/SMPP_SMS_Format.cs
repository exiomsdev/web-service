﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Etopup_Ws.old_App_Code
{
    public class SMPP_SMS_Format
    {
        DateFormat objDT = new DateFormat();
        public string MemberRegistration(string strUserName, string strFullName, string strPassword, string strMPin)
        {
            string strMessage = "Dear " + strFullName.Trim() + ", Welcome to Setaragan Mutahed Ltd, Your User Name: " + strUserName.Trim() + ", Password: " + strPassword + " and M-Pin: " + strMPin + ". Thanks for using Setaragan Top-up !";
            
            return strMessage;
        }

        public string USSDTopupBalance(string strUserName, string strFullName, double strBalance)
        {
            // string strMessage = "Dear " + strFullName.Trim() + ", Your Current Bal:" + strBalance + " AFN for account '" + strUserName + "',Thanks for using Setaragan Top-up !";
            string strMessage = "Dear " + strFullName.Trim() + ", Your Balance:" + strBalance + " AFN,Thanks for using Setaragan Top-up !";
            return strMessage;
        }
        public string USSDTopupSaleSMS(string strUserName, string strFullName, double strBalance, string usertype)
        {
            // string strMessage = "Dear " + strFullName.Trim() + ", Your Current Bal:" + strBalance + " AFN for account '" + strUserName + "',Thanks for using Setaragan Top-up !";
            if (usertype == "5")
            {
                string strMessage = "Dear " + strFullName.Trim() + ", your Today total-topup:" + strBalance + " AFN,Thanks for using Setaragan Top-up !";
                return strMessage;
            }
            else
            {
                string strMessage = "Dear " + strFullName.Trim() + ", your Today total-Sales:" + strBalance + " AFN,Thanks for using Setaragan Top-up !";
                return strMessage;
            }
        }
        public string USSDMessage()
        {
            string strMessage = ". Thanks for using Setaragan Top-up !";
            return strMessage;
        }


        public string TopupLowBalance(string strUserName, string strFullName, string Mobile, string intOperaterName, double dblRechargeAmount, string TransactionID, double dblWalletBalance)
        {
            string strDate = objDT.getCurDateTimeString();
            // string strMessage = "Dear " + strFullName.Trim() + ", Topup Fail for " + Mobile + " of amount " + dblRechargeAmount + " AFN for " + intOperaterName + " at " + strDate + ".Now, your balance is " + dblWalletBalance + " AFN.Thanks for using Setaragan Top-up !";

            string strMessage = "Dear " + strFullName.Trim() + ", your topped-up Failed Mobile: " + Mobile + " with " + dblRechargeAmount + " AFN Transaction ID:" + TransactionID + ", Current Balance:" + dblWalletBalance + ".";

            return strMessage;
        }
        public string TopupSuccessBalance(string strUserName, string strFullName, string Mobile, string intOperaterName, double dblRechargeAmount, string TransactionID, double dblWalletBalance)
        {
            string strDate = objDT.getCurDateTimeString();
          //  string strMessage = "Dear " + strFullName.Trim() + ", Topup Successful of amount " + dblRechargeAmount + " AFN for " + Mobile + " of " + intOperaterName + "  at " + strDate + ". Now, your balance is " + dblWalletBalance + " AFN and Transaction ID is " + TransactionID + ". Thanks for using Setaragan Top-up !";
            string strMessage = "Dear " + strFullName.Trim() + ", you have topped-up Mobile: " + Mobile + " with " + dblRechargeAmount + " AFN Transaction ID:" + TransactionID + ", Current Balance:" + dblWalletBalance + ".";
            //  Dear #Retailername, you have topped-up Mobile: 071234568 with 10 AFN  TXNID:…….. Current Balance:………
            return strMessage;
        }


        public string TopupSuccessEndUser(string strUsername,string Mobile, double dblRechargeAmount, string TransactionID)
        {
            //  string strMessage = "Your Mobile has been Successfully Topped-Up for AFN " + dblRechargeAmount + " by "+ strUsername.Trim() + " with TnxID#" + TransactionID + ". Thanks for using Setaragan Top-up !";

            string strMessage = "Your Mobile:" + Mobile + " has been successfully Topped-up with " + dblRechargeAmount + " AFN By:#" + strUsername + ",TXNID:#" + TransactionID + ". Thanks for using Setaragan Top-up !";

            return strMessage;
        }

        public string TopupPendingEndUser(string strUsername, string Mobile,  double dblRechargeAmount, string TransactionID)
        {
            string strMessage = "Your Mobile Topped-Up is in Pending for AFN " + dblRechargeAmount + " by Setaragan Mutahed with TnxID:#" + TransactionID + ". Thanks for using Setaragan Top-up !";
            return strMessage;

        }
        public string TopupFailedEndUser(string strUsername, string Mobile, double dblRechargeAmount, string TransactionID)
        {
            //string strMessage = "Your Mobile Topped-Up is Failed for AFN " + dblRechargeAmount + " by Setaragan Mutahed with TnxID#" + TransactionID + ". Thanks for using Setaragan Top-up !";
            //return strMessage;

           // string strMessage = "Your Mobile:" + Mobile + " Topped-up Failed with " + dblRechargeAmount + " AFN TXNID#" + TransactionID + ". Thanks for using Setaragan Top-up !";

            string strMessage = "Your Mobile:" + Mobile + " Topped-up Failed with " + dblRechargeAmount + " AFN By:#" + strUsername + ",TXNID:#" + TransactionID + ". Thanks for using Setaragan Top-up !";
            return strMessage;
        }


        public string ReverseTopupSuccessBalance(string strUserName, string strFullName, string Mobile, string intOperaterName, double dblRechargeAmount, string TransactionID, double dblWalletBalance)
        {
            string strDate = objDT.getCurDateTimeString();
            //string strMessage = "Dear " + strFullName.Trim() + ", Reverse Topup is Successful completed of amount " + dblRechargeAmount + " AFN for " + Mobile + " of " + intOperaterName + "  at " + strDate + ". Now, your balance is " + dblWalletBalance + " AFN and Transaction ID is " + TransactionID + ". Thanks for using Setaragan Top-up !";

            //  string strMessage = "Dear " + strFullName.Trim() + ", your rollback request for amount  " + dblRechargeAmount + " AFN for Mobile: " + Mobile + " as been succeed, your Current Balance is " + dblWalletBalance + ".";

            //  string strMessage = "Dear " + strFullName.Trim() + ", your rollback request has been succssfully Proceed for the Amount: " + dblRechargeAmount + " AFN. Thanks for using Setaragan Top-up !";
            string strMessage = "Dear " + strFullName.Trim() + ", your rollback request has been succssfully Complete Amount: " + dblRechargeAmount + " AFN, Mobile: " + Mobile + ",Current Balance:" + dblWalletBalance + ". Thanks for using Setaragan Top-up !";

            return strMessage;
        }

        public string ReverseTopupFailBalance(string strUserName, string strFullName, string Mobile, string intOperaterName, double dblRechargeAmount, string TransactionID, double dblWalletBalance)
        {
            string strDate = objDT.getCurDateTimeString();
            //  string strMessage = "Dear " + strFullName.Trim() + ",Reverse Topup Fail of amount " + dblRechargeAmount + " AFN for " + Mobile + " of " + intOperaterName + "  at " + strDate + ". Now, your balance is " + dblWalletBalance + " AFN and Transaction ID is " + TransactionID + ". Thanks for using Setaragan Top-up !";

            string strMessage = "Dear " + strFullName.Trim() + ", your rollback request has been Failed due to Low balance in customer GSM, Amount:" + dblRechargeAmount + " AFN,Mobile:" + Mobile + ",Current Balance: " + dblWalletBalance + ".";

            return strMessage;
        }

        public string TopupFailBalance(string strUserName, string strFullName, string Mobile, string intOperaterName, double dblRechargeAmount, string TransactionID, double dblWalletBalance)
        {
            string strDate = objDT.getCurDateTimeString();
            string strMessage = "Dear " + strFullName.Trim() + ", Topup Fail of amount " + dblRechargeAmount + " AFN for " + Mobile + " of " + intOperaterName + "  at " + strDate + ". Now, your balance is " + dblWalletBalance + " AFN and Transaction ID is " + TransactionID + ". Thanks for using Setaragan Top-up !";

            return strMessage;
        }

        


        public string TopupPendingBalance(string strUserName, string strFullName, string Mobile, string intOperaterName, double dblRechargeAmount, string TransactionID, double dblWalletBalance)
        {
            string strDate = objDT.getCurDateTimeString();
            string strMessage = "Dear " + strFullName.Trim() + ", Topup Pending of amount " + dblRechargeAmount + " AFN for " + Mobile + " of " + intOperaterName + "  at " + strDate + ". Now, your balance is " + dblWalletBalance + " AFN and Transaction ID is " + TransactionID + ". Thanks for using Setaragan Top-up !";

            return strMessage;
        }
        public string ReverseTopupPendingBalance(string strUserName, string strFullName, string Mobile, string intOperaterName, double dblRechargeAmount, string TransactionID, double dblWalletBalance)
        {
            string strDate = objDT.getCurDateTimeString();
            string strMessage = "Dear " + strFullName.Trim() + ",Topup Request for Reverse is in Pending of amount " + dblRechargeAmount + " AFN for " + Mobile + " of " + intOperaterName + "  at " + strDate + ". Now, your balance is " + dblWalletBalance + " AFN and Transaction ID is " + TransactionID + ". Thanks for using Setaragan Top-up !";

            return strMessage;
        }

        public string LowBalance(string strUserName, string strFullName, double dblBalance)
        {
            string strMessage = "Dear " + strFullName.Trim() + ", Your Wallet balance is low, your Balance is " + dblBalance + " AFN."; 
            return strMessage;
        }

        public string Login(string strUserName, string strFullName, string strSource)
        {
            string strMessage = "";
            string strDate = objDT.getCurDateTimeString();
            if (strSource == "1") // Web Login
            {
                strMessage = "Dear " + strFullName.Trim() + ", You have been logged to your account " + strUserName.Trim() + " From Web at " + strDate + " .Thanks for using Setaragan Top-up !";
            }
            else if (strSource == "2") // Android Login
            {
                strMessage = "Dear " + strFullName.Trim() + ", You have been logged to your account " + strUserName.Trim() + " From android device at " + strDate + " .Thanks for using Setaragan Top-up !";
            }
            else if (strSource == "3") // Android Login
            {
                strMessage = "Dear " + strFullName.Trim() + ", You have been logged to your account " + strUserName.Trim() + " From IOS device at " + strDate + " .Thanks for using Setaragan Top-up !";
            }

            return strMessage;
        }


        public string ChangePassword(string strUserName, string strFullName, int strSource,string strNewPassword)
        {
            string strMessage = "";
            string strDate = objDT.getCurDateTimeString();
            if (strSource == 1) // Web Login
            {
                //  strMessage = "Dear " + strFullName.Trim() + ", your password had been changed for account " + strUserName.Trim() + " From Web at " + strDate + " .Thanks for using Setaragan Top-up !";

                 strMessage = "Dear " + strFullName.Trim() + ",Your Password has been changed From Web at " + strDate + " , New Password is" + strNewPassword + " . Thanks for using Setaragan Top-up !";
            }
            else if (strSource == 2) // Android Login
            {
                // strMessage = "Dear " + strFullName.Trim() + ", your password had been changed for account " + strUserName.Trim() + " From Android device at " + strDate + " .Thanks for using Setaragan Top-up !";

                strMessage = "Dear " + strFullName.Trim() + ",Your Password has been changed From Android at " + strDate + " , New Password is" + strNewPassword + " . Thanks for using Setaragan Top-up !";
            }
            else if (strSource == 3) // Android Login
            {
                // strMessage = "Dear " + strFullName.Trim() + ", your password had been changed for account " + strUserName.Trim() + " From IOS device at " + strDate + " .Thanks for using Setaragan Top-up !";
                strMessage = "Dear " + strFullName.Trim() + ",Your Password has been changed From IOS at " + strDate + " , New Password is" + strNewPassword + " . Thanks for using Setaragan Top-up !";
            }

            return strMessage;
        }


        public string EditProfile(string strUserName, string strFullName, int strSource)
        {
            string strMessage = "";
            string strDate = objDT.getCurDateTimeString();
            if (strSource == 1) // Web Login
            {
                strMessage = "Dear " + strFullName.Trim() + ", You have changed the profile details for your account " + strUserName.Trim() + " From Web at " + strDate + " .Thanks for using Setaragan Top-up !";
            }
            else if (strSource == 2) // Android Login
            {
                strMessage = "Dear " + strFullName.Trim() + ", You have changed the profile details for your account " + strUserName.Trim() + " From Android device at " + strDate + " .Thanks for using Setaragan Top-up !";
            }
            else if (strSource == 3) // Android Login
            {
                strMessage = "Dear " + strFullName.Trim() + ", You have changed the profile details for your account " + strUserName.Trim() + " From IOS device at " + strDate + " .Thanks for using Setaragan Top-up !";
            }

            return strMessage;
        }


        public string FundRequestWallet(string strUserName, string strFullName, int strSource, double dblRequestAmount, string strTransacctionNumber, int intModeOfPayment)
        {
            string strMessage = "";
            string strDate = objDT.getCurDateTimeString();
            if (strSource == 1) // Web Login
            {
                strMessage = "Dear " + strFullName.Trim() + ", Fund Request for your account " + strUserName.Trim() + " is send Successfuly from web of amount " + dblRequestAmount + " and transaction ID is " + strTransacctionNumber + " at " + strDate + " .Thanks for using Setaragan Top-up !";
            }
            else if (strSource == 2) // Android Login
            {
                strMessage = "Dear " + strFullName.Trim() + ", Fund Request for your account " + strUserName.Trim() + " is send Successfuly from android device of amount " + dblRequestAmount + " and transaction ID is " + strTransacctionNumber + " at " + strDate + " .Thanks for using Setaragan Top-up !";
            }
            else if (strSource == 3) // Android Login
            {
                strMessage = "Dear " + strFullName.Trim() + ", Fund Request for your account " + strUserName.Trim() + " is send Successfuly from IOS device of amount " + dblRequestAmount + " and transaction ID is " + strTransacctionNumber + " at " + strDate + " .Thanks for using Setaragan Top-up !";
            }

            return strMessage;
        }

        public string FundRequestWalletParent(string strFullName, int strSource, double dblRequestAmount, string strTransacctionNumber, int intModeOfPayment,string strReceiverUserName,string strReceiverFullName)
        {
            string strMessage = "";
            string strDate = objDT.getCurDateTimeString();
            if (strSource == 1) // Web Login
            {
                strMessage = "Dear " + strFullName.Trim() + ", Fund Request from "+ strReceiverFullName + " for account " + strReceiverUserName + " had been received from web of amount " + dblRequestAmount + " and transaction ID is " + strTransacctionNumber + " at " + strDate + " .Thanks for using Setaragan Top-up !";
            }
            else if (strSource == 2) // Android Login
            {
                strMessage = "Dear " + strFullName.Trim() + ", Fund Request from " + strReceiverFullName + " for account " + strReceiverUserName + " had been received from android device of amount " + dblRequestAmount + " and transaction ID is " + strTransacctionNumber + " at " + strDate + " .Thanks for using Setaragan Top-up !";
            }
            else if (strSource == 3) // IOS Login
            {
                strMessage = "Dear " + strFullName.Trim() + ", Fund Request from " + strReceiverFullName + " for account " + strReceiverUserName + " had been received from IOS device of amount " + dblRequestAmount + " and transaction ID is " + strTransacctionNumber + " at " + strDate + " .Thanks for using Setaragan Top-up !";
            }
            return strMessage;
        }


        public string ReverseFundRequestWallet(string strUserName, string strFullName, int strSource, double dblRequestAmount, double WalletBalance)
        {
            string strMessage = "";
            string strDate = objDT.getCurDateTimeString();
            if (strSource == 1) // Web Login
            {
                strMessage = "Dear " + strFullName.Trim() + ", Fund Request for your account " + strUserName.Trim() + " is debited from web of amount " + dblRequestAmount + " at " + strDate + " and your current wallet balance is " + WalletBalance + "  .Thanks for using Setaragan Top-up !";
            }
            else if (strSource == 2) // Android Login
            {
                strMessage = "Dear " + strFullName.Trim() + ", Fund Request for your account " + strUserName.Trim() + " is debited Successfuly from android device of amount " + dblRequestAmount + " at " + strDate + " and your current wallet balance is " + WalletBalance + "  .Thanks for using Setaragan Top-up !";
            }
            else if (strSource == 3) // Android Login
            {
                strMessage = "Dear " + strFullName.Trim() + ", Fund Request for your account " + strUserName.Trim() + " is debited Successfuly from IOS device of amount " + dblRequestAmount + " at " + strDate + " and your current wallet balance is " + WalletBalance + "  .Thanks for using Setaragan Top-up !";
            }
            return strMessage;
        }

        public string FundRequestApproveWallet(string strUserName, string strFullName, int strSource, double dblRequestAmount, string strTransacctionNumber, double dblWalletbalance)
        {
            string strMessage = "";
            string strDate = objDT.getCurDateTimeString();
            if (strSource == 1) // Web Login
            {
                strMessage = "Dear " + strFullName.Trim() + ", Fund Request of amount " + dblRequestAmount + " AFN for the account " + strUserName.Trim() + " is Approve Successfuly from web with transaction ID is " + strTransacctionNumber + " and your currenct Wallet balance is " + dblWalletbalance + " at " + strDate + " .Thanks for using Setaragan Top-up !";
            }
            else if (strSource == 2) // Android Login
            {
                strMessage = "Dear " + strFullName.Trim() + ", Fund Request of amount " + dblRequestAmount + " AFN for the account " + strUserName.Trim() + " is Approve Successfuly from android device with transaction ID is " + strTransacctionNumber + " and your currenct Wallet balance is " + dblWalletbalance + " at " + strDate + " .Thanks for using Setaragan Top-up !";
            }
            else if (strSource == 3) // Android Login
            {
                strMessage = "Dear " + strFullName.Trim() + ", Fund Request of amount " + dblRequestAmount + " AFN for the account " + strUserName.Trim() + " is Approve Successfuly from IOS device with transaction ID is " + strTransacctionNumber + " and your currenct Wallet balance is " + dblWalletbalance + " at " + strDate + " .Thanks for using Setaragan Top-up !";
            }
            return strMessage;
        }

        public string FundTransferWalletSender(string strUserName, string strFullName, int strSource, double dblTransferAmount, string strSenderFullName, string strSenderUserName, double strBalance,string TransID)
        {
            string strMessage = "";
            string strDate = objDT.getCurDateTimeString();
            if (strSource == 1) // Web Login
            {
                //  strMessage = "Dear " + strSenderFullName.Trim() + ", Amount of " + dblTransferAmount + " is debited from your account " + strSenderUserName.Trim() + " and credited to " + strFullName.Trim() + " account " + strUserName.Trim() + " from web at " + strDate + ", Your Current Bal amount is " + strBalance + " .Thanks for using Setaragan Top-up !";
               // Dear #UserName, you have transferred Amount: 100 to #UserName TXNID:……………..Current balance:………………..Happy Selling.

                strMessage = "Dear " + strSenderFullName.Trim() + ", you have transferred Amount :" + dblTransferAmount + " AFN to " + strFullName.Trim() + ", Current balance:" + strBalance + "AFN,Tnx ID#"+ TransID + ". Happy Selling !";
            }
            else if (strSource == 2) // Android Login
            {
                // strMessage = "Dear " + strSenderFullName.Trim() + ", Amount of " + dblTransferAmount + " is debited from your account " + strSenderUserName.Trim() + " and credited to " + strFullName.Trim() + " account " + strUserName.Trim() + "  from android device at " + strDate + ", Your Current Bal amount is " + strBalance + " .Thanks for using Setaragan Top-up !";

                strMessage = "Dear " + strSenderFullName.Trim() + ", you have transferred Amount :" + dblTransferAmount + " AFN to " + strFullName.Trim() + ", Current balance:" + strBalance + "AFN,Tnx ID#" + TransID + ". Happy Selling !";
            }
            else if (strSource == 3) // IOS Login
            {
                //   strMessage = "Dear " + strSenderFullName.Trim() + ", Amount of " + dblTransferAmount + " is debited from your account " + strSenderUserName.Trim() + " and credited to " + strFullName.Trim() + " account " + strUserName.Trim() + "  from IOS device at " + strDate + ", Your Current Bal amount is " + strBalance + " .Thanks for using Setaragan Top-up !";

                strMessage = "Dear " + strSenderFullName.Trim() + ", you have transferred Amount :" + dblTransferAmount + " AFN to " + strFullName.Trim() + ", Current balance:" + strBalance + "AFN,Tnx ID#" + TransID + ". Happy Selling !";
            }
            else if (strSource == 4) // USSD Request
            {
                //  strMessage = "Dear " + strSenderFullName.Trim() + ", Amount of " + dblTransferAmount + " is debited from your account " + strSenderUserName.Trim() + " and credited to " + strFullName.Trim() + " account " + strUserName.Trim() + "  from USSD Request at " + strDate + ", Your Current Bal amount is " + strBalance + " .Thanks for using Setaragan Top-up !";

                strMessage = "Dear " + strSenderFullName.Trim() + ", you have transferred Amount :" + dblTransferAmount + " AFN to " + strFullName.Trim() + ", Current balance:" + strBalance + "AFN,Tnx ID#" + TransID + ". Happy Selling !";
            }
            else if (strSource == 5) // SMS Request
            {
                //  strMessage = "Dear " + strSenderFullName.Trim() + ", Amount of " + dblTransferAmount + " is debited from your account " + strSenderUserName.Trim() + " and credited to " + strFullName.Trim() + " account " + strUserName.Trim() + "  from SMS Request at " + strDate + ", Your Current Bal amount is " + strBalance + " .Thanks for using Setaragan Top-up !";

                strMessage = "Dear " + strSenderFullName.Trim() + ", you have transferred Amount :" + dblTransferAmount + " AFN to " + strFullName.Trim() + ", Current balance:" + strBalance + "AFN,Tnx ID#" + TransID + ". Happy Selling !";
            }

            return strMessage;
        }

        public string FundTransferWalletReceiver(string strUserName, string strFullName, int strSource, double dblTransferAmount, string strSenderFullName, string strSenderUserName, double strBalance, string TransID)
        {
            string strMessage = "";
            string strDate = objDT.getCurDateTimeString();
            if (strSource == 1) // Web Login
            {
                //   strMessage = "Dear " + strFullName.Trim() + ", Amount of " + dblTransferAmount + " is Creadited to your account " + strUserName.Trim() + " from " + strSenderFullName + " account " + strSenderUserName.Trim() + " from web  at " + strDate + ", Your Current Bal amount is " + strBalance + " .Thanks for using Setaragan Top-up !";
                //Dear #UserName, you have received Amount: 100 from #UserName TXNID:……………..Current balance:………………..Happy Selling.
                strMessage = "Dear " + strFullName.Trim() + ", you have received Amount " + dblTransferAmount + " AFN from " + strSenderFullName + ", Current balance:" + strBalance + " AFN,Tnx ID#" + TransID + ". Happy Selling !";
            }
            else if (strSource == 2) // Android Login
            {
                // strMessage = "Dear " + strFullName.Trim() + ", Amount of " + dblTransferAmount + " is Creadited to your account " + strUserName.Trim() + " from " + strSenderFullName + " account " + strSenderUserName.Trim() + " from android device  at " + strDate + ", Your Current Bal amount is " + strBalance + " .Thanks for using Setaragan Top-up !";

                strMessage = "Dear " + strFullName.Trim() + ", you have received Amount " + dblTransferAmount + " AFN from " + strSenderFullName + ", Current balance:" + strBalance + " AFN,Tnx ID#" + TransID + ". Happy Selling !";
            }
            else if (strSource == 3) // ISO Login
            {
                //   strMessage = "Dear " + strFullName.Trim() + ", Amount of " + dblTransferAmount + " is Creadited to your account " + strUserName.Trim() + " from " + strSenderFullName + " account " + strSenderUserName.Trim() + " from IOS device at " + strDate + ", Your Current Bal amount is " + strBalance + " .Thanks for using Setaragan Top-up !";


                strMessage = "Dear " + strFullName.Trim() + ", you have received Amount " + dblTransferAmount + " AFN from " + strSenderFullName + ", Current balance:" + strBalance + " AFN,Tnx ID#" + TransID + ". Happy Selling !";
            }
            else if (strSource == 4) // USSD Request
            {
                // strMessage = "Dear " + strFullName.Trim() + ", Amount of " + dblTransferAmount + " is Creadited to your account " + strUserName.Trim() + " from " + strSenderFullName + " account " + strSenderUserName.Trim() + " from USSD Request at " + strDate + ", Your Current Bal amount is " + strBalance + " .Thanks for using Setaragan Top-up !";


                strMessage = "Dear " + strFullName.Trim() + ", you have received Amount " + dblTransferAmount + " AFN from " + strSenderFullName + ", Current balance:" + strBalance + " AFN,Tnx ID#" + TransID + ". Happy Selling !";
            }
            else if (strSource == 5) // SMS Request
            {
                // strMessage = "Dear " + strFullName.Trim() + ", Amount of " + dblTransferAmount + " is Creadited to your account " + strUserName.Trim() + " from " + strSenderFullName + " account " + strSenderUserName.Trim() + " from USSD Request at " + strDate + ", Your Current Bal amount is " + strBalance + " .Thanks for using Setaragan Top-up !";


                strMessage = "Dear " + strFullName.Trim() + ", you have received Amount " + dblTransferAmount + " AFN from " + strSenderFullName + ", Current balance:" + strBalance + " AFN,Tnx ID#" + TransID + ". Happy Selling !";
            }

            return strMessage;
        }


        public string FundTransferWalletReceiver_reverse(string strUserName, string strFullName, int strSource, double dblTransferAmount, string strSenderFullName, string strSenderUserName, double strBalance)
        {
            string strMessage = "";
            string strDate = objDT.getCurDateTimeString();
            if (strSource == 1) // Web Login
            {
                //   strMessage = "Dear " + strFullName.Trim() + ", Amount of " + dblTransferAmount + " is Creadited to your account " + strUserName.Trim() + " from " + strSenderFullName + " account " + strSenderUserName.Trim() + " from web  at " + strDate + ", Your Current Bal amount is " + strBalance + " .Thanks for using Setaragan Top-up !";
                //Dear #UserName, you have received Amount: 100 from #UserName TXNID:……………..Current balance:………………..Happy Selling.
                strMessage = "Dear " + strFullName.Trim() + ", you have received Amount " + dblTransferAmount + " AFN from " + strSenderFullName + ", Current balance:" + strBalance + " AFN. Happy Selling !";
            }
            else if (strSource == 2) // Android Login
            {
                // strMessage = "Dear " + strFullName.Trim() + ", Amount of " + dblTransferAmount + " is Creadited to your account " + strUserName.Trim() + " from " + strSenderFullName + " account " + strSenderUserName.Trim() + " from android device  at " + strDate + ", Your Current Bal amount is " + strBalance + " .Thanks for using Setaragan Top-up !";

                strMessage = "Dear " + strFullName.Trim() + ", you have received Amount " + dblTransferAmount + " AFN from " + strSenderFullName + ", Current balance:" + strBalance + " AFN. Happy Selling !";
            }
            else if (strSource == 3) // ISO Login
            {
                //   strMessage = "Dear " + strFullName.Trim() + ", Amount of " + dblTransferAmount + " is Creadited to your account " + strUserName.Trim() + " from " + strSenderFullName + " account " + strSenderUserName.Trim() + " from IOS device at " + strDate + ", Your Current Bal amount is " + strBalance + " .Thanks for using Setaragan Top-up !";


                strMessage = "Dear " + strFullName.Trim() + ", you have received Amount " + dblTransferAmount + " AFN from " + strSenderFullName + ", Current balance:" + strBalance + " AFN. Happy Selling !";
            }
            else if (strSource == 4) // USSD Request
            {
                // strMessage = "Dear " + strFullName.Trim() + ", Amount of " + dblTransferAmount + " is Creadited to your account " + strUserName.Trim() + " from " + strSenderFullName + " account " + strSenderUserName.Trim() + " from USSD Request at " + strDate + ", Your Current Bal amount is " + strBalance + " .Thanks for using Setaragan Top-up !";


                strMessage = "Dear " + strFullName.Trim() + ", you have received Amount " + dblTransferAmount + " AFN from " + strSenderFullName + ", Current balance:" + strBalance + " AFN. Happy Selling !";
            }
            else if (strSource == 5) // SMS Request
            {
                // strMessage = "Dear " + strFullName.Trim() + ", Amount of " + dblTransferAmount + " is Creadited to your account " + strUserName.Trim() + " from " + strSenderFullName + " account " + strSenderUserName.Trim() + " from USSD Request at " + strDate + ", Your Current Bal amount is " + strBalance + " .Thanks for using Setaragan Top-up !";


                strMessage = "Dear " + strFullName.Trim() + ", you have received Amount " + dblTransferAmount + " AFN from " + strSenderFullName + ", Current balance:" + strBalance + " AFN. Happy Selling !";
            }

            return strMessage;
        }
        public string GetOTPtoChangeMobile(string strUserName, string strFullName, string strMobileNo, string strOTP, string ExpireTime)
        {
            string strMessage = "Dear " + strFullName.Trim() + ", OTP " + strOTP + " for change the mobile " + strMobileNo + " and it will be expire in time " + ExpireTime + " .Thanks for using Setaragan Top-up !";
            return strMessage;
        }


        public string GetOTPforgetMPIN(string strUserName, string strFullName, string strMobileNo, string strOTP, string ExpireTime)
        {
            string strMessage = "Dear " + strFullName.Trim() + ", OTP " + strOTP + " for change the MPIN and it will be expire in time " + ExpireTime + " .Thanks for using Setaragan Top-up !";
            return strMessage;
        }

        public string GetForgotMPIN(string strUserName, string strFullName, string strMobileNo, string strMPIN)
        {
            // string strMessage = "Dear " + strFullName.Trim() + ", MPIN for your account " + strUserName.Trim() + " is " + strMPIN + " Successfuly updated, Do not share with anyone .Thanks for using Setaragan Top-up !";

            string strMessage = "Dear " + strFullName.Trim() + ", You have successfully changed your M-Pin, New M-Pin is" + strMPIN + ". Thanks for using Setaragan Top-up !";
            return strMessage;
        }


        public string GetOTPforgetPassword(string strUserName, string strFullName, string strMobileNo, string strOTP, string ExpireTime)
        {
            string strMessage = "Dear " + strFullName.Trim() + ", OTP  " + strOTP + " to change the Password and it will be expire in time " + ExpireTime + " .Thanks for using Setaragan Top-up !";

            return strMessage;
        }

        public string GetForgotPassword(string strUserName, string strFullName, string strMobileNo, string strMPIN, string ExpireTime)
        {
            string strMessage = "Dear " + strFullName.Trim() + ", You have successfully changed your Password, New Password is" + strMPIN + ". Thanks for using Setaragan Top-up !";

            return strMessage;
        }
    }
}