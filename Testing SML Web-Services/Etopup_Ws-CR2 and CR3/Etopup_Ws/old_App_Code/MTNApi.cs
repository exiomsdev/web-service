﻿using System;
using System.IO;
using System.Net;
using System.Xml;

namespace Etopup_Ws.old_App_Code
{
    public class MTNApi
    {
       
        // API request users that is used in the soap request
        //=========================
        //SMLUser
        //SMLU@1220

        //Agent Portal user
        //=====================
        //SMLAgent
        //@#SML&CH#S



        public string MTNAPIBalance()
        {
            //  @"<soapenv:Envelope xmlns:soapenv=""http://schemas.xmlsoap.org/soap/envelope/""             xmlns: ext = ""http://external.interfaces.ers.seamless.com/""> <soapenv:Header />    <soapenv:Body>        <ext:requestTopup> <context>     < channel > WEBSERVICE </ channel > < clientComment > SML Top - up tnxID#" + strTransxID + "</clientComment>  <clientId>SML</clientId>     <clientReference>" + strTransxID + "</clientReference>     <clientRequestTimeout>5000</clientRequestTimeout>     <initiatorPrincipalId>         <id>SML</id>         <type>RESELLERUSER</type>         <userId>SMLUser</userId>     </initiatorPrincipalId>     <password>sml123</password> </context> <senderPrincipalId>     <id>SML</id>     <type>RESELLERID</type>     <userId>SMLUser</userId> </senderPrincipalId> <topupPrincipalId>     <id>" + strMobileNumber + "</id><type>SUBSCRIBERMSISDN</type> </topupPrincipalId> <senderAccountSpecifier>     <accountId>SML</accountId>     <accountTypeId>RESELLER_AIRTIME</accountTypeId> </senderAccountSpecifier> <topupAccountSpecifier>     <accountId>?</accountId>     <accountTypeId>SUBSCRIBER_AIRTIME</accountTypeId> </topupAccountSpecifier> <productId>2029</productId> <amount>     <currency>AFA</currency>     <value>" + dblAmount + "</value> </amount></ext:requestTopup></soapenv:Body></soapenv:Envelope>";


            // <string xmlns="http://tempuri.org/">< soap:Envelope xmlns:soap = "http://schemas.xmlsoap.org/soap/envelope/" >< soap:Body >< ns2:requestPrincipalInformationResponse xmlns:ns2 = "http://external.interfaces.ers.seamless.com/" ><return>< ersReference > 2020040111262925801000079 </ ersReference >< resultCode > 0 </ resultCode >< resultDescription > SUCCESS </ resultDescription >< requestedPrincipal >< principalId >< id > SML </ id >< type > RESELLERID </ type ></ principalId >< principalName > SML </ principalName >< accounts >< account >< accountDescription > Airtime </ accountDescription >< accountSpecifier >< accountId > SML </ accountId >< accountTypeId > RESELLER_AIRTIME </ accountTypeId ></ accountSpecifier >< balance >< currency > AFA </ currency >< value > 978.00000 </ value ></ balance >< creditLimit >< currency > AFA </ currency >< value > 0.00000 </ value ></ creditLimit ></ account ></ accounts >< status > Active </ status >< msisdn > 93773333985 </ msisdn ></ requestedPrincipal ></return></ ns2:requestPrincipalInformationResponse ></ soap:Body ></ soap:Envelope >  </ string >
            return CallWebService_getBalance();
        }

        public string GetMTNAPIBalance()
        {
            try
            {
                //  @"<soapenv:Envelope xmlns:soapenv=""http://schemas.xmlsoap.org/soap/envelope/""             xmlns: ext = ""http://external.interfaces.ers.seamless.com/""> <soapenv:Header />    <soapenv:Body>        <ext:requestTopup> <context>     < channel > WEBSERVICE </ channel > < clientComment > SML Top - up tnxID#" + strTransxID + "</clientComment>  <clientId>SML</clientId>     <clientReference>" + strTransxID + "</clientReference>     <clientRequestTimeout>5000</clientRequestTimeout>     <initiatorPrincipalId>         <id>SML</id>         <type>RESELLERUSER</type>         <userId>SMLUser</userId>     </initiatorPrincipalId>     <password>sml123</password> </context> <senderPrincipalId>     <id>SML</id>     <type>RESELLERID</type>     <userId>SMLUser</userId> </senderPrincipalId> <topupPrincipalId>     <id>" + strMobileNumber + "</id><type>SUBSCRIBERMSISDN</type> </topupPrincipalId> <senderAccountSpecifier>     <accountId>SML</accountId>     <accountTypeId>RESELLER_AIRTIME</accountTypeId> </senderAccountSpecifier> <topupAccountSpecifier>     <accountId>?</accountId>     <accountTypeId>SUBSCRIBER_AIRTIME</accountTypeId> </topupAccountSpecifier> <productId>2029</productId> <amount>     <currency>AFA</currency>     <value>" + dblAmount + "</value> </amount></ext:requestTopup></soapenv:Body></soapenv:Envelope>";


                //  var strResponseBalance = @"<soap:Envelope xmlns:soap=""http://schemas.xmlsoap.org/soap/envelope/""><soap:Body><ns2:requestPrincipalInformationResponse xmlns:ns2=""http://external.interfaces.ers.seamless.com/""><return><ersReference>2020040212470101701000104</ersReference><resultCode>0</resultCode><resultDescription>SUCCESS</resultDescription><requestedPrincipal><principalId><id>SML</id><type>RESELLERID</type></principalId><principalName>SML</principalName><accounts><account><accountDescription>Airtime</accountDescription><accountSpecifier><accountId>SML</accountId><accountTypeId>RESELLER_AIRTIME</accountTypeId></accountSpecifier><balance><currency>AFA</currency><value>877.00000</value></balance><creditLimit><currency>AFA</currency><value>0.00000</value></creditLimit></account></accounts><status>Active</status><msisdn>93773333985</msisdn></requestedPrincipal></return></ns2:requestPrincipalInformationResponse></soap:Body></soap:Envelope>";


                var strResponseBalance = CallWebService_getBalance();
                string strBalance = "0";
                //XDocument xml = XDocument.Parse(strResponseBalance);
                //var soapResponse = xml.Descendants().Where(x => x.Name.LocalName == "return").Select(x => new BalanceSoapResponse()
                //{
                //    ersReference = (string)x.Element(x.Name.Namespace + "ersReference"),
                //    resultCode = (string)x.Element(x.Name.Namespace + "resultCode"),
                //    resultDescription = (string)x.Element(x.Name.Namespace + "resultDescription"),
                //    requestedPrincipal = (string)x.Element(x.Name.Namespace + "requestedPrincipal")                
                //}).FirstOrDefault();


                var doc = new XmlDocument();
                doc.LoadXml(strResponseBalance);
                strBalance = doc.GetElementsByTagName("value")[0].InnerText;
                return strBalance;
            }
            catch (Exception)
            {
                return "0";
            }
        }
        public class BalanceSoapResponse
        {
            public string ersReference { get; set; }
            public string resultCode { get; set; }
            public string resultDescription { get; set; }

            public string requestedPrincipal { get; set; }

            public string value { get; set; }


        }

        public string MTNAPITopup(string intMTNID, string strTransxID, string strMobileNumber, double dblAmount)
        {
          

            if (strMobileNumber.Length == 9)
            {
                strMobileNumber = "93" + strMobileNumber;               
            }


            // <soap:Envelope xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/"><soap:Body><ns2:requestTopupResponse xmlns:ns2="http://external.interfaces.ers.seamless.com/"><return><ersReference>2020040111293091201000082</ersReference><resultCode>20</resultCode><resultDescription>The given password is not correct.</resultDescription><requestedTopupAmount><currency>AFA</currency><value>1</value></requestedTopupAmount><senderPrincipal><accounts/></senderPrincipal><topupPrincipal><accounts/></topupPrincipal></return></ns2:requestTopupResponse></soap:Body></soap:Envelope>

            // <soap:Envelope xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/"><soap:Body><ns2:requestTopupResponse xmlns:ns2="http://external.interfaces.ers.seamless.com/"><return><ersReference>2020040111320629301000083</ersReference><resultCode>11</resultCode><resultDescription>You provided an amount outside the margins of the contract. Please try again with a valid amount.</resultDescription><requestedTopupAmount><currency>AFA</currency><value>1</value></requestedTopupAmount><senderPrincipal><principalId><id>SML</id><type>RESELLERID</type></principalId><principalName>SML</principalName><accounts><account><accountSpecifier><accountId>SML</accountId><accountTypeId>RESELLER_AIRTIME</accountTypeId></accountSpecifier></account></accounts><status>Active</status><msisdn>93773333985</msisdn></senderPrincipal><topupAccountSpecifier><accountId>764524139</accountId><accountTypeId>SUBSCRIBER_AIRTIME</accountTypeId></topupAccountSpecifier><topupPrincipal><principalId><id>93764524139</id><type>SUBSCRIBERMSISDN</type></principalId><principalName></principalName><accounts><account><accountSpecifier><accountId>764524139</accountId><accountTypeId>SUBSCRIBER_AIRTIME</accountTypeId></accountSpecifier></account></accounts></topupPrincipal></return></ns2:requestTopupResponse></soap:Body></soap:Envelope>

            //<soap:Envelope xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/"><soap:Body><ns2:requestTopupResponse xmlns:ns2="http://external.interfaces.ers.seamless.com/"><return><ersReference>2020040111361978801000085</ersReference><resultCode>0</resultCode><resultDescription>You topped up 10 AFA to 93764524139. Your balance is now 957 AFA. The transaction ID: 2020040111361978801000085</resultDescription><requestedTopupAmount><currency>AFA</currency><value>10</value></requestedTopupAmount><senderPrincipal><principalId><id>SML</id><type>RESELLERID</type></principalId><principalName>SML</principalName><accounts><account><accountSpecifier><accountId>SML</accountId><accountTypeId>RESELLER_AIRTIME</accountTypeId></accountSpecifier><balance><currency>AFA</currency><value>967.00000</value></balance><creditLimit><currency>AFA</currency><value>0.00000</value></creditLimit></account></accounts><status>Active</status><msisdn>93773333985</msisdn></senderPrincipal><topupAccountSpecifier><accountId>764524139</accountId><accountTypeId>SUBSCRIBER_AIRTIME</accountTypeId></topupAccountSpecifier><topupAmount><currency>AFA</currency><value>10.00</value></topupAmount><topupPrincipal><principalId><id>93764524139</id><type>SUBSCRIBERMSISDN</type></principalId><principalName></principalName><accounts><account><accountSpecifier><accountId>764524139</accountId><accountTypeId>SUBSCRIBER_AIRTIME</accountTypeId></accountSpecifier></account></accounts></topupPrincipal></return></ns2:requestTopupResponse></soap:Body></soap:Envelope>

            return CallWebService_TopUp(intMTNID, strTransxID, strMobileNumber, dblAmount);

         //   return @"<soap:Envelope xmlns:soap=""http://schemas.xmlsoap.org/soap/envelope/""><soap:Body><ns2:requestTopupResponse xmlns:ns2=""http://external.interfaces.ers.seamless.com/""><return><ersReference>2020040111361978801000085</ersReference><resultCode>0</resultCode><resultDescription>You topped up 10 AFA to 93764524139. Your balance is now 957 AFA. The transaction ID: 2020040111361978801000085</resultDescription><requestedTopupAmount><currency>AFA</currency><value>10</value></requestedTopupAmount><senderPrincipal><principalId><id>SML</id><type>RESELLERID</type></principalId><principalName>SML</principalName><accounts><account><accountSpecifier><accountId>SML</accountId><accountTypeId>RESELLER_AIRTIME</accountTypeId></accountSpecifier><balance><currency>AFA</currency><value>967.00000</value></balance><creditLimit><currency>AFA</currency><value>0.00000</value></creditLimit></account></accounts><status>Active</status><msisdn>93773333985</msisdn></senderPrincipal><topupAccountSpecifier><accountId>764524139</accountId><accountTypeId>SUBSCRIBER_AIRTIME</accountTypeId></topupAccountSpecifier><topupAmount><currency>AFA</currency><value>10.00</value></topupAmount><topupPrincipal><principalId><id>93764524139</id><type>SUBSCRIBERMSISDN</type></principalId><principalName></principalName><accounts><account><accountSpecifier><accountId>764524139</accountId><accountTypeId>SUBSCRIBER_AIRTIME</accountTypeId></accountSpecifier></account></accounts></topupPrincipal></return></ns2:requestTopupResponse></soap:Body></soap:Envelope>";
        }
        public string CallWebService_TopUp(string intMTNID, string strTransxID, string strMobileNumber, double dblAmount)
        {
            try
            {


                // Live IP 
                var _url = "http://192.168.14.66:8913/extclientproxy/service";
                var _action = "http://192.168.14.66:8913/extclientproxy/service";


                XmlDocument soapEnvelopeXml = CreateGetBalanceEnvelope_topup(intMTNID, strTransxID, strMobileNumber, dblAmount);
                HttpWebRequest webRequest = CreateWebRequest(_url, _action);
                InsertSoapEnvelopeIntoWebRequest(soapEnvelopeXml, webRequest);

                // begin async call to web request.
                IAsyncResult asyncResult = webRequest.BeginGetResponse(null, null);

                // suspend this thread until call is complete. You might want to
                // do something usefull here like update your UI.
                asyncResult.AsyncWaitHandle.WaitOne();

                // get the response from the completed web request.
                string soapResult;
                using (WebResponse webResponse = webRequest.EndGetResponse(asyncResult))
                {
                    using (StreamReader rd = new StreamReader(webResponse.GetResponseStream()))
                    {
                        soapResult = rd.ReadToEnd();
                    }
                    return soapResult;
                }
            }
            catch (Exception)
            {
                return "NA";

            }
        }

        public string CallWebService_getBalance()
        {
            // Testing IP 
            // var _url = " http://192.168.14.57:8913/extclientproxy/service";
            // var _action = " http://192.168.14.57:8913/extclientproxy/service";


            // Live / Production IP  192.168.1.66

            var _url = "http://192.168.14.66:8913/extclientproxy/service";
            var _action = "http://192.168.14.66:8913/extclientproxy/service";

            XmlDocument soapEnvelopeXml = CreateGetBalanceEnvelope();
            HttpWebRequest webRequest = CreateWebRequest(_url, _action);
            InsertSoapEnvelopeIntoWebRequest(soapEnvelopeXml, webRequest);

            // begin async call to web request.
            IAsyncResult asyncResult = webRequest.BeginGetResponse(null, null);

            // suspend this thread until call is complete. You might want to
            // do something usefull here like update your UI.
            asyncResult.AsyncWaitHandle.WaitOne();

            // get the response from the completed web request.
            string soapResult;
            using (WebResponse webResponse = webRequest.EndGetResponse(asyncResult))
            {
                using (StreamReader rd = new StreamReader(webResponse.GetResponseStream()))
                {
                    soapResult = rd.ReadToEnd();
                }
                return soapResult;
            }
        }

        private static HttpWebRequest CreateWebRequest(string url, string action)
        {
            HttpWebRequest webRequest = (HttpWebRequest)WebRequest.Create(url);
            webRequest.Headers.Add("SOAPAction", action);
            webRequest.ContentType = "text/xml;charset=\"utf-8\"";
            webRequest.Accept = "text/xml";
            webRequest.Method = "POST";
            return webRequest;
        }

        private static XmlDocument CreateGetBalanceEnvelope_topup(string intMTNID, string strTransxID, string strMobileNumber, double dblAmount)
        {
            XmlDocument soapEnvelopeDocument = new XmlDocument();
            //string MTNBalanceRequest = @"

            // 93764524139   

            // Testing below
            //     string MTNTopupRequest = @"<soapenv:Envelope xmlns:soapenv=""http://schemas.xmlsoap.org/soap/envelope/""
            //       xmlns:ext = ""http://external.interfaces.ers.seamless.com/""> <soapenv:Header />    <soapenv:Body>        <ext:requestTopup> <context>
            //<channel>WEBSERVICE</channel> <clientComment>SML Top-up tnxID#" + strTransxID + "</clientComment>  <clientId>SML</clientId>     <clientReference>" + strTransxID + "</clientReference>     <clientRequestTimeout>5000</clientRequestTimeout>     <initiatorPrincipalId>         <id>SML</id>         <type>RESELLERUSER</type>         <userId>SMLUser</userId>     </initiatorPrincipalId>     <password>sml123</password> </context> <senderPrincipalId>     <id>SML</id>     <type>RESELLERID</type>     <userId>SMLUser</userId> </senderPrincipalId> <topupPrincipalId>     <id>" + strMobileNumber + "</id><type>SUBSCRIBERMSISDN</type> </topupPrincipalId> <senderAccountSpecifier>     <accountId>SML</accountId>     <accountTypeId>RESELLER_AIRTIME</accountTypeId> </senderAccountSpecifier> <topupAccountSpecifier>     <accountId>?</accountId>     <accountTypeId>SUBSCRIBER_AIRTIME</accountTypeId> </topupAccountSpecifier> <productId>2029</productId> <amount>     <currency>AFA</currency>     <value>" + dblAmount + "</value> </amount></ext:requestTopup></soapenv:Body></soapenv:Envelope>";



            //  Live below

            //    string MTNTopupRequest = @"<soapenv:Envelope xmlns:soapenv=""http://schemas.xmlsoap.org/soap/envelope/"" xmlns:ext=""http://external.interfaces.ers.seamless.com/""> <soapenv:Header />    <soapenv:Body>        <ext:requestTopup> <context>
         //   <channel>WEBSERVICE</channel> <clientComment>SML Top-up tnxID#" + strTransxID + "</clientComment>  <clientId>SML</clientId>     <clientReference>" + strTransxID + "</clientReference>     <clientRequestTimeout>5000</clientRequestTimeout>     <initiatorPrincipalId>         <id>SML</id>         <type>RESELLERUSER</type>         <userId>SMLUser</userId>     </initiatorPrincipalId>     <password>SMLU@1220</password> </context> <senderPrincipalId>     <id>SML</id>     <type>RESELLERID</type>     <userId>SMLUser</userId> </senderPrincipalId> <topupPrincipalId> <id>" + strMobileNumber + "</id><type>SUBSCRIBERMSISDN</type> </topupPrincipalId> <senderAccountSpecifier>     <accountId>SML</accountId>     <accountTypeId>RESELLER_AIRTIME</accountTypeId> </senderAccountSpecifier> <topupAccountSpecifier>     <accountId>?</accountId>     <accountTypeId>SUBSCRIBER_AIRTIME</accountTypeId> </topupAccountSpecifier> <productId>2029</productId> <amount>     <currency>AFA</currency>     <value>" + dblAmount + "</value> </amount></ext:requestTopup></soapenv:Body></soapenv:Envelope>"; 
            // 22323544 93773333598

         

            //  Live 2 Static
             string MTNTopupRequest = @"<soapenv:Envelope xmlns:soapenv=""http://schemas.xmlsoap.org/soap/envelope/"" xmlns:ext=""http://external.interfaces.ers.seamless.com/""><soapenv:Header /><soapenv:Body><ext:requestTopup><context><channel>WEBSERVICE</channel><clientComment>Top-up #" + strTransxID + "</clientComment><clientId>SML</clientId><clientReference>" + strTransxID + "</clientReference><clientRequestTimeout>5000</clientRequestTimeout><initiatorPrincipalId><id>SML</id><type>RESELLERUSER</type><userId>SMLUser</userId></initiatorPrincipalId><password>SMLU@1220</password></context><senderPrincipalId><id>SML</id><type>RESELLERID</type><userId>SMLUser</userId></senderPrincipalId><topupPrincipalId><id>" + strMobileNumber + "</id><type>SUBSCRIBERMSISDN</type></topupPrincipalId><senderAccountSpecifier><accountId>SML</accountId><accountTypeId>RESELLER_AIRTIME</accountTypeId></senderAccountSpecifier><topupAccountSpecifier><accountId>?</accountId><accountTypeId>SUBSCRIBER_AIRTIME</accountTypeId></topupAccountSpecifier><productId>2029</productId><amount><currency>AFA</currency><value>" + dblAmount + "</value></amount></ext:requestTopup></soapenv:Body></soapenv:Envelope>";

            try
            {
                ODBC objODBC = new ODBC();
                 objODBC.executeNonQuery("update er_mtn_topup set topupRequest='" + MTNTopupRequest + "' where id='" + intMTNID + "'");

               // objODBC.executeNonQuery("INSERT INTO `temp`(`data`,`request`) VALUES ('" + MTNTopupRequest + "','" + MTNTopupRequest + "')");
            }
            catch (Exception)
            {

            }

            soapEnvelopeDocument.LoadXml(MTNTopupRequest);
            return soapEnvelopeDocument;
        }

        private static XmlDocument CreateGetBalanceEnvelope()
        {
            XmlDocument soapEnvelopeDocument = new XmlDocument();


            // Testing below
            /*   string MTNBalanceRequest = @"<soapenv:Envelope xmlns:soapenv=""http://schemas.xmlsoap.org/soap/envelope/""
                  xmlns:ext = ""http://external.interfaces.ers.seamless.com/"">     <soapenv:Header />    <soapenv:Body>        <ext:requestPrincipalInformation>            <!--Optional:-->            <context>                <!--Optional:-->
                      <channel>WSClient</channel>                <!--Optional:-->                <clientComment>test</clientComment>                <!--Optional:-->                <clientId>ERS</clientId>                <!--Optional:-->                <clientReference>1234</clientReference>               <clientRequestTimeout>500</clientRequestTimeout>                <!--Optional:-->
                      <initiatorPrincipalId>                    <!--Optional:-->                    <id>SML</id>                    <!--Optional:-->
                          <type>RESELLERUSER</type>                    <!--Optional:-->                    <userId>SMLUser</userId>                </initiatorPrincipalId>                <!--Optional:-->                <password>sml123</password>            </context>            <!--Optional:-->            <principalId>                <!--Optional:-->                <id>sml</id>               <!--Optional:-->                <type>RESELLERUSER</type>                <!--Optional:-->                <userId>SMLUser</userId>            </principalId>        </ext:requestPrincipalInformation>    </soapenv:Body> </soapenv:Envelope>";

               */

            // Live or Production
            string MTNBalanceRequest = @"<soapenv:Envelope xmlns:soapenv=""http://schemas.xmlsoap.org/soap/envelope/""
                xmlns:ext = ""http://external.interfaces.ers.seamless.com/"">     <soapenv:Header />    <soapenv:Body>        <ext:requestPrincipalInformation>            <!--Optional:-->            <context>                <!--Optional:-->
                    <channel>WSClient</channel>                <!--Optional:-->                <clientComment>test</clientComment>                <!--Optional:-->                <clientId>ERS</clientId>                <!--Optional:-->                <clientReference>1234</clientReference>               <clientRequestTimeout>500</clientRequestTimeout>                <!--Optional:-->
                    <initiatorPrincipalId>                    <!--Optional:-->                    <id>SML</id>                    <!--Optional:-->
                        <type>RESELLERUSER</type>                    <!--Optional:-->                    <userId>SMLUser</userId>                </initiatorPrincipalId>                <!--Optional:-->                <password>SMLU@1220</password>            </context>            <!--Optional:-->            <principalId>                <!--Optional:-->                <id>sml</id>               <!--Optional:-->                <type>RESELLERUSER</type>                <!--Optional:-->                <userId>SMLUser</userId>            </principalId>        </ext:requestPrincipalInformation>    </soapenv:Body> </soapenv:Envelope>";


            // Live or Production 2
          /*  string MTNBalanceRequest = @"<soapenv:Envelope xmlns:soapenv=""http://schemas.xmlsoap.org/soap/envelope/""
            xmlns: ext = ""http://external.interfaces.ers.seamless.com/""> <soapenv:Header />    <soapenv:Body>        <ext:requestPrincipalInformation>            <!--Optional:-->            <context>                <!--Optional:-->
                    <channel>WSClient</channel>                <!--Optional:-->                <clientComment>test</clientComment>                <!--Optional:-->                <clientId>ERS</clientId>                <!--Optional:-->                <clientReference>1234</clientReference>               <clientRequestTimeout>500</clientRequestTimeout>                <!--Optional:-->
                    <initiatorPrincipalId>                    <!--Optional:-->                    <id>SML</id>                    <!--Optional:-->
                        <type>RESELLERUSER</type>                    <!--Optional:-->                    <userId>SMLAgent</userId>                </initiatorPrincipalId>                <!--Optional:-->                <password>#SML&CH#S</password>            </context>            <!--Optional:-->            <principalId>                <!--Optional:-->                <id>sml</id>               <!--Optional:-->                <type>RESELLERUSER</type>                <!--Optional:-->                <userId>SMLUser</userId>            </principalId>        </ext:requestPrincipalInformation>    </soapenv:Body> </soapenv:Envelope>"; */

            soapEnvelopeDocument.LoadXml(MTNBalanceRequest);
            return soapEnvelopeDocument;
        }

        private static void InsertSoapEnvelopeIntoWebRequest(XmlDocument soapEnvelopeXml, HttpWebRequest webRequest)
        {
            using (Stream stream = webRequest.GetRequestStream())
            {
                soapEnvelopeXml.Save(stream);
            }
        }
    }
}
