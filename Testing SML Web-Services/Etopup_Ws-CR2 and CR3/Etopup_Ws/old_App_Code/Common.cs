﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Security;
using System.Text;
using System.Threading.Tasks;
using Microsoft.VisualBasic;
using System.Net.Mail;
using System.Xml;
using System.Data;
using System.Web;
using System.Web.Services;
using System.Collections;

namespace Etopup_Ws.old_App_Code
{
    public class Common
    {
        // **************Get Time of a Specific Time Zone**********************************
        // eg. strTimeZone = "India Standard Time"
        private ODBC obj = new ODBC();
        public DateTime getTime(string strTimeZone)
        {
            DateTime timeUtc = DateTime.UtcNow;

            return DateTime.Now;
        }

        // *************************************************************************************************

        public string getCurDateTimeString()
        {
            DateTime dtcurDateTime = getTime("India Standard Time");
            string strcurDateTime = dtcurDateTime.ToString("yyyy-MM-dd HH:mm:ss");
            return strcurDateTime;
        }

        public string getCurDateTimeString1()
        {
            DateTime dtcurDateTime = getTime("India Standard Time");
            string strcurDateTime = dtcurDateTime.ToString("yyyy-MM-dd_HHmmss");
            return strcurDateTime;
        }

        // ******************************************************************************************************************

        public string getCurDateString()
        {
            DateTime dtcurDateTime = getTime("India Standard Time");
            string strcurDate = dtcurDateTime.ToString("yyyy-MM-dd");
            return strcurDate;
        }

        public string GenrateRandomIntegerString()
        {
            string allowedChars = "";
            allowedChars = "1,2,3,4,5,6,7,8,9,0";
            char[] sep = new[] { ',' };
            string[] arr = allowedChars.Split(sep);
            string passwordString = "";
            string temp = "";
            Random rand = new Random();
            for (int i = 0; i <= Convert.ToInt32(4) - 1; i++)
            {
                temp = arr[rand.Next(0, arr.Length)];
                passwordString += temp;
            }

            return passwordString;
        }

        public string GenrateRandomIntegerString1()
        {
            string allowedChars = "";
            allowedChars = "1,2,3,4,5,6,7,8,9,0";
            char[] sep = new[] { ',' };
            string[] arr = allowedChars.Split(sep);
            string passwordString = "";
            string temp = "";
            Random rand = new Random();
            for (int i = 0; i <= Convert.ToInt32(8) - 1; i++)
            {
                temp = arr[rand.Next(0, arr.Length)];
                passwordString += temp;
            }

            return passwordString;
        }


        public ArrayList Get_ArrayLlist(string strquery)
        {
            ODBC objODBC = new ODBC();
            DataSet ds = new DataSet();
            ArrayList arrUserParams = new ArrayList();

            try
            {
                ds = objODBC.getDataSet(strquery);
                if (ds.Tables[0].Rows.Count > 0)
                {
                    arrUserParams.Add("TRUE");

                    arrUserParams.Add((ds.Tables[0].Rows.Count).ToString());
                    int col_count = ds.Tables[0].Columns.Count;

                    arrUserParams.Add(col_count);

                    System.Data.DataTable firstTable = ds.Tables[0];

                    for (int i = 0; i <= firstTable.Rows.Count - 1; i++)
                    {
                        for (int j = 0; j <= firstTable.Columns.Count - 1; j++)

                            arrUserParams.Add(firstTable.Rows[i][j].ToString());
                    }
                }
                else
                    arrUserParams.Add("FALSE");

                return arrUserParams;
            }
            catch (Exception ex)
            {
                arrUserParams.Add(ex.Message.ToString());

                return arrUserParams;
            }
        }


       

        // **** Send SMS Function for Transactional SMS ****
        public string SendTransactionSMS(string strSID, string strCampaign, string strMessage, string strNumber)
        {
            try
            {
                string authKey = "3928AJzMC51k5359f3c5";

                string strMSG = HttpUtility.UrlEncode(strMessage);

                // Dim strStrigNumber As String = ReturnNumberString(strNumber)

                // Dim srtData As String = "<MESSAGE>" _
                // & " <AUTHKEY>" & authKey & "</AUTHKEY>" _
                // & "<SENDER>" & strSID & "</SENDER>" _
                // & "<ROUTE>4</ROUTE>" _
                // & "<CAMPAIGN>" & strCampaign & "</CAMPAIGN>" _
                // & "<SMS TEXT=""" & strMSG & """ >" _
                // & "<ADDRESS TO=""" & strNumber & """></ADDRESS>" _
                // & "</SMS>" _
                // & "</MESSAGE>"

                string sURL;

                sURL = "http://panel.indoresms.com/api/sendhttp.php?authkey=3928AJzMC51k5359f3c5&mobiles=" + strNumber + "&message=" + strMessage + "&sender=" + strSID + "&route=4";

                System.Net.WebRequest request;

                request = System.Net.WebRequest.Create(sURL);

                System.Net.WebResponse resp = request.GetResponse();

                System.IO.StreamReader reader = new System.IO.StreamReader(resp.GetResponseStream());
                string responseString = reader.ReadToEnd();

                reader.Close();
                resp.Close();

                return responseString;
            }
            catch (Exception ex)
            {
                return "False";
            }
        }

        private string PopulateBody(string LeadName, string UserName, string Password, string strTempPath)
        {
            try
            {
                string body = string.Empty;
                StreamReader reader = new StreamReader(HttpContext.Current.Server.MapPath(strTempPath));
                body = reader.ReadToEnd();
                reader.Close();
                body = body.Replace("{LeadName}", LeadName);
                body = body.Replace("{UserName}", UserName);
                body = body.Replace("{Password}", Password);
                return body;
            }
            catch (Exception ex)
            {
                return "False";
            }
        }

        public string PopulateSMSBody(string LeadName, string UserName, string Password)
        {
            try
            {
                string body = string.Empty;
                StreamReader reader = new StreamReader(HttpContext.Current.Server.MapPath("SMSTemplates/Welcome_SMS_Client.txt"));
                body = reader.ReadToEnd();
                reader.Close();
                body = body.Replace("{LeadName}", LeadName);
                body = body.Replace("{UserName}", UserName);
                body = body.Replace("{Password}", Password);
                return body;
            }
            catch (Exception ex)
            {
                return "False";
            }
        }


      
        // **** Generate Random String for 10 Digit *****
        public string GenrateRandomString()
        {
            string allowedChars = "";
            allowedChars = "a,b,c,d,e,f,g,h,i,j,k,l,m,n,o,p,q,r,s,t,u,v,w,x,y,z,";
            allowedChars += "A,B,C,D,E,F,G,H,I,J,K,L,M,N,O,P,Q,R,S,T,U,V,W,X,Y,Z,";
            allowedChars += "1,2,3,4,5,6,7,8,9,0";
            char[] sep = new[] { ',' };
            string[] arr = allowedChars.Split(sep);
            string passwordString = "";
            string temp = "";
            Random rand = new Random();
            for (int i = 0; i <= Convert.ToInt32(10) - 1; i++)
            {
                temp = arr[rand.Next(0, arr.Length)];
                passwordString += temp;
            }

            return passwordString;
        }

        public string GenrateRandomAuthString()
        {
            string allowedChars = "";
            allowedChars = "a,b,c,d,e,f,g,h,i,j,k,l,m,n,o,p,q,r,s,t,u,v,w,x,y,z,";
            allowedChars += "A,B,C,D,E,F,G,H,I,J,K,L,M,N,O,P,Q,R,S,T,U,V,W,X,Y,Z,";
            allowedChars += "1,2,3,4,5,6,7,8,9,0";
            char[] sep = new[] { ',' };
            string[] arr = allowedChars.Split(sep);
            string passwordString = "";
            string temp = "";
            Random rand = new Random();
            for (int i = 0; i <= Convert.ToInt32(16) - 1; i++)
            {
                temp = arr[rand.Next(0, arr.Length)];
                passwordString += temp;
            }

            return passwordString;
        }


        public DataSet Get_datalist(string strquery)
        {
            ODBC objODBC = new ODBC();
            DataSet ds = new DataSet();
            ArrayList arrUserParams = new ArrayList();

            try
            {
                ds = objODBC.getDataSet(strquery);
                if (ds.Tables[0].Rows.Count > 0)
                    return ds;
                else
                {
                }

                return ds;
            }
            catch (Exception ex)
            {
                return ds;

                return ds;
            }
        }
    }
}