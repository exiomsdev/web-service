﻿using Microsoft.VisualBasic;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Web;
using System.Data.Odbc;
using System.IO;
using System.Security.Cryptography;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using MySql.Data;
using Etopup_Ws.old_App_Code;

namespace Etopup_Ws.old_App_Code 
{
    public class ODBC
    {
         public OdbcConnection con = new OdbcConnection();

        public OdbcCommand cmd = new OdbcCommand();

        public string strCon;
        private string _path;
        

        public static System.Exception Lasterror_page = new System.Exception();
        // if Var_connectiontype is not supplied then default value "P" is supplied for pooled connection
        public void openAGet(ref OdbcConnection conn)
        {
            try
            {
                //strCon = ConfigurationSettings.AppSettings("PooledConnectionString")
                strCon = System.Configuration.ConfigurationManager.AppSettings["PooledConnectionString"];
                conn = new OdbcConnection(strCon);
                conn.Open();
            }
            catch (OdbcException odbcEx)
            {
                throw new Exception(odbcEx.Message);
            }
        }

        // if Var_connectiontype is not supplied then default value "P" is supplied for pooled connection
        public void getConn(string Var_connectiontype)
        {

            try
            {
                if (Var_connectiontype == "U")
                {
                    // strCon = ConfigurationSettings.AppSettings("UnPooledConnectionString")

                    strCon = System.Configuration.ConfigurationManager.AppSettings["UnPooledConnectionString"];
                }
                else
                {
                    //strCon = ConfigurationSettings.AppSettings("PooledConnectionString")
                    strCon = System.Configuration.ConfigurationManager.AppSettings["PooledConnectionString"];
                }
                con = new OdbcConnection(strCon);
            }
            catch (OdbcException odbcEx)
            {
                throw new Exception(odbcEx.Message);
            }
        }


        public void OpenConn()
        {
            string Var_errmsg = null;
            if ((con.State != ConnectionState.Open))
            {
                try
                {
                    con.Open();
                }
                catch (OdbcException odbcEx)
                {
                    throw new Exception(odbcEx.Message);
                }
                catch (Exception appEx)
                {
                    Var_errmsg = appEx.Message.ToString();
                    if (Var_errmsg == "Timeout expired.  The timeout period elapsed prior to obtaining a connection from the pool.  This may have occurred because all pooled connections were in use and max pool size was reached.")
                    {
                        getConn("U");
                        //"U" parameter is being passed for unpooled connections in case the connection pooling limit has been reached
                        OpenConn();
                    }
                }
            }
        }

        //Example
        //clsdal.getDataReader("select * from product_mstr")  
        public OdbcDataReader getDataReader(string pstrQuery)
        {
            OdbcDataReader datareader = null;
            try
            {
                getConn("P");
                OpenConn();
                cmd = new OdbcCommand();
                cmd.CommandType = CommandType.Text;
                cmd.CommandTimeout = 120;
                cmd.CommandText = pstrQuery;
                cmd.Connection = con;
                datareader = cmd.ExecuteReader();
                return datareader;
            }
            catch (OdbcException odbcEx)
            {
                throw new Exception(odbcEx.Message);
            }
            closeConn();
            datareader.Close();
            cmd.Dispose();
        }

        //Example
        //Dim ds As DataSet = Nothing
        //ds = CType(clsdal.getDataSet("select * from product_mstr"), DataSet)
        //GridView1.DataSource = ds
        //GridView1.DataBind()
        public DataSet getDataSet(string pstrQuery)
        {
            DataSet ds = null;
            OdbcDataAdapter dapt = null;
            try
            {
                getConn("P");
                OpenConn();
                dapt = new OdbcDataAdapter(pstrQuery, con);
                ds = new DataSet();
                dapt.Fill(ds);
                return (ds);
            }
            catch (OdbcException odbcEx)
            {
                throw new Exception(odbcEx.Message);
            }
            finally
            {
                closeConn();
                dapt.Dispose();
                ds.Dispose();
            }
        }

        //Example
        //Dim ds As DataSet = Nothing
        //ds = CType(clsdal.getDataSet("select * from product_mstr", "productmstr"), DataSet)
        //GridView1.DataSource = ds.Tables("productmstr").DefaultView
        //GridView1.DataBind()
        public DataSet getDataSet(string pstrQuery, string pstrtable)
        {
            DataSet ds = null;
            OdbcDataAdapter dapt = null;
            try
            {
                getConn("P");
                OpenConn();
                dapt = new OdbcDataAdapter(pstrQuery, con);
                ds = new DataSet();
                dapt.Fill(ds, pstrtable);
                return (ds);
            }
            catch (OdbcException odbcEx)
            {
                throw new Exception(odbcEx.Message);
            }
            finally
            {
                closeConn();
                dapt.Dispose();
                ds.Dispose();
            }
        }

        //Example
        //Dim int As Integer
        //Int = clsdal.executeScalar("select * from product_mstr")
        public int executeScalar_int(string pstrQuery)
        {
            OdbcCommand cmd = null;
            int retval = 0;
            try
            {
                getConn("P");
                OpenConn();
                cmd = new OdbcCommand(pstrQuery, con);
                retval = Int32.Parse(cmd.ExecuteScalar().ToString());
                return retval;
            }
            catch (OdbcException odbcEx)
            {
                throw new Exception(odbcEx.Message);
            }
            finally
            {
                closeConn();
                cmd.Dispose();
            }
        }

        public double executeScalar_dbl(string pstrQuery)
        {
            OdbcCommand cmd = null;
            double retval = 0;
            try
            {
                getConn("P");
                OpenConn();
                cmd = new OdbcCommand(pstrQuery, con);
                retval = double.Parse(cmd.ExecuteScalar().ToString());
                return retval;
            }
            catch (OdbcException odbcEx)
            {
                throw new Exception(odbcEx.Message);
            }
            finally
            {
                closeConn();
                cmd.Dispose();
            }
        }

        public string executeScalar_str(string pstrQuery)
        {
            OdbcCommand cmd = null;
            string retval = null;
            try
            {
                getConn("P");
                OpenConn();
                cmd = new OdbcCommand(pstrQuery, con);
                retval = cmd.ExecuteScalar().ToString();
                return retval;
            }
            catch (OdbcException odbcEx)
            {
                throw new Exception(odbcEx.Message);
            }
            finally
            {
                closeConn();
                cmd.Dispose();
            }
        }

        //Example
        //int = clsdal.executeNonQuery("insert into temp1 values(1,2,3,4,5)")
        //MsgBox(int & " seccussfully inserted")
        public int executeNonQuery(string pstrQuery)
        {
            OdbcCommand cmd = null;
            int retval = 0;
            try
            {
                getConn("P");
                OpenConn();
                cmd = new OdbcCommand(pstrQuery, con);
                retval = cmd.ExecuteNonQuery();
                return retval;
            }
            catch (OdbcException odbcEx)
            {
                throw new Exception(odbcEx.Message);
            }
            finally
            {
                closeConn();
                cmd.Dispose();
            }
        }

        public void closeConn()
        {
            try
            {
                if (con.State == ConnectionState.Open)
                {
                    con.Close();
                    con.Dispose();
                }
            }
            catch (OdbcException odbcEx)
            {
                throw new Exception(odbcEx.Message);
            }
        }

        //Example
        //clsdal.getMultiTableDataSet("select * from product_mstr", "prom", ds)
        //clsdal.getMultiTableDataSet("select * from product_mstr_mak", "promm", ds)
        //clsdal.getMultiTableDataSet("select * from status_mstr", "stam", ds)
        //clsdal.getMultiTableDataSet("select * from status_mstr_mak", "stamm", ds)
        public DataSet getMultiTableDataSet(string pstrQuery, string pstrtable, DataSet pstrDataSet = null)
        {
            OdbcDataAdapter adpt = null;
            DataSet ds = new DataSet();
            try
            {
                getConn("P");
                OpenConn();
                adpt = new OdbcDataAdapter(pstrQuery, con);
                adpt.SelectCommand = new OdbcCommand(pstrQuery, con);
                if ((pstrDataSet == null) == true)
                {
                    ds = new DataSet();
                    adpt.Fill(ds, pstrtable);
                }
                else
                {
                    adpt.Fill(pstrDataSet, pstrtable);
                    ds = pstrDataSet;
                }
                return (ds);
            }
            catch (OdbcException odbcEx)
            {
                throw new Exception(odbcEx.Message);
            }
            finally
            {
                closeConn();
                adpt.Dispose();
                ds.Dispose();
            }
        }

        public void GetPath(string path)
        {
            _path = path;
        }

        public void GetPathHome(string path)
        {
            _path = path;
        }

        //Public Function IsAuthenticated(ByVal domain As String, ByVal username As String, ByVal pwd As String) As Boolean
        //    Dim domainAndUsername As String = domain + "\" + username + ""

        //    Dim entry As New DirectoryEntry(_path, domainAndUsername, pwd)

        //    Try
        //        ' Bind to the native AdsObject to force authentication.

        //        Dim obj As [Object] = entry.NativeObject
        //        Dim search As New DirectorySearcher(entry)
        //        search.Filter = "(SAMAccountName=" + username + ")"
        //        search.PropertiesToLoad.Add("cn")
        //        Dim result As SearchResult = search.FindOne()
        //        If IsNothing(result) = True Then
        //            Return False
        //        End If
        //        _path = result.Path
        //        _filterAttribute = CType(result.Properties("cn")(0), [String])
        //    Catch ex As Exception
        //        Throw New Exception("error_page authenticating user. " + ex.Message)
        //    End Try
        //    Return True


        //    ' Update the new path to the user in the directory

        //End Function 'IsAuthenticated


        public static void ConvertXlsNew(GridView ds, HttpResponse response)
        {
            string style = null;
            style = "<style> .text { mso-number-format:\\@; } </style> ";
            //first let's clean up the response.object
            response.Clear();
            response.Charset = "";

            response.AddHeader("content-disposition", "attachment;filename=Download.xls");
            //response.Cache.SetCacheability(HttpCacheability.NoCache)

            //set the response mime type for excel
            response.ContentType = "application/vnd.ms-excel";
            //create a string writer
            System.IO.StringWriter stringWrite = new System.IO.StringWriter();
            //create an htmltextwriter which uses the stringwriter
            System.Web.UI.HtmlTextWriter htmlWrite = new System.Web.UI.HtmlTextWriter(stringWrite);
            //instantiate a datagrid
            //Dim dg As New DataGrid
            //set the datagrid datasource to the dataset passed in
            //dg.DataSource = ds.Tables(0)
            //bind the datagrid
            //dg.DataBind()
            //tell the datagrid to render itself to our htmltextwriter
            HtmlForm HtmlForm = new HtmlForm();
            ds.Parent.Controls.Add(HtmlForm);
            HtmlForm.Attributes["runat"] = "server";
            HtmlForm.Controls.Add(ds);
            HtmlForm.RenderControl(htmlWrite);
            //all that's left is to output the html
            response.Write(style);
            response.Write(stringWrite);
            response.End();
        }

        public static void ConvertXls(DataSet ds, HttpResponse response)
        {
            //first let's clean up the response.object
            response.Clear();
            response.Charset = "";

            response.AddHeader("content-disposition", "attachment;filename=Download.xls");
            //response.Cache.SetCacheability(HttpCacheability.NoCache)

            //set the response mime type for excel
            response.ContentType = "application/vnd.ms-excel";
            //create a string writer
            System.IO.StringWriter stringWrite = new System.IO.StringWriter();
            //create an htmltextwriter which uses the stringwriter
            System.Web.UI.HtmlTextWriter htmlWrite = new System.Web.UI.HtmlTextWriter(stringWrite);
            //instantiate a datagrid
            DataGrid dg = new DataGrid();
            //set the datagrid datasource to the dataset passed in
            dg.DataSource = ds.Tables[0];
            //bind the datagrid
            dg.DataBind();
            //tell the datagrid to render itself to our htmltextwriter
            dg.RenderControl(htmlWrite);
            //all that's left is to output the html
            response.Write(stringWrite);
            response.End();
        }

        //Bhushan
        //Get DataTable
        //Create Object of This Class like OBDC obj=new ODBC Then Use It
        //Eg :- DataTable dt=obj.getDataTable("Select * From Customer");
        public DataTable getDataTable(string pstrQuery)
        {
            DataTable dt = null;
            OdbcDataAdapter dapt = null;
            try
            {
                getConn("p");
                OpenConn();
                dapt = new OdbcDataAdapter(pstrQuery, con);
                dt = new DataTable();
                dapt.Fill(dt);
                return (dt);
            }
            catch (OdbcException odbcEx)
            {
                throw new Exception(odbcEx.Message);
            }
            finally
            {
                closeConn();
                dapt.Dispose();
                dt.Dispose();
            }
        }

        public string GetStringForSQL(string inputSQL)
        {
            return inputSQL.Replace("'", "''");
        }

        public DataSet getresult(string stresult)
        {
            System.Data.DataSet ds = new System.Data.DataSet();
            try
            {
                DataTable dt = new DataTable("Data");
                dt.Columns.Add(new DataColumn("Status", typeof(string)));
                dt.Columns.Add(new DataColumn("Result", typeof(string)));

                DataRow dr = dt.NewRow();
                dr[0] = "False";
                dr[1] = stresult;
                dt.Rows.Add(dr);
                ds.Tables.Add(dt);
                return ds;
            }
            catch (Exception ex)
            {
                DataTable dt = new DataTable("Data");
                dt.Columns.Add(new DataColumn("Status", typeof(string)));
                dt.Columns.Add(new DataColumn("Result", typeof(string)));

                DataRow dr = dt.NewRow();
                dr[0] = "False";
                dr[1] = ex.Message.ToString();
                dt.Rows.Add(dr);
                ds.Tables.Add(dt);
                return ds;
            }
            finally
            {
                ds.Dispose(); 
            }
        }

        public DataSet Report(string strQuery)
        {
           
            System.Data.DataSet ds = new System.Data.DataSet();

            try
            {
                ds = getDataSet(strQuery);

                if (ds.Tables[0].Rows.Count > 0)
                {
                    return ds;
                }
                else
                {
                    return getresult("Records not found !!!");
                }

            }
            catch (Exception ex)
            {
                return getresult(ex.Message.ToString());
            }
            finally
            {
                ds.Dispose();
            }
        }
    }
}