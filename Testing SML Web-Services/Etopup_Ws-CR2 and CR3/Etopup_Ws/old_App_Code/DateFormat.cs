﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Etopup_Ws.old_App_Code
{
    public class DateFormat
    {
        public string getCurDateTimeString()
        {
            DateTime dtcurDateTime = getTime("Afghanistan standard time");
            string strcurDateTime = dtcurDateTime.ToString("yyyy-MM-dd HH:mm:ss");
            return strcurDateTime;
        }

        public string getCurDateString()
        {
            DateTime dtcurDateTime = getTime("Afghanistan standard time");
            string strcurDateTime = dtcurDateTime.ToString("yyyy-MM-dd");
            return strcurDateTime;
        }

        public DateTime getTime(string strTimeZone)
        {
            DateTime timeUtc = DateTime.UtcNow;
            return DateTime.Now;
        }


        public string getTimeString()
        {
            DateTime dtcurDateTime = getTime("Afghanistan standard time");
            string strcurDateTime = dtcurDateTime.ToString("HH:mm:ss");
            return strcurDateTime;
        }

        public string getYearString()
        {
            DateTime dtcurDateTime = getTime("Afghanistan standard time");
            string strcurDateTime = dtcurDateTime.ToString("yyyy");
            return strcurDateTime;
        }

        public string getCurDateTime_Dayanamic_format(string strDateFormat)
        {
            DateTime dtcurDateTime = getTime("Afghanistan standard time");
            string strcurDateTime = dtcurDateTime.ToString(strDateFormat);
            return strcurDateTime;
        }
    }
}