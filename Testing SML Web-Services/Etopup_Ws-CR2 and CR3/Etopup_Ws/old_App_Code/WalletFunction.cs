﻿using Microsoft.VisualBasic;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Web;
using System.Data.Odbc;
using System.IO;
using System.Security.Cryptography;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using MySql.Data;
using Etopup_Ws.old_App_Code;

namespace Etopup_Ws.old_App_Code
{


    public class WalletFunction
    {

        ODBC objODBC = new ODBC();
        DataSet ds = new DataSet();

        public double GetWalletAmount(int intUserID)
        {
            double amount = 0;
            try
            {
                amount = objODBC.executeScalar_dbl("SELECT ex_wallet FROM er_wallet WHERE userid=" + intUserID);
                return amount;
            }
            catch (Exception ex)
            {
                return 0.0;
            }
        }
    }
}