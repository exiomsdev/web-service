﻿Imports Microsoft.VisualBasic
Namespace Etopup_Ws.old_App_Code
    Public Class clsMailTemplate

        Public Function getRegisterMessage(strUsername As [String], strUserID As String, strPassword As String, strEmail As String, strTransPass As String) As String

            Dim strMailString As String = System.IO.File.ReadAllText(System.Web.HttpContext.Current.Server.MapPath("Template/register.html"))


            strMailString = strMailString.Replace("&&username&&", strUsername)
            strMailString = strMailString.Replace("&&userID&&", strUserID)
            strMailString = strMailString.Replace("&&pass&&", strPassword)
            strMailString = strMailString.Replace("&&email&&", strEmail)
            strMailString = strMailString.Replace("&&transPass&&", strTransPass)
            Return strMailString
        End Function
        Public Function getInvestmentNotificationMessage(strUsername As [String]) As String

            Dim strMailString As String = System.IO.File.ReadAllText(System.Web.HttpContext.Current.Server.MapPath("Template/InvestmentNotification.html"))


            strMailString = strMailString.Replace("&&UserName&&", strUsername)
            Return strMailString
        End Function
        Public Function getTransPasswordMessage(strUsername As [String], strPass As String) As String

            Dim strMailString As String = System.IO.File.ReadAllText(System.Web.HttpContext.Current.Server.MapPath("../../Template/transPassword.html"))


            strMailString = strMailString.Replace("&&username&&", strUsername)
            strMailString = strMailString.Replace("&&strPassword&&", strPass)

            Return strMailString
        End Function
        Public Function getTransPasswordMessage1(strUsername As [String], strPass As String) As String

            Dim strMailString As String = System.IO.File.ReadAllText(System.Web.HttpContext.Current.Server.MapPath("Template/transPassword.html"))


            strMailString = strMailString.Replace("&&username&&", strUsername)
            strMailString = strMailString.Replace("&&strPassword&&", strPass)

            Return strMailString
        End Function
        Public Function getEmailVerificationMessage(strUsername As [String], strLink As String) As String

            Dim strMailString As String = System.IO.File.ReadAllText(System.Web.HttpContext.Current.Server.MapPath("Template/emailVerification.html"))


            strMailString = strMailString.Replace("&&username&&", strUsername)
            strMailString = strMailString.Replace("&&link&&", strLink)

            Return strMailString
        End Function
        Public Function getFundtransferMessage(strUsername As [String], strLink As String) As String

            Dim strMailString As String = System.IO.File.ReadAllText(System.Web.HttpContext.Current.Server.MapPath("Template/emailVerification.html"))


            strMailString = strMailString.Replace("&&username&&", strUsername)
            strMailString = strMailString.Replace("&&link&&", strLink)

            Return strMailString
        End Function

        Public Function getForgotPasswordMessage(strUsername As [String], strPassword As String) As String

            Dim strMailString As String = System.IO.File.ReadAllText(System.Web.HttpContext.Current.Server.MapPath("~/Template/forgotPassword.html"))


            strMailString = strMailString.Replace("&&username&&", strUsername)
            strMailString = strMailString.Replace("&&strUserName&&", strUsername)
            strMailString = strMailString.Replace("&&strNewPassword&&", strPassword)

            Return strMailString
        End Function
    End Class
End Namespace

