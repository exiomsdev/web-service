﻿using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Text;
using System.Security.Cryptography;

/// <summary>
/// Summary description for Encryption
/// </summary>

namespace Etopup_Ws.old_App_Code
{
    public class Encryption
    {
        public Encryption()
        {
            //
            // TODO: Add constructor logic here
            //
        }
        public string EncryptQueryString(string strQueryString, string strKey)
        {
            EncryptDecryptQueryString objEDQueryString = new EncryptDecryptQueryString();
            return objEDQueryString.Encrypt(strQueryString, strKey); //the key must be only 8 digit in length
        }
        public string DecryptQueryString(string strQueryString, string strKey)
        {
            EncryptDecryptQueryString objEDQueryString = new EncryptDecryptQueryString();
            return objEDQueryString.Decrypt(strQueryString, strKey);
        }

        public string EncryptSalt(string strQueryString, string strKey)
        {
            EncryptDecryptQueryString objEDQueryString = new EncryptDecryptQueryString();
            return objEDQueryString.Encryptsalt(strQueryString, strKey); //the key must be only 8 digit in length
        }
        public string DecrypSalt(string strQueryString, string strKey)
        {
            EncryptDecryptQueryString objEDQueryString = new EncryptDecryptQueryString();
            return objEDQueryString.Decryptsalt(strQueryString, strKey);
        }

        public string Decrypt(string strReq, string strKey)
        {
            Boolean check = strReq.Contains("?");
            if (check == true)
            {
                strReq = strReq.Substring(strReq.IndexOf("?") + 1);

                if (!strReq.Equals(""))
                {
                    strReq = DecryptQueryString(strReq, strKey);  // decrypting whole query string
                    string[] arrMsgs = strReq.Split('&');  // separating all query string parameters in case of multiple parameter passed
                    string[] arrIndMsg;
                    string roll = "";
                    //you can add loops or so to get the values out of the query string...

                    arrIndMsg = arrMsgs[0].Split('='); //getting roll
                    roll = arrIndMsg[1].ToString().Trim();
                    return roll;

                    // lblRoll.Text = roll;

                }
            }
            return null;
        }

        public string DecryptSearch(string strReq, string strKey)
        {
            Boolean check = strReq.Contains("?");
            if (check == true)
            {
                strReq = strReq.Substring(strReq.IndexOf("?") + 1);

                if (!strReq.Equals(""))
                {
                    strReq = DecryptQueryString(strReq, strKey);  // decrypting whole query string
                    string[] arrMsgs = strReq.Split('&');  // separating all query string parameters in case of multiple parameter passed
                    string[] arrIndMsg;
                    string roll = "";
                    //you can add loops or so to get the values out of the query string...

                    arrIndMsg = arrMsgs[0].Split('='); //getting roll
                    roll = arrIndMsg[1].ToString().Trim();
                    return roll;

                    // lblRoll.Text = roll;

                }
            }
            return null;
        }
        public string GenrateRandomString()
        {

            string allowedChars = "";
            allowedChars = "a,b,c,d,e,f,g,h,i,j,k,l,m,n,o,p,q,r,s,t,u,v,w,x,y,z,";
            allowedChars += "A,B,C,D,E,F,G,H,I,J,K,L,M,N,O,P,Q,R,S,T,U,V,W,X,Y,Z,";
            allowedChars += "1,2,3,4,5,6,7,8,9,0";
            char[] sep = { ',' };
            string[] arr = allowedChars.Split(sep);
            string passwordString = "";
            string temp = "";
            Random rand = new Random();
            for (int i = 0; i <= Convert.ToInt32(10) - 1; i++)
            {
                temp = arr[rand.Next(0, arr.Length)];
                passwordString += temp;
            }

            return passwordString;

        }


        public string MD5Hash(string input)
        {
            StringBuilder hash = new StringBuilder();
            MD5CryptoServiceProvider md5provider = new MD5CryptoServiceProvider();
            byte[] bytes = md5provider.ComputeHash(new UTF8Encoding().GetBytes(input));

            for (int i = 0; i < bytes.Length; i++)
            {
                hash.Append(bytes[i].ToString("x2"));
            }
            return hash.ToString();
        }

        public string SHA1Hash(string input)
        {
            string hash = "";
            byte[] temp;
            SHA1 sha = new SHA1CryptoServiceProvider();
            // This is one implementation of the abstract class SHA1.
            temp = sha.ComputeHash(Encoding.UTF8.GetBytes(input));


            //storing hashed vale into byte data type
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < temp.Length; i++)
            {
                sb.Append(temp[i].ToString("x2"));
            }

            hash = sb.ToString();


           
            return hash.ToString();
        }
    }
}
