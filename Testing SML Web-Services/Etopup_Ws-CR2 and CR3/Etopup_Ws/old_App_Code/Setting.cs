﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace old_App_Code
{
    public class Setting
    {
        public DataSet ReturnState()
        {
            DataSet ds = new DataSet();
            DataTable dt = new DataTable();



            try
            {
                dt.Columns.Add("state_id");
                dt.Columns.Add("state_name");

                DataRow dr = dt.NewRow();
                dr["state_id"] = "1";
                dr["state_name"] = "Andaman and Nicobar Islands";
                dt.Rows.Add(dr);

                DataRow dr1 = dt.NewRow();
                dr1["state_id"] = "2";
                dr1["state_name"] = "Andhra Pradesh";
                dt.Rows.Add(dr1);

                DataRow dr2 = dt.NewRow();
                dr2["state_id"] = "3";
                dr2["state_name"] = "Arunachal Pradesh";
                dt.Rows.Add(dr2);

                DataRow dr3 = dt.NewRow();
                dr3["state_id"] = "4";
                dr3["state_name"] = "Assam";
                dt.Rows.Add(dr3);

                DataRow dr4 = dt.NewRow();
                dr4["state_id"] = "5";
                dr4["state_name"] = "Bihar";
                dt.Rows.Add(dr4);

                DataRow dr5 = dt.NewRow();
                dr5["state_id"] = "6";
                dr5["state_name"] = "Chandigarh";
                dt.Rows.Add(dr5);

                DataRow dr6 = dt.NewRow();
                dr6["state_id"] = "7";
                dr6["state_name"] = "Chhattisgarh";
                dt.Rows.Add(dr6);

                DataRow dr7 = dt.NewRow();
                dr7["state_id"] = "8";
                dr7["state_name"] = "Dadra and Nagar Haveli";
                dt.Rows.Add(dr7);

                DataRow dr8 = dt.NewRow();
                dr8["state_id"] = "9";
                dr8["state_name"] = "Daman and Diu";
                dt.Rows.Add(dr8);

                DataRow dr9 = dt.NewRow();
                dr9["state_id"] = "10";
                dr9["state_name"] = "Delhi";
                dt.Rows.Add(dr9);

                DataRow dr10 = dt.NewRow();
                dr10["state_id"] = "11";
                dr10["state_name"] = "Gujarat";
                dt.Rows.Add(dr10);

                DataRow dr11 = dt.NewRow();
                dr11["state_id"] = "12";
                dr11["state_name"] = "Haryana";

                DataRow dr12 = dt.NewRow();
                dr12["state_id"] = "13";
                dr12["state_name"] = "Himachal Pradesh";
                dt.Rows.Add(dr12);

                DataRow dr13 = dt.NewRow();
                dr13["state_id"] = "14";
                dr13["state_name"] = "Jammu and Kashmir";
                dt.Rows.Add(dr13);

                DataRow dr14 = dt.NewRow();
                dr14["state_id"] = "15";
                dr14["state_name"] = "Jharkhand";
                dt.Rows.Add(dr14);

                DataRow dr15 = dt.NewRow();
                dr15["state_id"] = "16";
                dr15["state_name"] = "Karnataka";
                dt.Rows.Add(dr15);

                DataRow dr16 = dt.NewRow();
                dr16["state_id"] = "17";
                dr16["state_name"] = "Kerala";
                dt.Rows.Add(dr16);

                DataRow dr17 = dt.NewRow();
                dr17["state_id"] = "18";
                dr17["state_name"] = "Lakshadweep";
                dt.Rows.Add(dr17);

                DataRow dr18 = dt.NewRow();
                dr18["state_id"] = "19";
                dr18["state_name"] = "Madhya Pradesh";
                dt.Rows.Add(dr18);

                DataRow dr19 = dt.NewRow();
                dr19["state_id"] = "20";
                dr19["state_name"] = "Maharashtra";
                dt.Rows.Add(dr19);

                DataRow dr20 = dt.NewRow();
                dr20["state_id"] = "21";
                dr20["state_name"] = "Manipur";
                dt.Rows.Add(dr20);

                DataRow dr21 = dt.NewRow();
                dr21["state_id"] = "22";
                dr21["state_name"] = "Meghalaya";
                dt.Rows.Add(dr21);

                DataRow dr22 = dt.NewRow();
                dr22["state_id"] = "23";
                dr22["state_name"] = "Mizoram";
                dt.Rows.Add(dr22);

                DataRow dr23 = dt.NewRow();
                dr23["state_id"] = "24";
                dr23["state_name"] = "Nagaland";
                dt.Rows.Add(dr23);

                DataRow dr24 = dt.NewRow();
                dr24["state_id"] = "25";
                dr24["state_name"] = "Odisha";
                dt.Rows.Add(dr24);

                DataRow dr25 = dt.NewRow();
                dr25["state_id"] = "26";
                dr25["state_name"] = "Puducherry";
                dt.Rows.Add(dr25);

                DataRow dr26 = dt.NewRow();
                dr26["state_id"] = "27";
                dr26["state_name"] = "Punjab";
                dt.Rows.Add(dr26);

                DataRow dr27 = dt.NewRow();
                dr27["state_id"] = "28";
                dr27["state_name"] = "Rajasthan";
                dt.Rows.Add(dr27);

                DataRow drp = dt.NewRow();
                drp["state_id"] = "29";
                drp["state_name"] = "Sikkim";
                dt.Rows.Add(drp);

                DataRow dr28 = dt.NewRow();
                dr28["state_id"] = "30";
                dr28["state_name"] = "Tamil Nadu";
                dt.Rows.Add(dr28);

                DataRow dr29 = dt.NewRow();

                dr29["state_id"] = "31";
                dr29["state_name"] = "Tripura";
                dt.Rows.Add(dr29);

                DataRow dr30 = dt.NewRow();
                dr30["state_id"] = "32";
                dr30["state_name"] = "Uttar Pradesh";
                dt.Rows.Add(dr30);

                DataRow dr31 = dt.NewRow();
                dr31["state_id"] = "33";
                dr31["state_name"] = "Uttarakhand";
                dt.Rows.Add(dr31);

                DataRow dr32 = dt.NewRow();
                dr32["state_id"] = "34";
                dr32["state_name"] = "West Bengal";
                dt.Rows.Add(dr32);

                DataRow dr33 = dt.NewRow();
                dr33["state_id"] = "35";
                dr33["state_name"] = "Goa";



                dt.Rows.Add(dr33);

                ds.Tables.Add(dt);
                return ds;
            }
            catch (Exception ex)
            {
                return ds;
            }

            finally
            {
            }
        }

    }
}