﻿using System;
using RestSharp;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Data;
using Etopup_Ws.old_App_Code;
using System.Xml;
using System.Text.RegularExpressions;
using Newtonsoft.Json.Linq;
using System.Diagnostics;
using System.Xml.Linq;

namespace Etopup_Ws
{
    /// <summary>
    /// Summary description for Recharge
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class Recharge : System.Web.Services.WebService
    {
        ODBC objODBC = new ODBC(); Encrypt_Decrypt ED = new Encrypt_Decrypt();
        ODBC_LOG objODBCLOG = new ODBC_LOG();
        CreateFile objCreateFile = new CreateFile();
        CommonFunction objCOMFUN = new CommonFunction();
        Encryption objEncrypMD5SHA1 = new Encryption();
        old_App_Code.DateFormat objDateFor = new old_App_Code.DateFormat();


        [WebMethod]

        // ActivationContext Type=62
        // int intUserID=0, int intOperatorID=1, string strMobile=2, double dblAmount=3, string strIPAddress=4, int intDesignationType=5, string strMacAddress=6, string strOSDetails=7, string strIMEINo=8, string strGcmID=9, string strAPPVersion=10, int intSource=11, int intCreatedByType=12
        public ArrayList MakeMobileRecharge(string strUserName, string strData, int intEUserType, int intType, int intSource)
        {
            int intActivityType = 62;
            //  string strUserName; string strData; int intEUserType; int intType, intSourceData;
            string strMessage = "Top Up", strExiomsOpName = "";

            ArrayList arrUserParams = new ArrayList();

            var data = ED.Decrypt(strUserName, strData, intEUserType, intType, intSource).Split(',');
            try
            {
                int intUserID = int.Parse(data[0]);
                int intOperatorID = int.Parse(data[1]);
                // intOperatorID = 3;
                string strMobile = data[2];
                //  strMobile = "0799477575";
                int dblAmount = int.Parse(data[3]);
                // dblAmount =31;
                string strIPAddress = data[4];
                int intDesignationType = int.Parse(data[5]);
                string strMacAddress = data[6];
                string strOSDetails = data[7];
                string strIMEINo = data[8];
                string strGcmID = data[9];
                string strAPPVersion = data[10];
                int intSourceData = int.Parse(data[11]);
                int intCreatedByType = int.Parse(data[12]);
                try
                {
                    try
                    {
                        objCreateFile.CreateTextFile(strIPAddress, strMacAddress, strOSDetails, strIMEINo, strGcmID, strAPPVersion, intSourceData.ToString(), intCreatedByType, intActivityType, " MakeMobileRecharge" + ",Data:" + strData.ToString(), strUserName);
                    }
                    catch (Exception)
                    {

                    }

                    if (dblAmount > 0)
                    {
                        string strCommission = objODBC.executeScalar_str("call getCommissionForRecharge(" + intUserID + "," + intOperatorID + ")");

                        string[] strCommissionStructure = strCommission.Split(new char[] { ',' });

                        double dblMyComm = double.Parse(strCommissionStructure[0]);
                        dblMyComm = 100 * dblMyComm / (100 + dblMyComm);
                        string strMyAPIType = strCommissionStructure[1];
                        // ‘ 1-Salaam;2-Etisalat;3-Roshan;4-Easy Pay
                        double dblResellerComm = double.Parse(strCommissionStructure[2]);
                        int intOperatorType = 1;

                        // int intAPIType = ReturnAPITypeForOperator(intOperatorID)
                        //   string strExiomsOpName = objNewRecharge.GetExiomsOperatorName(intOperatorID);

                        string strCheckResponse = CheckRechargeRequest(intUserID, objODBC.executeScalar_str("select username FROM `er_login` where userid='" + intUserID + "'"), intOperatorType, intOperatorID, strMobile, dblAmount, dblMyComm, dblResellerComm, int.Parse(strMyAPIType));

                        string[] strResponseStructure = strCheckResponse.Split(new char[] { ',' });
                        // strMessage = "2,OOPs! You do not have sufficient balance.,Failed,NA,NA";

                        string strStatusCheckResponse = strResponseStructure[0];
                        string strResponseMessage = strResponseStructure[1];
                        string strResposneStatusMessage = strResponseStructure[2];
                        string strResposneStatus3 = strResponseStructure[3];
                        string strResposneStatus4 = strResponseStructure[4];
                        if (strStatusCheckResponse == "1")
                        {
                            if (strMyAPIType == "1")
                            {
                                try
                                {

                                    strExiomsOpName = "Salaam";
                                    try
                                    {

                                        objODBCLOG.executeNonQuery("call activity_logs('" + intUserID + "','" + intDesignationType + "','" + intCreatedByType + "','" + strIPAddress + "','" + strMacAddress + "','" + strOSDetails + "','" + strIMEINo + "','" + strGcmID + "','" + strAPPVersion + "','" + intSourceData + "','" + strMessage + "','" + intActivityType + "','','')");
                                    }
                                    catch (Exception)
                                    {

                                    }
                                    strMessage = MakeMobileRechargeAPI1(intUserID, strUserName, intOperatorType, intOperatorID, strMobile, dblAmount, strExiomsOpName, dblMyComm, dblResellerComm, strMyAPIType, strIPAddress, intDesignationType, strMacAddress, strOSDetails, strIMEINo, strGcmID, strAPPVersion, intSourceData, intCreatedByType, "0");

                                    string[] strResponseMessageSalaam = strMessage.Split(new char[] { ',' });

                                    string strStatusRes = strResponseMessageSalaam[0];
                                    if (strStatusRes == "SUCCESS")
                                    {
                                        string strStatusMessage = strResponseMessageSalaam[1];
                                        string strStatusTopUpTransactionID = strResponseMessageSalaam[2];
                                        string strStatusOperaterTransID = strResponseMessageSalaam[3];
                                        arrUserParams.Add("TRUE");
                                        arrUserParams.Add(strStatusMessage);
                                        arrUserParams.Add(strStatusTopUpTransactionID);
                                        arrUserParams.Add(strStatusOperaterTransID);
                                        //    objODBC.executeNonQuery("INSERT INTO `temp`( `data`, `type`, `request`) VALUES('4','3','" + strStatusMessage + "')");
                                        return arrUserParams;
                                    }
                                    else if (strStatusRes == "PENDING")
                                    {
                                        string strStatusMessage = strResponseMessageSalaam[1];
                                        string strStatusTopUpTransactionID = strResponseMessageSalaam[2];
                                        string strStatusOperaterTransID = strResponseMessageSalaam[3];
                                        arrUserParams.Add("FALSE");
                                        arrUserParams.Add(strStatusRes);
                                        arrUserParams.Add(strStatusTopUpTransactionID);
                                        arrUserParams.Add(strStatusOperaterTransID);
                                        //    objODBC.executeNonQuery("INSERT INTO `temp`( `data`, `type`, `request`) VALUES('4','3','" + strStatusMessage + "')");
                                        return arrUserParams;
                                    }
                                    else if (strStatusRes == "FAILED")
                                    {
                                        string strStatusMessage = strResponseMessageSalaam[1];
                                        string strStatusTopUpTransactionID = strResponseMessageSalaam[2];
                                        string strStatusOperaterTransID = strResponseMessageSalaam[3];
                                        arrUserParams.Add("FALSE");
                                        arrUserParams.Add(strStatusMessage);
                                        arrUserParams.Add(strStatusTopUpTransactionID);
                                        arrUserParams.Add(strStatusOperaterTransID);
                                        //    objODBC.executeNonQuery("INSERT INTO `temp`( `data`, `type`, `request`) VALUES('4','3','" + strStatusMessage + "')");
                                        return arrUserParams;
                                    }
                                    else
                                    {
                                        // objODBC.executeNonQuery("INSERT INTO `temp`( `data`, `type`, `request`) VALUES('4','3','Invalid Details')");
                                        arrUserParams.Add("FALSE");
                                        arrUserParams.Add("Invalid Details");
                                        arrUserParams.Add("NA");
                                        arrUserParams.Add("NA");
                                        return arrUserParams;
                                    }

                                }
                                catch (Exception)
                                {

                                }
                                return arrUserParams;
                            }
                            else if (strMyAPIType == "2")
                            {
                                try
                                {
                                    //int intOperatorIDRechargeAPI = objNewRecharge.GetOperatorIDForRechargeAPI(intOperatorID);

                                    strExiomsOpName = "Etisalat";
                                    try
                                    {
                                        objODBCLOG.executeNonQuery("call activity_logs('" + intUserID + "','" + intDesignationType + "','" + intCreatedByType + "','" + strIPAddress + "','" + strMacAddress + "','" + strOSDetails + "','" + strIMEINo + "','" + strGcmID + "','" + strAPPVersion + "','" + intSourceData + "','" + strMessage + "','" + intActivityType + "','','')");
                                    }
                                    catch (Exception)
                                    {

                                    }
                                    strMessage = MakeMobileRechargeAPI2(intUserID, strUserName, intOperatorType, intOperatorID, strMobile, dblAmount, strExiomsOpName, dblMyComm, dblResellerComm, strMyAPIType, strIPAddress, intDesignationType, strMacAddress, strOSDetails, strIMEINo, strGcmID, strAPPVersion, intSourceData, intCreatedByType, "0");

                                    string[] strResponseMessagEtisalat = strMessage.Split(new char[] { ',' });

                                    string strStatusRes = strResponseMessagEtisalat[0];
                                    if (strStatusRes == "SUCCESS")
                                    {
                                        string strStatusMessage = strResponseMessagEtisalat[1];
                                        string strStatusTopUpTransactionID = strResponseMessagEtisalat[2];
                                        string strStatusOperaterTransID = strResponseMessagEtisalat[3];
                                        arrUserParams.Add("TRUE");
                                        arrUserParams.Add(strStatusMessage);
                                        arrUserParams.Add(strStatusTopUpTransactionID);
                                        arrUserParams.Add(strStatusOperaterTransID);
                                        //    objODBC.executeNonQuery("INSERT INTO `temp`( `data`, `type`, `request`) VALUES('4','3','" + strStatusMessage + "')");
                                        return arrUserParams;
                                    }
                                    else if (strStatusRes == "FAILED")
                                    {
                                        string strStatusMessage = strResponseMessagEtisalat[1];
                                        string strStatusTopUpTransactionID = strResponseMessagEtisalat[2];
                                        string strStatusOperaterTransID = strResponseMessagEtisalat[3];
                                        arrUserParams.Add("FALSE");
                                        arrUserParams.Add(strStatusMessage);
                                        arrUserParams.Add(strStatusTopUpTransactionID);
                                        arrUserParams.Add(strStatusOperaterTransID);
                                        //    objODBC.executeNonQuery("INSERT INTO `temp`( `data`, `type`, `request`) VALUES('4','3','" + strStatusMessage + "')");
                                        return arrUserParams;
                                    }
                                    else if (strStatusRes == "PENDING")
                                    {
                                        string strStatusMessage = strResponseMessagEtisalat[1];
                                        string strStatusTopUpTransactionID = strResponseMessagEtisalat[2];
                                        string strStatusOperaterTransID = strResponseMessagEtisalat[3];
                                        arrUserParams.Add("FALSE");
                                        arrUserParams.Add(strStatusRes);
                                        arrUserParams.Add(strStatusTopUpTransactionID);
                                        arrUserParams.Add(strStatusOperaterTransID);
                                        //    objODBC.executeNonQuery("INSERT INTO `temp`( `data`, `type`, `request`) VALUES('4','3','" + strStatusMessage + "')");
                                        return arrUserParams;
                                    }
                                    else
                                    {
                                        // objODBC.executeNonQuery("INSERT INTO `temp`( `data`, `type`, `request`) VALUES('4','3','Invalid Details')");
                                        arrUserParams.Add("FALSE");
                                        arrUserParams.Add("Invalid Details");
                                        arrUserParams.Add("NA");
                                        arrUserParams.Add("NA");
                                        return arrUserParams;
                                    }

                                }
                                catch (Exception)
                                {

                                }
                                return arrUserParams;
                            }
                            else if (strMyAPIType == "3")
                            {
                                try
                                {
                                    //int intOperatorIDRechargeAPI = objNewRecharge.GetOperatorIDForRechargeAPI(intOperatorID);

                                    strExiomsOpName = "Roshan";
                                    try
                                    {
                                        objODBCLOG.executeNonQuery("call activity_logs('" + intUserID + "','" + intDesignationType + "','" + intCreatedByType + "','" + strIPAddress + "','" + strMacAddress + "','" + strOSDetails + "','" + strIMEINo + "','" + strGcmID + "','" + strAPPVersion + "','" + intSourceData + "','" + strMessage + "','" + intActivityType + "','','')");
                                    }
                                    catch (Exception)
                                    {

                                    }
                                    strMessage = MakeMobileRechargeAPI3(intUserID, strUserName, intOperatorType, intOperatorID, strMobile, dblAmount, strExiomsOpName, dblMyComm, dblResellerComm, strMyAPIType, strIPAddress, intDesignationType, strMacAddress, strOSDetails, strIMEINo, strGcmID, strAPPVersion, intSourceData, intCreatedByType, "0");

                                    string[] strResponseMessageRoshan = strMessage.Split(new char[] { ',' });

                                    string strStatusRes = strResponseMessageRoshan[0];
                                    if (strStatusRes == "SUCCESS")
                                    {
                                        string strStatusMessage = strResponseMessageRoshan[1];
                                        string strStatusTopUpTransactionID = strResponseMessageRoshan[2];
                                        string strStatusOperaterTransID = strResponseMessageRoshan[3];
                                        arrUserParams.Add("TRUE");
                                        arrUserParams.Add(strStatusMessage);
                                        arrUserParams.Add(strStatusTopUpTransactionID);
                                        arrUserParams.Add(strStatusOperaterTransID);
                                        //    objODBC.executeNonQuery("INSERT INTO `temp`( `data`, `type`, `request`) VALUES('4','3','" + strStatusMessage + "')");
                                        return arrUserParams;
                                    }
                                    else if (strStatusRes == "FAILED")
                                    {
                                        string strStatusMessage = strResponseMessageRoshan[1];
                                        string strStatusTopUpTransactionID = strResponseMessageRoshan[2];
                                        string strStatusOperaterTransID = strResponseMessageRoshan[3];
                                        arrUserParams.Add("FALSE");
                                        arrUserParams.Add(strStatusMessage);
                                        arrUserParams.Add(strStatusTopUpTransactionID);
                                        arrUserParams.Add(strStatusOperaterTransID);
                                        //    objODBC.executeNonQuery("INSERT INTO `temp`( `data`, `type`, `request`) VALUES('4','3','" + strStatusMessage + "')");
                                        return arrUserParams;
                                    }
                                    else if (strStatusRes == "PENDING")
                                    {
                                        string strStatusMessage = strResponseMessageRoshan[1];
                                        string strStatusTopUpTransactionID = strResponseMessageRoshan[2];
                                        string strStatusOperaterTransID = strResponseMessageRoshan[3];
                                        arrUserParams.Add("FALSE");
                                        arrUserParams.Add(strStatusRes);
                                        arrUserParams.Add(strStatusTopUpTransactionID);
                                        arrUserParams.Add(strStatusOperaterTransID);
                                        //    objODBC.executeNonQuery("INSERT INTO `temp`( `data`, `type`, `request`) VALUES('4','3','" + strStatusMessage + "')");
                                        return arrUserParams;
                                    }
                                    else
                                    {
                                        // objODBC.executeNonQuery("INSERT INTO `temp`( `data`, `type`, `request`) VALUES('4','3','Invalid Details')");
                                        arrUserParams.Add("FALSE");
                                        arrUserParams.Add("Invalid Details");
                                        arrUserParams.Add("NA");
                                        arrUserParams.Add("NA");
                                        return arrUserParams;
                                    }

                                }
                                catch (Exception)
                                {

                                }
                                return arrUserParams;
                            }
                            else if (strMyAPIType == "4")
                            {
                                //int intOperatorIDRechargeAPI = objNewRecharge.GetOperatorIDForRechargeAPI(intOperatorID);
                                if (intOperatorID == 4)
                                {
                                    strExiomsOpName = "MTN";
                                }
                                else
                                {
                                    strExiomsOpName = "AWCC";
                                }
                                try
                                {
                                    objODBCLOG.executeNonQuery("call activity_logs('" + intUserID + "','" + intDesignationType + "','" + intCreatedByType + "','" + strIPAddress + "','" + strMacAddress + "','" + strOSDetails + "','" + strIMEINo + "','" + strGcmID + "','" + strAPPVersion + "','" + intSourceData + "','" + strMessage + "','" + intActivityType + "','','')");
                                }
                                catch (Exception)
                                {

                                }
                                strMessage = MakeMobileRechargeAPI4(intUserID, strUserName, intOperatorType, intOperatorID, strMobile, dblAmount, strExiomsOpName, dblMyComm, dblResellerComm, strMyAPIType, strIPAddress, intDesignationType, strMacAddress, strOSDetails, strIMEINo, strGcmID, strAPPVersion, intSourceData, intCreatedByType, "0");

                                string[] strResponseMessageEasyPay = strMessage.Split(new char[] { ',' });

                                string strStatusRes = strResponseMessageEasyPay[0];

                                if (strStatusRes == "SUCCESS")
                                {
                                    string strStatusMessage = strResponseMessageEasyPay[1];
                                    string strStatusTopUpTransactionID = strResponseMessageEasyPay[2];
                                    string strStatusOperaterTransID = strResponseMessageEasyPay[3];
                                    arrUserParams.Add("TRUE");
                                    arrUserParams.Add(strStatusMessage);
                                    arrUserParams.Add(strStatusTopUpTransactionID);
                                    arrUserParams.Add(strStatusOperaterTransID);
                                    return arrUserParams;

                                }
                                else if (strStatusRes == "FAILED")
                                {
                                    string strStatusMessage = strResponseMessageEasyPay[1];
                                    string strStatusTopUpTransactionID = strResponseMessageEasyPay[2];
                                    string strStatusOperaterTransID = strResponseMessageEasyPay[3];
                                    arrUserParams.Add("FALSE");
                                    arrUserParams.Add(strStatusMessage);
                                    arrUserParams.Add(strStatusTopUpTransactionID);
                                    arrUserParams.Add(strStatusOperaterTransID);
                                    return arrUserParams;
                                }
                                else if (strStatusRes == "PENDING")
                                {
                                    string strStatusMessage = strResponseMessageEasyPay[1];
                                    string strStatusTopUpTransactionID = strResponseMessageEasyPay[2];
                                    string strStatusOperaterTransID = strResponseMessageEasyPay[3];
                                    arrUserParams.Add("FALSE");
                                    arrUserParams.Add(strStatusRes);
                                    arrUserParams.Add(strStatusTopUpTransactionID);
                                    arrUserParams.Add(strStatusOperaterTransID);
                                    //    objODBC.executeNonQuery("INSERT INTO `temp`( `data`, `type`, `request`) VALUES('4','3','" + strStatusMessage + "')");
                                    return arrUserParams;
                                }
                                else
                                {
                                    // objODBC.executeNonQuery("INSERT INTO `temp`( `data`, `type`, `request`) VALUES('4','3','Invalid Details')");
                                    arrUserParams.Add("FALSE");
                                    arrUserParams.Add("Invalid Details");
                                    arrUserParams.Add("NA");
                                    arrUserParams.Add("NA");
                                    return arrUserParams;
                                }


                            }
                            else if (strMyAPIType == "5")
                            {
                                //int intOperatorIDRechargeAPI = objNewRecharge.GetOperatorIDForRechargeAPI(intOperatorID);
                                if (intOperatorID == 1)
                                {
                                    strExiomsOpName = "Salaam";
                                }
                                if (intOperatorID == 2)
                                {
                                    strExiomsOpName = "Etisalat";
                                }
                                if (intOperatorID == 3)
                                {
                                    strExiomsOpName = "Roshan";
                                }
                                if (intOperatorID == 4)
                                {
                                    strExiomsOpName = "MTN";
                                }
                                else if (intOperatorID == 5)
                                {
                                    strExiomsOpName = "AWCC";
                                }

                                try
                                {
                                    objODBCLOG.executeNonQuery("call activity_logs('" + intUserID + "','" + intDesignationType + "','" + intCreatedByType + "','" + strIPAddress + "','" + strMacAddress + "','" + strOSDetails + "','" + strIMEINo + "','" + strGcmID + "','" + strAPPVersion + "','" + intSourceData + "','" + strMessage + "','" + intActivityType + "','','')");
                                }
                                catch (Exception)
                                {

                                }
                                strMessage = MakeMobileRechargeAPI5(intUserID, strUserName, intOperatorType, intOperatorID, strMobile, dblAmount, strExiomsOpName, dblMyComm, dblResellerComm, strMyAPIType, strIPAddress, intDesignationType, strMacAddress, strOSDetails, strIMEINo, strGcmID, strAPPVersion, intSourceData, intCreatedByType, "0");

                                string[] strResponseMessageEasyPay = strMessage.Split(new char[] { ',' });

                                string strStatusRes = strResponseMessageEasyPay[0];

                                if (strStatusRes == "SUCCESS")
                                {
                                    string strStatusMessage = strResponseMessageEasyPay[1];
                                    string strStatusTopUpTransactionID = strResponseMessageEasyPay[2];
                                    string strStatusOperaterTransID = strResponseMessageEasyPay[3];
                                    arrUserParams.Add("TRUE");
                                    arrUserParams.Add(strStatusMessage);
                                    arrUserParams.Add(strStatusTopUpTransactionID);
                                    arrUserParams.Add(strStatusOperaterTransID);
                                    return arrUserParams;

                                }
                                else if (strStatusRes == "FAILED")
                                {
                                    string strStatusMessage = strResponseMessageEasyPay[1];
                                    string strStatusTopUpTransactionID = strResponseMessageEasyPay[2];
                                    string strStatusOperaterTransID = strResponseMessageEasyPay[3];
                                    arrUserParams.Add("FALSE");
                                    arrUserParams.Add(strStatusMessage);
                                    arrUserParams.Add(strStatusTopUpTransactionID);
                                    arrUserParams.Add(strStatusOperaterTransID);
                                    return arrUserParams;
                                }
                                else if (strStatusRes == "PENDING")
                                {
                                    string strStatusMessage = strResponseMessageEasyPay[1];
                                    string strStatusTopUpTransactionID = strResponseMessageEasyPay[2];
                                    string strStatusOperaterTransID = strResponseMessageEasyPay[3];
                                    arrUserParams.Add("FALSE");
                                    arrUserParams.Add(strStatusRes);
                                    arrUserParams.Add(strStatusTopUpTransactionID);
                                    arrUserParams.Add(strStatusOperaterTransID);
                                    //    objODBC.executeNonQuery("INSERT INTO `temp`( `data`, `type`, `request`) VALUES('4','3','" + strStatusMessage + "')");
                                    return arrUserParams;
                                }
                                else
                                {
                                    // objODBC.executeNonQuery("INSERT INTO `temp`( `data`, `type`, `request`) VALUES('4','3','Invalid Details')");
                                    arrUserParams.Add("FALSE");
                                    arrUserParams.Add("Invalid Details");
                                    arrUserParams.Add("NA");
                                    arrUserParams.Add("NA");
                                    return arrUserParams;
                                }


                            }
                            else if (strMyAPIType == "6")
                            {
                                try
                                {
                                    //int intOperatorIDRechargeAPI = objNewRecharge.GetOperatorIDForRechargeAPI(intOperatorID);

                                    strExiomsOpName = "MTN";
                                    try
                                    {
                                        objODBCLOG.executeNonQuery("call activity_logs('" + intUserID + "','" + intDesignationType + "','" + intCreatedByType + "','" + strIPAddress + "','" + strMacAddress + "','" + strOSDetails + "','" + strIMEINo + "','" + strGcmID + "','" + strAPPVersion + "','" + intSourceData + "','" + strMessage + "','" + intActivityType + "','','')");
                                    }
                                    catch (Exception)
                                    {

                                    }
                                    strMessage = MakeMobileRechargeAPI6(intUserID, strUserName, intOperatorType, intOperatorID, strMobile, dblAmount, strExiomsOpName, dblMyComm, dblResellerComm, strMyAPIType, strIPAddress, intDesignationType, strMacAddress, strOSDetails, strIMEINo, strGcmID, strAPPVersion, intSourceData, intCreatedByType, "0");

                                    string[] strResponseMessageMTN = strMessage.Split(new char[] { ',' });

                                    string strStatusRes = strResponseMessageMTN[0];
                                    if (strStatusRes == "SUCCESS")
                                    {
                                        string strStatusMessage = strResponseMessageMTN[1];
                                        string strStatusTopUpTransactionID = strResponseMessageMTN[2];
                                        string strStatusOperaterTransID = strResponseMessageMTN[3];
                                        arrUserParams.Add("TRUE");
                                        arrUserParams.Add(strStatusMessage);
                                        arrUserParams.Add(strStatusTopUpTransactionID);
                                        arrUserParams.Add(strStatusOperaterTransID);
                                        //    objODBC.executeNonQuery("INSERT INTO `temp`( `data`, `type`, `request`) VALUES('4','3','" + strStatusMessage + "')");
                                        return arrUserParams;
                                    }
                                    else if (strStatusRes == "FAILED")
                                    {
                                        string strStatusMessage = strResponseMessageMTN[1];
                                        string strStatusTopUpTransactionID = strResponseMessageMTN[2];
                                        string strStatusOperaterTransID = strResponseMessageMTN[3];
                                        arrUserParams.Add("FALSE");
                                        arrUserParams.Add(strStatusMessage);
                                        arrUserParams.Add(strStatusTopUpTransactionID);
                                        arrUserParams.Add(strStatusOperaterTransID);
                                        //    objODBC.executeNonQuery("INSERT INTO `temp`( `data`, `type`, `request`) VALUES('4','3','" + strStatusMessage + "')");
                                        return arrUserParams;
                                    }
                                    else
                                    {
                                        // objODBC.executeNonQuery("INSERT INTO `temp`( `data`, `type`, `request`) VALUES('4','3','Invalid Details')");
                                        arrUserParams.Add("FALSE");
                                        arrUserParams.Add("Invalid Details");
                                        arrUserParams.Add("NA");
                                        arrUserParams.Add("NA");
                                        return arrUserParams;
                                    }

                                }
                                catch (Exception)
                                {

                                }
                                return arrUserParams;
                            }
                            else
                            {
                                arrUserParams.Add("FALSE");
                                strMessage = "Invalid Details for Recharge !";
                                arrUserParams.Add(strMessage);
                                arrUserParams.Add("NA");
                                arrUserParams.Add("NA");
                                return arrUserParams;
                            }
                        }
                        else
                        {
                            arrUserParams.Add("FALSE");

                            arrUserParams.Add(strResponseMessage);
                            arrUserParams.Add("NA");
                            arrUserParams.Add("NA");
                            return arrUserParams;
                        }
                    }
                    else
                    {
                        arrUserParams.Add("FALSE");
                        strMessage = "Amount Should be Greater than 0.";
                        arrUserParams.Add(strMessage);
                        arrUserParams.Add("NA");
                        arrUserParams.Add("NA");
                        return arrUserParams;
                    }
                }
                catch (Exception e1)
                {

                    strMessage = "Exception / Error : " + e1.ToString();
                    arrUserParams.Add("FALSE");
                    arrUserParams.Add(strMessage);
                    arrUserParams.Add("NA");
                    arrUserParams.Add("NA");
                    return arrUserParams;
                }

            }
            catch (Exception e1)
            {

                strMessage = "FAILED,Exception,NA,NA";
                arrUserParams.Add(strMessage);
                arrUserParams.Add("NA");
                arrUserParams.Add("NA");
                return arrUserParams;
            }
        }
        // Salaam Recharge TopUp
        public string MakeMobileRechargeAPI1(int intUserID, string strUserName, int intOperatorType, int intOperatorID, string strMobile, double dblAmount, string strExiomsOpName, double dblMyComm, double dblResellerComm, string strMyAPIType, string strIPAddress, int intDesignationType, string strMacAddress, string strOSDetails, string strIMEINo, string strGcmID, string strAPPVersion, int intSourceData, int intCreatedByType, string strPhone)
        {
            string strMessage = "";
            DataTable dt = new DataTable();
            // string strRequestType = "5"; For balance

            try
            {

                Regex objMobilePattern = new Regex(@"^74[\d]{7}$");
                if (objMobilePattern.IsMatch(strMobile))
                {
                    int intTopupStatus = objODBC.executeScalar_int("SELECT `status` FROM `er_status_manager_operator` WHERE operator_id='" + intOperatorID + "'");
                    if (intTopupStatus == 1)
                    {
                        int intRechargeStatus = objODBC.executeScalar_int("SELECT `salaam_status` FROM `er_login` WHERE userid='" + intUserID + "'");
                        if (intRechargeStatus == 1)
                        {
                            if (dblAmount >= 0)
                            {

                                SalaamApi objSalamApi = new SalaamApi();

                                int intTopUpRecTransactionID = 0;
                                string strPRIVAPICOmment = "";
                                int intPrivAPIType = 0;
                                double dblCommissionAmt = dblAmount * dblMyComm / 100;
                                double dblDeductAmt = dblAmount - dblCommissionAmt;
                                double dblResellerDeductAmt = dblAmount * (1 - (dblResellerComm / 100));
                                int intParentID = objODBC.executeScalar_int("call `er_getParentID`('" + intUserID + "',@intParentID)");
                                dt = objODBC.getDataTable("call `ap_insert_mobile`('" + intUserID + "', '" + intOperatorType + "', '" + intOperatorID + "','" + strExiomsOpName + "','" + strMobile + "', '" + dblAmount + "','" + dblDeductAmt + "', '" + dblResellerDeductAmt + "', '" + dblCommissionAmt + "', '" + dblMyComm + "', '" + dblCommissionAmt + "', '" + strIPAddress + "','" + strOSDetails + "',  '" + intSourceData + "', '" + strGcmID + "','" + strIMEINo + "','" + strMyAPIType + "','" + intPrivAPIType + "','" + strPRIVAPICOmment + "','" + intParentID + "')");
                                if (dt.Rows.Count > 0)
                                {
                                    intTopUpRecTransactionID = Convert.ToInt32(dt.Rows[0][0].ToString());
                                    int intSalaamID = Convert.ToInt32(dt.Rows[0][1].ToString());
                                    int intWalletID = Convert.ToInt32(dt.Rows[0][2].ToString());
                                    if (intWalletID > 0)
                                    {
                                        if ((intTopUpRecTransactionID > 0) && (intSalaamID > 0))
                                        {
                                            try
                                            {

                                                var SalaamResponse = objSalamApi.SalaamTopup(intTopUpRecTransactionID, intSalaamID, intTopUpRecTransactionID.ToString(), strMobile, dblAmount.ToString(), "");
                                                // var SalaamResponse = objODBC.executeScalar_str("SELECT data FROM temp WHERE id=1");
                                                try
                                                {
                                                    if (strPhone.Length == 11)
                                                    {
                                                        strPhone = strPhone.Substring(2, strPhone.Length - 2);
                                                    }
                                                    objODBC.executeNonQuery("update er_recharge set comment='" + SalaamResponse.ToString() + "',request_mobile_no='" + strPhone + "' where id='" + intTopUpRecTransactionID + "'");
                                                }
                                                catch (Exception)
                                                {

                                                }



                                                dynamic data = JObject.Parse(SalaamResponse);
                                                string strRequestEncrypted = data.Request;
                                                string strResponseEncrypted = data.Response;
                                                string strResponseCode = data.ResponseCode;
                                                string strResponseDescription = "NA";
                                                strResponseDescription = data.ResponseDescription;
                                                if (strResponseCode == "000")
                                                {
                                                    // {
                                                    //    "Request": "ZmRzZmRzODU0MzVuZmRmc2Pq0nYybI045HO2pX/CcZHHBoWSdGg3DqG/Mw1bcIm2cpfXa5d/DmLHWyN2BDCXoRvqT1XNcucz/xF7xNuB9nsyt//8pLvb5emLCXcV/Plej1GWPt7YpU1TeC5E+s9iGWfZvdsWSmyoBZ+fEmFccNNLiZ3Gy8jzo6bBQ31y9IbNQiz9fZ9m0V0h4d716rIQikcBDfC5SQTVF/x/s17OKFgMlsMMDZcfFejRu3Mkl+1kGacu09+gvlc4R0h2kei+vot/kQM=",
                                                    //    "Response": "vcbcDPmUDUzI1WNu5OZ8eRXpQdZ8UWZ3zpWz+7WAK1yGdeUtlq3mRmbm4ZzYafmBMkUeZ6oOQW5knAA4sDnI3omYaR3y6oNQpmkQQVcsfdKdB4zRvf+FNl4pdp0+czc5zK8eNoRAeUxIZ+y1env+p6fkCtl1ACEv6NYZJ8esTp8D7KO/W6mW8vhsmkQeZgUAPr3mlWeXToibEIspnfotpbUsIjtiNha5mElvk0JgFkth2VoKXnKw7JVFSu1mEN+RUUWnNXdvrhXDq7sYE4pQ1dyALYHuO1W/fklMpJxL7PxBbm+AYIQXVokYdtUAkMO+ZHmN6PSSwWli1p65easyUOcTZGQTSDOgCCHSB4JN+B/cydjXMPug9bdu2IpcpMlkZ8RwIk/8pqMG4Dg2gBVje9hTVE/OMKwZf3bs7Za1BRHmjyRH",
                                                    //    "ResponseCode": "000",
                                                    //    "ResponseDescription": "Txn Successful",
                                                    //    "Balance": "530.000000",
                                                    //    "ConfirmationCode": "",
                                                    //    "TransactionID": "4063542",
                                                    //    "TransactionInfo": "{\"BEFOROPER\":\"90084\",\"AFTEROPER\":\"180084\"}",
                                                    //    "RequestDateTime": "2019-02-15 15:30:00",
                                                    //    "TransactionFee": "0",
                                                    //    "Commission": "0.0"
                                                    //}


                                                    string strBalance = "NA";
                                                    string strConfirmationCode = "NA";
                                                    string strTransactionID = "NA";
                                                    string strTransactionInfo = "NA";
                                                    string strRequestDateTime = "NA";
                                                    string strTransactionFee = "NA";
                                                    string strCommission = "NA";

                                                    string strto_surcharge = "NA";
                                                    string strto_commission = "NA";
                                                    string strsender_phone = "NA";
                                                    string strsender_name = "NA";
                                                    string strsender_id_type = "NA";
                                                    string strsender_id_number = "NA";
                                                    string strreceiver_phone = "NA";
                                                    string strreceiver_name = "NA";
                                                    string strreceiver_id_type = "NA";
                                                    string strreceiver_id_number = "NA";

                                                    try
                                                    {
                                                        objODBC.executeNonQuery("call `updateResponseV3`('" + strTransactionID + "','" + intTopUpRecTransactionID + "',1)");
                                                    }
                                                    catch (Exception)
                                                    {

                                                    }
                                                    strMessage = "SUCCESS," + "Top Up Transaction is Successfull ! ," + intTopUpRecTransactionID + "," + strResponseDescription;
                                                    try
                                                    {

                                                        try
                                                        {
                                                            strBalance = data.Balance;
                                                            strTransactionID = data.TransactionID;
                                                            strConfirmationCode = data.ConfirmationCode;
                                                            strTransactionInfo = data.TransactionInfo;
                                                            strRequestDateTime = data.RequestDateTime;
                                                            strTransactionFee = data.TransactionFee;
                                                            strCommission = data.Commission;
                                                        }
                                                        catch (Exception)
                                                        {

                                                        }
                                                        try
                                                        {
                                                            strto_surcharge = data.to_surcharge;
                                                            strto_commission = data.to_commission;
                                                            strsender_phone = data.sender_phone;
                                                            strsender_name = data.sender_name;
                                                            strsender_id_type = data.sender_id_type;
                                                            strsender_id_number = data.sender_id_number;
                                                            strreceiver_phone = data.receiver_phone;
                                                            strreceiver_name = data.receiver_name;
                                                            strreceiver_id_type = data.receiver_id_type;
                                                            strreceiver_id_number = data.receiver_id_number;
                                                        }
                                                        catch (Exception)
                                                        {

                                                        }
                                                    }
                                                    catch (Exception)
                                                    {

                                                    }
                                                    try
                                                    {
                                                        objODBC.executeNonQuery("update er_recharge set operator_balance='" + strBalance.ToString() + "',ap_transid='" + strTransactionID + "',operator_transid='" + strTransactionID + "' where id='" + intTopUpRecTransactionID + "'");

                                                        objODBC.executeNonQuery("UPDATE er_salaam SET status=1,response_code='" + strResponseCode + "',response_description='" + strResponseDescription + "',transaction_id='" + strTransactionID + "',Balance='" + strBalance.ToString() + "',confirmation_code='" + strConfirmationCode + "',TransactionInfo='" + strTransactionInfo + "',RequestDateTime='" + strRequestDateTime + "',TransactionFee='" + strTransactionFee + "',Commission='" + strCommission + "',to_surcharge='" + strto_surcharge + "',to_commission='" + strto_commission + "',sender_phone='" + strsender_phone + "',sender_name='" + strsender_name + "',sender_id_type='" + strsender_id_type + "',sender_id_number='" + strsender_id_number + "',receiver_phone='" + strreceiver_phone + "',receiver_name='" + strreceiver_name + "',receiver_id_type='" + strreceiver_id_type + "',receiver_id_number='" + strreceiver_id_number + "',encrypted_request='" + strRequestEncrypted + "',encrypted_response='" + strResponseEncrypted + "' WHERE id='" + intSalaamID + "'");
                                                    }
                                                    catch (Exception)
                                                    {

                                                    }
                                                    try
                                                    {
                                                        if (strPhone.Length == 11)
                                                        {
                                                            strPhone = strPhone.Substring(2, strPhone.Length - 2);
                                                        }
                                                        SendSMPPSMS(intUserID, strUserName, strMobile, strExiomsOpName, intOperatorType, dblAmount, 1, intTopUpRecTransactionID.ToString(), strPhone);
                                                    }
                                                    catch (Exception)
                                                    {

                                                    }

                                                    return strMessage;
                                                }
                                                else
                                                {
                                                    //  {
                                                    //  "Request": "ZmRzZmRzODU0MzVuZmRmc9FihL/Ubj2NoI+k9ksld6xC/hyGdGg3DqG/Mw1bcIm2cpfXaxTnIrb98S2aKcuC5SA99F401/1qSitySGxJc7ia3kNmEmicVdM8qMf92kmXwXN71KmdJkvs9yxeXsrv8ltD1mgjdq6U1f4wuvbMvMTsscJge3D3tm9IOxrf3fAJYcrM4N4E2VePbUg1zdLDDKLhs5nQjUNKnrbOE0t6tZCsk4dd2IIwmWp0wDH5B2S0aIaFHNss3g4AMT9WZvYAbjbAl0U=",
                                                    // "Response": "CPQxNu/lWopDrPlrOa4jkj+wrewP0f7xuML/YwImEaTPXDr1h/6GRjd1LRnrLlk6C8sEsimFNG66SjqIZOlPEhv96u5cCmz7e4eOwLu5inrTC+VUVTiB5O5OOxKUHSr4Unl2aZ8NNxQ5iW9m842fHt3K9ag=",
                                                    // "ResponseCode": "102",
                                                    // "ResponseDescription": "Incomplete Txn Request - ERC102"
                                                    //}



                                                    string strBalance = "0";
                                                    string strConfirmationCode = "NA";
                                                    string strTransactionID = "NA";
                                                    string strTransactionInfo = "NA";
                                                    string strRequestDateTime = "NA";
                                                    string strTransactionFee = "NA";
                                                    string strCommission = "NA";

                                                    string strto_surcharge = "NA";
                                                    string strto_commission = "NA";
                                                    string strsender_phone = "NA";
                                                    string strsender_name = "NA";
                                                    string strsender_id_type = "NA";
                                                    string strsender_id_number = "NA";
                                                    string strreceiver_phone = "NA";
                                                    string strreceiver_name = "NA";
                                                    string strreceiver_id_type = "NA";
                                                    string strreceiver_id_number = "NA";
                                                    try
                                                    {
                                                        objODBC.executeNonQuery("call `updateResponseV3`('" + strTransactionID + "','" + intTopUpRecTransactionID + "',2)");
                                                    }
                                                    catch (Exception)
                                                    {

                                                    }
                                                    strMessage = "FAILED," + "Top Up Transaction is FAILED !.. " + strResponseDescription + "," + intTopUpRecTransactionID + "," + strResponseDescription + "";
                                                    try
                                                    {
                                                        try
                                                        {
                                                            strTransactionID = data.TransactionID;
                                                            strto_surcharge = data.to_surcharge;
                                                            strto_commission = data.to_commission;
                                                            strsender_phone = data.sender_phone;
                                                            strsender_name = data.sender_name;
                                                            strsender_id_type = data.sender_id_type;
                                                            strsender_id_number = data.sender_id_number;
                                                            strreceiver_phone = data.receiver_phone;
                                                            strreceiver_name = data.receiver_name;
                                                            strreceiver_id_type = data.receiver_id_type;
                                                            strreceiver_id_number = data.receiver_id_number;
                                                        }
                                                        catch (Exception)
                                                        {

                                                        }
                                                        try
                                                        {

                                                            strBalance = "0";
                                                            strConfirmationCode = data.ConfirmationCode;
                                                            strTransactionInfo = data.TransactionInfo;
                                                            strRequestDateTime = data.RequestDateTime;
                                                            strTransactionFee = data.TransactionFee;
                                                            strCommission = data.Commission;
                                                        }
                                                        catch (Exception)
                                                        {

                                                        }

                                                    }
                                                    catch (Exception)
                                                    {

                                                    }
                                                    try
                                                    {

                                                        objODBC.executeNonQuery("update er_recharge set operator_balance='" + strBalance.ToString() + "',ap_transid='" + strTransactionID + "',operator_transid='" + strTransactionID + "' where id='" + intTopUpRecTransactionID + "'");

                                                        objODBC.executeNonQuery("UPDATE er_salaam SET status=2,response_code='" + strResponseCode + "',response_description='" + strResponseDescription + "',transaction_id='" + strTransactionID + "',Balance='" + strBalance.ToString() + "',confirmation_code='" + strConfirmationCode + "',TransactionInfo='" + strTransactionInfo + "',RequestDateTime='" + strRequestDateTime + "',TransactionFee='" + strTransactionFee + "',Commission='" + strCommission + "',to_surcharge='" + strto_surcharge + "',to_commission='" + strto_commission + "',sender_phone='" + strsender_phone + "',sender_name='" + strsender_name + "',sender_id_type='" + strsender_id_type + "',sender_id_number='" + strsender_id_number + "',receiver_phone='" + strreceiver_phone + "',receiver_name='" + strreceiver_name + "',receiver_id_type='" + strreceiver_id_type + "',receiver_id_number='" + strreceiver_id_number + "',encrypted_request='" + strRequestEncrypted + "',encrypted_response='" + strResponseEncrypted + "' WHERE id='" + intSalaamID + "'");
                                                    }
                                                    catch (Exception)
                                                    {

                                                    }


                                                    try
                                                    {
                                                        if (strPhone.Length == 11)
                                                        {
                                                            strPhone = strPhone.Substring(2, strPhone.Length - 2);
                                                        }
                                                        SendSMPPSMS(intUserID, strUserName, strMobile, strExiomsOpName, intOperatorType, dblAmount, 2, intTopUpRecTransactionID.ToString(), strPhone);
                                                    }
                                                    catch (Exception)
                                                    {

                                                    }

                                                }
                                                return strMessage;
                                            }
                                            catch (Exception ex)
                                            {

                                                strMessage = "FAILED,Sorry Something Went Wrong Please try again...," + intTopUpRecTransactionID + ",NA";
                                                return strMessage;

                                            }

                                        }
                                        else
                                        {
                                            strMessage = "FAILED, Invalid Details" + intTopUpRecTransactionID + ",NA,NA";
                                            return strMessage;
                                        }
                                    }
                                    else
                                    {
                                        strMessage = "FAILED, Parent wallet balance limit extended !" + ",NA,NA";
                                        return strMessage;
                                    }
                                }
                                else
                                {
                                    strMessage = "FAILED, Invalid Details" + intTopUpRecTransactionID + ",NA,NA";
                                    return strMessage;
                                }

                            }
                            else
                            {
                                strMessage = "FAILED,Amount is too Low !,NA,NA";

                                try
                                {
                                    if (strPhone.Length == 11)
                                    {
                                        strPhone = strPhone.Substring(2, strPhone.Length - 2);
                                    }
                                    SendSMPPSMS(intUserID, strUserName, strMobile, strExiomsOpName, intOperatorType, dblAmount, 0, "", strPhone);
                                    return strMessage;
                                }
                                catch (Exception)
                                {
                                    return strMessage;
                                }

                            }
                            return strMessage;
                        }
                        else
                        {
                            strMessage = "FAILED,You do not have permission to recharge this operator,NA,NA";
                        }
                    }
                    else
                    {
                        strMessage = "FAILED,You do not have permission to recharge this operator,NA,NA";
                    }
                }
                else
                {
                    strMessage = "FAILED,Mobile Number is not Exist for This Operater !,NA,NA";

                    return strMessage;
                }
            }
            catch (Exception)
            {
                strMessage = "FAILED,Exception,NA,NA";
                return strMessage;
            }
            finally
            {

                dt.Dispose();
            }
            return strMessage;
        }


        public string TruncateAtWord(string input, int length)
        {
            if (input == null || input.Length < length)
                return input;
            int iNextSpace = input.LastIndexOf("", length, StringComparison.Ordinal);
            return string.Format("{0}", input.Substring(0, (iNextSpace > 0) ? iNextSpace : length).Trim());
        }

        public string MakeMobileRechargeAPI2(int intUserID, string strUserName, int intOperatorType, int intOperatorID, string strMobile, double dblAmount, string strExiomsOpName, double dblMyComm, double dblResellerComm, string strMyAPIType, string strIPAddress, int intDesignationType, string strMacAddress, string strOSDetails, string strIMEINo, string strGcmID, string strAPPVersion, int intSourceData, int intCreatedByType, string strPhone)
        {
            string strMessage = "";
            DataTable dt = new DataTable();
            // string strRequestType = "5"; For balance

            try
            {
                Regex objMobilePattern = new Regex(@"^(78|73)[\d]{7}$"); // Etisalat
                if (objMobilePattern.IsMatch(strMobile))
                {
                    int intTopupStatus = objODBC.executeScalar_int("SELECT `status` FROM `er_status_manager_operator` WHERE operator_id='" + intOperatorID + "'");
                    if (intTopupStatus == 1)
                    {

                        int intRechargeStatus = objODBC.executeScalar_int("SELECT `etisalat_status` FROM `er_login` WHERE userid='" + intUserID + "'");
                        if (intRechargeStatus == 1)
                        {


                            if (dblAmount > 0)
                            {



                                int intTopUpRecTransactionID = 0;
                                string strPRIVAPICOmment = "";
                                int intPrivAPIType = 0;
                                double dblCommissionAmt = dblAmount * dblMyComm / 100;
                                double dblDeductAmt = dblAmount - dblCommissionAmt;
                                double dblResellerDeductAmt = dblAmount * (1 - (dblResellerComm / 100));
                                int intParentID = objODBC.executeScalar_int("call `er_getParentID`('" + intUserID + "',@intParentID)");

                                dt = objODBC.getDataTable("call `ap_insert_mobile`('" + intUserID + "', '" + intOperatorType + "', '" + intOperatorID + "','" + strExiomsOpName + "','" + strMobile + "', '" + dblAmount + "','" + dblDeductAmt + "', '" + dblResellerDeductAmt + "', '" + dblCommissionAmt + "', '" + dblMyComm + "', '" + dblCommissionAmt + "', '" + strIPAddress + "','" + strOSDetails + "',  '" + intSourceData + "', '" + strGcmID + "','" + strIMEINo + "','" + strMyAPIType + "','" + intPrivAPIType + "','" + strPRIVAPICOmment + "','" + intParentID + "')");
                                if (dt.Rows.Count > 0)
                                {
                                    intTopUpRecTransactionID = Convert.ToInt32(dt.Rows[0][0].ToString());
                                    int intEtisalatID = Convert.ToInt32(dt.Rows[0][1].ToString());
                                    int intWalletID = Convert.ToInt32(dt.Rows[0][2].ToString());
                                    if (intWalletID > 0)
                                    {
                                        if ((intTopUpRecTransactionID > 0) && (intEtisalatID > 0))
                                        {
                                            try
                                            {
                                                EtisalatTopup objEtisalat = new EtisalatTopup();
                                                try
                                                {
                                                    objEtisalat.ETS_Topup(intTopUpRecTransactionID.ToString(), strMobile, int.Parse(dblAmount.ToString()), intEtisalatID, 1, "0");
                                                }
                                                catch (Exception)
                                                {

                                                }

                                                string strQ = "SELECT Transaction_Response FROM etisalat_topup_request WHERE recharge_id =" + intTopUpRecTransactionID + " order by id desc limit 1";

                                                string EtisalatResponse = objODBC.executeScalar_str(strQ);
                                                try
                                                {
                                                    if (strPhone.Length == 11)
                                                    {
                                                        strPhone = strPhone.Substring(2, strPhone.Length - 2);
                                                    }

                                                    objODBC.executeNonQuery("update er_recharge set request_mobile_no='" + strPhone + "' where id='" + intTopUpRecTransactionID + "'");
                                                }
                                                catch (Exception)
                                                {

                                                }

                                                if (EtisalatResponse.ToString() != "NA")
                                                {
                                                    strMessage = "PENDING, Your Transaction is in process... Please check topup report status! ," + intTopUpRecTransactionID + ",NA";
                                                    //if (Regex.IsMatch(EtisalatResponse, string.Format(@"\b{0}\b", Regex.Escape("ACNT:RETN=0000,DESC=SUCCESS"))))
                                                    //{
                                                    //    string strResposne = TruncateAtWord(EtisalatResponse, 64); // Truncate From Position of Words
                                                    //    string output = EtisalatResponse.Replace(strResposne, ""); // Replace Words From Data
                                                    //    output = output.Remove(output.Length - 8); // Deduct Words from Last                   
                                                    //    string StrCheckSum = EtisalatResponse.Substring(EtisalatResponse.Length - 8); // Getting Checksum

                                                    //    string[] strDescriptionResponse = output.Split(new char[] { ',' });
                                                    //    // strMessage = "2,OOPs! You do not have sufficient balance.,Failed,NA,NA";

                                                    //    string TRDStatus = strDescriptionResponse[0];
                                                    //    string TRDDescription = strDescriptionResponse[1]; ;
                                                    //    string BEFOROPER = strDescriptionResponse[2];
                                                    //    string FEE = strDescriptionResponse[3];
                                                    //    string AFTEROPER = strDescriptionResponse[4];
                                                    //    string CSVSTOP = strDescriptionResponse[5];

                                                    //    string TXNID = objODBC.executeScalar_str("SELECT transaction_ID_recharge FROM etisalat_topup_request WHERE recharge_id='" + intTopUpRecTransactionID + "'").ToString();

                                                    //    objODBC.executeNonQuery("call `updateResponseV3`('" + TXNID + "','" + intTopUpRecTransactionID + "',1)");
                                                    //    strMessage = "SUCCESS," + "Top Up Transaction is Successfull ! ," + intTopUpRecTransactionID + "," + TXNID;

                                                    //    try
                                                    //    {
                                                    //        objODBC.executeNonQuery("update etisalat_topup_request set transaction_DESC='" + TRDDescription + "',Transactio_Status='" + TRDStatus + "',BEFOROPER='" + BEFOROPER + "',FEE='" + FEE + "',AFTEROPER='" + AFTEROPER + "','" + CSVSTOP + "',Trans_Receive_Checksum='" + StrCheckSum + "',status=1 WHERE recharge_id =" + intTopUpRecTransactionID + "");
                                                    //    }
                                                    //    catch (Exception)
                                                    //    {

                                                    //    }


                                                    //    try
                                                    //    {
                                                    //        string strBalance = objODBC.executeScalar_str("SELECT amt FROM `er_sys_wallet` where id=2;");
                                                    //        objODBC.executeNonQuery("update er_recharge set comment='" + EtisalatResponse + "',operator_balance='" + strBalance + "' where id='" + intTopUpRecTransactionID + "'");
                                                    //    }
                                                    //    catch (Exception)
                                                    //    {

                                                    //    }



                                                    //    try
                                                    //    {
                                                    //        if (strPhone.Length == 11)
                                                    //        {
                                                    //            strPhone = strPhone.Substring(2, strPhone.Length - 2);
                                                    //        }
                                                    //        SendSMPPSMS(intUserID, strUserName, strMobile, strExiomsOpName, intOperatorType, dblAmount, 1, intTopUpRecTransactionID.ToString(),strPhone);
                                                    //        SMPP_SMS_Format objSMPP_SMS_Format = new SMPP_SMS_Format();
                                                    //        string strGetSMS = objSMPP_SMS_Format.TopupSuccessEndUser(objODBC.executeScalar_str("SELECT full_name From er_login Where userid='" + intUserID + "'").ToString(), strMobile, dblAmount, intTopUpRecTransactionID.ToString());
                                                    //        objCOMFUN.er_SMS_Alert(intUserID, 2, strMobile, 2, strGetSMS, 9, "");
                                                    //    }
                                                    //    catch (Exception)
                                                    //    {

                                                    //    }
                                                    //}
                                                    //else
                                                    //{
                                                    //    string strResposne = TruncateAtWord(EtisalatResponse, 64); // Truncate From Position of Words
                                                    //    string output = EtisalatResponse.Replace(strResposne, ""); // Replace Words From Data
                                                    //    output = output.Remove(output.Length - 8); // Deduct Words from Last                   
                                                    //    string StrCheckSum = EtisalatResponse.Substring(EtisalatResponse.Length - 8); // Getting Checksum

                                                    //    string[] strDescriptionResponse = output.Split(new char[] { ',' });
                                                    //    // strMessage = "2,OOPs! You do not have sufficient balance.,Failed,NA,NA";

                                                    //    string TRDStatus = strDescriptionResponse[0];
                                                    //    string TRDDescription = strDescriptionResponse[1];


                                                    //    string TXNID = objODBC.executeScalar_str("SELECT transaction_ID_recharge FROM etisalat_topup_request WHERE recharge_id='" + intTopUpRecTransactionID + "'").ToString();

                                                    //    objODBC.executeNonQuery("call `updateResponseV3`('" + TXNID + "','" + intTopUpRecTransactionID + "',2)");
                                                    //    strMessage = "FAILED," + "Top Up Transaction is FAILED !.. " + TRDDescription + "," + intTopUpRecTransactionID + "," + TXNID + "";

                                                    //    try
                                                    //    {
                                                    //        string strBalance = objODBC.executeScalar_str("SELECT amt FROM `er_sys_wallet` where id=2;");
                                                    //        objODBC.executeNonQuery("update er_recharge set comment='" + EtisalatResponse + "',operator_balance='" + strBalance + "' where id='" + intTopUpRecTransactionID + "'");
                                                    //    }
                                                    //    catch (Exception)
                                                    //    {

                                                    //    }

                                                    //    try
                                                    //    {
                                                    //        objODBC.executeNonQuery("update etisalat_topup_request set transaction_DESC='" + TRDDescription + "',Transactio_Status='" + TRDStatus + "',Trans_Receive_Checksum='" + StrCheckSum + "',status=2 WHERE recharge_id =" + intTopUpRecTransactionID + "");
                                                    //    }
                                                    //    catch (Exception)
                                                    //    {

                                                    //    }

                                                    //    try
                                                    //    {
                                                    //        if (strPhone.Length == 11)
                                                    //        {
                                                    //            strPhone = strPhone.Substring(2, strPhone.Length - 2);
                                                    //        }
                                                    //        SendSMPPSMS(intUserID, strUserName, strMobile, strExiomsOpName, intOperatorType, dblAmount, 2, intTopUpRecTransactionID.ToString(),strPhone);

                                                    //        //SMPP_SMS_Format objSMPP_SMS_Format = new SMPP_SMS_Format();
                                                    //        //string strGetSMS = objSMPP_SMS_Format.TopupFailedEndUser(objODBC.executeScalar_str("SELECT full_name From er_login Where userid='" + intUserID + "'").ToString(), strMobile, dblAmount, intTopUpRecTransactionID.ToString());
                                                    //        //objCOMFUN.er_SMS_Alert(intUserID, 2, strMobile, 2, strGetSMS, 9, "");
                                                    //    }
                                                    //    catch (Exception)
                                                    //    {

                                                    //    }



                                                    //}
                                                }
                                                else
                                                {
                                                    strMessage = "PENDING, Your Transaction is in process... Please check topup report status! ," + intTopUpRecTransactionID + ",NA";
                                                    //try
                                                    //{
                                                    //    SendSMPPSMS(intUserID, strUserName, strMobile, strExiomsOpName, intOperatorType, dblAmount, 3, intTopUpRecTransactionID.ToString());


                                                    //}
                                                    //catch (Exception)
                                                    //{

                                                    //}

                                                }

                                                return strMessage;
                                            }
                                            catch (Exception ex)
                                            {
                                                strMessage = "FAILED,Sorry Something Went Wrong Please try again...," + intTopUpRecTransactionID + ",NA";
                                                return strMessage;

                                            }
                                        }
                                        else
                                        {

                                            strMessage = "FAILED," + "Top Up Transaction is FAILED !..," + intTopUpRecTransactionID + ",NA";
                                            return strMessage;
                                        }
                                    }
                                    else
                                    {
                                        strMessage = "FAILED, Parent wallet balance limit extended !" + ",NA,NA";
                                        return strMessage;
                                    }
                                }
                                else
                                {
                                    strMessage = "FAILED, Invalid Details" + intTopUpRecTransactionID + ",NA,NA";
                                    return strMessage;
                                }

                                //}

                                //else
                                //{
                                //    strMessage = "FAILED,Amount is less then 1000 !,NA,NA";
                                //    try
                                //    {
                                //        SendSMPPSMS(intUserID, strUserName, strMobile, strExiomsOpName, intOperatorType, dblAmount, 0, "");
                                //    }
                                //    catch (Exception)
                                //    {

                                //    }
                                //    return strMessage;
                                //}
                            }
                            else
                            {
                                strMessage = "FAILED,Amount is too Low !,NA,NA";
                                try
                                {
                                    if (strPhone.Length == 11)
                                    {
                                        strPhone = strPhone.Substring(2, strPhone.Length - 2);
                                    }
                                    SendSMPPSMS(intUserID, strUserName, strMobile, strExiomsOpName, intOperatorType, dblAmount, 0, "", strPhone);
                                }
                                catch (Exception)
                                {

                                }
                                return strMessage;
                            }
                        }
                        else
                        {
                            strMessage = "FAILED,You do not have permission to recharge this operator,NA,NA";
                        }
                    }
                    else
                    {
                        strMessage = "FAILED,You do not have permission to recharge this operator,NA,NA";
                    }
                }
                else
                {
                    strMessage = "FAILED,Mobile Number is not Exist for This Operater !,NA,NA";

                    return strMessage;
                }
            }
            catch (Exception)
            {
                strMessage = "FAILED,Exception,NA,NA";
                return strMessage;
            }
            finally
            {
                dt.Dispose();
            }

            return strMessage;
        }

        // Roshan Recharge TopUp
        public string MakeMobileRechargeAPI3(int intUserID, string strUserName, int intOperatorType, int intOperatorID, string strMobile, double dblAmount, string strExiomsOpName, double dblMyComm, double dblResellerComm, string strMyAPIType, string strIPAddress, int intDesignationType, string strMacAddress, string strOSDetails, string strIMEINo, string strGcmID, string strAPPVersion, int intSourceData, int intCreatedByType, string strPhone)
        {
            string strMessage = "";
            string StrEndUserCoustomerMob = "";
            DataTable dt = new DataTable();
            // string strRequestType = "5"; For balance

            try
            {
                Regex objMobilePattern = new Regex(@"^(79|72)[\d]{7}$");
                if (objMobilePattern.IsMatch(strMobile))
                {
                    int intTopupStatus = objODBC.executeScalar_int("SELECT `status` FROM `er_status_manager_operator` WHERE operator_id='" + intOperatorID + "'");
                    if (intTopupStatus == 1)
                    {
                        int intRechargeStatus = objODBC.executeScalar_int("SELECT `roshan_status` FROM `er_login` WHERE userid='" + intUserID + "'");
                        if (intRechargeStatus == 1)
                        {

                            if (dblAmount >= 25)
                            {
                                StrEndUserCoustomerMob = strMobile;
                                strMobile = "0" + strMobile;
                                Roshan objRoshan = new Roshan();

                                int intTopUpRecTransactionID = 0;
                                string strPRIVAPICOmment = "";
                                int intPrivAPIType = 0;
                                double dblCommissionAmt = dblAmount * dblMyComm / 100;
                                double dblDeductAmt = dblAmount - dblCommissionAmt;
                                double dblResellerDeductAmt = dblAmount * (1 - (dblResellerComm / 100));

                                int intParentID = objODBC.executeScalar_int("call `er_getParentID`('" + intUserID + "',@intParentID)");

                                dt = objODBC.getDataTable("call `ap_insert_mobile`('" + intUserID + "', '" + intOperatorType + "', '" + intOperatorID + "','" + strExiomsOpName + "','" + strMobile + "', '" + dblAmount + "','" + dblDeductAmt + "', '" + dblResellerDeductAmt + "', '" + dblCommissionAmt + "', '" + dblMyComm + "', '" + dblCommissionAmt + "', '" + strIPAddress + "','" + strOSDetails + "',  '" + intSourceData + "', '" + strGcmID + "','" + strIMEINo + "','" + strMyAPIType + "','" + intPrivAPIType + "','" + strPRIVAPICOmment + "','" + intParentID + "')");
                                if (dt.Rows.Count > 0)
                                {
                                    intTopUpRecTransactionID = Convert.ToInt32(dt.Rows[0][0].ToString());
                                    int intRoshanID = Convert.ToInt32(dt.Rows[0][1].ToString());
                                    int intWalletID = Convert.ToInt32(dt.Rows[0][2].ToString());
                                    if (intWalletID > 0)
                                    {
                                        if ((intTopUpRecTransactionID > 0) && (intRoshanID > 0))
                                        {
                                            try
                                            {
                                                string RoshanResponse = "NA";
                                                try
                                                {
                                                    RoshanResponse = objRoshan.RoshanTopUp(intRoshanID, strMobile, dblAmount.ToString());
                                                }
                                                catch (Exception)
                                                {

                                                }


                                                // var RoshanResponse = objODBC.executeScalar_str("SELECT data FROM temp WHERE id=1").ToString();
                                                XmlDocument xmlDoc = new XmlDocument();
                                                xmlDoc.LoadXml(RoshanResponse);
                                                try
                                                {
                                                    if (strPhone.Length == 11)
                                                    {
                                                        strPhone = strPhone.Substring(2, strPhone.Length - 2);
                                                    }
                                                    objODBC.executeNonQuery("update er_recharge set comment='" + RoshanResponse.ToString() + "',request_mobile_no='" + strPhone + "' where id='" + intTopUpRecTransactionID + "'");
                                                }
                                                catch (Exception)
                                                {

                                                }
                                                string strTXNSTATUS = "";
                                                try
                                                {
                                                    strTXNSTATUS = xmlDoc.DocumentElement.ChildNodes[2].ChildNodes[0].Value.ToString(); // TXNSTATUS
                                                }
                                                catch (Exception)
                                                {
                                                }

                                                if (strTXNSTATUS == "200") // Success
                                                {


                                                    //<? xml version = "1.0" ?>
                                                    //< COMMAND >
                                                    //    < TYPE > RTMMRESP </ TYPE >
                                                    //    < TXNID > RC181220.1704.A06626 </ TXNID >
                                                    //    < TXNSTATUS > 200 </ TXNSTATUS >
                                                    //    < TRID > 0799066145201812201704A9301 </ TRID >

                                                    //       < MESSAGE > Transaction successful.Type : Buy Airtime, Mobile Number: 0799477575, Amount  25 AFA, Txn ID:RC181220.1704.A06626, New balance: 52501.4276 AFA </ MESSAGE >

                                                    //        < IVR - RESPONSE > 1065#0799477575|25|0|0|RC181220.1704.A06626|52501.4276#</IVR-RESPONSE>
                                                    //</ COMMAND >



                                                    string TXNID = " NA";
                                                    string strTYPE = "NA";
                                                    string strTRID = "NA";
                                                    string strRoshanMessage = "NA";
                                                    string strIVR_RESPONSE = "NA";
                                                    string strBalance = "NA";

                                                    TXNID = xmlDoc.DocumentElement.ChildNodes[1].ChildNodes[0].Value.ToString(); //TXNID = Transaction ID

                                                    try
                                                    {
                                                        objODBC.executeNonQuery("call `updateResponseV3`('" + TXNID + "','" + intTopUpRecTransactionID + "',1)");
                                                    }
                                                    catch (Exception)
                                                    {

                                                    }
                                                    strMessage = "SUCCESS," + "Top Up Transaction is Successfull ! ," + intTopUpRecTransactionID + "," + strIVR_RESPONSE;
                                                    try
                                                    {

                                                        strTYPE = xmlDoc.DocumentElement.ChildNodes[0].ChildNodes[0].Value.ToString(); // TYPE

                                                        strTRID = xmlDoc.DocumentElement.ChildNodes[3].ChildNodes[0].Value.ToString(); // TRID = Transfer ID

                                                        strRoshanMessage = xmlDoc.DocumentElement.ChildNodes[4].ChildNodes[0].Value.ToString(); // MESSAGE

                                                        strIVR_RESPONSE = xmlDoc.DocumentElement.ChildNodes[5].ChildNodes[0].Value.ToString(); // IVR - RESPONSE
                                                        try
                                                        {
                                                            string[] strCommissionStructure = strRoshanMessage.Split(new char[] { ',' });
                                                            string strOpMessage = strCommissionStructure[4];
                                                            var replacements = new[] { new { Find = " New  balance: ", Replace = "0" }, new { Find = " AFA", Replace = "" }, };
                                                            var myLongString = strOpMessage;
                                                            foreach (var set in replacements)
                                                            {
                                                                myLongString = myLongString.Replace(set.Find, set.Replace);
                                                            }
                                                            strBalance = myLongString;
                                                        }
                                                        catch (Exception)
                                                        {

                                                        }
                                                        try
                                                        {
                                                            objODBC.executeNonQuery("update er_recharge set operator_balance='" + strBalance + "' where id='" + intTopUpRecTransactionID + "'");
                                                        }
                                                        catch (Exception)
                                                        {

                                                        }
                                                    }
                                                    catch (Exception)
                                                    {

                                                    }



                                                    try
                                                    {
                                                        objODBC.executeNonQuery("update er_roshan_request set txnID='" + TXNID + "',trID='" + strTRID + "',type='" + strTYPE + "',trans_date='" + objDateFor.getCurDateTimeString() + "',message='" + strRoshanMessage + "',IVR_response='" + strIVR_RESPONSE + "',status=1,TXNSTATUS='" + strTXNSTATUS + "' where id='" + intRoshanID + "'");

                                                    }
                                                    catch (Exception)
                                                    {

                                                    }
                                                    try
                                                    {
                                                        if (strPhone.Length == 11)
                                                        {
                                                            strPhone = strPhone.Substring(2, strPhone.Length - 2);
                                                        }
                                                        SendSMPPSMS(intUserID, strUserName, strMobile, strExiomsOpName, intOperatorType, dblAmount, 1, intTopUpRecTransactionID.ToString(), strPhone);


                                                        SMPP_SMS_Format objSMPP_SMS_Format = new SMPP_SMS_Format();
                                                        CommonFunction objCOMFUN = new CommonFunction();
                                                        string strGetSMS = objSMPP_SMS_Format.TopupSuccessEndUser(objODBC.executeScalar_str("SELECT full_name From er_login Where userid='" + intUserID + "'").ToString(), StrEndUserCoustomerMob, dblAmount, intTopUpRecTransactionID.ToString());
                                                        objCOMFUN.er_SMS_Alert(intUserID, 2, StrEndUserCoustomerMob, 3, strGetSMS, 9, "");
                                                    }
                                                    catch (Exception)
                                                    {

                                                    }

                                                    return strMessage;
                                                }
                                                else //Failed
                                                {
                                                    //                                        <? xml version = "1.0" ?>
                                                    //< COMMAND >
                                                    //    < TYPE > RTMMRESP </ TYPE >
                                                    //    < TXNID > RC181220.1700.A06329 </ TXNID >
                                                    //    < TXNSTATUS > 10038 </ TXNSTATUS >
                                                    //    < TRID > 0799066145201812201700A8967 </ TRID >

                                                    //       < MESSAGE > Amount less than the flexible amount defined for the operator.</ MESSAGE >

                                                    //          < IVR - RESPONSE > 10038##</IVR-RESPONSE>
                                                    //</ COMMAND >
                                                    //                                          int strPayStatus = 3;

                                                    string TXNID = " NA";
                                                    string strTYPE = "NA";
                                                    string strTRID = "NA";
                                                    string strRoshanMessage = "NA";
                                                    string strIVR_RESPONSE = "NA";
                                                    string strBalance = "0";
                                                    try
                                                    {
                                                        TXNID = xmlDoc.DocumentElement.ChildNodes[1].ChildNodes[0].Value.ToString(); //TXNID = Transaction ID

                                                        strTYPE = xmlDoc.DocumentElement.ChildNodes[0].ChildNodes[0].Value.ToString(); // TYPE

                                                        strTRID = xmlDoc.DocumentElement.ChildNodes[3].ChildNodes[0].Value.ToString(); // TRID = Transfer ID

                                                        strRoshanMessage = xmlDoc.DocumentElement.ChildNodes[4].ChildNodes[0].Value.ToString(); // MESSAGE

                                                        strIVR_RESPONSE = xmlDoc.DocumentElement.ChildNodes[5].ChildNodes[0].Value.ToString(); // IVR - RESPONSE



                                                        try
                                                        {
                                                            string[] strCommissionStructure = strRoshanMessage.Split(new char[] { ',' });
                                                            string strOpMessage = strCommissionStructure[4];
                                                            var replacements = new[] { new { Find = " New  balance: ", Replace = "0" }, new { Find = " AFA", Replace = "" }, };
                                                            var myLongString = strOpMessage;
                                                            foreach (var set in replacements)
                                                            {
                                                                myLongString = myLongString.Replace(set.Find, set.Replace);
                                                            }
                                                            strBalance = myLongString;
                                                        }
                                                        catch (Exception)
                                                        {

                                                        }
                                                        try
                                                        {
                                                            objODBC.executeNonQuery("update er_recharge set operator_balance='" + strBalance + "' where id='" + intTopUpRecTransactionID + "'");
                                                        }
                                                        catch (Exception)
                                                        {

                                                        }

                                                    }
                                                    catch (Exception)
                                                    {

                                                    }
                                                    try
                                                    {
                                                        objODBC.executeNonQuery("call `updateResponseV3`('" + TXNID + "','" + intTopUpRecTransactionID + "',2)");
                                                    }
                                                    catch (Exception)
                                                    {

                                                    }
                                                    strMessage = "FAILED," + "Top Up Transaction is FAILED !.. " + strRoshanMessage + "," + intTopUpRecTransactionID + "," + strRoshanMessage + "";

                                                    try
                                                    {
                                                        objODBC.executeNonQuery("update er_roshan_request set txnID='" + TXNID + "',trID='" + strTRID + "',type='" + strTYPE + "',trans_date='" + objDateFor.getCurDateTimeString() + "',message='" + strRoshanMessage + "',IVR_response='" + strIVR_RESPONSE + "',status=1,TXNSTATUS='" + strTXNSTATUS + "' where id='" + intRoshanID + "'");

                                                        //   objODBC.executeNonQuery("INSERT INTO `temp`( `data`, `type`, `request`) VALUES('4','3','" + strMessage + "')");
                                                    }
                                                    catch (Exception)
                                                    { }

                                                    try
                                                    {
                                                        if (strPhone.Length == 11)
                                                        {
                                                            strPhone = strPhone.Substring(2, strPhone.Length - 2);
                                                        }
                                                        SendSMPPSMS(intUserID, strUserName, strMobile, strExiomsOpName, intOperatorType, dblAmount, 2, intTopUpRecTransactionID.ToString(), strPhone);

                                                        //SMPP_SMS_Format objSMPP_SMS_Format = new SMPP_SMS_Format();
                                                        //CommonFunction objCOMFUN = new CommonFunction();
                                                        //string strGetSMS = objSMPP_SMS_Format.TopupFailedEndUser(objODBC.executeScalar_str("SELECT full_name From er_login Where userid='" + intUserID + "'").ToString(), StrEndUserCoustomerMob, dblAmount, intTopUpRecTransactionID.ToString());
                                                        //objCOMFUN.er_SMS_Alert(intUserID, 2, StrEndUserCoustomerMob, 3, strGetSMS, 9, "");
                                                    }
                                                    catch (Exception)
                                                    {

                                                    }
                                                    return strMessage;

                                                }
                                                return strMessage;
                                            }
                                            catch (Exception ex)
                                            {
                                                //    objODBC.executeNonQuery("call `updateResponseV3`('','" + intTopUpRecTransactionID + "',2)");
                                                strMessage = "PENDING,Something Went Wrong kindly contact to admin...," + intTopUpRecTransactionID + ",NA";
                                                return strMessage;

                                            }
                                        }
                                        else
                                        {
                                            strMessage = "FAILED, Invalid Details" + intTopUpRecTransactionID + ",NA,NA";
                                            return strMessage;
                                        }
                                    }
                                    else
                                    {
                                        strMessage = "FAILED, Parent wallet balance limit extended !" + ",NA,NA";
                                        return strMessage;
                                    }
                                }
                                else
                                {
                                    strMessage = "FAILED, Invalid Details" + intTopUpRecTransactionID + ",NA,NA";
                                    return strMessage;
                                }

                            }
                            else
                            {
                                strMessage = "FAILED,Amount is too Low !,NA,NA";
                                try
                                {
                                    if (strPhone.Length == 11)
                                    {
                                        strPhone = strPhone.Substring(2, strPhone.Length - 2);
                                    }
                                    SendSMPPSMS(intUserID, strUserName, strMobile, strExiomsOpName, intOperatorType, dblAmount, 0, "", strPhone);
                                }
                                catch (Exception)
                                {

                                }
                                return strMessage;
                            }
                        }
                        else
                        {
                            strMessage = "FAILED,You do not have permission to recharge this operator,NA,NA";
                        }
                    }
                    else
                    {
                        strMessage = "FAILED,You do not have permission to recharge this operator,NA,NA";
                    }
                }
                else
                {
                    strMessage = "FAILED,Mobile Number is not Exist for This Operater !,NA,NA";

                    return strMessage;
                }
            }
            catch (Exception)
            {
                strMessage = "FAILED,Exception,NA,NA";
                return strMessage;
            }
            finally
            {
                dt.Dispose();
            }
            return strMessage;
        }

        // Easy pay Recharge 
        public string MakeMobileRechargeAPI4(int intUserID, string strUserName, int intOperatorType, int intOperatorID, string strMobile, double dblAmount, string strExiomsOpName, double dblMyComm, double dblResellerComm, string strMyAPIType, string strIPAddress, int intDesignationType, string strMacAddress, string strOSDetails, string strIMEINo, string strGcmID, string strAPPVersion, int intSourceData, int intCreatedByType, string strPhone)
        {

            string strMessage = "";
            DataTable dt = new DataTable();
            // string strRequestType = "5"; For balance

            try
            {
                string[] strApiTypeData = getEasyPaySEVID(intOperatorID, strMobile).Split(new char[] { ',' });
                string strSEVID = strApiTypeData[0].ToString();
                double dblMinAmount = double.Parse(strApiTypeData[1].ToString());
                double dblMaximumAmount = double.Parse(strApiTypeData[2].ToString());
                string strMatchMob = strApiTypeData[3].ToString();
                if (strMatchMob == "1")
                {
                    if ((strSEVID != "0") && (dblMinAmount <= dblAmount) && (dblMaximumAmount >= dblAmount))
                    {
                        strMobile = "0" + strMobile;
                        EasyPay objEasypay = new EasyPay();
                        string strPassword = "set2018";
                        string strTerminalID = "347922";
                        string strLoginID = "mutahedapi";
                        int intTopUpRecTransactionID = 0;
                        string strPRIVAPICOmment = "";
                        int intPrivAPIType = 0;
                        double dblCommissionAmt = dblAmount * dblMyComm / 100;
                        double dblDeductAmt = dblAmount - dblCommissionAmt;
                        double dblResellerDeductAmt = dblAmount * (1 - (dblResellerComm / 100));


                        Regex objMobilePatternMTN = new Regex(@"^(077|076)[\d]{7}$");
                        Regex objMobilePatternAWCC = new Regex(@"^(070|071)[\d]{7}$");

                        int intRechargeStatus = 0;



                        if (objMobilePatternMTN.IsMatch(strMobile))
                        {
                            strExiomsOpName = "MTN";
                            intRechargeStatus = objODBC.executeScalar_int("SELECT `mtn_status` FROM `er_login` WHERE userid='" + intUserID + "'");
                        }
                        if (objMobilePatternAWCC.IsMatch(strMobile))
                        {
                            strExiomsOpName = "AWCC";
                            intRechargeStatus = objODBC.executeScalar_int("SELECT `awcc_status` FROM `er_login` WHERE userid='" + intUserID + "'");
                        }

                        int intTopupStatus = objODBC.executeScalar_int("SELECT `status` FROM `er_status_manager_operator` WHERE operator_id='" + intOperatorID + "'");
                        if (intTopupStatus == 1)
                        {
                            if (intRechargeStatus == 1)
                            {
                                int intParentID = objODBC.executeScalar_int("call `er_getParentID`('" + intUserID + "',@intParentID)");

                                dt = objODBC.getDataTable("call `ap_insert_mobile`('" + intUserID + "', '" + intOperatorType + "', '" + intOperatorID + "','" + strExiomsOpName + "','" + strMobile + "', '" + dblAmount + "','" + dblDeductAmt + "', '" + dblResellerDeductAmt + "', '" + dblCommissionAmt + "', '" + dblMyComm + "', '" + dblCommissionAmt + "', '" + strIPAddress + "','" + strOSDetails + "',  '" + intSourceData + "', '" + strGcmID + "','" + strIMEINo + "','" + strMyAPIType + "','" + intPrivAPIType + "','" + strPRIVAPICOmment + "','" + intParentID + "')");
                                if (dt.Rows.Count > 0)
                                {
                                    intTopUpRecTransactionID = Convert.ToInt32(dt.Rows[0][0].ToString());
                                    int intEasypayID = Convert.ToInt32(dt.Rows[0][1].ToString());
                                    int intWalletID = Convert.ToInt32(dt.Rows[0][2].ToString());
                                    if (intWalletID > 0)
                                    {
                                        if ((intTopUpRecTransactionID > 0) && (intEasypayID > 0))
                                        {
                                            string MD5Password = objEncrypMD5SHA1.MD5Hash(strPassword);
                                            string strSign = objEncrypMD5SHA1.SHA1Hash(MD5Password + intTopUpRecTransactionID + strMobile).ToString();

                                            // string strRequestType = "1"; For TopUP
                                            //  var EasyPayResponse = objEasypay.getBalance(strTerminalID, MD5Password, "5", strLoginID);

                                            try
                                            {
                                                var EasyPayResponse = objEasypay.ESPY_Transaction(intEasypayID, strTerminalID, MD5Password, "1", strLoginID, intTopUpRecTransactionID.ToString(), strSign, strSEVID, strMobile, dblAmount.ToString(), dblDeductAmt.ToString());
                                                //   objODBC.executeNonQuery("update er_recharge set comment='" + EasyPayResponse.ToString() + "' where id='" + intTopUpRecTransactionID + "'");
                                                XmlDocument xmlDoc = new XmlDocument();
                                                xmlDoc.LoadXml(EasyPayResponse);
                                                // objODBC.executeNonQuery(" insert into temp(`data`,`type`,`created_on`,`request`) values('1',1,'2019-01-29 19:58:58','5')");
                                                XmlNode paymentItem = xmlDoc.DocumentElement.ChildNodes[0];
                                                try
                                                {
                                                    if (strPhone.Length == 11)
                                                    {
                                                        strPhone = strPhone.Substring(2, strPhone.Length - 2);
                                                    }
                                                    objODBC.executeNonQuery("update er_recharge set comment='" + EasyPayResponse.ToString() + "',request_mobile_no='" + strPhone + "' where id='" + intTopUpRecTransactionID + "'");
                                                }
                                                catch (Exception)
                                                {

                                                }
                                                string strStatusCode = paymentItem.Attributes["status-code"].Value.ToString();
                                                string strStatuTxnDate = paymentItem.Attributes["txn-date"].Value.ToString();
                                                string strPayStatus = "NA";
                                                string strPayID = "NA";
                                                string strDiscription_P = "NA";

                                                if (strStatusCode == "100") //In Process
                                                {

                                                    try
                                                    {
                                                        strPayStatus = paymentItem.Attributes["payment-state"].Value;
                                                        strPayID = paymentItem.Attributes["payid"].Value.ToString();
                                                        strDiscription_P = paymentItem.Attributes["status-desc"].Value.ToString();
                                                    }
                                                    catch (Exception)
                                                    {

                                                    }
                                                    if (strStatusCode == "100" && strPayStatus == "1") // Success
                                                    {
                                                        try
                                                        {
                                                            objODBC.executeNonQuery("call `updateResponseV3`('" + strPayID + "','" + intTopUpRecTransactionID + "',1)");
                                                        }
                                                        catch (Exception)
                                                        {

                                                        }
                                                        //    objODBC.executeNonQuery(" insert into temp(`data`,`type`,`created_on`,`request`) values(" + strPayID + "," + intTopUpRecTransactionID + ",'2019-01-29 19:58:58','5')");
                                                        strMessage = "SUCCESS," + "Top Up Transaction is Successfull ! ," + intTopUpRecTransactionID + ",NA";

                                                        objODBC.executeNonQuery("update er_recharge set ap_transid='" + strPayID + "',operator_transid='" + strPayID + "' where id='" + intTopUpRecTransactionID + "'");



                                                        string strStatusDescription = "NA";
                                                        try
                                                        {
                                                            strStatusDescription = paymentItem.Attributes["payid"].Value;
                                                        }
                                                        catch (Exception)
                                                        {

                                                        }

                                                        string strRequestType_P = "NA";
                                                        string strTerminalIDRes_P = "NA";
                                                        double strCurrencyRate_P = 0;
                                                        double strBalance_P = 0;
                                                        double strOverdraft_P = 0;
                                                        double strWithheld_P = 0;
                                                        double strPayable_P = 0;

                                                        try
                                                        {
                                                            strRequestType_P = xmlDoc.DocumentElement.ChildNodes[1].ChildNodes[0].Value;
                                                            strTerminalIDRes_P = xmlDoc.DocumentElement.ChildNodes[2].ChildNodes[0].Value;
                                                            strCurrencyRate_P = double.Parse(xmlDoc.DocumentElement.ChildNodes[3].ChildNodes[0].Value);
                                                            strBalance_P = double.Parse(xmlDoc.DocumentElement.ChildNodes[4].ChildNodes[0].Value);
                                                            strOverdraft_P = double.Parse(xmlDoc.DocumentElement.ChildNodes[5].ChildNodes[0].Value);
                                                            strWithheld_P = double.Parse(xmlDoc.DocumentElement.ChildNodes[6].ChildNodes[0].Value);
                                                            strPayable_P = double.Parse(xmlDoc.DocumentElement.ChildNodes[7].ChildNodes[0].Value);
                                                        }
                                                        catch (Exception)
                                                        {

                                                        }

                                                        try
                                                        {
                                                            objODBC.executeNonQuery("update er_recharge set operator_balance='" + strBalance_P + "' where id='" + intTopUpRecTransactionID + "'");
                                                        }
                                                        catch (Exception)
                                                        {

                                                        }
                                                        try
                                                        {
                                                            objODBC.executeNonQuery("UPDATE `er_easy_pay_request` set `terminal_id`='" + strTerminalIDRes_P + "', `request_type`='" + strRequestType_P + "', `sign`='" + strSign + "', `srvid`='" + strSEVID + "',`mobile_number`='" + strMobile + "', `amount`='" + dblAmount.ToString() + "', `net_amount`=0, `payid_responseid`='" + strPayID + "', `payment_state`='" + strPayStatus + "', `status_code`='" + strStatusCode + "', `status_desc`='', `txn_date`='" + strStatuTxnDate + "',`currency_rate`='" + strCurrencyRate_P + "', `balance`='" + strBalance_P + "', `overdraft`='" + strOverdraft_P + "', `withheld`='" + strWithheld_P + "', `payable`='" + strPayable_P + "', `status_desc`='" + strDiscription_P + "',`Response`='" + EasyPayResponse + "',`status`=1 where id='" + intEasypayID + "'");
                                                            // Fun SMS
                                                        }
                                                        catch
                                                        {

                                                        }

                                                        try
                                                        {
                                                            SendSMPPSMS(intUserID, strUserName, strMobile, strExiomsOpName, intOperatorType, dblAmount, 1, intTopUpRecTransactionID.ToString(), strPhone);
                                                        }
                                                        catch (Exception)
                                                        {

                                                        }

                                                        return strMessage;
                                                    }

                                                    else if (strStatusCode == "100" && strPayStatus == "0")  // Pending
                                                    {
                                                        // <? xml version = "1.0" encoding = "utf-8" ?>   
                                                        //< response >   
                                                        //  < payment payid = "24055190" payment - state = "0" status - code = "100" txn - date = "2019-02-06T10:29:13Z" >              
                                                        //               < to >              
                                                        //                 < srvid > 78 </ srvid >              
                                                        //                 < account > 0774441111 </ account >              
                                                        //                 < amount > 25.00 </ amount >              
                                                        //                 < amount - net > 24.50 </ amount - net >              
                                                        //               </ to >              
                                                        //             </ payment >              
                                                        //             < request - type > 1 </ request - type >              
                                                        //             < terminal - id > 347922 </ terminal - id >              
                                                        //             < currency - rate > 65.0598 </ currency - rate >              
                                                        //             < balance > 984.94 </ balance >              
                                                        //             < overdraft > 0.00 </ overdraft >              
                                                        //             < withheld > 24.50 </ withheld >              
                                                        //             < payable > 960.44 </ payable >              
                                                        //           </ response >

                                                        objODBC.executeNonQuery("update er_recharge set ap_transid='" + strPayID + "',operator_transid='" + strPayID + "' where id='" + intTopUpRecTransactionID + "'");

                                                        strMessage = "PENDING," + "Your Transaction is in Process... Please check topup report status! ," + intTopUpRecTransactionID + "," + strPayID;
                                                        string strRequestType_F = "NA";
                                                        string strTerminalIDRes_F = "NA";
                                                        string strDiscription_F = "NA";
                                                        string strBalance_P = "NA";
                                                        try
                                                        {
                                                            strRequestType_F = xmlDoc.DocumentElement.ChildNodes[1].ChildNodes[0].Value.ToString();
                                                            strTerminalIDRes_F = xmlDoc.DocumentElement.ChildNodes[2].ChildNodes[0].Value.ToString();
                                                            strDiscription_F = paymentItem.Attributes["status-desc"].Value.ToString();
                                                            strBalance_P = xmlDoc.DocumentElement.ChildNodes[4].ChildNodes[0].Value;

                                                        }
                                                        catch (Exception)
                                                        {

                                                        }

                                                        try
                                                        {
                                                            objODBC.executeNonQuery("update er_recharge set operator_balance='" + strBalance_P + "' where id='" + intTopUpRecTransactionID + "'");
                                                        }
                                                        catch (Exception)
                                                        {

                                                        }
                                                        try
                                                        {
                                                            objODBC.executeNonQuery("UPDATE `er_easy_Pay_request` set `payid_responseid`='" + strPayID + "',`terminal_id`='" + strTerminalIDRes_F + "', `request_type`='" + strRequestType_F + "', `sign`='" + strSign + "', `srvid`='" + strSEVID + "',`mobile_number`='" + strMobile + "', `amount`='" + dblAmount.ToString() + "', `net_amount`=0, `payment_state`='" + strPayStatus + "', `status_code`='" + strStatusCode + "', `status_desc`='" + strDiscription_F + "', `txn_date`='" + strStatuTxnDate + "',`Response`='" + EasyPayResponse + "',`status`=0 where id='" + intEasypayID + "'");
                                                        }
                                                        catch (Exception)
                                                        {

                                                        }
                                                        //try
                                                        //{
                                                        //    SendSMPPSMS(intUserID, strUserName, strMobile, strExiomsOpName, intOperatorType, dblAmount, 3, intTopUpRecTransactionID.ToString());
                                                        //}
                                                        //catch (Exception)
                                                        //{

                                                        //}

                                                        return strMessage;

                                                    }
                                                    else if (strStatusCode == "100" && strPayStatus == "2") // Failed
                                                    {
                                                        try
                                                        {
                                                            objODBC.executeNonQuery("call `updateResponseV3`('" + strPayID + "','" + intTopUpRecTransactionID + "',2)");
                                                        }
                                                        catch (Exception)
                                                        {

                                                        }
                                                        strMessage = "FAILED," + "Top Up Transaction is FAILED ! Access denied ," + intTopUpRecTransactionID + ",NA";
                                                        string strRequestType_F = "NA";
                                                        string strTerminalIDRes_F = "NA";
                                                        string strDiscription_F = "NA";

                                                        try
                                                        {
                                                            strRequestType_F = xmlDoc.DocumentElement.ChildNodes[1].ChildNodes[0].Value.ToString();
                                                            strTerminalIDRes_F = xmlDoc.DocumentElement.ChildNodes[2].ChildNodes[0].Value.ToString();
                                                            strDiscription_F = paymentItem.Attributes["status-desc"].Value.ToString();
                                                        }
                                                        catch (Exception)
                                                        {

                                                        }
                                                        try
                                                        {
                                                            objODBC.executeNonQuery("UPDATE `er_easy_Pay_request` set `payid_responseid`='" + strPayID + "',`terminal_id`='" + strTerminalIDRes_F + "', `request_type`='" + strRequestType_F + "', `sign`='" + strSign + "', `srvid`='" + strSEVID + "',`mobile_number`='" + strMobile + "', `amount`='" + dblAmount.ToString() + "', `net_amount`=0,`payment_state`='" + strPayStatus + "', `status_code`='" + strStatusCode + "', `status_desc`='" + strDiscription_F + "', `txn_date`='" + strStatuTxnDate + "',`Response`='" + EasyPayResponse + "',`status`=2 where id='" + intEasypayID + "'");
                                                        }
                                                        catch (Exception)
                                                        {
                                                        }

                                                        try
                                                        {
                                                            SendSMPPSMS(intUserID, strUserName, strMobile, strExiomsOpName, intOperatorType, dblAmount, 2, intTopUpRecTransactionID.ToString(), strPhone);
                                                        }
                                                        catch (Exception)
                                                        {

                                                        }
                                                        return strMessage;
                                                    }
                                                    else
                                                    {
                                                        // objODBC.executeNonQuery("call `updateResponseV3`('" + strPayID + "','" + intTopUpRecTransactionID + "',2)");
                                                        objODBC.executeNonQuery("update er_recharge set ap_transid='" + strPayID + "',operator_transid='" + strPayID + "' where id='" + intTopUpRecTransactionID + "'");

                                                        strMessage = "PENDING," + "Your Transaction is in Process... Please check topup report status! ," + intTopUpRecTransactionID + "," + strPayID;



                                                        string strRequestType_F = "NA";
                                                        string strTerminalIDRes_F = "NA";
                                                        string strCurrencyRate_F = "NA";
                                                        string strBalance_F = "NA";
                                                        string strOverdraft_F = "NA";
                                                        string strWithheld_F = "NA";
                                                        string strPayable_F = "NA";
                                                        try
                                                        {
                                                            strRequestType_F = xmlDoc.DocumentElement.ChildNodes[1].ChildNodes[0].Value.ToString();
                                                            strTerminalIDRes_F = xmlDoc.DocumentElement.ChildNodes[2].ChildNodes[0].Value.ToString();
                                                            strCurrencyRate_F = xmlDoc.DocumentElement.ChildNodes[3].ChildNodes[0].Value.ToString();
                                                            strBalance_F = xmlDoc.DocumentElement.ChildNodes[4].ChildNodes[0].Value.ToString();
                                                            strOverdraft_F = xmlDoc.DocumentElement.ChildNodes[5].ChildNodes[0].Value.ToString();
                                                            strWithheld_F = xmlDoc.DocumentElement.ChildNodes[6].ChildNodes[0].Value.ToString();
                                                            strPayable_F = xmlDoc.DocumentElement.ChildNodes[7].ChildNodes[0].Value.ToString();
                                                        }
                                                        catch (Exception)
                                                        {

                                                        }
                                                        try
                                                        {
                                                            objODBC.executeNonQuery("UPDATE `er_easy_Pay_request` set `terminal_id`='" + strTerminalIDRes_F + "', `request_type`='" + strRequestType_F + "', `sign`='" + strSign + "', `srvid`='" + strSEVID + "',`mobile_number`='" + strMobile + "', `amount`='" + dblAmount.ToString() + "', `net_amount`=0, `payid_responseid`='" + strPayID + "', `payment_state`='" + strPayStatus + "', `status_code`='" + strStatusCode + "', `status_desc`='', `txn_date`='" + strStatuTxnDate + "',`currency_rate`='" + strCurrencyRate_F + "', `balance`='" + strBalance_F + "', `overdraft`='" + strOverdraft_F + "', `withheld`='" + strWithheld_F + "', `payable`='" + strPayable_F + "', `Response`='" + EasyPayResponse + "',`status`=0 where id='" + intEasypayID + "'");
                                                        }
                                                        catch (Exception)
                                                        {

                                                        }
                                                        try
                                                        {
                                                            SendSMPPSMS(intUserID, strUserName, strMobile, strExiomsOpName, intOperatorType, dblAmount, 2, intTopUpRecTransactionID.ToString(), strPhone);
                                                        }
                                                        catch (Exception)
                                                        {

                                                        }
                                                        return strMessage;
                                                    }

                                                }
                                                else if (strStatusCode == "202") //Failed
                                                {


                                                    // <?xml version="1.0" encoding="utf-8"?>
                                                    //< response >
                                                    //  < payment status - code = "202" status - desc = "Amount is too low" txn - date = "2019-01-21T10:35:15Z" />

                                                    //           < request - type > 1 </ request - type >

                                                    //           < terminal - id > 347922 </ terminal - id >
                                                    //         </ response >
                                                    try
                                                    {

                                                        objODBC.executeNonQuery("call `updateResponseV3`('NA','" + intTopUpRecTransactionID + "',2)");
                                                    }
                                                    catch (Exception)
                                                    {

                                                    }
                                                    strMessage = "FAILED," + "Top Up Transaction is FAILED. Amount is Low ! ," + intTopUpRecTransactionID + ",NA";


                                                    string strRequestType_F = "NA";
                                                    string strTerminalIDRes_F = "NA";
                                                    string strDiscription_F = "NA";

                                                    try
                                                    {
                                                        strRequestType_F = xmlDoc.DocumentElement.ChildNodes[1].ChildNodes[0].Value.ToString();
                                                        strTerminalIDRes_F = xmlDoc.DocumentElement.ChildNodes[2].ChildNodes[0].Value.ToString();
                                                        strDiscription_F = paymentItem.Attributes["status-desc"].Value.ToString();
                                                    }
                                                    catch (Exception)
                                                    {

                                                    }

                                                    try
                                                    {
                                                        objODBC.executeNonQuery("UPDATE `er_easy_Pay_request` set `terminal_id`='" + strTerminalIDRes_F + "', `request_type`='" + strRequestType_F + "', `sign`='" + strSign + "',`payid_responseid`='" + strPayID + "', `srvid`='" + strSEVID + "',`mobile_number`='" + strMobile + "', `amount`='" + dblAmount.ToString() + "', `net_amount`=0, `payment_state`='" + strPayStatus + "', `status_code`='" + strStatusCode + "', `status_desc`='" + strDiscription_F + "', `txn_date`='" + strStatuTxnDate + "',`Response`='" + EasyPayResponse + "',`status`=2 where id='" + intEasypayID + "'");
                                                    }
                                                    catch (Exception)
                                                    {

                                                    }
                                                    try
                                                    {
                                                        SendSMPPSMS(intUserID, strUserName, strMobile, strExiomsOpName, intOperatorType, dblAmount, 2, intTopUpRecTransactionID.ToString(), strPhone);
                                                    }
                                                    catch (Exception)
                                                    {

                                                    }

                                                    return strMessage;

                                                }
                                                else if (strStatusCode == "4") // Failed
                                                {
                                                    // objODBC.executeNonQuery(" insert into temp(`data`,`type`,`created_on`,`request`) values('5',1,'2019-01-29 19:58:58','5')");
                                                    // <? xml version = "1.0" encoding = "utf-8" ?>
                                                    //< response >

                                                    //  < payment status - code = "4" status - desc = "Access denied" txn - date = "2019-01-21T10:48:13Z" />


                                                    //           < request - type > 1 </ request - type >


                                                    //           < terminal - id > 347922 </ terminal - id >
                                                    //         </ response >


                                                    try
                                                    {
                                                        strPayID = paymentItem.Attributes["payid"].Value.ToString();
                                                    }
                                                    catch (Exception)
                                                    {

                                                    }
                                                    try
                                                    {
                                                        objODBC.executeNonQuery("call `updateResponseV3`('" + strPayID + "','" + intTopUpRecTransactionID + "',2)");
                                                    }
                                                    catch (Exception)
                                                    {

                                                    }
                                                    strMessage = "FAILED," + "Top Up Transaction is FAILED ! Access denied ," + intTopUpRecTransactionID + ",NA";


                                                    string strRequestType_F = "NA";
                                                    string strTerminalIDRes_F = "NA";
                                                    string strDiscription_F = "NA";

                                                    try
                                                    {
                                                        strRequestType_F = xmlDoc.DocumentElement.ChildNodes[1].ChildNodes[0].Value.ToString();
                                                        strTerminalIDRes_F = xmlDoc.DocumentElement.ChildNodes[2].ChildNodes[0].Value.ToString();
                                                        strDiscription_F = paymentItem.Attributes["status-desc"].Value.ToString();
                                                    }
                                                    catch (Exception)
                                                    {

                                                    }
                                                    try
                                                    {
                                                        objODBC.executeNonQuery("UPDATE `er_easy_Pay_request` set `terminal_id`='" + strTerminalIDRes_F + "', `request_type`='" + strRequestType_F + "', `sign`='" + strSign + "', `srvid`='" + strSEVID + "',`mobile_number`='" + strMobile + "', `amount`='" + dblAmount.ToString() + "', `net_amount`=0, `payid_responseid`='" + strPayID + "', `payment_state`='" + strPayStatus + "', `status_code`='" + strStatusCode + "', `status_desc`='" + strDiscription_F + "', `txn_date`='" + strStatuTxnDate + "',`Response`='" + EasyPayResponse + "',`status`=2 where id='" + intEasypayID + "'");
                                                    }
                                                    catch (Exception)
                                                    {

                                                    }
                                                    try
                                                    {
                                                        SendSMPPSMS(intUserID, strUserName, strMobile, strExiomsOpName, intOperatorType, dblAmount, 2, intTopUpRecTransactionID.ToString(), strPhone);
                                                    }
                                                    catch (Exception)
                                                    {

                                                    }
                                                    return strMessage;

                                                }
                                                else
                                                {
                                                    try
                                                    {
                                                        strPayID = paymentItem.Attributes["payid"].Value.ToString();
                                                    }
                                                    catch (Exception)
                                                    {

                                                    }

                                                    // objODBC.executeNonQuery("call `updateResponseV3`('" + strPayID + "','" + intTopUpRecTransactionID + "',2)");
                                                    strMessage = "FAILED," + "Top Up Transaction is FAILED ! Access denied ," + intTopUpRecTransactionID + ",NA";


                                                    string strRequestType_F = "NA";
                                                    string strTerminalIDRes_F = "NA";
                                                    string strDiscription_F = "NA";

                                                    try
                                                    {
                                                        strRequestType_F = xmlDoc.DocumentElement.ChildNodes[1].ChildNodes[0].Value.ToString();
                                                        strTerminalIDRes_F = xmlDoc.DocumentElement.ChildNodes[2].ChildNodes[0].Value.ToString();
                                                        strDiscription_F = paymentItem.Attributes["status-desc"].Value.ToString();
                                                    }
                                                    catch (Exception)
                                                    {

                                                    }
                                                    try
                                                    {
                                                        objODBC.executeNonQuery("UPDATE `er_easy_Pay_request` set `terminal_id`='" + strTerminalIDRes_F + "', `request_type`='" + strRequestType_F + "', `sign`='" + strSign + "', `srvid`='" + strSEVID + "',`mobile_number`='" + strMobile + "', `amount`='" + dblAmount.ToString() + "', `net_amount`=0, `payid_responseid`='" + strPayID + "', `payment_state`='" + strPayStatus + "', `status_code`='" + strStatusCode + "', `status_desc`='" + strDiscription_F + "', `txn_date`='" + strStatuTxnDate + "',`Response`='" + EasyPayResponse + "',`status`=2 where id='" + intEasypayID + "'");
                                                    }
                                                    catch (Exception)
                                                    {

                                                    }
                                                    try
                                                    {
                                                        SendSMPPSMS(intUserID, strUserName, strMobile, strExiomsOpName, intOperatorType, dblAmount, 2, intTopUpRecTransactionID.ToString(), strPhone);
                                                    }
                                                    catch (Exception)
                                                    {

                                                    }
                                                    return strMessage;
                                                }
                                            }
                                            catch (Exception ex)
                                            {
                                                //  objODBC.executeNonQuery("call `updateResponseV3`('','" + intTopUpRecTransactionID + "',2)");
                                                strMessage = "FAILED, Sorry Something Went Wrong Please try again..." + intTopUpRecTransactionID + ",NA,NA1";
                                                return strMessage;
                                            }
                                        }
                                        else
                                        {
                                            strMessage = "FAILED, Invalid Details" + intTopUpRecTransactionID + ",NA,NA2";
                                            return strMessage;
                                        }
                                    }
                                    else
                                    {
                                        strMessage = "FAILED, Parent wallet balance limit extended !" + ",NA,NA";
                                        return strMessage;
                                    }
                                }
                                else
                                {
                                    strMessage = "FAILED, Invalid Details" + intTopUpRecTransactionID + ",NA,NA3";
                                    return strMessage;
                                }
                            }
                            else
                            {
                                strMessage = "FAILED,You do not have permission to recharge this operator,NA,NA";
                            }
                        }
                        else
                        {
                            strMessage = "FAILED,You do not have permission to recharge this operator,NA,NA";
                        }
                    }
                    else
                    {
                        strMessage = "FAILED, Invalid Amount," + dblMinAmount + "," + dblMaximumAmount;
                        try
                        {
                            SendSMPPSMS(intUserID, strUserName, strMobile, strExiomsOpName, intOperatorType, dblAmount, 0, "", strPhone);
                        }
                        catch (Exception)
                        {

                        }
                        return strMessage;
                    }
                }
                else
                {
                    strMessage = "FAILED,Mobile Number is not Exist for This Operater !," + dblMinAmount + "," + dblMaximumAmount;

                    return strMessage;
                }
            }
            catch (Exception ex)
            {
                strMessage = "FAILED," + ex.ToString() + ",NA,NA";

                return strMessage;

            }
            finally
            {
                dt.Dispose();
            }
            return strMessage;
        }


        // Boloro Recharge 
        public string MakeMobileRechargeAPI5(int intUserID, string strUserName, int intOperatorType, int intOperatorID, string strMobile, double dblAmount, string strExiomsOpName, double dblMyComm, double dblResellerComm, string strMyAPIType, string strIPAddress, int intDesignationType, string strMacAddress, string strOSDetails, string strIMEINo, string strGcmID, string strAPPVersion, int intSourceData, int intCreatedByType, string strPhone)
        {

            string strMessage = "";
            DataTable dt = new DataTable();
            // string strRequestType = "5"; For balance

            try
            {
                if (strPhone.Length == 11)
                {
                    strPhone = strPhone.Substring(2, strPhone.Length - 2);
                }
                string[] strApiTypeData = getBoloroSEVID(intOperatorID, strMobile).Split(new char[] { ',' });
                string strSEVID = strApiTypeData[0].ToString();
                double dblMinAmount = double.Parse(strApiTypeData[1].ToString());
                double dblMaximumAmount = double.Parse(strApiTypeData[2].ToString());
                string strMatchMob = strApiTypeData[3].ToString();
                if (strMatchMob == "1")
                {
                    if ((strSEVID != "0") && (dblMinAmount <= dblAmount) && (dblMaximumAmount >= dblAmount))
                    {
                        strMobile = "0" + strMobile;
                        BoloroApi objBoloroApi = new BoloroApi();
                        string strTelco = string.Empty;
                        int intTopUpRecTransactionID = 0;
                        string strPRIVAPICOmment = "";
                        int intPrivAPIType = 0;
                        double dblCommissionAmt = dblAmount * dblMyComm / 100;
                        double dblDeductAmt = dblAmount - dblCommissionAmt;
                        double dblResellerDeductAmt = dblAmount * (1 - (dblResellerComm / 100));


                        Regex objMobilePatternMTN = new Regex(@"^(077|076)[\d]{7}$");
                        Regex objMobilePatternAWCC = new Regex(@"^(070|071)[\d]{7}$");
                        Regex objMobilePatternEtisalat = new Regex(@"^(78|73)[\d]{7}$");
                        Regex objMobilePatternSalaam = new Regex(@"^74[\d]{7}$");
                        Regex objMobilePatternRoshan = new Regex(@"^(79|72)[\d]{7}$");
                        int intRechargeStatus = 0;


                        if (objMobilePatternSalaam.IsMatch(strMobile))
                        {
                            strExiomsOpName = "Salaam";
                            intRechargeStatus = objODBC.executeScalar_int("SELECT `salaam_status` FROM `er_login` WHERE userid='" + intUserID + "'");

                        }
                        if (objMobilePatternEtisalat.IsMatch(strMobile))
                        {
                            strExiomsOpName = "Etisalat";
                            intRechargeStatus = objODBC.executeScalar_int("SELECT `etisalat_status` FROM `er_login` WHERE userid='" + intUserID + "'");

                        }
                        if (objMobilePatternRoshan.IsMatch(strMobile))
                        {
                            strExiomsOpName = "Roshan";
                            intRechargeStatus = objODBC.executeScalar_int("SELECT `roshan_status` FROM `er_login` WHERE userid='" + intUserID + "'");

                        }
                        if (objMobilePatternMTN.IsMatch(strMobile))
                        {
                            strExiomsOpName = "MTN";
                            intRechargeStatus = objODBC.executeScalar_int("SELECT `mtn_status` FROM `er_login` WHERE userid='" + intUserID + "'");

                        }
                        if (objMobilePatternAWCC.IsMatch(strMobile))
                        {
                            strExiomsOpName = "AWCC";
                            intRechargeStatus = objODBC.executeScalar_int("SELECT `awcc_status` FROM `er_login` WHERE userid='" + intUserID + "'");
                        }

                        int intTopupStatus = objODBC.executeScalar_int("SELECT `status` FROM `er_status_manager_operator` WHERE operator_id='" + intOperatorID + "'");
                        if (intTopupStatus == 1)
                        {
                            if (intRechargeStatus == 1)
                            {
                                int intParentID = objODBC.executeScalar_int("call `er_getParentID`('" + intUserID + "',@intParentID)");

                                dt = objODBC.getDataTable("call `ap_insert_mobile`('" + intUserID + "', '" + intOperatorType + "', '" + intOperatorID + "','" + strExiomsOpName + "','" + strMobile + "', '" + dblAmount + "','" + dblDeductAmt + "', '" + dblResellerDeductAmt + "', '" + dblCommissionAmt + "', '" + dblMyComm + "', '" + dblCommissionAmt + "', '" + strIPAddress + "','" + strOSDetails + "',  '" + intSourceData + "', '" + strGcmID + "','" + strIMEINo + "','" + strMyAPIType + "','" + intPrivAPIType + "','" + strPRIVAPICOmment + "','" + intParentID + "')");
                                if (dt.Rows.Count > 0)
                                {
                                    intTopUpRecTransactionID = Convert.ToInt32(dt.Rows[0][0].ToString());
                                    int intBoloroID = Convert.ToInt32(dt.Rows[0][1].ToString());
                                    int intWalletID = Convert.ToInt32(dt.Rows[0][2].ToString());
                                    if (intWalletID > 0)
                                    {
                                        if ((intTopUpRecTransactionID > 0) && (intBoloroID > 0))
                                        {


                                            // string strRequestType = "1"; For TopUP
                                            //  var EasyPayResponse = objEasypay.getBalance(strTerminalID, MD5Password, "5", strLoginID);

                                            try
                                            {
                                                // {"response_code":1,"response_desc":"Operation successful","data":{"transaction_no":"5e0af26a9868f","status_code":160,"status_desc":"In Process Initiating Topup request","msisdn":"93711707015","amount":50,"balance":"5000.0000","sequence_no":"5e0af26a87738"}}

                                                var BoloroResponse = objBoloroApi.BoloroTopup(intUserID, intTopUpRecTransactionID, intBoloroID, strMobile, dblAmount.ToString(), strSEVID);
                                                try
                                                {
                                                    objODBC.executeNonQuery("update er_recharge set request_mobile_no='" + strPhone + "' where id='" + intTopUpRecTransactionID + "'");
                                                }
                                                catch (Exception)
                                                {

                                                }


                                                if (BoloroResponse != "NA")
                                                {

                                                    try
                                                    {
                                                        string strRequest_Code = "";
                                                        string strResponse_desc = "";
                                                        string transaction_no = "";
                                                        string status_code = "";
                                                        string status_desc = "";
                                                        string msisdn = "";
                                                        string amount = "";
                                                        string balance = "";
                                                        string sequence_no = "";
                                                        try
                                                        {
                                                            dynamic data = JObject.Parse(BoloroResponse);
                                                            strRequest_Code = data.response_code;
                                                            strResponse_desc = data.response_desc;
                                                            // string strResponseData = data.data;
                                                            transaction_no = data["data"]["transaction_no"].ToString();
                                                            status_code = data["data"]["status_code"].ToString();
                                                            status_desc = data["data"]["status_desc"].Value;
                                                            amount = data["data"]["amount"].ToString();
                                                            balance = data["data"]["balance"].ToString();
                                                            sequence_no = data["data"]["sequence_no"].ToString();

                                                        }
                                                        catch (Exception)
                                                        {

                                                        }
                                                        if (strRequest_Code == "4" && strResponse_desc == "Access token denied")
                                                        {
                                                            BoloroResponse = objBoloroApi.BoloroTopup(intUserID, intTopUpRecTransactionID, intBoloroID, strMobile, dblAmount.ToString(), strSEVID);

                                                            if (BoloroResponse != "NA")
                                                            {

                                                                try
                                                                {

                                                                    try
                                                                    {
                                                                        dynamic data = JObject.Parse(BoloroResponse);
                                                                        strRequest_Code = data.response_code;
                                                                        strResponse_desc = data.response_desc;
                                                                        // string strResponseData = data.data;
                                                                        transaction_no = data["data"]["transaction_no"].ToString();
                                                                        status_code = data["data"]["status_code"].ToString();
                                                                        status_desc = data["data"]["status_desc"].Value;
                                                                        amount = data["data"]["amount"].ToString();
                                                                        balance = data["data"]["balance"].ToString();
                                                                        sequence_no = data["data"]["sequence_no"].ToString();

                                                                    }
                                                                    catch (Exception)
                                                                    {

                                                                    }
                                                                    if (strRequest_Code == "4" && strResponse_desc == "Access token denied")
                                                                    {
                                                                        // {"response_code":4,"response_desc":"Access token denied"}
                                                                        try
                                                                        {
                                                                            objODBC.executeNonQuery("call `updateResponseV3`('','" + intTopUpRecTransactionID + "',2)");
                                                                        }
                                                                        catch (Exception)
                                                                        {

                                                                        }
                                                                        strMessage = "FAILED," + "Top Up Transaction is FAILED ! Access denied ," + intTopUpRecTransactionID + ",NA";

                                                                        try
                                                                        {
                                                                            SendSMPPSMS(intUserID, strUserName, strMobile, strExiomsOpName, intOperatorType, dblAmount, 2, "", strPhone);
                                                                        }
                                                                        catch (Exception)
                                                                        {

                                                                        }
                                                                        return strMessage;
                                                                    }
                                                                    if (strRequest_Code == "2" && strResponse_desc == "Operation failed")
                                                                    {
                                                                        // {"response_code":2,"response_desc":"Operation failed","data":{"errors":["AWCC telco is currently disabled."],"sequence_no":"5e4d099ed6ca1"}}
                                                                        try
                                                                        {

                                                                            objODBC.executeNonQuery("call `updateResponseV3`('','" + intTopUpRecTransactionID + "',2)");
                                                                        }
                                                                        catch (Exception)
                                                                        {

                                                                        }
                                                                        strMessage = "FAILED," + "Top Up Transaction is FAILED ! telco is currently disabled ," + intTopUpRecTransactionID + ",NA";

                                                                        try
                                                                        {
                                                                            SendSMPPSMS(intUserID, strUserName, strMobile, strExiomsOpName, intOperatorType, dblAmount, 2, "", strPhone);
                                                                        }
                                                                        catch (Exception)
                                                                        {

                                                                        }
                                                                        return strMessage;
                                                                    }
                                                                    if (strRequest_Code == "5" && strResponse_desc == "Sequence No. denied")
                                                                    {
                                                                        // {"response_code":2,"response_desc":"Operation failed","data":{"errors":["AWCC telco is currently disabled."],"sequence_no":"5e4d099ed6ca1"}}
                                                                        try
                                                                        {
                                                                            objODBC.executeNonQuery("call `updateResponseV3`('','" + intTopUpRecTransactionID + "',2)");
                                                                        }
                                                                        catch (Exception)
                                                                        {

                                                                        }
                                                                        strMessage = "FAILED," + "Top Up Transaction is FAILED ! telco is currently disabled ," + intTopUpRecTransactionID + ",NA";

                                                                        try
                                                                        {
                                                                            SendSMPPSMS(intUserID, strUserName, strMobile, strExiomsOpName, intOperatorType, dblAmount, 2, "", strPhone);
                                                                        }
                                                                        catch (Exception)
                                                                        {

                                                                        }
                                                                        return strMessage;
                                                                    }
                                                                    else
                                                                    {

                                                                        objODBC.executeNonQuery("Update er_boloro_api set `transaction_no`='" + transaction_no + "',`topup_response_code`='" + strRequest_Code + "',`topup_response_desc`='" + strResponse_desc + "',`topup_status_code`='" + status_code + "',`topup_status_desc`='" + status_desc + "',`topup_msisdn`='" + msisdn + "',`topup_amount`='" + amount + "',`topup_operator_balance`='" + balance + "',`topup_sequence_no`='" + sequence_no + "',`status`=0 where id='" + intBoloroID + "'");

                                                                        objODBC.executeNonQuery("update er_recharge set ap_transid='" + intBoloroID + "',operator_transid='" + transaction_no + "' where id='" + intTopUpRecTransactionID + "'");

                                                                        strMessage = "PENDING," + "Your Transaction is in Process... Please check topup report status! ," + intTopUpRecTransactionID + "," + intBoloroID;

                                                                        return strMessage;
                                                                    }

                                                                }
                                                                catch (Exception)
                                                                {
                                                                    strMessage = "PENDING," + "Your Transaction is in Process... Please check topup report status! ," + intTopUpRecTransactionID + "," + intBoloroID;
                                                                }
                                                            }
                                                            else
                                                            {
                                                                try
                                                                {
                                                                    objODBC.executeNonQuery("call `updateResponseV3`('','" + intTopUpRecTransactionID + "',2)");
                                                                }
                                                                catch (Exception)
                                                                {

                                                                }
                                                                strMessage = "FAILED," + "Top Up Transaction is FAILED ! Access denied ," + intTopUpRecTransactionID + ",NA";
                                                                try
                                                                {
                                                                    SendSMPPSMS(intUserID, strUserName, strMobile, strExiomsOpName, intOperatorType, dblAmount, 2, "", strPhone);
                                                                }
                                                                catch (Exception)
                                                                {

                                                                }
                                                                return strMessage;
                                                            }
                                                        }
                                                        if (strRequest_Code == "2" && strResponse_desc == "Operation failed")
                                                        {
                                                            try
                                                            {
                                                                string strresponseBolo = "Response=@" + BoloroResponse.ToString();
                                                                objODBC.executeNonQuery("update er_recharge set comment='" + strresponseBolo + "' where id='" + intTopUpRecTransactionID + "'");
                                                            }
                                                            catch (Exception)
                                                            {

                                                            }

                                                            // {"response_code":2,"response_desc":"Operation failed","data":{"errors":["AWCC telco is currently disabled."],"sequence_no":"5e4d099ed6ca1"}}
                                                            try
                                                            {
                                                                objODBC.executeNonQuery("call `updateResponseV3`('','" + intTopUpRecTransactionID + "',2)");
                                                            }
                                                            catch (Exception)
                                                            {

                                                            }
                                                            strMessage = "FAILED," + "Top Up Transaction is FAILED ! telco is currently disabled ," + intTopUpRecTransactionID + ",NA";

                                                            try
                                                            {
                                                                SendSMPPSMS(intUserID, strUserName, strMobile, strExiomsOpName, intOperatorType, dblAmount, 2, "", strPhone);
                                                            }
                                                            catch (Exception)
                                                            {

                                                            }
                                                            return strMessage;
                                                        }
                                                        if (strRequest_Code == "5" && strResponse_desc == "Sequence No. denied")
                                                        {
                                                            BoloroResponse = objBoloroApi.BoloroTopup(intUserID, intTopUpRecTransactionID, intBoloroID, strMobile, dblAmount.ToString(), strSEVID);

                                                            if (BoloroResponse != "NA")
                                                            {

                                                                try
                                                                {

                                                                    try
                                                                    {
                                                                        dynamic data = JObject.Parse(BoloroResponse);
                                                                        strRequest_Code = data.response_code;
                                                                        strResponse_desc = data.response_desc;
                                                                        // string strResponseData = data.data;
                                                                        transaction_no = data["data"]["transaction_no"].ToString();
                                                                        status_code = data["data"]["status_code"].ToString();
                                                                        status_desc = data["data"]["status_desc"].Value;
                                                                        amount = data["data"]["amount"].ToString();
                                                                        balance = data["data"]["balance"].ToString();
                                                                        sequence_no = data["data"]["sequence_no"].ToString();

                                                                    }
                                                                    catch (Exception)
                                                                    {

                                                                    }
                                                                    if (strRequest_Code == "4" && strResponse_desc == "Access token denied")
                                                                    {
                                                                        // {"response_code":4,"response_desc":"Access token denied"}
                                                                        try
                                                                        {
                                                                            objODBC.executeNonQuery("call `updateResponseV3`('','" + intTopUpRecTransactionID + "',2)");
                                                                        }
                                                                        catch (Exception)
                                                                        {

                                                                        }
                                                                        strMessage = "FAILED," + "Top Up Transaction is FAILED ! Access denied ," + intTopUpRecTransactionID + ",NA";

                                                                        try
                                                                        {
                                                                            SendSMPPSMS(intUserID, strUserName, strMobile, strExiomsOpName, intOperatorType, dblAmount, 2, "", strPhone);
                                                                        }
                                                                        catch (Exception)
                                                                        {

                                                                        }
                                                                        return strMessage;
                                                                    }
                                                                    if (strRequest_Code == "2" && strResponse_desc == "Operation failed")
                                                                    {
                                                                        // {"response_code":2,"response_desc":"Operation failed","data":{"errors":["AWCC telco is currently disabled."],"sequence_no":"try5e4d099ed6ca1"}}
                                                                        try
                                                                        {
                                                                            objODBC.executeNonQuery("call `updateResponseV3`('','" + intTopUpRecTransactionID + "',2)");
                                                                        }
                                                                        catch (Exception)
                                                                        {

                                                                        }
                                                                        strMessage = "FAILED," + "Top Up Transaction is FAILED ! telco is currently disabled ," + intTopUpRecTransactionID + ",NA";

                                                                        try
                                                                        {
                                                                            SendSMPPSMS(intUserID, strUserName, strMobile, strExiomsOpName, intOperatorType, dblAmount, 2, "", strPhone);
                                                                        }
                                                                        catch (Exception)
                                                                        {

                                                                        }
                                                                        return strMessage;
                                                                    }
                                                                    if (strRequest_Code == "5" && strResponse_desc == "Sequence No. denied")
                                                                    {
                                                                        // {"response_code":2,"response_desc":"Operation failed","data":{"errors":["AWCC telco is currently disabled."],"sequence_no":"5e4d099ed6ca1"}}
                                                                        try
                                                                        {
                                                                            objODBC.executeNonQuery("call `updateResponseV3`('','" + intTopUpRecTransactionID + "',2)");
                                                                        }
                                                                        catch (Exception)
                                                                        {

                                                                        }
                                                                        strMessage = "FAILED," + "Top Up Transaction is FAILED ! telco is currently disabled ," + intTopUpRecTransactionID + ",NA";

                                                                        try
                                                                        {
                                                                            SendSMPPSMS(intUserID, strUserName, strMobile, strExiomsOpName, intOperatorType, dblAmount, 2, "", strPhone);
                                                                        }
                                                                        catch (Exception)
                                                                        {

                                                                        }
                                                                        return strMessage;
                                                                    }
                                                                    else
                                                                    {

                                                                        objODBC.executeNonQuery("Update er_boloro_api set `transaction_no`='" + transaction_no + "',`topup_response_code`='" + strRequest_Code + "',`topup_response_desc`='" + strResponse_desc + "',`topup_status_code`='" + status_code + "',`topup_status_desc`='" + status_desc + "',`topup_msisdn`='" + msisdn + "',`topup_amount`='" + amount + "',`topup_operator_balance`='" + balance + "',`topup_sequence_no`='" + sequence_no + "',`status`=0 where id='" + intBoloroID + "'");

                                                                        objODBC.executeNonQuery("update er_recharge set ap_transid='" + intBoloroID + "',operator_transid='" + transaction_no + "' where id='" + intTopUpRecTransactionID + "'");

                                                                        strMessage = "PENDING," + "Your Transaction is in Process... Please check topup report status! ," + intTopUpRecTransactionID + "," + intBoloroID;

                                                                        return strMessage;
                                                                    }

                                                                }
                                                                catch (Exception)
                                                                {
                                                                    strMessage = "PENDING," + "Your Transaction is in Process... Please check topup report status! ," + intTopUpRecTransactionID + "," + intBoloroID;
                                                                }
                                                            }
                                                            else
                                                            {
                                                                try
                                                                {
                                                                    objODBC.executeNonQuery("call `updateResponseV3`('','" + intTopUpRecTransactionID + "',2)");
                                                                }
                                                                catch (Exception)
                                                                {

                                                                }
                                                                strMessage = "FAILED," + "Top Up Transaction is FAILED ! Access denied ," + intTopUpRecTransactionID + ",NA";
                                                                try
                                                                {
                                                                    SendSMPPSMS(intUserID, strUserName, strMobile, strExiomsOpName, intOperatorType, dblAmount, 2, "", strPhone);
                                                                }
                                                                catch (Exception)
                                                                {

                                                                }
                                                                return strMessage;
                                                            }
                                                        }
                                                        else
                                                        {

                                                            objODBC.executeNonQuery("Update er_boloro_api set `transaction_no`='" + transaction_no + "',`topup_response_code`='" + strRequest_Code + "',`topup_response_desc`='" + strResponse_desc + "',`topup_status_code`='" + status_code + "',`topup_status_desc`='" + status_desc + "',`topup_msisdn`='" + msisdn + "',`topup_amount`='" + amount + "',`topup_operator_balance`='" + balance + "',`topup_sequence_no`='" + sequence_no + "',`status`=0 where id='" + intBoloroID + "'");

                                                            objODBC.executeNonQuery("update er_recharge set ap_transid='" + intBoloroID + "',operator_transid='" + transaction_no + "' where id='" + intTopUpRecTransactionID + "'");

                                                            strMessage = "PENDING," + "Your Transaction is in Process... Please check topup report status! ," + intTopUpRecTransactionID + "," + intBoloroID;

                                                            return strMessage;
                                                        }

                                                    }
                                                    catch (Exception)
                                                    {
                                                        strMessage = "PENDING," + "Your Transaction is in Process... Please check topup report status! ," + intTopUpRecTransactionID + "," + intBoloroID;
                                                    }
                                                }
                                                else
                                                {
                                                    try
                                                    {
                                                        objODBC.executeNonQuery("call `updateResponseV3`('','" + intTopUpRecTransactionID + "',2)");
                                                    }
                                                    catch (Exception)
                                                    {

                                                    }
                                                    strMessage = "FAILED," + "Top Up Transaction is FAILED ! Access denied ," + intTopUpRecTransactionID + ",NA";
                                                    try
                                                    {
                                                        SendSMPPSMS(intUserID, strUserName, strMobile, strExiomsOpName, intOperatorType, dblAmount, 2, "", strPhone);
                                                    }
                                                    catch (Exception)
                                                    {

                                                    }
                                                    return strMessage;
                                                }


                                            }
                                            catch (Exception ex)
                                            {
                                                //  objODBC.executeNonQuery("call `updateResponseV3`('','" + intTopUpRecTransactionID + "',2)");
                                                strMessage = "FAILED, Sorry Something Went Wrong Please try again..." + intTopUpRecTransactionID + ",NA,NA1";
                                                return strMessage;
                                            }
                                        }
                                        else
                                        {
                                            strMessage = "FAILED, Invalid Details" + intTopUpRecTransactionID + ",NA,NA2";
                                            return strMessage;
                                        }
                                    }
                                    else
                                    {
                                        strMessage = "FAILED, Parent wallet balance limit extended !" + ",NA,NA";
                                        return strMessage;
                                    }
                                }
                                else
                                {
                                    strMessage = "FAILED, Invalid Details" + intTopUpRecTransactionID + ",NA,NA3";
                                    return strMessage;
                                }
                            }
                            else
                            {
                                strMessage = "FAILED,You do not have permission to recharge this operator,NA,NA";
                            }
                        }
                        else
                        {
                            strMessage = "FAILED,You do not have permission to recharge this operator,NA,NA";
                        }
                    }
                    else
                    {
                        strMessage = "FAILED, Invalid Amount," + dblMinAmount + "," + dblMaximumAmount;
                        try
                        {
                            SendSMPPSMS(intUserID, strUserName, strMobile, strExiomsOpName, intOperatorType, dblAmount, 0, "", strPhone);
                        }
                        catch (Exception)
                        {

                        }
                        return strMessage;
                    }
                }
                else
                {
                    strMessage = "FAILED,Mobile Number is not Exist for This Operater !," + dblMinAmount + "," + dblMaximumAmount;

                    return strMessage;
                }
            }
            catch (Exception ex)
            {
                strMessage = "FAILED," + ex.ToString() + ",NA,NA";

                return strMessage;

            }
            finally
            {
                dt.Dispose();
            }
            return strMessage;
        }


        public class MTNSoapResponseParse
        {
            public string ersReference { get; set; }
            public int resultCode { get; set; }
            public string resultDescription { get; set; }


        }

        // MTN Recharge TopUp
        public string MakeMobileRechargeAPI6(int intUserID, string strUserName, int intOperatorType, int intOperatorID, string strMobile, double dblAmount, string strExiomsOpName, double dblMyComm, double dblResellerComm, string strMyAPIType, string strIPAddress, int intDesignationType, string strMacAddress, string strOSDetails, string strIMEINo, string strGcmID, string strAPPVersion, int intSourceData, int intCreatedByType, string strPhone)
        {
            string strMessage = "";
            string StrEndUserCoustomerMob = "";
            DataTable dt = new DataTable();
            // string strRequestType = "5"; For balance

            try
            {
                if (strPhone.Length == 11)
                {
                    strPhone = strPhone.Substring(2, strPhone.Length - 2);
                }
                Regex objMobilePatternMTN = new Regex(@"^(77|76)[\d]{7}$");

                if (objMobilePatternMTN.IsMatch(strMobile))
                {
                    int intTopupStatus = objODBC.executeScalar_int("SELECT `status` FROM `er_status_manager_operator` WHERE operator_id='" + intOperatorID + "'");
                    if (intTopupStatus == 1)
                    {
                        int intRechargeStatus = objODBC.executeScalar_int("SELECT `mtn_status` FROM `er_login` WHERE userid='" + intUserID + "'");
                        if (intRechargeStatus == 1)
                        {

                            if (dblAmount >= 10)
                            {
                                StrEndUserCoustomerMob = strMobile;

                                MTNApi objMTNAPI = new MTNApi();

                                int intTopUpRecTransactionID = 0;
                                string strPRIVAPICOmment = "";
                                int intPrivAPIType = 0;
                                double dblCommissionAmt = dblAmount * dblMyComm / 100;
                                double dblDeductAmt = dblAmount - dblCommissionAmt;
                                double dblResellerDeductAmt = dblAmount * (1 - (dblResellerComm / 100));

                                int intParentID = objODBC.executeScalar_int("call `er_getParentID`('" + intUserID + "',@intParentID)");

                                dt = objODBC.getDataTable("call `ap_insert_mobile`('" + intUserID + "', '" + intOperatorType + "', '" + intOperatorID + "','" + strExiomsOpName + "','" + strMobile + "', '" + dblAmount + "','" + dblDeductAmt + "', '" + dblResellerDeductAmt + "', '" + dblCommissionAmt + "', '" + dblMyComm + "', '" + dblCommissionAmt + "', '" + strIPAddress + "','" + strOSDetails + "',  '" + intSourceData + "', '" + strGcmID + "','" + strIMEINo + "','" + strMyAPIType + "','" + intPrivAPIType + "','" + strPRIVAPICOmment + "','" + intParentID + "')");
                                if (dt.Rows.Count > 0)
                                {
                                    intTopUpRecTransactionID = Convert.ToInt32(dt.Rows[0][0].ToString());
                                    int intMTNID = Convert.ToInt32(dt.Rows[0][1].ToString());
                                    int intWalletID = Convert.ToInt32(dt.Rows[0][2].ToString());
                                    if (intWalletID > 0)
                                    {
                                        if ((intTopUpRecTransactionID > 0) && (intMTNID > 0))
                                        {
                                            try
                                            {
                                                string MTNResponse = "NA";
                                                try
                                                {
                                                    MTNResponse = objMTNAPI.MTNAPITopup(intMTNID.ToString(), intTopUpRecTransactionID.ToString(), strMobile, dblAmount);

                                                    try
                                                    {
                                                        objODBC.executeNonQuery("update er_mtn_topup set response='" + MTNResponse + "' where id='" + intMTNID + "'");

                                                        objODBC.executeNonQuery("update er_recharge set comment='" + MTNResponse.ToString() + "',request_mobile_no='" + strPhone + "' where id='" + intTopUpRecTransactionID + "'");

                                                    }
                                                    catch (Exception)
                                                    {

                                                    }
                                                }
                                                catch (Exception)
                                                {

                                                }
                                                try
                                                {
                                                    XDocument xml = XDocument.Parse(MTNResponse);
                                                    var soapResponse = xml.Descendants().Where(x => x.Name.LocalName == "return").Select(x => new MTNSoapResponseParse()
                                                    {
                                                        ersReference = (string)x.Element(x.Name.Namespace + "ersReference"),
                                                        resultCode = (int)x.Element(x.Name.Namespace + "resultCode"),
                                                        resultDescription = (string)x.Element(x.Name.Namespace + "resultDescription")
                                                    }).FirstOrDefault();


                                                    if (soapResponse.resultCode == 0) // Success
                                                    {


                                                        //<soap:Envelope xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/"><soap:Body><ns2:requestTopupResponse xmlns:ns2="http://external.interfaces.ers.seamless.com/"><return><ersReference>2020040111361978801000085</ersReference><resultCode>0</resultCode><resultDescription>You topped up 10 AFA to 93764524139. Your balance is now 957 AFA. The transaction ID: 2020040111361978801000085</resultDescription><requestedTopupAmount><currency>AFA</currency><value>10</value></requestedTopupAmount><senderPrincipal><principalId><id>SML</id><type>RESELLERID</type></principalId><principalName>SML</principalName><accounts><account><accountSpecifier><accountId>SML</accountId><accountTypeId>RESELLER_AIRTIME</accountTypeId></accountSpecifier><balance><currency>AFA</currency><value>967.00000</value></balance><creditLimit><currency>AFA</currency><value>0.00000</value></creditLimit></account></accounts><status>Active</status><msisdn>93773333985</msisdn></senderPrincipal><topupAccountSpecifier><accountId>764524139</accountId><accountTypeId>SUBSCRIBER_AIRTIME</accountTypeId></topupAccountSpecifier><topupAmount><currency>AFA</currency><value>10.00</value></topupAmount><topupPrincipal><principalId><id>93764524139</id><type>SUBSCRIBERMSISDN</type></principalId><principalName></principalName><accounts><account><accountSpecifier><accountId>764524139</accountId><accountTypeId>SUBSCRIBER_AIRTIME</accountTypeId></accountSpecifier></account></accounts></topupPrincipal></return></ns2:requestTopupResponse></soap:Body></soap:Envelope>


                                                        string TXNID = soapResponse.ersReference; //TXNID = Transaction ID
                                                        string strBalance = "NA";
                                                        try
                                                        {
                                                            objODBC.executeNonQuery("call `updateResponseV3`('" + TXNID + "','" + intTopUpRecTransactionID + "',1)");
                                                        }
                                                        catch (Exception)
                                                        {

                                                        }
                                                        strMessage = "SUCCESS," + "Top Up Transaction is Successfull ! ," + intTopUpRecTransactionID + "," + TXNID;
                                                        try
                                                        {
                                                            try
                                                            {
                                                                // You topped up 10 AFA to 93764524139. Your balance is now 957 AFA. The transaction ID: 2020040111361978801000085
                                                                string[] strBalanceStructure = soapResponse.resultDescription.Split(new char[] { '.' });
                                                                string strOpMessage = strBalanceStructure[2];
                                                                var replacements = new[] { new { Find = "Your balance is now ", Replace = "0" }, new { Find = " AFA", Replace = "" }, };
                                                                var myLongString = strOpMessage;
                                                                foreach (var set in replacements)
                                                                {
                                                                    myLongString = myLongString.Replace(set.Find, set.Replace);
                                                                }
                                                                strBalance = myLongString;
                                                            }
                                                            catch (Exception)
                                                            {

                                                            }
                                                            try
                                                            {
                                                                objODBC.executeNonQuery("update er_recharge set operator_balance='" + strBalance + "' where id='" + intTopUpRecTransactionID + "'");

                                                            }
                                                            catch (Exception)
                                                            {

                                                            }
                                                        }
                                                        catch (Exception)
                                                        {

                                                        }


                                                        try
                                                        {
                                                            objODBC.executeNonQuery("update er_mtn_topup set mobile='" + strMobile + "',amount='" + dblAmount + "',api_type='" + strMyAPIType + "',ersReference='" + TXNID + "',resultCode='" + soapResponse.resultCode + "',resultDescription='" + soapResponse.resultDescription + "',value='" + dblAmount + "',bal_api='" + strBalance + "',status=1  where id='" + intMTNID + "'");

                                                        }
                                                        catch (Exception)
                                                        {

                                                        }
                                                        try
                                                        {
                                                            SendSMPPSMS(intUserID, strUserName, strMobile, strExiomsOpName, intOperatorType, dblAmount, 1, intTopUpRecTransactionID.ToString(), strPhone);


                                                            SMPP_SMS_Format objSMPP_SMS_Format = new SMPP_SMS_Format();
                                                            CommonFunction objCOMFUN = new CommonFunction();
                                                            string strGetSMS = objSMPP_SMS_Format.TopupSuccessEndUser(objODBC.executeScalar_str("SELECT full_name From er_login Where userid='" + intUserID + "'").ToString(), StrEndUserCoustomerMob, dblAmount, intTopUpRecTransactionID.ToString());
                                                            objCOMFUN.er_SMS_Alert(intUserID, 2, StrEndUserCoustomerMob, 4, strGetSMS, 9, "");
                                                        }
                                                        catch (Exception)
                                                        {

                                                        }

                                                        return strMessage;
                                                    }
                                                    else if (soapResponse.resultCode > 0) //Failed
                                                    {


                                                        // <soap:Envelope xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/"><soap:Body><ns2:requestTopupResponse xmlns:ns2="http://external.interfaces.ers.seamless.com/"><return><ersReference>2020040111320629301000083</ersReference><resultCode>11</resultCode><resultDescription>You provided an amount outside the margins of the contract. Please try again with a valid amount.</resultDescription><requestedTopupAmount><currency>AFA</currency><value>1</value></requestedTopupAmount><senderPrincipal><principalId><id>SML</id><type>RESELLERID</type></principalId><principalName>SML</principalName><accounts><account><accountSpecifier><accountId>SML</accountId><accountTypeId>RESELLER_AIRTIME</accountTypeId></accountSpecifier></account></accounts><status>Active</status><msisdn>93773333985</msisdn></senderPrincipal><topupAccountSpecifier><accountId>764524139</accountId><accountTypeId>SUBSCRIBER_AIRTIME</accountTypeId></topupAccountSpecifier><topupPrincipal><principalId><id>93764524139</id><type>SUBSCRIBERMSISDN</type></principalId><principalName></principalName><accounts><account><accountSpecifier><accountId>764524139</accountId><accountTypeId>SUBSCRIBER_AIRTIME</accountTypeId></accountSpecifier></account></accounts></topupPrincipal></return></ns2:requestTopupResponse></soap:Body></soap:Envelope>

                                                        try
                                                        {
                                                            string TXNID = soapResponse.ersReference;
                                                            try
                                                            {
                                                                objODBC.executeNonQuery("call `updateResponseV3`('" + TXNID + "','" + intTopUpRecTransactionID + "',2)");
                                                            }
                                                            catch (Exception)
                                                            {

                                                            }

                                                            strMessage = "FAILED," + "Top Up Transaction is FAILED !..," + intTopUpRecTransactionID + "," + TXNID + "";
                                                        }
                                                        catch (Exception)
                                                        {

                                                        }



                                                        try
                                                        {
                                                            objODBC.executeNonQuery("update er_mtn_topup set mobile='" + strMobile + "',amount='" + dblAmount + "',api_type=6,ersReference='" + soapResponse.ersReference + "',resultCode='" + soapResponse.resultCode + "',resultDescription='" + soapResponse.resultDescription + "',status=2 where id='" + intMTNID + "'");

                                                        }
                                                        catch (Exception)
                                                        {

                                                        }

                                                        try
                                                        {
                                                            SendSMPPSMS(intUserID, strUserName, strMobile, strExiomsOpName, intOperatorType, dblAmount, 2, intTopUpRecTransactionID.ToString(), strPhone);

                                                            //SMPP_SMS_Format objSMPP_SMS_Format = new SMPP_SMS_Format();
                                                            //CommonFunction objCOMFUN = new CommonFunction();
                                                            //string strGetSMS = objSMPP_SMS_Format.TopupFailedEndUser(objODBC.executeScalar_str("SELECT full_name From er_login Where userid='" + intUserID + "'").ToString(), StrEndUserCoustomerMob, dblAmount, intTopUpRecTransactionID.ToString());
                                                            //objCOMFUN.er_SMS_Alert(intUserID, 2, StrEndUserCoustomerMob, 4, strGetSMS, 9, "");
                                                        }
                                                        catch (Exception)
                                                        {

                                                        }
                                                        return strMessage;

                                                    }



                                                }
                                                catch (Exception)
                                                {

                                                }

                                                // var RoshanResponse = objODBC.executeScalar_str("SELECT data FROM temp WHERE id=1").ToString();


                                                return strMessage;
                                            }
                                            catch (Exception ex)
                                            {
                                                //    objODBC.executeNonQuery("call `updateResponseV3`('','" + intTopUpRecTransactionID + "',2)");
                                                strMessage = "PENDING,Something Went Wrong kindly contact to admin...," + intTopUpRecTransactionID + ",NA";
                                                return strMessage;

                                            }
                                        }
                                        else
                                        {
                                            strMessage = "FAILED, Invalid Details" + intTopUpRecTransactionID + ",NA,NA";
                                            return strMessage;
                                        }
                                    }
                                    else
                                    {
                                        strMessage = "FAILED, Parent wallet balance limit extended !" + ",NA,NA";
                                        return strMessage;
                                    }
                                }
                                else
                                {
                                    strMessage = "FAILED, Invalid Details" + intTopUpRecTransactionID + ",NA,NA";
                                    return strMessage;
                                }

                            }
                            else
                            {
                                strMessage = "FAILED,Amount is too Low !,NA,NA";
                                try
                                {
                                    SendSMPPSMS(intUserID, strUserName, strMobile, strExiomsOpName, intOperatorType, dblAmount, 0, "", strPhone);
                                }
                                catch (Exception)
                                {

                                }
                                return strMessage;
                            }
                        }
                        else
                        {
                            strMessage = "FAILED,You do not have permission to recharge this operator,NA,NA";
                        }
                    }
                    else
                    {
                        strMessage = "FAILED,You do not have permission to recharge this operator,NA,NA";
                    }
                }
                else
                {
                    strMessage = "FAILED,Mobile Number is not Exist for This Operater !,NA,NA";

                    return strMessage;
                }
            }
            catch (Exception)
            {
                strMessage = "FAILED,Exception,NA,NA";
                return strMessage;
            }
            finally
            {
                dt.Dispose();
            }
            return strMessage;
        }


        // Easy pay Min and Max Limit Function
        public string getEasyPaySEVID(int intOperaterID, string strMobile)
        {
            string strReturn = "";
            try
            {
                switch (intOperaterID)
                {
                    case 1: // Salam
                        {
                            Regex objMobilePattern = new Regex(@"^74[\d]{7}$");
                            if (objMobilePattern.IsMatch(strMobile))
                            {
                                strReturn = "74,10.00,10000.00,1";
                                break;
                            }
                            else
                            {
                                strReturn = "0,1000000000.00,1000000000.00,0";
                                break;
                            }
                        }

                    case 2: // Etisalat
                        {
                            Regex objMobilePattern = new Regex(@"^(78|73)[\d]{7}$");
                            if (objMobilePattern.IsMatch(strMobile))
                            {
                                strReturn = "78,1.00,10000.00,1";
                                break;
                            }
                            else
                            {
                                strReturn = "0,1000000000.00,1000000000.00,0";
                                break;
                            }

                        }
                    case 3: // Roshan
                        {

                            Regex objMobilePattern = new Regex(@"^(79|72)[\d]{7}$");
                            if (objMobilePattern.IsMatch(strMobile))
                            {
                                strReturn = "79,25.00,5000.00,1";
                                break;
                            }
                            else
                            {
                                strReturn = "0,1000000000.00,1000000000.00,0";
                                break;
                            }

                        }
                    case 4: // MTN
                        {

                            Regex objMobilePattern = new Regex(@"^(77|76)[\d]{7}$");
                            if (objMobilePattern.IsMatch(strMobile))
                            {
                                strReturn = "77,20.00,10000.00,1";
                                break;
                            }
                            else
                            {
                                strReturn = "0,1000000000.00,1000000000.00,0";
                                break;
                            }

                        }
                    case 5: //AWCC
                        {

                            Regex objMobilePattern = new Regex(@"^(70|71)[\d]{7}$");
                            if (objMobilePattern.IsMatch(strMobile))
                            {
                                strReturn = "70,25.00,10000.00,1";
                                break;
                            }
                            else
                            {

                                strReturn = "0,1000000000.00,1000000000.00,0";
                                break;
                            }

                        }
                    default:
                        {
                            strReturn = "0,1000000000.00,1000000000.00,0";
                            break;
                        }
                }
            }
            catch (Exception)
            {

            }
            return strReturn;
        }



        // Boloro Min and Max Limit Function
        public string getBoloroSEVID(int intOperaterID, string strMobile)
        {
            string strReturn = "";
            try
            {
                switch (intOperaterID)
                {
                    case 1: // Salam
                        {
                            Regex objMobilePattern = new Regex(@"^74[\d]{7}$");
                            if (objMobilePattern.IsMatch(strMobile))
                            {
                                strReturn = "74,10.00,5000.00,1";
                                break;
                            }
                            else
                            {
                                strReturn = "0,1000000000.00,1000000000.00,0";
                                break;
                            }
                        }

                    case 2: // Etisalat
                        {
                            Regex objMobilePattern = new Regex(@"^(78|73)[\d]{7}$");
                            if (objMobilePattern.IsMatch(strMobile))
                            {
                                strReturn = "78,5.00,5001.00,1";
                                break;
                            }
                            else
                            {
                                strReturn = "0,1000000000.00,1000000000.00,0";
                                break;
                            }

                        }
                    case 3: // Roshan
                        {

                            Regex objMobilePattern = new Regex(@"^(79|72)[\d]{7}$");
                            if (objMobilePattern.IsMatch(strMobile))
                            {
                                strReturn = "79,25.00,5001.00,1";
                                break;
                            }
                            else
                            {
                                strReturn = "0,1000000000.00,1000000000.00,0";
                                break;
                            }

                        }
                    case 4: // MTN
                        {

                            Regex objMobilePattern = new Regex(@"^(77|76)[\d]{7}$");
                            if (objMobilePattern.IsMatch(strMobile))
                            {
                                strReturn = "77,12.00,20000.00,1";
                                break;
                            }
                            else
                            {
                                strReturn = "0,1000000000.00,1000000000.00,0";
                                break;
                            }

                        }
                    case 5: //AWCC
                        {

                            Regex objMobilePattern = new Regex(@"^(70|71)[\d]{7}$");
                            if (objMobilePattern.IsMatch(strMobile))
                            {
                                strReturn = "70,25.00,5001.00,1";
                                break;
                            }
                            else
                            {

                                strReturn = "0,1000000000.00,1000000000.00,0";
                                break;
                            }

                        }
                    default:
                        {
                            strReturn = "0,1000000000.00,1000000000.00,0";
                            break;
                        }
                }
            }
            catch (Exception)
            {

            }
            return strReturn;
        }


        public string CheckRechargeRequest(int intUserID, string strUserName, int intOperatorType, int intOperatorID, string strMobile, double dblAmount, double dblMyComm, double dblResellerComm, int strMyAPIType)
        {
            DataTable dt = new DataTable();
            string strMessage = "";
            if (strMyAPIType == 3 || strMyAPIType == 4 || strMyAPIType == 5)
            {
                strMobile = "0" + strMobile;
            }

            try
            {
                int intChkUserStatus = objODBC.executeScalar_int("Select Count(1) From er_login where userid='" + intUserID + "' and user_status=1 and Active=1");
                if (intChkUserStatus == 1)
                {
                    string strDate2 = objDateFor.getCurDateTimeString();
                    int intchkDuplicateRecgarge = objODBC.executeScalar_int("Select Count(1) From er_recharge where mobile_number='" + strMobile + "' and amount='" + dblAmount + "' and status<3 And TIMESTAMPDIFF(MINUTE,created_on,'" + strDate2 + "')<=5");

                    if (intchkDuplicateRecgarge == 0)
                    {
                        double dblWallet = objODBC.executeScalar_dbl("SELECT (ex_wallet-min_wallet) as walletbalance From er_wallet Where userid=" + intUserID);

                        double dblDeductAmt, dblResellerDeductAmt;

                        double dblCommissionAmt;

                        dblCommissionAmt = (dblAmount * dblMyComm) / 100;

                        dblDeductAmt = dblAmount - dblCommissionAmt;

                        dblResellerDeductAmt = dblAmount - (dblAmount * dblResellerComm / 100);

                        if (dblWallet >= dblDeductAmt)
                        {
                            double dblSysWallet = 0;

                            dblSysWallet = objODBC.executeScalar_dbl("SELECT amt From er_sys_wallet Where id=" + intOperatorID);


                            if (dblSysWallet >= dblResellerDeductAmt)
                            {

                                dt = objODBC.getDataTable("SELECT salaam_status,etisalat_status,roshan_status,mtn_status,awcc_status from er_login where userid='" + intUserID + "'");
                                if (dt.Rows.Count > 0)
                                {
                                    int intSalaam = int.Parse(dt.Rows[0][0].ToString());
                                    int intEtisalat = int.Parse(dt.Rows[0][1].ToString());
                                    int intRoshan = int.Parse(dt.Rows[0][2].ToString());
                                    int intMTN = int.Parse(dt.Rows[0][3].ToString());
                                    int intAwcc = int.Parse(dt.Rows[0][4].ToString());
                                    if (intOperatorID == 1) // Salaam
                                    {
                                        if (intSalaam == 1)
                                        {
                                            strMessage = "1,Your request in process,Pending,NA,NA";
                                        }
                                        else
                                        {
                                            strMessage = "2,You dont have permission for recharge this operator. Please contact your Customer Care !,Failed,NA,NA";
                                            return strMessage;
                                        }
                                    }
                                    else if (intOperatorID == 2) // Etisalat
                                    {
                                        if (intEtisalat == 1)
                                        {
                                            strMessage = "1,Your request in process,Pending,NA,NA";
                                        }
                                        else
                                        {
                                            strMessage = "2,You dont have permission for recharge this operator. Please contact your Customer Care !,Failed,NA,NA";
                                            return strMessage;
                                        }
                                    }
                                    else if (intOperatorID == 3) // Roshan
                                    {
                                        if (intRoshan == 1)
                                        {
                                            strMessage = "1,Your request in process,Pending,NA,NA";
                                        }
                                        else
                                        {
                                            strMessage = "2,You dont have permission for recharge this operator. Please contact your Customer Care !,Failed,NA,NA";
                                            return strMessage;
                                        }
                                    }
                                    else if (intOperatorID == 4) // MTN
                                    {
                                        if (intMTN == 1)
                                        {
                                            strMessage = "1,Your request in process,Pending,NA,NA";
                                        }
                                        else
                                        {
                                            strMessage = "2,You dont have permission for recharge this operator. Please contact your Customer Care !,Failed,NA,NA";
                                            return strMessage;
                                        }
                                    }
                                    else if (intOperatorID == 5) // AWCC
                                    {
                                        if (intAwcc == 1)
                                        {
                                            strMessage = "1,Your request in process,Pending,NA,NA";
                                        }
                                        else
                                        {
                                            strMessage = "2,You dont have permission for recharge this operator. Please contact your Customer Care !,Failed,NA,NA";
                                            return strMessage;
                                        }
                                    }
                                    else
                                    {

                                    }

                                }
                                else
                                {
                                    strMessage = "2,You dont have permission for recharge this operator. Please contact your Customer Care !,Failed,NA,NA";
                                    return strMessage;
                                }

                            }
                            else
                            {
                                strMessage = "2,You cannot do this recharge. Please contact your Customer Care !,Failed,NA,NA";
                                return strMessage;
                            }
                        }
                        else
                        {
                            strMessage = "2, You do not have sufficient balance ,Failed,NA,NA";
                            try
                            {
                                SendSMPPSMS(intUserID, strUserName, strMobile, "", intOperatorType, dblAmount, 4, "", "0");
                            }
                            catch (Exception)
                            {

                            }
                            return strMessage;
                        }
                    }
                    else
                    {
                        strMessage = "2,You can not recharge on same mobile with same amount more than once within 5 minutes,Failed,NA,NA";

                        return strMessage;
                    }

                }
                else
                {
                    strMessage = "2,You can not recharge user is In Active,Failed,NA,NA";

                    return strMessage;
                }
            }
            catch (Exception ex)
            {
                strMessage = ex.Message.ToString();

                return "0,Something Went Wrong,Failed,NA,NA";
            }
            return strMessage;
        }






        // make GroupTopup



        // ActivationContext Type=62
        // int intUserID=0, int intOperatorID=1, string strMobile=2, double dblAmount=3, string strIPAddress=4, int intDesignationType=5, string strMacAddress=6, string strOSDetails=7, string strIMEINo=8, string strGcmID=9, string strAPPVersion=10, int intSource=11, int intCreatedByType=12
        public ArrayList MakeMobileRechargeforGroupTopup(int intUserID, int intOperatorID, string strMobile, double dblAmount, int intGroupTopUp_ID)
        {
            int intActivityType = 62;
            //  string strUserName; string strData; int intEUserType; int intType, intSourceData;
            string strMessage = "Group Top Up", strExiomsOpName = "";

            ArrayList arrUserParams = new ArrayList();

            try
            {

                string strIPAddress = "NA";
                int intDesignationType = 5;
                string strMacAddress = "NA";
                string strOSDetails = "NA";
                string strIMEINo = "NA";
                string strGcmID = "NA";
                string strAPPVersion = "NA";
                int intSourceData = 1;
                int intCreatedByType = 1;
                try
                {
                    try
                    {
                        objCreateFile.CreateTextFile(strIPAddress, strMacAddress, strOSDetails, strIMEINo, strGcmID, strAPPVersion, intSourceData.ToString(), intCreatedByType, intActivityType, " Mobile Recharge Group Topup", "NA");
                    }
                    catch (Exception)
                    {

                    }

                    if (dblAmount > 0)
                    {
                        string strCommission = objODBC.executeScalar_str("call getCommissionForRecharge(" + intUserID + "," + intOperatorID + ")");

                        string[] strCommissionStructure = strCommission.Split(new char[] { ',' });

                        double dblMyComm = double.Parse(strCommissionStructure[0]);
                        dblMyComm = 100 * dblMyComm / (100 + dblMyComm);
                        string strMyAPIType = strCommissionStructure[1];
                        // ‘ 1-Salaam;2-Etisalat;3-Roshan;4-Easy Pay
                        double dblResellerComm = double.Parse(strCommissionStructure[2]);
                        int intOperatorType = 1;

                        // int intAPIType = ReturnAPITypeForOperator(intOperatorID)
                        //   string strExiomsOpName = objNewRecharge.GetExiomsOperatorName(intOperatorID);

                        string strCheckResponse = CheckRechargeRequest(intUserID, objODBC.executeScalar_str("select username FROM `er_login` where userid='" + intUserID + "'"), intOperatorType, intOperatorID, strMobile, dblAmount, dblMyComm, dblResellerComm, int.Parse(strMyAPIType));

                        string[] strResponseStructure = strCheckResponse.Split(new char[] { ',' });
                        // strMessage = "2,OOPs! You do not have sufficient balance.,Failed,NA,NA";

                        string strStatusCheckResponse = strResponseStructure[0];
                        string strResponseMessage = strResponseStructure[1];
                        string strResposneStatusMessage = strResponseStructure[2];
                        string strResposneStatus3 = strResponseStructure[3];
                        string strResposneStatus4 = strResponseStructure[4];
                        if (strStatusCheckResponse == "1")
                        {
                            if (strMyAPIType == "1")
                            {
                                try
                                {

                                    strExiomsOpName = "Salaam";
                                    try
                                    {
                                        objODBCLOG.executeNonQuery("call activity_logs('" + intUserID + "','" + intDesignationType + "','" + intCreatedByType + "','" + strIPAddress + "','" + strMacAddress + "','" + strOSDetails + "','" + strIMEINo + "','" + strGcmID + "','" + strAPPVersion + "','" + intSourceData + "','" + strMessage + "','" + intActivityType + "','','')");
                                    }
                                    catch (Exception)
                                    {

                                    }
                                    strMessage = MakeMobileRechargeAPI1(intUserID, objODBC.executeScalar_str("select username FROM `er_login` where userid='" + intUserID + "'"), intOperatorType, intOperatorID, strMobile, dblAmount, strExiomsOpName, dblMyComm, dblResellerComm, strMyAPIType, strIPAddress, intDesignationType, strMacAddress, strOSDetails, strIMEINo, strGcmID, strAPPVersion, intSourceData, intCreatedByType, "0");

                                    string[] strResponseMessageSalaam = strMessage.Split(new char[] { ',' });

                                    string strStatusRes = strResponseMessageSalaam[0];
                                    objODBC.executeNonQuery("update er_recharge set group_topup_id='" + intGroupTopUp_ID + "' where id ='" + strResponseMessageSalaam[2] + "'");
                                    if (strStatusRes == "SUCCESS")
                                    {
                                        string strStatusMessage = strResponseMessageSalaam[1];
                                        string strStatusTopUpTransactionID = strResponseMessageSalaam[2];
                                        string strStatusOperaterTransID = strResponseMessageSalaam[3];
                                        arrUserParams.Add("TRUE");
                                        arrUserParams.Add(strStatusMessage);
                                        arrUserParams.Add(strStatusTopUpTransactionID);
                                        arrUserParams.Add(strStatusOperaterTransID);
                                        //    objODBC.executeNonQuery("INSERT INTO `temp`( `data`, `type`, `request`) VALUES('4','3','" + strStatusMessage + "')");
                                        return arrUserParams;
                                    }
                                    else if (strStatusRes == "FAILED")
                                    {
                                        string strStatusMessage = strResponseMessageSalaam[1];
                                        string strStatusTopUpTransactionID = strResponseMessageSalaam[2];
                                        string strStatusOperaterTransID = strResponseMessageSalaam[3];
                                        arrUserParams.Add("FALSE");
                                        arrUserParams.Add(strStatusMessage);
                                        arrUserParams.Add(strStatusTopUpTransactionID);
                                        arrUserParams.Add(strStatusOperaterTransID);
                                        //    objODBC.executeNonQuery("INSERT INTO `temp`( `data`, `type`, `request`) VALUES('4','3','" + strStatusMessage + "')");
                                        return arrUserParams;
                                    }
                                    else
                                    {
                                        // objODBC.executeNonQuery("INSERT INTO `temp`( `data`, `type`, `request`) VALUES('4','3','Invalid Details')");
                                        arrUserParams.Add("FALSE");
                                        arrUserParams.Add("Invalid Details");
                                        arrUserParams.Add("NA");
                                        arrUserParams.Add("NA");
                                        return arrUserParams;
                                    }

                                }
                                catch (Exception)
                                {

                                }
                                return arrUserParams;
                            }
                            else if (strMyAPIType == "2")
                            {
                                try
                                {
                                    //int intOperatorIDRechargeAPI = objNewRecharge.GetOperatorIDForRechargeAPI(intOperatorID);

                                    strExiomsOpName = "Etisalat";
                                    try
                                    {
                                        objODBCLOG.executeNonQuery("call activity_logs('" + intUserID + "','" + intDesignationType + "','" + intCreatedByType + "','" + strIPAddress + "','" + strMacAddress + "','" + strOSDetails + "','" + strIMEINo + "','" + strGcmID + "','" + strAPPVersion + "','" + intSourceData + "','" + strMessage + "','" + intActivityType + "','','')");
                                    }
                                    catch (Exception)
                                    {

                                    }
                                    strMessage = MakeMobileRechargeAPI2(intUserID, objODBC.executeScalar_str("select username FROM `er_login` where userid='" + intUserID + "'"), intOperatorType, intOperatorID, strMobile, dblAmount, strExiomsOpName, dblMyComm, dblResellerComm, strMyAPIType, strIPAddress, intDesignationType, strMacAddress, strOSDetails, strIMEINo, strGcmID, strAPPVersion, intSourceData, intCreatedByType, "0");

                                    string[] strResponseMessagEtisalat = strMessage.Split(new char[] { ',' });

                                    string strStatusRes = strResponseMessagEtisalat[0];
                                    if (strStatusRes == "SUCCESS")
                                    {
                                        string strStatusMessage = strResponseMessagEtisalat[1];
                                        string strStatusTopUpTransactionID = strResponseMessagEtisalat[2];
                                        string strStatusOperaterTransID = strResponseMessagEtisalat[3];
                                        arrUserParams.Add("TRUE");
                                        arrUserParams.Add(strStatusMessage);
                                        arrUserParams.Add(strStatusTopUpTransactionID);
                                        arrUserParams.Add(strStatusOperaterTransID);
                                        //    objODBC.executeNonQuery("INSERT INTO `temp`( `data`, `type`, `request`) VALUES('4','3','" + strStatusMessage + "')");
                                        return arrUserParams;
                                    }
                                    else if (strStatusRes == "FAILED")
                                    {
                                        string strStatusMessage = strResponseMessagEtisalat[1];
                                        string strStatusTopUpTransactionID = strResponseMessagEtisalat[2];
                                        string strStatusOperaterTransID = strResponseMessagEtisalat[3];
                                        arrUserParams.Add("FALSE");
                                        arrUserParams.Add(strStatusMessage);
                                        arrUserParams.Add(strStatusTopUpTransactionID);
                                        arrUserParams.Add(strStatusOperaterTransID);
                                        //    objODBC.executeNonQuery("INSERT INTO `temp`( `data`, `type`, `request`) VALUES('4','3','" + strStatusMessage + "')");
                                        return arrUserParams;
                                    }
                                    else if (strStatusRes == "PENDING")
                                    {
                                        string strStatusMessage = strResponseMessagEtisalat[1];
                                        string strStatusTopUpTransactionID = strResponseMessagEtisalat[2];
                                        string strStatusOperaterTransID = strResponseMessagEtisalat[3];
                                        arrUserParams.Add("FALSE");
                                        arrUserParams.Add(strStatusRes);
                                        arrUserParams.Add(strStatusTopUpTransactionID);
                                        arrUserParams.Add(strStatusOperaterTransID);
                                        //    objODBC.executeNonQuery("INSERT INTO `temp`( `data`, `type`, `request`) VALUES('4','3','" + strStatusMessage + "')");
                                        return arrUserParams;
                                    }
                                    else
                                    {
                                        // objODBC.executeNonQuery("INSERT INTO `temp`( `data`, `type`, `request`) VALUES('4','3','Invalid Details')");
                                        arrUserParams.Add("FALSE");
                                        arrUserParams.Add("Invalid Details");
                                        arrUserParams.Add("NA");
                                        arrUserParams.Add("NA");
                                        return arrUserParams;
                                    }

                                }
                                catch (Exception)
                                {

                                }
                                return arrUserParams;
                            }
                            else if (strMyAPIType == "3")
                            {
                                try
                                {
                                    //int intOperatorIDRechargeAPI = objNewRecharge.GetOperatorIDForRechargeAPI(intOperatorID);

                                    strExiomsOpName = "Roshan";
                                    try
                                    {
                                        objODBCLOG.executeNonQuery("call activity_logs('" + intUserID + "','" + intDesignationType + "','" + intCreatedByType + "','" + strIPAddress + "','" + strMacAddress + "','" + strOSDetails + "','" + strIMEINo + "','" + strGcmID + "','" + strAPPVersion + "','" + intSourceData + "','" + strMessage + "','" + intActivityType + "','','')");
                                    }
                                    catch (Exception)
                                    {

                                    }
                                    strMessage = MakeMobileRechargeAPI3(intUserID, objODBC.executeScalar_str("select username FROM `er_login` where userid='" + intUserID + "'"), intOperatorType, intOperatorID, strMobile, dblAmount, strExiomsOpName, dblMyComm, dblResellerComm, strMyAPIType, strIPAddress, intDesignationType, strMacAddress, strOSDetails, strIMEINo, strGcmID, strAPPVersion, intSourceData, intCreatedByType, "0");

                                    string[] strResponseMessageRoshan = strMessage.Split(new char[] { ',' });

                                    string strStatusRes = strResponseMessageRoshan[0];
                                    objODBC.executeNonQuery("update er_recharge set group_topup_id='" + intGroupTopUp_ID + "' where id ='" + strResponseMessageRoshan[2] + "'");
                                    if (strStatusRes == "SUCCESS")
                                    {
                                        string strStatusMessage = strResponseMessageRoshan[1];
                                        string strStatusTopUpTransactionID = strResponseMessageRoshan[2];
                                        string strStatusOperaterTransID = strResponseMessageRoshan[3];
                                        arrUserParams.Add("TRUE");
                                        arrUserParams.Add(strStatusMessage);
                                        arrUserParams.Add(strStatusTopUpTransactionID);
                                        arrUserParams.Add(strStatusOperaterTransID);
                                        //    objODBC.executeNonQuery("INSERT INTO `temp`( `data`, `type`, `request`) VALUES('4','3','" + strStatusMessage + "')");
                                        return arrUserParams;
                                    }
                                    else if (strStatusRes == "FAILED")
                                    {
                                        string strStatusMessage = strResponseMessageRoshan[1];
                                        string strStatusTopUpTransactionID = strResponseMessageRoshan[2];
                                        string strStatusOperaterTransID = strResponseMessageRoshan[3];
                                        arrUserParams.Add("FALSE");
                                        arrUserParams.Add(strStatusMessage);
                                        arrUserParams.Add(strStatusTopUpTransactionID);
                                        arrUserParams.Add(strStatusOperaterTransID);
                                        //    objODBC.executeNonQuery("INSERT INTO `temp`( `data`, `type`, `request`) VALUES('4','3','" + strStatusMessage + "')");
                                        return arrUserParams;
                                    }
                                    else
                                    {
                                        // objODBC.executeNonQuery("INSERT INTO `temp`( `data`, `type`, `request`) VALUES('4','3','Invalid Details')");
                                        arrUserParams.Add("FALSE");
                                        arrUserParams.Add("Invalid Details");
                                        arrUserParams.Add("NA");
                                        arrUserParams.Add("NA");
                                        return arrUserParams;
                                    }

                                }
                                catch (Exception)
                                {

                                }
                                return arrUserParams;
                            }
                            else if (strMyAPIType == "4")
                            {
                                //int intOperatorIDRechargeAPI = objNewRecharge.GetOperatorIDForRechargeAPI(intOperatorID);

                                strExiomsOpName = "Easy Pay";
                                try
                                {
                                    objODBCLOG.executeNonQuery("call activity_logs('" + intUserID + "','" + intDesignationType + "','" + intCreatedByType + "','" + strIPAddress + "','" + strMacAddress + "','" + strOSDetails + "','" + strIMEINo + "','" + strGcmID + "','" + strAPPVersion + "','" + intSourceData + "','" + strMessage + "','" + intActivityType + "','','')");
                                }
                                catch (Exception)
                                {

                                }
                                strMessage = MakeMobileRechargeAPI4(intUserID, objODBC.executeScalar_str("select username FROM `er_login` where userid='" + intUserID + "'"), intOperatorType, intOperatorID, strMobile, dblAmount, strExiomsOpName, dblMyComm, dblResellerComm, strMyAPIType, strIPAddress, intDesignationType, strMacAddress, strOSDetails, strIMEINo, strGcmID, strAPPVersion, intSourceData, intCreatedByType, "0");

                                string[] strResponseMessageEasyPay = strMessage.Split(new char[] { ',' });

                                string strStatusRes = strResponseMessageEasyPay[0];
                                objODBC.executeNonQuery("update er_recharge set group_topup_id='" + intGroupTopUp_ID + "' where id ='" + strResponseMessageEasyPay[2] + "'");
                                if (strStatusRes == "SUCCESS")
                                {
                                    string strStatusMessage = strResponseMessageEasyPay[1];
                                    string strStatusTopUpTransactionID = strResponseMessageEasyPay[2];
                                    string strStatusOperaterTransID = strResponseMessageEasyPay[3];
                                    arrUserParams.Add("TRUE");
                                    arrUserParams.Add(strStatusMessage);
                                    arrUserParams.Add(strStatusTopUpTransactionID);
                                    arrUserParams.Add(strStatusOperaterTransID);
                                    return arrUserParams;

                                }
                                else if (strStatusRes == "FAILED")
                                {
                                    string strStatusMessage = strResponseMessageEasyPay[1];
                                    string strStatusTopUpTransactionID = strResponseMessageEasyPay[2];
                                    string strStatusOperaterTransID = strResponseMessageEasyPay[3];
                                    arrUserParams.Add("FALSE");
                                    arrUserParams.Add(strStatusMessage);
                                    arrUserParams.Add(strStatusTopUpTransactionID);
                                    arrUserParams.Add(strStatusOperaterTransID);
                                    return arrUserParams;
                                }
                                else if (strStatusRes == "PENDING")
                                {
                                    string strStatusMessage = strResponseMessageEasyPay[1];
                                    string strStatusTopUpTransactionID = strResponseMessageEasyPay[2];
                                    string strStatusOperaterTransID = strResponseMessageEasyPay[3];
                                    arrUserParams.Add("FALSE");
                                    arrUserParams.Add(strStatusRes);
                                    arrUserParams.Add(strStatusTopUpTransactionID);
                                    arrUserParams.Add(strStatusOperaterTransID);
                                    //    objODBC.executeNonQuery("INSERT INTO `temp`( `data`, `type`, `request`) VALUES('4','3','" + strStatusMessage + "')");
                                    return arrUserParams;
                                }
                                else
                                {
                                    // objODBC.executeNonQuery("INSERT INTO `temp`( `data`, `type`, `request`) VALUES('4','3','Invalid Details')");
                                    arrUserParams.Add("FALSE");
                                    arrUserParams.Add("Invalid Details");
                                    arrUserParams.Add("NA");
                                    arrUserParams.Add("NA");
                                    return arrUserParams;
                                }


                            }
                            else if (strMyAPIType == "5")
                            {
                                //int intOperatorIDRechargeAPI = objNewRecharge.GetOperatorIDForRechargeAPI(intOperatorID);
                                if (intOperatorID == 4)
                                {
                                    strExiomsOpName = "MTN";
                                }
                                else
                                {
                                    strExiomsOpName = "AWCC";
                                }
                                try
                                {
                                    objODBCLOG.executeNonQuery("call activity_logs('" + intUserID + "','" + intDesignationType + "','" + intCreatedByType + "','" + strIPAddress + "','" + strMacAddress + "','" + strOSDetails + "','" + strIMEINo + "','" + strGcmID + "','" + strAPPVersion + "','" + intSourceData + "','" + strMessage + "','" + intActivityType + "','','')");
                                }
                                catch (Exception)
                                {

                                }
                                strMessage = MakeMobileRechargeAPI5(intUserID, objODBC.executeScalar_str("select username FROM `er_login` where userid='" + intUserID + "'"), intOperatorType, intOperatorID, strMobile, dblAmount, strExiomsOpName, dblMyComm, dblResellerComm, strMyAPIType, strIPAddress, intDesignationType, strMacAddress, strOSDetails, strIMEINo, strGcmID, strAPPVersion, intSourceData, intCreatedByType, "0");

                                string[] strResponseMessageEasyPay = strMessage.Split(new char[] { ',' });

                                string strStatusRes = strResponseMessageEasyPay[0];

                                if (strStatusRes == "SUCCESS")
                                {
                                    string strStatusMessage = strResponseMessageEasyPay[1];
                                    string strStatusTopUpTransactionID = strResponseMessageEasyPay[2];
                                    string strStatusOperaterTransID = strResponseMessageEasyPay[3];
                                    arrUserParams.Add("TRUE");
                                    arrUserParams.Add(strStatusMessage);
                                    arrUserParams.Add(strStatusTopUpTransactionID);
                                    arrUserParams.Add(strStatusOperaterTransID);
                                    return arrUserParams;

                                }
                                else if (strStatusRes == "FAILED")
                                {
                                    string strStatusMessage = strResponseMessageEasyPay[1];
                                    string strStatusTopUpTransactionID = strResponseMessageEasyPay[2];
                                    string strStatusOperaterTransID = strResponseMessageEasyPay[3];
                                    arrUserParams.Add("FALSE");
                                    arrUserParams.Add(strStatusMessage);
                                    arrUserParams.Add(strStatusTopUpTransactionID);
                                    arrUserParams.Add(strStatusOperaterTransID);
                                    return arrUserParams;
                                }
                                else if (strStatusRes == "PENDING")
                                {
                                    string strStatusMessage = strResponseMessageEasyPay[1];
                                    string strStatusTopUpTransactionID = strResponseMessageEasyPay[2];
                                    string strStatusOperaterTransID = strResponseMessageEasyPay[3];
                                    arrUserParams.Add("FALSE");
                                    arrUserParams.Add(strStatusRes);
                                    arrUserParams.Add(strStatusTopUpTransactionID);
                                    arrUserParams.Add(strStatusOperaterTransID);
                                    //    objODBC.executeNonQuery("INSERT INTO `temp`( `data`, `type`, `request`) VALUES('4','3','" + strStatusMessage + "')");
                                    return arrUserParams;
                                }
                                else
                                {
                                    // objODBC.executeNonQuery("INSERT INTO `temp`( `data`, `type`, `request`) VALUES('4','3','Invalid Details')");
                                    arrUserParams.Add("FALSE");
                                    arrUserParams.Add("Invalid Details");
                                    arrUserParams.Add("NA");
                                    arrUserParams.Add("NA");
                                    return arrUserParams;
                                }


                            }
                            else if (strMyAPIType == "6")
                            {
                                try
                                {
                                    //int intOperatorIDRechargeAPI = objNewRecharge.GetOperatorIDForRechargeAPI(intOperatorID);

                                    strExiomsOpName = "MTN";
                                    try
                                    {
                                        objODBCLOG.executeNonQuery("call activity_logs('" + intUserID + "','" + intDesignationType + "','" + intCreatedByType + "','" + strIPAddress + "','" + strMacAddress + "','" + strOSDetails + "','" + strIMEINo + "','" + strGcmID + "','" + strAPPVersion + "','" + intSourceData + "','" + strMessage + "','" + intActivityType + "','','')");
                                    }
                                    catch (Exception)
                                    {

                                    }
                                    strMessage = MakeMobileRechargeAPI6(intUserID, objODBC.executeScalar_str("select username FROM `er_login` where userid='" + intUserID + "'"), intOperatorType, intOperatorID, strMobile, dblAmount, strExiomsOpName, dblMyComm, dblResellerComm, strMyAPIType, strIPAddress, intDesignationType, strMacAddress, strOSDetails, strIMEINo, strGcmID, strAPPVersion, intSourceData, intCreatedByType, "0");

                                    string[] strResponseMessageMTN = strMessage.Split(new char[] { ',' });

                                    string strStatusRes = strResponseMessageMTN[0];
                                    if (strStatusRes == "SUCCESS")
                                    {
                                        string strStatusMessage = strResponseMessageMTN[1];
                                        string strStatusTopUpTransactionID = strResponseMessageMTN[2];
                                        string strStatusOperaterTransID = strResponseMessageMTN[3];
                                        arrUserParams.Add("TRUE");
                                        arrUserParams.Add(strStatusMessage);
                                        arrUserParams.Add(strStatusTopUpTransactionID);
                                        arrUserParams.Add(strStatusOperaterTransID);
                                        //    objODBC.executeNonQuery("INSERT INTO `temp`( `data`, `type`, `request`) VALUES('4','3','" + strStatusMessage + "')");
                                        return arrUserParams;
                                    }
                                    else if (strStatusRes == "FAILED")
                                    {
                                        string strStatusMessage = strResponseMessageMTN[1];
                                        string strStatusTopUpTransactionID = strResponseMessageMTN[2];
                                        string strStatusOperaterTransID = strResponseMessageMTN[3];
                                        arrUserParams.Add("FALSE");
                                        arrUserParams.Add(strStatusMessage);
                                        arrUserParams.Add(strStatusTopUpTransactionID);
                                        arrUserParams.Add(strStatusOperaterTransID);
                                        //    objODBC.executeNonQuery("INSERT INTO `temp`( `data`, `type`, `request`) VALUES('4','3','" + strStatusMessage + "')");
                                        return arrUserParams;
                                    }
                                    else
                                    {
                                        // objODBC.executeNonQuery("INSERT INTO `temp`( `data`, `type`, `request`) VALUES('4','3','Invalid Details')");
                                        arrUserParams.Add("FALSE");
                                        arrUserParams.Add("Invalid Details");
                                        arrUserParams.Add("NA");
                                        arrUserParams.Add("NA");
                                        return arrUserParams;
                                    }

                                }
                                catch (Exception)
                                {

                                }
                                return arrUserParams;
                            }
                            else
                            {
                                arrUserParams.Add("FALSE");
                                strMessage = "Invalid Details for Recharge !";
                                arrUserParams.Add(strMessage);
                                arrUserParams.Add("NA");
                                arrUserParams.Add("NA");
                                return arrUserParams;
                            }
                        }
                        else
                        {
                            arrUserParams.Add("FALSE");

                            arrUserParams.Add(strResponseMessage);
                            arrUserParams.Add("NA");
                            arrUserParams.Add("NA");
                            return arrUserParams;
                        }
                    }
                    else
                    {
                        arrUserParams.Add("FALSE");
                        strMessage = "Amount Should be Greater than 0.";
                        arrUserParams.Add(strMessage);
                        arrUserParams.Add("NA");
                        arrUserParams.Add("NA");
                        return arrUserParams;
                    }
                }
                catch (Exception e1)
                {

                    strMessage = "Exception / Error : " + e1.ToString();
                    arrUserParams.Add("FALSE");
                    arrUserParams.Add(strMessage);
                    arrUserParams.Add("NA");
                    arrUserParams.Add("NA");
                    return arrUserParams;
                }

            }
            catch (Exception e1)
            {

                strMessage = "FAILED,Exception,NA,NA";
                arrUserParams.Add(strMessage);
                arrUserParams.Add("NA");
                arrUserParams.Add("NA");
                return arrUserParams;
            }
        }



        // make USSD Topup



        [WebMethod]
        // ActivationContext Type=62
        // int intUserID=0, int intOperatorID=1, string strMobile=2, double dblAmount=3, string strIPAddress=4, int intDesignationType=5, string strMacAddress=6, string strOSDetails=7, string strIMEINo=8, string strGcmID=9, string strAPPVersion=10, int intSource=11, int intCreatedByType=12
        public ArrayList MakeMobileRechargeforUSSDTopup(int intUserID, int intOperatorID, string strMobile, double dblAmount, int intUSSDTopUp_ID, int intSourceData, string strPhone)
        {
            int intActivityType = 462;
            //  string strUserName; string strData; int intEUserType; int intType, intSourceData;
            string strMessage = "USSD Top Up", strExiomsOpName = "";

            ArrayList arrUserParams = new ArrayList();

            try
            {

                string strIPAddress = "NA";
                int intDesignationType = 5;
                string strMacAddress = "NA";
                string strOSDetails = "NA";
                string strIMEINo = "NA";
                string strGcmID = "NA";
                string strAPPVersion = "NA";

                int intCreatedByType = 1;
                try
                {
                    try
                    {
                        objCreateFile.CreateTextFile(strIPAddress, strMacAddress, strOSDetails, strIMEINo, strGcmID, strAPPVersion, intSourceData.ToString(), intCreatedByType, intActivityType, " Mobile Recharge Group Topup", "NA");
                    }
                    catch (Exception)
                    {

                    }

                    if (dblAmount > 0)
                    {
                        string strCommission = objODBC.executeScalar_str("call getCommissionForRecharge(" + intUserID + "," + intOperatorID + ")");

                        string[] strCommissionStructure = strCommission.Split(new char[] { ',' });

                        double dblMyComm = double.Parse(strCommissionStructure[0]);
                        dblMyComm = 100 * dblMyComm / (100 + dblMyComm);
                        string strMyAPIType = strCommissionStructure[1];
                        // ‘ 1-Salaam;2-Etisalat;3-Roshan;4-Easy Pay
                        double dblResellerComm = double.Parse(strCommissionStructure[2]);
                        int intOperatorType = 1;

                        // int intAPIType = ReturnAPITypeForOperator(intOperatorID)
                        //   string strExiomsOpName = objNewRecharge.GetExiomsOperatorName(intOperatorID);

                        string strCheckResponse = CheckRechargeRequest(intUserID, objODBC.executeScalar_str("select username FROM `er_login` where userid='" + intUserID + "'"), intOperatorType, intOperatorID, strMobile, dblAmount, dblMyComm, dblResellerComm, int.Parse(strMyAPIType));

                        string[] strResponseStructure = strCheckResponse.Split(new char[] { ',' });
                        // strMessage = "2,OOPs! You do not have sufficient balance.,Failed,NA,NA";

                        string strStatusCheckResponse = strResponseStructure[0];
                        string strResponseMessage = strResponseStructure[1];
                        string strResposneStatusMessage = strResponseStructure[2];
                        string strResposneStatus3 = strResponseStructure[3];
                        string strResposneStatus4 = strResponseStructure[4];
                        if (strStatusCheckResponse == "1")
                        {
                            if (strMyAPIType == "1")
                            {
                                try
                                {

                                    strExiomsOpName = "Salaam";
                                    try
                                    {

                                        objODBCLOG.executeNonQuery("call activity_logs('" + intUserID + "','" + intDesignationType + "','" + intCreatedByType + "','" + strIPAddress + "','" + strMacAddress + "','" + strOSDetails + "','" + strIMEINo + "','" + strGcmID + "','" + strAPPVersion + "','" + intSourceData + "','" + strMessage + "','" + intActivityType + "','','')");
                                    }
                                    catch (Exception)
                                    {

                                    }
                                    strMessage = MakeMobileRechargeAPI1(intUserID, objODBC.executeScalar_str("select username FROM `er_login` where userid='" + intUserID + "'"), intOperatorType, intOperatorID, strMobile, dblAmount, strExiomsOpName, dblMyComm, dblResellerComm, strMyAPIType, strIPAddress, intDesignationType, strMacAddress, strOSDetails, strIMEINo, strGcmID, strAPPVersion, intSourceData, intCreatedByType, strPhone);

                                    string[] strResponseMessageSalaam = strMessage.Split(new char[] { ',' });

                                    string strStatusRes = strResponseMessageSalaam[0];
                                    objODBC.executeNonQuery("update er_recharge set ussd_topup_id='" + intUSSDTopUp_ID + "' where id ='" + strResponseMessageSalaam[2] + "'");
                                    if (strStatusRes == "SUCCESS")
                                    {
                                        string strStatusMessage = strResponseMessageSalaam[1];
                                        string strStatusTopUpTransactionID = strResponseMessageSalaam[2];
                                        string strStatusOperaterTransID = strResponseMessageSalaam[3];
                                        arrUserParams.Add("TRUE");
                                        arrUserParams.Add(strStatusMessage);
                                        arrUserParams.Add(strStatusTopUpTransactionID);
                                        arrUserParams.Add(strStatusOperaterTransID);
                                        //    objODBC.executeNonQuery("INSERT INTO `temp`( `data`, `type`, `request`) VALUES('4','3','" + strStatusMessage + "')");
                                        return arrUserParams;
                                    }
                                    else if (strStatusRes == "FAILED")
                                    {
                                        string strStatusMessage = strResponseMessageSalaam[1];
                                        string strStatusTopUpTransactionID = strResponseMessageSalaam[2];
                                        string strStatusOperaterTransID = strResponseMessageSalaam[3];
                                        arrUserParams.Add("FALSE");
                                        arrUserParams.Add(strStatusMessage);
                                        arrUserParams.Add(strStatusTopUpTransactionID);
                                        arrUserParams.Add(strStatusOperaterTransID);
                                        //    objODBC.executeNonQuery("INSERT INTO `temp`( `data`, `type`, `request`) VALUES('4','3','" + strStatusMessage + "')");
                                        return arrUserParams;
                                    }
                                    else
                                    {
                                        // objODBC.executeNonQuery("INSERT INTO `temp`( `data`, `type`, `request`) VALUES('4','3','Invalid Details')");
                                        arrUserParams.Add("FALSE");
                                        arrUserParams.Add("Invalid Details");
                                        arrUserParams.Add("NA");
                                        arrUserParams.Add("NA");
                                        return arrUserParams;
                                    }

                                }
                                catch (Exception)
                                {

                                }
                                return arrUserParams;
                            }
                            else if (strMyAPIType == "2")
                            {

                                try
                                {
                                    //int intOperatorIDRechargeAPI = objNewRecharge.GetOperatorIDForRechargeAPI(intOperatorID);

                                    strExiomsOpName = "Etisalat";
                                    try
                                    {

                                        objODBCLOG.executeNonQuery("call activity_logs('" + intUserID + "','" + intDesignationType + "','" + intCreatedByType + "','" + strIPAddress + "','" + strMacAddress + "','" + strOSDetails + "','" + strIMEINo + "','" + strGcmID + "','" + strAPPVersion + "','" + intSourceData + "','" + strMessage + "','" + intActivityType + "','','')");
                                    }
                                    catch (Exception)
                                    {

                                    }
                                    strMessage = MakeMobileRechargeAPI2(intUserID, objODBC.executeScalar_str("select username FROM `er_login` where userid='" + intUserID + "'"), intOperatorType, intOperatorID, strMobile, dblAmount, strExiomsOpName, dblMyComm, dblResellerComm, strMyAPIType, strIPAddress, intDesignationType, strMacAddress, strOSDetails, strIMEINo, strGcmID, strAPPVersion, intSourceData, intCreatedByType, strPhone);

                                    string[] strResponseMessagEtisalat = strMessage.Split(new char[] { ',' });
                                    objODBC.executeNonQuery("update er_recharge set ussd_topup_id='" + intUSSDTopUp_ID + "' where id ='" + strResponseMessagEtisalat[2] + "'");
                                    string strStatusRes = strResponseMessagEtisalat[0];
                                    if (strStatusRes == "SUCCESS")
                                    {
                                        string strStatusMessage = strResponseMessagEtisalat[1];
                                        string strStatusTopUpTransactionID = strResponseMessagEtisalat[2];
                                        string strStatusOperaterTransID = strResponseMessagEtisalat[3];
                                        arrUserParams.Add("TRUE");
                                        arrUserParams.Add(strStatusMessage);
                                        arrUserParams.Add(strStatusTopUpTransactionID);
                                        arrUserParams.Add(strStatusOperaterTransID);
                                        //    objODBC.executeNonQuery("INSERT INTO `temp`( `data`, `type`, `request`) VALUES('4','3','" + strStatusMessage + "')");
                                        return arrUserParams;
                                    }
                                    else if (strStatusRes == "FAILED")
                                    {
                                        string strStatusMessage = strResponseMessagEtisalat[1];
                                        string strStatusTopUpTransactionID = strResponseMessagEtisalat[2];
                                        string strStatusOperaterTransID = strResponseMessagEtisalat[3];
                                        arrUserParams.Add("FALSE");
                                        arrUserParams.Add(strStatusMessage);
                                        arrUserParams.Add(strStatusTopUpTransactionID);
                                        arrUserParams.Add(strStatusOperaterTransID);
                                        //    objODBC.executeNonQuery("INSERT INTO `temp`( `data`, `type`, `request`) VALUES('4','3','" + strStatusMessage + "')");
                                        return arrUserParams;
                                    }
                                    else if (strStatusRes == "PENDING")
                                    {
                                        string strStatusMessage = strResponseMessagEtisalat[1];
                                        string strStatusTopUpTransactionID = strResponseMessagEtisalat[2];
                                        string strStatusOperaterTransID = strResponseMessagEtisalat[3];
                                        arrUserParams.Add("FALSE");
                                        arrUserParams.Add(strStatusRes);
                                        arrUserParams.Add(strStatusTopUpTransactionID);
                                        arrUserParams.Add(strStatusOperaterTransID);
                                        //    objODBC.executeNonQuery("INSERT INTO `temp`( `data`, `type`, `request`) VALUES('4','3','" + strStatusMessage + "')");
                                        return arrUserParams;
                                    }
                                    else
                                    {
                                        // objODBC.executeNonQuery("INSERT INTO `temp`( `data`, `type`, `request`) VALUES('4','3','Invalid Details')");
                                        arrUserParams.Add("FALSE");
                                        arrUserParams.Add("Invalid Details");
                                        arrUserParams.Add("NA");
                                        arrUserParams.Add("NA");
                                        return arrUserParams;
                                    }

                                }
                                catch (Exception)
                                {

                                }
                                return arrUserParams;
                            }
                            else if (strMyAPIType == "3")
                            {
                                try
                                {
                                    //int intOperatorIDRechargeAPI = objNewRecharge.GetOperatorIDForRechargeAPI(intOperatorID);

                                    strExiomsOpName = "Roshan";
                                    try
                                    {

                                        objODBCLOG.executeNonQuery("call activity_logs('" + intUserID + "','" + intDesignationType + "','" + intCreatedByType + "','" + strIPAddress + "','" + strMacAddress + "','" + strOSDetails + "','" + strIMEINo + "','" + strGcmID + "','" + strAPPVersion + "','" + intSourceData + "','" + strMessage + "','" + intActivityType + "','','')");
                                    }
                                    catch (Exception)
                                    {

                                    }
                                    strMessage = MakeMobileRechargeAPI3(intUserID, objODBC.executeScalar_str("select username FROM `er_login` where userid='" + intUserID + "'"), intOperatorType, intOperatorID, strMobile, dblAmount, strExiomsOpName, dblMyComm, dblResellerComm, strMyAPIType, strIPAddress, intDesignationType, strMacAddress, strOSDetails, strIMEINo, strGcmID, strAPPVersion, intSourceData, intCreatedByType, strPhone);

                                    string[] strResponseMessageRoshan = strMessage.Split(new char[] { ',' });

                                    string strStatusRes = strResponseMessageRoshan[0];
                                    objODBC.executeNonQuery("update er_recharge set ussd_topup_id='" + intUSSDTopUp_ID + "' where id ='" + strResponseMessageRoshan[2] + "'");
                                    if (strStatusRes == "SUCCESS")
                                    {
                                        string strStatusMessage = strResponseMessageRoshan[1];
                                        string strStatusTopUpTransactionID = strResponseMessageRoshan[2];
                                        string strStatusOperaterTransID = strResponseMessageRoshan[3];
                                        arrUserParams.Add("TRUE");
                                        arrUserParams.Add(strStatusMessage);
                                        arrUserParams.Add(strStatusTopUpTransactionID);
                                        arrUserParams.Add(strStatusOperaterTransID);
                                        //    objODBC.executeNonQuery("INSERT INTO `temp`( `data`, `type`, `request`) VALUES('4','3','" + strStatusMessage + "')");
                                        return arrUserParams;
                                    }
                                    else if (strStatusRes == "FAILED")
                                    {
                                        string strStatusMessage = strResponseMessageRoshan[1];
                                        string strStatusTopUpTransactionID = strResponseMessageRoshan[2];
                                        string strStatusOperaterTransID = strResponseMessageRoshan[3];
                                        arrUserParams.Add("FALSE");
                                        arrUserParams.Add(strStatusMessage);
                                        arrUserParams.Add(strStatusTopUpTransactionID);
                                        arrUserParams.Add(strStatusOperaterTransID);
                                        //    objODBC.executeNonQuery("INSERT INTO `temp`( `data`, `type`, `request`) VALUES('4','3','" + strStatusMessage + "')");
                                        return arrUserParams;
                                    }
                                    else
                                    {
                                        // objODBC.executeNonQuery("INSERT INTO `temp`( `data`, `type`, `request`) VALUES('4','3','Invalid Details')");
                                        arrUserParams.Add("FALSE");
                                        arrUserParams.Add("Invalid Details");
                                        arrUserParams.Add("NA");
                                        arrUserParams.Add("NA");
                                        return arrUserParams;
                                    }

                                }
                                catch (Exception)
                                {

                                }
                                return arrUserParams;
                            }
                            else if (strMyAPIType == "4")
                            {
                                //int intOperatorIDRechargeAPI = objNewRecharge.GetOperatorIDForRechargeAPI(intOperatorID);

                                strExiomsOpName = "Easy Pay";
                                try
                                {

                                    objODBCLOG.executeNonQuery("call activity_logs('" + intUserID + "','" + intDesignationType + "','" + intCreatedByType + "','" + strIPAddress + "','" + strMacAddress + "','" + strOSDetails + "','" + strIMEINo + "','" + strGcmID + "','" + strAPPVersion + "','" + intSourceData + "','" + strMessage + "','" + intActivityType + "','','')");
                                }
                                catch (Exception)
                                {

                                }
                                strMessage = MakeMobileRechargeAPI4(intUserID, objODBC.executeScalar_str("select username FROM `er_login` where userid='" + intUserID + "'"), intOperatorType, intOperatorID, strMobile, dblAmount, strExiomsOpName, dblMyComm, dblResellerComm, strMyAPIType, strIPAddress, intDesignationType, strMacAddress, strOSDetails, strIMEINo, strGcmID, strAPPVersion, intSourceData, intCreatedByType, strPhone);

                                string[] strResponseMessageEasyPay = strMessage.Split(new char[] { ',' });

                                string strStatusRes = strResponseMessageEasyPay[0];
                                objODBC.executeNonQuery("update er_recharge set ussd_topup_id='" + intUSSDTopUp_ID + "' where id ='" + strResponseMessageEasyPay[2] + "'");
                                if (strStatusRes == "SUCCESS")
                                {
                                    string strStatusMessage = strResponseMessageEasyPay[1];
                                    string strStatusTopUpTransactionID = strResponseMessageEasyPay[2];
                                    string strStatusOperaterTransID = strResponseMessageEasyPay[3];
                                    arrUserParams.Add("TRUE");
                                    arrUserParams.Add(strStatusMessage);
                                    arrUserParams.Add(strStatusTopUpTransactionID);
                                    arrUserParams.Add(strStatusOperaterTransID);
                                    return arrUserParams;

                                }
                                else if (strStatusRes == "FAILED")
                                {
                                    string strStatusMessage = strResponseMessageEasyPay[1];
                                    string strStatusTopUpTransactionID = strResponseMessageEasyPay[2];
                                    string strStatusOperaterTransID = strResponseMessageEasyPay[3];
                                    arrUserParams.Add("FALSE");
                                    arrUserParams.Add(strStatusMessage);
                                    arrUserParams.Add(strStatusTopUpTransactionID);
                                    arrUserParams.Add(strStatusOperaterTransID);
                                    return arrUserParams;
                                }
                                else if (strStatusRes == "PENDING")
                                {
                                    string strStatusMessage = strResponseMessageEasyPay[1];
                                    string strStatusTopUpTransactionID = strResponseMessageEasyPay[2];
                                    string strStatusOperaterTransID = strResponseMessageEasyPay[3];
                                    arrUserParams.Add("FALSE");
                                    arrUserParams.Add(strStatusRes);
                                    arrUserParams.Add(strStatusTopUpTransactionID);
                                    arrUserParams.Add(strStatusOperaterTransID);
                                    //    objODBC.executeNonQuery("INSERT INTO `temp`( `data`, `type`, `request`) VALUES('4','3','" + strStatusMessage + "')");
                                    return arrUserParams;
                                }
                                else
                                {
                                    // objODBC.executeNonQuery("INSERT INTO `temp`( `data`, `type`, `request`) VALUES('4','3','Invalid Details')");
                                    arrUserParams.Add("FALSE");
                                    arrUserParams.Add("Invalid Details");
                                    arrUserParams.Add("NA");
                                    arrUserParams.Add("NA");
                                    return arrUserParams;
                                }


                            }
                            else if (strMyAPIType == "5")
                            {
                                //int intOperatorIDRechargeAPI = objNewRecharge.GetOperatorIDForRechargeAPI(intOperatorID);
                                if (intOperatorID == 4)
                                {
                                    strExiomsOpName = "MTN";
                                }
                                else
                                {
                                    strExiomsOpName = "AWCC";
                                }
                                try
                                {
                                    objODBCLOG.executeNonQuery("call activity_logs('" + intUserID + "','" + intDesignationType + "','" + intCreatedByType + "','" + strIPAddress + "','" + strMacAddress + "','" + strOSDetails + "','" + strIMEINo + "','" + strGcmID + "','" + strAPPVersion + "','" + intSourceData + "','" + strMessage + "','" + intActivityType + "','','')");
                                }
                                catch (Exception)
                                {

                                }
                                strMessage = MakeMobileRechargeAPI5(intUserID, objODBC.executeScalar_str("select username FROM `er_login` where userid='" + intUserID + "'"), intOperatorType, intOperatorID, strMobile, dblAmount, strExiomsOpName, dblMyComm, dblResellerComm, strMyAPIType, strIPAddress, intDesignationType, strMacAddress, strOSDetails, strIMEINo, strGcmID, strAPPVersion, intSourceData, intCreatedByType, strPhone);

                                string[] strResponseMessageEasyPay = strMessage.Split(new char[] { ',' });

                                string strStatusRes = strResponseMessageEasyPay[0];

                                if (strStatusRes == "SUCCESS")
                                {
                                    string strStatusMessage = strResponseMessageEasyPay[1];
                                    string strStatusTopUpTransactionID = strResponseMessageEasyPay[2];
                                    string strStatusOperaterTransID = strResponseMessageEasyPay[3];
                                    arrUserParams.Add("TRUE");
                                    arrUserParams.Add(strStatusMessage);
                                    arrUserParams.Add(strStatusTopUpTransactionID);
                                    arrUserParams.Add(strStatusOperaterTransID);
                                    return arrUserParams;

                                }
                                else if (strStatusRes == "FAILED")
                                {
                                    string strStatusMessage = strResponseMessageEasyPay[1];
                                    string strStatusTopUpTransactionID = strResponseMessageEasyPay[2];
                                    string strStatusOperaterTransID = strResponseMessageEasyPay[3];
                                    arrUserParams.Add("FALSE");
                                    arrUserParams.Add(strStatusMessage);
                                    arrUserParams.Add(strStatusTopUpTransactionID);
                                    arrUserParams.Add(strStatusOperaterTransID);
                                    return arrUserParams;
                                }
                                else if (strStatusRes == "PENDING")
                                {
                                    string strStatusMessage = strResponseMessageEasyPay[1];
                                    string strStatusTopUpTransactionID = strResponseMessageEasyPay[2];
                                    string strStatusOperaterTransID = strResponseMessageEasyPay[3];
                                    arrUserParams.Add("FALSE");
                                    arrUserParams.Add(strStatusRes);
                                    arrUserParams.Add(strStatusTopUpTransactionID);
                                    arrUserParams.Add(strStatusOperaterTransID);
                                    //    objODBC.executeNonQuery("INSERT INTO `temp`( `data`, `type`, `request`) VALUES('4','3','" + strStatusMessage + "')");
                                    return arrUserParams;
                                }
                                else
                                {
                                    // objODBC.executeNonQuery("INSERT INTO `temp`( `data`, `type`, `request`) VALUES('4','3','Invalid Details')");
                                    arrUserParams.Add("FALSE");
                                    arrUserParams.Add("Invalid Details");
                                    arrUserParams.Add("NA");
                                    arrUserParams.Add("NA");
                                    return arrUserParams;
                                }


                            }
                            else if (strMyAPIType == "6")
                            {
                                try
                                {
                                    //int intOperatorIDRechargeAPI = objNewRecharge.GetOperatorIDForRechargeAPI(intOperatorID);

                                    strExiomsOpName = "MTN";
                                    try
                                    {
                                        objODBCLOG.executeNonQuery("call activity_logs('" + intUserID + "','" + intDesignationType + "','" + intCreatedByType + "','" + strIPAddress + "','" + strMacAddress + "','" + strOSDetails + "','" + strIMEINo + "','" + strGcmID + "','" + strAPPVersion + "','" + intSourceData + "','" + strMessage + "','" + intActivityType + "','','')");
                                    }
                                    catch (Exception)
                                    {

                                    }
                                    strMessage = MakeMobileRechargeAPI6(intUserID, objODBC.executeScalar_str("select username FROM `er_login` where userid='" + intUserID + "'"), intOperatorType, intOperatorID, strMobile, dblAmount, strExiomsOpName, dblMyComm, dblResellerComm, strMyAPIType, strIPAddress, intDesignationType, strMacAddress, strOSDetails, strIMEINo, strGcmID, strAPPVersion, intSourceData, intCreatedByType, strPhone);

                                    string[] strResponseMessageMTN = strMessage.Split(new char[] { ',' });

                                    string strStatusRes = strResponseMessageMTN[0];
                                    if (strStatusRes == "SUCCESS")
                                    {
                                        string strStatusMessage = strResponseMessageMTN[1];
                                        string strStatusTopUpTransactionID = strResponseMessageMTN[2];
                                        string strStatusOperaterTransID = strResponseMessageMTN[3];
                                        arrUserParams.Add("TRUE");
                                        arrUserParams.Add(strStatusMessage);
                                        arrUserParams.Add(strStatusTopUpTransactionID);
                                        arrUserParams.Add(strStatusOperaterTransID);
                                        //    objODBC.executeNonQuery("INSERT INTO `temp`( `data`, `type`, `request`) VALUES('4','3','" + strStatusMessage + "')");
                                        return arrUserParams;
                                    }
                                    else if (strStatusRes == "FAILED")
                                    {
                                        string strStatusMessage = strResponseMessageMTN[1];
                                        string strStatusTopUpTransactionID = strResponseMessageMTN[2];
                                        string strStatusOperaterTransID = strResponseMessageMTN[3];
                                        arrUserParams.Add("FALSE");
                                        arrUserParams.Add(strStatusMessage);
                                        arrUserParams.Add(strStatusTopUpTransactionID);
                                        arrUserParams.Add(strStatusOperaterTransID);
                                        //    objODBC.executeNonQuery("INSERT INTO `temp`( `data`, `type`, `request`) VALUES('4','3','" + strStatusMessage + "')");
                                        return arrUserParams;
                                    }
                                    else
                                    {
                                        // objODBC.executeNonQuery("INSERT INTO `temp`( `data`, `type`, `request`) VALUES('4','3','Invalid Details')");
                                        arrUserParams.Add("FALSE");
                                        arrUserParams.Add("Invalid Details");
                                        arrUserParams.Add("NA");
                                        arrUserParams.Add("NA");
                                        return arrUserParams;
                                    }

                                }
                                catch (Exception)
                                {

                                }
                                return arrUserParams;
                            }
                            else
                            {
                                arrUserParams.Add("FALSE");
                                strMessage = "Invalid Details for Recharge !";
                                arrUserParams.Add(strMessage);
                                arrUserParams.Add("NA");
                                arrUserParams.Add("NA");
                                return arrUserParams;
                            }
                        }
                        else
                        {
                            arrUserParams.Add("FALSE");

                            arrUserParams.Add(strResponseMessage);
                            arrUserParams.Add("NA");
                            arrUserParams.Add("NA");
                            return arrUserParams;
                        }
                    }
                    else
                    {
                        arrUserParams.Add("FALSE");
                        strMessage = "Amount Should be Greater than 0.";
                        arrUserParams.Add(strMessage);
                        arrUserParams.Add("NA");
                        arrUserParams.Add("NA");
                        return arrUserParams;
                    }
                }
                catch (Exception e1)
                {

                    strMessage = "Invalid Details ";
                    arrUserParams.Add("FALSE");
                    arrUserParams.Add(strMessage);
                    arrUserParams.Add("NA");
                    arrUserParams.Add("NA");
                    return arrUserParams;
                }

            }
            catch (Exception e1)
            {

                strMessage = "FAILED,Exception,NA,NA";
                arrUserParams.Add(strMessage);
                arrUserParams.Add("NA");
                arrUserParams.Add("NA");
                return arrUserParams;
            }
        }


        //(int intUserID=0, int intOperatorID=1,int intGroupID=2, double dblAmount=3, string strIPAddress=4, int intDesignationType=5, string strMacAddress=6, string strOSDetails=7, string strIMEINo=8, string strGcmID=9, string strAPPVersion=10, int intSourceData=11, int intCreatedByType=12)

        [WebMethod]
        public ArrayList er_GroupTopup(string strUserName, string strData, int intType, int intSource, int intEUserType)
        {
            ArrayList arlst = new ArrayList();
            int intUserID = 0, intOperatorID = 0, intGroupID = 0, intActivityType = 0;
            string strConcatMob = string.Empty;
            int intPID = 0;
            double dblAmount = 0;
            var data = ED.Decrypt(strUserName, strData, intEUserType, intType, intSource).Split(',');
            try
            {
                intUserID = int.Parse(data[0]);
                intOperatorID = int.Parse(data[1]);
                intGroupID = int.Parse(data[2]);
                dblAmount = double.Parse(data[3]);
                string strIPAddress = data[4];
                int intDesignationType = int.Parse(data[5]);
                string strMacAddress = data[6];
                string strOSDetails = data[7];
                string strIMEINo = data[8];
                string strGcmID = data[9];
                string strAPPVersion = data[10];
                int intSourceData = int.Parse(data[11]);
                int intCreatedByType = int.Parse(data[12]);

                try
                {
                    objCreateFile.CreateTextFile(strIPAddress, strMacAddress, strOSDetails, strIMEINo, strGcmID, strAPPVersion, intSourceData.ToString(), intCreatedByType, intActivityType, " Group Topup" + ",Data:" + strData.ToString(), strUserName);
                }
                catch (Exception)
                {

                }
                try
                {
                    objODBCLOG.executeNonQuery("call activity_logs('" + intUserID + "','" + intDesignationType + "','" + intCreatedByType + "','" + strIPAddress + "','" + strMacAddress + "','" + strOSDetails + "','" + strIMEINo + "','" + strGcmID + "','" + strAPPVersion + "','" + intSourceData + "','Group Topup','" + intActivityType + "','','')");
                }
                catch (Exception)
                {

                }
            }
            catch
            {

            }
            finally
            {
                try
                {
                    // intPID = objODBC.executeScalar_int("select parent_id from er_login where userid =" + intUserID + "");
                    if (intUserID > 0 && dblAmount > 0 && intOperatorID > 0 && intGroupID > 0)
                    {
                        int intchkGroup = objODBC.executeScalar_int("select count(1) from er_top_up_group_details where group_id =" + intGroupID + "");
                        if (intchkGroup > 0)
                        {
                            Guid strGuid = Guid.NewGuid();
                            string strTranID = strGuid.ToString() + intGroupID.ToString() + dblAmount.ToString() + intGroupID.ToString();
                            //er_group_topup_inprocess status=0(pending) 1(inprocess) 2(done)
                            int intCount = objODBC.executeNonQuery("insert into er_group_topup_inprocess(tran_id,parentid,groupid,operatorid,amount,mobile,created_on,status) values ('" + strTranID + "','" + intUserID + "','" + intGroupID + "','" + intOperatorID + "','" + dblAmount + "',0,'" + objDateFor.getCurDateTimeString() + "','0')");
                            if (intCount > 0)
                            {
                                strConcatMob = objODBC.executeScalar_str("select GROUP_CONCAT(mobile_no) from er_top_up_group_details where group_id =" + intGroupID + "");

                                objODBC.executeNonQuery("update er_group_topup_inprocess set mobile='" + strConcatMob + "' where tran_id='" + strTranID + "'");

                                arlst.Add("TRUE");
                                arlst.Add("group topup is in processing");
                            }
                            else
                            {
                                arlst.Add("FALSE");
                                arlst.Add("Invalid Details");
                            }
                        }
                        else
                        {
                            arlst.Add("FALSE");
                            arlst.Add("Invalid Details");
                        }
                    }
                    else
                    {
                        arlst.Add("FALSE");
                        arlst.Add("Group is Empty");
                    }
                }
                catch (Exception)
                {
                    arlst.Add("FALSE");
                    arlst.Add("Something went Wrong");
                }

            }
            return arlst;
        }


        // intRechargeLogType= 0:Low Balance Recharge,1:Success,2:Fail ,3:Pending,4:Low Wallet Balance
        public void SendSMPPSMS(int intUserID, string strUserName, string strCousMobileNumber, string strOperaterName, int intOperaterID, double dblAmount, int intRechargeLogType, string TransactionID, string strPhone)
        {


            // Revert Balance
            try
            {
                Regex objMobilePatternMTN = new Regex(@"^(77|76)[\d]{7}$");
                Regex objMobilePatternAWCC = new Regex(@"^(70|71)[\d]{7}$");
                Regex objMobilePatternEtisalat = new Regex(@"^(78|73)[\d]{7}$");
                Regex objMobilePatternSalaam = new Regex(@"^74[\d]{7}$");
                Regex objMobilePatternRoshan = new Regex(@"^(79|72)[\d]{7}$");

                purchase_wallet1 objWallet = new purchase_wallet1();
                double dblWalletBalance = objWallet.GetWalletAmount(intUserID);
                // SMPP SMS Code Below
                SMPP_SMS_Format objSMPP_SMS_Format = new SMPP_SMS_Format();
                string strGetSMS = "NA";

                if (intRechargeLogType == 1) // Success
                {
                    strGetSMS = objSMPP_SMS_Format.TopupSuccessBalance(strUserName, objODBC.executeScalar_str("SELECT full_name From er_login Where userid='" + intUserID + "'").ToString(), strCousMobileNumber, strOperaterName, dblAmount, TransactionID, dblWalletBalance).ToString();

                    objCOMFUN.er_insert_notification(intUserID, "Top Up Success", strGetSMS, 2);


                    SMPP1 objSMPP1 = new SMPP1();
                    string strSMPPResponse = "NA";
                    DataTable dtSMPP = new DataTable();
                    try
                    {
                        if (strPhone == "0")
                        {
                            dtSMPP = objODBC.getDataTable("Select mobile,operator_id from er_mobile where userid='" + intUserID + "' and usertype=2 and mob_type=1");

                            if (dtSMPP.Rows.Count > 0)
                            {
                                string strMobile = dtSMPP.Rows[0][0].ToString();
                                int intOperaterRetID = int.Parse(dtSMPP.Rows[0][1].ToString());
                                try
                                {
                                    // objODBCLOG.executeNonQuery("INSERT INTO `er_wallet_transfer_sms_log`(`userid`, `username`, `mobile`, `operator_id`, `SMS`, `created_on`) VALUES ('" + intUserID + "','','" + strMobile + "','" + intOperaterRetID + "','" + strGetSMS + "','" + objDateFor.getCurDateTimeString() + "')");
                                }
                                catch (Exception)
                                {

                                }

                                objCOMFUN.er_SMS_Alert(intUserID, 2, strMobile, intOperaterRetID, strGetSMS, 1, strSMPPResponse);

                            }
                        }
                        else
                        {
                            if (strPhone.Length == 11)
                            {
                                strPhone = strPhone.Substring(2, strPhone.Length - 2);
                            }


                            string strMobile = "";
                            int intOperaterRetID = 0;


                            if (objMobilePatternSalaam.IsMatch(strPhone))
                            {
                                strMobile = strPhone;
                                intOperaterRetID = 1;

                            }
                            if (objMobilePatternEtisalat.IsMatch(strPhone))
                            {
                                strMobile = strPhone;
                                intOperaterRetID = 2;

                            }
                            if (objMobilePatternRoshan.IsMatch(strPhone))
                            {
                                strMobile = strPhone;
                                intOperaterRetID = 3;

                            }
                            if (objMobilePatternMTN.IsMatch(strPhone))
                            {
                                strMobile = strPhone;
                                intOperaterRetID = 4;

                            }
                            if (objMobilePatternAWCC.IsMatch(strPhone))
                            {
                                strMobile = strPhone;
                                intOperaterRetID = 5;
                            }

                            objCOMFUN.er_SMS_Alert(intUserID, 2, strMobile, intOperaterRetID, strGetSMS, 1, strSMPPResponse);
                            try
                            {
                                //  objODBCLOG.executeNonQuery("INSERT INTO `er_wallet_transfer_sms_log`(`userid`, `username`, `mobile`, `operator_id`, `SMS`, `created_on`) VALUES ('" + intUserID + "','','" + strMobile + "','" + intOperaterRetID + "','" + strGetSMS + "','" + objDateFor.getCurDateTimeString() + "')");
                            }
                            catch (Exception)
                            {

                            }

                        }
                    }
                    catch (Exception)
                    {

                    }
                    finally
                    {
                        dtSMPP.Dispose();
                    }
                }
                else if (intRechargeLogType == 0) // Low Balance Recharge
                {
                    strGetSMS = objSMPP_SMS_Format.TopupLowBalance(strUserName, objODBC.executeScalar_str("SELECT full_name From er_login Where userid='" + intUserID + "'").ToString(), strCousMobileNumber, strOperaterName, dblAmount, TransactionID, dblWalletBalance).ToString();

                    objCOMFUN.er_insert_notification(intUserID, "Top Up Fail", strGetSMS, 2);

                    SMPP1 objSMPP1 = new SMPP1();
                    string strSMPPResponse = "NA";
                    DataTable dtSMPP = new DataTable();
                    try
                    {
                        if (strPhone == "0")
                        {
                            dtSMPP = objODBC.getDataTable("Select mobile,operator_id from er_mobile where userid='" + intUserID + "' and usertype=2 and mob_type=1");

                            if (dtSMPP.Rows.Count > 0)
                            {
                                string strMobile = dtSMPP.Rows[0][0].ToString();
                                int intOperaterRetID = int.Parse(dtSMPP.Rows[0][1].ToString());
                                try
                                {
                                    //  objODBCLOG.executeNonQuery("INSERT INTO `er_wallet_transfer_sms_log`(`userid`, `username`, `mobile`, `operator_id`, `SMS`, `created_on`) VALUES ('" + intUserID + "','','" + strMobile + "','" + intOperaterRetID + "','" + strGetSMS + "','" + objDateFor.getCurDateTimeString() + "')");
                                }
                                catch (Exception)
                                {

                                }
                                objCOMFUN.er_SMS_Alert(intUserID, 2, strMobile, intOperaterRetID, strGetSMS, 1, strSMPPResponse);




                            }

                        }
                        else
                        {
                            if (strPhone.Length == 11)
                            {
                                strPhone = strPhone.Substring(2, strPhone.Length - 2);
                            }

                            string strMobile = "";
                            int intOperaterRetID = 0;


                            if (objMobilePatternSalaam.IsMatch(strPhone))
                            {
                                strMobile = strPhone;
                                intOperaterRetID = 1;

                            }
                            if (objMobilePatternEtisalat.IsMatch(strPhone))
                            {
                                strMobile = strPhone;
                                intOperaterRetID = 2;

                            }
                            if (objMobilePatternRoshan.IsMatch(strPhone))
                            {
                                strMobile = strPhone;
                                intOperaterRetID = 3;

                            }
                            if (objMobilePatternMTN.IsMatch(strPhone))
                            {
                                strMobile = strPhone;
                                intOperaterRetID = 4;

                            }
                            if (objMobilePatternAWCC.IsMatch(strPhone))
                            {
                                strMobile = strPhone;
                                intOperaterRetID = 5;
                            }

                            try
                            {
                                //  objODBCLOG.executeNonQuery("INSERT INTO `er_wallet_transfer_sms_log`(`userid`, `username`, `mobile`, `operator_id`, `SMS`, `created_on`) VALUES ('" + intUserID + "','','" + strMobile + "','" + intOperaterRetID + "','" + strGetSMS + "','" + objDateFor.getCurDateTimeString() + "')");
                            }
                            catch (Exception)
                            {

                            }
                            objCOMFUN.er_SMS_Alert(intUserID, 2, strMobile, intOperaterRetID, strGetSMS, 1, strSMPPResponse);

                        }
                    }
                    catch (Exception)
                    {

                    }
                    finally
                    {
                        dtSMPP.Dispose();
                    }
                }
                else if (intRechargeLogType == 2) // Fail
                {
                    strGetSMS = objSMPP_SMS_Format.TopupFailBalance(strUserName, objODBC.executeScalar_str("SELECT full_name From er_login Where userid='" + intUserID + "'").ToString(), strCousMobileNumber, strOperaterName, dblAmount, TransactionID, dblWalletBalance).ToString();

                    objCOMFUN.er_insert_notification(intUserID, "Top Up Fail", strGetSMS, 2);


                    SMPP1 objSMPP1 = new SMPP1();
                    string strSMPPResponse = "NA";
                    DataTable dtSMPP = new DataTable();
                    try
                    {
                        if (strPhone == "0")
                        {
                            dtSMPP = objODBC.getDataTable("Select mobile,operator_id from er_mobile where userid='" + intUserID + "' and usertype=2 and mob_type=1");

                            if (dtSMPP.Rows.Count > 0)
                            {
                                string strMobile = dtSMPP.Rows[0][0].ToString();
                                int intOperaterRetID = int.Parse(dtSMPP.Rows[0][1].ToString());
                                try
                                {
                                    //  objODBCLOG.executeNonQuery("INSERT INTO `er_wallet_transfer_sms_log`(`userid`, `username`, `mobile`, `operator_id`, `SMS`, `created_on`) VALUES ('" + intUserID + "','','" + strMobile + "','" + intOperaterRetID + "','" + strGetSMS + "','" + objDateFor.getCurDateTimeString() + "')");
                                }
                                catch (Exception)
                                {

                                }
                                objCOMFUN.er_SMS_Alert(intUserID, 2, strMobile, intOperaterRetID, strGetSMS, 1, strSMPPResponse);

                            }
                        }
                        else
                        {
                            if (strPhone.Length == 11)
                            {
                                strPhone = strPhone.Substring(2, strPhone.Length - 2);
                            }


                            string strMobile = "";
                            int intOperaterRetID = 0;


                            if (objMobilePatternSalaam.IsMatch(strPhone))
                            {
                                strMobile = strPhone;
                                intOperaterRetID = 1;

                            }
                            if (objMobilePatternEtisalat.IsMatch(strPhone))
                            {
                                strMobile = strPhone;
                                intOperaterRetID = 2;

                            }
                            if (objMobilePatternRoshan.IsMatch(strPhone))
                            {
                                strMobile = strPhone;
                                intOperaterRetID = 3;

                            }
                            if (objMobilePatternMTN.IsMatch(strPhone))
                            {
                                strMobile = strPhone;
                                intOperaterRetID = 4;

                            }
                            if (objMobilePatternAWCC.IsMatch(strPhone))
                            {
                                strMobile = strPhone;
                                intOperaterRetID = 5;
                            }
                            try
                            {
                                //  objODBCLOG.executeNonQuery("INSERT INTO `er_wallet_transfer_sms_log`(`userid`, `username`, `mobile`, `operator_id`, `SMS`, `created_on`) VALUES ('" + intUserID + "','','" + strMobile + "','" + intOperaterRetID + "','" + strGetSMS + "','" + objDateFor.getCurDateTimeString() + "')");
                            }
                            catch (Exception)
                            {

                            }
                            objCOMFUN.er_SMS_Alert(intUserID, 2, strMobile, intOperaterRetID, strGetSMS, 1, strSMPPResponse);
                        }
                    }
                    catch (Exception)
                    {

                    }
                    finally
                    {
                        dtSMPP.Dispose();
                    }
                }
                else if (intRechargeLogType == 3) // Pending
                {
                    strGetSMS = objSMPP_SMS_Format.TopupPendingBalance(strUserName, objODBC.executeScalar_str("SELECT full_name From er_login Where userid='" + intUserID + "'").ToString(), strCousMobileNumber, strOperaterName, dblAmount, TransactionID, dblWalletBalance).ToString();

                    objCOMFUN.er_insert_notification(intUserID, "Top Up Pending", strGetSMS, 2);

                    SMPP1 objSMPP1 = new SMPP1();
                    string strSMPPResponse = "NA";
                    DataTable dtSMPP = new DataTable();
                    try
                    {
                        if (strPhone == "0")
                        {
                            dtSMPP = objODBC.getDataTable("Select mobile,operator_id from er_mobile where userid='" + intUserID + "' and usertype=2 and mob_type=1");

                            if (dtSMPP.Rows.Count > 0)
                            {
                                string strMobile = dtSMPP.Rows[0][0].ToString();
                                int intOperaterRetID = int.Parse(dtSMPP.Rows[0][1].ToString());

                                try
                                {
                                    // objODBCLOG.executeNonQuery("INSERT INTO `er_wallet_transfer_sms_log`(`userid`, `username`, `mobile`, `operator_id`, `SMS`, `created_on`) VALUES ('" + intUserID + "','','" + strMobile + "','" + intOperaterRetID + "','" + strGetSMS + "','" + objDateFor.getCurDateTimeString() + "')");
                                }
                                catch (Exception)
                                {

                                }

                                objCOMFUN.er_SMS_Alert(intUserID, 2, strMobile, intOperaterRetID, strGetSMS, 1, strSMPPResponse);

                            }

                        }
                        else
                        {
                            if (strPhone.Length == 11)
                            {
                                strPhone = strPhone.Substring(2, strPhone.Length - 2);
                            }


                            string strMobile = "";
                            int intOperaterRetID = 0;


                            if (objMobilePatternSalaam.IsMatch(strPhone))
                            {
                                strMobile = strPhone;
                                intOperaterRetID = 1;

                            }
                            if (objMobilePatternEtisalat.IsMatch(strPhone))
                            {
                                strMobile = strPhone;
                                intOperaterRetID = 2;

                            }
                            if (objMobilePatternRoshan.IsMatch(strPhone))
                            {
                                strMobile = strPhone;
                                intOperaterRetID = 3;

                            }
                            if (objMobilePatternMTN.IsMatch(strPhone))
                            {
                                strMobile = strPhone;
                                intOperaterRetID = 4;

                            }
                            if (objMobilePatternAWCC.IsMatch(strPhone))
                            {
                                strMobile = strPhone;
                                intOperaterRetID = 5;
                            }
                            try
                            {
                                // objODBCLOG.executeNonQuery("INSERT INTO `er_wallet_transfer_sms_log`(`userid`, `username`, `mobile`, `operator_id`, `SMS`, `created_on`) VALUES ('" + intUserID + "','','" + strMobile + "','" + intOperaterRetID + "','" + strGetSMS + "','" + objDateFor.getCurDateTimeString() + "')");
                            }
                            catch (Exception)
                            {

                            }
                            objCOMFUN.er_SMS_Alert(intUserID, 2, strMobile, intOperaterRetID, strGetSMS, 1, strSMPPResponse);

                        }
                    }
                    catch (Exception)
                    {

                    }
                    finally
                    {
                        dtSMPP.Dispose();
                    }
                }
                else if (intRechargeLogType == 4) // Low Wallet Balance For Retailer
                {
                    strGetSMS = objSMPP_SMS_Format.LowBalance(strUserName, objODBC.executeScalar_str("SELECT full_name From er_login Where userid='" + intUserID + "'").ToString(), dblWalletBalance).ToString();

                    objCOMFUN.er_insert_notification(intUserID, "Top Up Fail", strGetSMS, 2);

                    SMPP1 objSMPP1 = new SMPP1();
                    string strSMPPResponse = "NA";
                    DataTable dtSMPP = new DataTable();
                    try
                    {
                        if (strPhone == "0")
                        {
                            dtSMPP = objODBC.getDataTable("Select mobile,operator_id from er_mobile where userid='" + intUserID + "' and usertype=2 and mob_type=1");

                            if (dtSMPP.Rows.Count > 0)
                            {
                                string strMobile = dtSMPP.Rows[0][0].ToString();
                                int intOperaterRetID = int.Parse(dtSMPP.Rows[0][1].ToString());

                                try
                                {
                                    // objODBCLOG.executeNonQuery("INSERT INTO `er_wallet_transfer_sms_log`(`userid`, `username`, `mobile`, `operator_id`, `SMS`, `created_on`) VALUES ('" + intUserID + "','','" + strMobile + "','" + intOperaterRetID + "','" + strGetSMS + "','" + objDateFor.getCurDateTimeString() + "')");
                                }
                                catch (Exception)
                                {

                                }

                                objCOMFUN.er_SMS_Alert(intUserID, 2, strMobile, intOperaterRetID, strGetSMS, 1, strSMPPResponse);

                            }

                        }
                        else
                        {
                            if (strPhone.Length == 11)
                            {
                                strPhone = strPhone.Substring(2, strPhone.Length - 2);
                            }


                            string strMobile = "";
                            int intOperaterRetID = 0;


                            if (objMobilePatternSalaam.IsMatch(strPhone))
                            {
                                strMobile = strPhone;
                                intOperaterRetID = 1;

                            }
                            if (objMobilePatternEtisalat.IsMatch(strPhone))
                            {
                                strMobile = strPhone;
                                intOperaterRetID = 2;

                            }
                            if (objMobilePatternRoshan.IsMatch(strPhone))
                            {
                                strMobile = strPhone;
                                intOperaterRetID = 3;

                            }
                            if (objMobilePatternMTN.IsMatch(strPhone))
                            {
                                strMobile = strPhone;
                                intOperaterRetID = 4;

                            }
                            if (objMobilePatternAWCC.IsMatch(strPhone))
                            {
                                strMobile = strPhone;
                                intOperaterRetID = 5;
                            }
                            try
                            {
                                // objODBCLOG.executeNonQuery("INSERT INTO `er_wallet_transfer_sms_log`(`userid`, `username`, `mobile`, `operator_id`, `SMS`, `created_on`) VALUES ('" + intUserID + "','','" + strMobile + "','" + intOperaterRetID + "','" + strGetSMS + "','" + objDateFor.getCurDateTimeString() + "')");
                            }
                            catch (Exception)
                            {

                            }
                            objCOMFUN.er_SMS_Alert(intUserID, 2, strMobile, intOperaterRetID, strGetSMS, 1, strSMPPResponse);
                        }
                    }
                    catch (Exception)
                    {

                    }
                    finally
                    {
                        dtSMPP.Dispose();
                    }
                }

                else if (intRechargeLogType == 5) // Success on Rollback
                {
                    strGetSMS = objSMPP_SMS_Format.ReverseTopupSuccessBalance(strUserName, objODBC.executeScalar_str("SELECT full_name From er_login Where userid='" + intUserID + "'").ToString(), strCousMobileNumber, strOperaterName, dblAmount, TransactionID, dblWalletBalance).ToString();

                    objCOMFUN.er_insert_notification(intUserID, "Top Up Rollback Fail", strGetSMS, 2);

                    SMPP1 objSMPP1 = new SMPP1();
                    string strSMPPResponse = "NA";
                    DataTable dtSMPP = new DataTable();
                    try
                    {

                        dtSMPP = objODBC.getDataTable("Select mobile,operator_id from er_mobile where userid='" + intUserID + "' and usertype=2 and mob_type=1");

                        if (dtSMPP.Rows.Count > 0)
                        {
                            string strMobile = dtSMPP.Rows[0][0].ToString();
                            int intOperaterRetID = int.Parse(dtSMPP.Rows[0][1].ToString());
                            try
                            {
                                // objODBCLOG.executeNonQuery("INSERT INTO `er_wallet_transfer_sms_log`(`userid`, `username`, `mobile`, `operator_id`, `SMS`, `created_on`) VALUES ('" + intUserID + "','','" + strMobile + "','" + intOperaterRetID + "','" + strGetSMS + "','" + objDateFor.getCurDateTimeString() + "')");
                            }
                            catch (Exception)
                            {

                            }
                            objCOMFUN.er_SMS_Alert(intUserID, 2, strMobile, intOperaterRetID, strGetSMS, 1, strSMPPResponse);
                        }
                    }
                    catch (Exception)
                    {

                    }
                    finally
                    {
                        dtSMPP.Dispose();
                    }
                }
                else if (intRechargeLogType == 6) // FAIL on Rollback
                {
                    strGetSMS = objSMPP_SMS_Format.ReverseTopupFailBalance(strUserName, objODBC.executeScalar_str("SELECT full_name From er_login Where userid='" + intUserID + "'").ToString(), strCousMobileNumber, strOperaterName, dblAmount, TransactionID, dblWalletBalance).ToString();

                    objCOMFUN.er_insert_notification(intUserID, "Top Up Rollback Fail", strGetSMS, 2);

                    SMPP1 objSMPP1 = new SMPP1();
                    string strSMPPResponse = "NA";
                    DataTable dtSMPP = new DataTable();
                    try
                    {
                        dtSMPP = objODBC.getDataTable("Select mobile,operator_id from er_mobile where userid='" + intUserID + "' and usertype=2 and mob_type=1");

                        if (dtSMPP.Rows.Count > 0)
                        {
                            string strMobile = dtSMPP.Rows[0][0].ToString();
                            int intOperaterRetID = int.Parse(dtSMPP.Rows[0][1].ToString());
                            try
                            {
                                // objODBCLOG.executeNonQuery("INSERT INTO `er_wallet_transfer_sms_log`(`userid`, `username`, `mobile`, `operator_id`, `SMS`, `created_on`) VALUES ('" + intUserID + "','','" + strMobile + "','" + intOperaterRetID + "','" + strGetSMS + "','" + objDateFor.getCurDateTimeString() + "')");
                            }
                            catch (Exception)
                            {

                            }

                            objCOMFUN.er_SMS_Alert(intUserID, 2, strMobile, intOperaterRetID, strGetSMS, 1, strSMPPResponse);

                        }

                    }
                    catch (Exception)
                    {

                    }
                    finally
                    {
                        dtSMPP.Dispose();
                    }
                }
            }
            catch (Exception ex)
            {
            }
        }


        // intRechargeLogType= 0:Low Balance Recharge,1:Success,2:Fail ,3:Pending,4:Low Wallet Balance
        public void SendSMPPSMSReverse(int intUserID, string strUserName, string strCousMobileNumber, string strOperaterName, int intOperaterID, double dblAmount, int intRechargeLogType, string TransactionID)
        {


            // Revert Balance
            try
            {
                purchase_wallet1 objWallet = new purchase_wallet1();
                double dblWalletBalance = objWallet.ReturnUsableWalletAmount(intUserID);
                // SMPP SMS Code Below
                SMPP_SMS_Format objSMPP_SMS_Format = new SMPP_SMS_Format();
                string strGetSMS = "NA";

                if (intRechargeLogType == 1) // Success
                {
                    strGetSMS = objSMPP_SMS_Format.ReverseTopupSuccessBalance(strUserName, objODBC.executeScalar_str("SELECT full_name From er_login Where userid='" + intUserID + "'").ToString(), strCousMobileNumber, strOperaterName, dblAmount, TransactionID, dblWalletBalance).ToString();

                    objCOMFUN.er_insert_notification(intUserID, "Top Up Success", strGetSMS, 2);


                    SMPP1 objSMPP1 = new SMPP1();
                    string strSMPPResponse = "NA";
                    DataTable dtSMPP = new DataTable();
                    try
                    {
                        dtSMPP = objODBC.getDataTable("Select mobile,operator_id from er_mobile where userid='" + intUserID + "' and usertype=2 and mob_type=1");

                        if (dtSMPP.Rows.Count > 0)
                        {
                            string strMobile = dtSMPP.Rows[0][0].ToString();
                            int intOperaterRetID = int.Parse(dtSMPP.Rows[0][1].ToString());

                            objCOMFUN.er_SMS_Alert(intUserID, 2, strMobile, intOperaterRetID, strGetSMS, 1, strSMPPResponse);

                        }
                    }
                    catch (Exception)
                    {

                    }
                    finally
                    {
                        dtSMPP.Dispose();
                    }
                }
                else if (intRechargeLogType == 0) // Low Balance Recharge
                {
                    strGetSMS = objSMPP_SMS_Format.TopupLowBalance(strUserName, objODBC.executeScalar_str("SELECT full_name From er_login Where userid='" + intUserID + "'").ToString(), strCousMobileNumber, strOperaterName, dblAmount, TransactionID, dblWalletBalance).ToString();

                    objCOMFUN.er_insert_notification(intUserID, "Top Up Pending", strGetSMS, 2);

                    SMPP1 objSMPP1 = new SMPP1();
                    string strSMPPResponse = "NA";
                    DataTable dtSMPP = new DataTable();
                    try
                    {
                        dtSMPP = objODBC.getDataTable("Select mobile,operator_id from er_mobile where userid='" + intUserID + "' and usertype=2 and mob_type=1");

                        if (dtSMPP.Rows.Count > 0)
                        {
                            string strMobile = dtSMPP.Rows[0][0].ToString();
                            int intOperaterRetID = int.Parse(dtSMPP.Rows[0][1].ToString());

                            objCOMFUN.er_SMS_Alert(intUserID, 2, strMobile, intOperaterRetID, strGetSMS, 1, strSMPPResponse);

                        }
                    }
                    catch (Exception)
                    {

                    }
                    finally
                    {
                        dtSMPP.Dispose();
                    }
                }
                else if (intRechargeLogType == 2) // Fail
                {
                    strGetSMS = objSMPP_SMS_Format.ReverseTopupFailBalance(strUserName, objODBC.executeScalar_str("SELECT full_name From er_login Where userid='" + intUserID + "'").ToString(), strCousMobileNumber, strOperaterName, dblAmount, TransactionID, dblWalletBalance).ToString();

                    objCOMFUN.er_insert_notification(intUserID, "Top Up Fail", strGetSMS, 2);


                    SMPP1 objSMPP1 = new SMPP1();
                    string strSMPPResponse = "NA";
                    DataTable dtSMPP = new DataTable();
                    try
                    {
                        dtSMPP = objODBC.getDataTable("Select mobile,operator_id from er_mobile where userid='" + intUserID + "' and usertype=2 and mob_type=1");

                        if (dtSMPP.Rows.Count > 0)
                        {
                            string strMobile = dtSMPP.Rows[0][0].ToString();
                            int intOperaterRetID = int.Parse(dtSMPP.Rows[0][1].ToString());

                            objCOMFUN.er_SMS_Alert(intUserID, 2, strMobile, intOperaterRetID, strGetSMS, 1, strSMPPResponse);

                        }
                    }
                    catch (Exception)
                    {

                    }
                    finally
                    {
                        dtSMPP.Dispose();
                    }
                }
                else if (intRechargeLogType == 3) // Pending
                {
                    strGetSMS = objSMPP_SMS_Format.ReverseTopupPendingBalance(strUserName, objODBC.executeScalar_str("SELECT full_name From er_login Where userid='" + intUserID + "'").ToString(), strCousMobileNumber, strOperaterName, dblAmount, TransactionID, dblWalletBalance).ToString();

                    objCOMFUN.er_insert_notification(intUserID, "Top Up Fail", strGetSMS, 2);

                    SMPP1 objSMPP1 = new SMPP1();
                    string strSMPPResponse = "NA";
                    DataTable dtSMPP = new DataTable();
                    try
                    {
                        dtSMPP = objODBC.getDataTable("Select mobile,operator_id from er_mobile where userid='" + intUserID + "' and usertype=2 and mob_type=1");

                        if (dtSMPP.Rows.Count > 0)
                        {
                            string strMobile = dtSMPP.Rows[0][0].ToString();
                            int intOperaterRetID = int.Parse(dtSMPP.Rows[0][1].ToString());

                            objCOMFUN.er_SMS_Alert(intUserID, 2, strMobile, intOperaterRetID, strGetSMS, 1, strSMPPResponse);

                        }
                    }
                    catch (Exception)
                    {

                    }
                    finally
                    {
                        dtSMPP.Dispose();
                    }
                }
                else if (intRechargeLogType == 4) // Low Wallet Balance For Retailer
                {
                    strGetSMS = objSMPP_SMS_Format.LowBalance(strUserName, objODBC.executeScalar_str("SELECT full_name From er_login Where userid='" + intUserID + "'").ToString(), dblWalletBalance).ToString();

                    objCOMFUN.er_insert_notification(intUserID, "Top Up Fail", strGetSMS, 2);

                    SMPP1 objSMPP1 = new SMPP1();
                    string strSMPPResponse = "NA";
                    DataTable dtSMPP = new DataTable();
                    try
                    {
                        dtSMPP = objODBC.getDataTable("Select mobile,operator_id from er_mobile where userid='" + intUserID + "' and usertype=2 and mob_type=1");

                        if (dtSMPP.Rows.Count > 0)
                        {
                            string strMobile = dtSMPP.Rows[0][0].ToString();
                            int intOperaterRetID = int.Parse(dtSMPP.Rows[0][1].ToString());

                            objCOMFUN.er_SMS_Alert(intUserID, 2, strMobile, intOperaterRetID, strGetSMS, 1, strSMPPResponse);

                        }
                    }
                    catch (Exception)
                    {

                    }
                    finally
                    {
                        dtSMPP.Dispose();
                    }
                }
            }
            catch (Exception ex)
            {
            }
        }



        [WebMethod]

        // ActivationContext Type=62
        // int intUserID=0, int intOperatorID=1, string strMobile=2, double dblAmount=3,string strIPAddress=4, int intDesignationType=5, string strMacAddress=6, string strOSDetails=7, string strIMEINo=8, string strGcmID=9, string strAPPVersion=10, int intSource=11, int intCreatedByType=12,string strTopupTransactionID=13,int intRollbackStatus=14
        public ArrayList MakeReverseMobileRecharge_Approve_Reject(string strUserName, string strData, int intEUserType, int intType, int intSource)
        {
            int intActivityType = 62;
            //  string strUserName; string strData; int intEUserType; int intType, intSourceData;
            string strMessage = "Rollback Top Up", strExiomsOpName = "";
            DataTable dt2 = new DataTable();
            ArrayList arrUserParams = new ArrayList();

            var data = ED.Decrypt(strUserName, strData, intEUserType, intType, intSource).Split(',');
            try
            {
                int intUserID = int.Parse(data[0]);
                int intOperatorID = int.Parse(data[1]);
                // intOperatorID = 3;
                string strMobile = data[2];
                //  strMobile = "0799477575";
                double dblAmount = double.Parse(data[3]);
                // dblAmount =31;
                string strIPAddress = data[4];
                int intDesignationType = int.Parse(data[5]);
                string strMacAddress = data[6];
                string strOSDetails = data[7];
                string strIMEINo = data[8];
                string strGcmID = data[9];
                string strAPPVersion = data[10];
                int intSourceData = int.Parse(data[11]);
                int intCreatedByType = int.Parse(data[12]);
                string strTopupTransactionID = data[13];
                int intRollbackStatus = int.Parse(data[14]);
                try
                {
                    try
                    {
                        objCreateFile.CreateTextFile(strIPAddress, strMacAddress, strOSDetails, strIMEINo, strGcmID, strAPPVersion, intSourceData.ToString(), intCreatedByType, intActivityType, " MakeReverseMobileRecharge" + ",Data:" + strData.ToString(), strUserName);
                    }
                    catch (Exception)
                    {

                    }

                    if (dblAmount > 0)
                    {

                        if (intOperatorID == 1)
                        {
                            try
                            {
                                strExiomsOpName = "Salaam";

                                dt2 = objODBC.getDataTable("call `ap_rollbackRechargeProcessing`('" + strTopupTransactionID + "','" + intRollbackStatus + "')");
                                if (dt2.Rows.Count > 0)
                                {
                                    string strRollStatus = dt2.Rows[0][0].ToString();
                                    if (strRollStatus == "TRUE")
                                    {
                                        arrUserParams.Add("TRUE");
                                        if (intRollbackStatus == 1)
                                        {
                                            arrUserParams.Add("Your Rollback topup Approved Successfully !");
                                        }
                                        else
                                        {
                                            try
                                            {
                                                intUserID = objODBC.executeScalar_int("select userid from er_recharge where id='" + strTopupTransactionID + "' order by id limit 1;");
                                                SendSMPPSMS(intUserID, objODBC.executeScalar_str("select username FROM `er_login` where userid='" + intUserID + "'"), strMobile, "Salaam", 2, dblAmount, 6, strTopupTransactionID.ToString(), "0"); // 6 Failed Reverse
                                            }
                                            catch (Exception)
                                            {

                                            }

                                            arrUserParams.Add("Your Rollback topup Reject Successfully !");
                                        }
                                        arrUserParams.Add("NA");
                                        arrUserParams.Add("NA");
                                    }
                                    else
                                    {
                                        arrUserParams.Add("FALSE");
                                        arrUserParams.Add("Rollback Top Up Transaction can not Process !");
                                        arrUserParams.Add("NA");
                                        arrUserParams.Add("NA");
                                    }
                                }

                            }
                            catch (Exception)
                            {

                            }
                            finally
                            {
                                dt2.Dispose();
                            }
                            return arrUserParams;
                        }
                        else if (intOperatorID == 3)
                        {
                            try
                            {
                                strExiomsOpName = "Roshan";

                                dt2 = objODBC.getDataTable("call `ap_rollbackRechargeProcessing`('" + strTopupTransactionID + "','" + intRollbackStatus + "')");
                                if (dt2.Rows.Count > 0)
                                {
                                    string strRollStatus = dt2.Rows[0][0].ToString();
                                    if (strRollStatus == "TRUE")
                                    {
                                        arrUserParams.Add("TRUE");
                                        if (intRollbackStatus == 1)
                                        {
                                            arrUserParams.Add("Your Rollback topup Approved Successfully !");
                                        }
                                        else
                                        {
                                            arrUserParams.Add("Your Rollback topup Reject Successfully !");
                                            try
                                            {
                                                intUserID = objODBC.executeScalar_int("select userid from er_recharge where id='" + strTopupTransactionID + "' order by id limit 1;");
                                                SendSMPPSMS(intUserID, objODBC.executeScalar_str("select username FROM `er_login` where userid='" + intUserID + "'"), strMobile, "Roshan", 2, dblAmount, 6, strTopupTransactionID.ToString(), "0"); // 6 Failed Reverse
                                            }
                                            catch (Exception)
                                            {

                                            }

                                        }
                                        arrUserParams.Add("NA");
                                        arrUserParams.Add("NA");
                                    }
                                    else
                                    {
                                        arrUserParams.Add("FALSE");
                                        arrUserParams.Add("Rollback Top Up Transaction can not Process !");
                                        arrUserParams.Add("NA");
                                        arrUserParams.Add("NA");
                                    }
                                }

                            }
                            catch (Exception)
                            {

                            }
                            finally
                            {
                                dt2.Dispose();
                            }
                            return arrUserParams;
                        }
                        else if (intOperatorID == 4)
                        {
                            try
                            {
                                strExiomsOpName = "MTN";

                                dt2 = objODBC.getDataTable("call `ap_rollbackRechargeProcessing`('" + strTopupTransactionID + "','" + intRollbackStatus + "')");
                                if (dt2.Rows.Count > 0)
                                {
                                    string strRollStatus = dt2.Rows[0][0].ToString();
                                    if (strRollStatus == "TRUE")
                                    {
                                        arrUserParams.Add("TRUE");
                                        if (intRollbackStatus == 1)
                                        {
                                            arrUserParams.Add("Your Rollback topup Approved Successfully !");
                                        }
                                        else
                                        {
                                            arrUserParams.Add("Your Rollback topup Reject Successfully !");

                                            try
                                            {
                                                intUserID = objODBC.executeScalar_int("select userid from er_recharge where id='" + strTopupTransactionID + "' order by id limit 1;");
                                                SendSMPPSMS(intUserID, objODBC.executeScalar_str("select username FROM `er_login` where userid='" + intUserID + "'"), strMobile, "MTN", 2, dblAmount, 6, strTopupTransactionID.ToString(), "0"); // 6 Failed Reverse
                                            }
                                            catch (Exception)
                                            {

                                            }
                                        }
                                        arrUserParams.Add("NA");
                                        arrUserParams.Add("NA");
                                    }
                                    else
                                    {
                                        arrUserParams.Add("FALSE");
                                        arrUserParams.Add("Rollback Top Up Transaction can not Process !");
                                        arrUserParams.Add("NA");
                                        arrUserParams.Add("NA");
                                    }
                                }

                            }
                            catch (Exception)
                            {

                            }
                            finally
                            {
                                dt2.Dispose();
                            }
                            return arrUserParams;

                        }
                        else if (intOperatorID == 5)
                        {
                            try
                            {
                                strExiomsOpName = "AWCC";

                                dt2 = objODBC.getDataTable("call `ap_rollbackRechargeProcessing`('" + strTopupTransactionID + "','" + intRollbackStatus + "')");
                                if (dt2.Rows.Count > 0)
                                {
                                    string strRollStatus = dt2.Rows[0][0].ToString();
                                    if (strRollStatus == "TRUE")
                                    {
                                        arrUserParams.Add("TRUE");
                                        if (intRollbackStatus == 1)
                                        {
                                            arrUserParams.Add("Your Rollback topup Approved Successfully !");
                                        }
                                        else
                                        {
                                            arrUserParams.Add("Your Rollback topup Reject Successfully !");
                                            try
                                            {
                                                intUserID = objODBC.executeScalar_int("select userid from er_recharge where id='" + strTopupTransactionID + "' order by id limit 1;");
                                                SendSMPPSMS(intUserID, objODBC.executeScalar_str("select username FROM `er_login` where userid='" + intUserID + "'"), strMobile, "AWCC", 2, dblAmount, 6, strTopupTransactionID.ToString(), "0"); // 6 Failed Reverse
                                            }
                                            catch (Exception)
                                            {

                                            }
                                        }
                                        arrUserParams.Add("NA");
                                        arrUserParams.Add("NA");
                                    }
                                    else
                                    {
                                        arrUserParams.Add("FALSE");
                                        arrUserParams.Add("Rollback Top Up Transaction can not Process !");
                                        arrUserParams.Add("NA");
                                        arrUserParams.Add("NA");
                                    }
                                }

                            }
                            catch (Exception)
                            {

                            }
                            finally
                            {
                                dt2.Dispose();
                            }
                            return arrUserParams;

                        }
                        else
                        {
                            arrUserParams.Add("FALSE");

                            arrUserParams.Add("Invalid Operater ID");
                            arrUserParams.Add("NA");
                            arrUserParams.Add("NA");
                            return arrUserParams;
                        }



                    }
                    else
                    {

                        arrUserParams.Add("FALSE");

                        arrUserParams.Add("Amount should be greater then 0");
                        arrUserParams.Add("NA");
                        arrUserParams.Add("NA");
                        return arrUserParams;
                    }
                }
                catch (Exception e1)
                {

                    strMessage = "Exception / Error : " + e1.ToString();
                    arrUserParams.Add("FALSE");
                    arrUserParams.Add(strMessage);
                    arrUserParams.Add("NA");
                    arrUserParams.Add("NA");
                    return arrUserParams;
                }

            }
            catch (Exception e1)
            {

                strMessage = "FAILED,Exception,NA,NA";
                arrUserParams.Add(strMessage);
                arrUserParams.Add("NA");
                arrUserParams.Add("NA");
                return arrUserParams;
            }
            return arrUserParams;
        }


        [WebMethod]

        // ActivationContext Type=62
        // int intUserID=0, int intOperatorID=1, string strMobile=2, double dblAmount=3,string strIPAddress=4, int intDesignationType=5, string strMacAddress=6, string strOSDetails=7, string strIMEINo=8, string strGcmID=9, string strAPPVersion=10, int intSource=11, int intCreatedByType=12,string strTopupTransactionID=13 ,int intRollbackStatus=14
        public ArrayList MakeReverseMobileCompleted_Status(string strUserName, string strData, int intEUserType, int intType, int intSource)
        {
            int intActivityType = 62;
            //  string strUserName; string strData; int intEUserType; int intType, intSourceData;
            string strMessage = "Rollback Top Up", strExiomsOpName = "";
            DataTable dt2 = new DataTable();
            ArrayList arrUserParams = new ArrayList();

            var data = ED.Decrypt(strUserName, strData, intEUserType, intType, intSource).Split(',');
            try
            {
                int intUserID = int.Parse(data[0]);
                int intOperatorID = int.Parse(data[1]);
                // intOperatorID = 3;
                string strMobile = data[2];
                //  strMobile = "0799477575";
                double dblAmount = double.Parse(data[3]);
                // dblAmount =31;
                string strIPAddress = data[4];
                int intDesignationType = int.Parse(data[5]);
                string strMacAddress = data[6];
                string strOSDetails = data[7];
                string strIMEINo = data[8];
                string strGcmID = data[9];
                string strAPPVersion = data[10];
                int intSourceData = int.Parse(data[11]);
                int intCreatedByType = int.Parse(data[12]);
                string strTopupTransactionID = data[13];
                int intRollbackStatus = int.Parse(data[14]);
                try
                {
                    try
                    {
                        objCreateFile.CreateTextFile(strIPAddress, strMacAddress, strOSDetails, strIMEINo, strGcmID, strAPPVersion, intSourceData.ToString(), intCreatedByType, intActivityType, " MakeReverseMobileCompleted_Status" + ",Data:" + strData.ToString(), strUserName);
                    }
                    catch (Exception)
                    {

                    }

                    if (dblAmount > 0)
                    {

                        if (intOperatorID == 1)
                        {
                            try
                            {
                                strExiomsOpName = "Salaam";

                                objODBC.executeNonQuery("call `ap_rollbackRechargeComplete`('" + intUserID + "','" + strTopupTransactionID + "','" + dblAmount + "')");
                                try
                                {
                                    intUserID = objODBC.executeScalar_int("select userid from er_recharge where id='" + strTopupTransactionID + "' order by id limit 1;");
                                    SendSMPPSMS(intUserID, objODBC.executeScalar_str("select username FROM `er_login` where userid='" + intUserID + "'"), strMobile, "Salaam", 2, dblAmount, 5, strTopupTransactionID.ToString(), "0"); // 5 Success Reverse
                                }
                                catch (Exception)
                                {

                                }


                                arrUserParams.Add("TRUE");
                                arrUserParams.Add("Your Rollback topup completed for Salaam !");
                                arrUserParams.Add("NA");
                                arrUserParams.Add("NA");
                                return arrUserParams;
                            }
                            catch (Exception)
                            {

                            }
                            finally
                            {
                                dt2.Dispose();
                            }
                            return arrUserParams;
                        }
                        else if (intOperatorID == 3)
                        {
                            try
                            {
                                strExiomsOpName = "Roshan";

                                objODBC.executeNonQuery("call `ap_rollbackRechargeComplete`('" + intUserID + "','" + strTopupTransactionID + "','" + dblAmount + "')");
                                try
                                {
                                    intUserID = objODBC.executeScalar_int("select userid from er_recharge where id='" + strTopupTransactionID + "' order by id limit 1;");
                                    SendSMPPSMS(intUserID, objODBC.executeScalar_str("select username FROM `er_login` where userid='" + intUserID + "'"), strMobile, "Roshan", 2, dblAmount, 5, strTopupTransactionID.ToString(), "0"); // 5 Success Reverse
                                }
                                catch (Exception)
                                {

                                }
                                arrUserParams.Add("TRUE");
                                arrUserParams.Add("Your Rollback topup completed for Roshan !");
                                arrUserParams.Add("NA");
                                arrUserParams.Add("NA");
                                return arrUserParams;

                            }
                            catch (Exception)
                            {

                            }
                            finally
                            {
                                dt2.Dispose();
                            }
                            return arrUserParams;
                        }
                        else if (intOperatorID == 4)
                        {
                            try
                            {
                                strExiomsOpName = "MTN";

                                objODBC.executeNonQuery("call `ap_rollbackRechargeComplete`('" + intUserID + "','" + strTopupTransactionID + "','" + dblAmount + "')");

                                try
                                {
                                    intUserID = objODBC.executeScalar_int("select userid from er_recharge where id='" + strTopupTransactionID + "' order by id limit 1;");
                                    SendSMPPSMS(intUserID, objODBC.executeScalar_str("select username FROM `er_login` where userid='" + intUserID + "'"), strMobile, "MTN", 4, dblAmount, 5, strTopupTransactionID.ToString(), "0"); // 5 Success Reverse
                                }
                                catch (Exception)
                                {

                                }
                                arrUserParams.Add("TRUE");
                                arrUserParams.Add("Your Rollback topup completed for MTN !");
                                arrUserParams.Add("NA");
                                arrUserParams.Add("NA");
                                return arrUserParams;

                            }
                            catch (Exception)
                            {

                            }
                            finally
                            {
                                dt2.Dispose();
                            }
                            return arrUserParams;

                        }
                        else if (intOperatorID == 5)
                        {
                            try
                            {
                                strExiomsOpName = "AWCC";

                                objODBC.executeNonQuery("call `ap_rollbackRechargeComplete`('" + intUserID + "','" + strTopupTransactionID + "','" + dblAmount + "')");
                                try
                                {
                                    intUserID = objODBC.executeScalar_int("select userid from er_recharge where id='" + strTopupTransactionID + "' order by id limit 1;");
                                    SendSMPPSMS(intUserID, objODBC.executeScalar_str("select username FROM `er_login` where userid='" + intUserID + "'"), strMobile, "AWCC", 2, dblAmount, 5, strTopupTransactionID.ToString(), "0"); // 5 Success Reverse
                                }
                                catch (Exception)
                                {

                                }

                                arrUserParams.Add("TRUE");
                                arrUserParams.Add("Your Rollback topup completed for AWCC !");
                                arrUserParams.Add("NA");
                                arrUserParams.Add("NA");
                                return arrUserParams;

                            }
                            catch (Exception)
                            {

                            }
                            finally
                            {
                                dt2.Dispose();
                            }
                            return arrUserParams;

                        }
                        else
                        {
                            arrUserParams.Add("FALSE");
                            arrUserParams.Add("Invalid Operater ID");
                            arrUserParams.Add("NA");
                            arrUserParams.Add("NA");
                            return arrUserParams;
                        }



                    }
                    else
                    {

                        arrUserParams.Add("FALSE");

                        arrUserParams.Add("Amount should be greater then 0");
                        arrUserParams.Add("NA");
                        arrUserParams.Add("NA");
                        return arrUserParams;
                    }
                }
                catch (Exception e1)
                {

                    strMessage = "Exception / Error : " + e1.ToString();
                    arrUserParams.Add("FALSE");
                    arrUserParams.Add(strMessage);
                    arrUserParams.Add("NA");
                    arrUserParams.Add("NA");
                    return arrUserParams;
                }

            }
            catch (Exception e1)
            {

                strMessage = "FAILED,Exception,NA,NA";
                arrUserParams.Add(strMessage);
                arrUserParams.Add("NA");
                arrUserParams.Add("NA");
                return arrUserParams;
            }
            return arrUserParams;
        }


        [WebMethod]

        // ActivationContext Type=62
        // int intUserID=0, int intOperatorID=1, string strMobile=2, double dblAmount=3,string strIPAddress=4, int intDesignationType=5, string strMacAddress=6, string strOSDetails=7, string strIMEINo=8, string strGcmID=9, string strAPPVersion=10, int intSource=11, int intCreatedByType=12,string strTopupTransactionID=13 
        public ArrayList MakeReverseMobileRecharge(string strUserName, string strData, int intEUserType, int intType, int intSource)
        {
            int intActivityType = 62;
            //  string strUserName; string strData; int intEUserType; int intType, intSourceData;
            string strMessage = "Rollback Top Up", strExiomsOpName = "";
            DataTable dt2 = new DataTable();
            ArrayList arrUserParams = new ArrayList();

            var data = ED.Decrypt(strUserName, strData, intEUserType, intType, intSource).Split(',');
            try
            {
                int intUserID = int.Parse(data[0]);
                int intOperatorID = int.Parse(data[1]);
                // intOperatorID = 3;
                string strMobile = data[2];
                //  strMobile = "0799477575";
                double dblAmount = double.Parse(data[3]);
                // dblAmount =31;
                string strIPAddress = data[4];
                int intDesignationType = int.Parse(data[5]);
                string strMacAddress = data[6];
                string strOSDetails = data[7];
                string strIMEINo = data[8];
                string strGcmID = data[9];
                string strAPPVersion = data[10];
                int intSourceData = int.Parse(data[11]);
                int intCreatedByType = int.Parse(data[12]);
                string strTopupTransactionID = data[13];

                try
                {
                    try
                    {
                        objCreateFile.CreateTextFile(strIPAddress, strMacAddress, strOSDetails, strIMEINo, strGcmID, strAPPVersion, intSourceData.ToString(), intCreatedByType, intActivityType, " MakeReverseMobileCompleted_Status" + ",Data:" + strData.ToString(), strUserName);
                    }
                    catch (Exception)
                    {

                    }
                    try
                    {
                        objODBCLOG.executeNonQuery("call activity_logs('" + intUserID + "','" + intDesignationType + "','" + intCreatedByType + "','" + strIPAddress + "','" + strMacAddress + "','" + strOSDetails + "','" + strIMEINo + "','" + strGcmID + "','" + strAPPVersion + "','" + intSourceData + "','" + strMessage + "','" + intActivityType + "','','')");
                    }
                    catch (Exception)
                    {

                    }

                    if (dblAmount > 0)
                    {

                        if (intOperatorID == 1)
                        {
                            try
                            {
                                strExiomsOpName = "Salaam";

                                objODBC.executeNonQuery("call `ap_rollbackRechargePending`('" + strTopupTransactionID + "')");
                                arrUserParams.Add("TRUE");
                                arrUserParams.Add("Your Rollback topup completed for Salaam !");
                                arrUserParams.Add("NA");
                                arrUserParams.Add("NA");
                                return arrUserParams;
                            }
                            catch (Exception)
                            {

                            }
                            finally
                            {
                                dt2.Dispose();
                            }
                            return arrUserParams;
                        }
                        else if (intOperatorID == 2)
                        {
                            // int intAPIType = ReturnAPITypeForOperator(intOperatorID)
                            //   string strExiomsOpName = objNewRecharge.GetExiomsOperatorName(intOperatorID);
                            intUserID = objODBC.executeScalar_int("select userid from er_recharge where id='" + strTopupTransactionID + "' order by id limit 1;");
                            string strCheckResponse = CheckReverseRequest(intUserID, strTopupTransactionID, intOperatorID, strMobile, dblAmount);

                            string[] strResponseStructure = strCheckResponse.Split(new char[] { ',' });
                            // strMessage = "2,OOPs! You do not have sufficient balance.,Failed,NA,NA";

                            string strStatusCheckResponse = strResponseStructure[0];
                            string strResponseMessage = strResponseStructure[1];
                            string strResposneStatusMessage = strResponseStructure[2];
                            string strResposneStatus3 = strResponseStructure[3];
                            string strResposneStatus4 = strResponseStructure[4];
                            if (strStatusCheckResponse == "1")
                            {

                                try
                                {
                                    //int intOperatorIDRechargeAPI = objNewRecharge.GetOperatorIDForRechargeAPI(intOperatorID);

                                    strExiomsOpName = "Etisalat";

                                    strMessage = ReverseTopupEtisalat(intUserID, objODBC.executeScalar_str("select username FROM `er_login` where userid='" + intUserID + "'").ToString(), int.Parse(strTopupTransactionID), intOperatorID, strMobile, dblAmount);

                                    string[] strResponseMessagEtisalat = strMessage.Split(new char[] { ',' });

                                    string strStatusRes = strResponseMessagEtisalat[0];
                                    if (strStatusRes == "SUCCESS")
                                    {
                                        string strStatusMessage = strResponseMessagEtisalat[1];
                                        string strStatusTopUpTransactionID = strResponseMessagEtisalat[2];
                                        string strStatusOperaterTransID = strResponseMessagEtisalat[3];
                                        arrUserParams.Add("TRUE");
                                        arrUserParams.Add(strStatusMessage);
                                        arrUserParams.Add(strStatusTopUpTransactionID);
                                        arrUserParams.Add(strStatusOperaterTransID);
                                        //    objODBC.executeNonQuery("INSERT INTO `temp`( `data`, `type`, `request`) VALUES('4','3','" + strStatusMessage + "')");
                                        return arrUserParams;
                                    }
                                    else if (strStatusRes == "FAILED")
                                    {
                                        string strStatusMessage = strResponseMessagEtisalat[1];
                                        string strStatusTopUpTransactionID = strResponseMessagEtisalat[2];
                                        string strStatusOperaterTransID = strResponseMessagEtisalat[3];
                                        arrUserParams.Add("FALSE");
                                        arrUserParams.Add(strStatusMessage);
                                        arrUserParams.Add(strStatusTopUpTransactionID);
                                        arrUserParams.Add(strStatusOperaterTransID);
                                        //    objODBC.executeNonQuery("INSERT INTO `temp`( `data`, `type`, `request`) VALUES('4','3','" + strStatusMessage + "')");
                                        return arrUserParams;
                                    }
                                    else if (strStatusRes == "PENDING")
                                    {
                                        string strStatusMessage = strResponseMessagEtisalat[1];
                                        string strStatusTopUpTransactionID = strResponseMessagEtisalat[2];
                                        string strStatusOperaterTransID = strResponseMessagEtisalat[3];
                                        arrUserParams.Add("TRUE");
                                        arrUserParams.Add("Reverse Topup is in Pending...");
                                        arrUserParams.Add(strStatusTopUpTransactionID);
                                        arrUserParams.Add(strStatusOperaterTransID);
                                        //    objODBC.executeNonQuery("INSERT INTO `temp`( `data`, `type`, `request`) VALUES('4','3','" + strStatusMessage + "')");
                                        return arrUserParams;
                                    }
                                    else
                                    {
                                        // objODBC.executeNonQuery("INSERT INTO `temp`( `data`, `type`, `request`) VALUES('4','3','Invalid Details')");
                                        arrUserParams.Add("FALSE");
                                        arrUserParams.Add("Invalid Details");
                                        arrUserParams.Add("NA");
                                        arrUserParams.Add("NA");
                                        return arrUserParams;
                                    }

                                }
                                catch (Exception)
                                {

                                }
                                return arrUserParams;

                            }
                            else
                            {
                                arrUserParams.Add("FALSE");

                                arrUserParams.Add(strResponseMessage);
                                arrUserParams.Add("NA");
                                arrUserParams.Add("NA");
                                return arrUserParams;
                            }
                        }
                        else if (intOperatorID == 3)
                        {
                            try
                            {
                                strExiomsOpName = "Roshan";

                                objODBC.executeNonQuery("call `ap_rollbackRechargePending`('" + strTopupTransactionID + "')");
                                arrUserParams.Add("TRUE");
                                arrUserParams.Add("Your Rollback topup completed for Roshan !");
                                arrUserParams.Add("NA");
                                arrUserParams.Add("NA");
                                return arrUserParams;

                            }
                            catch (Exception)
                            {

                            }
                            finally
                            {
                                dt2.Dispose();
                            }
                            return arrUserParams;
                        }
                        else if (intOperatorID == 4)
                        {
                            try
                            {
                                strExiomsOpName = "MTN";

                                objODBC.executeNonQuery("call `ap_rollbackRechargePending`('" + strTopupTransactionID + "')");
                                arrUserParams.Add("TRUE");
                                arrUserParams.Add("Your Rollback topup completed for MTN !");
                                arrUserParams.Add("NA");
                                arrUserParams.Add("NA");
                                return arrUserParams;

                            }
                            catch (Exception)
                            {

                            }
                            finally
                            {
                                dt2.Dispose();
                            }
                            return arrUserParams;

                        }
                        else if (intOperatorID == 5)
                        {
                            try
                            {
                                strExiomsOpName = "AWCC";

                                objODBC.executeNonQuery("call `ap_rollbackRechargePending`('" + strTopupTransactionID + "')");
                                arrUserParams.Add("TRUE");
                                arrUserParams.Add("Your Rollback topup completed for AWCC !");
                                arrUserParams.Add("NA");
                                arrUserParams.Add("NA");
                                return arrUserParams;

                            }
                            catch (Exception)
                            {

                            }
                            finally
                            {
                                dt2.Dispose();
                            }
                            return arrUserParams;

                        }
                        else
                        {
                            arrUserParams.Add("FALSE");
                            arrUserParams.Add("Invalid Operater ID");
                            arrUserParams.Add("NA");
                            arrUserParams.Add("NA");
                            return arrUserParams;
                        }



                    }
                    else
                    {

                        arrUserParams.Add("FALSE");

                        arrUserParams.Add("Amount should be greater then 0");
                        arrUserParams.Add("NA");
                        arrUserParams.Add("NA");
                        return arrUserParams;
                    }
                }
                catch (Exception e1)
                {

                    strMessage = "Exception / Error : " + e1.ToString();
                    arrUserParams.Add("FALSE");
                    arrUserParams.Add(strMessage);
                    arrUserParams.Add("NA");
                    arrUserParams.Add("NA");
                    return arrUserParams;
                }

            }
            catch (Exception e1)
            {

                strMessage = "Exception / Error : " + e1.ToString();
                arrUserParams.Add("FALSE");
                arrUserParams.Add(strMessage);
                arrUserParams.Add("NA");
                arrUserParams.Add("NA");
                return arrUserParams;
            }
            return arrUserParams;
        }


        public string ReverseTopupEtisalat(int intUserID, string strUserName, int intTransactionID, int intOperatorID, string strMobile, double dblAmount)
        {
            string strMessage = "";
            DataTable dt = new DataTable();
            // string strRequestType = "5"; For balance

            try
            {
                Regex objMobilePattern = new Regex(@"^(78|73)[\d]{7}$"); // Etisalat
                if (objMobilePattern.IsMatch(strMobile))
                {
                    if (dblAmount > 0)
                    {
                        int intReverseTransactionID = 0;
                        int intTopUpRecTransactionID = intTransactionID;

                        dt = objODBC.getDataTable("call `er_reverse_topup`('" + intUserID + "','" + strMobile + "','" + dblAmount + "','" + intOperatorID + "','" + intTransactionID + "')");
                        if (dt.Rows.Count > 0)
                        {
                            intReverseTransactionID = Convert.ToInt32(dt.Rows[0][0].ToString());
                            int intEtisalatID = Convert.ToInt32(dt.Rows[0][1].ToString());
                            if ((intTopUpRecTransactionID > 0) && (intReverseTransactionID > 0) && (intEtisalatID > 0))
                            {
                                try
                                {
                                    EtisalatTopup objEtisalat = new EtisalatTopup();
                                    try
                                    {
                                        objEtisalat.ETS_Topup(intTopUpRecTransactionID.ToString(), strMobile, int.Parse(dblAmount.ToString()), intEtisalatID, 2, intReverseTransactionID.ToString());
                                    }
                                    catch (Exception)
                                    {

                                    }
                                    string strQ = "SELECT Transaction_Response FROM etisalat_topup_request WHERE ReverseID =" + intReverseTransactionID + " order by id desc limit 1";

                                    string EtisalatResponse = objODBC.executeScalar_str(strQ);


                                    if (EtisalatResponse.ToString() != "NA")
                                    {
                                        if (Regex.IsMatch(EtisalatResponse, string.Format(@"\b{0}\b", Regex.Escape("ACK:RLLBCK CHRG VOUCH:RETN=0000,DESC=SUCCESS"))))
                                        {

                                            string strResposne = TruncateAtWord(EtisalatResponse, 64); // Truncate From Position of Words
                                            string output = EtisalatResponse.Replace(strResposne, ""); // Replace Words From Data
                                            output = output.Remove(output.Length - 8); // Deduct Words from Last                   
                                            string StrCheckSum = EtisalatResponse.Substring(EtisalatResponse.Length - 8); // Getting Checksum

                                            string[] strDescriptionResponse = output.Split(new char[] { ',' });
                                            // strMessage = "2,OOPs! You do not have sufficient balance.,Failed,NA,NA";
                                            string TRDStatus = strDescriptionResponse[0];
                                            string TRDDescription = strDescriptionResponse[1];


                                            string TXNID = objODBC.executeScalar_str("SELECT ReverseID_operaterID FROM etisalat_topup_request WHERE ReverseID='" + intReverseTransactionID + "'").ToString();
                                            objODBC.executeNonQuery("update er_recharge set getStatus=0,status=1 where id='" + intTopUpRecTransactionID + "'");
                                            try
                                            {
                                                objODBC.getDataTable("call `ap_rollbackRechargeProcessing`('" + intTopUpRecTransactionID + "','1')");
                                                objODBC.executeNonQuery("call `ap_rollbackRechargeComplete`('" + intUserID + "','" + intTopUpRecTransactionID + "','" + int.Parse(dblAmount.ToString()) + "')");
                                            }
                                            catch (Exception)
                                            {

                                            }
                                            strMessage = "SUCCESS," + "Reverse Top Up Transaction is Successfull ! ," + intReverseTransactionID + "," + intTopUpRecTransactionID;
                                            try
                                            {
                                                string strBalance = objODBC.executeScalar_str("SELECT amt FROM `er_sys_wallet` where id=2;");
                                                // objODBC.executeNonQuery("update er_recharge set comment='" + EtisalatResponse + "',operator_balance='" + strBalance + "' where id='" + intTopUpRecTransactionID + "'"); --Changes here
                                            }
                                            catch (Exception)
                                            {

                                            }

                                            try
                                            {
                                                objODBC.executeNonQuery("update etisalat_topup_request set transaction_DESC='" + TRDDescription + "',Transactio_Status='" + TRDStatus + "',Trans_Receive_Checksum='" + StrCheckSum + "',status=1 WHERE ReverseID =" + intReverseTransactionID + "");
                                            }
                                            catch (Exception)
                                            {

                                            }

                                            try
                                            {
                                                SendSMPPSMSReverse(intUserID, strUserName, strMobile, "Etisalat", intOperatorID, dblAmount, 1, intReverseTransactionID.ToString());
                                            }
                                            catch (Exception)
                                            {

                                            }
                                        }
                                        else
                                        {
                                            string strResposne = TruncateAtWord(EtisalatResponse, 64); // Truncate From Position of Words
                                            string output = EtisalatResponse.Replace(strResposne, ""); // Replace Words From Data
                                            output = output.Remove(output.Length - 8); // Deduct Words from Last                   
                                            string StrCheckSum = EtisalatResponse.Substring(EtisalatResponse.Length - 8); // Getting Checksum

                                            string[] strDescriptionResponse = output.Split(new char[] { ',' });
                                            // strMessage = "2,OOPs! You do not have sufficient balance.,Failed,NA,NA";

                                            string TRDStatus = strDescriptionResponse[0];
                                            string TRDDescription = strDescriptionResponse[1];


                                            string TXNID = objODBC.executeScalar_str("SELECT ReverseID_operaterID FROM etisalat_topup_request WHERE ReverseID='" + intReverseTransactionID + "'").ToString();
                                            objODBC.getDataTable("call `ap_rollbackRechargeProcessing`('" + intTopUpRecTransactionID + "','2')");

                                            strMessage = "FAILED," + "Reverse Top Up Transaction is FAILED !.. " + TRDDescription + "," + intReverseTransactionID + "," + TXNID + "";



                                            try
                                            {
                                                objODBC.executeNonQuery("update etisalat_topup_request set transaction_DESC='" + TRDDescription + "',Transactio_Status='" + TRDStatus + "',Trans_Receive_Checksum='" + StrCheckSum + "',status=2 WHERE ReverseID =" + intReverseTransactionID + "");
                                            }
                                            catch (Exception)
                                            {

                                            }

                                            try
                                            {
                                                SendSMPPSMSReverse(intUserID, strUserName, strMobile, "Etisalat", intOperatorID, dblAmount, 2, intReverseTransactionID.ToString());
                                            }
                                            catch (Exception)
                                            {

                                            }
                                        }
                                    }
                                    else
                                    {
                                        strMessage = "PENDING, Rollback Topup is in Pending...," + intTopUpRecTransactionID + ",NA";
                                        try
                                        {
                                            SendSMPPSMSReverse(intUserID, strUserName, strMobile, "Etisalat", intOperatorID, dblAmount, 3, intReverseTransactionID.ToString());
                                        }
                                        catch (Exception)
                                        {

                                        }

                                    }

                                    return strMessage;
                                }
                                catch (Exception ex)
                                {

                                    strMessage = "FAILED," + ex.ToString() + "," + intTopUpRecTransactionID + ",NA";
                                    return strMessage;

                                }
                            }
                            else
                            {

                                strMessage = "FAILED," + "Top Up Transaction is FAILED !..," + intTopUpRecTransactionID + ",NA";
                                return strMessage;
                            }
                        }
                        else
                        {
                            strMessage = "FAILED, Invalid Details" + intTopUpRecTransactionID + ",NA,NA";
                            return strMessage;
                        }
                    }
                    else
                    {
                        strMessage = "FAILED,Amount is too Low !,NA,NA";
                        try
                        {
                            SendSMPPSMSReverse(intUserID, strUserName, strMobile, "Etisalat", intOperatorID, dblAmount, 0, "");
                        }
                        catch (Exception)
                        {

                        }
                        return strMessage;
                    }
                }
                else
                {
                    strMessage = "FAILED,Mobile Number is not Exist for This Operater !,NA,NA";
                    return strMessage;
                }
            }
            catch (Exception)
            {
                strMessage = "FAILED,Exception,NA,NA";
                return strMessage;
            }
            finally
            {
                dt.Dispose();
            }
        }

        public string CheckReverseRequest(int intUserID, string intTransactionID, int intOperatorID, string strMobile, double dblAmount)
        {
            string strMessage = "";

            try
            {
                int intChkUserStatus = objODBC.executeScalar_int("Select Count(1) From er_login where userid='" + intUserID + "' and user_status=1 and Active=1");
                if (intChkUserStatus == 1)
                {
                    string strDate2 = objDateFor.getCurDateTimeString();
                    int intchkDuplicateRecgarge = objODBC.executeScalar_int("Select Count(1) From etisalat_reverse_topup where mobile='" + strMobile + "' and amount='" + dblAmount + "' and status<3 And TIMESTAMPDIFF(MINUTE,created_on,'" + strDate2 + "')<=5");

                    if (intchkDuplicateRecgarge == 0)
                    {
                        strMessage = "1,Your request in process,Pending,NA,NA";
                    }
                    else
                    {
                        strMessage = "2,You can not recharge on same mobile with same amount more than once within 5 minutes,Failed,NA,NA";

                        return strMessage;
                    }

                }
                else
                {
                    strMessage = "2,You can not recharge user is In Active,Failed,NA,NA";

                    return strMessage;
                }
            }
            catch (Exception ex)
            {
                strMessage = ex.Message.ToString();

                return "0," + strMessage + ",Failed,NA,NA";
            }
            return strMessage;
        }



        public ArrayList ReverseRechargeforUSSDTopup(int intUserID, int intOperatorID, string strMobile, double dblAmount, int intUSSDTopUp_ID, string strTopupTransactionID)
        {

            int intActivityType = 399;
            //  string strUserName; string strData; int intEUserType; int intType, intSourceData;
            string strMessage = "RollBack Top Up", strExiomsOpName = "";

            ArrayList arrUserParams = new ArrayList();

            try
            {
                // dblAmount =31;
                string strIPAddress = "NA";
                int intDesignationType = 5;
                string strMacAddress = "NA";
                string strOSDetails = "NA";
                string strIMEINo = "NA";
                string strGcmID = "NA";
                string strAPPVersion = "NA";
                int intSourceData = 2;
                int intCreatedByType = 2;

                try
                {
                    try
                    {
                        objCreateFile.CreateTextFile(strIPAddress, strMacAddress, strOSDetails, strIMEINo, strGcmID, strAPPVersion, intSourceData.ToString(), intCreatedByType, intActivityType, " ReverseRechargeforUSSDTopup", intUserID.ToString());
                    }
                    catch (Exception)
                    {

                    }

                    if (dblAmount > 0)
                    {

                        // int intAPIType = ReturnAPITypeForOperator(intOperatorID)
                        //   string strExiomsOpName = objNewRecharge.GetExiomsOperatorName(intOperatorID);
                        intUserID = objODBC.executeScalar_int("select userid from er_recharge where id='"+ strTopupTransactionID + "' order by id limit 1;");
                        string strCheckResponse = CheckReverseRequest(intUserID, strTopupTransactionID, intOperatorID, strMobile, dblAmount);

                        string[] strResponseStructure = strCheckResponse.Split(new char[] { ',' });
                        // strMessage = "2,OOPs! You do not have sufficient balance.,Failed,NA,NA";

                        string strStatusCheckResponse = strResponseStructure[0];
                        string strResponseMessage = strResponseStructure[1];
                        string strResposneStatusMessage = strResponseStructure[2];
                        string strResposneStatus3 = strResponseStructure[3];
                        string strResposneStatus4 = strResponseStructure[4];
                        if (strStatusCheckResponse == "1")
                        {
                            if (intOperatorID == 1)
                            {
                                try
                                {

                                    strExiomsOpName = "Salaam";

                                    arrUserParams.Add("FALSE");
                                    arrUserParams.Add("You can not do Reverse Topup for this Operater");
                                    arrUserParams.Add("NA");
                                    arrUserParams.Add("NA");

                                }
                                catch (Exception)
                                {

                                }
                                return arrUserParams;
                            }
                            else if (intOperatorID == 2)
                            {
                                try
                                {
                                    //int intOperatorIDRechargeAPI = objNewRecharge.GetOperatorIDForRechargeAPI(intOperatorID);

                                    strExiomsOpName = "Etisalat";
                                    try
                                    {
                                        objODBCLOG.executeNonQuery("call activity_logs('" + intUserID + "','" + intDesignationType + "','" + intCreatedByType + "','" + strIPAddress + "','" + strMacAddress + "','" + strOSDetails + "','" + strIMEINo + "','" + strGcmID + "','" + strAPPVersion + "','" + intSourceData + "','" + strMessage + "','" + intActivityType + "','','')");
                                    }
                                    catch (Exception)
                                    {

                                    }
                                    strMessage = ReverseTopupEtisalat(intUserID, objODBC.executeScalar_str("select username FROM `er_login` where userid='" + intUserID + "'").ToString(), int.Parse(strTopupTransactionID), intOperatorID, objODBC.executeScalar_str("select mobile_number FROM `er_recharge` where id='" + strTopupTransactionID + "'").ToString(), dblAmount);

                                    string[] strResponseMessagEtisalat = strMessage.Split(new char[] { ',' });

                                    string strStatusRes = strResponseMessagEtisalat[0];
                                    if (strStatusRes == "SUCCESS")
                                    {
                                        string strStatusMessage = strResponseMessagEtisalat[1];
                                        string strStatusTopUpTransactionID = strResponseMessagEtisalat[2];
                                        string strStatusOperaterTransID = strResponseMessagEtisalat[3];
                                        arrUserParams.Add("TRUE");
                                        arrUserParams.Add(strStatusMessage);
                                        arrUserParams.Add(strStatusTopUpTransactionID);
                                        arrUserParams.Add(strStatusOperaterTransID);
                                        //    objODBC.executeNonQuery("INSERT INTO `temp`( `data`, `type`, `request`) VALUES('4','3','" + strStatusMessage + "')");
                                        return arrUserParams;
                                    }
                                    else if (strStatusRes == "FAILED")
                                    {
                                        string strStatusMessage = strResponseMessagEtisalat[1];
                                        string strStatusTopUpTransactionID = strResponseMessagEtisalat[2];
                                        string strStatusOperaterTransID = strResponseMessagEtisalat[3];
                                        arrUserParams.Add("FALSE");
                                        arrUserParams.Add(strStatusMessage);
                                        arrUserParams.Add(strStatusTopUpTransactionID);
                                        arrUserParams.Add(strStatusOperaterTransID);
                                        //    objODBC.executeNonQuery("INSERT INTO `temp`( `data`, `type`, `request`) VALUES('4','3','" + strStatusMessage + "')");
                                        return arrUserParams;
                                    }
                                    else if (strStatusRes == "PENDING")
                                    {
                                        string strStatusMessage = strResponseMessagEtisalat[1];
                                        string strStatusTopUpTransactionID = strResponseMessagEtisalat[2];
                                        string strStatusOperaterTransID = strResponseMessagEtisalat[3];
                                        arrUserParams.Add("FALSE");
                                        arrUserParams.Add(strStatusRes);
                                        arrUserParams.Add(strStatusTopUpTransactionID);
                                        arrUserParams.Add(strStatusOperaterTransID);
                                        //    objODBC.executeNonQuery("INSERT INTO `temp`( `data`, `type`, `request`) VALUES('4','3','" + strStatusMessage + "')");
                                        return arrUserParams;
                                    }
                                    else
                                    {
                                        // objODBC.executeNonQuery("INSERT INTO `temp`( `data`, `type`, `request`) VALUES('4','3','Invalid Details')");
                                        arrUserParams.Add("FALSE");
                                        arrUserParams.Add("Invalid Details");
                                        arrUserParams.Add("NA");
                                        arrUserParams.Add("NA");
                                        return arrUserParams;
                                    }

                                }
                                catch (Exception)
                                {

                                }
                                return arrUserParams;
                            }
                            else if (intOperatorID == 3)
                            {
                                try
                                {
                                    //int intOperatorIDRechargeAPI = objNewRecharge.GetOperatorIDForRechargeAPI(intOperatorID);

                                    strExiomsOpName = "Roshan";

                                    arrUserParams.Add("FALSE");
                                    arrUserParams.Add("You can not do Reverse Topup for this Operater");
                                    arrUserParams.Add("NA");
                                    arrUserParams.Add("NA");
                                    return arrUserParams;
                                }
                                catch (Exception)
                                {

                                }
                            }
                            else if (intOperatorID == 4)
                            {
                                strExiomsOpName = "Easy Pay";


                                arrUserParams.Add("FALSE");
                                arrUserParams.Add("You can not do Reverse Topup for this Operater");
                                arrUserParams.Add("NA");
                                arrUserParams.Add("NA");
                                return arrUserParams;


                            }
                            else
                            {
                                arrUserParams.Add("FALSE");

                                arrUserParams.Add("Invalid Operater ID");
                                arrUserParams.Add("NA");
                                arrUserParams.Add("NA");
                                return arrUserParams;
                            }
                        }
                        else
                        {
                            arrUserParams.Add("FALSE");

                            arrUserParams.Add(strResponseMessage);
                            arrUserParams.Add("NA");
                            arrUserParams.Add("NA");
                            return arrUserParams;
                        }
                    }
                    else
                    {

                        arrUserParams.Add("FALSE");

                        arrUserParams.Add("Amount should be greater then 0");
                        arrUserParams.Add("NA");
                        arrUserParams.Add("NA");
                        return arrUserParams;
                    }
                }
                catch (Exception e1)
                {

                    strMessage = "Exception / Error : " + e1.ToString();
                    arrUserParams.Add("FALSE");
                    arrUserParams.Add(strMessage);
                    arrUserParams.Add("NA");
                    arrUserParams.Add("NA");
                    return arrUserParams;
                }

            }
            catch (Exception e1)
            {

                strMessage = "FAILED,Exception,NA,NA";
                arrUserParams.Add(strMessage);
                arrUserParams.Add("NA");
                arrUserParams.Add("NA");
                return arrUserParams;
            }
            return arrUserParams;
        }



        [WebMethod]
        public ArrayList ReverseRechargeforAdmin(int intUserID, int intOperatorID, string strMobile, double dblAmount, string strTopupTransactionID, string strIPAddress, int intDesignationType, string strMacAddress, string strOSDetails, string strIMEINo, string strGcmID, string strAPPVersion, int intSource, int intCreatedByType)
        {

            int intActivityType = 399;
            //  string strUserName; string strData; int intEUserType; int intType, intSourceData;
            string strMessage = "Rollback Top Up", strExiomsOpName = "";

            ArrayList arrUserParams = new ArrayList();

            try
            {
                // dblAmount =31;

                int intSourceData = intSource;


                try
                {
                    try
                    {
                        objCreateFile.CreateTextFile(strIPAddress, strMacAddress, strOSDetails, strIMEINo, strGcmID, strAPPVersion, intSourceData.ToString(), intCreatedByType, intActivityType, " Reverse Rollback Recharge", intUserID.ToString());
                    }
                    catch (Exception)
                    {

                    }

                    if (dblAmount > 0)
                    {

                        // int intAPIType = ReturnAPITypeForOperator(intOperatorID)
                        //   string strExiomsOpName = objNewRecharge.GetExiomsOperatorName(intOperatorID);
                        intUserID = objODBC.executeScalar_int("select userid from er_recharge where id='" + strTopupTransactionID + "' order by id limit 1;");
                        string strCheckResponse = CheckReverseRequest(intUserID, strTopupTransactionID, intOperatorID, strMobile, dblAmount);

                        string[] strResponseStructure = strCheckResponse.Split(new char[] { ',' });
                        // strMessage = "2,OOPs! You do not have sufficient balance.,Failed,NA,NA";

                        string strStatusCheckResponse = strResponseStructure[0];
                        string strResponseMessage = strResponseStructure[1];
                        string strResposneStatusMessage = strResponseStructure[2];
                        string strResposneStatus3 = strResponseStructure[3];
                        string strResposneStatus4 = strResponseStructure[4];
                        if (strStatusCheckResponse == "1")
                        {
                            if (intOperatorID == 1)
                            {
                                try
                                {

                                    strExiomsOpName = "Salaam";

                                    arrUserParams.Add("FALSE");
                                    arrUserParams.Add("You can not do Reverse Topup for this Operater");
                                    arrUserParams.Add("NA");
                                    arrUserParams.Add("NA");

                                }
                                catch (Exception)
                                {

                                }
                                return arrUserParams;
                            }
                            else if (intOperatorID == 2)
                            {
                                try
                                {
                                    //int intOperatorIDRechargeAPI = objNewRecharge.GetOperatorIDForRechargeAPI(intOperatorID);

                                    strExiomsOpName = "Etisalat";
                                    try
                                    {
                                        objODBCLOG.executeNonQuery("call activity_logs('" + intUserID + "','" + intDesignationType + "','" + intCreatedByType + "','" + strIPAddress + "','" + strMacAddress + "','" + strOSDetails + "','" + strIMEINo + "','" + strGcmID + "','" + strAPPVersion + "','" + intSourceData + "','" + strMessage + "','" + intActivityType + "','','')");
                                    }
                                    catch (Exception)
                                    {

                                    }
                                    strMessage = ReverseTopupEtisalat(intUserID, objODBC.executeScalar_str("select username FROM `er_login` where userid='" + intUserID + "'").ToString(), int.Parse(strTopupTransactionID), intOperatorID, objODBC.executeScalar_str("select mobile_number FROM `er_recharge` where id='" + strTopupTransactionID + "'").ToString(), dblAmount);

                                    string[] strResponseMessagEtisalat = strMessage.Split(new char[] { ',' });

                                    string strStatusRes = strResponseMessagEtisalat[0];
                                    if (strStatusRes == "SUCCESS")
                                    {
                                        string strStatusMessage = strResponseMessagEtisalat[1];
                                        string strStatusTopUpTransactionID = strResponseMessagEtisalat[2];
                                        string strStatusOperaterTransID = strResponseMessagEtisalat[3];
                                        arrUserParams.Add("TRUE");
                                        arrUserParams.Add(strStatusMessage);
                                        arrUserParams.Add(strStatusTopUpTransactionID);
                                        arrUserParams.Add(strStatusOperaterTransID);
                                        //    objODBC.executeNonQuery("INSERT INTO `temp`( `data`, `type`, `request`) VALUES('4','3','" + strStatusMessage + "')");
                                        return arrUserParams;
                                    }
                                    else if (strStatusRes == "FAILED")
                                    {
                                        string strStatusMessage = strResponseMessagEtisalat[1];
                                        string strStatusTopUpTransactionID = strResponseMessagEtisalat[2];
                                        string strStatusOperaterTransID = strResponseMessagEtisalat[3];
                                        arrUserParams.Add("FALSE");
                                        arrUserParams.Add(strStatusMessage);
                                        arrUserParams.Add(strStatusTopUpTransactionID);
                                        arrUserParams.Add(strStatusOperaterTransID);
                                        //    objODBC.executeNonQuery("INSERT INTO `temp`( `data`, `type`, `request`) VALUES('4','3','" + strStatusMessage + "')");
                                        return arrUserParams;
                                    }
                                    else if (strStatusRes == "PENDING")
                                    {
                                        string strStatusMessage = strResponseMessagEtisalat[1];
                                        string strStatusTopUpTransactionID = strResponseMessagEtisalat[2];
                                        string strStatusOperaterTransID = strResponseMessagEtisalat[3];
                                        arrUserParams.Add("FALSE");
                                        arrUserParams.Add(strStatusMessage);
                                        arrUserParams.Add(strStatusTopUpTransactionID);
                                        arrUserParams.Add(strStatusOperaterTransID);
                                        //    objODBC.executeNonQuery("INSERT INTO `temp`( `data`, `type`, `request`) VALUES('4','3','" + strStatusMessage + "')");
                                        return arrUserParams;
                                    }
                                    else
                                    {
                                        // objODBC.executeNonQuery("INSERT INTO `temp`( `data`, `type`, `request`) VALUES('4','3','Invalid Details')");
                                        arrUserParams.Add("FALSE");
                                        arrUserParams.Add("Invalid Details");
                                        arrUserParams.Add("NA");
                                        arrUserParams.Add("NA");
                                        return arrUserParams;
                                    }

                                }
                                catch (Exception)
                                {

                                }
                                return arrUserParams;
                            }
                            else if (intOperatorID == 3)
                            {
                                try
                                {
                                    //int intOperatorIDRechargeAPI = objNewRecharge.GetOperatorIDForRechargeAPI(intOperatorID);

                                    strExiomsOpName = "Roshan";

                                    arrUserParams.Add("FALSE");
                                    arrUserParams.Add("You can not do Reverse Topup for this Operater");
                                    arrUserParams.Add("NA");
                                    arrUserParams.Add("NA");
                                    return arrUserParams;
                                }
                                catch (Exception)
                                {

                                }
                            }
                            else if (intOperatorID == 4)
                            {
                                strExiomsOpName = "Easy Pay";


                                arrUserParams.Add("FALSE");
                                arrUserParams.Add("You can not do Reverse Topup for this Operater");
                                arrUserParams.Add("NA");
                                arrUserParams.Add("NA");
                                return arrUserParams;


                            }
                            else
                            {
                                arrUserParams.Add("FALSE");

                                arrUserParams.Add("Invalid Operater ID");
                                arrUserParams.Add("NA");
                                arrUserParams.Add("NA");
                                return arrUserParams;
                            }
                        }
                        else
                        {
                            arrUserParams.Add("FALSE");

                            arrUserParams.Add(strResponseMessage);
                            arrUserParams.Add("NA");
                            arrUserParams.Add("NA");
                            return arrUserParams;
                        }
                    }
                    else
                    {

                        arrUserParams.Add("FALSE");

                        arrUserParams.Add("Amount should be greater then 0");
                        arrUserParams.Add("NA");
                        arrUserParams.Add("NA");
                        return arrUserParams;
                    }
                }
                catch (Exception e1)
                {

                    strMessage = "Exception / Error : " + e1.ToString();
                    arrUserParams.Add("FALSE");
                    arrUserParams.Add(strMessage);
                    arrUserParams.Add("NA");
                    arrUserParams.Add("NA");
                    return arrUserParams;
                }

            }
            catch (Exception e1)
            {

                strMessage = "FAILED,Exception,NA,NA";
                arrUserParams.Add(strMessage);
                arrUserParams.Add("NA");
                arrUserParams.Add("NA");
                return arrUserParams;
            }
            return arrUserParams;
        }




        [WebMethod]
        public ArrayList GetPendingRechargeDetailsStatus(int intUserID, string strStrTransactionID, string strIPAddress, int intDesignationType, string strMacAddress, string strOSDetails, string strIMEINo, string strGcmID, string strAPPVersion, int intSource, int intCreatedByType)
        {
            DataSet ds = new DataSet();
            ArrayList arrayList = new ArrayList();
            try
            {
                try
                {
                    objCreateFile.CreateTextFile(strIPAddress, strMacAddress, strOSDetails, strIMEINo, strGcmID, strAPPVersion, intSource.ToString(), intCreatedByType, 885, " Get Pending Recharge Details Status,TransactionID:" + strStrTransactionID, intUserID.ToString());
                }
                catch (Exception)
                {

                }

                ds = objODBC.getDataSet("Select id,userid,trans_number,operator_id,operator_name,deduct_amt,api_type,Operater_table_id,amount,mobile_number from er_recharge where status=1 and id='" + strStrTransactionID + "' order by id Desc limit 1;");
                int intCount = Convert.ToInt32((ds.Tables[0].Rows.Count));
                if (intCount > 0)
                {
                    int i = 0;
                    int intInvestmentID = int.Parse(ds.Tables[0].Rows[i]["id"].ToString());
                    intUserID = int.Parse(ds.Tables[0].Rows[i]["userid"].ToString());
                    string strTransactionNumber = ds.Tables[0].Rows[i]["trans_number"].ToString();
                    string strOperaterID = ds.Tables[0].Rows[i]["operator_id"].ToString();
                    string strOperaterName = ds.Tables[0].Rows[i]["operator_name"].ToString();
                    double dblDeductAmt = double.Parse(ds.Tables[0].Rows[i]["deduct_amt"].ToString());
                    string api_type = ds.Tables[0].Rows[i]["api_type"].ToString();
                    string strOperaterTableID = ds.Tables[0].Rows[i]["Operater_table_id"].ToString();
                    double dblActualAmt = double.Parse(ds.Tables[0].Rows[i]["amount"].ToString());
                    string strMobile = ds.Tables[0].Rows[i]["mobile_number"].ToString();


                    if (api_type == "1") // Salaam
                    {
                        arrayList.Add("FALSE");
                        arrayList.Add("Sorry, Services is not Available");
                    }
                    else if (api_type == "2") // Etisalat
                    {

                        string strQ = "SELECT Transaction_Response FROM etisalat_topup_request WHERE recharge_id =" + intInvestmentID + " order by id desc limit 1";

                        string EtisalatResponse = objODBC.executeScalar_str(strQ);


                        if (EtisalatResponse.ToString() != "NA")
                        {
                            if (Regex.IsMatch(EtisalatResponse, string.Format(@"\b{0}\b", Regex.Escape("ACNT:RETN=0000,DESC=SUCCESS"))))
                            {
                                string strResposne = TruncateAtWord(EtisalatResponse, 64); // Truncate From Position of Words
                                string output = EtisalatResponse.Replace(strResposne, ""); // Replace Words From Data
                                output = output.Remove(output.Length - 8); // Deduct Words from Last                   
                                string StrCheckSum = EtisalatResponse.Substring(EtisalatResponse.Length - 8); // Getting Checksum

                                string[] strDescriptionResponse = output.Split(new char[] { ',' });
                                // strMessage = "2,OOPs! You do not have sufficient balance.,Failed,NA,NA";

                                string TRDStatus = strDescriptionResponse[0];
                                string TRDDescription = strDescriptionResponse[1]; ;
                                string BEFOROPER = strDescriptionResponse[2];
                                string FEE = strDescriptionResponse[3];
                                string AFTEROPER = strDescriptionResponse[4];
                                string CSVSTOP = strDescriptionResponse[5];

                                string TXNID = objODBC.executeScalar_str("SELECT transaction_ID_recharge FROM etisalat_topup_request WHERE id='" + strOperaterTableID + "'").ToString();
                                try
                                {
                                    objODBC.executeNonQuery("call `updateResponseV3`('" + TXNID + "','" + intInvestmentID + "',1)");
                                }
                                catch (Exception)
                                {

                                }
                                arrayList.Add("TRUE");
                                arrayList.Add("Top Up Transaction is Successfull ! ");

                                try
                                {
                                    string strBalance = objODBC.executeScalar_str("SELECT amt FROM `er_sys_wallet` where id=2;");
                                    objODBC.executeNonQuery("update er_recharge set comment='" + EtisalatResponse + "',operator_balance='" + strBalance + "' where id='" + intInvestmentID + "'");

                                    objODBC.executeNonQuery("update etisalat_topup_request set status=1 WHERE recharge_id =" + intInvestmentID + "");
                                }
                                catch (Exception)
                                {

                                }

                                try
                                {
                                    objODBC.executeNonQuery("update etisalat_topup_request set transaction_DESC='" + TRDDescription + "',Transactio_Status='" + TRDStatus + "',BEFOROPER='" + BEFOROPER + "',FEE='" + FEE + "',AFTEROPER='" + AFTEROPER + "','" + CSVSTOP + "',Trans_Receive_Checksum='" + StrCheckSum + "',status=1 WHERE recharge_id =" + intInvestmentID + "");
                                }
                                catch (Exception)
                                {

                                }


                                try
                                {
                                    Recharge objRecharge = new Recharge();
                                    objRecharge.SendSMPPSMS(intUserID, objODBC.executeScalar_str("select username FROM `er_login` where userid='" + intUserID + "'"), strMobile, strOperaterName, int.Parse(strOperaterID), dblActualAmt, 1, strTransactionNumber.ToString(), "0");
                                }
                                catch (Exception)
                                {

                                }
                            }
                            else
                            {
                                string strResposne = TruncateAtWord(EtisalatResponse, 64); // Truncate From Position of Words
                                string output = EtisalatResponse.Replace(strResposne, ""); // Replace Words From Data
                                output = output.Remove(output.Length - 8); // Deduct Words from Last                   
                                string StrCheckSum = EtisalatResponse.Substring(EtisalatResponse.Length - 8); // Getting Checksum

                                string[] strDescriptionResponse = output.Split(new char[] { ',' });
                                // strMessage = "2,OOPs! You do not have sufficient balance.,Failed,NA,NA";

                                string TRDStatus = strDescriptionResponse[0];
                                string TRDDescription = strDescriptionResponse[1];


                                string TXNID = objODBC.executeScalar_str("SELECT transaction_ID_recharge FROM etisalat_topup_request WHERE id='" + strOperaterTableID + "'").ToString();
                                try
                                {
                                    objODBC.executeNonQuery("call `updateResponseV3`('" + TXNID + "','" + intInvestmentID + "',2)");
                                }
                                catch (Exception)
                                {

                                }

                                arrayList.Add("FALSE");
                                arrayList.Add(" Top Up Transaction is FAILED !..");

                                try
                                {
                                    string strBalance = objODBC.executeScalar_str("SELECT amt FROM `er_sys_wallet` where id=2;");
                                    objODBC.executeNonQuery("update er_recharge set comment='" + EtisalatResponse + "',operator_balance='" + strBalance + "' where id='" + intInvestmentID + "'");
                                    objODBC.executeNonQuery("update etisalat_topup_request set status=2 WHERE recharge_id =" + intInvestmentID + "");
                                }
                                catch (Exception)
                                {

                                }

                                try
                                {
                                    objODBC.executeNonQuery("update etisalat_topup_request set transaction_DESC='" + TRDDescription + "',Transactio_Status='" + TRDStatus + "',Trans_Receive_Checksum='" + StrCheckSum + "',status=2 WHERE recharge_id =" + intInvestmentID + "");
                                }
                                catch (Exception)
                                {

                                }

                                try
                                {
                                    Recharge objRecharge = new Recharge();
                                    objRecharge.SendSMPPSMS(intUserID, objODBC.executeScalar_str("select username FROM `er_login` where userid='" + intUserID + "'"), strMobile, strOperaterName, int.Parse(strOperaterID), dblActualAmt, 2, strTransactionNumber.ToString(), "0");
                                }
                                catch (Exception)
                                {

                                }
                            }
                        }
                        else
                        {

                        }
                    }
                    else if (api_type == "3") // Roshan
                    {
                        arrayList.Add("FALSE");
                        arrayList.Add("Sorry, Services is not Available");
                    }
                    else if (api_type == "4") // Easy pay
                    {

                        //<? xml version = "1.0" encoding = "utf-8" ?>
                        //< request >
                        //< request - type > 2 </ request - type >
                        //< terminal - id > 347922 </ terminal - id >
                        //< login > mutahedapi </ login >
                        //< password - md5 > 9b3ee0226cd2556f96eafbbdb1e5f398 </ password - md5 >
                        //< payment >
                        //< payid > 23577618 </ payid >
                        //</ payment >
                        //</ request >
                        string strPayID = "NA";
                        try
                        {
                            strPayID = objODBC.executeScalar_str("Select payid_responseid from er_easy_pay_request where id='" + strOperaterTableID + "'");
                        }
                        catch (Exception)
                        {

                        }
                        if (strPayID != "NA")
                        {
                            EasyPay objEasypay = new EasyPay();
                            Encryption objEncrypMD5SHA1 = new Encryption();
                            string strPassword = "set2018";
                            string strTerminalID = "347922";
                            string strLoginID = "mutahedapi";
                            string MD5Password = objEncrypMD5SHA1.MD5Hash(strPassword);

                            var EasyPayResponse = objEasypay.ESPY_Transaction_Pending(int.Parse(strOperaterTableID), intInvestmentID, strTerminalID, MD5Password, "2", strLoginID, strPayID);


                            //   objODBC.executeNonQuery("update er_recharge set comment='" + EasyPayResponse.ToString() + "' where id='" + intInvestmentID + "'");
                            XmlDocument xmlDoc = new XmlDocument();
                            xmlDoc.LoadXml(EasyPayResponse);
                            // objODBC.executeNonQuery(" insert into temp(`data`,`type`,`created_on`,`request`) values('1',1,'2019-01-29 19:58:58','5')");
                            XmlNode paymentItem = xmlDoc.DocumentElement.ChildNodes[0];

                            string strStatusCode = paymentItem.Attributes["status-code"].Value.ToString();
                            string strStatuTxnDate = paymentItem.Attributes["txn-date"].Value.ToString();
                            string strPayStatus = "NA";



                            try
                            {
                                strPayStatus = paymentItem.Attributes["payment-state"].Value;
                                strPayID = paymentItem.Attributes["payid"].Value.ToString();
                            }
                            catch (Exception)
                            {

                            }
                            if (strPayStatus == "1") // Success
                            {
                                try
                                {
                                    objODBC.executeNonQuery("call `updateResponseV3`('" + strPayID + "','" + intInvestmentID + "',1)");
                                }
                                catch (Exception)
                                {

                                }
                                //    objODBC.executeNonQuery(" insert into temp(`data`,`type`,`created_on`,`request`) values(" + strPayID + "," + intInvestmentID + ",'2019-01-29 19:58:58','5')");
                                arrayList.Add("TRUE");
                                arrayList.Add("Top Up Transaction is Successfull ! ");


                                objODBC.executeNonQuery("update er_recharge set comment='" + EasyPayResponse + "',ap_transid='" + strPayID + "',operator_transid='" + strPayID + "' where id='" + intInvestmentID + "'");



                                string strStatusDescription = "N/A";
                                try
                                {
                                    strStatusDescription = paymentItem.Attributes["payid"].Value;
                                }
                                catch (Exception)
                                {

                                }

                                string strRequestType_P = "N/A";
                                string strTerminalIDRes_P = "N/A";
                                double strCurrencyRate_P = 0;
                                double strBalance_P = 0;
                                double strOverdraft_P = 0;
                                double strWithheld_P = 0;
                                double strPayable_P = 0;

                                try
                                {
                                    strRequestType_P = xmlDoc.DocumentElement.ChildNodes[1].ChildNodes[0].Value;
                                    strTerminalIDRes_P = xmlDoc.DocumentElement.ChildNodes[2].ChildNodes[0].Value;
                                    strCurrencyRate_P = double.Parse(xmlDoc.DocumentElement.ChildNodes[3].ChildNodes[0].Value);
                                    strBalance_P = double.Parse(xmlDoc.DocumentElement.ChildNodes[4].ChildNodes[0].Value);
                                    strOverdraft_P = double.Parse(xmlDoc.DocumentElement.ChildNodes[5].ChildNodes[0].Value);
                                    strWithheld_P = double.Parse(xmlDoc.DocumentElement.ChildNodes[6].ChildNodes[0].Value);
                                    strPayable_P = double.Parse(xmlDoc.DocumentElement.ChildNodes[7].ChildNodes[0].Value);
                                }
                                catch (Exception)
                                {

                                }

                                try
                                {
                                    objODBC.executeNonQuery("update er_recharge set operator_balance='" + strBalance_P + "' where id='" + intInvestmentID + "'");
                                }
                                catch (Exception)
                                {

                                }
                                try
                                {
                                    objODBC.executeNonQuery("UPDATE `er_easy_pay_request` set `terminal_id`='" + strTerminalIDRes_P + "', `request_type`='" + strRequestType_P + "', `net_amount`=0, `payid_responseid`='" + strPayID + "', `payment_state`='" + strPayStatus + "', `status_code`='" + strStatusCode + "', `status_desc`='', `txn_date`='" + strStatuTxnDate + "',`currency_rate`='" + strCurrencyRate_P + "', `balance`='" + strBalance_P + "', `overdraft`='" + strOverdraft_P + "', `withheld`='" + strWithheld_P + "', `payable`='" + strPayable_P + "', `Response`='" + EasyPayResponse + "',`status`=1 where id='" + strOperaterTableID + "'");
                                    // Fun SMS
                                }
                                catch
                                {

                                }

                                try
                                {
                                    Recharge objRecharge = new Recharge();
                                    objRecharge.SendSMPPSMS(intUserID, objODBC.executeScalar_str("select username FROM `er_login` where userid='" + intUserID + "'"), strMobile, strOperaterName, int.Parse(strOperaterID), dblActualAmt, 1, strTransactionNumber.ToString(), "0");
                                }
                                catch (Exception)
                                {

                                }

                            }

                            else if (strPayStatus == "0")  // Pending
                            {

                                arrayList.Add("FALSE");
                                arrayList.Add(" Top Up Transaction is in Pending !..");

                            }
                            else if (strPayStatus == "2") // Failed
                            {
                                try
                                {
                                    objODBC.executeNonQuery("call `updateResponseV3`('" + strPayID + "','" + intInvestmentID + "',2)");
                                }
                                catch (Exception)
                                {

                                }

                                arrayList.Add("FALSE");
                                arrayList.Add(" Top Up Transaction is FAILED !..");

                                string strRequestType_F = "N/A";
                                string strTerminalIDRes_F = "N/A";
                                string strDiscription_F = "N/A";
                                try
                                {
                                    strRequestType_F = xmlDoc.DocumentElement.ChildNodes[1].ChildNodes[0].Value.ToString();
                                    strTerminalIDRes_F = xmlDoc.DocumentElement.ChildNodes[2].ChildNodes[0].Value.ToString();
                                    strDiscription_F = paymentItem.Attributes["status-desc"].Value.ToString();
                                }
                                catch (Exception)
                                {

                                }
                                try
                                {
                                    objODBC.executeNonQuery("UPDATE `er_easy_Pay_request` set `payid_responseid`='" + strPayID + "',`terminal_id`='" + strTerminalIDRes_F + "', `request_type`='" + strRequestType_F + "', `net_amount`=0,`payment_state`='" + strPayStatus + "', `status_code`='" + strStatusCode + "', `status_desc`='" + strDiscription_F + "', `txn_date`='" + strStatuTxnDate + "',`Response`='" + EasyPayResponse + "',`status`=2 where id='" + strOperaterTableID + "'");
                                }
                                catch (Exception)
                                {
                                }

                                try
                                {
                                    Recharge objRecharge = new Recharge();
                                    objRecharge.SendSMPPSMS(intUserID, objODBC.executeScalar_str("select username FROM `er_login` where userid='" + intUserID + "'"), strMobile, strOperaterName, int.Parse(strOperaterID), dblActualAmt, 2, strTransactionNumber.ToString(), "0");
                                }
                                catch (Exception)
                                {

                                }

                            }
                            else
                            {
                                try
                                {
                                    objODBC.executeNonQuery("call `updateResponseV3`('" + strPayID + "','" + intInvestmentID + "',2)");
                                }
                                catch (Exception)
                                {

                                }

                                arrayList.Add("FALSE");
                                arrayList.Add(" Top Up Transaction is FAILED !..");

                                objODBC.executeNonQuery("update er_recharge set ap_transid='" + strPayID + "',operator_transid='" + strPayID + "' where id='" + intInvestmentID + "'");

                                string strRequestType_F = "NA";
                                string strTerminalIDRes_F = "NA";
                                string strCurrencyRate_F = "NA";
                                string strBalance_F = "NA";
                                string strOverdraft_F = "NA";
                                string strWithheld_F = "NA";
                                string strPayable_F = "NA";
                                try
                                {
                                    strRequestType_F = xmlDoc.DocumentElement.ChildNodes[1].ChildNodes[0].Value.ToString();
                                    strTerminalIDRes_F = xmlDoc.DocumentElement.ChildNodes[2].ChildNodes[0].Value.ToString();
                                    strCurrencyRate_F = xmlDoc.DocumentElement.ChildNodes[3].ChildNodes[0].Value.ToString();
                                    strBalance_F = xmlDoc.DocumentElement.ChildNodes[4].ChildNodes[0].Value.ToString();
                                    strOverdraft_F = xmlDoc.DocumentElement.ChildNodes[5].ChildNodes[0].Value.ToString();
                                    strWithheld_F = xmlDoc.DocumentElement.ChildNodes[6].ChildNodes[0].Value.ToString();
                                    strPayable_F = xmlDoc.DocumentElement.ChildNodes[7].ChildNodes[0].Value.ToString();
                                }
                                catch (Exception)
                                {

                                }

                                try
                                {
                                    objODBC.executeNonQuery("update er_recharge set operator_balance='" + strBalance_F + "' where id='" + intInvestmentID + "'");
                                }
                                catch (Exception)
                                {

                                }
                                try
                                {
                                    objODBC.executeNonQuery("UPDATE `er_easy_Pay_request` set `terminal_id`='" + strTerminalIDRes_F + "', `request_type`='" + strRequestType_F + "', `net_amount`=0, `payid_responseid`='" + strPayID + "', `payment_state`='" + strPayStatus + "', `status_code`='" + strStatusCode + "', `status_desc`='', `txn_date`='" + strStatuTxnDate + "',`currency_rate`='" + strCurrencyRate_F + "', `balance`='" + strBalance_F + "', `overdraft`='" + strOverdraft_F + "', `withheld`='" + strWithheld_F + "', `payable`='" + strPayable_F + "', `Response`='" + EasyPayResponse + "',`status`=1 where id='" + strOperaterTableID + "'");
                                }
                                catch (Exception)
                                {

                                }
                                try
                                {
                                    Recharge objRecharge = new Recharge();
                                    objRecharge.SendSMPPSMS(intUserID, objODBC.executeScalar_str("select username FROM `er_login` where userid='" + intUserID + "'"), strMobile, strOperaterName, int.Parse(strOperaterID), dblActualAmt, 2, strTransactionNumber.ToString(), "0");
                                }
                                catch (Exception)
                                {

                                }


                            }
                        }

                    }
                }

                else
                {
                    arrayList.Add("FALSE");
                    arrayList.Add("Invalid Transaction ID");
                }
            }
            catch (Exception)
            {
                arrayList.Add("FALSE");
                arrayList.Add(" Top Up Transaction is in Pending !..");
            }
            finally
            {
                ds.Dispose();
            }
            return arrayList;
        }



        [WebMethod]
        public ArrayList GetApproveRejectByAdmin(int intUserID, string strStrTransactionID, string strOperatorTransactionID, int intType, string strIPAddress, int intDesignationType, string strMacAddress, string strOSDetails, string strIMEINo, string strGcmID, string strAPPVersion, int intSource, int intCreatedByType)
        {
            DataSet ds = new DataSet();
            ArrayList arrayList = new ArrayList();
            string strOperaterID = "0";
            try
            {
                objCreateFile.CreateTextFile(strIPAddress, strMacAddress, strOSDetails, strIMEINo, strGcmID, strAPPVersion, intSource.ToString(), intCreatedByType, 586, " Get Approve Reject By  Admin Topup Pending Report", intUserID.ToString());
            }
            catch (Exception)
            {

            }

            ds = objODBC.getDataSet("Select id,userid,trans_number,operator_id,operator_name,deduct_amt,api_type,operator_transid,amount,mobile_number from er_recharge where status=1 and id='" + strStrTransactionID + "' order by id Desc limit 1;");
            int intCount = Convert.ToInt32((ds.Tables[0].Rows.Count));
            if (intCount > 0)
            {
                int intInvestmentID = int.Parse(ds.Tables[0].Rows[0]["id"].ToString());
                intUserID = int.Parse(ds.Tables[0].Rows[0]["userid"].ToString());
                string strTransactionNumber = ds.Tables[0].Rows[0]["trans_number"].ToString();
                string strOperatorTransactionTID = ds.Tables[0].Rows[0]["operator_transid"].ToString();
                if (strOperatorTransactionTID == "")
                {
                    strOperatorTransactionTID = intInvestmentID.ToString();
                }
                strOperaterID = ds.Tables[0].Rows[0]["operator_id"].ToString();
                string strOperaterName = ds.Tables[0].Rows[0]["operator_name"].ToString();
                double dblDeductAmt = double.Parse(ds.Tables[0].Rows[0]["deduct_amt"].ToString());
                string api_type = ds.Tables[0].Rows[0]["api_type"].ToString();

                double dblActualAmt = double.Parse(ds.Tables[0].Rows[0]["amount"].ToString());
                string strMobile = ds.Tables[0].Rows[0]["mobile_number"].ToString();

                if (strMobile.Length == 11)
                {
                    strMobile = strMobile.Substring(2).ToString();
                }
                else if (strMobile.Length == 10)
                {
                    strMobile = strMobile.Substring(1);

                }

                if (intType == 1)
                {
                    try
                    {
                        objODBC.executeNonQuery("call `updateResponseV3`('" + strOperatorTransactionTID + "','" + intInvestmentID + "',1)");

                    }
                    catch (Exception)
                    {

                    }

                    arrayList.Add("TRUE");
                    arrayList.Add("Request Approved for Top up Transaction Successfully !");
                    try
                    {
                        if (strOperaterID == "3" || strOperaterID == "2")
                        {
                            SMPP_SMS_Format objSMPP_SMS_Format = new SMPP_SMS_Format();
                            CommonFunction objCOMFUN = new CommonFunction();
                            string strGetSMS = objSMPP_SMS_Format.TopupSuccessEndUser(objODBC.executeScalar_str("SELECT full_name From er_login Where userid='" + intUserID + "'").ToString(), strMobile, dblActualAmt, intInvestmentID.ToString());
                            objCOMFUN.er_SMS_Alert(intUserID, 2, strMobile, int.Parse(strOperaterID), strGetSMS, 1, "");
                        }
                    }
                    catch (Exception)
                    {

                    }
                }
                else if (intType == 2)
                {
                    try
                    {
                        objODBC.executeNonQuery("call `updateResponseV3`('" + strOperatorTransactionTID + "','" + intInvestmentID + "',2)");
                    }
                    catch (Exception)
                    {

                    }
                    arrayList.Add("TRUE");
                    arrayList.Add("Request Reject for Top up Transaction !");
                    try
                    {
                        //if (strOperaterID == "3" || strOperaterID == "2")
                        //{
                        //    SMPP_SMS_Format objSMPP_SMS_Format = new SMPP_SMS_Format();
                        //    CommonFunction objCOMFUN = new CommonFunction();
                        //    string strGetSMS = objSMPP_SMS_Format.TopupFailedEndUser(objODBC.executeScalar_str("SELECT full_name From er_login Where userid='" + intUserID + "'").ToString(), strMobile, dblActualAmt, intInvestmentID.ToString());
                        //    objCOMFUN.er_SMS_Alert(intUserID, 2, strMobile, int.Parse(strOperaterID), strGetSMS, 1, "");
                        //}
                    }
                    catch (Exception)
                    {

                    }
                }
                else
                {
                    arrayList.Add("FALSE");
                    arrayList.Add("Invalid Transaction Type");
                }


            }
            else
            {
                arrayList.Add("FALSE");
                arrayList.Add("Invalid Topup Transaction ID");
            }
            return arrayList;
        }



        // Activity Type=667
        [WebMethod]
        public ArrayList GetOperaterTypeRecharge(string strUserName, int intUserID, string strIPAddress, int intDesignationType, string strMacAddress, string strOSDetails, string strIMEINo, string strGcmID, string strAPPVersion, int intSource, int intCreatedByType)
        {
            DataSet ds = new DataSet();
            DataTable dt = new DataTable();
            ArrayList arrUserParams = new ArrayList();
            try
            {
                if ((intUserID > 0))
                {
                    try
                    {
                        objCreateFile.CreateTextFile(strIPAddress, strMacAddress, strOSDetails, strIMEINo, strGcmID, strAPPVersion, intSource.ToString(), intCreatedByType, 39, "Get Operater Type", strUserName);
                       
                    }
                    catch (Exception)
                    {

                        
                    }
                   
                    dt = objODBC.getDataTable("SELECT salaam_status,etisalat_status, roshan_status,mtn_status,awcc_status from er_login  where userid='" + intUserID + "' order by userid desc limit 1");
                    if (dt.Rows.Count > 0)
                    {

                      
                        int intSalaamStatus = int.Parse(dt.Rows[0]["salaam_status"].ToString());
                        int intEtisalatStatus = int.Parse(dt.Rows[0]["etisalat_status"].ToString());
                        int intRoshanStatus = int.Parse(dt.Rows[0]["roshan_status"].ToString());
                        int intMTNStatus = int.Parse(dt.Rows[0]["mtn_status"].ToString());
                        int intAwccStatus = int.Parse(dt.Rows[0]["awcc_status"].ToString());

                        try
                        {


                            arrUserParams.Add("TRUE");

                            if (intSalaamStatus == 1)
                            {
                                arrUserParams.Add("1");
                                arrUserParams.Add("Salaam");
                            }
                            if(intEtisalatStatus==1)
                            {
                                arrUserParams.Add("2");
                                arrUserParams.Add("Etisalat");
                            }
                            if (intEtisalatStatus == 1)
                            {
                                arrUserParams.Add("3");
                                arrUserParams.Add("Roshan");
                            }
                            if (intEtisalatStatus == 1)
                            {
                                arrUserParams.Add("4");
                                arrUserParams.Add("MTN");
                            }
                           if (intEtisalatStatus == 1)
                            {
                                arrUserParams.Add("5");
                                arrUserParams.Add("AWCC");
                            }


                            return arrUserParams;

                        }
                        catch (Exception ex)
                        {
                            arrUserParams.Add("False");
                            return arrUserParams;
                        }
                        finally
                        {
                            ds.Dispose();
                        }
                    }
                    else
                    {
                        arrUserParams.Add("False");
                        return arrUserParams;
                    }
                }
                else
                {
                    arrUserParams.Add("False");
                    return arrUserParams;
                }
            }

            catch (Exception ex)
            {
                arrUserParams.Add("False");
                return arrUserParams;
            }
            finally
            {
                ds.Dispose();
            }
        }
    }
}
