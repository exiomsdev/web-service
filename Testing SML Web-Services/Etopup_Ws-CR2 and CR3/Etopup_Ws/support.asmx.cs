﻿using Etopup_Ws.old_App_Code;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Services;

namespace Etopup_Ws
{
    /// <summary>
    /// Summary description for support
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class support : System.Web.Services.WebService
    {
        DataSet ds = new DataSet();
        string strQuery = string.Empty;
        string strLimit = string.Empty;
        ODBC objODBC = new ODBC();
        ODBC_LOG objODBCLOG = new ODBC_LOG();
        DataTable dt = new DataTable();
        DataTable dt2 = new DataTable();
        DateFormat objDT = new DateFormat();
        Encrypt_Decrypt ED = new Encrypt_Decrypt();
        CreateFile objCreateFile = new CreateFile();
        CommonFunction objComFun = new CommonFunction();


        [WebMethod]
        public string ico_Raised_Ticket(int intUserID, int intCategory, string strPreority, string strSub, string strMsg, string strBase64Image)
        {
            string result = string.Empty;        

            try
            {
                string strNewSub = string.Empty;
                if (strSub.Contains("'"))
                {
                    strNewSub = strSub.Replace("'", "@");
                }
                else
                {
                    strNewSub = strSub;
                }

                string strNewMsg = string.Empty;
                if (strMsg.Contains("'"))
                {
                    strNewMsg = strMsg.Replace("'", "@");
                }
                else
                {
                    strNewMsg = strMsg;
                }
                string strTransacctionNumber = objDT.getCurDateTime_Dayanamic_format("yyyymmdd").ToString();

                string strGenerateRandumName = objComFun.GenrateRandomString() + strTransacctionNumber.ToString() + intUserID.ToString();
                byte[] imageBytes;
                string imgURl = "";
                try
                {
                    if (strBase64Image != null && strBase64Image != "" && strBase64Image != "0")
                    {
                        strGenerateRandumName= strGenerateRandumName + ".png";
                        imageBytes = Convert.FromBase64String(strBase64Image);
                        MemoryStream ms = new MemoryStream(imageBytes, 0, imageBytes.Length);
                        ms.Write(imageBytes, 0, imageBytes.Length);
                        System.Drawing.Image image = System.Drawing.Image.FromStream(ms, true);
                        image.Save(Server.MapPath("~/Support/" + strGenerateRandumName));
                        imgURl = "http://topup.setaraganmutahed.com/Support/" + strGenerateRandumName;
                    }
                    else
                    {
                        strGenerateRandumName = "0";
                    }
                }
                catch (Exception)
                {
                    strGenerateRandumName = strBase64Image; // bysneha
                }
                strQuery = "call ico_Ticket_Raised(" + intUserID + "," + intCategory + ",'" + strPreority + "','" + strNewSub + "','" + strNewMsg + "','" + strGenerateRandumName + "')";
                objODBC.executeNonQuery(strQuery);
                objComFun.er_insert_notification(1, "Ticket Raised Request", strNewMsg, 1);
                result = "Ticket raised successfully";
            }
            catch (Exception e)
            {

            }
            return result;
        }

        [WebMethod]
        public DataSet ico_Support_Category(int intusertype)
        {
            string result = string.Empty;
            try
            {
                string strQuery = "call ico_Get_Support_Category(" + intusertype + ") ";
                ds = objODBC.getDataSet(strQuery);

            }
            catch (Exception e)
            {


            }
            return ds;
        }

        [WebMethod]
        public string ico_Support_Category_operation(string strCat, int intAction)
        {
            string result = string.Empty;
            try
            {
                if (intAction == 1 || intAction == 0) // delecte
                {
                    string strQuery = "update ico_support_setting set status = " + intAction + " where id = '" + strCat + "'";
                    objODBC.executeNonQuery(strQuery);
                    result = "Status update successfully";
                }
                else if (intAction == 2) //insert
                {
                    int intcount = objODBC.executeScalar_int("select count(1) from  `ico_support_setting` where categoryy = '" + strCat + "'");
                    if (intcount == 0)
                    {
                        string strQuery = "INSERT INTO `ico_support_setting`(categoryy) values('" + strCat + "')";

                        objODBC.executeNonQuery(strQuery);
                        result = "Ticket Category added successfully";
                    }
                    else
                    {
                        result = "category already exists";
                    }
                }
                else if (intAction > 2)//edit
                {
                    string strQuery = "update  `ico_support_setting` set categoryy = '" + strCat + "' where id =" + intAction + "";

                    objODBC.executeNonQuery(strQuery);
                    result = "operation successfull";
                }

            }
            catch (Exception e)
            {


            }
            return result;
        }
        [WebMethod]
        public DataSet ico_Report_Tickets(int intUserID, int intUserType, string strSearch, string strOrderby) // when use by admin intUserID = 0 else intUserID = user id;
        {

            try
            {
                string strQuery = "call ico_Get_Tickets(" + intUserID + "," + intUserType + ",'" + strSearch + "','" + strOrderby + "')";
                ds = objODBC.Report(strQuery);
            }
            catch (Exception e)
            {


            }


            return ds;
        }

        [WebMethod]
        public DataSet ico_Ticket_Conversation(int intUserType, int intUserID, string strTcktNo, int intResolvedStatus, int intButtonValue, string strMsg, string strImg) // when use by admin intUserID = 0 else intUserID = user id;
        {
            string result = string.Empty;
            try
            {

                string strNewMsg = string.Empty;
                if (strMsg.Contains("'"))
                {
                    strNewMsg = strMsg.Replace("'", "@");
                }
                else
                {
                    strNewMsg = strMsg;
                }
                Guid strGuid = Guid.NewGuid();
                string strGenerateRandumName = strTcktNo+strGuid.ToString();
                byte[] imageBytes;
                string imgURl = "";
                try
                {
                    if (strImg != null && strImg != "" && strImg != " " && strImg != "0")
                    {
                        strGenerateRandumName = strGenerateRandumName + ".png";
                        imageBytes = Convert.FromBase64String(strImg);
                        MemoryStream ms = new MemoryStream(imageBytes, 0, imageBytes.Length);
                        ms.Write(imageBytes, 0, imageBytes.Length);
                        System.Drawing.Image image = System.Drawing.Image.FromStream(ms, true);
                        image.Save(Server.MapPath("~/Support/" + strGenerateRandumName));
                        imgURl = "http://topup.setaraganmutahed.com/Support/" + strGenerateRandumName;
                    }
                    else
                    {
                        strGenerateRandumName = "0";
                    }
                }
                catch (Exception)
                {
                    strGenerateRandumName = "0"; // bysneha
                }

                string strQuery = "call ico_Support_Conversation(" + intUserType + "," + intUserID + ",'" + strTcktNo + "'," + intResolvedStatus + "," + intButtonValue + ",'" + strNewMsg + "','" + strGenerateRandumName + "')";
                ds = objODBC.getDataSet(strQuery);

                if (intButtonValue > 0)
                {
                    ds.Tables[0].Rows[0][0].ToString();
                }
            }
            catch (Exception e)
            {


            }


            return ds;
        }

        // ------------------------- android -----------------------------

        [WebMethod]
        public ArrayList er_android_Report_Tickets(int intUserID, int intUserType, string strSearch, string strOrderby)
        {
            ArrayList arlst = new ArrayList();
            ds = ico_Report_Tickets(intUserID, intUserType, strSearch, strOrderby);
            arlst = objComFun.Get_ArrayLlist_From_Dataset(ds);
            return arlst;
        }

        [WebMethod]
        public ArrayList er_android_Ticket_Conversation(int intUserType, int intUserID, string strTcktNo, int intResolvedStatus, int intButtonValue, string strMsg, string strImg)
        {
            ArrayList arlst = new ArrayList();
            ds = ico_Ticket_Conversation(intUserType, intUserID, strTcktNo, intResolvedStatus, intButtonValue, strMsg, strImg);
            arlst = objComFun.Get_ArrayLlist_From_Dataset(ds);
            return arlst;
        }
        [WebMethod]
        public ArrayList er_android_Support_Category(int intusertype)
        {
            ArrayList arlst = new ArrayList();
            ds = ico_Support_Category(intusertype);
            arlst = objComFun.Get_ArrayLlist_From_Dataset(ds);
            return arlst;

        }
    }
}
