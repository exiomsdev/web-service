﻿Imports Microsoft.VisualBasic
Imports System.Data
Namespace Etopup_Ws.old_App_Code
    Public Class ClsCountry
        Public Function get_countries() As DataSet
            Dim ds As New DataSet
            Try
                Dim dt As New DataTable

                dt.Columns.Add("country_id", GetType(Integer))
                dt.Columns.Add("country_name", GetType(String))

                dt.Rows.Add(0, "Select Country")
                dt.Rows.Add(1, "India")
                dt.Rows.Add(2, "Indonasia")
                dt.Rows.Add(3, "Bangladesh")
                dt.Rows.Add(4, "China")
                dt.Rows.Add(5, "Malaysia")
                dt.Rows.Add(6, "Philippines")
                dt.Rows.Add(7, "Thailand")
                dt.Rows.Add(8, "Afghanistan")
                dt.Rows.Add(9, "Albania")
                dt.Rows.Add(10, "Algeria")
                dt.Rows.Add(11, "Andorra")
                dt.Rows.Add(12, "Angola")
                dt.Rows.Add(13, "Antigua and Barbuda")
                dt.Rows.Add(14, "Armenia")
                dt.Rows.Add(15, "Australia")
                dt.Rows.Add(16, "Austria")
                dt.Rows.Add(17, "Azerbaijan")
                dt.Rows.Add(18, "Bahamas")
                dt.Rows.Add(19, "Bahrain")
                dt.Rows.Add(21, "Barbados")
                dt.Rows.Add(22, "Belarus")
                dt.Rows.Add(23, "Belgium")
                dt.Rows.Add(24, "Belize")
                dt.Rows.Add(25, "Benin")
                dt.Rows.Add(26, "Bhutan")
                dt.Rows.Add(27, "Bolivia")
                dt.Rows.Add(28, "Bosnia and Herzegovina")
                dt.Rows.Add(29, "Botswana")
                dt.Rows.Add(30, "Brazil")
                dt.Rows.Add(31, "Brunei")
                dt.Rows.Add(32, "Bulgaria")
                dt.Rows.Add(33, "Burkina Faso")
                dt.Rows.Add(34, "Burundi")
                dt.Rows.Add(35, "Cambodia")
                dt.Rows.Add(36, "Cameroon")
                dt.Rows.Add(37, "Canada")
                dt.Rows.Add(38, "United States")
                dt.Rows.Add(40, "Pakistan")
                dt.Rows.Add(41, "Nigeria")
                dt.Rows.Add(42, "Russia")
                dt.Rows.Add(43, "Japan")
                dt.Rows.Add(44, "Mexico")
                dt.Rows.Add(46, "Vietnam")
                dt.Rows.Add(47, "Ethiopia")
                dt.Rows.Add(48, "Egypt")
                dt.Rows.Add(49, "Germany")
                dt.Rows.Add(50, "Iran")
                dt.Rows.Add(51, "Turkey")
                dt.Rows.Add(52, "Democratic Republic of the Congo")
                ' dt.Rows.Add(53, "Thailand")
                dt.Rows.Add(54, "France")
                dt.Rows.Add(55, "United Kingdom")
                dt.Rows.Add(56, "Italy")
                dt.Rows.Add(57, "Myanmar")
                dt.Rows.Add(58, "South Africa")
                dt.Rows.Add(59, "South Korea")
                dt.Rows.Add(60, "Colombia")
                dt.Rows.Add(61, "Spain")
                dt.Rows.Add(62, "Ukraine")
                dt.Rows.Add(63, "Tanzania")
                dt.Rows.Add(64, "Kenya")
                dt.Rows.Add(65, "Argentina")
                dt.Rows.Add(66, "Poland")
                dt.Rows.Add(67, "Sudan")
                ' dt.Rows.Add(68, "Algeria")
                dt.Rows.Add(69, "Iraq")
                dt.Rows.Add(70, "Uganda")
                dt.Rows.Add(71, "Morocco")
                dt.Rows.Add(72, "Peru")
                ' dt.Rows.Add(73, "Malaysia")
                dt.Rows.Add(74, "Uzbekistan")
                dt.Rows.Add(75, "Saudi Arabia")
                dt.Rows.Add(76, "Venezuela")
                dt.Rows.Add(77, "Nepal")
                dt.Rows.Add(78, "North Korea")
                dt.Rows.Add(79, "Ghana")
                dt.Rows.Add(80, "Yemen")
                dt.Rows.Add(81, "Mozambique")
                dt.Rows.Add(82, "Taiwan")
                dt.Rows.Add(83, "Ivory Coast")
                dt.Rows.Add(84, "Syria")
                dt.Rows.Add(85, "Madagascar")
                dt.Rows.Add(86, "Romania")
                ' dt.Rows.Add(87, "Burkina Faso")
                dt.Rows.Add(88, "Niger")
                dt.Rows.Add(89, "Kazakhstan")
                dt.Rows.Add(90, "Netherlands")
                dt.Rows.Add(91, "Chile")
                dt.Rows.Add(92, "Zimbabwe")
                dt.Rows.Add(93, "South Sudan")
                dt.Rows.Add(94, "Greece")
                dt.Rows.Add(95, "Portugal")
                dt.Rows.Add(96, "Hungary")
                dt.Rows.Add(97, "Sweden")
                dt.Rows.Add(98, "Dominican Republic")
                'dt.Rows.Add(99, "Austria")
                dt.Rows.Add(100, "Honduras")
                dt.Rows.Add(101, "United Arab Emirates")
                dt.Rows.Add(102, "Switzerland")
                dt.Rows.Add(103, "Israel")
                dt.Rows.Add(104, "Tajikistan")
                'dt.Rows.Add(105, "Bulgaria")
                dt.Rows.Add(106, "Paraguay")
                dt.Rows.Add(107, "Jordan")
                dt.Rows.Add(108, "Eritrea")
                dt.Rows.Add(109, "Denmark")
                dt.Rows.Add(110, "Kyrgyzstan")
                dt.Rows.Add(111, "Finland")
                dt.Rows.Add(112, "Singapore")
                dt.Rows.Add(113, "Turkmenistan")
                dt.Rows.Add(114, "Norway")
                dt.Rows.Add(115, "Lebanon")
                dt.Rows.Add(117, "Ireland")
                dt.Rows.Add(118, "New Zealand")
                dt.Rows.Add(119, "uwait")
                dt.Rows.Add(120, "Panama")
                dt.Rows.Add(121, "Mongolia")
                dt.Rows.Add(122, "Jamaica")
                dt.Rows.Add(123, "Qatar")
                'dt.Rows.Add(124, "Bhutan")
                dt.Rows.Add(126, "Maldives")
                'dt.Rows.Add(127, "Barbados")
                dt.Rows.Add(128, "Togo")
                dt.Rows.Add(129, "Tunisia")
                dt.Rows.Add(130, "Gabon")
                dt.Rows.Add(131, "Oman")
                dt.Rows.Add(132, "Haiti")
                dt.Rows.Add(133, "Guadeloupe")
                dt.Rows.Add(134, "Martinique")
                dt.Rows.Add(135, "French Guiana")
                dt.Rows.Add(136, "La Réunion")
                dt.Rows.Add(137, "Slovenia")
                dt.Rows.Add(138, "Cyprus")
                ds.Tables.Add(dt)

                Return ds
            Catch ex As Exception

            Finally
                ds.Dispose()
            End Try
        End Function

        Public Function getCountryCode(ByVal strCountry As String) As Integer

            Dim intMobileCode As Integer = 0

            Select Case strCountry
                Case "India"
                    intMobileCode = 91

                Case "Indonasia"
                    intMobileCode = 62

                Case "Bangladesh"
                    intMobileCode = 880

                Case "China"
                    intMobileCode = 0

                Case "Malaysia"
                    intMobileCode = 60

                Case "Philippines"
                    intMobileCode = 63

                Case "Thailand"
                    intMobileCode = 66

                Case "Afghanistan"
                    intMobileCode = 93

                Case "Albania"
                    intMobileCode = 355

                Case "Algeria"
                    intMobileCode = 213

                Case "Andorra"
                    intMobileCode = 376

                Case "Angola"
                    intMobileCode = 244

                Case "Antigua and Barbuda"
                    intMobileCode = 1

                Case "Armenia"
                    intMobileCode = 374

                Case "Australia"
                    intMobileCode = 61

                Case "Austria"
                    intMobileCode = 43

                Case "Azerbaijan"
                    intMobileCode = 994

                Case "Bahamas, The"
                    intMobileCode = 1

                Case "Bahrain"
                    intMobileCode = 973

                Case "Barbados"
                    intMobileCode = 1

                Case "Belarus"
                    intMobileCode = 375

                Case "Belgium"
                    intMobileCode = 32

                Case "Belize"
                    intMobileCode = 501

                Case "Benin"
                    intMobileCode = 229

                Case "Bhutan"
                    intMobileCode = 975

                Case "Bolivia"
                    intMobileCode = 591

                Case "Bosnia and Herzegovina"
                    intMobileCode = 387

                Case "Botswana"
                    intMobileCode = 267

                Case "Brazil"
                    intMobileCode = 55

                Case "Brunei"
                    intMobileCode = 673

                Case "Bulgaria"
                    intMobileCode = 359

                Case "Burkina Faso"
                    intMobileCode = 226

                Case "Burundi"
                    intMobileCode = 257

                Case "Cambodia"
                    intMobileCode = 855

                Case "Cameroon"
                    intMobileCode = 237

                Case "Canada"
                    intMobileCode = 1

                Case "United States"
                    intMobileCode = 1

                Case "Pakistan"
                    intMobileCode = 92

                Case "Nigeria"
                    intMobileCode = 234

                Case "Russia"
                    intMobileCode = 7

                Case "Japan"
                    intMobileCode = 81

                Case "Mexico"
                    intMobileCode = 52

                Case "Vietnam"
                    intMobileCode = 84

                Case "Ethiopia"
                    intMobileCode = 251

                Case "Egypt"
                    intMobileCode = 20

                Case "Germany"
                    intMobileCode = 49

                Case "Iran"
                    intMobileCode = 98

                Case "Turkey"
                    intMobileCode = 90

                Case "Democratic Republic of the Congo"
                    intMobileCode = 243

                Case "Thailand"
                    intMobileCode = 66

                Case "France"
                    intMobileCode = 33

                Case "United Kingdom"
                    intMobileCode = 44

                Case "Italy"
                    intMobileCode = 39

                Case "Myanmar"
                    intMobileCode = 95

                Case "South Africa"
                    intMobileCode = 27

                Case "South Korea"
                    intMobileCode = 82

                Case "Colombia"
                    intMobileCode = 57

                Case "Spain"
                    intMobileCode = 34

                Case "Ukraine"
                    intMobileCode = 380

                Case "Tanzania"
                    intMobileCode = 255

                Case "Kenya"
                    intMobileCode = 254

                Case "Argentina"
                    intMobileCode = 54

                Case "Poland"
                    intMobileCode = 48

                Case "Sudan"
                    intMobileCode = 249

                Case "Algeria"
                    intMobileCode = 213

                Case "Iraq"
                    intMobileCode = 964

                Case "Uganda"
                    intMobileCode = 256

                Case "Morocco"
                    intMobileCode = 212

                Case "Peru"
                    intMobileCode = 51

                Case "Malaysia"
                    intMobileCode = 60

                Case "Uzbekistan"
                    intMobileCode = 998

                Case "Saudi Arabia"
                    intMobileCode = 966

                Case "Venezuela"
                    intMobileCode = 58

                Case "Nepal"
                    intMobileCode = 977

                Case "North Korea"
                    intMobileCode = 850

                Case "Ghana"
                    intMobileCode = 233

                Case "Yemen"
                    intMobileCode = 967

                Case "Mozambique"
                    intMobileCode = 258

                Case "Taiwan"
                    intMobileCode = 886

                Case "Ivory Coast"
                    intMobileCode = 225

                Case "Syria"
                    intMobileCode = 963

                Case "Madagascar"
                    intMobileCode = 261

                Case "Romania"
                    intMobileCode = 40

                Case "Burkina Faso"
                    intMobileCode = 226

                Case "Niger"
                    intMobileCode = 227

                Case "Kazakhstan"
                    intMobileCode = 7

                Case "Netherlands"
                    intMobileCode = 31

                Case "Chile"
                    intMobileCode = 56

                Case "Zimbabwe"
                    intMobileCode = 263

                Case "South Sudan"
                    intMobileCode = 211

                Case "Greece"
                    intMobileCode = 30

                Case "Portugal"
                    intMobileCode = 351

                Case "Hungary"
                    intMobileCode = 36

                Case "Sweden"
                    intMobileCode = 46

                Case "Dominican Republic"
                    intMobileCode = 1

                Case "Austria"
                    intMobileCode = 43

                Case "Honduras"
                    intMobileCode = 504

                Case "United Arab Emirates"
                    intMobileCode = 971

                Case "Switzerland"
                    intMobileCode = 268

                Case "Israel"
                    intMobileCode = 972

                Case "Tajikistan"
                    intMobileCode = 992

                Case "Bulgaria"
                    intMobileCode = 359

                Case "Paraguay"
                    intMobileCode = 595

                Case "Jordan"
                    intMobileCode = 962

                Case "Eritrea"
                    intMobileCode = 291

                Case "Denmark"
                    intMobileCode = 45

                Case "Kyrgyzstan"
                    intMobileCode = 996

                Case "Finland"
                    intMobileCode = 358

                Case "Singapore"
                    intMobileCode = 65

                Case "Turkmenistan"
                    intMobileCode = 993

                Case "Norway"
                    intMobileCode = 47

                Case "Lebanon"
                    intMobileCode = 961

                Case "Ireland"
                    intMobileCode = 353

                Case "New Zealand"
                    intMobileCode = 64

                Case "uwait"
                    intMobileCode = 0

                Case "Panama"
                    intMobileCode = 507

                Case "Mongolia"
                    intMobileCode = 0

                Case "Jamaica"
                    intMobileCode = 1

                Case "Qatar"
                    intMobileCode = 974

                Case "Bhutan"
                    intMobileCode = 975

                Case "Maldives"
                    intMobileCode = 960

                Case "Barbados"
                    intMobileCode = 1

                Case "Togo"
                    intMobileCode = 228

                Case "Tunisia"
                    intMobileCode = 216

                Case "Gabon"
                    intMobileCode = 241

                Case "Oman"
                    intMobileCode = 968

                Case "Haiti"
                    intMobileCode = 509

                Case "Guadeloupe"
                    intMobileCode = 590

                Case "Martinique"
                    intMobileCode = 596

                Case "French Guiana"
                    intMobileCode = 594

                Case "La Réunion"
                    intMobileCode = 262

                Case "Slovenia"
                    intMobileCode = 386

                Case "Cyprus"
                    intMobileCode = 357

                Case Else
                    intMobileCode = 0
            End Select
            Return intMobileCode

        End Function
    End Class
End Namespace

