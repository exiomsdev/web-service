﻿
Imports System.Collections.Generic
Imports System.Web
Imports System.Net
Imports System.Xml.XPath
Imports System.Data
Imports System.Diagnostics
Imports System.CodeDom
Imports System.CodeDom.Compiler
Imports System.Security.Permissions
Imports System.Web.Services.Description
Imports System.Reflection
Imports System.Collections

''' <summary>
''' Summary description for CallService
''' </summary>
Namespace Etopup_Ws.old_App_Code
    Public Class CallService
        '
        ' TODO: Add constructor logic here
        '
        Public Sub New()
        End Sub
        Public Function CallWebService(webServiceAsmxUrl As String, serviceName As String, methodName As String, args As Object()) As Object

            'Try
            Dim client As New System.Net.WebClient()

            '-Connect To the web service
            Dim stream As System.IO.Stream = client.OpenRead(webServiceAsmxUrl & Convert.ToString("?wsdl"))

            'Read the WSDL file describing a service.
            Dim description As ServiceDescription = ServiceDescription.Read(stream)

            'LOAD THE DOM'''''''''''''''''''''''''''

            '--Initialize a service description importer.
            Dim importer As New ServiceDescriptionImporter()
            importer.ProtocolName = "Soap12"
            ' Use SOAP 1.2.
            importer.AddServiceDescription(description, Nothing, Nothing)

            '--Generate a proxy client. 

            importer.Style = ServiceDescriptionImportStyle.Client
            '--Generate properties to represent primitive values.
            importer.CodeGenerationOptions = System.Xml.Serialization.CodeGenerationOptions.GenerateProperties

            'Initialize a Code-DOM tree into which we will import the service.
            Dim nmspace As New CodeNamespace()
            Dim unit1 As New CodeCompileUnit()
            unit1.Namespaces.Add(nmspace)

            'Import the service into the Code-DOM tree. 
            'This creates proxy code that uses the service.

            Dim warning As ServiceDescriptionImportWarnings = importer.Import(nmspace, unit1)


            If warning = 0 Then
                '--Generate the proxy code
                Dim provider1 As CodeDomProvider = CodeDomProvider.CreateProvider("VB")
                '--Compile the assembly proxy with the // appropriate references
                Dim assemblyReferences As String() = Nothing
                assemblyReferences = New String() {"System.dll", "System.Web.Services.dll", "System.Web.dll", "System.Xml.dll", "System.Data.dll"}
                Dim parms As New CompilerParameters(assemblyReferences)
                parms.GenerateInMemory = True
                '(Thanks for this line nikolas)
                Dim results As CompilerResults = provider1.CompileAssemblyFromDom(parms, unit1)

                '-Check For Errors

                If results.Errors.Count > 0 Then

                    Throw New System.Exception("Compile Error Occurred calling webservice.")
                End If

                '--Finally, Invoke the web service method
                Dim wsvcClass As Object = results.CompiledAssembly.CreateInstance(serviceName)
                Dim mi As MethodInfo = wsvcClass.[GetType]().GetMethod(methodName)

                Return mi.Invoke(wsvcClass, args)
            Else
                Return Nothing

            End If
            ' Catch ex As Exception
            ' Throw ex
            ' End Try
        End Function
    End Class
End Namespace
